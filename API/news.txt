MONBUILDING - 07/2017
@Author : Rodolphe Abeille
NEWS MODULE

========================================================================================================================================================================

TABLE NAME : "actus" AND "ActuPosts"

PUBLISH

"actus_gestionnaire" : PUBLISH ALL NEWS FROM CONDOS IN CHARGE OF CURRENT MANAGER
Params {
  NO PARAMS
}
Return : [FIND ON "actus" MONGO TABLE, FIND ON "ActuPosts" MONGO TABLE]

"actus" : PUBLISH ALL ACTUS RELATED TO ID RECEIVED AS PARAMETER
Params {
  id                      (String) ID OF ACTUS ("actus" TABLE)
}
Return : FIND ON "actus" MONGO TABLE

"ActuPosts" : PUBLISH ALL ACTUPOST RELATED TO POSTID RECEIVED AS PARAMETER
Params {
  postId                  (String) ID OF ACTUPOST ("ActuPosts" TABLE)
}
Return : FIND ON "ActuPosts" MONGO TABLE

"actuFiles" : PUBLISH ALL FILES RELATED TO ACTUPOST RECEIVED AS PARAMETER
Params {
  actuId                  (String) ID OF ACTUPOST ("ActuPosts" TABLE)
}
Return : CURSOR ON "UserFiles" MONGO TABLE

========================================================================================================================================================================
SCHEMA OF ACTUS
{
    "_id" : "H2LbeKP7ZiYYFcr2c",                  (String) ID OF ELEM IN TABLE
    "unfollowers" : [],                           (Table of String) TABLE OF USER IDS WHO DOESN'T WANT TO RECEIVE NOTIFICATIONS
    "views" : [                                   (Table of Object) TABLE TO TRACK VIEWS OF MODULE
        {
            "userId" : "HQZvtYaawg8c8yBnY",       (String) USERID OF THE VIEWER
            "date" : 1499091915662.0              (Date) DATE OF VIEW
        }
    ]
}

SCHEMA OF ACTUPOSTS
{
    "_id" : "vyW9L3zZjyKvauPty",                  (String) ID OF ELEM IN TABLE
    "title" : "Post title",                       (String) TITLE
    "locate" : "",                                (String) LOCATION
    "description" : "<p>ijh</p>",                 (RichText) DESCRIPTION
    "startDate" : "",                             (String) START DATE DD/MM/YYYY
    "startHour" : "00:00",                        (String) START TIME MM:HH
    "endDate" : "",                               (String) END DATE DD/MM/YYYY
    "endHour" : "00:00",                          (String) END TIME MM:HH
    "actualityId" : "H2LbeKP7ZiYYFcr2c",          (String) ID OF MODULE ("actus" TABLE)
    "userId" : "HQZvtYaawg8c8yBnY",               (String) USERID OF AUTHOR
    "createdAt" : 1499091923993.0,                (Date) DATE OF CREATION
    "views" : [],                                 (Table of Object) TABLE TO TRACK VIEWS OF POST
    "history" : [],                               (Table of Object) HISTORY OF EDITS
    "files" : [],                                 (Table of String) FILEIDs FROM "UserFiles" TABLE
    "condoId" : "XYWcgffWuL3eBaXPF"               (String) CONDOID OF CONDO RELATED TO THE POST
}

========================================================================================================================================================================

SERVER FUNCTIONS

"module-actu-create-post" : CREATE A NEW POST
  Params {
    data                                  (Object) CONTAINS DATA FOR THE CREATION
    {
        title                             (String) TITLE
        locate                            (String) LOCATION
        description                       (String) DESCRIPTION
        startDate                         (String) START DATE DD/MM/YYYY
        startHour                         (String) START TIME MM:HH
        endDate                           (String) END DATE DD/MM/YYYY
        endHour                           (String) END TIME MM:HH
        actualityId                       (String) ID OF MODULE ("actus" TABLE)
        userId                            (String) USERID OF AUTHOR
        createdAt                         (Date) DATE OF CREATION
        views                             (Table of Object) TABLE TO TRACK VIEWS
        history                           (Table of Object) HISTORY OF EDITS
        files                             (Table of String) FILEIDs FROM "UserFiles" TABLE
        condoId                           (String) CONDOID OF CONDO RELATED TO THE POST
    }
  }
  Return : Void

"module-actu-edit-post" : EDIT AN EXISTING POST
  Params {
    postId                                (String) ID OF THE POST TO EDIT
    modifier                              (Object) CONTAINS DATA FOR THE EDITION
    {
        title                             (String) TITLE
        locate                            (String) LOCATION
        description                       (String) DESCRIPTION
        startDate                         (String) START DATE DD/MM/YYYY
        startHour                         (String) START TIME MM:HH
        endDate                           (String) END DATE DD/MM/YYYY
        endHour                           (String) END TIME MM:HH
        actualityId                       (String) ID OF MODULE ("actus" TABLE)
        userId                            (String) USERID OF AUTHOR
        createdAt                         (Date) DATE OF CREATION
        views                             (Table of Object) TABLE TO TRACK VIEWS
        history                           (Table of Object) HISTORY OF EDITS
        files                             (Table of String) FILEIDs FROM "UserFiles" TABLE
        condoId                           (String) CONDOID OF CONDO RELATED TO THE POST
    }
  }
  Return : Void

"module-actu-post-delete" : DELETE AN EXISTING POST
  Params {
    id                                    (String) ID OF ACTUPOST ("ActuPosts" TABLE)
  }
  Return : Void

"module-follow-actu" : FOLLOW / UNFOLLOW AN ACTU MODULE
  Params {
    actualityId                           (String) ID OF THE MODULE TO FOLLOW / UNFOLLOW
  }
  Return : Void

"module-actu-view" : UPDATE THE DATE VIEW OF MODULE OF CURRENT USER
  Params {
    actualityId                           (String) ID OF THE MODULE TO UPDATE
  }
  Return : Void
