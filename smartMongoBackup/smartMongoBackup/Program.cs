﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Globalization;

namespace smartMongoBackup
{
	public class helpers {
		public static string GetDbName(string[] args)
		{
			string[] arg;
			foreach (string str in args)
			{
				arg = str.Split(new string[] { "=" }, StringSplitOptions.None);
				if (arg.Length > 1 && arg[0] == "--db")
					return arg[1];
			}
			return String.Empty;
		}

		public static int GetIso8601WeekOfYear(DateTime time)
		{
			// Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
			// be the same week# as whatever Thursday, Friday or Saturday are,
			// and we always get those right
			DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
			if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
			{
				time = time.AddDays(3);
			}

			// Return the week of our adjusted day
			return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
		}

		public static void FilterYear(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			var currentYear = DateTime.Now.ToString("yyyy");
			var fileYear = directoryInfo.CreationTimeUtc.ToString("yyyy");
			if (fileYear != currentYear)
			{
				string key = "year-" + fileYear;
				if (!Program.older.Year.TryAdd(key, directoryInfo.FullName))
				{
					if (new DirectoryInfo(Program.older.Year[key]).CreationTimeUtc > directoryInfo.CreationTimeUtc)
						Program.older.Year[key] = directoryInfo.FullName;
				}
			}
			else
				FilterMonth(path);
		}

		public static void FilterMonth(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			var currentMonth = DateTime.Now.ToString("MM");
			var fileMonth = directoryInfo.CreationTimeUtc.ToString("MM");
			if (fileMonth != currentMonth)
			{
				string key = "month-" + fileMonth;
				if (!Program.older.Month.TryAdd(key, directoryInfo.FullName))
				{
					if (new DirectoryInfo(Program.older.Month[key]).CreationTimeUtc > directoryInfo.CreationTimeUtc)
						Program.older.Month[key] = directoryInfo.FullName;
				}
			}
			else
				FilterWeek(path);
		}

		public static void FilterWeek(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			var currentWeek = GetIso8601WeekOfYear(DateTime.Now).ToString();
			var fileWeek = GetIso8601WeekOfYear(directoryInfo.CreationTimeUtc).ToString();
			if (fileWeek != currentWeek)
			{
				string key = "week-" + fileWeek;
				if (!Program.older.Week.TryAdd(key, directoryInfo.FullName))
				{
					if (new DirectoryInfo(Program.older.Week[key]).CreationTimeUtc > directoryInfo.CreationTimeUtc)
						Program.older.Week[key] = directoryInfo.FullName;
				}
			}
			else
				FilterDay(path);
		}

		public static void FilterDay(string path)
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			var fileDay = directoryInfo.CreationTimeUtc.ToString("dd");
			string key = "day-" + fileDay;
			if (!Program.older.Day.TryAdd(key, directoryInfo.FullName))
			{
				if (new DirectoryInfo(Program.older.Day[key]).CreationTimeUtc > directoryInfo.CreationTimeUtc)
					Program.older.Day[key] = directoryInfo.FullName;
			}
		}
	}

	class Program
	{
		public static Older older = new Older();
		static void Main(string[] args)
		{
			DateTime now = DateTime.Now;
			// Getting debug mode
			bool debug = false;
			string[] arguments = args;
			if (args.Length > 0 && args[0] == "-v")
			{
				debug = true;
				arguments = ExtendedArray.RemoveAt(args, 0);
			}

			// Initializing and starting `mongodump` process with the right params
			if (!Directory.Exists("backups"))
				Directory.CreateDirectory("backups");
			Process proc = new Process
			{
				StartInfo = new ProcessStartInfo
				{
					FileName = "mongodump",
					Arguments = String.Join(" ", arguments) + " --out=\"backups/backup-" + now.ToString("d").Replace("/", "-") + "\"",
					UseShellExecute = false,
					RedirectStandardOutput = true,
					CreateNoWindow = true
				}
			};
			proc.Start();
			if (debug)
				while (!proc.StandardOutput.EndOfStream)
					Console.WriteLine(proc.StandardOutput.ReadLine());

			if (debug)
				Console.WriteLine("Current time: " + now.ToString());

			string[] backups = Directory.GetDirectories("backups");
			foreach (string backup in backups)
				helpers.FilterYear(backup);

			Dictionary<string, string> toSave = new Dictionary<string, string>();
			foreach (KeyValuePair<string, string> kvp in older.Year)
				toSave.Add(kvp.Key, kvp.Value);
			foreach (KeyValuePair<string, string> kvp in older.Month)
				toSave.Add(kvp.Key, kvp.Value);
			foreach (KeyValuePair<string, string> kvp in older.Week)
				toSave.Add(kvp.Key, kvp.Value);
			foreach (KeyValuePair<string, string> kvp in older.Day)
				toSave.Add(kvp.Key, kvp.Value);

			if (debug)
				Console.WriteLine("Will be deleted:");
			foreach (string backup in backups)
			{
				var fullpath = new DirectoryInfo(backup).FullName;
				if (!toSave.ContainsValue(fullpath))
				{
					if (debug)
						Console.WriteLine("Deleting " + fullpath + "...");
					Directory.Delete(fullpath, true);
				}
			}


			if (debug)
			{
				Console.WriteLine("Will be saved:");
				Console.WriteLine("\n- Year");
				foreach (KeyValuePair<string, string> kvp in older.Year)
					Console.WriteLine(kvp.Key + " | " + kvp.Value);

				Console.WriteLine("\n- Month");
				foreach (KeyValuePair<string, string> kvp in older.Month)
					Console.WriteLine(kvp.Key + "   | " + kvp.Value);

				Console.WriteLine("\n- Week");
				foreach (KeyValuePair<string, string> kvp in older.Week)
					Console.WriteLine(kvp.Key + "   | " + kvp.Value);

				Console.WriteLine("\n- Day");
				foreach (KeyValuePair<string, string> kvp in older.Day)
					Console.WriteLine(kvp.Key + "   | " + kvp.Value);
			}

		}
	}
}
