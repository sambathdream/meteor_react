﻿using System;
using System.Collections.Generic;
namespace smartMongoBackup
{
	public class Older
	{
		public Dictionary<string, string> Year = new Dictionary<string, string>();
		public Dictionary<string, string> Month = new Dictionary<string, string>();
		public Dictionary<string, string> Week = new Dictionary<string, string>();
		public Dictionary<string, string> Day = new Dictionary<string, string>();
		public Older()
		{

		}
	}
}
