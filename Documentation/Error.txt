/*
** Page d'erreur
*/

NotFound :
# Une page demandé n'existe pas
# Résolu par le router
  - code : 404
  - name : "notFound"
  - path : "/error/notfound"
  - route : NA

notAuthenticated :
# L'utilisateur doit se connecter pour accéder à cette page
  - code : 401
  - name : "notAuthenticated"
  - path : "/error/notauthenticated"
  - route : "app.error.notAuthenticated"

accessDenied :
# L'utilisateur est connecté mais n'as pas le droit d'accéder à cette page
  - code : 403
  - name : "accessDenied"
  - path : "/error/accessDenied"
  - route : "app.error.accessDenied"

internalError :
# Une erreur interne est surevenue sur le serveur
# L'application ne peux plus continuer à fonctionner
  - code : 500
  - name : "internalError"
  - path : "/error/internalError"
  - route : "app.error.internalError"

/*
** Code d'erreur des methodes serveur
**
** Une erreur est call côté serveur par :
** throw new Meteor.Error("code", "reason", ["details"]);
** code et reason réfère au tableau suivant. Details est propre à chaque cas
*/
3XX => Non autorisé (prérequis non remplis : user non identifié, role insufisant, etc...)
4XX => Erreur de paramètres (type, quantité, etc...)
5XX => Erreur interne lors du traitement (fichier non existant, calcule impossible, etc...)
6XX => Erreur en bdd (id introuvable, plusieurs résultats au lieu d'un, etc...)

/* 3XX */
  - 300, "Not Authenticated"
    => Pas d'utilisateur identifié (Meteor.userId == undefined)
  - 301, "Access Denied"
    => L'utilisateur n'a pas l'identité nécessaire (ex: pas d'idResident)
  - 302, "Access Denied for id"
    => L'utilisateur n'a pas le droit d'accéder à l'id (ex: condoId non authorisé)
  - 303, "Access Denied for user"
    => L'utilisateur n'a pas le droit d'accéder à la requète (ex: User de gestionnaire pour un condo qui ne lui est pas attribué)
  - 304, "Server Side Only"
    => La méthode ne doit pas être appellé par le client mais uniquement via le server;

/* 4XX */
  - 400, "Invalid request"
    => La requète ne possède pas le bon nombre de paramètres (paramX == undefined)
  - 401, "Invalid parameters"
    => Le type d'un paramètre n'est pas correct (typeof param !== 'string')

/* 5XX */
  - 500, "Internal error"
    => Le serveur n'arrive pas à traiter la requète pour une raison interne
      (couvre tout les cas non prévu par les autres erreurs 5XX)

/* 6XX */
  - 600, "Unkown error"
    => Une requète bdd renvoi un document invalide pour une raison inconnue
  - 601, "Invalid id"
    => Une requète sur un Id renvoi un document empty.
  - 602, "Data Inconsistency"
    => Une requète  sur la bdd fais remonter une incohérence
      (ex :plusieurs document pour un id unique)
  - 603, "Invalid state object"
    => L'etat actuel d'un objet rend la requete incohérente (ex: valider un incident déja validé)
  - 604, "Id not found in document"
    => Un id n'a pas été trouvé dans le document
