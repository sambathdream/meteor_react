![Alt text](https://www.monbuilding.com/img/logo/homeLogo_menu.png "Mon Building")

# **How To**
***
## **Make a good syntax commit**

### **Exemple** :
    <ACTION> [SIDE > MODULE1, MODULE2, ...] : <resume of what you did>

#### **ACTION** :
* new (When you add new features/modules)
* add (When you add something to an existing feature/module)
* chg (When you change something already created before)
* del (When you delete something)
* test (When you need to push some test)
* fix (When you resolve a previous problem)
* err (When you need to push while it doesn't work yet)

#### **SIDE** :
* O => Occupant
* M => Manager
* B => Backoffice
* S => Server
* ? => Others

#### **MODULES** :
* Occupant
	* Actuality
	* ClassifiedsAds
	* Feedlist
	* Forum
	* Incident
	* Manual
	* Map
	* Messenger
	* Reservation
	* Trombinoscope

* Manager
	* SAME_AS_OCCUPANT
	* buildingsView
	* Documents
	* EtatsDesLieux
	* Events
	* Formulaire
	* Planning

* Backoffice
	* Archives
	* Buildings
	* Condo
	* Enterprise
	* Managers
	* User

*Date : 30 Juil 2017*  
*Made By : Ludovic Nouvel*