![MonBuilding](https://monbuilding.com/img/logo/logo.svg "MonBuilding")

-------------------

# 1) Install meteor:
* $ curl https://install.meteor.com/ | sh
* for more informations: https://www.meteor.com/install

-------------------

# 2) Git clone the **monbuildingmeteor** project from bitbucket:
* $ git clone https://<YOUR_LOGIN>@bitbucket.org/MonBuilding/monbuildingmeteor.git
* $ cd monbuildingmeteor/monbuilding
* $ meteor npm install
* $ meteor

-------------------

# 3) Install graphicsmagick:
* $ brew install graphicsmagick

-------------------

# 4) Install Robo 3T:
* https://robomongo.org/download

-------------------

# 5) Good things to know:
* **Meteor's server default port is 3000**. 
It is possible to run a different port by using this command `meteor --port <PORT>` instead of `meteor`
* MongoDB default port is 3001.
