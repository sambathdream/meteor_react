import './setup'
import {appLoadRunner} from './runs/app-load'
import {appFeedRunner} from './runs/load-feed'
import {printReport} from './zenyatta/reports'
import {Session} from './zenyatta/session'

const users = [
  {
    tag: 'resident1',
    state: {
      email: 'resident1@gmail.com',
      password: 'resident'
    }
  },
  {
    tag: 'resident2',
    state: {
      email: 'resident2@gmail.com',
      password: 'resident'
    }
  }
]

async function run () {
  let allUsers = []
  for (let i = 20; i < 221; i += 1) {
    const name = 'resident' + i
    const email = name + '@gmail.com'

    allUsers = [...allUsers, {
      tag: name,
      state: {
        email: email,
        password: 'resident'
      }
    }]
  }
  console.log('==== START ====')
  // await appLoadRunner(allUsers)
  await appFeedRunner(allUsers)

  console.log('==== DONE ====')
  printReport()
  console.log('==== CLOSED ====')
  process.exit()
}

run()

// behavior('open messenger', [

// ])
