import {runForAll} from '../zenyatta/runner'

// actions
import '../actions/login'
import '../actions/feed'

type RunConfig = {
  tag: string,
  state: any
}

type State = {
  email: string,
  password: string
}


export function appFeedRunner(runs: RunConfig[]) {
  return runForAll('Feed Load', runs, [
    'login-connect',
    'login-email',
    // 'login-occupant load badges',
    'login-occupant load data',
    'feed-load-data',
    'feed-load-feed-data',
    'feed-new-post-feed',
  ])
}
