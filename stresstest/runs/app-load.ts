import {runForAll} from '../zenyatta/runner'

// actions
import '../actions/login'

type RunConfig = {
  tag: string,
  state: any
}

type State = {
  email: string,
  password: string
}


export function appLoadRunner(runs: RunConfig[]) {
  return runForAll('App Load', runs, [
    'login-connect',
    'login-email',
    'login-occupant load data'
  ])
}
