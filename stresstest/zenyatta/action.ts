import {performance} from 'perf_hooks'
import { reportAction } from './reports'
import { Session } from './session'

let actions: {[key: string]: (s: Session, state: any) => Promise<any>} = {}

export async function action (name: string, runner: (s: Session, state: any) => Promise<any>) {
  if (actions[name]) {
    throw new Error(`Action already defined: ${name}`)
  }
  actions[name] = runner
}

export async function runAction (name: string, tag: string, session: Session, state: any) {
  const runner = actions[name]
  const start = performance.now()
  await runner(session, state)
  const end = performance.now()
  reportAction(name, tag, end - start, end)
}
