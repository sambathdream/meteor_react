import {performance} from 'perf_hooks'
import {action, runAction} from './action'
import {Session} from './session'
import {reportRun} from './reports'

type RunConfig = {
  tag: string,
  state: any
}

export async function runner(name: string, tag: string, state: any, actions: string[]) {
  const session = new Session()
  const start = performance.now()
  for (const actionName of actions) {
    await runAction(actionName, tag, session, state)
  }
  const end = performance.now()
  reportRun(name, tag, end - start, end)
}

export async function runForAll(name: string, runs: RunConfig[], actions) {
  const runners = []
  for (const run of runs) {
    runners.push(runner(name, run.tag, {...(<Object>run.state)}, actions))
  }
  try {
    await Promise.all(runners)
  } catch (e) {
    console.log(`Error running ${name}`, e)
  }
  return
}
