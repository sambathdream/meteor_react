type reportState =  {
  name: string
  min: number
  max: number
  avg: number
  total: number
  count: number
  history: {
    tag: string,
    timestamp: number
    time: number
  }[]
}
let action: {[key: string]: reportState} = {}
let run: {[key: string]: reportState} = {}

function newEmptyState (name: string): reportState {
  return {
    name,
    min: Number.MAX_SAFE_INTEGER,
    max: 0,
    avg: 0,
    total: 0,
    count: 0,
    history: []
  }
}

export function printReport() {
  console.log('action', action)
  console.log('run', run)
}
export function reportAction(name: string, tag: string, time: number, timestamp: number = performance.now()) {
  const current = action[name] || newEmptyState(name)
  current.min = Math.min(time, current.min)
  current.max = Math.max(time, current.max)
  current.total += time
  current.count += 1
  current.avg = current.total/current.count
  current.history.push({timestamp, time, tag})
  action[name] = current
}

export function reportRun(name: string, tag: string, time: number, timestamp: number = performance.now()) {
  const current = run[name] || newEmptyState(name)
  current.min = Math.min(time, current.min)
  current.max = Math.max(time, current.max)
  current.total += time
  current.count += 1
  current.history.push({timestamp, time, tag})
  run[name] = current
}
