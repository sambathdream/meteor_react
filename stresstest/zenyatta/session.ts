import DDP from '../shooting-star-core/ddp/ddp'
import {Connection} from '../shooting-star-core/ddp/connection'
import {sha256} from 'js-sha256'

type Update = {
  type: 'update' | 'reset'
  collection: {
    [key: string]: {
      collection: string
      msg: 'added' | 'changed' | 'removed'
      id: string,
      fields: any
    }[]
  }
}

export class Session {
  private connection: Connection
  private collections: {[key: string]: any} = {}

  async connect () {
    return new Promise ((resolve, reject) => {
      this.connection =  DDP.connect('ws://localhost:3000/websocket')
      // this.connection.on('*', console.log.bind(console))
      this.connection.on('loaded', resolve)
      this.connection.watchCollection().subscribe(this.onCollectionData.bind(this))
    })
  }

  private onCollectionData(update: Update) {
    if (update.type === 'update') {
      for (const changeList of Object.values(update.collection)) {
        for (const change of changeList) {
          switch (change.msg) {
            case 'added': {
              const collection = this.collections[change.collection] || {}
              collection[change.id] = {_id: change.id, ...change.fields}
              this.collections[change.collection] = collection
              break
            }
            case 'changed': {
              const collection = this.collections[change.collection] || {}
              collection[change.id] = {...collection[change.id], ...change.fields}
              this.collections[change.collection] = collection
              break
            }
            case 'removed': {
              const collection = this.collections[change.collection] || {}
              delete collection[change.id]
              this.collections[change.collection] = collection
              break
            }
          }
        }
      }
    }
  }

  collection(name): any {
    return this.collections[name] || {}
  }

  collectionNames() {
    return Object.keys(this.collections)
  }

  async login (email, password) {
    const credentials = {
      user: {email},
      password: {
        digest: sha256(password),
        algorithm: 'sha-256'
      }
    }
    return await this.connection.waitAndCall('login', credentials)
  }

  close () {
    this.connection.close()
  }

  subscribe (name: string, ...args) {
    return new Promise ((resolve, reject) => {
      this.connection.subscribe(name, ...args).observable.subscribe(({state, error}) => {
        if (state === 'ready') {
          resolve()
        } else if (state === 'error') {
          reject(error)
        } else if (state === 'stopped') {
          reject(new Error(`Subscription ${name} is stopped`))
        }
      })
    })
  }

  call (name: string, ...args) {
    return this.connect.call(name, ...args)
  }
}
