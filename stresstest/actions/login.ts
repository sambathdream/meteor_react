import { Session } from '../zenyatta/session'
import { action } from '../zenyatta/action'

action('login-connect', async (session: Session) => {
  await session.connect()
})

action('login-email', async (session: Session, {email, password}: {email: string, password: string}) => {
  await session.login(email, password)
})

action('login-occupant load badges', async (session: Session, state: any) => {
  session.subscribe('resident_badges')
})

action('login-occupant load data', async (session: Session, state: any) => {
  await session.subscribe('board_resident')
  const condos = session.collection('condos')
  const condoId = Object.values<any>(condos)[0]._id
  state.selectedCondo = condoId
  await session.subscribe('avatarLinks', condoId)
})
