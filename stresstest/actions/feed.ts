import { Session } from '../zenyatta/session'
import { action } from '../zenyatta/action'

action('feed-load-data', async (session: Session, {selectedCondo}: {selectedCondo: string}) => {
  await Promise.all([
    session.subscribe('module_incidents_resident', selectedCondo),
    session.subscribe('avatarLinks', selectedCondo),
    session.subscribe('messagerie'),
    session.subscribe('getCondosRole'),
    session.subscribe('getDefaultRoles')
  ])
})

action('feed-load-feed-data', async (session: Session, state: {selectedCondo: string, date: Date}) => {
  const {selectedCondo} = state
  const date = new Date()
  state.date = date
  await Promise.all([
    session.subscribe('feedActu', selectedCondo, 10, date),
    session.subscribe('feedClassified', selectedCondo, 10, date),
    session.subscribe('feedForum', selectedCondo, 10, date),
    session.subscribe('feedIncident', selectedCondo, 10, date.getTime()),
    session.subscribe('feedUsersJoined', selectedCondo, 10, date)
  ])
})

action('feed-new-post-feed', async (session: Session, {selectedCondo, date}: {selectedCondo: string, date: Date}) => {
  session.subscribe('newPostFeed', selectedCondo, date.getTime(), date)
})



