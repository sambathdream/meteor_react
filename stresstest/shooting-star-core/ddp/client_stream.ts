import {Subject} from 'rxjs/Subject'
import {BehaviorSubject} from 'rxjs/BehaviorSubject'
import {Observable} from 'rxjs/Observable'
import * as retry from 'retry'
import {Timer, setTimeoutP} from '../helper/timer'
import {logError} from '../logger'

type CSOptions = {
  retry?: boolean
  url?: string
  connectTimeoutMs?: number
}

export class ClientStream {
  options: CSOptions
  eventCallbacks: {
    message: Subject<any>
    reset: Subject<any>
    disconnect: Subject<any>
  }

  _forcedToDisconnect: boolean
  _statusSubject: BehaviorSubject<any>
  _cleanupOperation: () => any
  operation: any

  status: Observable<any>
  currentStatus: {
    status: string,
    connected: boolean,
    connecting: boolean
  }
  connectionTimeout: number
  socket: WebSocket
  _rawUrl: string

  constructor (url, options?: CSOptions) {
    options = {
      retry: true,
      ...options
    }

    /* initialization */
    this.options = options
    // how long to wait until we declare the connection attempt
    // failed.
    this.eventCallbacks = {
      message: new Subject(),
      reset: new Subject(),
      disconnect: new Subject()
    }
    this._forcedToDisconnect = false
    this._statusSubject = new BehaviorSubject(null)
    this.status = this._statusSubject.asObservable()
    this.currentStatus = {
      status: 'offline',
      connected: true,
      connecting: false
    }

    this.connectionTimeout = options.connectTimeoutMs || 10000

    // this.heartbeatTimer = new Timer({
    //   time: 100 * 1000,
    //   callback: this._onHeartbeatTimeout.bind(this)
    // })

    this.rawUrl = url
    this.socket = null
    this._forcedToDisconnect = false

    /* start connection */
    this._launchConnection()
  }

  set rawUrl (url) {
    this._rawUrl = url.replace(/^http/, 'ws')
  }
  get rawUrl () {
    return this._rawUrl
  }

  send (data) {
    if (this.socket && this.socket.readyState === this.socket.OPEN) {
      // console.log('SEND', data)
      this.socket.send(data)
    }
  }

  on (name: string, callback?) {
    const subject = this.eventCallbacks[name]
    if (!subject) {
      throw new Error(`Unknown event type ${name}`)
    }
    if (callback) {
      subject.subscribe(callback)
    } else {
      return subject.asObservable()
    }
  }

  async reconnect (options: CSOptions = {}) {
    await this.disconnect(false, true)
    if (options.url) {
      this.rawUrl = options.url
    }
    this._launchConnection()
  }

  disconnect (failed = false, silent = false) {
    this._forcedToDisconnect = true
    if (failed) {
      this._setState({
        status: 'failed',
        error: failed,
        connected: false,
        connecting: false
      })
    } else if (silent) {
      this._setState({
        status: 'offline',
        connecting: false
      })
    } else {
      this._setState({
        status: 'offline',
        connected: false,
        connecting: false
      })
    }
    this._cleanup()
    this._forcedToDisconnect = false
  }

  watchStatus () {
    return this._statusSubject.asObservable()
  }

  getStatus () {
    return {...this.currentStatus}
  }

  retryConnection () {
    if (this.currentStatus.connected) { return }
    if (this._cleanupOperation) {
      this._cleanupOperation()
      this._cleanupOperation = undefined
    }
    this._launchConnection()
  }

  _setState (state) {
    this.currentStatus = {...this.currentStatus, ...{error: false}, ...state}
    this._statusSubject.next(this.currentStatus)
  }

  _onmessage (data) {
    // this._onHeartbeatReceived()
    this.eventCallbacks.message.next(data.data)
  }

  _onconnection () {
    this._setState({
      status: 'connected',
      connected: true
    })
    this.eventCallbacks.reset.next()
  }

  _lostConnection (error) {
    console.log('_lostConnection')
    this._setState({
      status: 'waiting'
    })
    this._launchConnection(error)
  }

  // _onHeartbeatReceived () {
  //   this.heartbeatTimer.reset()
  // }

  _onHeartbeatTimeout () {
    console.log('_onHeartbeatTimeout')
    this._lostConnection(new Error('Heartbeat timed out'))
  }

  async _launchConnection (fromError?) {
    if (this._forcedToDisconnect) {
      return
    }
    this._cleanup(fromError)
    this._setState({
      status: 'connection',
      connecting: true
    })
    try {
      const socket: WebSocket = await this._connect()
      socket.onclose = this._lostConnection.bind(this)
      socket.onmessage = this._onmessage.bind(this)
      this.socket = socket
      this._onconnection()
      // this.heartbeatTimer.start()
    } catch (e) {
      if (this._forcedToDisconnect) {
        this._setState({
          status: 'offline',
          connected: false,
          connecting: false
        })
      } else {
        this._setState({
          status: 'failed',
          connected: false,
          connecting: false
        })
        logError(e)
      }
    }
  }

  async _connect (): Promise<WebSocket> {
    if (this._forcedToDisconnect) { throw new Error('Forced to disconnect') }
    let stop = false
    return new Promise<WebSocket>((resolve, reject) => {
      if (this._cleanupOperation) { reject(new Error('DDP connection already in process')) }
      this.operation = retry.operation()
      this._cleanupOperation = () => {
        this.operation.stop()
        stop = true
        this._cleanupOperation = undefined
        reject(new Error('DDP connection aborted'))
      }
      this.operation.attempt(async currentAttempt => {
        if (this._forcedToDisconnect) { throw new Error('Forced to disconnect') }
        try {
          if (stop) { return }
          resolve(await this._connectSingle())
          this._cleanupOperation = undefined
          this.operation = null
        } catch (e) {
          if (!this.operation.retry(e)) {
            this._cleanupOperation = undefined
            if (stop) { return }
            reject(e)
          }
        }
      })
    })
  }

  async _connectSingle (): Promise<WebSocket> {
    if (this._forcedToDisconnect) { throw new Error('Forced to disconnect') }
    const socket = new WebSocket(this.rawUrl)
    const conn = new Promise((resolve, reject) => {
      socket.onopen = resolve
      socket.onclose = () => reject(new Error('stream closed'))
      socket.onerror = (event) => reject(new Error('Error with WebSocket'))
    })
    try {
      await Promise.race([
        conn,
        setTimeoutP(this.connectionTimeout).then(() => { throw new Error('DDP connection timeout') })]
      )
      return socket
    } catch (e) {
      throw e
    } finally {
      socket.onopen = undefined
      socket.onclose = undefined
      socket.onerror = undefined
    }
  }

  _cleanup (fromError?) {
    if (this._cleanupOperation) {
      this._cleanupOperation()
    }
    // this.heartbeatTimer.stop()
    if (this.socket) {
      const cb = () => {}
      this.socket.onmessage = cb
      this.socket.onclose = cb
      this.socket.onerror = cb
      this.socket.onopen = cb
      this.socket.close()
      this.socket = null
    }
    this.eventCallbacks.disconnect.next(fromError)
  }
}
