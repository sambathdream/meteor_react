import * as EJSON from 'ejson'

class ObjectID {
  _str: string
  constructor (hexString) {
    if (hexString) {
      hexString = hexString.toLowerCase();
      if (!_looksLikeObjectID(hexString)) {
        throw new Error('Invalid hexadecimal string for creating an ObjectID')
      }
      // meant to work with _.isEqual(), which relies on structural equality
      this._str = hexString
    } else {
      throw new Error('Hexstring should be provided')
    }
  }

  typeName () { return 'oid' }

  toString () {
    return "ObjectID(\"" + this._str + "\")"
  }

  equals (other) {
    return other instanceof ObjectID &&
      this.valueOf() === this.valueOf()
  }

  clone () {
    return new ObjectID(this._str)
  }

  getTimestamp () {
    return parseInt(this._str.substr(0, 8), 16)
  }

  valueOf () { return this._str }
  toJSONValue () { return this._str }
  toHexString () { return this._str }
}

function _looksLikeObjectID (str) {
  return str.length === 24 && str.match(/^[0-9a-f]*$/)
}

EJSON.addType('oid', function (str) {
  return new ObjectID(str)
})
