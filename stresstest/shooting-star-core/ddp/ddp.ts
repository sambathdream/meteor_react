import {Connection} from './connection'
class DDP {
  _connections: Connection[]
  constructor () {
    // connection cache
    this._connections = []
  }

  // Creates new conneciton and stores it
  connect (url, options = {}) {
    const connection = new Connection(url, options)
    this._connections.push(connection)
    return connection
  }

}

export default new DDP()
