import {Subject} from 'rxjs/Subject'
import {Timer} from '../helper/timer'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/filter'

type CollectionBuffer = {
  type: string
  collection: any
}

export class Collection {
  private _subject: Subject<any>
  private _flushTimer: Timer
  private _buffer: CollectionBuffer

  constructor (connection) {
    connection.on('added changed removed', this._onOperation.bind(this))
    connection.on('ready updated', this._flush.bind(this))

    this._subject = new Subject()
    this._flushTimer = new Timer({
      time: 200,
      callback: this._flush.bind(this)
    })
    this._buffer = {type: 'update', collection: {}}
  }

  resetStores (force: boolean = false) {
    this._flushTimer.stop()
    this._buffer = {type: 'update', collection: {}}
    this._subject.next({type: 'reset', force, collection: {}})
  }

  watchCollection (name) {
    if (!name) {
      return this._subject.asObservable()
    } else {
      return this._subject.asObservable()
        .map(buffer => ({...buffer, collection: buffer.collection[name]}))
        .filter(buffer => buffer.type === 'reset' || buffer.collection)
    }
  }

  _onOperation (msg) {
    const {collection} = msg
    if (!this._buffer.collection[collection]) {
      this._buffer.collection[collection] = []
    }
    this._buffer.collection[collection].push(msg)
    this._flushTimer.reset()
  }

  _flush () {
    this._flushTimer.stop()
    if (Object.keys(this._buffer.collection).length > 0) {
      this._subject.next(this._buffer)
    }
    this._buffer = {type: 'update', collection: {}}
  }
}
