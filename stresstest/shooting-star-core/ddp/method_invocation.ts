import {Mutex} from '../helper/mutex'
import { logDebug } from '../logger'
import {Connection} from './connection'

export class MethodInvocation {
  _pendingMethodBlocks: any[]
  _connection: Connection
  _mutex: Mutex
  _id: number

  constructor (connection) {
    this._pendingMethodBlocks = []
    this._connection = connection
    this._mutex = new Mutex()
    this._id = 0
    this._connection.on('result', this._methodResolved.bind(this))
    this._connection.on('updated', this._methodUpdated.bind(this))
  }

  async call (name, args, options, callback) {
    const id = this._newId()
    this._mutex.lock(id)
    const invocation = { id, name, args, started: false }
    this._performInvocation(invocation, options.wait)
    const mId = await this._mutex.wait(id, callback)
    return mId
  }

  onResetConnection () {
    this._sendBlock()
  }

  reset () {
    this._pendingMethodBlocks = []
  }

  _performInvocation (invocation, wait = false) {
    let lastBlock = this._pendingMethodBlocks[this._pendingMethodBlocks.length - 1]
    if (wait || !lastBlock || lastBlock.wait) {
      lastBlock = { wait, methods: [] }
      this._pendingMethodBlocks.push(lastBlock)
    }
    lastBlock.methods.push(invocation)
    if (this._pendingMethodBlocks.length === 1) {
      this._invoke(invocation)
    }
  }

  _invoke (invocation) {
    const {id, name, args} = invocation
    invocation.started = true
/*    console.log('invoke call', {
      msg: 'method',
      method: name,
      params: args,
      id
    })*/
    this._connection._send({
      msg: 'method',
      method: name,
      params: args,
      id
    })
  }

  _findMethod (id) {
    let block = this._pendingMethodBlocks[0]
    if (!block) {
      this._mutex.fail(id)
      logDebug('Received method result but no methods pending', id)
      return {}
    }
    const methods = block.methods
    let i = 0
    let m = null
    for (; i < methods.length; i += 1) {
      if (methods[i].id === id) {
        m = methods[i]
        break
      }
    }
    return {
      idx: i,
      method: m
    }
  }

  _tryCallMethod ({id, name, error, result, updated, hasResult}, idx) {
    if (hasResult && updated) {
      let block = this._pendingMethodBlocks[0]
      if (!block) {
        logDebug('no block, this should not happen EVER!')
      }
      const methods = block.methods
      methods.splice(idx, 1)
      if (error) {
        this._mutex.fail(id, error)
      } else {
        this._mutex.release(id, result)
      }

      this._trySendNextBlock()
    }
  }

  _methodResolved ({id, result, error}) {
    //console.log('invoke result', {id, result, error})
    const {idx, method} = this._findMethod(id)
    if (!method) {
      this._mutex.fail(id)
      logDebug('Can\'t match method response to original method call', id, result || error)
      return
    }
    method.result = result
    method.error = error
    method.hasResult = true
    this._tryCallMethod(method, idx)
  }

  _methodUpdated ({methods}) {
    methods.forEach(id => {
      const {idx, method} = this._findMethod(id)
      if (!method) {
        logDebug(`Updtaing non existant method ${id}`)
        return
      }
      method.updated = true
      this._tryCallMethod(method, idx)
    })
  }

  _trySendNextBlock () {
    let block = this._pendingMethodBlocks[0]
    if (block && block.methods.length === 0) {
      this._pendingMethodBlocks.shift()
      this._sendBlock()
    }
  }

  _sendBlock () {
    if (this._pendingMethodBlocks.length > 0) {
      const block = this._pendingMethodBlocks[0]
      block.methods.forEach(m => !m.started && this._invoke(m))
    }
  }

  _newId () {
    return '' + (this._id++)
  }
}
