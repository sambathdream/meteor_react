import { logDebug } from '../logger'
import * as EJSON from 'ejson'
import './mongo_id'

export function parseDDP (stringMessage) {
  try {
    var msg = JSON.parse(stringMessage)
  } catch (e) {
    logDebug('Discarding message with invalid JSON', stringMessage)
    return null
  }
  // DDP messages must be objects.
  if (msg === null || typeof msg !== 'object') {
    logDebug('Discarding non-object DDP message', stringMessage)
    return null
  }

  // massage msg to get it into "abstract ddp" rather than "wire ddp" format.

  // switch between "cleared" rep of unsetting fields and "undefined"
  // rep of same
  if (msg.hasOwnProperty('cleared')) {
    if (!msg.hasOwnProperty('fields')) {
      msg.fields = {}
    }
    msg.cleared.forEach(function (clearKey) {
      msg.fields[clearKey] = undefined
    })
    delete msg.cleared
  }

  ['fields', 'params', 'result'].forEach(function (field) {
    if (msg.hasOwnProperty(field)) {
      msg[field] = EJSON._adjustTypesFromJSONValue(msg[field])
    }
  })

  return msg
}

export function stringifyDDP (msg) {
  var copy = EJSON.clone(msg)
  // swizzle 'changed' messages from 'fields undefined' rep to 'fields
  // and cleared' rep
  if (msg.hasOwnProperty('fields')) {
    var cleared = []
    msg.fields.forEach(function (value, key) {
      if (value === undefined) {
        cleared.push(key)
        delete copy.fields[key]
      }
    })
    if (cleared.length > 0) {
      copy.cleared = cleared
    }
    if (copy.fields && copy.fields.length === 0) {
      delete copy.fields
    }
  }
  // adjust types to basic
  ['fields', 'params', 'result'].forEach(function (field) {
    if (copy.hasOwnProperty(field)) {
      copy[field] = EJSON._adjustTypesToJSONValue(copy[field])
    }
  })
  if (msg.id && typeof msg.id !== 'string') {
    throw new Error('Message id is not a string')
  }
  return JSON.stringify(copy)
}
