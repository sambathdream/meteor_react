import * as EJSON from 'ejson'
import { stringifyDDP } from './utils'
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import { Observable } from 'rxjs/Observable'
import { logDebug } from '../logger'
import { Subject } from 'rxjs/Subject'
import {Connection} from './connection'

type SubscriptionData = {

  id: string
  name:string
  ready: () => boolean
  observable: Observable<any>

  _inactive: boolean
  _ready: false,
  _remove: () => void
  _markReady: () => void
  _markError: (error: string) => void
  _send: () => void,
  _completeStop: () => void
  stop?: () => Promise<any>
}

export class Subscription {
  _connection: Connection
  _subscriptions: {[key: string]: SubscriptionData}
  _hashId: any
  _id: number
  _watchSubscription: Subject<any>

  constructor (connection) {
    this._connection = connection
    this._subscriptions = {}
    this._hashId = {}
    this._id = 0
    this._connection.on('ready', this._onReady.bind(this))
    this._connection.on('nosub', this._onNoSub.bind(this))
    this._watchSubscription = new Subject()
  }

  subscribe (name: string, ...args) {
    const {callbacks, params} = this._parseArgs(args)
    const hash = this._getSubscriptionHash(name, params)

    let subscription = this._subscriptions[this._hashId[hash]]
    const connection = this._connection
    const subscriptions = this._subscriptions
    const watchSubscription = this._watchSubscription

    if (!subscription) {
      const id = this._getNewId()
      const subject = new BehaviorSubject(null)

      subscription = {
        get id () { return id },
        get name () { return name },
        get ready () { return subscriptions[id] && this._ready },
        get observable () { return subject.asObservable() },

        _inactive: false,
        _ready: false,

        _remove () {
          delete subscriptions[id]
        },
        _markReady () {
          this._ready = true
          subject.next({subscription: this, state: 'ready'})
          watchSubscription.next({name, state: 'ready'})
        },
        _markError (error) {
          this._ready = false
          this._remove()
          subject.next({subscription: this, state: 'error', error})
          watchSubscription.next({name, state: 'error', error})
          if (this._resolveStop) {
            this._resolveStop()
            this._resolveStop = null
          }
        },
        _send () {
          connection._send({msg: 'sub', id, name, params})
          this._ready = false
          subject.next({subscription, state: 'waiting'})
          watchSubscription.next({name, state: 'waiting'})
        },
        _completeStop () {
          subject.next({subscription: this, state: 'stopped'})
          watchSubscription.next({name, state: 'stopped'})
          this._remove()
          if (this._resolveStop) {
            this._resolveStop()
            this._resolveStop = null
          }
        },
        stop () {
          if (!this._removePromise) {
            this._removePromise = new Promise(resolve => { this._resolveStop = resolve })
            connection._send({msg: 'unsub', id})
          }
          return this._removePromise
        }
      }
      subscription._send()
      this._subscriptions[id] = subscription
      this._hashId[hash] = id
    }
    this._bindCallbacks(callbacks, subscription)
    return subscription
  }

  watchSubscription (name?: string) {
    return this._watchSubscription.asObservable()
  }

  unsubscribeAll (force: boolean = false) {
    if (force) {
      Object.values(this._subscriptions).map(s => s._completeStop())
      return Promise.resolve()
    } else {
      return Promise.all(Object.values(this._subscriptions).map(s => s.stop()))
    }

  }

  reset () {
    Object.values(this._subscriptions).forEach(s => s._send())
  }

  _onReady ({subs}) {
    subs.forEach(subId => {
      const sub = this._subscriptions[subId]
      if (!sub) {
        logDebug(`DDP received ready for unknown sub ${subId}`)
      } else if (sub.ready) {
        logDebug(`DDP sub is already ready ${subId}`)
      } else {
        sub._markReady()
      }
    })
  }

  _onNoSub ({id, error}) {
    const sub = this._subscriptions[id]
    if (!sub) {
      logDebug(`DDP NoSub arrived non existing sub ${id}`)
    } else {
      if (error) {
        sub._markError(error)
      } else {
        sub._completeStop()
      }
    }
  }

  _bindCallbacks (callbacks, subscription) {
    if (callbacks) {
      subscription.observable.subscribe(({state, error}) => {
        switch (state) {
          case 'stopped': return callbacks.onStop && callbacks.onStop()
          case 'ready': return callbacks.onReady && callbacks.onReady()
          case 'error': return callbacks.onError && callbacks.onError(error)
        }
      })
    }
  }

  _getNewId () {
    return `${this._id++}`
  }

  _getSubscriptionHash (name, params) {
    return `${name}%SS%${stringifyDDP(params)}`
  }

  _parseArgs (params) {
    let callbacks = {}
    let lastParam = params[params.length - 1] || {}
    if (typeof lastParam === 'function') {
      lastParam = {onReady: lastParam}
    }
    if (
      typeof lastParam.onReady === 'function' ||
      typeof lastParam.onError === 'function' ||
      typeof lastParam.onStop === 'function'
    ) {
      params.pop()
      callbacks = lastParam
    }
    return {callbacks, params}
  }
}
