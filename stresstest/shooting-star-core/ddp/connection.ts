import {SUPPORTED_DDP_VERSIONS} from './common'
import {logDebug} from '../logger'
import {ClientStream} from './client_stream'
import {stringifyDDP, parseDDP} from './utils'
import {Subject} from 'rxjs/Subject'
import {Timer} from '../helper/timer'
import * as EJSON from 'ejson'
import {MethodInvocation} from './method_invocation'
import {Collection} from './collection'
import {Subscription} from './subscription'

export class Connection {
  connectionValidator: (conn: Connection) => any
  _supportedDDPVersions: string[]
  _stream: ClientStream
  messageEvents: {[key: string]: any}
  _heartbeat: Timer
  heartbeatTimer: Timer
  heartbeatTimeoutTimer: Timer
  collection: Collection
  methodInvoker: MethodInvocation
  subscription: Subscription
  _autoLogin: Function
  _versionSuggestion: string
  _version: string
  _lastSessionId: string

  constructor (url, options) {
    this.connectionValidator = options.connectionValidator
    // Socket events
    this._supportedDDPVersions = SUPPORTED_DDP_VERSIONS
    this._stream = new ClientStream(url)
    this._stream.on('message', this._onSocketMessage.bind(this))
    this._stream.on('reset', this._onSocketReset.bind(this))
    this._stream.on('disconnect', this._onSocketDisconnect.bind(this))

    // Message type
    this.messageEvents = {}
    this.on('connected', this._onConnected.bind(this))
    this.on('failed', this._onFailed.bind(this))
    this.on('ping', this._onPing.bind(this))
    this.on('pong')
    this.on('error', this._onError.bind(this))

    // heartbeat
    this.heartbeatTimer = new Timer({
      time: 17500,
      callback: this._sendHeartbeat.bind(this)
    })

    this.heartbeatTimeoutTimer = new Timer({
      time: 15000,
      callback: this._heartbeatTimeout.bind(this)
    })

    // methods
    this.collection = new Collection(this)
    this.methodInvoker = new MethodInvocation(this)
    this.subscription = new Subscription(this)

  }

  on (nameValues, callback?, replace = false) {
    const nameList = nameValues.split(' ').filter(a => a !== '')
    const observableList = nameList.map(name => {
      if (!this.messageEvents[name] || replace) {
        this.messageEvents[name] = new Subject()
        this.messageEvents[name].__observable = this.messageEvents[name].asObservable()
      }
      const subject = this.messageEvents[name]
      if (callback) {
        subject.__observable.subscribe(callback)
      } else {
        return subject.__observable
      }
    })
    if (observableList.length === 1) {
      return observableList[0]
    } else if (observableList.length > 1) {
      return observableList
    } else {
      return undefined
    }
  }

  onStatus (name) {
    return this._stream.on(name)
  }

  call (name, ...args) {
    let callback
    if (typeof args[args.length - 1] === 'function') {
      callback = args.pop()
    }
    return this._rawCall(name, args, callback)
  }

  waitAndCall (name, ...args) {
    let callback
    if (typeof args[args.length -1] === 'function') {
      callback = args.pop()
    }
    return this._rawCall(name, args, {wait: true}, callback)
  }

  subscribe (name: string, ...args) {
    return this.subscription.subscribe(name, ...args)
  }

  watchCollection (name?: string) {
    return this.collection.watchCollection(name)
  }

  watchSubscription (name) {
    return this.subscription.watchSubscription(name)
  }

  watchStatus () {
    return this._stream.watchStatus()
  }

  getStatus () {
    return this._stream.getStatus()
  }

  setConnectionValidator (validator) {
    if (typeof validator === 'function') {
      this.connectionValidator = validator
    }
  }

  /* ************** *
   * Stream methods *
   * ************** */

  _send (msg) { this._stream.send(stringifyDDP(msg)) }

  _lostConnection (error?: Error) { this._stream._lostConnection(error || new Error('DDP lost connection.')) }

  get status () { return this._stream.status }

  reconnect () { return this._stream.reconnect() }

  disconnect (failed) { return this._stream.disconnect(failed) }

  close () { return this._stream.disconnect() }

  registerAutoLogin (cb)  {
    this._autoLogin = cb
  }

  removeAutoLogin () {
    this._autoLogin = null
  }

  unsubscribeAll () {
    return this.subscription.unsubscribeAll()
  }

  cleanSession () {
    this.subscription.unsubscribeAll(true)
    this.collection.resetStores(true)
    this.methodInvoker.reset()
    return this.reconnect()
  }

  retryConnection () {
    this._stream.retryConnection()
  }

  /* ************** *
   * Message events *
   * ************** */

  async _onConnected (msg) {
    if (this.connectionValidator) {
      const msg = await this.connectionValidator(this)
      if (msg) {
        this.disconnect(msg)
        const readySub = this.messageEvents['loaded']
        if (readySub) {
          readySub.next()
        }
        return
      }
    }
    this._version = this._versionSuggestion
    let resetStores = !!this._lastSessionId

    if (typeof msg.session === 'string') {
      var reconnectedToPreviousSession = this._lastSessionId === msg.session
      this._lastSessionId = msg.session
    }

    if (reconnectedToPreviousSession) {
      // if reconnected to previous session, nothing should be done
      // this actually never happens
      return
    }
    this.methodInvoker.onResetConnection()
    this.collection.resetStores()
    if (this._autoLogin) {
      await this._autoLogin()
    }
    this.subscription.reset()

    this._reset(resetStores)
    const readySub = this.messageEvents['loaded']
    if (readySub) {
      readySub.next()
    }
  }

  _onFailed (msg) { console.log(msg) }

  _onPing (msg) { this._send({msg: 'pong', id: msg.id}) }

  _onError (msg) { console.log(msg) }

  /* ************* *
   * Socket events *
   * ************* */

  _onSocketMessage (rawMsg) {
    const msg = parseDDP(rawMsg)

    this._onHeartbeat()

    if (msg === null || !msg.msg) {
      // XXX COMPAT WITH 0.6.6. ignore the old welcome message for back
      // compat.  Remove this 'if' once the server stops sending welcome
      // messages (stream_server.js).
      if (!(msg && msg.server_id)) {
        logDebug('discarding invalid livedata message', msg)
      }
      return
    }
    const subject = this.messageEvents[msg.msg]
    if (!subject) {
      logDebug('discarding unknown livedata message type', msg)
      return
    }
    subject.next(msg)

    const allSubject = this.messageEvents['*']
    if (allSubject) {
      allSubject.next(msg)
    }
  }

  _onSocketReset () {
    const msg: any = {msg: 'connect'}
    if (this._lastSessionId) {
      msg.session = this._lastSessionId
    }
    msg.version = this._versionSuggestion || this._supportedDDPVersions[0]
    this._versionSuggestion = msg.version
    msg.support = this._supportedDDPVersions
    this._send(msg)
  }

  _onSocketDisconnect () {
    if (this._heartbeat) {
      this._heartbeat.stop()
      this._heartbeat = null
    }
  }

  /* ********** *
   * Hearthbeat *
   * ********** */

  _sendHeartbeat () {
    this._send({msg: 'ping'})
    this.heartbeatTimeoutTimer.start()
  }

  _heartbeatTimeout () {
    this._lostConnection(
      new Error('DDP heartbeat timed out')
    )
  }

  _onHeartbeat () {
    this.heartbeatTimer.reset()
    this.heartbeatTimeoutTimer.stop()
  }

  /* ************* *
   * state methods *
   * ************* */

  _reset (resetStores) {

  }

  _rawCall (name, args, options = {}, callback?) {
    if (!callback && typeof options === 'function') {
      callback = options
      options = {}
    }
    args = EJSON.clone(args)
    return this.methodInvoker.call(name, args, options, callback)
  }

}
