/*
 * @providesModule meteor/logger
 */

export function logDefault (...args) {
  console.log(...args)
}

export function logError (...args) {
  console.log(...args)
}

export function logDebug (...args) {
  console.log(...args)
}
