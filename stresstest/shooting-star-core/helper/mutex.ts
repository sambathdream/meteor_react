type Handler = {
  promise?: Promise<any>
  resolve?: (...args) => void
  reject?: (...args) => void
}
export class Mutex {
  pool: {[key: string]: Handler}

  constructor () {
    this.pool = {}
  }

  lock (name) {
    if (!this.pool[name]) {
      const handler: Handler = {}
      handler.promise = new Promise((resolve, reject) => {
        handler.resolve = resolve
        handler.reject = reject
      })
      this.pool[name] = handler
      return true
    } else {
      return false
    }
  }

  wait (name, callback) {
    if (!this.pool[name]) {
      if (typeof callback === 'function') {
        callback()
      } else {
        return Promise.resolve(undefined)
      }
    } else {
      if (callback) {
        this.pool[name].promise.then(callback)
      } else {
        return this.pool[name].promise
      }
    }
  }

  release (name, ...args) {
    if (!this.pool[name]) {
      return false
    } else {
      this.pool[name].resolve(...args)
      this.pool[name] = null
      return true
    }
  }

  fail (name, error?: any) {
    if (!this.pool[name]) {
      return false
    } else {
      this.pool[name].reject(error)
      this.pool[name] = null
      return true
    }
  }
}
