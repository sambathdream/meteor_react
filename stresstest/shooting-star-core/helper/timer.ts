export class Timer {
  _time: number
  stopped: boolean
  _callback: Function
  timerHandler: number

  constructor ({time = 0, callback = () => {}}) {
    this.time = time
    this.stopped = true
    this.callback = callback
  }

  set time (time) {
    if (typeof time !== 'number') {
      throw new Error(`time is not a number: ${time}`)
    }
    this._time = time
  }
  get time () { return this._time }

  set callback (callback) {
    if (typeof callback !== 'function') {
      throw new Error(`callback is not a function: ${callback}`)
    }
    this._callback = callback
  }
  get callback () { return this._callback }

  start () {
    this.stopped = false
    this.reset()
  }

  reset () {
    this.stopped = false
    this._cleanTimer()
    this.timerHandler = setTimeout(this._runCallback.bind(this), this._time)
  }

  stop () {
    this.stopped = true
    this._cleanTimer()
  }

  _runCallback () {
    if (this.stopped) { return }
    this._callback()
  }

  _cleanTimer () {
    if (this.timerHandler) {
      clearTimeout(this.timerHandler)
      this.timerHandler = null
    }
  }
}

export function setTimeoutP (time) {
  return new Promise(resolve => setTimeout(resolve, time))
}
