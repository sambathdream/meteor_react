# Déploiement continue
> avec Bitbucket Pipeline

#### Informations

La branche `master` correspond a l’environnent de PROD.
La branche `develop` correspond a l’environnent de DEV.

Tout commits sur une de ces deux branches vont trigger un build de l'application sur `Bitbucket Pipeline`.

Si pas d'erreur lors du build, pipeline va uploader le build générer sur le serveur distant `/tmp/monbulding-build-(prod|dev).tar.gz` puis trigger le re-déploiement de l'instance via le script `/home/bb-pipeline/pipeline-event.sh`

Les deux instances ce trouvent respectivement a l'emplacement :

  * /opt/monbulding-dev/
  * /opt/monbulding-prod/

Toute les opérations sur le serveur sont effectues par l'utilisateur `bb-pipeline`

#### Configuration

https://bitbucket.org/MonBuilding/monbuildingmeteor/admin/addon/admin/pipelines/repository-variables

Attribuez une valeur aux variables suivantes :
```
PROD_HOST
PROD_PORT

DEV_HOST
DEV_PORT
```

L'URL utiliser pour accéder a l'instance doit être assigne a la variable HOST dans ce format :
> http://DOMAIN[:port]/

*commence par http(s):// et termine par un /*


#### Les différents fichiers

Sur le repository :

  * [/docker-compose.yaml](https://bitbucket.org/MonBuilding/monbuildingmeteor/src/master/docker-compose.yaml) | Fichier utiliser pour (re)déployer les instances docker qui contiennent l'application node.js.
  * [/bitbucket-pipelines.yml](https://bitbucket.org/MonBuilding/monbuildingmeteor/src/master/bitbucket-pipelines.yml) | Utiliser pour configure bitbucket-pipeline ([voir doc](https://confluence.atlassian.com/bitbucket/configure-bitbucket-pipelines-yml-792298910.html))

Sur le serveur :

  * [/home/bb-pipeline/pipeline-event.sh](#) | Entrypoint exécuter par bitbucket-pipeline avec sont exécution qui va re-pull les sources et relancer l'instance docker.

#### Les commandes serveur pour la maintenance
> Logs prod

  * meteor : `docker logs monbuldingprod_monbulding-app_1`
  * mongodb : `docker logs monbuldingprod_monbulding-mongo_1`

> Logs dev

  * meteor : `docker logs monbuldingpdev_monbulding-app_1`
  * mongodb : `docker logs monbuldingdev_monbulding-mongo_1`

> Statut des instances

  * `docker ps`