/**
 * Created by kuyarawa on 5/13/17.
 */

$(function() { // document ready

    $("#minicalendar").datepicker({
        language: "fr",
        todayHighlight: true
    })
        .on("changeDate", function(e) {
            // `e` here contains the extra attributes
            $("#calendar").fullCalendar("gotoDate", e.date);
        });

    $("#calendar").fullCalendar({
        schedulerLicenseKey: "0752529861-fcs-1493371835",
        now: moment(),
        editable: true, // enable draggable events
        aspectRatio: 1.8,
        scrollTime: "00:00", // undo default 6am scrollTime
        header: {
            left: "today",
            center: "prev title next",
            right: "timelineDay,agendaWeek,month"
        },
        slotLabelFormat: "HH",
        resourceColumns: [],
        defaultView: "timelineDay",
        titleFormat: "dddd D MMMM YYYY",
        axisFormat: "HH:mm",
        displayEventEnd: true,
        eventOverlap: false, // will cause the event to take up entire resource height
        views: {
            timelineThreeDays: {
                type: "timeline",
                duration: { days: 3 }
            }
        },
        refetchResourcesOnNavigate: true,

        resources: {
            // change the url with real API url
            // example: http://the.api.com/resources
            // on fetching data, param will be attached [start = start date, end = end date, _ = timestamp]
            // example: http://http://the.api.com/resources?start=2017-05-13&end=2017-05-14&_=1494692152532
            url: "json/resources.json",
            error: function() {
                $("#script-warning").show();
            }
        },

        events: {
            // change the url with real API url
            // example: http://the.api.com/events
            // on fetching data, param will be attached [start = start date, end = end date, _ = timestamp]
            // example: http://http://the.api.com/events?start=2017-05-13&end=2017-05-14&_=1494692152532
            url: "json/events.json",
            error: function() {
                $("#script-warning").show();
            }
        },
        eventAfterAllRender: function(view){
            generateNavigator();
            showEventLine();

            $("#calendar .fc-timelineDay-view .fc-body .fc-scroller").on("scroll",function(){
                showTimeLineEvent();
            });
        }
    });

    function generateNavigator() {
        var calendar = $("#calendar");
        var resources = calendar.fullCalendar( "getResources" );

        loopResourcesAppend(resources);

        var $checkableTree = $("#navigator-container").treeview({
            data: loopResources(resources),
            showIcon: false,
            showCheckbox: true,
            expandIcon: "glyphicon glyphicon-chevron-right",
            collapseIcon: "glyphicon glyphicon-chevron-down",
            onNodeChecked: function(event, node) {
                toggleRows($("#calendar"), function (row, resourceId) {
                    // do some checking magic here
                    return (resourceId == node["data-id"]);
                }, true);
            },
            onNodeUnchecked: function (event, node) {
                toggleRows($("#calendar"), function (row, resourceId) {
                    // do some checking magic here
                    return (resourceId == node["data-id"]);
                }, false);
            }
        });
    }

    function loopResources(resources) {
        var nodes = [];
        $.each( resources, function( key, value ) {
            var node = {
                text : value.title,
                "data-id" : value.id,
                selectable: false,
                state : {
                    checked : true
                }
            };

            if (typeof value.eventColor != "undefined") {
                node.color = value.eventColor;
            }

            if (value.children.length > 0) {
                node.nodes = loopResources(value.children)
            }

            nodes.push(node);
        });

        return nodes;
    }

    function toggleRows(calendarElement, callback, show) {
        var view = calendarElement.data("fullCalendar").view;
        var resourceRows = view.resourceRowHash;

        view.batchRows();

        for(var resourceId in resourceRows) {
            var row = resourceRows[resourceId];

            // show hidden row
            if((callback(row, resourceId)) && (show)) {
                row.show();
            }

            // hide shown row
            if((callback(row, resourceId)) && (!show)) {
                row.hide();
            }

        }

        view.unbatchRows();
    }

    // Append resources header on timeline
    function loopResourcesAppend(resources) {
        $.each( resources, function( key, value ) {
            var created = $("tr[data-resource-id='"+value.id+"'] div.fc-event-container").prepend("<p class='event-header'>"+value.title+"</p>");

            if (typeof value.eventColor != 'undefined') {
                created.css('color', value.eventColor);
            }

            if (value.children.length > 0) {
                loopResourcesAppend(value.children)
            }
        });
    }

    function showEventLine() {
        var type = $('#calendar').fullCalendar('getCalendar').getView().type;

        if(type == "timelineDay"){

            var calendarDate = $('#calendar').fullCalendar('getDate');
            setTimeDayLineBar(calendarDate.format('YYYY-MM-DD'));

            showTimeLineEvent();
        }
    }

    function setTimeDayLineBar(showingdate) {

        var today = moment().format('YYYY-MM-DD');
        // var calendar = $('#calendar');
        // var view = calendar.data('fullCalendar').view;

        if (today == showingdate) {
            // view.renderNowIndicator(calendar.fullCalendar('getCalendar').moment());
            var d = new Date();
            var h = d.getHours();
            var m = d.getMinutes();
            var timeband = (h*60+m) / 60 * 60;

            $("#calendar .fc-body .fc-scroller-canvas").append("<div class='fc-daylinebar'></div>");
            $("#calendar .fc-daylinebar").css("transform", "translateX(" + timeband + "px)");
            $('#calendar > div.fc-view-container > div > table > thead > tr > td.fc-time-area.fc-widget-header > div > div > div > div.fc-content > table > tbody > tr > th.fc-widget-header').each(function(){
                var times = $(this).attr("data-date").split("T");
                var hour = times[1].split(":");
                if(parseInt(hour[0]) == moment().get('hour'))
                    $(this).addClass("now-hour");
            });
        } else {
            // view.unrenderNowIndicator();
            $(".fc-body .fc-scroller-canvas .fc-daylinebar").remove();
        }
    }

    function showTimeLineEvent() {
        var scrollLeft = $('#calendar > div.fc-view-container > div > table > tbody > tr > td.fc-time-area.fc-widget-content.fc-unselectable > div > div.fc-scroller').scrollLeft();
        $('#calendar p.event-header').css('left', scrollLeft);
    }
});