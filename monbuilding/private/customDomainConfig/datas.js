const domainConfig = {
  name: 'MonBuilding',
  restrictToLoginPage: false,
  iosAppId: '1274455987',
  SEOconfig: {
    title: 'MonBuilding - Votre immeuble, en mieux.',
    description: 'Pratique & sécurisée, MonBuilding est la solution pour rester informé et communiquer dans l’immeuble : actualités, incidents, messenger, conciergerie.',
    meta: {
      'name="author"': 'monBuilding & Co',
      'property="og:locale"': "fr_FR",
      'property="og:locale:alternate"': "en_US",
    }
  },
  getConfig: function () {
    return JSON.stringify(domainConfig.appConfig)
  },
  defaultColorPalette: {
    'primary': '#69d2e7',
    'secondary': '#fe0900',
    'third': '#fdb813'
  },
  corporateName: 'MonBuilding&Co',
  corporateAddress: '146 boulevard de grenelle, 75015 Paris',
  corporateLegalRepresentative: 'Eliane Lugassy',
  corporateDPO: 'Eliane Lugassy'
}
