const domainConfig = {
  name: 'Kley',
  restrictToLoginPage: true,
  iosAppId: '1426940625',
  SEOconfig: {
    title: 'Résidences Etudiantes - Kley',
    description: 'Retrouvez toutes les adresses des résidences étudiantes Kley à travers la France.',
    meta: {
      'name="author"': 'monBuilding & Co',
      'property="og:locale"': "fr_FR",
      'property="og:locale:alternate"': "en_US",
    }
  },
  getConfig: function () {
    return JSON.stringify(domainConfig.appConfig)
  },
  defaultColorPalette: {
    'primary': '#c2da1d',
    'secondary': '#fe6325',
    'third': '#00afaa'
  },
  corporateName: 'KLEY',
  corporateAddress: '67 Rue Anatole France, 92300 Levallois-Perret, France',
  corporateLegalRepresentative: 'Jean Baptiste Mortier',
  corporateDPO: 'Jean Baptiste Mortier'
}
