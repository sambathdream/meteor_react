const domainConfig = {
  name: 'Student Factory',
  restrictToLoginPage: true,
  iosAppId: '1402051426',
  SEOconfig: {
    title: 'Student Factory | Résidences étudiantes',
    description: 'Pratique & sécurisée, Student Factory est l\'application de ta résidence pour communiquer dans l’immeuble et bénéficier des services de la résidence.',
    meta: {
      'name="author"': 'monBuilding & Co',
      'property="og:locale"': "fr_FR",
      'property="og:locale:alternate"': "en_US",
    }
  },
  getConfig: function () {
    return JSON.stringify(domainConfig.appConfig)
  },
  defaultColorPalette: {
    'primary': '#074666',
    'secondary': '#fecc00',
    'third': '#df2910'
  },
  corporateName: 'Student Factory',
  corporateAddress: '59 Rue Yves Kermen, 92100 Boulogne-Billancourt, France',
  corporateLegalRepresentative: 'Eric Lapierre',
  corporateDPO: 'Eric Lapierre'
}
