const domainConfig = {
  name: 'Nexity-Studéa',
  restrictToLoginPage: true,
  iosAppId: '1426940625',
  SEOconfig: {
    title: 'Nexity-Studéa | Résidences étudiantes',
    description: 'Réseau social dédié aux locataires de chez Nexity Studéa. Idéal pour suivre toute l’actualité de la résidence, et rencontrer / échanger entre voisins.',
    meta: {
      'name="author"': 'monBuilding & Co',
      'property="og:locale"': "fr_FR",
      'property="og:locale:alternate"': "en_US",
    }
  },
  getConfig: function () {
    return JSON.stringify(domainConfig.appConfig)
  },
  defaultColorPalette: {
    'primary': '#CB333B',
    'secondary': '#71B2C9',
    'third': '#fdb813'
  },
  corporateName: 'Nexity-Studéa',
  corporateAddress: '19 Rue de Vienne, 75008 Paris, France',
  corporateLegalRepresentative: 'Pascal Pedoux',
  corporateDPO: 'Pascal Pedoux'
}
