import './app.less';
import './index.html';

import 'moment-locale-fr';

const notRefreshableRoute = [
  'app.api.payzen',
  'app.api.payzen.status.paymentId',
  'app.testPerf'
]

Meteor.startup(function() {
  const appId = domainConfig && domainConfig.iosAppId
  let meta = document.createElement('meta');
  meta.name = 'apple-itunes-app';
  meta.setAttribute('content', 'app-id=' + appId);
  document.getElementsByTagName('head')[0].appendChild(meta);

	window.onbeforeunload = () => {
		const current = FlowRouter.current()
		let time = new Date()
		let log = `on before unload --- Prepare for update analytics\nmodule: ${current.params.module_slug}\ndate when left: ${time.toLocaleString()}\n`
		console.log(log);
		Meteor.call('updateAnalytics', {type: "module", module: current.params.module_slug, accessType: "web", condoId: (FlowRouter.getParam("condo_id") || FlowRouter.getParam("condoId"))});
		// return "Do you really want to close?";
	};

	Session.set('notConnected', !navigator.onLine)
	Meteor.notConnected = !navigator.onLine

	let myInterval = Meteor.setInterval(function() {
		if (navigator.onLine == false) {
			if ($(".notConnected").css("display") === 'none') {
				Session.set('notConnected', true)
				Meteor.notConnected = true
			}
		}
		else {
      if ($(".notConnected").css("display") !== 'none' && !_.contains(notRefreshableRoute, FlowRouter.getRouteName())) {
				Session.set('notConnected', false)
				window.location.reload();
			}
		}
	}, 10000);

	Meteor._debug = (function (super_meteor_debug) {
		return function (error, info) {
			if (info && _.has(info, 'msg'))
				;
				// super_meteor_debug("Streamy message is allowed!", info)
			else
				super_meteor_debug(error, info);
		}
	})(Meteor._debug);

  // SET SUBDOMAIN GLOBAL ATTR
  SEO.set(domainConfig.SEOconfig)
  Session.set('defaultColorPalette', domainConfig.defaultColorPalette)

  // GLOBAL LANG CONFIG FOR DATEPICKER
  $.fn.datepicker.dates['fr'] = {
    days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
    daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
    daysMin: ["D", "L", "M", "M", "J", "V", "S"],
    daysOfWeek: ["D", "L", "M", "M", "J", "V", "S"],
    months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
    monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
    monthNames: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
    today: "Aujourd'hui",
    monthsTitle: "Mois",
    clear: "Effacer",
    weekStart: 1,
    firstDay: 1,
    format: "DD/MM/YYYY"
  };
  $.fn.datepicker.dates['en'] = {
    days: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    daysMin: ["S", "M", "T", "W", "T", "F", "S"],
    daysOfWeek: ["S", "M", "T", "W", "T", "F", "S"],
    months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    today: "Today",
    clear: "Clear",
    format: "DD/MM/YYYY",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 1,
    firstDay: 1,
    monthsTitle: "Month",
  };
});
