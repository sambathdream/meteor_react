import { FileManager } from "./../fileManager/filemanager.js";

Handlebars.registerHelper("userStatus", UserStatus);
Handlebars.registerHelper("localeTime", function(date) {
	return date !== null ? date.toLocaleString() : void 0;
});

/* Appel d'une bulle de status

{{> userIcon userId="" type=""}}
userId    : userId pour un utilisateur.
type      : "U"  -> utilisateur,
			"Ge" -> Gestionnaire,
			"G"  -> Gardien,
			"CS" -> Conseil Syndical.
*/

Template.userIcon.onCreated(function() {
	this.readyState = new ReactiveVar(true);
	this.profilePicture = new ReactiveVar('');
});

Template.userIcon.events({
	'click .user' (event, tempate) {
		event.preventDefault();
	},
	'click .userIcon' (event, tempate) {
		event.preventDefault();
	},
	'click .userIcon_inactive' (event, tempate) {
		event.preventDefault();
	},
	'click .userIcon_active' (event, tempate) {
		event.preventDefault();
	},
	'click .userIcon_phone' (event, tempate) {
		event.preventDefault();
	}
});

/*For getInitiales and getStatus */
/*user can be :*/
/*		An array : [{_id:'', ...}, {_id:'', ...}, {_id:'', ...} ...]*/
/*		A string : 'id......'*/
/*		an objet : {_id:'', ...}*/

Template.userIcon.helpers({
	getPP: () => {
		return Template.instance().profilePicture.get();
	},
	avatarLink : (userId) => {
		if (_.isObject(userId))
			userId = userId._id;
		else if (_.isArray(userId))
			return "";
		let avatar = Avatars.findOne({userId: userId});
		if (avatar && avatar != undefined) {
			Template.instance().profilePicture.set(avatar.avatar.original);
			return avatar.avatar.original;
		}
		return "";
	},
	labelClass: function (user) {
		let status = "userIcon-inactive";

		if (Template.instance().readyState.get()) {
		  if (!!Template.currentData().status) {
        if (Template.currentData().status === 'idle')
          status = "userIcon-away";
        else if (Template.currentData().status === 'online')
          status = "userIcon-active";
      }
			if (_.isArray(user)) {
				let usrs = Meteor.users.find({ "status.online": true, _id: { $in: _.pluck(user, 'userId'), $ne : Meteor.userId() }}).fetch();
				for (let i = 0; i < usrs.length; i++) {
					if (usrs[i].status.idle)
						status = "userIcon-away";
					else if (usrs[i].status.online) {
						status = "userIcon-active";
						break;
					}
				}
			} else {
				let usr;
				if (_.isObject(user)) {
					usr = Meteor.users.findOne({ "status.online": true, _id: user._id});
				} else {
					usr = Meteor.users.findOne({ "status.online": true, _id: user});
				}

				if (usr !== undefined) {
					if (usr.status.idle)
						status = "userIcon-away";
					else if (usr.status.online)
						status = "userIcon-active";
				}
			}
		}

		return status;
	},
	getInitiales: function(user) {

		let userChecked;  /*For check if user exist*/
		let otherUser;    /*For an other user in conversation*/
		let gUser;        /*For check if a gestionnaire user*/
		let mostRecentId; /*In group, it's user than speek for last*/

		/*Gestionnaire : */
		if (Meteor.user() && Meteor.user().identities && Meteor.user().identities.gestionnaireId) {
			/*If user._id exist*/
			if (user && user._id) {
				/*Check if it's occupant user */
				if (userChecked = GestionnaireUsers.findOne(user._id))
					return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
				else {
					/*Check if it's gestionnaire user */
					if (userChecked = Enterprises.findOne({"users.userId": user._id}))
						if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === user._id}))
							return gUser.firstname[0]+gUser.lastname[0];
					/*Error user not found*/
					return '?';
				}
			}
			/*If user is an array*/
			else if (user && typeof user == 'object') {
				if (user.length > 2) {
					let tmp = 0;
					/*Search the most recent user in conversation */
					for (var i = 0; i < user.length; i++) {
						if (user[i].userId != Meteor.userId() && user[i].lastVisit && user[i].lastVisit > tmp){
							tmp = user[i].lastVisit;
							mostRecentId = user[i].userId;
						}
					}
					if (!mostRecentId) mostRecentId = (user[0].userId == Meteor.userId() ? user[1].userId : user[0].userId);
					if (userChecked = GestionnaireUsers.findOne(mostRecentId))
						return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
					else {
						if (otherUser && (userChecked = Enterprises.findOne({"users.userId": mostRecentId})))
							if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === mostRecentId}))
								return gUser.firstname[0]+gUser.lastname[0];
							return '?';
						}
					}
					else {
						otherUser = _.find(user, (u) => {return u.userId !== Meteor.userId()});
						if (otherUser && (userChecked = GestionnaireUsers.findOne(otherUser.userId))) {
							return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
						}
						else {
							if (otherUser && (userChecked = Enterprises.findOne({"users.userId": otherUser.userId})))
								if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === otherUser.userId}))
									return gUser.firstname[0]+gUser.lastname[0];
								return '?';
							}
						}
					}
			/*If user is user._id*/
			else {
				if (userChecked = GestionnaireUsers.findOne(user))
					return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
				else {
					if (userChecked = Enterprises.findOne({"users.userId": user}))
						if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === user}))
							return gUser.firstname[0]+gUser.lastname[0];
						return '?';
					}
				}
			}
		/*Occupant : */
		else {
			/*If user._id exist*/
			if (user && user._id) {
				/*Check if it's occupant user */
				if (userChecked = ResidentUsers.findOne(user._id))
					return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
				else {
					/*Check if it's gestionnaire user */
					if (userChecked = Enterprises.findOne({"users.userId": user._id}))
						if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === user._id}))
							return gUser.firstname[0]+gUser.lastname[0];
					return '?';
				}
			}
		/*If user is an array (messenger)*/
		else if (user && typeof user == 'object') {
			if (user.length > 2) {
				let tmp = 0;
				for (var i = 0; i < user.length; i++) {
					if (user[i].userId != Meteor.userId() && user[i].lastVisit && user[i].lastVisit > tmp){
						tmp = user[i].lastVisit;
						mostRecentId = user[i].userId;
					}
				}
				if (!mostRecentId)
					mostRecentId = (user[0].userId == Meteor.userId() ? user[1].userId : user[0].userId);
				if (userChecked = ResidentUsers.findOne(mostRecentId))
					return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
				else {
					if (userChecked = Enterprises.findOne({"users.userId": mostRecentId}))
						if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === mostRecentId}))
							return gUser.firstname[0]+gUser.lastname[0];
					return '?';
				}
			}
			else {
				otherUser = _.find(user, (u) => {return u.userId !== Meteor.userId()});
				if (otherUser && (userChecked = ResidentUsers.findOne(otherUser.userId))) {
					return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
				}
				else {
					if (otherUser && (userChecked = Enterprises.findOne({"users.userId": otherUser.userId})))
						if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === otherUser.userId}))
							return gUser.firstname[0]+gUser.lastname[0];
						return '?';
					}
				}
			}
		/*If user is user._id directly*/
		else {
			if (userChecked = ResidentUsers.findOne(user))
				return userChecked.profile.firstname[0] + userChecked.profile.lastname[0];
			else {
				if (userChecked = Enterprises.findOne({"users.userId": user}))
					if (gUser = _.find(userChecked.users, (gUser) => {return gUser.userId === user}))
						return gUser.firstname[0]+gUser.lastname[0];
					return '?';
				}
			}
		}
	},
    hideStatus: () => {
		return !!Template.instance().data.hideStatus;
	}
});

