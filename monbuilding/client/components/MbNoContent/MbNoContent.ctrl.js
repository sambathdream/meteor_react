Template.MbNoContent.onCreated(function() {

})

Template.MbNoContent.helpers({
  getTitle: () => {
    return Template.instance().data.title;
  },
  getDescription: () => {
    return Template.instance().data.description;
  }
})
