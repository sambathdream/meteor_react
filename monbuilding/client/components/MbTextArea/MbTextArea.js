/* globals _ */
/* globals getRandomString */

import React from 'react'
import AutoResizeTextArea from 'react-autosize-textarea';

import './MbTextArea.less'

/**
 * @param label
 * @param minRow
 * @param value
 * @param onChange
 * @param onKeyUp
 * @param placeholder
 * @param showLabel
 * @returns {XML}
 * @constructor
 */
class MbTextArea extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      isFocused: false,
      value: props.value
    }

    this.__onChange = this.__onChange.bind(this)
    this.__onKeyUp = this.__onKeyUp.bind(this)
    this.__onFocus = this.__onFocus.bind(this)
    this.__onBlur = this.__onBlur.bind(this)
    this.__onKeyPress = this.__onKeyPress.bind(this)
    this.__onKeyDown = this.__onKeyDown.bind(this)
  }

  __onChange (e) {
    const { value } = e.target
    this.setState({value}, () => {
      this.props.onChange(value)
    })
  }

  __onKeyDown (e) {
    this.props.onKeyDown(e)
  }

  __onKeyPress (e) {
    this.props.onKeyPress(e)
  }

  __onKeyUp (e) {
    this.props.onKeyUp(e)
  }

  __onFocus (e) {
    this.setState({isFocused: true})
    this.props.onFocus(e)
  }

  __onBlur (e) {
    this.setState({isFocused: false})
    this.props.onBlur(e)
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.isFocused || nextProps.value === '' || !nextProps.value) {
      this.setState({value: nextProps.value});
    }
  }

  render () {
    const { showLabel, label, minRow, value, placeholder, required } = this.props

    return (
      <div className="mb__textarea">
        { showLabel &&
        <label htmlFor={`mb-textarea-${getRandomString()}`}>
          { label }
          {
            required &&
            <span className='image-icon mandatory-icon' />
          }
        </label>
        }

        <AutoResizeTextArea
          id={`mb-textarea-${getRandomString()}`}
          rows={minRow}
          value={this.state.value || ''}
          onChange={this.__onChange}
          onKeyUp={this.__onKeyUp}
          onBlur={this.__onBlur}
          onFocus={this.__onFocus}
          onKeyPress={this.__onKeyPress}
          placeholder={placeholder}
          onKeyDown={this.__onKeyDown}
          maxLength={this.props.maxlength}
          // className="form-control borderless-textarea" that's break display on ads
        />
      </div>
    )
  }
}

MbTextArea.defaultProps = {
  label: '',
  value: '',
  onChange: () => _.noop,
  onKeyUp: () => _.noop,
  onFocus: () => _.noop,
  onBlur: () => _.noop,
  onKeyPress: () => _.noop,
  onKeyDown: () => _.noop,
  placeholder: '',
  minRow: 3,
  showLabel: true
}

export default MbTextArea
