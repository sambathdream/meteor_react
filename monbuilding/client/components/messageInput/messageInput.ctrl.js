import { FileManager } from "./../fileManager/filemanager.js";
import { MessageInput } from "/common/lang/lang";
import autosize from 'autosize';

/* APPEL DU MESSAGE INPUT*/
/*    {{> messageInput fileManager= sendMessage= isTyping= name= }}*/
/**/
/*    (optionnel) fileManager: un objet de type FileManager*/
/*    sendMessage: une fonction callback prenant en parametre (message, files, sendNotif)*/
/*    (optionnel) isTyping: une fonction callback appellee quand tappee*/
/*    name: pour différencier deux messageInput sur une meme page*/
/*    sendNotif: checkbox supplémentaire, renvoyé à la callback sendMessage*/


var index = 0;
var timer = {};

function constructAttachmentElement(data)  {
	var progress = $('<span></span>')
	.addClass('progress')
	.css('width', '0%');

	var meter = $('<div></div>')
	.addClass('meter')
	.append(progress);

	var iconFile = $('<span></span>')
	.addClass('icon-file')
	.append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'));

	var checkmark = $('<span class="checkmark"></span>')
	.append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'));

	var fileSize = $('<span class="file-size"></span>')
	.html(data.size)
	.append(checkmark);

	var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name);

	var fileRemove = $(`<span class="remove-item" index="${index}">`)
	.append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'));
	index += 1;

	var file = $('<div></div>')
	.addClass('file')
	.append(iconFile)
	.append(fileName)
	.append(fileSize)
	.append(fileRemove)
	.append(meter);

	var list = $('<li></li>')
	.attr('id', data.id)
	.addClass('attachment-item')
	.append(file);

	return list;
}

Template.messageInput.onCreated(function() {
	this.errorMessage = new ReactiveVar("");
	this.fm = this.data.fileManager ? this.data.fileManager : new FileManager(UserFiles);
	this.sendNotif = new ReactiveVar(true);
	this.isDisabled = new ReactiveVar(true);
});

Template.messageInput.onRendered(function() {
	autosize($('#answer'));
	let template = Template.instance();
	$('#answer').on('paste', function (ev) {
		var clipboardData = ev.originalEvent.clipboardData;

		var attachmentListUL = $('#attachments');
		let translation = new MessageInput((FlowRouter.getParam("lang") || "fr"));
		$.each(clipboardData.items, function (i, item) {
			let name = "";
			if (item.type.indexOf("text") !== -1) {
				item.getAsString(function(elem) { Session.set("name", elem.split('\r')) });
			}
			if (item.type.indexOf("image") !== -1) {
				ev.preventDefault();
				var reader = new FileReader();
				let test;

				let file = item.getAsFile();
				if (file) {
					$('#sendAnswer').attr('disabled', 'disabled');
					var data = {};
					let error = false;
					let id = template.fm.insert(file, (err, fileObj) => {
						if (err) {
							error = true;
							$('#sendAnswer').removeAttr('disabled');
							return sAlert.error(translation.messageInput[err.reason]);
						}
					});

					data.name = file.name;
					data.size = (file.size / 1000).toFixed(2) + ' kb';
					data.id = "file-" + id;

					attachmentListUL.append(constructAttachmentElement(data));

					var progress = template.$('#file-' + id +' .progress');
					var elapsed = 0;

					timer[id] = setInterval(function() {
						if (error)
							return template.$("#file-" + id).remove();
						var wrapper = template.$('#file-' + id);
						if (template.fm.getFileList()[id].done) {
							let fileDone = template.fm.getFileList()[id];
							let name = Session.get("name");
							if (name && name != undefined && name != "") {
								name = _.last(name) + fileDone.file.extensionWithDot;
								// name = name.last() + fileDone.file.extensionWithDot;  /* When the multiple file past will work.... */
								Meteor.call('updateFileName', fileDone.file._id, name);
								fileDone.file.name = name;
								$('#file-' + id).children(".file").children('.file-name').text(name);
							}
							wrapper.find('.meter').remove();
							wrapper.addClass('completed');
							$('#sendAnswer').removeAttr('disabled');
							clearInterval(timer[id]);
						}
						if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
							elapsed = template.fm.getFileList()[id].upload.progress.get();
							progress.animate({
								width: elapsed + '%'
							}, 200);
						}
					}, 100);
				}
			}
		});
	});
});

Template.messageInput.events({
	'focus #answer': function(event, template) {
		template.errorMessage.set('');
		$('#chat-window').prop('style', 'margin-top: 0 !important;');
	},
	'click #attachment' : function(event, template) {
		var fileInput = $('#fileUploadInput');
		fileInput.click();
	},
	'change #fileUploadInput': function (e, t) {
		var attachmentListUL = $('#attachments');
		let translation = new MessageInput((FlowRouter.getParam("lang") || "fr"));
		$('#sendAnswer').attr('disabled', 'disabled');
		if (e.currentTarget.files) {
			for (let i = 0; i < e.currentTarget.files.length; i++) {
				var data = {};
				let error = false;
				let id = t.fm.insert(e.currentTarget.files[i], (err, fileObj) => {
					if (err) {
						error = true;
						$('#sendAnswer').removeAttr('disabled');
						console.log('err', err)
						return sAlert.error(translation.messageInput[err.reason]);
					}
				});

				data.name = e.currentTarget.files[i].name;
				data.size = (e.currentTarget.files[i].size / 1000).toFixed(2) + ' kb';
				data.id = "file-" + id;

				attachmentListUL.append(constructAttachmentElement(data));

				var progress = t.$('#file-' + id +' .progress');
				var elapsed = 0;

				timer[id] = setInterval(function() {
					if (error)
						return t.$("#file-" + id).remove();
					var wrapper = t.$('#file-' + id);
					if (!t || !t.fm || !t.fm.getFileList() || !t.fm.getFileList()[id]|| t.fm.getFileList()[id] == undefined || t.fm.getFileList()[id].done) {
						wrapper.find('.meter').remove();
						wrapper.addClass('completed');
						$('#sendAnswer').removeAttr('disabled');
						clearInterval(timer[id]);
					}
					if (t && t.fm && t.fm.getFileList() && t.fm.getFileList()[id] && elapsed < t.fm.getFileList()[id].upload.progress.get()) {
						elapsed = t.fm.getFileList()[id].upload.progress.get();
						progress.animate({
							width: elapsed + '%'
						}, 200);
					}
				}, 100);

			}
		}
		else
			$('#sendAnswer').removeAttr('disabled');
	},
	'click .remove-item' : function (e, t) {
		for (let i = 0; i !== t.fm.getFileList().length; ++i) {
			if (t.$(e.currentTarget.parentElement)[0].innerText.match(t.fm.getFileList()[i].file.name)) {
				t.$(e.currentTarget.parentElement.parentElement).remove();
				t.fm.remove(i);
				$("#fileUploadInput").val('');
			}
		}
	},
	'click #sendAnswer' : function(e, t) {
		$('#sendAnswer').button('loading');
		$('#answer').height('46px');
		let translation = new MessageInput((FlowRouter.getParam("lang") || "fr"));
		e.preventDefault();
		let files = Template.instance().fm.getFileList();
		for (let i = 0; i < files.length; i++){
			if (files[i].done === false)
				return (t.errorMessage.set(translation.messageInput["waiting"]));
		}
		let text = document.getElementsByName(t.data.name)[0].value;
		text = text.trim();
		if (text.length < 1 && t.fm.getFileListIds().length === 0) {
			$('#sendAnswer').button('reset');
			$('#chat-window').prop('style', 'margin-top: 0 !important;');
			return t.errorMessage.set(translation.messageInput["type_message"]);
		}
		if (t.data.board && t.data.board === "Gestionnaire")
			t.data.sendMessage(text, t.fm.getFileListIds(), t.sendNotif.get());
		else
			t.data.sendMessage(text, t.fm.getFileListIds(), null, t.fm);
		t.errorMessage.set('');
		index = 0;
		if (t.data.name !== "deskTopGestionnaireArea" && t.data.name !== "newConciergeMessage" && t.data.name !== "contactGestionnaireFromIncident") {
			document.getElementsByName(t.data.name)[0].value = "";
			$('#attachments')[0].innerHTML = "";
			t.fm.clearFiles();
		}

		setTimeout(function() {
			recalculateHeigh();
		}, 500);
	},
	'input #answer': function(e, t) {
		// console.log('e.currentTarget.value', e.currentTarget.value);
		// t.isDisabled.set(document.getElementsByName(t.data.name)[0].value === '');
		if (e.currentTarget.value.trim() !== '') {
			$('#sendAnswer').prop('disabled', false)
		} else {
			$('#sendAnswer').prop('disabled', true)
		}
		if (t.data.isTyping && $('#answer').val().length > 0) {
			t.data.isTyping();
		}
	},
	'keydown #answer': function(e, t) {
		if (e.keyCode === 13 && e.shiftKey && !e.altKey) {
			e.preventDefault();
			$(e.currentTarget).css('height', '30px');
			$("#sendAnswer").click();
		}
	},
	'click #sendNotif, click #sendLabel': function(e, t) {
		t.sendNotif.set(!(t.sendNotif.get()));
	},
    'mouseenter #msgRappelInfo': function(e, t){
        $('.rappelInfo').addClass("visible")
    },
    'mouseleave #msgRappelInfo': function(e, t){
        $('.rappelInfo').removeClass("visible")
    },
    'mouseenter #info-bubble-reminder': function(e, t){
        $('#info-content-reminder').css("display", "block")
        $('#info-content-reminder').addClass("visible")
    },
    'mouseleave #info-bubble-reminder': function(e, t){
        $('#info-content-reminder').css("display", "none")
        $('#info-content-reminder').removeClass("visible")
    },
	'click #chat-window': function (e, t) {
		$(e.currentTarget).find('#answer').focus();
	},
});

function recalculateHeigh() {
	let height = window.innerHeight;
	let dynamicHeight = 0;
	$(".d-top-head").each(function( index ) {
		dynamicHeight += $( this ).height();
	});
	$(".chatbox-container").animate({height:(height - 185 - 167 - 25)},0);
	$(".chatbox-container-2").animate({height:(height - 200 - 167 - 8 - dynamicHeight)},0);
	$(".xx-sidebar").animate({height:(height - 183)},0);
}

Template.messageInput.helpers({
	isDisabled: () => {
		return false
		// return Template.instance().isDisabled.get() && Template.instance().fm.getFileList().length === 0;
	},
	getFileList: () => {
		return Template.instance().fm.getFileList();
	},
	errorMessage: () => {
		return Template.instance().errorMessage.get();
	},
	getIconOfFile: (type) => {
		let arrType = type.split("/");
		if (arrType[0] == "image") {
			if (["png", "PNG"].indexOf(arrType[1]) != -1)
				return "png.jpg";
			if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
				return "jpeg.jpg";
			if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
				return "bmp.jpg";
			if (["gif", "GIF"].indexOf(arrType[1]) != -1)
				return "gif.jpg";
			return "image.jpg";
		}
		if (arrType[0] == "application") {
			if (arrType[1] == "pdf")
				return "pdf.jpg";
			let appType = arrType[1].split(".");
			if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
				if (appType[2] == "presentation" || appType[2] == "presentationml")
					return "ppt.jpg"
				if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
					return "xlsx.jpg"
				if (appType[2] == "wordprocessingml" || appType[2] == "document")
					return "docx.jpg"
			}
		}
		if (arrType[0] == "text")
			return "txt.jpg"
		return "file.png"
	},
	reminderTime: (condoId, priorityId) => {
		return getReminderValue(condoId, priorityId)
	},
	sendNotif : () => {
		return Template.instance().sendNotif.get();
	},
    reminderCheckArgs: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new MessageInput(lang);
        const instance = Template.instance();
        return {
            checked : instance.sendNotif.get(),
            label: translate.messageInput['reminder'],
            onClick: () => {
                instance.sendNotif.set(!instance.sendNotif.get())
            }
        };
    },
});
