Template.MbRestrictedAccess.onCreated(function() {
  this.condoId = FlowRouter.current().params.condo_id;
});

Template.MbRestrictedAccess.helpers({
	onButtonClick: () => {
    const lang = FlowRouter.getParam("lang") || "fr"
    const condoId = Template.instance().condoId
		return () => {
      FlowRouter.go(`/${lang}/resident/${condoId}/messenger/manager/new`)
		}
  },
  canContactManager: () => {
    const userRight = Meteor.userHasRight('messenger', 'writeToManager', Template.instance().condoId)
    const thisCondo = Condos.findOne({ _id: Template.instance().condoId }, { fields: { settings: true } })
    if (thisCondo && thisCondo.settings && thisCondo.settings.options) {
      return (thisCondo.settings.options.messengerGestionnaire || false) && userRight
    } else {
      return false
    }
  }
});
