/**
 * Created by kuyarawa on 18/10/18.
 */

import MbCondoDropdown from '/client/components/MbCondoDropdown/MbCondoDropdown.js'
import MbMultiDropDown from '/client/components/MbMultiDropdown/MbMultiDropDown.js'

Template.MbMultipleCondoPicker.helpers({
  getNoLabel: () => {
    return Template.instance().data.noLabel ? Template.instance().data.noLabel : false
  },
  getLabel: () => {
    return Template.instance().data.label ? Template.instance().data.label : '';
  },
  isRequired: () => {
    return Template.instance().data.required
  },
  MbCondoDropdown: () => MbCondoDropdown,
  MbMultiDropDown : () =>MbMultiDropDown,
  condoIdCb: () => {
    const template = Template.instance()
    return (condoId) => {
      if (typeof template.data.onSelect === 'function') {
        template.data.onSelect(condoId);
      }
    }
  },
  defaultSelect : () =>{
    if(Template.instance().data.defaultSelect && Template.instance().data.label){
      return Template.instance().data.label
    }
    else{
      return false
    }
  },
  getOptions: () => {
    return Template.instance().data.options || []
  },
  getSelectedUserIds : () =>{
    if(Template.instance().data.selectedUserIds) {
      return Template.instance().data.selectedUserIds || []
    }else{
      return [];
    }
  },
  userIds : () => {
    const template = Template.instance()
    return (userId) => {
      if (typeof template.data.onSelect === 'function') {
        template.data.onSelect(userId);
      }
    }
  },
  isDisabled : () =>{
    return Template.instance().data.disabled || false
  },
  isMultiple : () =>{
    return Template.instance().data.multiple
  },
  isUser: () =>{
    if(Template.instance().data.type && Template.instance().data.type === 'user'){
      return true
    }
    else {
      return false;
    }
  },
  getProps: (props) => {
    return Template.instance().data[props]
  }
})
