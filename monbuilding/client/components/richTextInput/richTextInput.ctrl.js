import { FileManager } from "./../fileManager/filemanager.js";

/* APPEL DU RICHTEXT INPUT
 {{> richTextInput fileManager= sendMessage= withFiles= buttonText= html= }}

 (optionnel) fileManager: un objet de type FileManager
 sendMessage: une fonction callback prenant en parametre (message, files)
 withFiles: boolean pour display le button PJ
 buttonText: text du bouton envoyer
 html: data pour preremplir
 */

Template.richTextInput.onCreated(function() {
    this.fm = this.data.fileManager ? this.data.fileManager : new FileManager(UserFiles);
});

Template.richTextInput.onRendered(function() {
    const toolbarOptions = [
        ['bold', 'italic', 'underline'],
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [
            'align',
            {
                'align': 'center'
            },
            {
                'align': 'right'
            },
            {
                'align': 'justify'
            }
        ]
    ];

    this.editor = new Quill(this.find('.editor'), {
        theme: 'snow',
        modules: {
            toolbar: toolbarOptions
        },
    });

    this.editor.clipboard.dangerouslyPasteHTML(this.data.html || '');
});

Template.richTextInput.helpers({
    getFileList: () => {
        return Template.instance().fm.getFileList();
    },
    errorMessage: () => {
        return Template.instance().fm.getErrorMessage();
    },
    getIconOfFile: (type) => {
        let arrType = type.split("/");
        if (arrType[0] == "image") {
            if (["png", "PNG"].indexOf(arrType[1]) != -1)
                return "png.jpg";
            if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
                return "jpeg.jpg";
            if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
                return "bmp.jpg";
            if (["gif", "GIF"].indexOf(arrType[1]) != -1)
                return "gif.jpg";
            return "image.jpg";
        }
        if (arrType[0] == "application") {
            if (arrType[1] == "pdf")
                return "pdf.jpg";
            let appType = arrType[1].split(".");
            if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
                if (appType[2] == "presentation" || appType[2] == "presentationml")
                    return "ppt.jpg"
                if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
                    return "xlsx.jpg"
                if (appType[2] == "wordprocessingml" || appType[2] == "document")
                    return "docx.jpg"
            }
        }
        if (arrType[0] == "text")
            return "txt.jpg"
        return "file.png"
    },
    cancelRequested: () => {
      return Template.instance().data.cancelText ? true : false
    }
});

Template.richTextInput.events({
    'change #fileUploadInput': function (e, t) {
        if (e.currentTarget.files) {
            for (let i = 0; i < e.currentTarget.files.length; i++)
                t.fm.insert(e.currentTarget.files[i]);
        }
    },
    'click .removeFile' : function (e, t) {
        let idx = parseInt(e.currentTarget.getAttribute("index"));
        t.fm.remove(idx);
    },
    'click #sendAnswer' : function(e, t) {
      t.data.sendMessage(t.editor.root.innerHTML, t.fm.getFileListIds());
      t.fm.clearFiles();
    },
    'click #cancelAnswer' : (e, t) => {
      t.data.onCancel()
    }
});