import { CommonTranslation } from "/common/lang/lang";

Template.MbPanel.helpers({
  getContent: () => {
    return Template.instance().data.content ? Template.instance().data.content : '';
  },
  getSelectOne: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    return Template.instance().data.selectOne ? `<span class="red-panel">(${translate.commonTranslation.select_at_least_one})</span>` : '';
  },
  getMandatory: () => {
    return Template.instance().data.required ? `<span class="red-panel">*</span>` : '';
  },
});
