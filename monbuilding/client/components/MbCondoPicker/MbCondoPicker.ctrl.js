import { CommonTranslation } from '/common/lang/lang.js';

Template.MbCondoPicker.onCreated(function () {
  this.selected = new ReactiveVar(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || null)
});

Template.MbCondoPicker.onRendered(function() {
  let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
  $('.modal').on('shown.bs.modal', () => {
    const selectedCondo = Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || null

    $('#mb-condo-picker select').val(selectedCondo);

    $('#mb-condo-picker select').select2({
      placeholder: translation.commonTranslation['select_building'],
    });
  })
})

// Template.MbCondoPicker.events({
//   'shown.bs.modal #newIncident_modal': (t) => {
//     alert(1);
//   }
// })

Template.MbCondoPicker.helpers({
  getCondoOptions: () => {
    const condos = Condos.find({_id: {$in: Template.instance().data.condoIds}}).fetch();

    return _.map(condos, (condo) => {
      return {
        id: condo._id,
        text: condo.name
      }
    });
  },
  getSelectedCondo: () => {
    return Template.instance().selected.get();
  },
  onSelectCondo: () => {
    const selected = Template.instance().selected;
    const onSelect = Template.instance().data.onSelect;

    return (key) => {
      selected.set(key);
      if (onSelect && typeof onSelect === 'function') {
        onSelect(key);
      }
    }
  }
})