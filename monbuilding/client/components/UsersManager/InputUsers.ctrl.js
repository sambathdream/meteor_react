Template.InputUsers.onCreated(function () {
  if (this.data.except != undefined)
    this.except = this.data.except;
  else
    this.except = undefined;
});

Template.InputUsers.helpers({
    users: () => {
        var ret;
        let usersManager = Template.instance().data.UsersManager;
        let condoId = Template.instance().data.incident.condoId;
        if (usersManager) {
          if (Template.instance().except != undefined){
            return _.filter(usersManager.getCollection().find({_id: {$nin: Template.instance().except}}).fetch(), function(resident){
              ret = false;
              _.each( resident.resident[0].condos, function( condo, key ) {
                  _.each( condo,  function( value, key ) {
                    if (key == 'condoId' && condoId == value)
                      ret = true;
                  });
              });
              return ret;
            });
          }
          return usersManager.getCollection().find().fetch();
        }
    }
});

Template.InputUsers.onRendered(function() {
    $(".select-target").select2();
});

Template.InputUsers.events({
    'change .select-target': function(e, t) {
        t.data.UsersManager.setUsers(t.$('[name=utilisateurs]').val());
    }
});
