export class UsersManager {
    constructor(collection) {
        this.collection = collection; // Collection des utilisateurs
        this.selectedList = new ReactiveArray([]); // Liste des usersID sélectionné
    }

    // Ajouter un utilisateur à la liste
    addUser(userId) {
        if (this.selectedList.list().indexOf(userId) !== -1)
            return ;
        this.selectedList.push(userId);
    }

    setUsers(users) {
        this.selectedList.clear();
        if (users) {
            for (let i = 0; i < users.length; i++) {
                this.selectedList.push(users[i]);
            }
        }
    }

    // Supp utilisateur de la Liste
    delUser(userId) {
        this.selectedList.remove(userId);
    }

    // Get la collection
    getCollection() {
        return this.collection;
    }

    // Get la liste de users
    getUsersList() {
        return this.selectedList.array();
    }
}
