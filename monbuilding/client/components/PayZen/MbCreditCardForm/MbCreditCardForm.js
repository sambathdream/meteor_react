/* globals Condos */

import React from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import { FlowRouter } from 'meteor/kadira:flow-router'
import Blaze from 'meteor/gadicc:blaze-react-component'
import moment from 'moment'

import valid from 'card-validator'

import './MbCreditCardForm.less'

/**
 * @param saveForm
 * @constructor
 */

function updateSubmit (self) {
  let canSubmit = false
  const formData = self.state.formData
  const errorData = self.state.errorData
  canSubmit =
    self.props.parentSubmit &&
    (formData.name !== '' && errorData.name === false) &&
    (formData.number !== '' && errorData.number === false) &&
    (formData.cvc !== '' && errorData.cvc === false) &&
    (formData.expiration !== '' && errorData.expiration === false) &&
    (formData.type !== '' && errorData.type === false)/*  &&
    (formData.sameBillingAddress === true || (formData.sameBillingAddress === false && (formData.street !== '' && formData.code_city !== '' && formData.country !== ''))) */
  return canSubmit
    // self.setState({ canSubmit: canSubmit })
}

class MbCreditCardForm extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isOpen: false,
      canSubmit: false,
      number: null,
      codeName: 'CVC',
      codeLength: 3,
      errorData: {
        name: false,
        number: false,
        cvc: false,
        expiration: false,
        type: false
      },
      formData: {
        name: '',
        number: '',
        cvc: '',
        expiration: '',
        type: '',
        // sameBillingAddress: true,
        // street: '',
        // code_city: '',
        // country: '',
        saveCard: false
      }
    }

    this.__saveForm = this.__saveForm.bind(this)
    this.getForm = this.getForm.bind(this)
    this.getIsError = this.getIsError.bind(this)
    this.onClickCheckbox = this.onClickCheckbox.bind(this)
    this.getUserName = this.getUserName.bind(this)
    this.getBuildingName = this.getBuildingName.bind(this)
    this.getBuildingAddress = this.getBuildingAddress.bind(this)
    this.getBuildingAddress2 = this.getBuildingAddress2.bind(this)
  }

  getForm (key) {
    return this.state.formData[key]
  }

  getIsError (key) {
    return this.state.errorData[key] || false
  }

  onInputDetails (key, value) {
    const formData = { ...this.state.formData }
    let codeName = this.state.codeName
    let codeLength = this.state.codeLength
    const errorData = { ...this.state.errorData }

    errorData[key] = false
    this.setState({ errorData })
    if (key === 'number') {
      const withoutSpace = value.replace(/ /gi, '')
      const validNumber = valid.number(withoutSpace)
      formData.type = ''
      if (validNumber.card && validNumber.card.type) {
        codeName = validNumber.card.code.name
        codeLength = validNumber.card.code.size
        formData.type = validNumber.card.type === 'american-express' ? 'amex' : validNumber.card.type

        let formatedNumber = []
        let previousGap = 0

        if (value.length >= formData.number.length) {
          validNumber.card.gaps.forEach(gap => {
            formatedNumber.push(withoutSpace.slice(previousGap, gap))
            previousGap = gap
          })
          formatedNumber.push(withoutSpace.slice(previousGap))
          let spacedNumber = formatedNumber.join(' ').trim()
          formData.number = spacedNumber
        } else {
          formData.number = value
        }

        const validCVV = valid.cvv(formData.cvc, codeLength)
        errorData.cvc = !validCVV.isValid

        errorData.number = !validNumber.isValid
      } else {
        formData.number = value
        codeName = 'CVC'
        codeLength = 3
      }
      return this.setState({ formData, codeName, codeLength })
    } else if (key === 'cvc') {
      const validCVV = valid.cvv(value, codeLength)
      errorData.cvc = !validCVV.isValid
      this.setState({ errorData })
    } else if (key === 'expiration') {
      const previousValue = this.state.formData.expiration
      if (previousValue.length < value.length) {
        if (moment(value, 'MM/YYYY').isValid()) {
          const res = moment().isSameOrBefore(moment(value, 'MM/YYYY'), 'month')
          if (res === false) {
            errorData[key] = true
            this.setState({ errorData })
          } else {
            value = moment(value, 'MM/YYYY').format('MM/YYYY')
          }
        } else {
          errorData[key] = true
          this.setState({ errorData })
        }
      }
    }
    formData[key] = value
    this.setState({ formData })

    // updateSubmit(this)
  }

  onClickCheckbox (key) {
    const value = this.state.formData[key]
    const query = {...this.state.formData}

    query[key] = !value
    this.setState({ formData: query })
    // updateSubmit(this)
  }

  getUserName () {
    const user = Meteor.user()
    return user.profile.firstname + ' ' + user.profile.lastname
  }

  getBuildingName () {
    const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
    if (condo.name && condo.name !== '' && (condo.info.address && condo.name !== condo.info.address)) {
      return (condo.info.id !== '-1' ? condo.info.id + ' ' : '') + condo.name
    } else {
      return '-'
    }
  }

  getBuildingAddress () {
    const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
    return condo && (condo.info.address + ',')
  }

  getBuildingAddress2 () {
    const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
    return condo && (condo.info.code + ' ' + condo.info.city)
  }

  __saveForm () {
    if (this.state.canSubmit) {
      const form = this.state.formData
      if (typeof this.props.saveForm === 'function') {
        this.props.saveForm(form)
      }
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.canSubmit !== updateSubmit(this)) {
      this.setState({ canSubmit: !this.state.canSubmit})
    }
  }

  render () {
    const translate = this.props.translate
    const formData = this.state.formData

    // this.__saveForm()
    return (
      <div id='addNewCard' className={`mb__payzenWrapper displayed`}>
        <Blaze className='addNewCardReact' template='MbPanel' content={translate.commonTranslation['card_details']} />
        <div className='details-container'>
          <Blaze className='addNewCardReact'
            template='MbInput'
            label={translate.commonTranslation['name_on_card']}
            value={formData.name}
            refreshMe={formData.name}
            required
            onInput={this.onInputDetails.bind(this, 'name')}
            placeholder={translate.commonTranslation['type_name_on_card']}
          />
          <div className='cardNumber'>
            <Blaze className='addNewCardReact'
              template='MbInput'
              label={translate.commonTranslation['card_number_16']}
              value={formData.number}
              refreshMe={formData.number}
              isError={this.getIsError('number')}
              required
              onInput={this.onInputDetails.bind(this, 'number')}
              placeholder='0000 0000 0000 0000'
            />
            <div className={`paymentIcon paymentIcon-${formData.type}`} />
          </div>
          <Blaze className='addNewCardReact'
            template='MbInput'
            label={translate.commonTranslation['expiration_date']}
            value={formData.expiration}
            required
            refreshMe={formData.expiration}
            isError={this.getIsError('expiration')}
            onInput={this.onInputDetails.bind(this, 'expiration')}
            placeholder='MM/YYYY'
          />
          <Blaze className='addNewCardReact'
            template='MbInput'
            label={this.state.codeName + ' ' + translate.commonTranslation['cvc']}
            value={formData.cvc}
            refreshMe={formData.cvc}
            required
            isError={this.getIsError('cvc')}
            onInput={this.onInputDetails.bind(this, 'cvc')}
            type='number'
            placeholder='000'
          />
          {this.props.displaySaveButton !== false &&
            <Blaze
              className='addNewCardReact'
              template='MbCheckbox'
              checked={formData.saveCard}
              label={translate.commonTranslation['save_card_future_purchase']}
              onClick={(this.onClickCheckbox.bind(this, 'saveCard'))}
            />
          }
        </div>
        <div className='saveNewCardContainer'>
          <a type='submit' onClick={this.__saveForm} className={`saveNewCard ${!this.state.canSubmit && 'disabled'}`} data-loading-text={`<i className='fa fa-spinner fa-spin'></i> ${translate.commonTranslation['saving']}...`}>
            {this.props.customButtonText || translate.commonTranslation['personal_details_save']} </a>
        </div>
      </div>
    )
  }
}

MbCreditCardForm.defaultProps = {
  saveForm: null
}

export default withTracker((props) => {
  const condoId = FlowRouter.getParam('condo_id')

  return {
    transReady: Meteor.subscribe('getUserTransactions', condoId).ready()
  }
})(MbCreditCardForm)
