import React from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import Blaze from 'meteor/gadicc:blaze-react-component'
import { FlowRouter } from 'meteor/kadira:flow-router'

import { UserTransactions } from '/common/collections/Payments'

import './MbPayment.less'

/**
 * @param transactionId
 * @param updateValues
 * @constructor
 */

class MbHandlePaymentUpdates extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isRunning: false,
      transactionStatus: null,
      hasRun: false
    }
  }

  static getDerivedStateFromProps (props, state) {
    const { transactionId, transaction, transReady } = props
    const status = transaction && transaction.status
    // const checkTransaction = transaction && transaction.threeDsResponse && transaction.threeDsResponse.createPaymentResult && transaction.threeDsResponse.createPaymentResult.commonResponse.responseCode === 0
    const hasRun = !!state.hasRun || !!transactionId
    let checkTransaction = true

    if (status === 'PENDING') {
      checkTransaction = !!transaction && !!transaction.threeDsResponse && !!transaction.threeDsResponse.createPaymentResult && transaction.threeDsResponse.createPaymentResult.commonResponse.responseCode === 0
    } else if (!status) {
      checkTransaction = false
    }

    return {
      isRunning: transReady && transactionId && !checkTransaction,
      transactionStatus: status,
      hasRun
    }
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.state.hasRun === true && this.props.transReady && !this.state.isRunning) {
      this.setState({ hasRun: false, isRunning: false })
      if (typeof this.props.updateValues === 'function') {
        this.props.updateValues(this.state.transactionStatus)
      }
    }
  }

  render () {
    const { isRunning } = this.state

    return (
      <div>
        { isRunning &&
          <Blaze template='MbWorkInProgress' content='Waiting for 3DS to end' display='true' />
        }
      </div>
    )
  }
}

MbHandlePaymentUpdates.defaultProps = {
  transactionId: null,
  updateValues: null
}

export default withTracker((props) => {
  const condoId = FlowRouter.getParam('condo_id')

  return {
    transReady: Meteor.subscribe('getUserTransactions', condoId).ready(),
    transaction: UserTransactions.findOne({ _id: props.transactionId })
  }
})(MbHandlePaymentUpdates)
