import React from 'react'
import { Meteor } from 'meteor/meteor'
import Blaze from 'meteor/gadicc:blaze-react-component'
import { sAlert } from 'meteor/juliancwirko:s-alert'

import MbHandlePaymentUpdates from './MbHandlePaymentUpdates'

import './MbPayment.less'

/**
 * @param condoId
 * @param amount
 * @param getTransactionResult
 * @param isAliasPayment
 * @param paymentToken
 * @param cardInfo
 * @param basketId
 * @param reservationId
 * @param billingAddress
 * @constructor
 */

// THIS MODULE CAN BE USED ONLY ONE TIME WITH THE SAME AMOUNT TO PREVENT RE-SENDING PAYMENT
// IF YOU NEED TO USE IT MULTIPLE TIME WITH THE SAME `AMOUNT` YOU'LL NEED TO RELOAD THE COMPONENT

export default class MbPayment extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      creatingPayment: false,
      waitingTransactionId: null
    }

    this.__createPayment = this.__createPayment.bind(this)
    this.stopWaiting = this.stopWaiting.bind(this)
  }

  componentDidMount () {
    if (!!this.props.amount && this.state.creatingPayment === false && this.state.waitingTransactionId === null) {
      this.__createPayment()
    } else {
      sAlert.error('Wrong params')
    }
  }

  __createPayment () {
    this.setState({ creatingPayment: true })

    const { amount, condoId, isAliasPayment, paymentToken, cardInfo, billingAddress, basketId, reservationId } = this.props
    let self = this

    Meteor.call('createPayment', condoId, isAliasPayment, paymentToken, cardInfo, amount, billingAddress, basketId, reservationId, function (error, result) {
      if (!error && result.transactionId && result.threeDSAcsUrl) {
        self.setState({ creatingPayment: false, waitingTransactionId: result.transactionId })

        const form = document.createElement('form')
        let MD = document.createElement('input')
        let PaReq = document.createElement('input')
        let TermUrl = document.createElement('input')

        form.method = 'POST'
        form.action = result.threeDSAcsUrl
        form.style = 'display: none'
        form.target = 'payment_view'

        MD.value = result.queryParams.MD
        MD.name = 'MD'
        form.appendChild(MD)

        PaReq.value = result.queryParams.PaReq
        PaReq.name = 'PaReq'
        form.appendChild(PaReq)

        TermUrl.value = result.queryParams.TermUrl
        TermUrl.name = 'TermUrl'

        form.appendChild(TermUrl)

        document.body.appendChild(form)

        form.submit()
      } else if (!error && result.transactionId) {
        self.setState({ creatingPayment: false, waitingTransactionId: result.transactionId })
      } else {
        if (error) {
          sAlert.error(error)
        } else if (result) {
          sAlert.error(result)
        }
        self.setState({ creatingPayment: false })
      }
    })
  }

  stopWaiting (result) {
    if (typeof this.props.getTransactionResult === 'function') this.props.getTransactionResult(this.state.waitingTransactionId, result, this.props.billingAddress)
    this.setState({ creatingPayment: false, waitingTransactionId: null })
  }

  componentDidUpdate (prevProps, prevState) {
  }

  render () {
    const { waitingTransactionId, creatingPayment } = this.state

    return (
      <div>
        { creatingPayment === true &&
          <Blaze template='MbWorkInProgress' content='Creating payment' display='true' />
        }
        { creatingPayment === false && waitingTransactionId !== null &&
          <div>
            <MbHandlePaymentUpdates transactionId={waitingTransactionId} updateValues={this.stopWaiting} />
            <iframe className='payzen-payment-iframe' name='payment_view'></iframe>
          </div>
        }
      </div>
    )
  }
}

MbPayment.defaultProps = {
  condoId: null,
  amount: null,
  getTransactionResult: null,
  isAliasPayment: false,
  paymentToken: null,
  cardInfo: null,
  billingAddress: null,
  basketId: null,
  reservationId: null
}
