import { Meteor } from 'meteor/meteor'

export async function RetrievePayZenAliases (condoId) {
  let aliases = await new Promise(function (resolve, reject) {
    Meteor.call('getPayZenAliases', condoId, (error, result) => {
      if (!error) {
        resolve(result)
      } else {
        reject(error)
      }
    })
  })

  return aliases
}

export async function CreatePayZenAlias (condoId, cardForm) {
  let aliases = await new Promise(function (resolve, reject) {
    Meteor.call('createPayZenAliases', condoId, cardForm, (error, result) => {
      if (!error) {
        resolve(result)
      } else {
        reject(error)
      }
    })
  })

  return aliases
}

export async function UpdatePayZenAlias (condoId) {
  let aliases = await new Promise(function (resolve, reject) {
    Meteor.call('updatePayZenAliases', condoId, (error, result) => {
      if (!error) {
        resolve(result)
      } else {
        reject(error)
      }
    })
  })

  return aliases
}

export async function RemovePayZenAlias (condoId, paymentToken) {
  let aliases = await new Promise(function (resolve, reject) {
    Meteor.call('removePayZenAlias', condoId, paymentToken, (error, result) => {
      if (!error) {
        resolve(result)
      } else {
        reject(error)
      }
    })
  })

  return aliases
}
