import React from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { CommonTranslation } from '/common/lang/lang'
import Blaze from 'meteor/gadicc:blaze-react-component'
import { UsersPayzen } from '/common/collections/Payments'
import MbPayment from './MbPayment/MbPayment'
import {
  CreatePayZenAlias,
  RemovePayZenAlias
} from './MbPayzenAlias/MbPayzenAlias'

import MbCreditCardForm from './MbCreditCardForm/MbCreditCardForm'

import './PayzenWrapper.less'

/**
 * @param isSleeping
 * @param amount            // should be formated, for exemple: 1200 for 12€, 3789 for 37,89€
 * @param getTransactionId
 * @constructor
 */

// THIS MODULE CAN BE USED ONLY ONE TIME WITH THE SAME AMOUNT TO PREVENT RE-SENDING PAYMENT
// IF YOU NEED TO USE IT MULTIPLE TIME WITH THE SAME `AMOUNT` YOU'LL NEED TO RELOAD THE COMPONENT

class PayzenWrapper extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      isRunning: false,
      waitingTransactionId: null,
      selectedCard: null,
      cardInfo: null,
      forceNextStep: false,
      error: null,
      proceedPayment: false,
      sameBillingAddress: true,
      address: '',
      city: '',
      zipCode: '',
      country: ''
    }

    this.__saveCrediCard = this.__saveCrediCard.bind(this)
    this.__onSelectCard = this.__onSelectCard.bind(this)
    this.__selectedCard = this.__selectedCard.bind(this)
    this.__nextStep = this.__nextStep.bind(this)
    this.__saveTransactionId = this.__saveTransactionId.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    if (this.props.isSleeping === false && this.state.isRunning === false && (prevState.error !== null || this.state.error === null)) {
      if (this.props.subsReady && ((this.state.selectedCard === 'useNewCard' && this.state.cardInfo) || (this.state.selectedCard && this.state.forceNextStep))) {
        this.__nextStep()
      }
    }
  }

  __saveCrediCard (cardInfo) {
    const condoId = FlowRouter.getParam('condo_id')
    if (cardInfo && cardInfo.saveCard) {
      this.setState({ isRunning: true, error: null })
      CreatePayZenAlias(condoId, cardInfo).then(res => {
        if (res.success) {
          this.setState({ isRunning: false, selectedCard: res.token, forceNextStep: true })
        } else {
          sAlert.error(res.detail)
          this.setState({ isRunning: false })
        }
      }).catch(e => {
        sAlert.error(e)
        this.setState({ isRunning: false, error: e })
      })
    } else {
      this.setState({ isRunning: false, cardInfo: cardInfo })
    }
  }

  __onSelectCard (cardId) {
    this.setState({ selectedCard: cardId })
  }

  __selectedCard (cardId) {
    return this.state.selectedCard === cardId
  }

  __nextStep () {
    if (this.state.selectedCard && !this.state.isRunning) {
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      const condoId = FlowRouter.getParam('condo_id')
      this.setState({ isRunning: true })
      if (this.props.basketId) {
        Meteor.call('checkPaidBasketQuantity', condoId, (error, result) => {
          if (!result) {
            Meteor.call('checkTimeSlotUnavailable', condoId, (error, result) => {
              if (!result) {
                this.setState({ isRunning: true, proceedPayment: true })
              } else {
                sAlert.error(translate.commonTranslation.rserved_slots_unavailable)
              }
            })
          } else {
            sAlert.error(translate.commonTranslation.not_enough_item)
          }
        })
      } else {
        this.setState({ isRunning: true, proceedPayment: true })
      }
    }
  }

  __saveTransactionId (transactionId, result, billingAddress) {
    if (typeof this.props.getTransactionId === 'function') {
      this.props.getTransactionId(transactionId, result, billingAddress)
    }
    this.setState({ isRunning: false, proceedPayment: false, cardInfo: null, forceNextStep: false })
  }

  __removeCard (paymentToken) {
    const translate = this.props.translate
    const condoId = FlowRouter.getParam('condo_id')
    let self = this
    bootbox.confirm({
      size: 'medium',
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.you_sure_delete_card,
      buttons: {
        'cancel': { label: translate.commonTranslation['cancel'], className: 'btn-outline-red-confirm' },
        'confirm': { label: translate.commonTranslation['confirm'], className: 'btn-red-confirm' }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          RemovePayZenAlias(condoId, paymentToken).then(res => {
            if (res.success) {
              self.setState({ isRunning: false, selectedCard: null, forceNextStep: false })
            } else {
              sAlert.error(res.detail)
              self.setState({ isRunning: false })
            }
            // this.__nextStep()
          }).catch(e => {
            sAlert.error(e)
            self.setState({ isRunning: false, error: e })
          })
        }
      }
    })
  }

  getUserName () {
    const user = Meteor.user()
    return user.profile.firstname + ' ' + user.profile.lastname
  }

  getBuildingName () {
    const condo = Condos.findOne({ _id: FlowRouter.getParam('condo_id') })
    if (condo.name && condo.name !== '' && (condo.info.address && condo.name !== condo.info.address)) {
      return (condo.info.id !== '-1' ? condo.info.id + ' ' : '') + condo.name
    } else {
      return '-'
    }
  }

  getBuildingAddress () {
    const condo = Condos.findOne({ _id: FlowRouter.getParam('condo_id') })
    return condo && (condo.info.address + ',')
  }

  getBuildingAddress2 () {
    const condo = Condos.findOne({ _id: FlowRouter.getParam('condo_id') })
    return condo && (condo.info.code + ' ' + condo.info.city)
  }

  render () {
    const condoId = FlowRouter.getParam('condo_id')
    const { isSleeping, translate, amount, aliases, subsReady, basketId, reservationId } = this.props
    const { selectedCard, cardInfo, proceedPayment, sameBillingAddress, address, zipCode, city, country } = this.state

    const billingAddress = sameBillingAddress ? null : {
      address: address,
      zipCode: zipCode,
      city: city,
      country: country,
    }

    const canSubmit = !!selectedCard && (!!sameBillingAddress || (!!address && !!zipCode && !!city && !!country))

    return (
      <div className={`mb__payzenWrapper ${isSleeping ? 'hidden' : 'displayed'}`}>
        {/* {isRunning &&
          <Blaze template='MbWorkInProgress' content='Work in progress' display='true' />
        } */}
        {!!proceedPayment &&
          <MbPayment
            amount={amount}
            condoId={condoId}
            isAliasPayment={selectedCard !== 'useNewCard'}
            paymentToken={selectedCard}
            cardInfo={cardInfo}
            getTransactionResult={this.__saveTransactionId}
            billingAddress={billingAddress}
            basketId={basketId}
            reservationId={reservationId}
          />
        }
        {!isSleeping && subsReady === true &&
          <div className='billing-address-container'>
            <Blaze
              className='addNewCardReact'
              key={'panelSameBillingAddress'}
              template='MbPanel'
              content={translate.commonTranslation['billing_address']}
            />
            <div className='details-container'>
              <Blaze
                className='addNewCardReact'
                key={'checkboxSameBillingAddress'}
                template='MbCheckbox'
                checked={this.state.sameBillingAddress}
                label={translate.commonTranslation.same_billing_address}
                onClick={() => this.setState({ 'sameBillingAddress': !this.state.sameBillingAddress })}
              />
              {!this.state.sameBillingAddress &&
                <div className='billing-address-inputs'>
                  <Blaze
                    key={'streetBillingAddress'}
                    template='MbInput'
                    label={translate.commonTranslation.billing_address}
                    value={this.state.address}
                    refreshMe={this.state.address}
                    required
                    // isError={getIsError 'address'}
                    onInput={(value) => this.setState({ 'address': value })}
                    placeholder={translate.commonTranslation.street_address}
                  />
                  <div className='city-code-inputs-container'>
                    <Blaze
                      key={'codeBillingAddress'}
                      template='MbInput'
                      type='number'
                      noLabel
                      value={this.state.zipCode}
                      refreshMe={this.state.zipCode}
                      // isError={getIsError 'city'}
                      onInput={(value) => this.setState({ 'zipCode': value })}
                      placeholder={translate.commonTranslation.zip_code}
                    />
                    <Blaze
                      key={'cityBillingAddress'}
                      className={'city-inline-wrapper'}
                      template='MbInput'
                      noLabel
                      value={this.state.city}
                      refreshMe={this.state.city}
                      // isError={getIsError 'city'}
                      onInput={(value) => this.setState({ 'city': value })}
                      placeholder={translate.commonTranslation.city}
                    />
                  </div>
                  <Blaze
                    key={'countryBillingAddress'}
                    template='MbInput'
                    noLabel
                    value={this.state.country}
                    refreshMe={this.state.country}
                    // isError={getIsError 'country'}
                    onInput={(value) => this.setState({ 'country': value })}
                    placeholder={translate.commonTranslation.country}
                  />
                </div>
              }
              {this.state.sameBillingAddress &&
                <div className='sameAddressContainer'>
                  <span>{this.getUserName()}</span>
                  <span>{this.getBuildingName()}</span>
                  <span>{this.getBuildingAddress()}</span>
                  <span>{this.getBuildingAddress2()}</span>
                </div>
              }
            </div>
          </div>
        }
        {!isSleeping && subsReady === true && !!aliases &&
          <Blaze
            key={'panelSameBillingAddress'}
            template='MbPanel'
            content={translate.commonTranslation['payment_method']}
          />
        }
        {!isSleeping && subsReady === true && !!aliases &&
          aliases.map(card => {
            return (
              <div className='paymentMethodsController' key={card.paymentToken}>
                <Blaze
                  key={card.paymentToken}
                  template='MbCheckbox'
                  checked={this.__selectedCard(card.paymentToken)}
                  label={''}
                  onClick={(this.__onSelectCard.bind(this, card.paymentToken))}
                />
                <div className='cardIconContainer'>
                  <div className={`paymentIcon-${card.cardType}`} />
                </div>
                <div className='cardInfos'>
                  <div className='cardType'>{card.cardType}</div>
                  <div className='cardNumber'>{card.displayableNumber}</div>
                </div>
                <div className='cardActions'>
                  <div className='trashIcon' onClick={this.__removeCard.bind(this, card.paymentToken)} />
                </div>
              </div>
            )
          })
        }
        {!isSleeping && subsReady === true && !!aliases &&
          <div className='paymentMethodsController' key={'useNewCard'}>
            <Blaze
              key={'useNewCard'}
              template='MbCheckbox'
              checked={this.__selectedCard('useNewCard')}
              onClick={(this.__onSelectCard.bind(this, 'useNewCard'))}
            />
            <div className='cardInfos'>
              <div className='cardType'>{translate.commonTranslation.useNewCard}</div>
            </div>
          </div>
        }
        {!isSleeping && subsReady === true && (!!aliases && selectedCard !== 'useNewCard') &&
          <div className='next-step-button-container'>
            <a type='submit' onClick={this.__nextStep} className={`next-step-button ${canSubmit ? '' : 'disabled'}`} data-loading-text={`<i className='fa fa-spinner fa-spin'></i> ${translate.commonTranslation['saving']}...`}>
              {translate.commonTranslation['next']} </a>
          </div>
        }
        {!isSleeping && subsReady === true && (!aliases || selectedCard === 'useNewCard') &&
          <MbCreditCardForm saveForm={this.__saveCrediCard} parentSubmit={canSubmit} translate={translate} customButtonText={translate.commonTranslation['next']} displaySaveButton />
        }
      </div>
    )
  }
}

PayzenWrapper.defaultProps = {
  isSleeping: false,
  amount: null,
  getTransactionId: null,
  basketId: null,
  reservationId: null
}

export default withTracker((props) => {
  const condoId = FlowRouter.getParam('condo_id')
  const lang = FlowRouter.getParam('lang') || 'fr'
  const translate = new CommonTranslation(lang)

  return {
    subsReady: Meteor.subscribe('gePayzenAlias', condoId).ready(),
    aliases: UsersPayzen.find().fetch(),
    translate
  }
})(PayzenWrapper)
