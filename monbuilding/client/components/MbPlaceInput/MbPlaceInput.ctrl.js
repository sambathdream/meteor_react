/**
 * Created by kuyarawa on 8/2/17.
 */
import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var';
import { GoogleApiError } from "/common/lang/lang.js";

let autocomplete, thePlace;

Template.MbPlaceInput.onCreated(function () {
  this.randomId = getRandomString();
  this.autocomplete = null
  this.thePlace = null

  this.address = Template.currentData().address;
});

Template.MbPlaceInput.onRendered(function () {
  initAutocomplete(Template.instance());
});


Template.MbPlaceInput.helpers({
  getLabel: () => {
    return Template.instance().data.label ? Template.instance().data.label : '';
  },
  getPlaceholder: () => {
    return Template.instance().data.placeholder ? Template.instance().data.placeholder : '';
  },
  getValue: () => {
    const value = Template.instance().data.value
    return (value && value.address) ? value.address : '';
  },
  isRequired: () => {
    return Template.instance().data.required
  },
  getRandomId: () => {
    return Template.instance().randomId;
  },
  isError: () => {
    return Template.instance().data.isError || false
  },
  getDisabled: () => {
    return Template.instance().data.disabled || false
  }

});

Template.MbPlaceInput.events({
  'focus .mb-input > input': (e, t) => {
    $('.pac-container').css({ 'z-index': 1051 })
    geolocate(t)
  },
  /* It's causing double validation / error management.. */
  // 'blur .mb-input > input': (e, t) => {
  //   Meteor.setTimeout(function() {
  //     getAddress(t)
  //   }, 500);
  // },

  'submit #update-value': (e, t) => {
    e.preventDefault();
    t.data.onInput(t.thePlace, e)
  },
});

function initAutocomplete(t) {
  const id = 'mb-input-' + t.randomId

  t.autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById(id)),
    { types: ['geocode'] });
  t.autocomplete.addListener('place_changed', getAddress.bind(this, t));
}

function find_property(tabl, prop) {
  for (elem of tabl) {
    if (elem.types.indexOf(prop) !== -1)
      return elem.long_name;
  }
  return undefined;
}

function checkThisPlace(place) {
  let translation = new GoogleApiError((FlowRouter.getParam("lang") || "fr"));
  if (place === undefined || !place.formatted_address)
    return translation.googleApiError["error1"];
  if (!find_property(place.address_components, "street_number"))
    return translation.googleApiError["error2"];
  if (!find_property(place.address_components, "route"))
    return translation.googleApiError["error3"];
  if (!find_property(place.address_components, "locality"))
    return translation.googleApiError["error4"];
  if (!find_property(place.address_components, "country"))
    return translation.googleApiError["error5"];
  if (!find_property(place.address_components, "postal_code"))
    return translation.googleApiError["error6"];
  if (!place.geometry.location.lat())
    return translation.googleApiError["error7"];
  if (!place.geometry.location.lng())
    return translation.googleApiError["error7"];
  if (!place.place_id)
    return translation.googleApiError["error7"];
  return true;
}

function getAddress(t) {
  // Get the place details from the t.autocomplete object.
  let place = t.autocomplete.getPlace()
  const id = '#mb-input-' + t.randomId
  const inputValue = $(id).val()
  if (!inputValue) {
    return
  }
  const checkedPlace = checkThisPlace(place)
  if (checkedPlace === true) {
    t.thePlace = {
      street_number: find_property(place.address_components, "street_number"),
      road: find_property(place.address_components, "route"),
      city: find_property(place.address_components, "locality"),
      country: find_property(place.address_components, "country"),
      code: find_property(place.address_components, "postal_code"),
      address: place.formatted_address,
      lat: place.geometry.location.lat(),
      lng: place.geometry.location.lng(),
      id: place.place_id
    };
  }
  else {
    if (checkedPlace !== false) {
      t.thePlace = { error: checkedPlace }
    }
  }
  t.$('#update-value').submit();
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate(t) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
      let geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      let circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      t.autocomplete.setBounds(circle.getBounds());
    });
  }
}
