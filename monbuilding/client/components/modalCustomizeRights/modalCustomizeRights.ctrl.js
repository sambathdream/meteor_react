function getMergedRights(roleId, condoId) {
  let currentDefaultRole = DefaultRoles.findOne(roleId)
  if (!currentDefaultRole)
    return undefined

  let currentCondoRole = CondoRole.findOne({
    condoId: condoId,
    for: currentDefaultRole.for,
    name: currentDefaultRole.name
  })

  let mergedRoles = Meteor.mergeRoles(currentDefaultRole, currentCondoRole)
  mergedRoles.rights = _.sortBy(mergedRoles.rights, function (right) {
    return right.displayOrder
  })
  return mergedRoles
}

Template.modalCustomizeRights.onCreated(function () {
  this.userId = Meteor.userId()
  this.currentDisplayingModule = null
  this.roleId = Template.currentData().roleId
  this.condoId = Template.currentData().condoId
  this.customRights = Template.currentData().customRights
  this.modalTitle = Template.currentData().modalTitle
  this.cb = Template.currentData().cb

  let mergedRights = getMergedRights(this.roleId, this.condoId)

  let self = this
  const rightNameToModuleName = {
    emergencyContact: 'emergencyContact',
    wallet: 'digitalWallet',
    marketPlace: 'marketPlace',
    incident: 'incidents',
    actuality: 'informations',
    manual: 'manual',
    map: 'map',
    trombi: 'trombi',
    messenger: 'messenger',
    reservation: 'reservations',
    forum: 'forum',
    ads: 'classifieds',
    print: 'print'
  }
  const condo = Condos.findOne({ _id: this.condoId })

  _.each(mergedRights.rights, function (right, key) {
    const moduleName = rightNameToModuleName[right.module] || null
    let isActive = true
    if (condo) {
      if (moduleName && condo.settings && condo.settings.options) {
        isActive = condo.settings.options[moduleName];
      }
    }
    if (!isActive) {
      mergedRights.rights[key] = null
    } else {
      if (Meteor.isAdmin !== true && right.module === 'view' && right.right === 'concierge') {
        delete mergedRights.rights[key]
        return
      }
      if (self.customRights && self.customRights.module && self.customRights.module[right.module] && _.isBoolean(self.customRights.module[right.module][right.right])) {
        mergedRights.rights[key].default = self.customRights.module[right.module][right.right];
      }
    }
  });
  if (this.customRights === null) {
    this.customRights = { module: {} }
  }

  mergedRights.rights = _.without(mergedRights.rights, null)
  this.defaultRight = new ReactiveVar(mergedRights)
})

Template.modalCustomizeRights.onDestroyed(function () {
})

Template.modalCustomizeRights.onRendered(() => {
})

Template.modalCustomizeRights.events({
  'click .labelForCheckbox': function (event, template) {
    const userId = template.userId
    const roleId = template.roleId

    const edition = $(event.currentTarget).attr('for').split('.')
    edition[2] = edition[2] == "true" ? true : (edition[2] == "false" || edition[2] === '') ? false : null

    const rightName = $(event.currentTarget).data('display')
    let index = $(event.currentTarget).data('index')

    let defaultRight = template.defaultRight.get()

    if (Meteor.userHasRight(edition[0], edition[1], template.condoId)) {
      if (!!defaultRight.rights[index]) {
        defaultRight.rights[index].default = !edition[2]
        if (!template.customRights.module[edition[0]]) {
          template.customRights.module[edition[0]] = {}
        }
        template.customRights.module[edition[0]][edition[1]] = !edition[2]
      }

      template.defaultRight.set(defaultRight)
    }
  },
})

Template.modalCustomizeRights.helpers({
  iscurrentDisplayingModule: (moduleName) => {
    if (Template.instance().currentDisplayingModule == moduleName && moduleName !== 'view')
      return true
    Template.instance().currentDisplayingModule = moduleName
    return false
  },
  getUserRights: () => {
    return Template.instance().defaultRight.get()
  },
  viewName: (moduleName, displayOrder) => {
    return moduleName + displayOrder
  },
  saveRights: () => {
    let t = Template.instance()

    return () => {
      t.cb(t.customRights)
    }
  },
  getCondoId: () => {
    return Template.instance().condoId
  },
  getModalTitle: () => {
    return Template.instance().modalTitle
  }
})
