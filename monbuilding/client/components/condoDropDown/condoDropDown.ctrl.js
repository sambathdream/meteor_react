import { Session } from 'meteor/session';

/*
{{> CondoDropDown  condoList=CDD_Cursor  retCondo=CondoId/CondoIdModal  classes=""  noAll=0/1  noDisplay=0/1 }}

condoList       :   (~opt~)    It's a cursor of condo.
retCondo        :   ( opt )    Callback Function for set variables.    In your helper :         'helper_name': function() { return (condoId) => {} },
module          :   ( opt )    For check if the module is in condo.    (list line 23)
classes         :   ( opt )    Additional classes.                     (ex: pull-right)
noAll           :   ( opt )    View "All Building" or not.             (0 -> view  // 1 -> not)
noDisplay       :   ( opt )    View "Selection your building" or not.  (0 -> view  // 1 -> not)
selectedCondoId :   ( opt )    Auto select condoId
*/

Template.CondoDropDown.onCreated(function() {
	this.condoSearch = new ReactiveVar('');
	this.condoDropdown = null
	if (Template.instance().data.selectedCondoId) {
		this.condoDropdown = new ReactiveVar(Template.instance().data.selectedCondoId);
	} else {
		this.condoDropdown = new ReactiveVar(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || 'all');
	}
  if (Template.instance().data.condoList && this.condoDropdown.get() != 'all') {
    if (!_.contains(Template.instance().data.condolist, this.condoDropdown.get())) {
      if (Template.instance().data.noAll == true) {
        this.condoDropdown.set(Template.instance().data.condoList[0])
        Session.set('selectedCondo', Template.instance().data.condoList[0])
      } else {
        this.condoDropdown.set('all')
        Session.set('selectedCondo', 'all')
      }
    }
  } else if (Template.instance().data.noAll == true && this.condoDropdown.get() == 'all' && Template.instance().data.condoList) {
    this.condoDropdown.set(Template.instance().data.condoList[0])
    Session.set('selectedCondo', Template.instance().data.condoList[0])
  }
	this.query = new ReactiveVar({});
	this.autorun(function(){
		Session.set("currentCondo", Template.instance().condoDropdown.get());
	});
	if (Template.instance().data.module) {
		let opt = Template.instance().data.module;
		if (opt == 'incidents') {
			this.query.set({'settings.options.incidents': true});
		}
		else if (opt == 'EDL') {
			this.query.set({'settings.options.EDL': true});
		}
		else if (opt == 'informations') {
			this.query.set({'settings.options.informations': true});
		}
		else if (opt == 'classifieds') {
			this.query.set({'settings.options.classifieds': true});
		}
		else if (opt == 'forum') {
			this.query.set({'settings.options.forum': true});
		}
		else if (opt == 'forumSyndic') {
			this.query.set({'settings.options.forumSyndic': true});
		}
		else if (opt == 'messenger') {
			this.query.set({'settings.options.messenger': true});
		}
		else if (opt == 'reservations') {
			this.query.set({'settings.options.reservations': true});
		}
		else if (opt == 'concierge') {
			this.query.set({'settings.options.conciergerie': true});
		}
	}
	if (Template.instance().data.selectedCondoId) {
		Session.set("currentCondo", Template.instance().data.selectedCondoId);
		if (jQuery.isFunction(Template.instance().data.condoId)) {
			Template.instance().data.condoId(Template.instance().data.selectedCondoId);
		}
	}
});

Template.CondoDropDown.onRendered(function() {
	let template = Template.instance();
	if (template.data.selectAuto == true) {
		setTimeout(function() {
			condoId = template.condoDropdown.get()
      
			if (condoId == "all" && template.data.noAll && template.data.noAll == true) {
        
				let currentCondoId = [];
				let query = template.query.get();
        
				if (template.data.condoList) {
					query._id = {$in : template.data.condoList};
				}
				currentCondoId = Condos.findOne(query)._id || null;
        
				template.condoDropdown.set(currentCondoId);
				Session.set('selectedCondo', currentCondoId);
				if (template.data.condoId) {
					template.data.condoId(currentCondoId);
				}
			}
		}, 400);
	}
  
	// For Document ( Manual + Map )
  
	setTimeout(function() {
		if (Session.get("DocumentCondoId")) {
			let currentCondoId = Session.get("DocumentCondoId");
			template.condoDropdown.set(currentCondoId);
			Session.set('DocumentCondoId', null)
		}
	}, 300);
  
	setTimeout(function () {
		if (Session.get('selectedCondo') && !template.data.selectedCondoId) {
			template.condoDropdown.set(Session.get('selectedCondo'));
			if (template.data.condoId)
      template.data.condoId(Session.get('selectedCondo'));
		}
	}, 400);
	$('.modal').on('hidden.bs.modal', function (e) {
		template.condoSearch.set('');
		template.condoDropdown.set(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || 'all');
		if (template.data.condoId) {
			template.data.condoId(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || 'all');
		}
	});
});


Template.CondoDropDown.events({
	'click .dropdown-condo': function(event, template) {
		template.condoDropdown.set(event.currentTarget.getAttribute("data-id"));
		Session.set('selectedCondo', event.currentTarget.getAttribute("data-id"));
		if (template.data.condoId)
    template.data.condoId(event.currentTarget.getAttribute("data-id"));
	},
});

Template.CondoDropDown.helpers({
  checkSessionVar: () => {
    let oldCondoId = Template.instance().condoDropdown.get()
    let newCondoId = Session.get('selectedCondo')
    if (oldCondoId !== newCondoId && Template.instance().data.checkSessionVar === true) {
      Template.instance().condoDropdown.set(newCondoId)
    }
  },
	viewAllBuilding: () => {
		return !Template.instance().data.noAll;
	},
  onSearch: () => {
    const template = Template.instance();
    return (val) => {
      template.condoSearch.set(val);
    }
  },
	getCurrentCondo: () => {
		return Template.instance().condoDropdown.get();
	},
	getCondoList: () => {
		let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
		let query = {};
		query = Template.instance().query.get();
		query.$or = [{"name": regexp}, {"info.id": regexp}, {"info.address": regexp}, {"info.code": regexp}, {"info.city": regexp}];
		if (Template.instance().data.condoList) {
			query._id = {$in: Template.instance().data.condoList};
		}
		return (Condos.find(query).fetch());
	},
	getCondoName: () => {
		let condoId = Template.instance().data.selectedCondoId || Template.instance().condoDropdown.get()
		let condo = Condos.findOne(condoId);
		if (condo && condoId) {
			if (condo.info.id != '-1')
      return condo.info.id + ' - ' + condo.getName();
			else
      return condo.getName();
		}
		else {
			// If redirection
			condoId = FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getQueryParam("id");
			condo = Condos.findOne(condoId);
			if (condo) {
				if (condo.info.id != '-1')
        return condo.info.id + ' - ' + condo.getName();
				else
        return condo.getName();
			}
		}
	},
  getActiveClass: (type) => {
		return Template.instance().data.selectedCondoId === type || Template.instance().condoDropdown.get() === type ? 'active' : ''
	},
	getClass: () => {
		if (Template.instance().data.classes)
    return Template.instance().data.classes;
	},
	isModalDropDown: () => {
		if (Template.instance().data.noDisplay)
    return true;
	},
	isAllreadySelect: () => {
		// return ;
		let template = Template.instance();
    setTimeout(function() {
      if (template.data.condoId) {
        if (template.data.selectedCondoId) {
          Session.set("currentCondo", template.data.selectedCondoId);
          if (jQuery.isFunction(template.data.condoId)) {
            template.data.condoId(template.data.selectedCondoId);
          }
        } else {
          if (jQuery.isFunction(template.data.condoId)) {
            template.data.condoId(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || 'all');
          }
        }
      }
    }, 10);
	}
});
