Template.MbInput.onCreated(function () {
  this.randomId = getRandomString();

    // const key = "blur " + "#mb-input-" + id;
    // let eventsMap = {};
    // eventsMap[key] = (e) => {
    //     if (typeof Template.instance().data.onBlur === 'function') {
    //         Template.instance().data.onBlur($(e.currentTarget).val());
    //     }
    // }
});

Template.MbInput.onRendered(function () {

});

Template.MbInput.events({
    'blur .mb-input > input': (e, t) => {
        if (typeof t.data.onBlur === 'function') {
            t.data.onBlur($(e.currentTarget).val())
        }
    },
    'keydown .mb-input > input'(e, t) {
      if (Template.instance().data.type === 'number' && event.key === '.' ) {
        event.preventDefault();
      }
    },
    'input .mb-input > input': (e, t) => {
        if (typeof t.data.onInput === 'function') {
            t.data.onInput($(e.currentTarget).val(), e)
        }
    },
    'click .mb-input-addIcon': (e, t) => {
      if (typeof t.data.addButtonCb === 'function') {
        const inputVal = t.$('.mb-input > input').val()
        t.data.addButtonCb(inputVal)
      }
    }
});

Template.MbInput.helpers({
  displayAddButton: () => {
    return Template.instance().data.displayAddButton ? Template.instance().data.displayAddButton : false;
  },
  getDataList: () => {
    return Template.instance().data.dataList ? Template.instance().data.dataList : '';
  },
  getLabel: () => {
    return Template.instance().data.label ? Template.instance().data.label : '';
  },
  getNoLabel: () => {
    return Template.instance().data.noLabel ? Template.instance().data.noLabel : false
  },
  getPlaceholder: () => {
    return Template.instance().data.placeholder ? Template.instance().data.placeholder : '';
  },
  getValue: () => {
    return Template.instance().data.value ? Template.instance().data.value : '';
  },
  refreshVal: () => {
    if (typeof Template.currentData().refreshMe === 'string') {
      const thisId = Template.instance().randomId
      if (Template.instance().view.isRendered) { // prevent error in console when DOM not rendered on modal
        Template.instance().$('.mb-input > input').val(Template.currentData().refreshMe)
      }
    }
  },
  isRequired: () => {
    return Template.instance().data.required
  },
  getRandomId: () => {
    return Template.instance().randomId;
  },
  isError: () => {
    return Template.instance().data.isError || false
  },
  getMaxLengthAttr: () => {
    return Template.instance().data.maxLength || null
  },
  getMinLengthAttr: () => {
    return Template.instance().data.minLength || null
  },
  getType: () => {
    console.log(Template.instance().data.type);
    return Template.instance().data.type || 'text'
  },
  getDisabled: () => {
    return Template.instance().data.disabled || false
  },
  getMin: () =>{
    return Template.instance().data.min || null
  },
  getMax: () =>{
    return Template.instance().data.max || null
  },
  getId : () => {
    console.log(Template.instance().data.id)
    return Template.instance().data.id || null
  },
   getRemainingChar: () => {
    const maxLength = Template.instance().data.maxLength
    if (Template.instance().view.isRendered) { // prevent error in console when DOM not rendered on modal
      const actualLength = (Template.instance().$('.mb-input > input').val() || '').length
      return maxLength - actualLength
    } else {
      return maxLength
    }
  }
});
