import React from 'react'

import './MbFilePreview.less'
import FileServer from '/client/utils/file-server.js'

const getFileUrlPath = (file, tag) => {
  return `${file._downloadRoute}/${file._collectionName}/${file._id}/${tag}/${file._id}.${file.extension}`
}

class MbFilePreviewReactContainer extends React.Component {
  constructor (props) {
    super(props)
    this.timer = null
    this.state = {
      files: [],
      key: Math.random()
    }
  }

  getFiles (files) {
    const images = _.map(files, f => {
      if (f && f.meta && f.meta.thumb) {
        return {
          lowRes: f.meta.thumb,
          preview: f.meta.thumb,
          large: f.meta.thumb,
          type: 'image',
          filename: f.name,
          downloadPath: `${f.meta.thumb}?download=true`,
          isVideo: true,
          videoPath: getFileUrlPath(f, 'original'),
          videoType: f.type
        }
      } else if (f.type.split('/')[0] === 'image') {
        return {
          lowRes: getFileUrlPath(f, 'thumb100'),
          preview: getFileUrlPath(f, 'preview'),
          large: getFileUrlPath(f, 'large'),
          type: 'image',
          filename: f.name,
          downloadPath: `${getFileUrlPath(f, 'original')}?download=true`
        }
      } else {
        return {
          lowRes: `/img/icons/svg/${getImageByExtension(f.type)}`,
          preview: `/img/icons/svg/${getImageByExtension(f.type)}`,
          large: `/img/icons/svg/${getImageByExtension(f.type)}`,
          type: 'file',
          filename: f.name,
          downloadPath: `${getFileUrlPath(f, 'original')}?download=true`
        }
      }
    })

    return images
  }

  componentDidMount () {
    this.setState({
      files: this.props.nativeObject ? this.props.files : (this.props.files && this.getFiles(this.props.files.fetch()) || [])
    })
  }

  componentWillReceiveProps (props) {
    if (props.files) {
      if (!this.props.nativeObject) {
        if (!_.isEqual(this.getFiles(props.files.fetch()), this.state.files)) {
          clearTimeout(this.timer)

          this.timer = setTimeout(() => {
            this.setState({
              files: this.getFiles(props.files.fetch()),
              key: Math.random()
            })
          }, 500)
        } else if (props.limit !== this.props.limit) {
          this.timer = setTimeout(() => {
            this.setState({
              key: Math.random()
            })
          }, 500)
        }
      } else {
        if (!_.isEqual(this.getFiles(props.files), this.state.files)) {
          clearTimeout(this.timer)

          this.timer = setTimeout(() => {
            this.setState({
              files: props.files,
              key: Math.random()
            })
          }, 500)
        } else if (props.limit !== this.props.limit) {
          this.timer = setTimeout(() => {
            this.setState({
              key: Math.random()
            })
          }, 500)
        }
      }
    }
  }

  render () {
    return (
      <MbFilePreviewReact
        key={this.state.key}
        files={this.state.files}
        newFiles={this.props.newFiles}
        layout={this.props.layout}
        forceContainerWidth={this.props.forceContainerWidth || false}
        limit={this.props.limit}
        onMore={this.props.onMore}
      />
    )
  }
}

class MbFilePreviewReact extends React.Component {
  constructor (props) {
    super(props)
    this.timer = null
    this.randomId = getRandomString()
  }

  initPhotoGrid () {
    setTimeout(() => {
      $(`#mb-file-preview-${this.randomId}`).photosetGrid({
        gutter: '4px',
        width: '800px',
        // forceContainerWidth: !!this.props.forceContainerWidth,
        forceContainerWidth: false,
        highresLinks: true,
        limit: this.props.limit,
        autoCenter: true,
        onComplete: () => {
          const el = `#mb-file-preview-${this.randomId}`

          $(el).addClass('mb-file-preview-done')

          $(`${el} a.is-image`)
            .colorbox({
              rel: el,
              photo: true,
              scalePhotos: false,
              maxHeight: '75%',
              maxWidth: '90%',
              width: function () {
                const $img = $(this).find('img')

                return $img.attr('data-type') === 'file' ? 480 : false
              },
              height: function () {
                const $img = $(this).find('img')

                return $img.attr('data-type') === 'file' ? 480 : false
              },
              title: function () {
                const $img = $(this).find('img')

                if ($img.attr('data-type') === 'file') {
                  const title = $img.attr('alt')
                  const download = $img.attr('data-download')
                  return `${title} <a href="${download}" target="_blank">Download</a>`
                }

                return false
              }
            })

          // video iframe initialization
          $(`${el} a.is-video`).each((index) => {
            const element = $(`${el} a.is-video`).eq(index)
            const video = $(element.attr('href'))
            element.colorbox({
              rel: el,
              inline: true,
              maxHeight: '80%',
              maxWidth: '80%',
              href: video,
              scrolling: false
            })
          })

          $(`${el} a.photo-cell-img-last-item`).on('click', (e) => {
            if (typeof this.props.onMore === 'function') {
              e.preventDefault()
              this.props.onMore()
            }
          })
        }
      })
    }, 500)
  }

  componentDidMount () {
    if (
      (this.props.files && this.props.files.length) ||
      (this.props.newFiles && this.props.newFiles.length)
    ) {
      this.initPhotoGrid()
    }
  }

  shouldComponentUpdate (props) {
    return !_.isEqual(props.files, this.props.files) ||
      !_.isEqual(props.limit, this.props.limit) ||
      !_.isEqual(props.newFiles, this.props.newFiles)
  }

  componentDidUpdate () {
    this.initPhotoGrid()
  }

  render () {
    let files = this.props.files || []

    if (this.props.newFiles) {
      files = [
        ...files,
        ...this.props.newFiles
      ]
    }

    return (
      <div
        id={`mb-file-preview-${this.randomId}`}
        ref={ref => { this.ref = ref }}
        className='mb-file-preview-wrapper'
        data-layout={this.props.layout}
      >
        { files.map((file, i) => {
          const randomId = 'file-' + Math.floor(Math.random() * 10000)
          if (file.fileId && file.type === 'video') {
            const thumbnailUrl = FileServer.getFileUrl(file, 'thumbnail')
            const videoUrl = FileServer.getFileUrl(file, 'video')
            return (
              <div key={i}>
                <img
                  src={thumbnailUrl}
                  data-original={thumbnailUrl}
                  data-highres={thumbnailUrl}
                  data-download={videoUrl}
                  data-type={'image'}
                  data-video
                  data-href={randomId}
                />
                <div style={{display: 'none'}}>
                  <video id={randomId} height='100%' width='100%' controls poster={thumbnailUrl} preload='true'>
                    <source src={videoUrl} />
                  </video>
                </div>
              </div>
            )
          }
          if (!file.fileId) {
            return (
              <div key={i}>
                <img
                  src={file.lowRes}
                  alt={file.filename}
                  data-original={file.large}
                  data-highres={file.large}
                  data-type={file.type}
                  data-download={file.isVideo ? file.videoPath : file.downloadPath}
                  data-video={file.isVideo}
                  data-href={randomId}
                />
                {
                  file.isVideo &&
                  <div style={{display: 'none'}}>
                    <video id={randomId} width='100%' height='100%' controls poster={file.large} preload='true'>
                      <source src={file.videoPath} />
                    </video>
                  </div>
                }
              </div>
            )
          }
        })}
      </div>
    )
  }
}

MbFilePreviewReact.defaultProps = {
  limit: -1,
  nativeObject: false
}

export default MbFilePreviewReactContainer
