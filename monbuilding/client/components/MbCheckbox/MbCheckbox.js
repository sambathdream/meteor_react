/**
 * Created by kuyarawa on 31/01/18.
 */

/*
 {{>MbCheckbox  checked=true/false  label="The label"  onClick="onclick callback function"}}
 */
Template.MbCheckbox.onCreated(function() {
    const id = getRandomString();
    this.randomId = id;

    const key = "click " + "#mb-checkbox-" + id;
    let eventsMap = {};
    eventsMap[key] = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (typeof Template.instance().data.onClick === 'function') {
            Template.instance().data.onClick();
        }
    }

    Template.MbCheckbox.events(eventsMap);
});

Template.MbCheckbox.helpers({
    getLabel: () => {
        return Template.instance().data.label ? Template.instance().data.label : '';
    },
    getRandomId: () => {
        return Template.instance().randomId;
    },
    isChecked: () => {
        return Template.instance().data.checked;
    },
    getChecked: () => {
        return Template.instance().data.checked ? 'checked' : ''
    }
});
