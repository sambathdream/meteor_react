import { ENGINE_METHOD_ALL } from "constants";
import { CommonTranslation } from '/common/lang/lang'

Template.MbSelect.onCreated(function () {
  const id = getRandomString()
  this.randomId = id

  this.options = Template.instance().data.options ? Template.instance().data.options : []

  const key = 'change ' + '#mb-select-' + id
  let eventsMap = {}
  eventsMap[key] = (e) => {
    if (typeof Template.instance().data.onSelect === 'function') {
      let val = $(e.currentTarget).val()
      if (!!Template.instance().data.multiple) {
        val = _.compact($(e.currentTarget).val())
      }
      Template.instance().data.onSelect(val)
    }
  }

  eventsMap[`mouseenter #info-bubble-${id}`] = (e) => {
    $(`#info-content-${id}`).css('display', 'block')
    $(`#info-content-${id}`).addClass('visible')
  }

  eventsMap[`mouseleave #info-bubble-${id}`] = (e) => {
    $(`#info-content-${id}`).css('display', 'none')
    $(`#info-content-${id}`).removeClass('visible')
  }

  Template.MbSelect.events(eventsMap)
})

function formatState(state) {
  if (!state.id) {
    return state.text
  }
  var $state = $(
    '<span>+ ' + state.id + '</span>'
  )
  return $state
}

function formatStateResult(state) {
  console.log('stat'+state)
  if (!state.id) {
    return state.text
  }
  var $state = $('<span>' + state.text + '<span class="phoneCodeDisplay">+ ' + state.id + '</span></span>')
  return $state
}

function formatState2(state) {
  const splitted = state.text.split('_//_')
  if (!state.id || !splitted[1]) {
    return state.text
  }
  var $state = $(
    '<span>' + splitted[0] + '</span>'
  )
  return $state
}

function formatStateResult2(state) {
  const splitted = state.text.split('_//_')
  if (!state.id || !splitted[1]) {
    return state.text
  }
  // if (state.disabled === true && splitted[1]) {
  //   const lang = FlowRouter.getParam('lang') || 'fr'
  //   const translate = new CommonTranslation(lang)
  //   return $('<span>' + splitted[0] + '<span class="infoSelect2"> (' + splitted[1] + ') (' + translate.commonTranslation.noAccessManagerMP + ')</span></span>')
  // }
  let elem = '<span>' + splitted[0] + '<span class="infoSelect2">'
  splitted.forEach((split, index) => {
    if (index > 0 && !!split) {
      elem += ' (' + split + ')'
    }
  })
  elem += '</span></span>'
  var $state = $(elem)
  return $state
}

const initializeMbSelect = (selector, instance) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
  let options = {}
  if (instance.data.isPhoneCodeList) {
    options = {
      minimumResultsForSearch: 10,
      placeholder: instance.data.placeholder ? instance.data.placeholder : '-',
      multiple: !!instance.data.multiple,
      dropdownAutoWidth: 'true',
      templateSelection: formatState,
      templateResult: formatStateResult,
      closeOnSelect: true,
      language: {
        noResults: function () {
          return translate.commonTranslation['no_result']
        }
      }
    }
  } else {
    options = {
      minimumResultsForSearch: 10,
      placeholder: instance.data.placeholder ? instance.data.placeholder : '-',
      multiple: !!instance.data.multiple,
      templateSelection: formatState2,
      templateResult: formatStateResult2,
      language: {
        noResults: function () {
          return translate.commonTranslation['no_result']
        }
      }
    }
  }
  selector.select2(options)
    .on('select2:unselecting', function (e) {
      // before removing tag we check option element of tag and
      // if it has property 'locked' we will create error to prevent all select2 functionality
      if ($(e.params.args.data.element).attr('locked')) {
        // e.select2.pleaseStop()
        return false
      }
      $(this).data('unselecting', true);
    })
    .on('select2:opening', function(e) {
      if ($(this).data('unselecting')) {
        $(this).removeData('unselecting');
        e.preventDefault();
      }
    })
  // $('b[role="presentation"]').hide()
  // $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>')
  if (instance.data.selected !== undefined) {
    let val = instance.data.selected
    if (!!instance.data.multiple) {
      val = _.compact(val)
    }
    selector.val(val)
    selector.trigger('change')
  }
}

Template.MbSelect.onRendered(function () {
  const instance = Template.instance()
  const id = instance.randomId
  const selector = $('#mb-select-' + id)
  selector.select2()
  setTimeout(() => {
    initializeMbSelect(selector, instance)
  }, 300)
})

Template.MbSelect.helpers({
  getLabel: () => {
    return Template.instance().data.label ? Template.instance().data.label : ''
  },
  getNoLabel: () => {
    return Template.instance().data.noLabel ? Template.instance().data.noLabel : false
  },
  getClass: () => {
    return Template.instance().data.class ? Template.instance().data.class : ''
  },
  isRequired: () => {
    return Template.instance().data.required
  },
  getRandomId: () => {
    return Template.instance().randomId
  },
  getOptions: () => {
    return Template.instance().data.options || []
  },
  getGroup: () => {
    return Template.instance().data.group || []
  },
  getOptionsGroup: (index) => {
    return Template.instance().data.options[index] || []
  },
  getMultiSelectClass: () => {
    return !!Template.instance().data.multiple ? 'mb-select2-multiple' : ''
  },
  setSelected: () => {
    // const id = Template.instance().randomId
    // const selector = $('#mb-select-' + id)
    // if (Template.instance().data.selected) {
    //     selector.val(Template.instance().data.selected)
    //     selector.trigger('change')
    // }
  },
  refresher: () => {
    const instance = Template.instance()
    const refresher = instance.data.refresher
    // change the if statement to if (refresh), when everthing will be crawling
    if (refresher) {
      const id = instance.randomId
      const selector = $('#mb-select-' + id)
      setTimeout(() => {
        if (!_.isEqual(instance.options, instance.data.options)) {
          instance.options = instance.data.options
          selector.select2("destroy")
          initializeMbSelect(selector, instance)
        } else {
          selector.select2()
          initializeMbSelect(selector, instance)
        }
      }, 300)
    }
  },
  haveHelp: () => {
    return !!Template.instance().data.help
  },
  getHelp: () => {
    return Template.instance().data.help
  },
  isLocked: (opt) => {
    return (!!opt && !!opt.locked) ? { locked: 'locked', disabled: 'disabled' } : {}
  },
  /* setPlaceholder: () => {
    const placeholder = Template.currentData().placeholder || '-'
    const id = Template.instance().randomId
    const selector = $('#mb-select-' + id)
    selector.select2({
      placeholder: placeholder
    })
  } */
})
