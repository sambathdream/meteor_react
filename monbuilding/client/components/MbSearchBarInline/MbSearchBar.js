Template.MbSearchBarInline.onCreated(function() {
  
})

Template.MbSearchBarInline.onRendered(function() {
  
})

Template.MbSearchBarInline.events({
  'input .text-search-bar': (event, template) => {
    let value = $(event.currentTarget).val().trim()
    if (typeof template.data.callback === 'function') {
        template.data.callback(value)
    }
  }
})

Template.MbSearchBarInline.helpers({
  
})
