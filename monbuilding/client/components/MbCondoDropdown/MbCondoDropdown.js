/* globals getRandomString */
/* globals FlowRouter */
import React, { Component } from 'react'
import MbCondoDropdownList from './MbCondoDropdownList.js'

import { CommonTranslation } from '/common/lang/lang'

/**
 * @param module
 * @param right
 * @param showAllBuilding
 * @param onSelectBuilding
 * @returns {XML}
 * @constructor
 */
export default class MbCondoDropdown extends Component {
  constructor (props) {
    super(props)
    const condoIdSelect = localStorage.getItem('condoIdSelected')
    let condoSelected = {
      condoId: condoIdSelect,
      name: localStorage.getItem('condoNameSelected')
    }
    if (!condoIdSelect) {
      localStorage.setItem('condoIdSelected', 'all')
      localStorage.setItem('condoNameSelected', 'Tous les immeubles')
      condoSelected = {
        condoId: 'all',
        name: 'Tous les immeubles'
      }
    }
    if (condoSelected.condoId !== 'all') {
      const isManagerCondo = Enterprises.findOne({
        'users': {
          $elemMatch: {
            userId: Meteor.userId(),
            'condosInCharge.condoId': condoSelected.condoId
          }
        }
      }, { fields: { _id: true } })
      if (!isManagerCondo) {
        localStorage.setItem('condoIdSelected', 'all')
        localStorage.setItem('condoNameSelected', 'Tous les immeubles')
        condoSelected = {
          condoId: 'all',
          name: 'Tous les immeubles'
        }
      }
    }
    this.state = {
      lang: FlowRouter.getParam('lang'),
      filter: '',
      show: false,
      buildingSelected: condoSelected,
      selectedCondoIds: !!props.selectedCondoIds? props.selectedCondoIds: []
    }
    if (this.state.buildingSelected && this.state.buildingSelected.condoId && props.onSelectBuilding && typeof props.onSelectBuilding === 'function') {
      props.onSelectBuilding(this.state.buildingSelected.condoId)
    }
    this.renderBuildingName = this.renderBuildingName.bind(this)
    this._onSelectBuilding = this._onSelectBuilding.bind(this)
  }

  componentDidUpdate () {
    if (this.state.lang !== FlowRouter.getParam('lang')) {
      this.setState({ lang: FlowRouter.getParam('lang') })
    }
  }

  renderBuildingName () {
    let { buildingSelected, selectedCondoIds } = this.state

    const commonTranslation = (new CommonTranslation(this.state.lang) || {}).commonTranslation
    let buildingName = <div />
    if (buildingSelected !== null) {
      buildingName = !this.props.multiple? buildingSelected.name: (selectedCondoIds.length > 0 ? `${selectedCondoIds.length} ${commonTranslation.building_selected}` : '')
    } else {
      buildingName = commonTranslation.all_buildings
    }
    return buildingName
  }

  _onSelectBuilding (condo) {
    let { onSelectBuilding, multiple } = this.props
    if (!multiple) {
      this.setState({ show: false, filter: '', buildingSelected: condo })
      localStorage.setItem('condoIdSelected', condo.condoId);
      localStorage.setItem('condoNameSelected', condo.name);
      if (onSelectBuilding && typeof onSelectBuilding === 'function') {
        onSelectBuilding(condo.condoId)
      }
    } else {
      const newState = !!this.state.selectedCondoIds.includes(condo.condoId) ?
        this.state.selectedCondoIds.filter(c => c !== condo.condoId):
        [...this.state.selectedCondoIds, condo.condoId]
      this.setState({
        selectedCondoIds: newState
      }, () => {
        if (onSelectBuilding && typeof onSelectBuilding === 'function') {
          onSelectBuilding(newState)
        }
      })
    }
  }

  render () {
    const { lang, show, filter, selectedCondoIds } = this.state
    let { module, right, showAllBuilding, fullWidth, multiple } = this.props

    let condoIdsWithRight = null

    if (module && right) {
      condoIdsWithRight = Meteor.listCondoUserHasRight(module, right)
    }
    return (
      <div className={`mb-condodropdown mb-condodropdown-${getRandomString()} ${!!multiple? 'is-multiple': ''}`} style={!!fullWidth? {width: '100%'}: {}}>
        <span className={'mb-condodropdown-trigger-container'} onClick={() => this.setState({ show: !show, filter: '' })}>
          {!multiple && <img src={'/img/icons/building.png'} style={{ width: '20px', height: '20px', marginRight: '8px', marginTop: '9px' }} />}
          <div className={'mb-condodropdown-trigger-label'}>
            {this.renderBuildingName()}
          </div>
          <span className={'mb-condodropdown-trigger-caret'} />
        </span>
        {
          show === true &&
          <MbCondoDropdownList
            lang={lang}
            filter={filter}
            setFilter={(_filter) => {
              this.setState({ filter: _filter })
            }}
            closeDropdown={() => {
              this.setState({ show: false })
            }}
            condoIds={condoIdsWithRight}
            onSelectBuilding={condo => this._onSelectBuilding(condo)}
            showAllBuilding={!multiple && showAllBuilding}
            fullWidth={fullWidth}
            multiple={multiple}
            selectedCondoIds={selectedCondoIds}
          />
          }
      </div>
    )
  }
}
