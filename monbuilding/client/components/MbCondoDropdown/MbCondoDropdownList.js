import React, { Component } from 'react'
import MbSearchBarReact from '/client/components/MbSearchBarReact/MbSearchBarReact.js'
import MbCondoDropdownListItem from './MbCondoDropdownListItem.js'

import { CommonTranslation } from '/common/lang/lang'

import { withTracker } from 'meteor/react-meteor-data'

/**
 * @param lang
 * @param onSelectBuilding
 * @returns {XML}
 * @constructor
 */
class MbCondoDropdownList extends Component {
  constructor (props) {
    super(props)
    this.setSearchValue = this.setSearchValue.bind(this)
    this.resetDropdown = this.resetDropdown.bind(this)
  }

  setSearchValue (value) {
    const { setFilter } = this.props

    if (setFilter && typeof setFilter === 'function') {
      setFilter(value)
    }
  }

  renderEmptyData () {
    let { commonTranslation, filter } = this.props

    if (filter !== '') {
      return (
        <div className='mb-condodropdown-list-wrap'>
          <MbCondoDropdownListItem
            condoId={'all'}
            title={commonTranslation.no_output}
          />
        </div >
      )
    } else {
      return (
        <div className='mb-condodropdown-list-wrap'>
          <MbCondoDropdownListItem
            condoId={'all'}
            title={commonTranslation.no_result}
          />
        </div>
      )
    }
  }

  renderList () {
    const { commonTranslation, showAllBuilding, multiple, selectedCondoIds } = this.props
    // Props from meteor collection:
    const { condosList, condosLength, onSelectBuilding } = this.props
    const selectedCondo = localStorage.getItem('condoIdSelected')

    return (
      <div className='mb-condodropdown-list-wrap'>
        {
          showAllBuilding !== false &&
          <MbCondoDropdownListItem
            condoId={'all'}
            title={commonTranslation.all_buildings}
            subtitle={`${condosLength} ${commonTranslation.profile_details_buildings}`}
            onSelectBuilding={(condo) => onSelectBuilding(condo)}
            isSelected={selectedCondo === 'all'}
          />
        }
        {
          condosList && condosList.map((condoItem, index) =>
            <MbCondoDropdownListItem
              key={index}
              condoId={condoItem.id}
              title={condoItem.title}
              subtitle={condoItem.subtitle}
              onSelectBuilding={(condo) => onSelectBuilding(condo)}
              isSelected={!multiple ?
                selectedCondo === condoItem.id :
                selectedCondoIds.includes(condoItem.id)
              }
              multiple={multiple}
            />
          )
        }
      </div>
    )
  }

  resetDropdown (e) {
    const myElementToCheckIfClicksAreInsideOf = document.querySelector('.mb-condodropdown-list');
    if (!myElementToCheckIfClicksAreInsideOf.contains(e.target)) {
      this.props.closeDropdown()
    }

  }

  componentDidMount () {
    document.addEventListener('click', this.resetDropdown)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.resetDropdown)
  }

  render () {
    const { commonTranslation } = this.props
    // Props from meteor collection:
    const { condosLength, fullWidth } = this.props

    return (
      <div className={'mb-condodropdown-list'} style={!!fullWidth? {width: '100%'}: {}}>
        <MbSearchBarReact
          placehoder={commonTranslation.search}
          containerStyle={'mb-condodropdown-list-search'}
          onFilterChange={(value) => this.setSearchValue(value)}
        />
        { condosLength === 0 && this.renderEmptyData()}
        { condosLength > 0 && this.renderList() }
      </div>
    )
  }
}

export default MbCondoDropdownList = withTracker((props) => {
  const commonTranslation = (new CommonTranslation(props.lang) || {}).commonTranslation
  let condos = null
  let regexp = new RegExp(props.filter, "i");
  if (props.condoIds) {
    condos = Condos.find({ _id: { $in: props.condoIds } }, { fields: { _id: true, name: true, info: true } })
  } else {
    condos = Condos.find({}, { fields: { _id: true, name: true, info: true } })
  }
  condos = condos.fetch().filter((condo) => {
    const condoName = condo.getNameWithId()
    return condo.name.match(regexp) ||
      condo.info.address.match(regexp) ||
      condo.info.code.match(regexp) ||
      condo.info.city.match(regexp) ||
      condo.info.id.match(regexp) ||
      condoName.match(regexp)
  })
  const condosList = condos.map(condo => ({
    id: condo._id,
    title: condo.getNameWithId(),
    subtitle: condo.name !== condo.info.address ? `${condo.info.address}, ${condo.info.code} ${condo.info.city}` : null
  }))

  return {
    commonTranslation,
    condos: condos,
    condosList,
    condosLength: condos.length
  }
})(MbCondoDropdownList)
