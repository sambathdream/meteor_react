import React, { Component } from 'react'

/**
 * @param id
 * @param name
 * @param buildingSelected
 * @param onSelectBuilding
 * @returns {XML}
 * @constructor
 */
export default class MbCondoDropdownListItem extends Component {
  constructor (props) {
    super(props)
    this._onSelectBuilding = this._onSelectBuilding.bind(this)
  }

  _onSelectBuilding () {
    const { onSelectBuilding, condoId, title, subtitle } = this.props
    if (onSelectBuilding && typeof onSelectBuilding === 'function') {
      onSelectBuilding({
        condoId,
        name: title,
        address: subtitle
      })
    }
  }

  render () {
    const { title, subtitle, isSelected, multiple } = this.props
    return (
      <div className='mb-condodropdown-list-item' onClick={() => this._onSelectBuilding()}>
        { !multiple && isSelected &&
          <div className='mb-condodropdown-list-item-selected'></div>
        }
        <div className='mb-condodropdown-list-item-img'>
          <img src={'/img/icons/building.png'} />
        </div>
        <div className='mb-condodropdown-list-item-label'>
          <h5>{title}</h5>
          { subtitle && subtitle !== '' &&
            <p>{subtitle}</p>
          }
        </div>
        {
          !!multiple && <div className="mb-checkbox">
            <div className="checkboxred">
              <input type="checkbox" checked={isSelected} />
                <label/><p/>
            </div>
          </div>
        }
      </div>
    )
  }
}
