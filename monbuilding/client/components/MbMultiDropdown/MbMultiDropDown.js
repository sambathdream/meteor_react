/* globals getRandomString */
/* globals FlowRouter */
import React, { Component } from 'react'
import MbMultiDropDownList from './MbMultiDropDownList'
import { CommonTranslation } from '/common/lang/lang'
/**
 * @param onSelectItem
 * @returns {XML}
 * @constructor
 */
export default class MbMultiDropDown extends Component {
  constructor (props) {
    super(props)
    this.state = {
      lang: FlowRouter.getParam('lang'),
      filter: '',
      show: false,
      selectedUserIds: !!props.selectedUserIds? props.selectedUserIds: []
    }
    this._onSelectItem= this._onSelectItem.bind(this)
  }
  componentDidUpdate () {
    if (this.state.lang !== FlowRouter.getParam('lang')) {
      this.setState({ lang: FlowRouter.getParam('lang') })
    }
  }
  _onSelectItem (user) {
    let { onSelectItem , multiple} = this.props
    if(multiple){
      const newState = !!this.state.selectedUserIds.includes(user._id) ?
      this.state.selectedUserIds.filter(id => id !== user._id):
      [...this.state.selectedUserIds, user._id]
      this.setState({
        selectedUserIds: newState
      }, () => {
        if (this.state.selectedUserIds && typeof onSelectItem === 'function') {
          onSelectItem(newState)
        }
      })
    }else{
      this.setState({
        selectedUserIds: [user._id]
      }, () =>{
        if (this.state.selectedUserIds && typeof onSelectItem === 'function') {
          onSelectItem(user._id)
        }
      })
      console.log(this.state.selectedUserIds)
    }
  }
  renderSelectedUser () {
    let { selectedUserIds} = this.state
    let {multiple, defaultSelect, disabled} = this.props;
    let selectedUsers = [];
    const commonTranslation = (new CommonTranslation(this.state.lang) || {}).commonTranslation
    if(selectedUserIds.length >0 ){
      selectedUserIds.map((userId) =>{
        let user = Meteor.users.findOne({_id: userId})
        if(multiple){
          if(user !== undefined  && user._id !==undefined){
            selectedUsers.push(user)
          }
        }
        else{
          if(user !== undefined  && user._id !==undefined){
            !selectedUsers.length?selectedUsers.push(user):Object.assign(selectedUsers, [user])
          }
        }
      })
      return(
        <ul>
          {
            selectedUsers.map((user, index) =>(
              <li key={index} className={`mb-userdropdown-selected-user` }>
                <span className={`mb-user-name`}>{user.profile.firstname+' '+ user.profile.lastname}</span>
              </li>
            ))
          }
        </ul>
      )
    }else{
      return (
        <div className={`${disabled?`grey-text`: ``}`}>
          {commonTranslation.select_default+(!String(defaultSelect).indexOf('default')||!String(defaultSelect).indexOf('false')?'':' for '+defaultSelect)}
        </div>
      )
    }
  }
  render () {
    const { lang, show, filter, selectedUserIds } = this.state
    const {fullWidth, options, multiple, disabled}  = this.props
    return (
      <div className={`mb-userdropdown mb-userdropdown-${getRandomString()}`} style={!!fullWidth? {width: '100%'}: {}} >
        <span className={'mb-userdropdown-trigger-container'} onClick={() => this.setState({ show: !show, filter: '' })}>
          <div className={'mb-userdropdown-trigger-label'}>
            {this.renderSelectedUser()}
          </div>
          <span className={'mb-userdropdown-trigger-caret'} />
        </span>
        {
          !disabled && show === true &&
          <MbMultiDropDownList
            lang={lang}
            filter={filter}
            setFilter={(_filter) => {
              this.setState({ filter: _filter })
            }}
            closeDropdown={() => {
              this.setState({ show: false })
            }}
            onSelectItem={item => this._onSelectItem(item)}
            fullWidth={fullWidth}
            options={options}
            multiple = {multiple}
            selectedUserIds = {selectedUserIds}
          />
          }
      </div>
    )
  }
}
