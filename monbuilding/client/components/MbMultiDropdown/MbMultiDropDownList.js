import React, { Component } from 'react'
import MbSearchBarReact from '/client/components/MbSearchBarReact/MbSearchBarReact.js'
import MbMultiDropDownListItem from './MbMultiDropDownListItem'

import { CommonTranslation } from '/common/lang/lang'

import { withTracker } from 'meteor/react-meteor-data'

/**
 * @param lang
 * @param onSelectItem
 * @returns {XML}
 * @constructor
 */
class MbMultiDropDownList extends Component {
  constructor (props) {
    super(props)
    this.setSearchValue = this.setSearchValue.bind(this)
    this.resetDropdown = this.resetDropdown.bind(this)
  }

  setSearchValue (value) {
    const { setFilter } = this.props

    if (setFilter && typeof setFilter === 'function') {
      setFilter(value)
    }
  }

  renderEmptyData () {
    let { commonTranslation, filter } = this.props

    if (filter !== '') {
      return (
        <div className='mb-userdropdown-list-wrap'>
          <MbMultiDropDownListItem
            userId={'all'}
            name={commonTranslation.no_output}
          />
        </div >
      )
    } else {
      return (
        <div className='mb-userdropdown-list-wrap'>
          <MbCondoDropdownListItem
            userId={'all'}
            name={commonTranslation.no_result}
          />
        </div>
      )
    }
  }

  renderList () {
    const { selectedUserIds } = this.props
    // Props from meteor collection:
    const {onSelectItem , users , multiple} = this.props
    return (
      <div className='mb-userdropdown-list-wrap'>
        {
          users && users.map((user, index) =>
            <MbMultiDropDownListItem
              key={index}
              userId={user.id}
              name={user.name}
              onSelectItem={(user) => onSelectItem(user)}
              isSelected={!multiple?
                selectedUserIds[0] == user.id:
                selectedUserIds.includes(user.id)
              }
            />
          )
        }
      </div>
    )
  }

  resetDropdown (e) {
    const myElementToCheckIfClicksAreInsideOf = document.querySelector('.mb-userdropdown-list');
    if (!myElementToCheckIfClicksAreInsideOf.contains(e.target)) {
      this.props.closeDropdown()
    }

  }
  componentDidMount () {
    document.addEventListener('click', this.resetDropdown)
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.resetDropdown)
  }

  render () {
    const { commonTranslation } = this.props
    // Props from meteor collection:
    const { usersLength, fullWidth } = this.props

    return (
      <div className={'mb-userdropdown-list'} style={!!fullWidth? {width: '100%'}: {}}>
        <MbSearchBarReact
          placehoder={commonTranslation.search}
          containerStyle={'mb-userdropdown-list-search'}
          onFilterChange={(value) => this.setSearchValue(value)}
        />
        { usersLength === 0 && this.renderEmptyData()}
        { usersLength > 0 && this.renderList() }
      </div>
    )
  }
}

export default MbMultiDropDownList = withTracker((props) => {
  const commonTranslation = (new CommonTranslation(props.lang) || {}).commonTranslation
  let regexp = new RegExp(props.filter, "i");
  let users = props.options
  users = users.fetch().filter((user) => {
    return user.profile.firstname.match(regexp) ||
      user.profile.lastname.match(regexp) ||
      user.profile.role.match(regexp)
  })
  users = users.map(user => 
    ({
      id: user._id,
      name: `${user.profile.firstname}`+' '+`${user.profile.lastname}`,
    }))
  return {
    commonTranslation,
    users: users,
    usersLength: users.length
  }
})(MbMultiDropDownList)
