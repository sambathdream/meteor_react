import React, { Component } from 'react'

/**
 * @param id
 * @param name
 * @param ItemSelected
 * @param onSelectItem
 * @returns {XML}
 * @constructor
 */
export default class MbMultiDropDownListItem extends Component {
  constructor (props) {
    super(props)
    this._onSelectItem = this._onSelectItem.bind(this)
    this.defaultAvatar = "/img/anonymous.png";
  }

  _onSelectItem () {
    const { onSelectItem, userId, name } = this.props
    if (onSelectItem && typeof onSelectItem === 'function') {
      onSelectItem({
        _id: userId,
        name: name,
      })
    }
  }

  render () {
    const { userId, name, isSelected } = this.props
    const profilePicture = Avatars.findOne({userId: userId}); // get User's avatar URL.
    return (
      <div className='mb-userdropdown-list-item' onClick={() => this._onSelectItem()}>
        { isSelected &&
          <div className='mb-userdropdown-list-item-selected'></div>
        }
        {
          <div className='mb-userdropdown-list-item-img'>
            {
              profilePicture !== undefined  && profilePicture.avatar.original?<img src={profilePicture.avatar.original} />:<img src={this.defaultAvatar} />
            }
          </div>
        }
          <div className='mb-userdropdown-list-item-label'>
            <h5>{name}</h5>
          </div>
        {
          <div className="mb-checkbox">
            <div className="checkboxred">
              <input type="checkbox" checked={isSelected} />
                <label/><p/>
            </div>
          </div>
        }
      </div>
    )
  }
}
