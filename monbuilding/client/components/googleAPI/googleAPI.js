export class GoogleAPI {
  constructor() {
    this.input = "";
    this.baseURL = "https://maps.google.com/maps/api/geocode/json?components=country:FR";
    this.APIURL = "https://maps.google.com/maps/api/geocode/json?key=AIzaSyDqeZGlZB6f48uiz0uM0wpMnlgz_ZOfgmw"
    this.results = new ReactiveArray([]);
    this.search = new ReactiveVar(false);
  }

  find_property(tabl, prop) {
    for (elem of tabl) {
      if (elem.types.indexOf(prop) != -1)
        return elem.long_name;
    }
    return undefined;
  }

  updateResult(url) {
    this.search.set(true);
    this.results.clear();
    let self = this;
    $.getJSON(url, function(data) {
      for (res of data.results) {
        let val = {
          street_number: self.find_property(res.address_components, "street_number"),
          road: self.find_property(res.address_components, "route"),
          city: self.find_property(res.address_components, "locality"),
          country: self.find_property(res.address_components, "country"),
          code: self.find_property(res.address_components, "postal_code"),
          address: res.formatted_address,
          lat: res.geometry.location.lat,
          lng: res.geometry.location.lng,
          id: res.place_id
        }
        if (val.street_number != undefined &&
            val.road != undefined &&
            val.city != undefined &&
            val.code != undefined)
          self.results.push(val);
      }
      self.search.set(false);
    });
  }

  findAddress(string) {
    var url = this.baseURL + "&address=" + encodeURI(string);
    this.updateResult(url);
  }

  findPlaceId(placeid) {
    var url = this.APIURL + "&place_id=" + encodeURI(placeid);
    this.updateResult(url);
  }

  getResults() {
    return this.results.list();
  }

  isSearching() {
    return this.search.get();
  }
}
