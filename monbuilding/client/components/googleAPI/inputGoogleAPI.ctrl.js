import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var';

import { GoogleAPI } from './googleAPI.js';

import "./inputGoogleAPI.view.html";

Template.inputGoogleAPI.onCreated(function () {
  this.googleAPI = new GoogleAPI();
  this.focus = new ReactiveVar(false);
  this.maxResult = new ReactiveVar(10);
  this.arrowIndex = new ReactiveVar(-1);
});

Template.inputGoogleAPI.onRendered(function () {
});

Template.inputGoogleAPI.helpers({
  showMoreResults: () => {
    return Template.instance().googleAPI.getResults().length > Template.instance().maxResult.get() && Template.instance().maxResult.get();
  },
  getAllResultsLen: () => {
    return Template.instance().googleAPI.getResults().length;
  },
  getResults: () => {
    return Template.instance().googleAPI.getResults().slice(0, Template.instance().maxResult.get());
  },
  getResultsNumber: () => {
    return Template.instance().googleAPI.getResults().length -
    Template.instance().maxResult.get();
  },
  focus: () => {
    return Template.instance().focus.get();
  },
  search: () => {
    return Template.instance().googleAPI.isSearching();
  },
  somethingSearched: () => {
    return $('#addressInput').val() ? ($('#addressInput').val()).length > 1 : false;
  }
});

Template.inputGoogleAPI.events({
  'keyup #addressInput': function (e, template) {
    let index = 0;
    if (template.googleAPI.getResults().length) {
      switch (e.which) {
        case 13: //Enter
          index = template.arrowIndex.get() != -1 ? template.arrowIndex.get() : 0;
          $('#addressInput').val(template.googleAPI.getResults()[index].address);
          template.maxResult.set(0);
          template.arrowIndex.set(-1);
          template.data.callback(template.googleAPI.getResults()[index].address);
          break;
        case 38:// Arrow Up
          template.arrowIndex.set(template.arrowIndex.get() == -1 || !template.arrowIndex.get()
          ? (template.googleAPI.getResults()[template.maxResult.get() - 1] ? template.maxResult.get() - 1 : template.googleAPI.getResults().length - 1) : template.arrowIndex.get() - 1);
          $('#addressInput').val(template.googleAPI.getResults()[template.arrowIndex.get()].address);
          break;
        case 40:// Arrow Down
        template.arrowIndex.set(template.arrowIndex.get() == (template.googleAPI.getResults()[template.maxResult.get() - 1] ? template.maxResult.get() - 1 : template.googleAPI.getResults().length - 1)
        ? 0 : template.arrowIndex.get() + 1);
        $('#addressInput').val(template.googleAPI.getResults()[template.arrowIndex.get()].address);
          break;
        default:
          template.arrowIndex.set(-1);
          template.maxResult.set(10);
          break;
      }
    }
    if (template.arrowIndex.get() == -1)
      template.googleAPI.findAddress(e.currentTarget.value);
  },
  'focus #addressInput': function (e, template) {
    template.focus.set(true);
    template.maxResult.set(10);
  },
  'blur #addressInput': function (e, template) {
    template.focus.set(false);
    template.maxResult.set(0);
    template.arrowIndex.set(-1);
  },
  'mousedown .resultElem': function (e, template) {
    let idx = parseInt(e.currentTarget.getAttribute("index"));
    $('#addressInput').val(template.googleAPI.getResults()[idx].address);
    template.maxResult.set(0);
    template.arrowIndex.set(-1);
    template.data.callback(template.googleAPI.getResults()[idx]);
  },
  'click .moreResult': function (e, template) {
    template.maxResult.set(template.maxResult.get() +
    (template.googleAPI.getResults().length - template.maxResult.get() > 10
    ? 10 : template.googleAPI.getResults().length - template.maxResult.get()));
  },
});
