/**
 * Created by kuyarawa on 8/2/17.
 */
 import { Template } from 'meteor/templating'
 import { ReactiveVar } from 'meteor/reactive-var';
 import { GoogleApiError } from "/common/lang/lang.js";

 import "./placeSearchBox.view.html";

 let autocomplete, thePlace;

 Template.placeSearchBox.onCreated(function () {
    this.place = new ReactiveVar();

    this.address = Template.currentData().address;
});

 Template.placeSearchBox.onRendered(function () {
    initAutocomplete();
});


 Template.placeSearchBox.helpers({
    getAddress: () => {
        return JSON.stringify(Template.instance().place.get());
    },
});

 Template.placeSearchBox.events({
    'focus #place-auto-complete': (e, t) => {
        // FOR THE MODAL DISPLAY
        $('.pac-container').css({'z-index': 1051});

        geolocate();
    },
    'submit #update-value': (e, t) => {
        e.preventDefault();
        t.data.callback.set(thePlace);
        t.place.set(thePlace);
    }
});

 function initAutocomplete() {
    // Create the autocomplete object, restricting the search to geographical
    // location types.
    autocomplete = new google.maps.places.Autocomplete(
        /** @type {!HTMLInputElement} */(document.getElementById('place-auto-complete')),
        {types: ['geocode']});
    // When the user selects an address from the dropdown, populate the address
    // fields in the debugger.
    autocomplete.addListener('place_changed', getAddress);
}

function find_property(tabl, prop) {
    for (elem of tabl) {
        if (elem.types.indexOf(prop) !== -1)
            return elem.long_name;
    }
    return undefined;
}

function checkThisPlace(place) {
    let translation = new GoogleApiError((FlowRouter.getParam("lang") || "fr"));
    if (place == undefined || !place.formatted_address)
        return translation.googleApiError["error1"];
    if (!find_property(place.address_components, "street_number"))
        return translation.googleApiError["error2"];
    if (!find_property(place.address_components, "route"))
        return translation.googleApiError["error3"];
    if (!find_property(place.address_components, "locality"))
        return translation.googleApiError["error4"];
    if (!find_property(place.address_components, "country"))
        return translation.googleApiError["error5"];
    if (!find_property(place.address_components, "postal_code"))
        return translation.googleApiError["error6"];
    if (!place.geometry.location.lat())
        return translation.googleApiError["error7"];
    if (!place.geometry.location.lng())
        return translation.googleApiError["error7"];
    if (!place.place_id)
        return translation.googleApiError["error7"];
    return true;
}

function getAddress(t) {
    // Get the place details from the autocomplete object.
    let place = autocomplete.getPlace();
    if (checkThisPlace(place) == true) {
        thePlace = {
            street_number: find_property(place.address_components, "street_number"),
            road: find_property(place.address_components, "route"),
            city: find_property(place.address_components, "locality"),
            country: find_property(place.address_components, "country"),
            code: find_property(place.address_components, "postal_code"),
            address: place.formatted_address,
            lat: place.geometry.location.lat(),
            lng: place.geometry.location.lng(),
            id: place.place_id
        };
    }
    else {
        thePlace = {error: checkThisPlace(place)};
    }
    $('#update-value').submit();
}

// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            let geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            let circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}