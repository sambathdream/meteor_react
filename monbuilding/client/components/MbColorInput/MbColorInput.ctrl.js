/**
 * Created by kuyarawa on 06/05/18.
 */
Template.MbColorInput.onCreated(function() {
  this.randomId = getRandomString();
});

Template.MbColorInput.onRendered(function() {
  const instance = Template.instance();
  const id = instance.randomId;
  const selector = $('#mb-color-input-' + id);
  selector.spectrum({
    preferredFormat: 'hex',
    showInput: true,
    change: (val) => {
      instance.data.onChange(val.toHex());
    }
  })
});

Template.MbColorInput.helpers({
  getLabel: () => {
    return Template.instance().data.label ? Template.instance().data.label : '';
  },
  getClass: () => {
    return Template.instance().data.class ? Template.instance().data.class : '';
  },
  getRandomId: () => {
    return Template.instance().randomId;
  },
  getValue: () => {
    return Template.instance().data.value ? Template.instance().data.value : '';
  },
  setValue: () => {
    const instance = Template.instance()
    if (instance.data.value) {
      $('#mb-color-input-' + instance.randomId).spectrum("set", instance.data.value);
    }
  }
});
