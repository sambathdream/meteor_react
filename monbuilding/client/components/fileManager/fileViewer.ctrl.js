import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var';
import mime from 'mime-types'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';

import "./fileViewer.view.html";
import "./fileViewer.css";

Template.fileViewer.onCreated(function () {
    this.selectedTab = new ReactiveVar("images");
    this.randomId = new ReactiveVar("");
});

function makeid() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (let i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function addLinks() {
    let list = $("#links");

    if (!list.length) {
        list = $('<ul id="links">');

        $('<li></li>').click(function() { $.fancybox.prev();}).appendTo( list );
        for (var i = 0; i < this.group.length; i++) {
            $('<li data-index="' + i + '"><label></label></li>').click(function() { $.fancybox.jumpto( $(this).data('index'));}).appendTo( list );
        }
        $('<li></li>').click(function() { $.fancybox.next();}).appendTo( list );

        list.appendTo( 'body' );
    }

    list.find('li').removeClass('active').eq( this.index + 1 ).addClass('active');
}

function removeLinks() {
    $("#links").remove();
}

function initFlexslider(id) {
    let preview = $('#preview-' + id),
        scroll = $('#scroll-' + id);

    scroll.removeData("flexslider");
    preview.removeData("flexslider");

    scroll.flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 98,
        itemMargin: 12,
        asNavFor: '#preview-' + id
    });

    preview.flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: '#scroll-' + id
    });
}

Template.fileViewer.onRendered(function () {
    Template.instance().randomId.set(makeid());
    let id = Template.instance().randomId.get();

    setTimeout(function() {
        initFlexslider(id);

        this.$(".fancybox").fancybox({
            margin      : [15, 15, 60, 15],
            openEffect  : 'none',
            closeEffect : 'none',
            nextEffect  : 'none',
            prevEffect  : 'none',
            padding     : 0,
            arrows      : false,
            // margin      : [20, 60, 20, 60],
            afterLoad   : addLinks,
            beforeClose : removeLinks,

            beforeLoad: function() {
                this.title = $(this.element).data('title');
            }
        });
    }, 1000);
});

Template.fileViewer.helpers({
    selectedTab: () => {
        return (Template.instance().selectedTab.get());
    },
    invokeInit: () => {
        let id = Template.instance().randomId.get();
        setTimeout(function() {
            initFlexslider(id);
        }, 1000);
    },
    randomId: () => {
        return (Template.instance().randomId.get());
    },
    generateRandomId: () => {
        Template.instance().randomId.set(makeid());
    },
    getImageList: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] === "image"});
    },
    getFileList: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] !== "image"});
    },
    getFiles: () => {
      // console.log("files : ", Template.instance().data.files.fetch())
      return (Template.instance().data.files)
    },
    getNewFiles: () => {
      return (Template.instance().data.newFiles)
    },
    getImageByExtension: (type) => {
        const ext = mime.extension(type)

        switch (ext) {
            case 'zip':
                return 'extensionzip.svg'
            case 'docx':
            case 'doc':
                return 'extensiondoc.svg'
            case 'pptx':
            case 'ppt':
                return 'extensionppt.svg'
            case 'xls':
            case 'xlsx':
                return 'extensionxls.svg'
            case 'pdf':
                return 'extensionpdf.svg'
            case 'txt':
                return 'extensiontxt.svg'
            default:
                return 'extensionunknown.svg'
        }
    },
    getIconOfFile: (type) => {
        let arrType = type.split("/");
        if (arrType[0] == "application") {
            if (arrType[1] == "pdf")
                return "pdf.jpg";
            let appType = arrType[1].split(".");
            if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
                if (appType[2] == "presentation" || appType[2] == "presentationml")
                    return "ppt.jpg"
                if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
                    return "xlsx.jpg"
                if (appType[2] == "wordprocessingml" || appType[2] == "document")
                    return "docx.jpg"
            }
        }
        if (arrType[0] == "text")
            return "txt.jpg"
        return "file.png"
    },
    hasFiles: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] !== "image"}).length;
    },
    MbFilePreview: () => MbFilePreview,
});



Template.fileViewer.events({



    'click .tab' : function (e, template) {
        let val = e.currentTarget.getAttribute("tabName");
        template.selectedTab.set(val);
    }
});

Template.messengerFileViewer.onCreated(function () {
    this.selectedTab = new ReactiveVar("images");
});

Template.messengerFileViewer.helpers({
    selectedTab: () => {
        return (Template.instance().selectedTab.get());
    },
    getImageList: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] === "image"});
    },
    getFileList: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] !== "image"});
    },
    getImageByExtension: (type) => {
        const ext = mime.extension(type)

        switch (ext) {
            case 'zip':
                return 'extensionzip.svg'
            case 'docx':
            case 'doc':
                return 'extensiondoc.svg'
            case 'pptx':
            case 'ppt':
                return 'extensionppt.svg'
            case 'xls':
            case 'xlsx':
                return 'extensionxls.svg'
            case 'pdf':
                return 'extensionpdf.svg'
            case 'txt':
                return 'extensiontxt.svg'
            default:
                return 'extensionunknown.svg'
        }
    },
    getIconOfFile: (type) => {
        let arrType = type.split("/");
        if (arrType[0] == "application") {
            if (arrType[1] == "pdf")
                return "pdf.jpg";
            let appType = arrType[1].split(".");
            if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
                if (appType[2] == "presentation" || appType[2] == "presentationml")
                    return "ppt.jpg"
                if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
                    return "xlsx.jpg"
                if (appType[2] == "wordprocessingml" || appType[2] == "document")
                    return "docx.jpg"
            }
        }
        if (arrType[0] == "text")
            return "txt.jpg"
        return "file.png"
    },
    hasFiles: () => {
        return _.filter(Template.instance().data.files.each(),
            function (f) {return f.type.split("/")[0] !== "image"}).length;
    },
    hasImages: () => {
      return _.filter(Template.instance().data.files.each(),
          function (f) {return f.type.split("/")[0] == "image"}).length;
    },
});

Template.messengerFileViewer.events({
    'click .tab' : function (e, template) {
        let val = e.currentTarget.getAttribute("tabName");
        template.selectedTab.set(val);
    }
});

let index=1;

Template.firstConnectCaroussel.events({
    'click #next' : function (e, t) {


        if(index !=2){
            $('#prev').removeClass('nextHide');
        }
        if(index == 10){
            $('#next').addClass('nextHide');
        }
        if(index<11){
            console.log(index);
            let fleche = '#fleche'+index;
            let rond = '#rond'+index;
            let id='#car_' + index;
            $(id).toggleClass('visible hidden');
            $(rond).toggleClass('redActive rond');
            $(fleche).toggleClass('flecheActive fleche');
            console.log(rond);
            index++;
            id='#car_' + index;
            rond = '#rond'+index;
            fleche = '#fleche'+index;
            $(id).toggleClass('visible hidden');
            $(rond).toggleClass('redActive rond');
            $(fleche).toggleClass('flecheActive fleche');
        }
    },

    'click #prev' : function (e, t) {
        if(index != 10){
            $('#next').removeClass('nextHide');
        }
        if(index == 2){
            $('#prev').addClass('nextHide');
        }
        if(index>1){
            console.log(index);
            let fleche = '#fleche'+index;
            let rond = '#rond'+index;
            let id='#car_' + index;
            $(id).toggleClass('visible hidden');
            $(rond).toggleClass('redActive rond');
            $(fleche).toggleClass('flecheActive fleche');
            console.log(rond);
            index--;
            id='#car_' + index;
            rond = '#rond'+index;
            fleche = '#fleche'+index;
            $(id).toggleClass('visible hidden');
            $(rond).toggleClass('redActive rond');
            $(fleche).toggleClass('flecheActive fleche');
        }
    },

    'click .supFirst' : function (e, t) {
        $('.firstConnectContent').css('display','none');
    }
});
