import { FileManager } from "./../fileManager/filemanager.js";

/* APPEL DU MESSAGE INPUT*/
/*    {{> textAreaFileUpload fileManager= objMessage= name= }}*/
/*    fileManager: un objet de type FileManager*/
/*    objMessage: un objet de type {message: String, files: [Ids]}*/
/*    activeCB: callback, avec pour argument un boolean indiquant si le template contient un msg ou un fichier*/
/*    name: pour différencier deux messageInput sur une meme page*/


var index = 0;

function call_callback(self) {
	self.cb(self.fm.getFileList().length !== 0 || self.msg.value.length !== 0);
}

function constructAttachmentElement(data)  {
	var progress = $('<span></span>')
	.addClass('progress')
	.css('width', '0%')

	var meter = $('<div></div>')
	.addClass('meter')
	.append(progress)

	var iconFile = $('<span></span>')
	.addClass('icon-file')
	.append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'))

	var checkmark = $('<span class="checkmark"></span>')
	.append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'))

	var fileSize = $('<span class="file-size"></span>')
	.html(data.size)
	.append(checkmark)

	var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name)

	var fileRemove = $(`<span class="remove-item" index="${index}">`)
	.append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'))
	index += 1;

	var file = $('<div></div>')
	.addClass('file')
	.append(iconFile)
	.append(fileName)
	.append(fileSize)
	.append(fileRemove)
	.append(meter)

	var list = $('<li></li>')
	.attr('id', data.id)
	.addClass('attachment-item')
	.append(file)

	return list;
}


function autoGrow() {
	if ($("#answer").height() < document.getElementById("answer").scrollHeight - 16) {
		$("#answer").height(document.getElementById("answer").scrollHeight);
		$('#chat-window').scrollTop($('#answer')[0].scrollHeight - 60);
	}
}

function autogrowByName() {
	let name = Template.instance().name;
	let selector = $(".autogrow[name='"+name+"']");
	if (selector.height() < selector[0].scrollHeight - 16) {
		selector.height(selector[0].scrollHeight);
		$('.grow-class.'+name).scrollTop(selector[0].scrollHeight - 60);
	}
}

Template.textAreaFileUpload.onCreated(function() {
	this.fm = this.data.params.fileManager;
	this.msg = this.data.params.message;
	this.cb = this.data.params.activeCB;
	this.name = this.data.params.name;
});

Template.textAreaFileUpload.events({
	'click .attachment-icon-text-area' (e, t) {
		var fileInput = t.$('#fileUploadInput')
		fileInput.click();
	},
	'change #fileUploadInput' (e, t) {
		var attachmentListUL = t.$('#attachments');
		if (e.currentTarget.files) {
			for (let i = 0; i < e.currentTarget.files.length; i++) {
				var data = {};
				let error = false;
				let id = t.fm.insert(e.currentTarget.files[i], (err, fileObj) => {
					if (err) {
						error = true;
						return sAlert.error(err);
					}
				});

				data.name = e.currentTarget.files[i].name;
				data.size = (e.currentTarget.files[i].size / 1000).toFixed(2) + ' kb';
				data.id = 'file-' + id;

				attachmentListUL.append(constructAttachmentElement(data));

				var progress = t.$('#file-' + id +' .progress');
				var elapsed = 0;

				var timer = setInterval(function() {
					if (error)
						return t.$("#file-" + id).remove();
					var wrapper = t.$('#file-' + id);

					if (t.fm.getFileList()[id].done) {
						wrapper.find('.meter').remove();
						wrapper.addClass('completed');
						call_callback(t);
						clearInterval(timer)
					}
					if (elapsed < t.fm.getFileList()[id].upload.progress.get()) {
						elapsed = t.fm.getFileList()[id].upload.progress.get();
						progress.animate({
							width: elapsed + '%'
						}, 200);
					}
				}, 100)
			}
		}
	},
	'click .remove-item' (e, t) {
		for (let i = 0; i !== t.fm.getFileList().length; ++i) {
      let regexp = new RegExp(t.fm.getFileList()[i].file.name.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i")
      if (t.$(e.currentTarget.parentElement)[0].innerText.match(regexp)) {
				t.$(e.currentTarget.parentElement.parentElement).remove();
				t.fm.remove(i);
				$("#fileUploadInput").val('');
				break;
			}
		}
		call_callback(t);
	},
	'input #answer' (e, t) {
		t.msg.value = e.currentTarget.value;
		call_callback(t);
	},
	'keyup #answer' (e, t) {
		autoGrow();
	},
	'keyup .autogrow' (e, t) {
		autogrowByName();
	},
});

Template.textAreaFileUpload.helpers({
	noFileIcon: () => !Template.currentData().noFileIcon,
	getFileList: () => {
		return Template.instance().fm.getFileList();
	},
	errorMessage: () => {
		return Template.instance().fm.getErrorMessage();
	},
	getIconOfFile: (type) => {
		let arrType = type.split("/");
		if (arrType[0] == "image") {
			if (["png", "PNG"].indexOf(arrType[1]) != -1)
				return "png.jpg";
			if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
				return "jpeg.jpg";
			if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
				return "bmp.jpg";
			if (["gif", "GIF"].indexOf(arrType[1]) != -1)
				return "gif.jpg";
			return "image.jpg";
		}
		if (arrType[0] == "application") {
			if (arrType[1] == "pdf")
				return "pdf.jpg";
			let appType = arrType[1].split(".");
			if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
				if (appType[2] == "presentation" || appType[2] == "presentationml")
					return "ppt.jpg"
				if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
					return "xlsx.jpg"
				if (appType[2] == "wordprocessingml" || appType[2] == "document")
					return "docx.jpg"
			}
		}
		if (arrType[0] == "text")
			return "txt.jpg"
		return "file.png"
	}
});
