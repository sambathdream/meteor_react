import FileServer from '/client/utils/file-server.js'

export class FileManager {
  constructor (collection, customFields) {
    this.collection = collection
    this.fileList = new ReactiveArray([])
    this.errorMessage = new ReactiveVar('')
    this.customFields = customFields
  }

  addFiles (fileList) {
    fileList = _.map(fileList, function (f) { return {file: f, upload: null, done: true} })
    for (file of fileList) {
      this.fileList.push(file)
    }
  }

  setCustomFields (customFields) {
    this.customFields = customFields
  }

  remove (idx) {
    if (this.fileList.list()[idx].done) {
      let id = this.fileList.list()[idx].file._id
      let file = this.collection.findOne({_id: id})
      let self = this
      if (typeof file !== 'undefined') {
        file.remove(function (err) {
          if (err) {
            if (err.error === 403) { self.errorMessage.set("Vous n'avez pas le droit de supprimer ce fichier") } else { self.errorMessage.set('Une erreur est survenue lors de la suppression') }
          } else {
            self.fileList.remove(self.fileList.list()[idx])
            self.errorMessage.set('')
          }
        })
      }
    } else {
      this.fileList.list()[idx].upload.abort()
      this.fileList.remove(this.fileList.list()[idx])
    }
  }

  insert (file, callback) {
    if (!file) {
      return null
    }
    let data = {
      file: file,
      streams: 'dynamic',
      chunkSize: 'dynamic'
    }
    if (this.customFields) { data.meta = this.customFields }
    let upload = this.collection.insert(data, false)

    let id = this.fileList.push({file: file, upload: upload, done: false}) - 1
    let self = this

    upload.on('start', function () {
      self.fileList.list()[id].upload = this
      self.fileList.changed()
    })

    upload.on('end', function (error, fileObj) {
      if (error) {
        self.fileList.remove(self.fileList.list()[id])
        self.errorMessage.set(error.reason)
        if (callback) { callback(error, undefined) }
      } else {
        if (self.fileList.list()[id]) {
          self.fileList.list()[id].done = true
          self.fileList.list()[id].file = fileObj
          self.errorMessage.set('')
          if (callback) { callback(null, fileObj) }
        }
      }
      self.fileList.changed()
    })

    upload.start()
    return id
  }

  getFileList () {
    return this.fileList.list()
  }

  getFileListIds () {
    let list = _.filter(this.fileList.list(), (elem) => { return elem.done })
    return _.map(list, (elem) => { return elem.file._id })
  }

  getErrorMessage () {
    return this.errorMessage.get()
  }

  clearFiles () {
    this.fileList.clear()
  }

  /**
   *
   * @param file
   * @returns {Promise}
   */
  upload (file) {
    if (file.constructor !== Object) {
      console.error('only file object accepted in this method.')
      return
    }

    return new Promise((resolve, reject) => {
      this.insert(file, (err, obj) => {
        if (err) {
          reject(err)
        } else {
          resolve(obj)
        }
      })
    })
  }

  validateFiles (files) {
    return true
    // return files.reduce((valid, file) => valid && file.type.indexOf('video') !== 0, true)
  }

  /**
   *
   * @param files
   * @returns {*|Promise.<*>|Promise}
   */
  batchUpload (files) {
    if (files.constructor !== Array) {
      console.error('only Array of file accepted in this method.')
      return
    }

    const videoFiles = files.filter(file => /^video\//i.test(file.type))
    files = files.filter(file => !/^video\//i.test(file.type))

    const uploadPromises = _.map(files, (file) => {
      if (typeof file === 'string') {
        // allready add (normaly)
        return this.collection.findOne({ _id: file })
      } else if (file && file._id) {
        // allready add (normaly)
        return this.collection.findOne({ _id: file._id })
      } else {
        return new Promise((resolve, reject) => {
          this.insert(file, (err, obj) => {
            if (err) {
              reject(err)
            } else {
              resolve(obj)
            }
          })
        })
      }
    })

    if (videoFiles.length > 0) {
      videoFiles.forEach(file => {
        console.log('file', file)
        uploadPromises.push(new Promise(async (resolve, reject) => {
          try {
            const uploadResult = await FileServer.uploadVideo({
              file,
              fileMimeType: file.type
            })

            resolve({
              _id: { // TODO: We will change this when we move all files to use Files server
                fileId: uploadResult.data.fileId,
                type: 'video'
              }
            })
          } catch (error) {
            reject(error)
          }
        }))
      })
    }

    return Promise.all(uploadPromises)
  }
}
