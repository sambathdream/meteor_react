import { Template } from 'meteor/templating'

import "/client/components/fileManager/fileUpload.view.html";
import "/client/components/fileManager/fileUpload.css";

Template.fileUpload.onCreated(function () {
});

Template.fileUpload.helpers({
  getFileList: () => {
    return Template.instance().data.fileManager.getFileList();
  },
  errorMessage: () => {
    return Template.instance().data.fileManager.getErrorMessage();
  },
  getIconOfFile: (type) => {
    let arrType = type.split("/");
    if (arrType[0] == "image") {
      if (["png", "PNG"].indexOf(arrType[1]) != -1)
        return "png.jpg";
      if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
        return "jpeg.jpg";
      if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
        return "bmp.jpg";
      if (["gif", "GIF"].indexOf(arrType[1]) != -1)
        return "gif.jpg";
      return "image.jpg";
    }
    if (arrType[0] == "application") {
      if (arrType[1] == "pdf")
        return "pdf.jpg";
      let appType = arrType[1].split(".");
      if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
        if (appType[2] == "presentation" || appType[2] == "presentationml")
          return "ppt.jpg"
        if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
          return "xlsx.jpg"
        if (appType[2] == "wordprocessingml" || appType[2] == "document")
          return "docx.jpg"
      }
    }
    if (arrType[0] == "text")
      return "txt.jpg"
    return "file.png"
  }
});

Template.fileUpload.events({
  'change #fileUploadInput': function (e, template) {
    if (e.currentTarget.files) {
      for (let i = 0; i < e.currentTarget.files.length; i++)
        template.data.fileManager.insert(e.currentTarget.files[i]);
    }
  },
  'click .removeFile' : function (e, template) {
    let idx = parseInt(e.currentTarget.getAttribute("index"));
    template.data.fileManager.remove(idx);
  }
});
