/* global Template */
/* global _ */
import { CommonTranslation } from '/common/lang/lang'

function generateRandomString(length = 8) {
  let text = ''
  let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }

  return text
}

const mimeType = (mime) => {
  return mime.split('/')[0]
}

Template.FileUploadButton.onCreated = () => {
  this.id = generateRandomString()
}

Template.FileUploadButton.helpers({
  id: () => this.id
})

Template.FileUploadButton.events({
  'click .file-upload-icon': (e, t) => {
    t.$('.file-upload-button-input').click()
  },
  'change .file-upload-button-input': (e, t) => {
    const files = e.currentTarget.files

    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr_common = new CommonTranslation(lang)

    const filesAsArray = _.map(files, (file, index) => {
      file.id = `file-${generateRandomString()}`
      // Add preview, either image or file extension
      if (file.type && mimeType(file.type) === 'image') {
        file.preview = {
          type: 'image',
          url: window.URL.createObjectURL(file)
        }
      } else {
        file.preview = {
          type: 'file'
        }
      }

      return file
    })

    const currentFiles = t.data.files || []

    if (currentFiles.length + filesAsArray.length <= 5) {
      Template.instance().data.onFileAdded(filesAsArray)
      e.currentTarget.value = ''
    } else {
      return sAlert.info(tr_common.commonTranslation['error_upload_files'])
    }
  }
})
