/* globals Template */
/* globals _ */
/* globals getRandomString */
import mime from 'mime-types'
import { CommonTranslation } from '/common/lang/lang'
import FileServer from '/client/utils/file-server.js'

const mimeType = (type) => {
  return type.split('/')[0]
}

const getImageByExtension = (ext) => {
  const basePath = '/img/icons/svg/'

  switch (ext) {
    case 'zip':
      return `${basePath}extensionzip.svg`
    case 'docx':
    case 'doc':
      return `${basePath}extensiondoc.svg`
    case 'pptx':
    case 'ppt':
      return `${basePath}extensionppt.svg`
    case 'xls':
    case 'xlsx':
      return `${basePath}extensionxls.svg`
    case 'pdf':
      return `${basePath}extensionpdf.svg`
    case 'txt':
      return `${basePath}extensiontxt.svg`
    default:
      return `${basePath}extensionunknown.svg`
  }
}

Template.FileList.helpers({
  files: () => Template.instance().data.files,
  isImageFile: (type) => {
    const re = /gif|jpe?g|png|bmp|svg/i

    // console.log('type', type)
    return re.test(mime.extension(type))
  },
  showDropBox: () => ((Template.instance().data.files && Template.instance().data.files.length !== 0) || Template.instance().data.showDropBox) && (Template.instance().data.files && Template.instance().data.files.length < 5),
  getFileExtension: (file) => {
    if (file.ext) {
      return getImageByExtension(file.ext)
    } else {
      return getImageByExtension(mime.extension(file.type))
    }
  },
  getFileUrl: (file) => {
    if (file._id) {
      return file.link ? file.link() : file.preview.url
    } else {
      return file.preview.url
    }
  },
  getNewFileUrl: (file) => {
    let newFileUrl = ''
    switch (file.type) {
      case 'video':
        newFileUrl = FileServer.getFileUrl(file, 'thumbnail')
        break
    }
    return newFileUrl
  },
  getFileId: (file) => {
    if (file._id) {
      return file._id
    } else if (file.fileId) {
      return file.fileId
    } else {
      return file.id
    }
  }
})

Template.FileList.events({
  'click .file-list-item-delete, click .file-list-item-delete-new_display': (e, t) => {
    const fileId = e.currentTarget.getAttribute('data-file-id')

    $('.file-list-input').val('');

    if (fileId && typeof Template.instance().data.onRemove === 'function') {
      Template.instance().data.onRemove(fileId)
    }
  },
  'click .add-more, click .add-more-new_display': (e, t) => {
    t.$('.file-list-input').click()
  },
  'change .file-list-input': (e) => {
    const files = e.currentTarget.files
    const t = Template.instance();

    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr_common = new CommonTranslation(lang)

    if (t.data.files.length === 5) {
      return sAlert.info(tr_common.commonTranslation['error_upload_files'])
    }

    const filesAsArray = _.map(files, async (file) => {
      return new Promise((resolve, reject) => {
        file.id = `file-${getRandomString()}`
        // Add preview, either image or file extension
        if (file.type && mimeType(file.type) === 'image') {
          if (file._id) {
            file.preview = {
              type: 'image',
              url: file.link()
            }
          } else {
            const reader = new FileReader();

            reader.onload = function(){
              file.preview = {
                type: 'image',
                url: reader.result
              }

              resolve(file)
            };

            reader.readAsDataURL(file)
          }
        } else {
          file.preview = {
            type: 'file'
          }

          resolve(file);
        }
      })
    })

    // Wait for all promises to complete (All filereader async process)
    if (t.data.files.length + filesAsArray.length <= 5) {
      Promise.all(filesAsArray).then(files => {
        t.data.onFileAdded(files)
        e.currentTarget.value = ''
      })
    } else {
      return sAlert.info(tr_common.commonTranslation['error_upload_files'])
    }
  }
})
