import { ReactiveVar } from 'meteor/reactive-var';

// Usage: {{> lambdaDropDown classes="pull-right" FAIcon="fa-coffee" elems=HelperThatReturnAnArray placeholder="Choisir un élement" session="sessionVariableName" disabled=false}}

//HelperThatReturnAnArray must return an array of :
// {
// 	id: string,
// 	name: string,
// 	detail: string,
// 	~selected: boolean
// }

Template.lambdaDropDown.onCreated(function() {
	this.currentElemId = new ReactiveVar(undefined);
	this.currentElemName = new ReactiveVar();
	this.autorun(function(){
		Session.set(Template.instance().data.session, Template.instance().currentElemId.get());
	});
});

Template.lambdaDropDown.onDestroyed(function() {
	delete Session.keys[Template.instance().data.session];
});

Template.lambdaDropDown.onRendered(function(){
});

Template.lambdaDropDown.events({
	'click .dropdown-elems': function(event, template) {
		let currentElemId = $(event.currentTarget).data("id");
		template.currentElemId.set(currentElemId);
		Template.instance().currentElemName.set($(event.currentTarget).find('h5').text());
	},
});


Template.lambdaDropDown.helpers({
	getClass: () => {
		if (Template.instance().data.classes)
			return Template.instance().data.classes;
	},
	getFAIcon: () => {
		if (Template.instance().data.FAIcon)
			return Template.instance().data.FAIcon;
	},
	getElems: () => {
		if (Template.instance().data.elems)
			return Template.instance().data.elems;
	},
	getCurrentElemName: () => {
		return Template.instance().currentElemName.get();
	},
	getDefaultElemName: () => {
		let result;
		_.each(Template.instance().data.elems, function(elem){
			if(elem.selected) {
				result = elem.name;
				Template.instance().currentElemId.set(elem.id);
				return false;
			}
		});
		return result;
	},
	isDisabled: () => {
		return Template.instance().data.disabled ? true : false;
	}
});