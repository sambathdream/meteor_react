/* globals Template */

Template.MbBubble.onCreated(function() {
})

Template.MbBubble.helpers({
  getLabel: () => Template.instance().data.label || '',
  isActive: () => Template.instance().data.active || false,
  hasImage: () => Template.instance().data.image || false,
  getImage: () => Template.instance().data.image
})

Template.MbBubble.events({
  'click .mb-bubble-item': () => {
    if (Template.instance().data.onClick && typeof Template.instance().data.onClick === 'function') {
      Template.instance().data.onClick(Template.instance().data.value)
    }
  },
})