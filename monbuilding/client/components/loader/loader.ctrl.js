Template.loader.onCreated(function () {
});

Template.loader.onRendered(function () {
  if (!Template.instance().data || !Template.instance().data.display_auto || Template.instance().data.display_auto != false) {
    $(".loaderSpinner").show();
  }
});
