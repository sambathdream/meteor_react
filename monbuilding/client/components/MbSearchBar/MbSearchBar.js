Template.MbSearchBar.onCreated(function() {

})

Template.MbSearchBar.onRendered(function() {

})

Template.MbSearchBar.events({
  'input .text-search-bar': (event, template) => {
    let value = $(event.currentTarget).val().trim()
    if (typeof template.data.callback === 'function') {
        template.data.callback(value)
    }
  }
})

Template.MbSearchBar.helpers({

})
