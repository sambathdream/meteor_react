Template.MbEmpty.onCreated(function() {

});

Template.MbEmpty.helpers({
    getTitle: () => {
        return Template.instance().data.title ? Template.instance().data.title : '';
    },
    getSubTitle: () => {
        return Template.instance().data.subTitle ? Template.instance().data.subTitle : '';
    },
    getButtonLabel: () => {
        return Template.instance().data.buttonLabel ? Template.instance().data.buttonLabel : '';
    },
    getButtonAction: () => {
        const instance = Template.instance()
        if (typeof instance.data.onButtonClick === 'function') {
            return () => {
                instance.data.onButtonClick();
            }
        }
    },
  hasIcon: () => typeof Template.instance().data.icon !== 'undefined',
  getIcon: () => Template.instance().data.icon,
  getTitleStyle: () => Template.instance().data.titleStyle,
  getSubTitleStyle: () => Template.instance().data.subTitleStyle,
});