import React, { Component } from 'react'
import _ from 'lodash'

export default class MbSearchBarReact extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: ''
    }
    this.handleChange = /* _.debounce( */this.handleChange.bind(this)/* , 500) */
  }

  handleChange (event) {
    const { value } = event.target
    const { onFilterChange } = this.props
    this.setState({ value: value })
    if (onFilterChange && typeof onFilterChange === 'function') {
      onFilterChange(value)
    }
  }

  render () {
    const { placeholder, containerStyle } = this.props
    return (
      <div className={containerStyle}>
        <div className='icon-search-bar' />
        <input
          type='text'
          placeholder={placeholder}
          value={this.state.value}
          onChange={this.handleChange}
          className='search-input' />
      </div>
    )
  }
}
