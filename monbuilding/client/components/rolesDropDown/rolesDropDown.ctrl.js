import { ReactiveVar } from 'meteor/reactive-var';

Template.rolesDropDown.onCreated(function() {
	this.currentDefaultRole = new ReactiveVar({});
	this.currentDisplayingModule = "";
	this.autorun(function(){
		Session.set("currentDefaultRole", Template.instance().currentDefaultRole.get());
	});

	Meteor.subscribe('getDefaultRoles');
});

Template.rolesDropDown.onDestroyed(function() {
	delete Session.keys["currentDefaultRole"];
});

Template.rolesDropDown.onRendered(function(){
});

Template.rolesDropDown.events({
	'click .dropdown-roles': function(event, template) {
		let currentDefaultRoleId = $(event.currentTarget).data("id");
		template.currentDefaultRole.set(DefaultRoles.findOne(currentDefaultRoleId));
	},
});


Template.rolesDropDown.helpers({
	getClass: () => {
		if (Template.instance().data.classes)
			return Template.instance().data.classes;
	},
	getRoles: () => {
		return DefaultRoles.find().fetch();
	},
	getCurrentRoleName: () => {
		let currentDefaultRole = Template.instance().currentDefaultRole.get();
		if (currentDefaultRole)
			return currentDefaultRole.name;
		return false;
	}
});