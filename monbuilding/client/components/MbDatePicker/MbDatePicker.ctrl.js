import { DayOfWeek, MonthOfYear } from "/common/lang/lang.js";

Template.MbDatePicker.onCreated(function() {
  this.lang = new ReactiveVar(FlowRouter.getParam("lang") || "fr");
  this.dayofweek = new ReactiveVar(new DayOfWeek(this.lang).dayofweek);
  this.monthofyear = new ReactiveVar(new MonthOfYear(this.lang).month);

  this.dpId = `dp_${getRandomString()}`;
});

Template.MbDatePicker.onRendered(function() {
  let lang = this.lang.get();
  let day = this.dayofweek.get();
  let month = this.monthofyear.get();
  let clear = this.data.hasClear ? true : false;

  $(`#${this.dpId}`).datepicker({
    format: "dd/mm/yyyy",
    weekStart: lang === 'fr',
    maxViewMode: 0,
    language: lang,
    clearBtn: clear,
    autoclose: true,
    todayHighlight: true,
    startDate: '0d',
    templates: {
      leftArrow: '<i class="fa fa-chevron-left"></i>',
      rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
  })

  console.log(this.dpId);
})

Template.MbDatePicker.helpers({
  getId () {
    return Template.instance().dpId;
  },
  placeholder() {
    return Template.instance().data.placeholder || moment().locale(this.lang).format('lll');
  },
  value() {
    return Template.instance().data.value || null;
  },
  label() {
    return Template.instance().data.label || '';
  },
  required() {
    return Template.instance().data.required || false;
  }
})
