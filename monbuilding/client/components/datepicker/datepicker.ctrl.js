import { DayOfWeek, MonthOfYear } from "/common/lang/lang.js";

/*
{{> datepicker datepickerId= hasClear= value=}}

datepickerId : ID of datepicker
hasClear     : (opt) true or false
value        : (opt) value of date
*/

Template.datepicker.onCreated(function() {
	this.lang = new ReactiveVar(FlowRouter.getParam("lang") || "fr");
	this.dayofweek = new ReactiveVar(new DayOfWeek(this.lang).dayofweek);
	this.monthofyear = new ReactiveVar(new MonthOfYear(this.lang).month);
});

Template.datepicker.onRendered(function() {
	let lang = this.lang.get();
	let month = this.monthofyear.get();
	let clear = this.data.hasClear ? true : false;

	$('.datepicker').datepicker({
		format: !!this.data.format? this.data.format: "dd/mm/yyyy",
		weekStart: 1,
		maxViewMode: 0,
		language: lang,
		clearBtn: clear,
		autoclose: true,
		todayHighlight: true,
		startDate: '0d',
		templates: {
			leftArrow: '<i class="fa fa-chevron-left"></i>',
			rightArrow: '<i class="fa fa-chevron-right"></i>'
		},
    container: !!this.data.container ? "#" + this.data.container : 'body'
	}).on('changeMonth', function(event) {
		let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
		const now = moment().startOf('month');
		const ev = moment(event.date).startOf('month');

		if (ev.diff(now, 'months') <= 0) {
			$(prev).css('visibility', 'visible');
			$(prev).removeClass('prev');
			$(prev).html('');
		}
		else {
			$(prev).addClass('prev');
			$(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(1)").html('<i class="fa fa-chevron-left"></i>');
		}
	})
	$('.datepicker').find('.datepicker').remove();
});


Template.datepicker.events({
	'click .datepicker': function(event, template) {
		let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
		let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
		for (var i = 0; i != template.monthofyear.get().long.length; i++) {
			if (template.monthofyear.get().long[i] == date)
				break;
		}
		if (new Date().getMonth() == i) {
			$(prev).css('visibility', 'visible');
			$(prev).removeClass('prev');
			$(prev).html('');
		}
	},
	'click .input-group-addon': function(event, template) {
		let calendar = $(event.currentTarget).parent().find('input[data-provide=datepicker]');
		if (calendar.length) {
			calendar.focus();
			let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
			let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
			for (var i = 0; i != template.monthofyear.get().long.length; i++) {
				if (template.monthofyear.get().long[i] == date)
					break;
			}
			if (new Date().getMonth() == i) {
				$(prev).removeClass('prev');
				$(prev).html('');
			}
		}
	},
});

Template.datepicker.helpers({
	getIdOfDatepicker: () => {
		if (Template.instance().data.datepickerId)
			return Template.instance().data.datepickerId;
	},
	dateToday: () => {
		let date = new Date(Date('now'));
		return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
	},
	dateOfDatePicker: () => {
		if (Template.instance().data.value)
			return Template.instance().data.value;
	},
});
