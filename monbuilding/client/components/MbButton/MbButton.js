/**
 * Created by kuyarawa on 31/01/18.
 */

/*
 {{>MbButton label="The label"  onClick="onclick callback function"}}
 */
Template.MbButton.onCreated(function() {
    const id = getRandomString();
    this.randomId = id;

    const key = "click " + "#mb-button-" + id;
    let eventsMap = {};
    eventsMap[key] = (e) => {
        if (typeof Template.instance().data.onClick === 'function') {
            Template.instance().data.onClick();
        }
    }

    Template.MbButton.events(eventsMap);
});

Template.MbButton.helpers({
    getLabel: () => {
        return Template.instance().data.label ? Template.instance().data.label : '';
    },
    getId: () => {
      return Template.instance().data.id ? Template.instance().data.id : Template.instance().randomId
    },
    getRandomId: () => {
        return Template.instance().randomId;
    },
    getClass: () => {
        return Template.instance().data.class ? Template.instance().data.class : '';
    },
    getType: () => {
      return Template.instance().data.type ? Template.instance().data.type : '';
    }
});