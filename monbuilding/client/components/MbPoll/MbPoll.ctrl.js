Template.MbPoll.onCreated(function() {

})

Template.MbPoll.onRendered(function() {
})

Template.MbPoll.events({
  'click #vote': (e) => {
    e.stopImmediatePropagation();

    const idx = $(e.currentTarget).attr('data-vote-index');
    const callback = Template.instance().data.onVote;

    if (callback && typeof callback === 'function') {
      callback(parseInt(idx));
    }
  }
})

Template.MbPoll.helpers({
  isVoted: (index) => {
    const vote = _.find(Template.instance().data.post.votes, (v, key) => {
      return parseInt(v) === index && key === Meteor.userId();
    });

    return typeof vote !== 'undefined';
  },
  getProgress: (index) => {
    const votes = Template.instance().data.post.votes || [];
    const voteByindex = _.filter(votes, (v) => parseInt(v) === index);

    return 100 * (voteByindex.length / Object.keys(votes).length);
  },
  getCount: (index) => {
    return _.filter(Template.instance().data.post.votes, (v) => parseInt(v) === (index)).length;
  },
  getChecked: (checked) => {
    return !!checked ? 'checked' : ''
  }
})
