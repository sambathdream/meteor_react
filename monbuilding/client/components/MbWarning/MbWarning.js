Template.MbWarning.helpers({
    getContent: () => {
        return !!Template.instance().data.content ? Template.instance().data.content : '';
    }
})