export class Pagination {
  constructor(collection, limit) {
    this.col = collection;
    this.start = new ReactiveVar(0);
    this.limit = new ReactiveVar(limit || 10);
    this.query = new ReactiveVar({});
    this.option = new ReactiveVar({});
  }

  setCollection(collection) {
    this.col = collection;
  }

  setQuery(query, option) {
    this.query.set(query);
    this.option.set(option);
  }

  getQuery() {
    return {query: this.query.get(), option: this.option.get()};
  }

  setLimit(limit) {
    this.limit.set(limit);
  }

  setPage(page) {
    this.start.set((page -1) * this.limit.get());
  }

  getPage() {
    return this.start.get() / this.limit.get() + 1;
  }

  getNbPage() {
    let c = this.col.find(this.query.get(), this.option.get()).count();
    return Math.trunc(c / this.limit.get() + (c % this.limit.get() != 0));
  }

  getResults() {
    const opt = {
      ...this.option.get(),
      skip: this.start.get(),
      limit: this.limit.get()
    };
    return this.col.find(this.query.get(), opt);
  }

  getTotal() {
    return this.col.find(this.query.get(), this.option.get()).count()
  }

  getSortedResults(sortedField) {
    let col =  _.sortBy(this.col.find(this.query.get()).fetch(), sortedField).reverse();
    return col.slice(this.start.get(), this.start.get() + this.limit.get());
  }

  getSortedResultsAsc(sortedField) {
      let col =  _.sortBy(this.col.find(this.query.get()).fetch(), sortedField);
      return col.slice(this.start.get(), this.start.get() + this.limit.get());
  }
}
