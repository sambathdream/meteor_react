import {Pagination} from './pagination.js';

import "./pagination.css";

Template.pagination.onCreated(function() {
  this.pagination = this.data.pagination;
});

Template.pagination.helpers({
  hasSpaceWith: (w) => {
    if (w == "first")
      return Template.instance().pagination.getPage() > 3;
    if (w == "last")
      return Template.instance().pagination.getPage() < Template.instance().pagination.getNbPage() - 2;
  },
  getPageValue: (w) => {
    if (w == "prev")
      return Template.instance().pagination.getPage() - 1;
    if (w == "next")
      return Template.instance().pagination.getPage() + 1;
    if (w == "curr")
      return Template.instance().pagination.getPage();
    if (w == "last")
      return Template.instance().pagination.getNbPage();
  },
  hasPage: (w) => {
    if (w == "prev")
      return Template.instance().pagination.getPage() > 2;
    if (w == "next")
      return Template.instance().pagination.getPage() < Template.instance().pagination.getNbPage() - 1;
    if (w == "last")
      return Template.instance().pagination.getNbPage() > 1;
  }
});

Template.pagination.events({
  'click .pageBtn' (e, t) {
    let page = parseInt(e.currentTarget.getAttribute('page'));
    t.pagination.setPage(page);
  }
})
