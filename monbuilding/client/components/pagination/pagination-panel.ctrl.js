import {Pagination} from './pagination.js';

import "./pagination.css";

Template.pagination_panel.onCreated(function() {
  this.pagination = this.data.pagination;
});

Template.pagination_panel.helpers({
  getPages: () => {
    const len = Template.instance().pagination.getNbPage();
    const currentPageIndex = Template.instance().pagination.getPage();
    const truncatedLimit = 10;
    const visibleMidCount = 5;
    const disableTruncateThreshold = 3;
    const maxVisiblePagination = 7;

    if (len > truncatedLimit) {
      if (currentPageIndex >= (len - 1)) {
        const visiblePagination = _.last(_.range(1, len + 1), visibleMidCount)

        return [
          1,
          '...',
          ...visiblePagination,
        ]
      }
      else if (currentPageIndex > 2 && currentPageIndex <= (len - visibleMidCount)) {
        const visibleNumbers = _.range(
          Math.max(disableTruncateThreshold, currentPageIndex - 2),
          Math.max(maxVisiblePagination, currentPageIndex + disableTruncateThreshold)
        )

        return [
          1,
          '...',
          ...visibleNumbers,
          '...',
          len
        ]
      } else if (currentPageIndex >= (len - disableTruncateThreshold)) {
        const visibleNumbers = _.range(Math.max(disableTruncateThreshold, currentPageIndex - 2), len + 1)

        return [
          1,
          '...',
          ...visibleNumbers
        ]
      } else {
        const visiblePagination = _.range(1, maxVisiblePagination)

        return [
          ...visiblePagination,
          '...',
          len
        ]
      }
    } else {
      return _.range(1, (len + 1));
    }
  },
  getCurrPage: () => {
    return Template.instance().pagination.getPage();
  },
  showPrev: () => {
    return Template.instance().pagination.getPage() > 1;
  },
  showNext: () => {
    return Template.instance().pagination.getPage() < Template.instance().pagination.getNbPage();
  }
});

Template.pagination_panel.events({
  'click .pageSelect' (e, t) {
    let page = parseInt(e.currentTarget.getAttribute('index'));
    t.pagination.setPage(page);
  },
  'click .pageNext' (e, t) {
    let p = t.pagination.getPage();
    if (p < t.pagination.getNbPage())
      t.pagination.setPage(p + 1);
  },
  'click .pagePrev' (e, t) {
    let p = t.pagination.getPage();
    if (p > 1)
      t.pagination.setPage(p - 1);
  }
})
