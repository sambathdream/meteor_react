import axios from 'axios'
import { environment } from '/common/environment'
const videoRegex = /^video\/.*/i
let filesServerUrl = environment.filesServerUrl

const videoEndpoint = `${filesServerUrl}/videos`

const meteorInformation = () => {
  return {
    userId: localStorage.getItem('Meteor.userId'),
    token: localStorage.getItem('Meteor.loginToken')
  }
}

const upload = ({ endpointPath, body, data }) => {
  const { userId, token } = meteorInformation()
  const headers = {
    'Content-Type': 'multipart/form-data',
    authorization: '',
    userid: ''
  }

  if (token && userId) {
    headers.authorization = `Bearer ${token}`
    headers.userid = userId
  }

  return axios({
    url: endpointPath,
    method: 'POST',
    body,
    data,
    headers
  })
}

const getFileUrl = (file, type) => {
  let fileTypePath = ''
  let queryString = ''
  switch (type) {
    case 'video':
      fileTypePath = 'videos'
      break
    case 'image':
      fileTypePath = 'images'
      break
    case 'thumbnail':
      fileTypePath = 'images'
      queryString = 'size=thumbnail'
      break
  }

  if (queryString.length > 0) {
    queryString = '?' + queryString
  }

  return `${filesServerUrl}/${fileTypePath}/${file.fileId}${queryString}`
}

const uploadVideo = ({ file, fileMimeType }) => {
  if (videoRegex.exec(fileMimeType) !== null) {
    const formData = new FormData()
    formData.append('video', file)
    return upload({
      endpointPath: videoEndpoint,
      data: formData
    })
  } else {
    throw Error('Mimetype should start with video/*')
  }
}

const uploadVideos = async (videoFiles) => {
  let videoFileIds = []
  if (Array.isArray(videoFiles) && videoFiles.length > 0) {
    await Promise.all(videoFiles.map(async file => {
      const uploadResult = await uploadVideo({
        file,
        fileMimeType: file.type
      })
      videoFileIds.push({
        fileId: uploadResult.data.fileId,
        type: 'video'
      })
    }))
  }
  return videoFileIds
}

export default {
  uploadVideo,
  uploadVideos,
  getFileUrl
}
