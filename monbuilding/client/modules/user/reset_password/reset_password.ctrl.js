import { Home } from "/common/lang/lang.js";

let doneCallback = null

Accounts.onResetPasswordLink(function (token, done) {
  const lang = (FlowRouter.getParam('lang') || 'fr')
  FlowRouter.go('app.forgot_password.reset', { lang, resetToken: token })
  doneCallback = done
});

Template.reset_password.onCreated(function() {
  this.form = new ReactiveDict()
  this.error = new ReactiveVar(false)
  this.resetSuccess = new ReactiveVar(false)
  this.loading = new ReactiveVar(true)

  this.form.setDefault({
    password: '',
    password_confirmation: ''
  })
})

Template.reset_password.helpers({
  isSuccess: () => {
    return Template.instance().resetSuccess.get()
  },
  getValue: (key) => {
    return Template.instance().form.get(key)
  },
  updateValue: (key) => {
    const t = Template.instance();

    return () => value => {
      return t.form.set(key, value)
    }
  },
  hasError: () => {
    return Template.instance().error.get() !== false
  },
  getError: () => {
    return Template.instance().error.get()
  },
  showForm: () => {
    return !Template.instance().resetSuccess.get() && !Template.instance().loading.get()
  }
})

Template.reset_password.onRendered(function() {
  const t = Template.instance();
  Session.set('isValidToken', true)
  $('#loader').show().css('display', 'flex');

  if (FlowRouter.getParam('resetToken') === undefined) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.login.login", {lang});
  }

  Meteor.call('isResetTokenActive', FlowRouter.getParam('resetToken'), function(error, result) {
    if (!error) {
      Session.set('isValidToken', !!result)
    }
  });

  this.autorun(function () {
    if (Session.get('isValidToken') === false) {
      const lang = (FlowRouter.getParam("lang") || "fr");
      FlowRouter.go("app.login.login", {lang});
    } else {
      t.loading.set(false);
      $('#loader').hide();
    }
  });
})

Template.reset_password.events({
  'click #go-back': () => {
    if (doneCallback) {
      doneCallback()
    }
    const lang = (FlowRouter.getParam("lang") || "fr")
    if (Template.instance().resetSuccess.get()) {
      FlowRouter.go("app.login.login", { lang });
    } else {
      FlowRouter.go("app.forgot_password.request", {lang});
    }
  },
  'click #back-to-request-password': () => {
    if (doneCallback) {
      doneCallback()
    }
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.login.login", { lang });
  },
  'click #reset_password': (e, t) => {
    t.error.set(false);

    const password = t.form.get('password').trim();
    const passwordc = t.form.get('password_confirmation').trim();
    const translation = new Home((FlowRouter.getParam("lang") || "fr"));
    const el = t.find(e.currentTarget);

    if (password === '' || passwordc === '') {
      t.error.set(translation.home.reset_password_empty);
    } else if (password !== passwordc) {
      t.error.set(translation.home.reset_password_not_match);
    } else {
      $(el).button('loading');

      Accounts.resetPassword(FlowRouter.getParam('resetToken'), password, function () {
        $(el).button('reset');
        t.resetSuccess.set(true)
        if (doneCallback) {
          doneCallback()
        }
      });
    }
  }
})
