import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'
import './auto_login.view.html'

Template.auto_login.onCreated(function () {
  const { token } = FlowRouter.current().queryParams

  Meteor.logout(() => {
    Accounts.callLoginMethod({
      methodArguments: [{
        ad: true,
        token
      }],
      userCallback: (error, result) => {
        if (!error) {
          const { type } = result
          let url = '/fr'
          
          if (type.residentId) {
            url = `/fr/resident/${type.condoId}`
          }

          if (type.gestionnaireId) {
            url = `/fr/gestionnaire/buildingsview`
          }
          FlowRouter.go(url)
        }
      }
    })
  })
})
