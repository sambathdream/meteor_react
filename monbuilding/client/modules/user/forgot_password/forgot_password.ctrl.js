/* global isValidEmail */
import { CommonTranslation } from '/common/lang/lang.js';

Template.forgot_password.onCreated(function() {
  this.isError = new ReactiveVar(false);
  this.isSuccess = new ReactiveVar(false);
  this.email = new ReactiveVar(FlowRouter.getParam('email') || '')
});

Template.forgot_password.helpers({
  updateEmail: () => {
    const tpl = Template.instance()

    return value => {
      tpl.isError.set(false);
      tpl.email.set(value);
    }
  },
  getEmailValue: () => Template.instance().email.get(),
  isError: () => Template.instance().isError.get(),
  isSuccess: () => Template.instance().isSuccess.get(),
  disableSubmit: () => !isValidEmail(Template.instance().email.get().trim())
})

Template.forgot_password.events({
  'click #go-back': () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.login.login", {lang});
  },
  'click #resendForgotPassword': (e, t) => {
    const email = Template.instance().email.get();

    if (isValidEmail(email.trim())) {
      let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
      Accounts.forgotPassword({ email: email.trim() }, function (e, r) {
        if (e) {
          sAlert.error(translation.commonTranslation['error_occured'])
        } else {
          sAlert.success(translation.commonTranslation['email_sent'])
        }
      });
    }
  },
  'click #submit': (e, t) => {
    e.preventDefault();
    const el = t.find(e.currentTarget);

    const email = Template.instance().email.get();

    if (!isValidEmail(email.trim())) {
      t.isError.set(true);

      // Do show some error messages here!
    } else {
      $(el).button('loading');

      Meteor.call("getUserIdWithEmail", t.email.get(), function(error, result) {
        if (!error && result && result.userId && result.userId != undefined) {
          // console.log("getUserIdwithEmail")
          const lang = (FlowRouter.getParam("lang") || "fr")
          FlowRouter.go("app.services.setPassword", {lang, userId: result.userId})
        } else {
          Accounts.forgotPassword({email: email.trim()}, function (e, r) {
            if (e) {
              $(el).button('reset');
              t.isSuccess.set(false);
            } else {
              $(el).button('reset');
              t.isSuccess.set(true);
            }
          });
        }

      });
    }


  }
})
