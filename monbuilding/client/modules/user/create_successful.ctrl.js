import { Template } from "meteor/templating";
import { FileManager } from "../../components/fileManager/filemanager.js";

import "./create_successful.view.html";

Template.create_successful.onCreated(function () {
	this.isToBackoffice = Session.get('isToBackoffice');
});

Template.create_successful.onRendered(function() {

});

Template.create_successful.events({
	'click #button_login': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
        const lang = (FlowRouter.getParam("lang") || "fr");
		FlowRouter.go("app.login.login", {lang});
	},
});

Template.create_successful.helpers({
	getLang: () => {
		return (FlowRouter.getParam("lang") || "fr");
	},
	isToBackoffice: () => {
		return Template.instance().isToBackoffice;
	}
});

