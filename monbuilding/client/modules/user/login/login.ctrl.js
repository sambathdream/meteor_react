import { Template } from "meteor/templating";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { CommonTranslation } from "/common/lang/lang.js"

import "./login.view.html";

Template.login.onCreated(function () {
	this.errorLogin = new ReactiveVar("");
	this.stayConnected = new ReactiveVar(false);

	// Tracker.autorun(function() {
	//     let user = Meteor.user();
	//     if (user && Meteor.loggingIn()) {
	//         if (user.identities.adminId !== undefined)
	//             FlowRouter.go('/backoffice');
	//         else if (user.identities.residentId !== undefined)
	//             FlowRouter.go('/fr/resident');
	//         else if (user.identities.gestionnaireId !== undefined)
	//             FlowRouter.go('/gestionnaire');
	//     }
	// });
});

Template.login.onRendered(function() {
});

Template.login.helpers({
	'loginError' : () => {return Template.instance().errorLogin.get();},
	'stayConnected' : () => {return Template.instance().stayConnected.get();},
});

function setSession(persist) {
	if (typeof(Storage) !== "undefined") {
		let expire = moment().add(2, 'd');
		if (persist) {
			expire = moment().add(1, 'y');
		}
		localStorage.setItem("Meteor.loginTokenExpires", expire.toDate());
	} else {
        // Sorry! No Web Storage support..
    }
}

Template.login.events({
	'click #backbuttonLogin': function(event, template) {
		FlowRouter.go("app.home", {lang: FlowRouter.getParam("lang") || "fr"});
	},
	'click #button_forgot': function(e, t) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (email = $('[name=email]').val())
      Session.set('email_forgot', email);
    if (email)
      FlowRouter.go("app.forgot_password.email", {lang, email})
    else
      FlowRouter.go("app.forgot_password.request", {lang})
	},
	'click #button_connect': function(event, template) {
		event.preventDefault();
		const email = $('[name=email]').val();
		const password = $('[name=password]').val();
		const lang = (FlowRouter.getParam("lang") || "fr");
		const translate = new CommonTranslation(lang);
		if (!email || !password) {
			template.errorLogin.set(translate.commonTranslation["login_required_fields"]);
			return ;
		}
		$("#button_connect").button('loading');
		$("#button_connect").attr('disabled', 'disabled');
		$("#button_connect").css('cursor', 'not-allowed');

		Meteor.loginWithPassword(email, password, function (error){
			if (error){
/* 				if (error.reason == "User has no password set") {
					Meteor.call("getUserIdWithEmail", email, function(error, result) {
						if (!error && result && result.userId && result.userId != undefined)
							FlowRouter.go("app.services.setPassword", {userId: result.userId});
					});
        } */
        if (error.reason === 'Your account is not activated yet') {
          sAlert.info(translate.commonTranslation['account_not_activated'])
          // template.errorLogin.set(translate.commonTranslation['account_not_activated']);
        } else {
          template.errorLogin.set(translate.commonTranslation["login_fail"]);
        }

        $("#button_connect").button('reset');
        $("#button_connect").removeAttr('disabled');
        $("#button_connect").css('cursor', 'pointer');
			}
			else {
        Meteor.subscribe('old-data', () => {
          if (!template.stayConnected.get()) {
            // set session for 2 days
            setSession(false);
          } else {
            // set session for 1 years
            setSession(true);
          }

          template.errorLogin.set("");
          $("#button_connect").button('reset');
          $("#button_connect").removeAttr('disabled');
          $("#button_connect").css('cursor', 'pointer');
          let target_url = Session.get('target_url');
          if (target_url) {
            Session.set('target_url', '/');
            FlowRouter.go(target_url);
          }
          else {
            let user = Meteor.users.findOne(Meteor.userId());
            if (user.identities.adminId !== undefined)
              FlowRouter.go('app.backoffice.managerList')
            else if (user.identities.residentId !== undefined) {
              const lang = (user.profile.lang || FlowRouter.getParam("lang") || "fr");
              if (user.lastCondoId) {
                const condo = {
                  lang: lang,
                  condo_id: user.lastCondoId,
                };
                FlowRouter.go('app.board.resident.condo', condo);
              }
              else
                FlowRouter.go('app.board.resident.home', {lang});
            }
            else if (user.identities.gestionnaireId !== undefined) {
              let gestionnaire = Enterprises.findOne({ _id: user.identities.gestionnaireId });
              if (gestionnaire) {
                gestionnaire = _.find(gestionnaire.users, function(elem) {
                  return elem.userId == user._id;
                });
                const lang = (FlowRouter.getParam("lang") || "fr");
                FlowRouter.go(`/${lang}/gestionnaire/buildingsview`);
              } else {
                Meteor.logout()
                template.errorLogin.set(translate.commonTranslation["login_fail"])
              }
            }
          }
        });
			}
		});
	},
	'click #facebookLogin': function(event, template) {
		event.preventDefault();
		Meteor.loginWithFacebook({requestPermissions: ['public_profile', 'email']}, function(err){
			if (err) {
				template.errorLogin.set(err);
			}
			else {

				if (!template.stayConnected.get()) {
					// set session for 2 days
					setSession(false);
				} else {
					// set session for 1 years
					setSession(true);
				}

				template.errorLogin.set("");
				let target_url = Session.get('target_url');
				if (target_url) {
					Session.set('target_url', '/');
					FlowRouter.go(target_url);
				}
				else {
					let user = Meteor.users.findOne(Meteor.userId());
					if (user.identities.adminId !== undefined)
            FlowRouter.go('app.backoffice.managerList')
					else if (user.identities.residentId !== undefined) {
						const lang = (FlowRouter.getParam("lang") || "fr");
						FlowRouter.go('app.board.resident.home', {lang});
					}
					else if (user.identities.gestionnaireId !== undefined) {
						let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
						if (gestionnaire) {
							gestionnaire = _.find(gestionnaire.users, function(elem) {
								return elem.userId == user._id;
							});
							const lang = (FlowRouter.getParam("lang") || "fr");
              FlowRouter.go(`/${lang}/gestionnaire/buildingsview`);
						}
					}
					else {
						const lang = (FlowRouter.getParam("lang") || "fr");
						const translate = new CommonTranslation(lang);

						template.errorLogin.set(translate.commonTranslation["login_last_building_fail"]);
					}
				}
			}
		});
	},
	'click [name="rememberMe"]': function(e, t) {
		console.log('t.stayConnected.get()', t.stayConnected.get());
		t.stayConnected.set(!t.stayConnected.get());
	},
	'click #goSignin': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		const lang = (FlowRouter.getParam("lang") || "fr");
		FlowRouter.go('app.login.signup', {lang});
	}
});
