import { Template } from "meteor/templating";
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import "./signup.view.html";
import { FileManager } from "/client/components/fileManager/filemanager";
import { GoogleApiError } from "/common/lang/lang.js";
import { Home } from "/common/lang/lang.js";

const regexpSearch = {
	'a': '[a|à|â|ä]',
	'e': '[e|é|è|ê|ë]',
	'i': '[i|î|ï|ì]',
	'o': '[o|ô|ö|ò]',
	'u': '[u|ù|ü|û]',
	'y': '[y|ý|ŷ|ÿ|ỳ]'
};

Template.signup.onCreated(function() {
	this.address = new ReactiveVar();
	this.isNewCondo = new ReactiveVar(false);
  this.needCompany = new ReactiveVar(false);

	this.errorSignin = new ReactiveVar("");
	this.apiGoogleError = new ReactiveVar("");
	this.subscribe('register_home_page', {});
	this.condoSearch = new ReactiveVar();
	this.condo = new ReactiveVar();
	this.statut = new ReactiveVar();
	this.company = null;
	this.validation = new ReactiveVar();
	this.hasGestionnaire = new ReactiveVar(false);
	this.hasGestionnaireToValid = new ReactiveVar(false);
	this.fm = new FileManager(UsersJustifFiles);
	this.predefinedInput = (FlowRouter.getQueryParam("formData") !== undefined ? FlowRouter.getQueryParam("formData") : null);
	this.signUpSuccess = new ReactiveVar(false);

});

function createNewCondo(address) {
	if (Template.instance().isNewCondo.get() == true) {
		let condo = {
			address: address.street_number + " " + address.road,
			city: address.city,
			code: address.code,
			name: "",
			type: "mono",
			_id: "",
			placeId: address.id,
			id: "-1"
		}
		Template.instance().condo.set(condo);
	}
}

function constructAttachmentElement(data)  {
	var progress = $('<span></span>')
	.addClass('progress')
	.css('width', '0%')

	var meter = $('<div></div>')
	.addClass('meter')
	.append(progress)

	var iconFile = $('<span></span>')
	.addClass('icon-file')
	.append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'))

	var checkmark = $('<span class="checkmark"></span>')
	.append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'))

	var fileSize = $('<span class="file-size"></span>')
	.html(data.size)
	.append(checkmark)

	var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name)

	var fileRemove = $(`<span class="remove-item" index="0">`)
	.append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'))

	var file = $('<div></div>')
	.addClass('file')
	.append(iconFile)
	.append(fileName)
	.append(fileSize)
	.append(fileRemove)
	.append(meter)

	var list = $('<li></li>')
	.attr('id', data.id)
	.addClass('attachment-item')
	.append(file)

	return list;
}


function checkElems(template, email, firstname, lastname, password, passwordVerif) {
	let Errors = false;
	let condo = template.condo.get();
	let statut = template.statut.get();
	let validation = template.validation.get();
	if (!statut || statut == "") {
		$('[name=statutButton]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=statutButton]').removeClass("errorSigninInput");
	}
	if ($('[name=cgu]').is(':checked') == false) {
		$('[name=cguGroup]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=cguGroup]').removeClass("errorSigninInput");
	}
	if ((validation == "backoffice" || Template.instance().isNewCondo.get() == true) && $('[name=justifDomTemp]').val() == "") {
		$('[name=justifDom]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=justifDom]').removeClass("errorSigninInput");
	}
	if (!condo || condo.length == 0) {
		const address = template.address.get();
		if (address && address.error)
			Template.instance().apiGoogleError.set(address.error);
		else {
			let translation = new GoogleApiError((FlowRouter.getParam("lang") || "fr"));
			Template.instance().apiGoogleError.set(translation.googleApiError['error1']);
		}
		$('#place-auto-complete').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('#place-auto-complete').removeClass("errorSigninInput");
	}
	if (!email || email == "" || !/^[A-Z0-9'.1234z_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
		$('[name=email]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=email]').removeClass("errorSigninInput");
	}
	if (!firstname || firstname == "") {
		$('[name=firstname]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=firstname]').removeClass("errorSigninInput");
	}
	if (!lastname || lastname == "") {
		$('[name=lastname]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=lastname]').removeClass("errorSigninInput");
	}
	if (!password || password == ""/*  || passwordVerif == "" || password != passwordVerif */) {
		$('[name=password]').addClass("errorSigninInput");
		// $('[name=passwordSigninVerif]').addClass("errorSigninInput");
		Errors = true;
		template.errorSignin.set(true);
	}
	else {
		$('[name=password]').removeClass("errorSigninInput");
		// $('[name=passwordSigninVerif]').removeClass("errorSigninInput");
	}
	return Errors;
}

function removeAllErrors(template) {
	$('#place-auto-complete').removeClass("errorSigninInput");
	$('[name=email]').removeClass("errorSigninInput");
	$('[name=firstname]').removeClass("errorSigninInput");
	$('[name=lastname]').removeClass("errorSigninInput");
	$('[name=password]').removeClass("errorSigninInput");
	// $('[name=passwordSigninVerif]').removeClass("errorSigninInput");
	$('[name=cguGroup]').removeClass("errorSigninInput");
	$('[name=statutButton]').removeClass("errorSigninInput");
	$('[name=justifDom]').removeClass("errorSigninInput");
	template.errorSignin.set(false);
}

Template.signup.events({
	'click #backbuttonSignUp': function(event, template) {
		FlowRouter.go("app.home", {lang: FlowRouter.getParam("lang") || "fr"});
	},
	'click #dropdownCondo': function(event, template) {
		removeAllErrors(template);
		template.condo.set(this);
			template.statut.set("tenant");
	},

	'click #dropdownStatut': function(event, template) {
		template.statut.set($(event.currentTarget).attr('name'));
	},

	'click #dropdownTypeCondo': function(event, template) {
		let condo = template.condo.get();
		condo.type = $(event.currentTarget).attr('name');
		template.condo.set(condo);
		template.statut.set("tenant");
	},

	'input #inputSearch': function(event, template) {
		let input = event.currentTarget.value;
		for (let i = 0; i != Object.keys(regexpSearch).length; i++) {
			input = input.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]]);
		}
		template.condoSearch.set(input);
	},

	'change #justifDomInput': function(e, t) {
		$('#justifLabel').css('display', 'block');
		$('#justifValidated').css('display', 'none');
		var attachmentListUL = $('#attachments');
		attachmentListUL.html("");
		if ($('[name=justifDomTemp]').val() != "") {
			UsersJustifFiles.findOne({_id: $('[name=justifDomTemp]').val()}).remove();
			$('[name=justifDomTemp]').val('');
		}
		if (e.currentTarget.files && e.currentTarget.files.length === 1) {
			$('#justifLabel').css('display', 'none');
			$('#justifValidated').css('display', 'block');
			var data = {};
			let error = false;
			$("#signin").attr('disabled', 'true');
			let id = t.fm.insert(e.currentTarget.files[0], function(err, file) {
				if(!err && file) {
					$('[name=justifDomTemp]').val(file._id);
				}
				else {
					sAlert.error(err);
				}
			});

			data.name = e.currentTarget.files[0].name;
			data.size = (e.currentTarget.files[0].size / 1000).toFixed(2) + ' kb';
			data.id = 'file-' + id;

			attachmentListUL.append(constructAttachmentElement(data));

			var progress = t.$('#file-' + id +' .progress');
			var elapsed = 0;

			var timer = setInterval(function() {
				if (error)
					return t.$("#file-" + id).remove();
				var wrapper = t.$('#file-' + id);

				if (t.fm.getFileList()[id].done) {
					wrapper.find('.meter').remove();
					wrapper.addClass('completed');
					$('#justifLabel').text("Modifier");
					$("#signin").removeAttr('disabled');
					clearInterval(timer);
				}
				if (elapsed < t.fm.getFileList()[id].upload.progress.get()) {
					elapsed = t.fm.getFileList()[id].upload.progress.get();
					progress.animate({
						width: elapsed + '%'
					}, 200);
				}
			}, 100)
		}
	},

	'click .remove-item': function(e, t) {
		if ($('[name=justifDomTemp]').val() != "") {
			UsersJustifFiles.findOne({_id: $('[name=justifDomTemp]').val()}).remove();
			t.$(e.currentTarget.parentElement.parentElement).remove();
			$("#fileUploadInput").val('');
			$("#justifDomInput").val('');
			$('[name=justifDomTemp]').val('');
			$('#justifLabel').text("Ajouter");
		}
	},

	'keyup': function(event, template) {
		if (template.errorSignin.get() == true) {
			var email = $('[name=email]').val();
			var firstname = $('[name=firstname]').val();
			var lastname = $('[name=lastname]').val();
			var password = $('[name=password]').val();
			// var passwordVerif = $('[name=passwordSigninVerif]').val();
			checkElems(template, email, firstname, lastname, password);
		}
	},

	'click #signin': function(event, template) {
    event.preventDefault();

		$(event.currentTarget).button('loading');
		var email = $('[name=email]').val();
		var firstname = $('[name=firstname]').val();
		var lastname = $('[name=lastname]').val();
		var password = $('[name=password]').val();
		// var passwordVerif = $('[name=passwordSigninVerif]').val();
		let _hasError = checkElems(template, email, firstname, lastname, password);
		if (_hasError == true) {
			$(event.currentTarget).button('reset');
			return;
		}
		template.errorSignin.set(false);
		if(/^[A-Z0-9'.1234z_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
			let condo = template.condo.get();
			let statut = template.statut.get();
			let validation = (Template.instance().isNewCondo.get() == true ? "backoffice" : template.validation.get());
			let justifFile = null;
			if (validation == "backoffice" || Template.instance().isNewCondo.get() == true) {
				justifFile = $('[name=justifDomTemp]').val();
      }
      let company = null
      if (template.company !== null) {
        const tempCompany = CompanyName.findOne({ name: template.company })
        if (tempCompany) {
          company = tempCompany._id
        }
      }
			Meteor.call('newResidentUser', {
				firstname: firstname,
				lastname: lastname,
				email: email,
				condo: condo,
				statut: statut,
				validation: validation,
				justifFile: justifFile,
				password: password,
				from: "web",
        lang: FlowRouter.getParam("lang") || "fr",
        company
			}, (err) => {
				if (err) {
					if (err.reason === 'Email already exists.') {
						let translation = new Home((FlowRouter.getParam("lang") || "fr"));
						err.message = translation.home['email_exist'];
					}
					$(event.currentTarget).button('reset');
					sAlert.error(err);
				}
				else {
					$('#modal_add_occupant').hide();
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
					const lang = (FlowRouter.getParam("lang") || "fr");
					const isToBackoffice = (validation == "backoffice") ? true : false
					Session.set('isToBackoffice', isToBackoffice);
					template.signUpSuccess.set(true);
				}
			});
		}
		else {
			$(event.currentTarget).button('reset');
			$('[name=email]').addClass("errorSigninInput");
			template.errorSignin.set(true);
		}
	},
	'click .goCgu': function(event, template) {
		event.preventDefault();
    let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},
	'click #goLogin': function(event, template) {
		event.preventDefault();
		FlowRouter.go("app.login.login", {lang: (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr')})
	},
	'click #backToHome': function(e) {
    let lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go("app.home", {lang: lang})
	}
});

Template.signup.helpers({
  signUpSuccess:() => {
  	return Template.instance().signUpSuccess.get()
	},
	signupError: () => {
		return Template.instance().errorSignin.get();
	},
	apiGoogleError: () => {
		return Template.instance().apiGoogleError.get();
	},
	isCondoSelected: () => {
		const address = Template.instance().address.get();
		if (!address || address.error) {
			if (address && address.error)
				Template.instance().apiGoogleError.set(address.error);
			else
				Template.instance().apiGoogleError.set("");
			return false;
		}
		Template.instance().apiGoogleError.set("");
		const regexpAddress = new RegExp(address.street_number + " " + address.road, "i");
		const regexpCode = new RegExp(address.code, "i");
		const regexpcity = new RegExp(address.city, "i");

		const building = SigninBuildings.findOne(
		{
			$and: [
			{"address": regexpAddress},
			{"code": regexpCode},
			{"city": regexpcity}
			]
		});
		let condo;
		if (building && building.name && building.name.length > 0) {
			condo = SigninCondos.findOne(building.condoId);
		}
		else {
			condo = SigninCondos.findOne(
			{
				$and: [
				{"address": regexpAddress},
				{"code": regexpCode},
				{"city": regexpcity}
				]
			});
		}
		if (condo && condo.name && condo.name.length > 0) {
			Template.instance().isNewCondo.set(false);
			removeAllErrors(Template.instance());
			Template.instance().condo.set(condo);
      Template.instance().statut.set("tenant");
      Template.instance().needCompany.set(condo.needCompany)
			return true;
		}
		else if (address) {
			Template.instance().isNewCondo.set(true);
			createNewCondo(address);
			Template.instance().statut.set("tenant");
      Template.instance().needCompany.set(false)
			return true;
		}
	},
	getCondoList: () => {
		return _.filter(SigninCondos.find().fetch(), function(elem) {
			if (!Template.instance().condoSearch.get())
				return elem;
			let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
			return elem.name.match(regexp);
		});
	},

	getCondoName: (idx) => {
		let condo = Template.instance().condo.get();
		if (condo && condo.name)
			return condo.name;
	},
	needStatut: () => {
		let condo = Template.instance().condo.get();
		if (condo.type == "copro")
			return true;
	},
	getStatut: () => {
		return Meteor.getBuildingStatusValue(Template.instance().statut.get(), true, null);
	},
	getHasGestionnaire: () => {
		return Template.instance().hasGestionnaire.get();
	},
	hasGestionnaire: () => {
		let condo = Template.instance().condo.get();
		if (condo && condo._id) {
			Meteor.call('signinHasGestionnaire', condo._id, function(err, ret) {
				Session.set('signinHasGestionnaire', ret);
			});
			return Session.get('signinHasGestionnaire');
		}
	},
	hasGestionnaireToValid: () => {
		let condo = Template.instance().condo.get();
		if (condo && condo._id) {
			Meteor.call('signinHasGestionnaireToValid', condo._id, condo.type, function(err, ret) {
				Session.set('signinHasGestionnaireToValid', ret);
			});
			return Session.get('signinHasGestionnaireToValid');
		}
	},
	setValidation: (validation) => {
		Template.instance().validation.set(validation);
	},
	getApiList: () => {
		let address = Template.instance().condoSearch.get();
		if (!address || address.length == 0) {
			return;
		}
	},
	getInputGoogleCB: () => {
		return Template.instance().address;
	},
	isNewCondo: () => {
		return Template.instance().isNewCondo.get();
	},
	getTypeCondo: () => {
		let translation = new Home((FlowRouter.getParam("lang") || "fr"));
		switch (Template.instance().condo.get().type) {
			case "mono":
			return translation.home['mono'];
			break;
			case "copro":
			return translation.home['copro'];
			break;
			case "office":
			return translation.home['office'];
			break;
			case "etudiante":
			return translation.home['student'];
			break;
			default:
			return translation.home['condos_type'];
			break;
		}
	},
	getPredefined: (key) => {
		if (Template.instance().predefinedInput !== null) {
			return Template.instance().predefinedInput[key];
		} else {
			return '';
		}
	},
	isSubscribeReady: () => {
		return Template.instance().subscriptionsReady();
	},
	buildingsStatus: () => {
		return BuildingStatus.find().fetch();
  },
  needCompany: () => {
    return Template.instance().needCompany.get()
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      t.company = value
    }
  }
});
