import { Template } from "meteor/templating";
import { Session } from 'meteor/session'
import "./satisfaction.view.html";

Template.satisfaction.onCreated(function () {
});

Template.satisfaction.onRendered(function () {
	$('.otherText').prop('disabled', true);
	$('[type="radio"]').change(function(){
		let input = $(this).parent().find('.otherText');
		if (!$(this).hasClass('otherRadio'))
			input.prop('disabled', true);
		else
			input.prop('disabled', false);
		input.val('');
	});

	$('.starRadio').change(function(){
		let starRate = $(this).parent();
		let stars = starRate.find('.starRadio + label');
		let inputs = starRate.find('.starRadio');
		stars.removeClass('starhighlight');

		let self = this;
		$.each(stars, function (key, val){
			$(stars[key]).addClass('starhighlight');
			if (self == inputs[key])
				return false;
		});
	});
});

function	getQuestionBoxRadio(name, suffix = "Text") {
	let checked = $('[name="' + name + '"]:checked');
	let result = [checked.val()];
	if (result[0] == "other" || suffix == "Why")
		result.push(checked.parent().find('[name="' + name + suffix + '"]').val())

	return result;
}

Template.satisfaction.events({
	'click input[type="submit"]' : function (event, template) {
		let result = {
			howknow:				null,
			whyresidence:			null,
			howbook:				null,
			isWebsiteOk:			null,
			howWasReception:		null,
			simpleProcedure:		null,
			howInventory:			null,
			haveYouBeenSatisfied: {
				receptionTeam:		null,
				cleanliness:		null,
				ambience:			null,
				breakfast:			null,
				laundromat:			null,
				changeOfLinen:		null,
				sportsHall:			null,
				internetConnection:	null,
			},
			globalMarkResidence:	null,
			globalMarkApartment:	null,
			reasonDeparture:		null,
			recommendation:			null,
			suggestions:			null,
		}

		let archiveId = FlowRouter.getParam('archiveId');
		result.howknow = getQuestionBoxRadio('howknow');
		result.whyresidence = getQuestionBoxRadio('whyresidence');
		result.howbook = getQuestionBoxRadio('howbook');
		result.isWebsiteOk = getQuestionBoxRadio('isWebsiteOk', 'Why');
		result.howWasReception = getQuestionBoxRadio('howWasReception');
		result.simpleProcedure = getQuestionBoxRadio('simpleProcedure', 'Why');
		result.howInventory = getQuestionBoxRadio('howInventory');
		result.haveYouBeenSatisfied.receptionTeam = getQuestionBoxRadio('receptionTeam');
		result.haveYouBeenSatisfied.cleanliness = getQuestionBoxRadio('cleanliness');
		result.haveYouBeenSatisfied.ambience = getQuestionBoxRadio('ambience');
		result.haveYouBeenSatisfied.breakfast = getQuestionBoxRadio('breakfast');
		result.haveYouBeenSatisfied.laundromat = getQuestionBoxRadio('laundromat');
		result.haveYouBeenSatisfied.changeOfLinen = getQuestionBoxRadio('changeOfLinen');
		result.haveYouBeenSatisfied.sportsHall = getQuestionBoxRadio('sportsHall');
		result.haveYouBeenSatisfied.internetConnection = getQuestionBoxRadio('internetConnection');
		result.globalMarkResidence = getQuestionBoxRadio('globalMarkResidence');
		result.globalMarkApartment = getQuestionBoxRadio('globalMarkApartment');
		result.reasonDeparture = [$('[name="reasonDeparture"]').val()];
		result.recommendation = getQuestionBoxRadio('recommendation', 'Why');
		result.suggestions = [$('[name="suggestions"]').val()];
		
		Meteor.call('submitSatisfaction', archiveId, result, function(error, result){
			if (error) {
				console.log(error);
				sAlert.error("Erreur: veuillez contacter le webmaster.");
			}
			else
			{
				Session.set("satisfaction", true);
				FlowRouter.go('app.home', {lang: FlowRouter.getParam("lang") || "fr"});
			}
		});
	},
});