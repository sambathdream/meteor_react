import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

const allowedLang = ["fr", "en"];

FlowRouter.notFound = {
	name: 'app.errorNotFound',
	action() {
			const path = FlowRouter.current().path;
			if (path === '/' || path === '' || !_.contains(allowedLang, FlowRouter.getParam('lang'))) {
				FlowRouter.withReplaceState(() => {
					FlowRouter.go('app.home', {lang: "fr"});
				})
			}
			else if (path !== "/error/notFound") {
        const lang = FlowRouter.getParam('lang') || 'fr'
        FlowRouter.go("app.errorAccessDenied", { lang });
      }
			BlazeLayout.render('main_body', { template: 'errorAccessDenied' });
	},
};

var errorRoutes = FlowRouter.group({
  prefix: '/:lang/error',
  name: 'app.error'
});

errorRoutes.route('/accessDenied', {
  name: 'app.errorAccessDenied',
  action(params, query) {
	Meteor.call('updateSEO', {routeName: 'app.errorAccessDenied', lang: FlowRouter.getParam("lang") || "fr"})
    BlazeLayout.render('main_body', { template: 'errorAccessDenied' });
  },
});

errorRoutes.route('/notFound', {
  name: 'app.errorPageNotFound',
  action(params, query) {
	Meteor.call('updateSEO', {routeName: 'app.errorPageNotFound', lang: FlowRouter.getParam("lang") || "fr"})
    BlazeLayout.render('main_body', { template: 'errorPageNotFound' });
  },
});

errorRoutes.route('/accountNotVerified', {
  name: 'app.errorAccountNotVerified',
  action(params, query) {
    BlazeLayout.render('main_body', { template: 'errorAccountNotVerified' });
  },
});

errorRoutes.route('/notAuthenticated', {
	name: 'app.errorNotAuthenticated',
  action(params, query) {
    let target_url = Session.get('target_url');
    if (target_url) {
      FlowRouter.go('app.home', {lang: "fr"});
    }
    else
      BlazeLayout.render('main_body', { template: 'errorNotAuthenticated' });
  },
});

errorRoutes.route('/internalError', {
	name: 'app.errorInternalError',
  action(params, query) {
    BlazeLayout.render('main_body', { template: 'errorInternalError' });
  },
});

errorRoutes.route('/disabledAccount', {
	name: 'app.error.disabledAccount',
  action(params, query) {
    BlazeLayout.render('main_body', { template: 'errorDisabledAccount' });
  },
});
