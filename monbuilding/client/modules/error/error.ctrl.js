import { Template } from "meteor/templating";
import "./error_templates.html";

Template.errorAccessDenied.events({
	'click #go_home' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go('/' + lang);
	},
	'click #go_connect' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go("app.login.login", {lang});		
	}
});

Template.errorNotAuthenticated.events({
	'click #go_home' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go('/' + lang);
	},
	'click #go_connect' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go("app.login.login", {lang});		
	}
});

Template.errorDisabledAccount.events({
	'click #go_home' (event, template) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    if (Meteor.user() && Meteor.loggingIn()) {
      $('.disconnecting').css('display', 'block')
      Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
        FlowRouter.go('app.home', { lang });
      });
    }
    else
      FlowRouter.go("app.login.login", { lang });		
	}
});

Template.errorAccountNotVerified.events({
	'click #go_home' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go("/" + lang);
	},
	'click #go_connect' (event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		if (Meteor.user() && Meteor.loggingIn()){
      $('.disconnecting').css('display', 'block')
			Meteor.logout(() => {
        $('.disconnecting').css('display', 'none')
				FlowRouter.go('app.home', {lang});
			});
		}
		else
			FlowRouter.go("app.login.login", {lang});		
	}
});
