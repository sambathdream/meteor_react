import { Template } from "meteor/templating";
import "./header.view.html";
import {Accounts} from 'meteor/accounts-base';

function checkInputs(template, firstname, lastname, email, password, passwordConf, oldPassword) {
    if (!firstname || firstname == undefined || firstname == "") {
      $("[name='firstname']").addClass("errorModalInput");
      template.isError.set(true);
    }
    if (!lastname || lastname == undefined || lastname == "") {
      $("[name='lastname']").addClass("errorModalInput");
      template.isError.set(true);
    }
    if (!email || email == undefined || email == "") {
      $("[name='email']").addClass("errorModalInput");
      template.isError.set(true);
    }

    if (password != passwordConf) {
      $("[name='password']").addClass("errorModalInput");
      $("[name='passwordConf']").addClass("errorModalInput");
      $("[name='oldPassword']").addClass("errorModalInput");
      template.isPasswordError.set(true);
    }
};

function removeErrorClass(template) {
    template.isError.set(false);
    template.isPasswordError.set(false);
    $("[name='firstname']").removeClass("errorModalInput");
    $("[name='lastname']").removeClass("errorModalInput");
    $("[name='email']").removeClass("errorModalInput");
    $("[name='oldPassword']").removeClass("errorModalInput");
    $("[name='oldPassword']").removeClass("errorModalInput");
    $("[name='passwordConf']").removeClass("errorModalInput");
}

function isSame(template, firstname, lastname, email) {
  let oldFirstname = template.firstname.get();
  let oldLastname = template.lastname.get();
  let oldEmail = template.email.get();

  if (oldFirstname == firstname && oldLastname == lastname && oldEmail == email)
    return true;
  return false;
}

Template.header.onCreated(function(){
    this.isError = new ReactiveVar(false);
    this.isPasswordError = new ReactiveVar(false);
    this.isOpen = new ReactiveVar(false);
    this.firstname = new ReactiveVar();
    this.lastname = new ReactiveVar();
    this.email = new ReactiveVar();
});

Template.header.onRendered(function() {
  if (Meteor.user() && Meteor.user().profile) {
    Template.instance().firstname.set(Meteor.user().profile.firstname);
    Template.instance().lastname.set(Meteor.user().profile.lastname);
    Template.instance().email.set(Meteor.user().emails[0].address);
  }
});

Template.header.events({

   'click .linkHomeBack' : function (event, template) {
       event.preventDefault();
     FlowRouter.go('app.backoffice.managerList')
   },
  'hidden.bs.modal #changeAdminId': function(event, template) {
    template.isOpen.set(false);
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #changeAdminId': function(event, template) {
    let modal = $(event.currentTarget);
    template.isOpen.set(true);
    window.location.hash = "#changeAdminId";
    window.onhashchange = function() {
      if (location.hash != "#changeAdminId"){
        modal.modal('hide');
      }
    }
  },
  'click .deconnectMenu': function(event, template) {
    Session.set('target_url', null);
    $('.disconnecting').css('display', 'block')
    Meteor.logout(() => {
      $('.disconnecting').css('display', 'none')
    	FlowRouter.go('app.home', {lang: 'fr'});
    });
  },
  'click #goprofile': function(event, template) {
    FlowRouter.go("/profile");
  },

  'click #saveAdminId': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    $("#saveAdminId").button("loading");
    let firstname = $("[name='firstname']").val();
    let lastname = $("[name='lastname']").val();
    let email = $("[name='email']").val();
    let oldPassword = $("[name='oldPassword']").val();
    let password = $("[name='password']").val();
    let passwordConf = $("[name='passwordConf']").val();
    let _isError = false;

    removeErrorClass(template);
    checkInputs(template, firstname, lastname, email, password, passwordConf, oldPassword);
    let isSameInfos = isSame(template, firstname, lastname, email);
    if (template.isError.get() == false && isSameInfos == false) {
      Meteor.call("updateAdmin", {firstname, lastname, email}, function(error) {
        $("#saveAdminId").button("reset");
        if (!error) {
          sAlert.success("Modifications enregistrées aves succès (autres que mot de passe)");
        }
        else {
          sAlert.error(error);
        }
      })
    }
    if (template.isPasswordError.get() == false) {
      if (password != "")
        Accounts.changePassword(oldPassword, password, function(error) {
          $("#saveAdminId").button("reset");
          if (!error) {
            sAlert.success("Modifications du mot de passe effectué");
            $('#changeAdminId').hide();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
          }
          else
            sAlert.error(error);
        });
      else {
        $("#saveAdminId").button("reset");
        $('#changeAdminId').hide();
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
      }
    }
  }
});

Template.header.helpers({
  'user' : function() {
    return (Meteor.user().emails[0].address);
  },
  firstLetters: () => {
      return Meteor.user() ? Meteor.user().profile.firstname[0].toUpperCase() + Meteor.user().profile.lastname[0].toUpperCase() : "";
  },
  getAdminFirstname: () => {
    return Template.instance().firstname.get();
  },
  getAdminLastname: () => {
    return Template.instance().lastname.get();
  },
  getAdminEmail: () => {
    return Template.instance().email.get();
  },
  isError: () => {
    return Template.instance().isError.get();
  },
  isPasswordError: () => {
    return Template.instance().isPasswordError.get();
  },
  isOpen: () => {
    return Template.instance().isOpen.get();
  }
});


