Template.feedback.onCreated(function() {
    let userId = FlowRouter.current().queryParams.userid;
    let comment = FlowRouter.current().queryParams.comment;
    let condoId = FlowRouter.current().queryParams.condoid;
    this.ret = new ReactiveVar(true);
    Meteor.call('updateUserCommentArchive', userId, condoId, comment, (err, res) => {
        this.ret.set(res);
    });
});

Template.feedback.events({
    // 'click #go_home' (event, template) {
    //     FlowRouter.go('app.home');
    // }
});

Template.feedback.helpers({
    ret: () => { return Template.instance().ret.get(); },
    backHome: () => {
      return () => FlowRouter.go('app.home')
    }
});