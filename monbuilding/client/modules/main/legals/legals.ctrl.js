import { Session } from 'meteor/session';

Template.legals.onCreated(function() {

});

Template.legals.events({
    'click #goHome' (event, template) {
        if (Session.get('backFromCgu')){
            FlowRouter.go(Session.get('backFromCgu'));
        }else{
            const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
            FlowRouter.go('app.home', {lang: lang});
        }
    },
});

Template.legals.helpers({
});
