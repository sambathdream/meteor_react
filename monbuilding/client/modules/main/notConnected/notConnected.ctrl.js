Template.notConnectedTemplate.onCreated(function () {
    this.autorun(function() { 
        let sessionVar = Session.get('notConnected')
        if (sessionVar === true) {
            $('.notConnected').css('display', '')
        } else {
            $('.notConnected').css('display', 'none')
        }
    });
});

Template.notConnectedTemplate.onRendered(function () {
});

Template.notConnectedTemplate.events({
    'click .__retryBtn': (event, template) => {
        window.location.reload();
    }
});

Template.notConnectedTemplate.helpers({
});