import { Session } from 'meteor/session';

Template.cgu.onCreated(function() {

});

Template.cgu.events({
    'click #goHome' (event, template) {
        if (Session.get('backFromCgu')){
            FlowRouter.go(Session.get('backFromCgu'));
        }else{
            const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
            FlowRouter.go('app.home', {lang: lang});
        }
    },
});

Template.cgu.helpers({
  getDomainConfig: () => {
    return domainConfig || {}
  },
  getUrl: () => {
    const url = window.location.origin
    if (!url || window.location.hostname === 'localhost') return 'https://www.monbuilding.com'
    return url
  }
});
