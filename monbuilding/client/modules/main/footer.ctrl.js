
import { Template } from "meteor/templating";
import { FileManager } from "../../components/fileManager/filemanager.js";
import {TweenMax, Power2, TimelineLite} from 'gsap';
import "./footer.view.html";

Template.footer.onCreated(function () {

});


Template.footer.onRendered(function() {

});



Template.footer.events({

    'click [name="infos"]': function(event, template) {
        const name = $(event.currentTarget).attr("id");
        const lang = FlowRouter.getParam("lang") || "fr";
        $('html, body').animate({scrollTop : 0},800);
        FlowRouter.go("app.infos." + name, {lang: lang});
    },
	'click #aboutus' (event, template) {
        const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
        $('html, body').animate({scrollTop : 0},800);
        FlowRouter.go('app.infos.aboutus', {lang: lang});
    },
    'click #legalnotice' (event, template) {
        // const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
        // $('html, body').animate({scrollTop : 0},800);
        // FlowRouter.go('app.services.cgu', {lang: lang});
        event.preventDefault();
        event.stopPropagation();
        Session.set('backFromCgu', FlowRouter.current().path);
        let lang = FlowRouter.getParam('lang') || 'fr'
        const win = window.open("/" + lang + "/services/legals/", '_blank');
        win.focus();
    },
    'click .back-to-top' (event, template) {
      $('html, body').animate({scrollTop : 0}, 800);
    }
});

Template.footer.helpers({

});


