import { Template } from "meteor/templating"

Template.test_main.onCreated(function () {
  this.pageCounter = new ReactiveVar(30)
  this.totalElem = null
  this.requestDate = new ReactiveVar(Date.now())  // Actu, Forum, Classified, UsersJoined
  this.oldWayRequestDate = new Date() // Incident
  this.recentlyJoinedUsers = new ReactiveVar([])
  this.mostViewedAds = new ReactiveVar([])
  this.mostViewedForum = new ReactiveVar([])

  this.elemRequested = {
    incidentCounter: 0,
    forumCounter: 0,
    classifiedCounter: 0,
    actuCounter: 0,
    usersJoinedCounter: 0
  }

  this.elemDisplayed = {
    Incident: 0,
    Forum: 0,
    Classified: 0,
    Actu: 0,
    UsersJoined: 0
  }

})

Template.test_main.onRendered(function () {

})

Template.test_main.events({
  'click .logMeIn': () => {
    Meteor.logout(() => {
      Meteor.loginWithPassword('resident1@gmail.com', 'resident', function (error) {
        console.log('error', error)
      })
      console.log('disconnected')
    })
  },
  'click .logMeInM': () => {
    Meteor.logout(() => {
      Meteor.loginWithPassword('m@m.mm', 'm', function (error) {
        console.log('error', error)
      })
      console.log('disconnected')
    })
  },
  'click .logMeOut': () => {
    Meteor.logout(() => {
      console.log('disconnected')
    })
  },
  'click .loadFeedM': () => {
    console.time('feed')
    console.time('board_gestionnaire')
    let self = Template.instance()
    self.subscribe('board_gestionnaire', function (error) {
      console.timeEnd('board_gestionnaire')
      let usersEnterprise = Enterprises.findOne().users
      let condoIds = _.map(_.find(usersEnterprise, (user) => { return user.userId === Meteor.userId() }).condosInCharge, (condo) => { return condo.condoId })
      console.time('usersProfile')
      self.subscribe('usersProfile', condoIds, function (error, result) {
        console.timeEnd('usersProfile')
        console.time('condodocuments')
        self.subscribe('condodocuments', function (error, result) {
          console.timeEnd('condodocuments')
          console.time('resources')
          self.subscribe('resources', function (error, result) {
            console.timeEnd('resources')
            console.time('avatarLinksGestionnaire')
            self.subscribe('avatarLinksGestionnaire', function (error, result) {
              console.timeEnd('avatarLinksGestionnaire')
              console.time('getDefaultRoles')
              self.subscribe('getDefaultRoles', function (error, result) {
                console.timeEnd('getDefaultRoles')
                console.time('getCondosRole')
                self.subscribe('getCondosRole', function (error, result) {
                  console.timeEnd('getCondosRole')
                  console.time('badges')
                  self.subscribe('badges', function (error, result) {
                    console.timeEnd('badges')
                    console.timeEnd('feed')
                  })
                })
              })
            })
          })
        })
      })
    })

  },
  'click .feedLoad': () => {
    let self = Template.instance()
    console.time('feed')
    console.time('board_resident')
    self.subscribe('board_resident', () => {
      console.timeEnd('board_resident')
      const resident = Residents.findOne({ userId: Meteor.userId() })
      const condos = resident.getCondos()
      console.time('avatarLinks')
      self.subscribe("avatarLinks", 'niuSbhant98XgcD7Q', function (error, result) {
        console.timeEnd('avatarLinks')
        console.time('getDefaultRolesFor')
        self.subscribe('getDefaultRolesFor', "occupant", function (error, result) {
          console.timeEnd('getDefaultRolesFor')
          console.time('getCondoRole')
          self.subscribe('getCondoRole', 'niuSbhant98XgcD7Q', function (error, result) {
            console.timeEnd('getCondoRole')
            console.time('userStatus')
            self.subscribe('userStatus', function (error, result) {
              console.timeEnd('userStatus')
              console.time('usersProfile')
              self.subscribe('usersProfile', ['niuSbhant98XgcD7Q'], function (error, result) {
                console.timeEnd('usersProfile')
                console.time('badges')
                self.subscribe('badges', 'niuSbhant98XgcD7Q', () => {
                  console.timeEnd('badges')
                  console.time('classifiedsFeedAnswerCounter')
                  self.subscribe('classifiedsFeedAnswerCounter', ['niuSbhant98XgcD7Q'], function (error, result) {
                    console.timeEnd('classifiedsFeedAnswerCounter')
                    console.time('forumFeedAnswerCounter')
                    self.subscribe('forumFeedAnswerCounter', ['niuSbhant98XgcD7Q'], function (error, result) {
                      console.timeEnd('forumFeedAnswerCounter')
                      console.time('feedIncidentInformation')
                      self.subscribe('feedIncidentInformation', ['niuSbhant98XgcD7Q'], function (error, result) {
                        console.timeEnd('feedIncidentInformation')
                        console.time('newPostFeed')
                        self.subscribe('newPostFeed', 'niuSbhant98XgcD7Q', self.requestDate.get(), self.oldWayRequestDate, function (error, result) {
                          console.timeEnd('newPostFeed')
                          const pageCounter = self.pageCounter.get()
                          const requestedDate = self.requestDate.get()
                          const previousRequested = self.elemRequested
                          if (previousRequested.incidentCounter === 0) {
                            self.elemRequested.incidentCounter = 30
                            self.elemRequested.forumCounter = 30
                            self.elemRequested.classifiedCounter = 30
                            self.elemRequested.actuCounter = 30
                            self.elemRequested.usersJoinedCounter = 30
                          } else {
                            self.elemRequested.incidentCounter = (previousRequested.incidentCounter - self.elemDisplayed.Incident) > 10 ? previousRequested.incidentCounter : previousRequested.incidentCounter + (10 - (previousRequested.incidentCounter - self.elemDisplayed.Incident))
                            self.elemRequested.forumCounter = (previousRequested.forumCounter - self.elemDisplayed.Forum) > 10 ? previousRequested.forumCounter : previousRequested.forumCounter + (10 - (previousRequested.forumCounter - self.elemDisplayed.Forum))
                            self.elemRequested.classifiedCounter = (previousRequested.classifiedCounter - self.elemDisplayed.Classified) > 10 ? previousRequested.classifiedCounter : previousRequested.classifiedCounter + (10 - (previousRequested.classifiedCounter - self.elemDisplayed.Classified))
                            self.elemRequested.actuCounter = (previousRequested.actuCounter - self.elemDisplayed.Actu) > 10 ? previousRequested.actuCounter : previousRequested.actuCounter + (10 - (previousRequested.actuCounter - self.elemDisplayed.Actu))
                            self.elemRequested.usersJoinedCounter = (previousRequested.usersJoinedCounter - self.elemDisplayed.UsersJoined) > 10 ? previousRequested.usersJoinedCounter : previousRequested.usersJoinedCounter + (10 - (previousRequested.usersJoinedCounter - self.elemDisplayed.UsersJoined))
                          }
                          console.time('recentlyJoinedUsers')
                          Meteor.call('recentlyJoinedUsers', 'niuSbhant98XgcD7Q', function (error, result) {
                            console.timeEnd('recentlyJoinedUsers')
                            if (!error && result) {
                              self.recentlyJoinedUsers.set(result)
                              console.time('mostViewedAds')
                              Meteor.call('mostViewedAds', 'niuSbhant98XgcD7Q', function (error, result) {
                                console.timeEnd('mostViewedAds')
                                if (!error && result) {
                                  self.mostViewedAds.set(result)
                                  console.time('mostViewedForum')
                                  Meteor.call('mostViewedForum', 'niuSbhant98XgcD7Q', function (error, result) {
                                    console.timeEnd('mostViewedForum')
                                    if (!error && result) {
                                      self.mostViewedForum.set(result)
                                      console.time('feedIncident')
                                      self.subscribe('feedIncident', 'niuSbhant98XgcD7Q', self.elemRequested.incidentCounter, self.oldWayRequestDate, function (error, result) {
                                        console.timeEnd('feedIncident')
                                        console.time('feedForum')
                                        self.subscribe('feedForum', 'niuSbhant98XgcD7Q', self.elemRequested.forumCounter, requestedDate, function (error, result) {
                                          console.timeEnd('feedForum')
                                          console.time('feedClassified')
                                          self.subscribe('feedClassified', 'niuSbhant98XgcD7Q', self.elemRequested.classifiedCounter, requestedDate, function (error, result) {
                                            console.timeEnd('feedClassified')
                                            console.time('feedActu')
                                            self.subscribe('feedActu', 'niuSbhant98XgcD7Q', self.elemRequested.actuCounter, requestedDate, function (error, result) {
                                              console.timeEnd('feedActu')
                                              console.time('feedUsersJoined')
                                              self.subscribe('feedUsersJoined', 'niuSbhant98XgcD7Q', self.elemRequested.usersJoinedCounter, requestedDate, function (error, result) {
                                                console.timeEnd('feedUsersJoined')
                                                console.timeEnd('feed')
                                              })
                                            })
                                          })
                                        })
                                      })
                                    }
                                  })
                                }
                              })
                            }
                          })
                        })
                      })
                    })
                  })
                })
              })
            })
          })
        })
      })
    })
    // self.subscribe('userWithJoinedDate', 'niuSbhant98XgcD7Q', function (error, result) {

    // })
    // @KUYA HERE YOU GO



    console.log('new Date()2', moment(Date.now()).format("ssSSS"))

  }
})

Template.test_main.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
})
