import { Template } from "meteor/templating";
import "./getapp.view.html";

function getMobileOperatingSystem() {
let userAgent = navigator.userAgent || navigator.vendor || window.opera;
	// Windows Phone must come first because its UA also contains "Android"
	if (/windows phone/i.test(userAgent))
		return "Windows Phone";
	if (/android/i.test(userAgent))
		return "Android";
	// iOS detection from: http://stackoverflow.com/a/9039885/177710
	if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
		return "iOS";
	return "unknown";
}

Template.getapp.onCreated(function() {
	if (!localStorage.getItem("getappViewed"))
		localStorage.setItem("getappViewed", 1);
	else
		localStorage["getappViewed"]++;
});

Template.getapp.onRendered(function() {
});

Template.getapp.events({
	'click #getappButton': function(event, template) {
		let OS = getMobileOperatingSystem();
		if (OS == "iOS")
			window.location.replace("http://itunes.com/apps/monbuilding");
		else
			window.location.replace("https://play.google.com/store/apps/details?id=com.monbuilding.app");
	}
});

Template.getapp.helpers({
  getPhoneImgSrc: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'

    return `/img/homeV2/phoneOverview-${lang}.png`
  }
});
