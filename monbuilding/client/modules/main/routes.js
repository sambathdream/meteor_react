import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

const allowedLang = ["fr", "en"];

FlowRouter.triggers.exit(context => {
  Session.set('lastRoute', { name: context.route.name, params: context.params });
});

FlowRouter.route('/sitemap.xml', {
	name: 'app.sitemap.xml'
});

FlowRouter.route('/:lang/testPerf', {
  name: 'app.testPerf',
  action() {
    BlazeLayout.render('test_main');
  }
})

FlowRouter.route('/:lang', {
	name: 'app.home',
	action() {
		if (!_.contains(allowedLang, FlowRouter.getParam('lang'))) {
				FlowRouter.withReplaceState(() => {
					FlowRouter.go('app.home', {lang: "fr"});
				})
		}
		else
			BlazeLayout.render('main_body', { template: 'home' });
	},
	subscriptions: function(){
		this.register('Ruser', Meteor.subscribe('userCollection'));
	}
});

FlowRouter.route('/api/payzen', {
  name: 'app.api.payzen',
  action: function(params, query) {
  }
})

FlowRouter.route('/api/payzen/threeDsResult', {
  name: 'app.api.payzen.3ds',
  action: function(params, query) {
    console.log('yo')
    window.close()
  }
})

FlowRouter.route('/api/payzen/:status/:paymentId', {
  name: 'app.api.payzen.status.paymentId',
  action: function(params, query) {
    console.log('yo')
    window.close()
  }
})

let servicesRoutes = FlowRouter.group({
	prefix: "/:lang/services",
	name: 'app.services',
    triggersEnter: [() => {
        window.scrollTo(0, 0);
    }]
});

servicesRoutes.route('/cgu', {
    name: 'app.services.cgu',
    action() {
        BlazeLayout.render('main_body', {template: 'cgu'});
    }
});

servicesRoutes.route('/cgu_old', {
    name: 'app.services.cgu_old',
    action() {
        BlazeLayout.render('main_body', {template: 'cgu_old'});
    }
});

servicesRoutes.route('/legals', {
    name: 'app.services.legals',
    action() {
        BlazeLayout.render('main_body', {template: 'legals'});
    }
});

servicesRoutes.route('/cgu_mobile', {
    name: 'app.services.cgu_mobile',
    action() {
        BlazeLayout.render('main_body', {template: 'cgu_mobile'});
    }
});

servicesRoutes.route('/set-password/:userId', {
    name: 'app.services.setPassword',
    action() {
        BlazeLayout.render('main_body', {template: 'setPassword'});
    }
});

servicesRoutes.route('/feedback', {
    name: 'app.services.feedback',
    action() {
        BlazeLayout.render('main_body', {template: 'feedback'});
    }
});

servicesRoutes.route('/deferredRegistration/:userId', {
  name: 'app.services.deferredRegistration',
  action() {
    BlazeLayout.render('main_body', {template: 'deferredRegistration'})
  }
})

let login = FlowRouter.group({
	prefix: "/:lang",
	name: 'app.login',
	triggersEnter: [
		function(context, redirect) {
			if (!_.contains(allowedLang, context.params.lang))
				FlowRouter.setParams({lang: 'fr'});
			// Meteor.call('updateSEO', {routeName: context.route.name, lang: context.params.lang || "fr"})
		}
	]
});

login.route('/auto-login', {
    name: 'app.login.auto_login',
    action() {
        BlazeLayout.render('main_body', { template: 'auto_login' })
    }
})

login.route('/create_successful', {
	name: 'app.login.create_successful',
	action() {
		BlazeLayout.render('main_body', { template: 'create_successful' });
	}
});

login.route('/login', {
    name: 'app.login.login',
    action() {
        BlazeLayout.render('main_body', { template: 'login' });
    }
});

login.route('/signup', {
    name: 'app.login.signup',
    action() {
        BlazeLayout.render('main_body', { template: 'signup' });
    }
});

login.route('/satisfaction/:archiveId', {
    name: 'app.login.satisfaction',
    action() {
        BlazeLayout.render('main_body', { template: 'satisfaction' });
    }
});

let resetPassword = FlowRouter.group({
    prefix: "/:lang/forgot-password",
    name: 'app.forgot_password',
});

resetPassword.route('/', {
  name: 'app.forgot_password.request',
  action: () => {
    BlazeLayout.render('main_body', { template: 'forgot_password' });
  }
})

resetPassword.route('/reset/:resetToken?', {
  name: 'app.forgot_password.reset',
  action() {
    BlazeLayout.render('main_body', { template: 'reset_password' });
  }
});

resetPassword.route('/:email', {
  name: 'app.forgot_password.email',
  action: () => {
    BlazeLayout.render('main_body', { template: 'forgot_password' });
  }
})

let infos = FlowRouter.group({
    prefix: "/:lang",
    name: 'app.infos'
});

infos.route('/aboutus', {
    name: 'app.infos.aboutus',
    action() {
        BlazeLayout.render('main_body', { template: 'aboutus' });
    }
});

infos.route('/faq', {
    name: 'app.infos.faq',
    action() {
        BlazeLayout.render('main_body', { template: 'faq' });
    }
});

infos.route('/ourvalues', {
    name: 'app.infos.ourvalues',
    action() {
        BlazeLayout.render('main_body', { template: 'ourvalues' });
    }
});

infos.route('/getapp', {
    name: 'app.infos.getapp',
    action() {
        BlazeLayout.render('main_body', { template: 'getapp' });
    }
});

