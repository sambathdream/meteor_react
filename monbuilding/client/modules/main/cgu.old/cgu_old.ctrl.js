import { Session } from 'meteor/session';

Template.cgu_old.onCreated(function() {

});

Template.cgu_old.events({
    'click #goHome' (event, template) {
        if (Session.get('backFromCgu')){
            FlowRouter.go(Session.get('backFromCgu'));
        }else{
            const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
            FlowRouter.go('app.home', {lang: lang});
        }
    },
});

Template.cgu_old.helpers({

});
