import { Template } from "meteor/templating";
import { Main } from "./../../../common/lang/lang";
import "./main.view.html";

Tracker.autorun(function () {
  if (Meteor.userId()) {
    try {
      if (!UserStatus.isMonitoring()) {
        UserStatus.startMonitor({
          threshold: 30000,
          interval: 1000,
          idleOnBlur: false
        });
      }
    } catch (err) {
    }
  } else {
    if (UserStatus.isMonitoring()) {
      UserStatus.stopMonitor();
    }
  }
});

Template.main_body.helpers({
  hasReactComponent: function () {
    return !_.isUndefined(FlowRouter.current().route.options.reactComponent);
  },
  reactComponent: function () {
    return FlowRouter.current().route.options.reactComponent();
  },
  getSubTemplate: () => {
    if (Template.currentData().sub_template) {
      return { sub_template: Template.currentData().sub_template() }
    }
    return {}
  }
});

Template.main_body.onCreated(function () {
  let palette = Session.get('defaultColorPalette');
  document.body.style.setProperty("--primary-color", palette.primary);
  document.body.style.setProperty("--secondary-color", palette.secondary);
  document.body.style.setProperty("--third-color", palette.third || '#fdb813');
})

Template.main_body.onRendered(function () {

  setTimeout(function () {
    let user = Meteor.user();
    if (Meteor.userId() != null && user != null && user.identities && FlowRouter.current().route.name == "app.home") {
      if (user.identities.adminId !== undefined)
        FlowRouter.go('app.backoffice.managerList')
      else if (user.identities.residentId !== undefined) {
        const lang = (FlowRouter.getParam("lang") || "fr");

        if (user.lastCondoId) {
          const condo = {
            lang: lang,
            condo_id: user.lastCondoId,
          };
          FlowRouter.go('app.board.resident.condo', condo);
        }
        else
          FlowRouter.go('app.board.resident.home', { lang });
      }
      else if (user.identities.gestionnaireId !== undefined) {
        let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
        if (gestionnaire) {
          gestionnaire = _.find(gestionnaire.users, function (elem) {
            return elem.userId == user._id;
          });
          const lang = (FlowRouter.getParam("lang") || "fr");
          FlowRouter.go(`/${lang}/gestionnaire/buildingsview`);
        }
      }
    }
  }, 100)
})

Meteor.startup(function () {
  emptyFormByParentId = (id) => {
    let inputs = $("#" + id + " :input");
    emptyByElem(inputs);
  };

  emptyByElem = (elements) => {
    _.each(elements, function (elem) {
      switch (elem.type) {
        case "radio":
          $(elem).attr("checked", false);
          break;
        case "checkbox":
          $(elem).attr("checked", false);
          break;
        case "select":
          if (!$(elem).hasClass('select2-hidden-accessible')) {
            $(elem).prop('selectedIndex', 0);
          } else {
            // select2
            $(elem).val(null).trigger("change");
          }
          break;
        case "select-multiple":
          if (!$(elem).hasClass('select2-hidden-accessible')) {
            $(elem).find("option:selected").prop("selected", false);
          } else {
            // select2
            $(elem).val(null).trigger("change");
          }
          break;
        default:
          $(elem).val("");
          break;
      }
    });
  };

  let translation = new Main((FlowRouter.getParam("lang") || "fr"));
  moment.tz.setDefault("UTC");

  sAlert.config({
    effect: 'scale',
    position: 'top',
    timeout: 3000,
    html: false,
    onRouteClose: true,
    stack: {
      limit: 2
    },
    offset: 0,
    beep: false,
    onClose: _.noop
  });

  bootbox.addLocale('custom', {
    OK: translation.main["ok"],
    CANCEL: translation.main["cancel"],
    CONFIRM: translation.main["confirm"]
  });

  bootbox.setDefaults({
		/**
		 * @optional String
		 * @default: en
		 * which locale settings to use to translate the three
		 * standard button labels: OK, CONFIRM, CANCEL
		 */
    locale: "custom",

		/**
		 * @optional Boolean
		 * @default: true
		 * whether the dialog should be shown immediately
		 */
    show: true,

		/**
		 * @optional Boolean
		 * @default: true
		 * whether the dialog should be have a backdrop or not
		 */
    backdrop: true,

		/**
		 * @optional Boolean
		 * @default: true
		 * show a close button
		 */
    closeButton: true,

		/**
		 * @optional Boolean
		 * @default: true
		 * animate the dialog in and out (not supported in < IE 10)
		 */
    animate: true,

		/**
		 * @optional String
		 * @default: null
		 * an additional class to apply to the dialog wrapper
		 */
    className: "bb-custom-modal"

  });
});
