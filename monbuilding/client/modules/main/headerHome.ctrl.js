import "./headerHome.view.html";


Template.headerHome.onCreated(function () {
});


Template.headerHome.onRendered(function () {

});



Template.headerHome.events({
  'click .logoGoHome': function (event, template) {
    if (!domainConfig.restrictToLoginPage) {
      const lang = (FlowRouter.getParam("lang") || "fr");
      FlowRouter.go('app.home', { lang });
    }
  },

  'click #langFr': function (event, template) {
    FlowRouter.setParams({ lang: 'fr' });
  },

  'click #langEn': function (event, template) {
    FlowRouter.setParams({ lang: 'en' });
  },
  'click #button_login': function (event, template) {
    event.preventDefault();
    event.stopPropagation();
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.login.login", { lang });
  },
  'click #button_signin': function (event, template) {
    event.preventDefault();
    event.stopPropagation();
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.login.signup", { lang });
  },
  'click #goHome'(event, template) {
    const lang = (FlowRouter.getParam('lang') ? FlowRouter.getParam('lang') : 'fr');
    FlowRouter.go('app.home', { lang: lang });
  }
});

Template.headerHome.helpers({
  langSelected: () => {
    return FlowRouter.getParam('lang') || 'fr'
  }
});
