import { Template } from 'meteor/templating';
import './setPassword.view.html';

function isResident() {
  const user = Meteor.users.findOne(FlowRouter.getParam('userId'));
	return _.has(user.identities, 'residentId');
};

function getMobileOperatingSystem() {
  let userAgent = navigator.userAgent || navigator.vendor || window.opera;
  // Windows Phone must come first because its UA also contains "Android"
  if (/windows phone/i.test(userAgent))
    return "Windows Phone";
  if (/android/i.test(userAgent))
    return "Android";
  // iOS detection from: http://stackoverflow.com/a/9039885/177710
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream)
    return "iOS";
  return "unknown";
}

Template.setPassword.onCreated(function() {
	this.userId = FlowRouter.getParam('userId');
  this.errorMessage = new ReactiveVar('');
  this.isReady = new ReactiveVar(false)
  const lang = FlowRouter.getParam('lang') || 'fr'

  let self = this
	Meteor.call('hasPassword', FlowRouter.getParam('userId'), (error, result) => {
    console.log('error', error)
    console.log('result', result)
		if (error || result != undefined ) {
			sAlert.error(error);
			FlowRouter.go('app.login.login', { lang });
		} else {
      self.isReady.set(true)
    }
	});
	Meteor.subscribe('newUserForPassword', FlowRouter.getParam('userId'));
});

Template.setPassword.events({
	'submit #set-password-form': function(event, template) {
    $('.set-pwd-btn').button('loading')
		event.preventDefault();

		$('[name=pwd]').removeClass('errorInput');
		$('[name=rePwd]').removeClass('errorInput');

		const target = event.target;
		const pwd = target.pwd.value;
		const rePwd = target.rePwd.value;
		let error = "";

		if ($("[name=cgu]").is(':checked') == false) {
			error += 'Merci d\'accepter les CGU. ';
			$('[name=cguGroup]').addClass('errorCheckbox');
		}

		if (pwd.length < 3) {
			error += 'Mot de passe incorrect (au moins 3 caractères). ';
			$('[name=pwd]').addClass('errorInput');
		}
		if (pwd !== rePwd) {
			error += 'Les mots de passes ne correspondent pas. ';
			$('[name=rePwd]').addClass('errorInput');
		}
		if (error !== "") {
      template.errorMessage.set(error);
      $('.set-pwd-btn').button('reset')
			return;
		}
		else {
			template.errorMessage.set("");
    }
    const lang = (FlowRouter.getParam("lang") || "fr");
    if (isResident()) {
      const userId = FlowRouter.getParam('userId')
			Meteor.call('setPasswordForOccupant', {
				userId: userId,
				pwd,
			}, (error, result) => {
				if (error) {
					template.errorMessage.set(error);
          $('.set-pwd-btn').button('reset')
        } else {
					Meteor.loginWithPassword(result.email, result.pwd, function (error){
            $('.set-pwd-btn').button('reset')
						if (error){
              if (error.reason === 'Your account is not activated yet') {
								FlowRouter.go('app.services.deferredRegistration', {lang, userId})
              } else {
                template.errorMessage.set(error.reason);
              }
						}
						else{
              // if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/iPhone/i)) {
              //   let OS = getMobileOperatingSystem()
              //   if (OS == "iOS") {
              //     setTimeout(function () { window.location = "http://itunes.com/apps/monbuilding" }, 25)
              //   } else {
              //     setTimeout(function () { window.location = "https://play.google.com/store/apps/details?id=com.monbuilding.app" }, 25)
              //   }
              //   window.location = "mb://"

              //   // window.location.href = 'mb://'
              //   // setTimeout(function () { window.location = "https://itunes.apple.com/appdir"; }, 25);
              // }

              let user = Meteor.user();
							if (user.identities.residentId !== undefined) {
								FlowRouter.go('app.board.resident.home', {lang});
							}
						}
					});
				}
			});
		}
		else {
      const userId = FlowRouter.getParam('userId')
			Meteor.call('setManagerPassword', userId, pwd, function(err, result) {
				if (err) {
					template.errorMessage.set(err);
          $('.set-pwd-btn').button('reset')
        } else {
					Meteor.loginWithPassword(result.email, result.password, function (error){
            $('.set-pwd-btn').button('reset')
						if (error){
              console.log('error : ', error)
              if (error.reason === 'Your account is not activated yet') {
								FlowRouter.go('app.services.deferredRegistration', {lang, userId})
              } else {
                template.errorMessage.set(error.reason);
              }
						}
						else{
							let user = Meteor.user();
							if (user.identities.adminId !== undefined)
                FlowRouter.go('app.backoffice.managerList')
							else if (user.identities.gestionnaireId !== undefined) {
								let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
								if (gestionnaire) {
									gestionnaire = _.find(gestionnaire.users, function(elem) {
										return elem.userId == user._id;
									});
                  const lang = (FlowRouter.getParam("lang") || "fr");
                  FlowRouter.go(`/${lang}/gestionnaire/buildingsview`);
								}
							}
						}
					});
				}
			});
		}
	},
	'click .goCgu': function(event, template) {
		event.preventDefault();
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	}
});

Template.setPassword.helpers({
  isReady: () => {
    return Template.instance().subscriptionsReady() && Template.instance().isReady.get()
  },
	user: () => {
		let u = Meteor.users.findOne(FlowRouter.getParam('userId'));
		if (u) {
			u.email = u.emails[0].address;
			return u;
		}
	},
	errorMessage: () => {
		return Template.instance().errorMessage.get();
	},
	hasInvitedByGestionnaire: () => {
		let r = Residents.findOne({userId: FlowRouter.getParam('userId')});
		if (r && r.pendings[0].invitByGestionnaire)
			return "disabled";
	},
	userCanAccess:() => {
		let user = Residents.findOne({userId: FlowRouter.getParam('userId')});
		if (user && user.services && user.services.password) {
			return false;
		}
		return true;
	}
});
