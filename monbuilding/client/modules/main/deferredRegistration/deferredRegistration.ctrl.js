import { CommonTranslation } from '/common/lang/lang'

Template.deferredRegistration.onCreated(function() {
});

Template.deferredRegistration.onDestroyed(function() {
});

Template.deferredRegistration.onRendered(function() {
});

Template.deferredRegistration.events({
	'click #backToHome': function() {
    let lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go("app.home", {lang: lang})
	}
});

Template.deferredRegistration.helpers({
  getDeferredRegistration: () => {
    const user = Meteor.users.findOne(FlowRouter.getParam('userId'))
    return user.deferredRegistrationDate
  }
});