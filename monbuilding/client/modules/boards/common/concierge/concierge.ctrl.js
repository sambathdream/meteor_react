import "./concierge.view.html";
import {fieldsFr, fieldsEn} from "./concierge.fields.js";
import { ModuleConcierge, InternalError } from "/common/lang/lang.js";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import MbTextArea from '/client/components/MbTextArea/MbTextArea';

Template.module_concierge.onCreated(function(){
	this.condoId = new ReactiveVar(FlowRouter.getParam("condo_id") || FlowRouter.getParam("condoId"))
	this.lang = new ReactiveVar(FlowRouter.getParam("lang") || 'fr')

	if (!this.condoId.get()) {
    let condoId = Condos.findOne()._id;
		if (Meteor.isManager)
			return FlowRouter.go('app.gestionnaire.concierge.condo', {condoId: condoId});
		else {
			return FlowRouter.go('app.board.resident.condo.module.concierge', {condo_id: condo._id});
		}
	}

	this.services = new ReactiveVar()
  this.isAllSelected = new ReactiveVar()
	this.files = new ReactiveVar([])
	this.form = new ReactiveDict()

	this.form.setDefault({
		category: null,
		request: null,
		service: null,
		detail: null,
		message: null,
		files: []
	})

  this.serviceTypeOptions = new ReactiveVar([])
	this.serviceDetailOptions = new ReactiveVar([])

	this.fm = new FileManager(UserFiles, {userId: Meteor.userId(), concierge: true});

	// Meteor.subscribe('conciergeMessage');
	Meteor.call('setNewAnalytics', {type: "module", module: "concierge", accessType: "web", condoId: this.condoId.get()});
});

Template.module_concierge.onRendered(function() {
  // let condo = Condos.findOne({ _id: Template.instance().condoId.get() })

  // if (condo && condo.settings.options.conciergerie === false) {
  //   if (Meteor.isManager) {
  //     const t = Template.instance()
  //     Meteor.defer(() => {
  //       return FlowRouter.go('app.gestionnaire.buildingsview', { lang: t.lang.get() });
  //     })
  //   } else
  //     return FlowRouter.go('/fr/resident');
  // }
})
Template.module_concierge.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "concierge", accessType: "web", condoId: Template.instance().condoId.get()});
})

Template.module_concierge.events({
	'click #submit-concierge': (e, t) => {
		$(e.currentTarget).button('loading')
		const data = {
      category: t.form.get('category'),
      param1: t.form.get('request'),
      param2: t.form.get('service'),
      param3: t.form.get('detail'),
      message: t.form.get('message'),
      files: [],
			condoId: t.condoId.get()
		}

    const lang = (FlowRouter.getParam("lang") || "fr")
    const translation = new ModuleConcierge(lang);
    // Pls use this to translate errors below
    const intern_translate = new InternalError(lang)

		t.fm.batchUpload(t.files.get())
      .then(files => {
        data.files = _.map(files, f => f._id);

        Meteor.call('addMessageConcierge', data, (error) => {
          $(e.currentTarget).button('reset')
          if (!error) {
            t.form.clear();
            t.files.set([])
            sAlert.success(translation.moduleConcierge["message_succefully_send"]);
          } else {
            sAlert.error(error);
          }
        })
      })
      .catch(err => {
        console.error(err);
        $(e.currentTarget).button('reset')
        sAlert.error(err.reason);

      })
	}
});

Template.module_concierge.helpers({
  disableSubmit: () => {
  	const t = Template.instance()

		return !t.form.get('message')
	},
  isAllSelected: () => Template.instance().isAllSelected.get(),
  refreshCondoId: () => {
    if (Meteor.isManager) {
      const condoId = Template.currentData().selectedCondoId
      const t = Template.instance()
      if (Meteor.isManager) {
        t.isAllSelected.set(condoId === 'all')
        if (condoId !== FlowRouter.getParam('condoId')) {
          FlowRouter.setParams({ 'condoId': condoId })
          t.condoId.set(condoId)
        }
      } else {
        if (condoId !== FlowRouter.getParam('condo_id')) {
          FlowRouter.setParams({ 'condo_id': condoId })
          t.condoId.set(condoId)
        }
      }
    }
    return true
  },
	notNull: (key) => {
    return Template.instance().form.get(key) !== null && typeof Template.instance().form.get(key) !== 'undefined'
	},
  getAvailableServices: () => {
    const translation = new ModuleConcierge((FlowRouter.getParam("lang") || "fr"));

    const availableServices = [{
      label: translation.moduleConcierge.intervUrg,
      value: 'Intervention urgente',
			image: '/img/icons/priorite-2.png'
    }, {
      label: translation.moduleConcierge.interv,
      value: 'Intervention non urgente',
      image: '/img/icons/priorite-1.png'
    }, {
      label: translation.moduleConcierge.estimate,
      value: 'Demande de devis',
			image: '/img/icons/svg/group.svg'
    }, {
      label: translation.moduleConcierge.service,
      value: 'Service récurrent',
			image: '/img/icons/svg/repeat.svg'
    }]

  	return availableServices
	},
	getForm: (key) => {
		const t = Template.instance()

		return () => t.form.get(key) || ''
	},
	setForm: (key) => {
    const t = Template.instance()

    return () => value => t.form.set(key, value)
	},
	isServiceActive: (value) => {
		return Template.instance().form.get('category') === value
	},
	// Service Types
	setRequestType: () => {
    const t = Template.instance()

  	return value => {
      t.form.set('request', value)
      t.form.set('detail', null)

			let fieldSelector = FlowRouter.getParam("lang") && FlowRouter.getParam("lang") === "en" ? fieldsEn : fieldsFr

			const requestTypeField = _.find(fieldSelector, f => f.name === value)
			let options = []

			// Check if elements exist
			if (requestTypeField.elements) {
        options = _.map(requestTypeField.elements, (el, i) => {
          return {
            text: el.name,
            id: el.name
          }
        })

        t.serviceTypeOptions.set(options)
			} else if (requestTypeField.fields) {
        options = _.map(requestTypeField.fields, (el, i) => {
          return {
            text: el,
            id: el
          }
        })
      }

      t.serviceTypeOptions.set(options)
		}
	},
  getServiceTypeOptions: () => {
		return Template.instance().serviceTypeOptions.get()
	},
  onSelectServiceType: () => {
  	const t = Template.instance()
    return value => {
      const serviceDetailEnabl = ['Works', 'Travaux']
			const requestType = t.form.get('request')

      if (!!value && serviceDetailEnabl.indexOf(requestType) !== -1) {
        let fieldSelector = FlowRouter.getParam("lang") && FlowRouter.getParam("lang") === "en" ? fieldsEn : fieldsFr
        const requestTypeField = _.find(fieldSelector, f => f.name === requestType)
        const detailOptions = _.find(requestTypeField.elements, e => e.name === value)

				t.serviceDetailOptions.set(_.map(detailOptions.fields, d => ({
					id: d,
					text: d
				})))
      }


      t.form.set('service', value)
		}
	},
	getServiceDetailOptions: () => Template.instance().serviceDetailOptions.get(),
	shouldShowServiceDetail: () => {
  	return ['Works', 'Travaux'].indexOf(Template.instance().form.get('request')) !== -1 && Template.instance().form.get('service') !== null
	},
  showMessageForm: () => {
  	const t = Template.instance()
  	const work = ['Works', 'Travaux'].indexOf(t.form.get('request')) !== -1

		if (work) {
  		return !!t.form.get('service') && !!t.form.get('request') && !!t.form.get('detail')
		} else {
      return !!t.form.get('service') && !!t.form.get('request')
		}
	},
	getMessage: () => Template.instance().message.get(),
  onMessageChange: () => {
  	const t = Template.instance()

		return value => t.message.set(value)
	},
	getFiles: () => Template.instance().files.get(),
  onFileRemove: () => {
    const tpl = Template.instance()

    return (fileId) => {
      const files = _.filter(tpl.files.get(), file => {
        return file.id !== fileId
      })

      tpl.files.set(files)
    }
  },
  onFileAdded: () => {
    const tpl = Template.instance()

    return (files) => {
      tpl.files.set(tpl.files.get().concat(files))

      return false
    }
  },
  MbTextArea: () => MbTextArea,
	getCondoId: () => {
		return Template.instance().condoId.get();
	},
  getRequestTypes: () => {
		if (FlowRouter.getParam("lang") === "en") {
    	return _.map(fieldsEn, function(elem, index) {
        return {name: elem.name, image: elem.image};
			})
		}
  	else {
      return (_.map(fieldsFr, function (elem, index) {
        return {name: elem.name, image: elem.image};
      }));
    }
  },
  isModuleActive: () => {
    let condo = Condos.findOne({ _id: Template.instance().condoId.get() }, { fields: {settings: true } })

    return condo && condo.settings.options.conciergerie
  }
	// CDD_cursor_concierge: () => {
	// 	return Meteor.listCondoUserHasRight('view', 'concierge')
	// },
	// condoIdCB: () => {
	// 	let template = Template.instance();
	// 	return (condoId) => {
	// 		if (Meteor.isManager)
	// 			FlowRouter.setParams({ 'condoId': condoId });
	// 		else {
	// 			FlowRouter.setParams({ 'condo_id': condoId });
	// 		}
	// 		template.condoId.set(condoId)
	// 	}
	// }
});
