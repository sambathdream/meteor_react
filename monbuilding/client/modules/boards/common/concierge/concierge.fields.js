let fieldsFr =
[{
	name: "Travaux",
	image: '/img/icons/svg/travaux.svg',
	elements: [{
		name: "Bricolage",
		fields: [
		"Réparation (placard, meuble)",
		"Fixation (étagère, tringle, tableau)",
		"Fixation store (intérieur, extérieur)",
		"Montage de meuble (Ikéa, Confo, But)",
		"Menuiserie",
		"Bricolage autre",
		]
	},
	{
		name: "Plomberie",
		fields: [
		"Sanitaire (cuisine, salle de bain)",
		"Chauffage",
		"Climatisation",
		"Gaz",
		]
	},
	{
		name: "Peinture & Sol",
		fields: [
		"Papier Peint",
		"Peinture",
		"Carrelage",
		"Parquet",
		"Revêtement de sols (moquette, Lino,...)",
		"Pose gazon synthétique",
		]
	},
	{
		name: "Serrurerie",
		fields: [
		"Ouverture d'une porte simple claquée",
		"Fourniture et pose de cornière anti effraction",
		"Sécurisation de porte",
		"Serrurerie Autre",
		]
	},
	{
		name: "Electricité",
		fields: [
		"Eclairage",
		"Prise, interrupteur",
		"Tableau électrique",
		"Multimédia, TNT, WiFi",
		"Surveillance et sécurité",
		"Electricité Autre",
		]
	},
	{
		name: "Vitrier",
		fields: [
		"Vitrage cassé - Simple vitrage",
		"Vitrage cassé - Double vitrage",
		"Vitrage cassé - Vitrage sécurité",
		"Vitrier Autre",
		]
	},
	{
		name: "Diagnostic immobilier",
		fields: [
		"Diagnostic avant travaux (suivant devis proposé par le professionnel)",
		"Diagnostic Location (suivant devis proposé par le professionnel)",
		"Diagnostic Vente (suivant devis proposé par le professionnel)",
		]
	},
	{
		name: "Ramonage",
		fields: [
		"Poele à granulés",
		"Cheminée foyer ouvert",
		"Conduit de chaudière à gaz",
		]
	},
	{
		name: "Architecte",
		fields: [
		"Réaménagement d'intérieur",
		"Création de terrasse",
		"Extension / Surélévation",
		]
	}]
},
{
	name: "Services à domicile",
  image: '/img/icons/svg/house.svg',
	fields: [
	"Cours de langue",
	"Soutien scolaire",
	"Babysitter",
	"Garde d'animaux domestiques",
	"Dépannage informatique",
	"Femme de ménage",
	"Entretien d'immeuble",
	"Traiteur",
	"Démanagement",
	"Espace de stockage",
	"Jardinage & maintenance extérieure",
	"Installation électroménager",
	"Réparation électroménager",
	"Services ménagers autre"
	]
}];
let fieldsEn =
[{
	name: "Works",
  image: '/img/icons/svg/travaux.svg',
	elements: [{
		name: "Handiwork",
		fields: [
		"Repairs furniture",
		"Fixing a rack, a rail, etc",
		" Fixation of the sun protection system (interior, exterior)",
		"Assemble furniture",
		"Carpentry",
		"Other handiwork",
		]   
	},
	{
		name: "Plumbing",
		fields: [
		"Sanitary (kitchen, bathroom)",
		"Heating",
		"AC",
		"Gas",
		]
	},
	{
		name: "Painting & flooring",
		fields: [
		"Wallpaper",
		"Painting",
		"Tiles",
		"Parquet",
		"Floor covering",
		"Artificial lawns",
		]
	},
	{
		name: "Locksmithing",
		fields: [
		"Opening of a slammed door",
		"Procurement and installation of door frame reinforcing bar ",
		"Additional security for the door",
		"Other locks services",
		]
	},
	{
		name: "Electricity",
		fields: [
		"Lighting",
		"Plug, switch",
		"Electrical board",
		"Multimedia, satellite, Wifi",
		"Surveillance & security services",
		"Other Electrician",
		]
	},
	{
		name: "Glaziery",
		fields: [
		"Broken window (simple glazing)",
		"Broken window (double glazing)",
		"Broken window (security glazing)",
		"Other Glazier services",
		]
	},
	{
		name: "Real estate diagnostic",
		fields: [
		"Diagnostic before works (as per quote)",
		"Rental diagnostic (as per quote)",
		"Sale diagnostic (as per quote)",
		]
	},
	{
		name: "Sweeping",
		fields: [
		"Wood stove ",
		"Open fireplace",
		"Gas boiler conduit",
		]
	},
	{
		name: "Architect",
		fields: [
		"Interior designer",
		"Design of roof terrace",
		"Design for added height or extensions",
		]
	}]
},
{
	name: "Household services",
  image: '/img/icons/svg/house.svg',
	fields: [
	"Language teacher",
	"Homework assistant",
	"Babysitting",
	"Pet boarding",
	"IT and electronics support",
	"Home cleaner",
	"Property maintenance",
	"Catering",
	"Moving services",
	"Storage",
	"Gardening & landscaping",
	"Install appliances",
	"Repair appliances",
	"Other household services"
	]
}];

export {fieldsEn, fieldsFr};
