import React from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { CommonTranslation } from '/common/lang/lang'
import Blaze from 'meteor/gadicc:blaze-react-component'
import { LaundryRegister } from '/common/collections/Laundry'
/* globals Meteor */
/* globals Template */
// import Blaze from 'meteor/gadicc:blaze-react-component'

/**
 * @param condoId
 * @param serial
 * @constructor
 */

class LaundryLineServices extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      count: 0
    }

    this.timer = setInterval(() => {
      this.setState({count: this.state.count + 1})
    }, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.timer)
  }

  renderTag (status) {
    const translate = this.props.translate
    return <span className={'tag-spn ' + status.tag}>{translate.commonTranslation[ status.text ]}</span>
  }

  componentWillReceiveProps (props) {
    if (
      (this.props.machine && props.machine) &&
      this.props.machine.remaining_time !== this.props.machine.remaining_time
      ) {
        this.setState({count: 0})
    }
  }

  renderRemaining(time, showSec = false) {
    if (time < 0) { return '' }
    time /= 1000
    let h = 0
    let m = 0
    let s = 0

    s = Math.floor(time % 60)
    m = Math.floor(time / 60)
    h = Math.floor(m / 60)
    m = Math.floor(m % 60)

    h = (h) ? h + ' h ' : ''
    m = (m) ? m + ' min ' : ''
    s = (s && showSec) ? s + ' s ' : ''
    return (h + m + s)
  }

  getButton () {
    let i = this.props.laundryRegister
    const machineId = this.props.idMachine
    const central = this.props.serial
    const condoId = this.props.condoId
    const type = this.props.machine.type_name

    return (
      <td class='laundry-button-container'>
        <div class={i.length > 0 || !this.props.machine.status.hasButton ? 'hidden-buttons' : ''}>
          <Blaze
            template='MbButton'
            class='danger'
            label={this.props.translate.commonTranslation['laundryRoom_this_machine']}
            onClick={function () { Meteor.call('laundryRegisterUser', condoId, central, machineId, type) }}
            className='button-notify notify-this'
          />
          <Blaze
            template='MbButton'
            class='danger'
            label={this.props.translate.commonTranslation['laundryRoom_any_machine']}
            onClick={function () { Meteor.call('laundryRegisterUserAll', condoId, central, type) }}
            className='button-notify'
          />
        </div>
        {
          (i.length > 0 && this.props.machine.status.hasButton) &&
          <div class="not-available">
            <span className='already-taken'>{this.props.translate.commonTranslation['laundryRoom_already_requested']}</span>
          </div>
        }
      </td>
    )
  }

  render () {
    // console.log(this.props.machine)
    return (
      <tr>
        <td class="number">
          <div>{this.props.machine.machine_number}</div>
        </td>
        <td class="name">
          <div>{this.props.machine.type_name}</div>
        </td>
        <td class="price">
          <div>{this.props.machine.price.toLocaleString('fr-FR', {style: 'currency', currency: 'EUR'})}</div>
        </td>
        <td class="status">
          <div>{this.renderTag(this.props.machine.status)}</div>
        </td>
        <td class="remaining">
          <div>{this.renderRemaining(this.props.machine.remaining_time - (this.state.count * 1000), true)}</div>
        </td>
        {this.getButton()}
      </tr>
    )
  }
}

LaundryLineServices.defaultProps = {
  condoId: null
}

export default withTracker((props) => {
  const condoId = props.condoId
  const lang = FlowRouter.getParam('lang') || 'fr'
  const translate = new CommonTranslation(lang)
  const laundryRegister = LaundryRegister.find({
    $or: [
      { machine: {$exists: false}, type: props.machine.type_name },
      { machine: props.idMachine }
    ]
  }).fetch()

  // const machinesList = props.machines

  return {
    laundryRegister,
    translate,
    condoId
  }
})(LaundryLineServices)
