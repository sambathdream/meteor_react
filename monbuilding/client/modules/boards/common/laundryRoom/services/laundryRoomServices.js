import React from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { CommonTranslation } from '/common/lang/lang'
import { LaundryRoom } from '/common/collections/Laundry'
import Blaze from 'meteor/gadicc:blaze-react-component'
// import { Translate } from '/common/lang/lang'
import LaundryLineServices from './laundryLineServices'
// import Blaze from 'meteor/gadicc:blaze-react-component'

/**
 * @param condoId
 * @param serial
 * @constructor
 */

class LaundryRoomServices extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
    }
  }

  componentDidUpdate (prevProps, prevState) {
  }

  render () {
    const translate = this.props.translate
    let machinesList
    
    machinesList = this.props.laundry.map((el) => {
      return <LaundryLineServices key={el._id} idMachine={el._id}
        machine={el}
        // selectionId={el.machine_number}
        // desc={el.type_name}
        // price={el.price}
        // status={el.status}
        serial={this.props.serial}
        condoId={this.props.condoId} />
    })
    if (this.props.laundry.length === 0) {
      machinesList = (
        <tr>
          <td colspan="6">
            <Blaze template="MbEmpty" title={translate.commonTranslation['error_occured']} icon='/img/icons/warningred_circle.png'></Blaze>
          </td>
        </tr>
      )
    }
    return (
      <div>
        <table className='managerDetailsContainer table'>
          <thead>
            <tr>
              <td>{translate.commonTranslation['laundryRoom_selection']}</td>
              <td>{translate.commonTranslation['laundryRoom_desc']}</td>
              <td>{translate.commonTranslation['laundryRoom_price']}</td>
              <td>{translate.commonTranslation['laundryRoom_status']}</td>
              <td>{translate.commonTranslation['laundryRoom_remaining']}</td>
              <td>{translate.commonTranslation['laundryRoom_availability_alert']}</td>
            </tr>
          </thead>
          <tbody>
            { machinesList }
          </tbody>
        </table>
      </div>
    )
  }
}

LaundryRoomServices.defaultProps = {
}

export default withTracker((props) => {
  // console.log('props', props)
  const condoId = props.condoId

  const lang = FlowRouter.getParam('lang') || 'fr'
  const translate = new CommonTranslation(lang)

  const machinesList = Meteor.subscribe('getLaundry', condoId, props.serial)
  machinesList.ready()
  const laundry = LaundryRoom.find({}).fetch()
  // console.log('machinesList', machinesList)

  return {
    laundry,
    translate,
    condoId
  }
})(LaundryRoomServices)
