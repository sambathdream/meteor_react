/* globals Meteor */
/* globals Template */
/* globals FlowRouter */
/* globals BadgesGestionnaire */
/* globals _ */
/* globals $ */

import { ReactiveVar } from 'meteor/reactive-var'
import LaundryRoomServices from './services/laundryRoomServices'

import { LaundrySettings } from '/common/collections/Laundry'
import { Condos } from '/common/collections/A - Condos'

Template.laundryRoom.onCreated(function () {
  this.tabRouter = Meteor.isManager ? 'tab' : 'wildcard'
  this.serial = new ReactiveVar(null)
  this.infoLaundry = new ReactiveVar(null)
  this.handler = Meteor.subscribe('usersProfile')
  this.laundry = new ReactiveVar(null)
  this.setting = new ReactiveVar(null)
  this.setting_tmp = new ReactiveVar(null)
  this.handlerSetting = Meteor.subscribe('getLaundrySetting')
  this.isOpen = new ReactiveVar(false)
  this.canSubmit = new ReactiveVar(false)
  // const cid = Meteor.isManager ? Template.instance().data.selectedCondoId : FlowRouter.getParam('condo_id')
  // this.laundry = this.subscribe('getLaundry', cid, this.serial.get())
})

Template.laundryRoom.onRendered(function () {

})

Template.laundryRoom.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ tab: tabName })
  },
  'hidden.bs.modal #modale-edit-laundry-info': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modale-edit-laundry-info': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)

    let modal = $(event.currentTarget)
    window.location.hash = '#modale-edit-laundry-info'
    let tmp = Object.assign({}, template.setting.get())
    template.setting_tmp.set(tmp)
    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash !== '#modale-edit-laundry-info') {
        modal.modal('hide')
      }
    }
  },
  'click #save-info-laundry' (e, t) {
    let data = t.setting_tmp.get()
    t.setting.set(data)
    Meteor.call('setLaundrySetting',
    // data._id,
    t.serial.get()[0],
    data.desc,
    data.location,
    data.opening,
    (Meteor.isManager ? Template.instance().data.selectedCondoId : FlowRouter.getParam('condo_id'))
    )
  }
})

Template.laundryRoom.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  condoIds: () => {
    return Meteor.listCondoUserHasRight('laundryRoom', 'see')
  },
  reactComponent: () => {
    return LaundryRoomServices
  },
  getCondoId: () => {
    return Meteor.isManager ? Template.instance().data.selectedCondoId : FlowRouter.getParam('condo_id')
  },
  updateSerial: () => {
    const t = Template.instance()
    Meteor.call('getListOfCentral', (Meteor.isManager ? Template.currentData().selectedCondoId : FlowRouter.getParam('condo_id')), (error, result) => {
      const tabRouter = t.tabRouter
      if (!result) {
        FlowRouter.setParams({ [tabRouter]: 'empty' })
      } else {
        FlowRouter.setParams({ [tabRouter]: result[0] })
        t.serial.set(!error && result)
        const cid = Meteor.isManager ? t.data.selectedCondoId : FlowRouter.getParam('condo_id')
        // const cid = Meteor.isManager ? Template.instance().data.selectedCondoId : FlowRouter.getParam('condo_id')
        if (t.laundry.get() !== null) {
          t.laundry.get().stop()
        }
        t.laundry.set(Meteor.subscribe('getLaundry', cid, (t.serial.get() || [])[0]))
        // let setting = getLaundrySetting.find({_id: t.serial.get()}).fetch()
        
        let setting = LaundrySettings.find({_id: t.serial.get()[0]}).fetch()
        if (setting.length === 0)
          setting = [{serial: t.serial.get()[0], desc: '', location: '', opening: ''}]
        // console.log('setting[0]', setting[0])
        t.setting.set(setting[0])
        t.setting_tmp.set(setting[0])
      }
    })
  },
  getSerial: () => {
    return Template.instance().serial.get()
  },
  selectedTab: () => {
    const tabRouter = Template.instance().tabRouter

    let tab = FlowRouter.getParam(tabRouter)
    let s
    if (!tab || tab === 'empty') {
      s = Template.instance().serial.get()
      if (s) {
        FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), [tabRouter]: s[0] })
      }
    }
    return tab === 'empty' ? s[0] : tab
  },
  shouldDisplayRestrictedAccess: () => {
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('laundryRoom', 'see')
    } else if (Meteor.userHasRight('laundryRoom', 'see', condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },

  getInfoLaundry: () => {
    return LaundryRoom.find({}).fetch();
    // return Template.instance().infoLaundry.get()
  },

  isSubscribeReady: () => {
    return Template.instance().handler.ready() &&
    Template.instance().laundry.get() &&
    Template.instance().laundry.get().ready()
  },
  isSubscribeSettingReady: () => {
    return Template.instance().handlerSetting.ready()
  },
  getUpcomingCounter: () => {
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('laundryRoom', 'see')
    } else if (Meteor.userHasRight('laundryRoom', 'see', condoId)) {
      condoIds = [condoId]
    }
    let counter = 0
    BadgesGestionnaire.find({ _id: { $in: condoIds } }).forEach(condoBadge => {
      counter += condoBadge.marketPlace || 0
    })
    return counter
  },
  getLaundrySetting: () => {
    return Template.instance().setting.get()
  },
  setDescription: (key) => {
    const t = Template.instance()
    return () => value => {
      try {
        let tmp = t.setting_tmp.get()
        tmp[key] = value
        t.setting_tmp.set(tmp)
      } catch (e) {
        // nothing
      }
    }
  },
  canValidateDesc: () => {
    const t = Template.instance()
    const s = t.setting_tmp.get()
    // s['desc'] !== '' &&
    return s !== null && s['location'] !== '' && s['opening'] !== ''
  }

})
