import React, { Component } from 'react'
import withForumContext from './withForumContext'
import Select from 'react-select' // v2, doc -> https://deploy-preview-2289--react-select.netlify.com/home
import _ from 'lodash'

class CategoryPicker extends Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedOption: [],
      options: [],
      allOption: false
    }
    this.getDefaultOption = this.getDefaultOption.bind(this)
    this.onSelect = this.onSelect.bind(this)
    this.formatOptions = this.formatOptions.bind(this)
    this.buildCustomStyle = this.buildCustomStyle.bind(this)
    this.translateSelection = this.translateSelection.bind(this)
  }

  componentDidMount () {
    const { context: { categories }, initCategories } = this.props
    this.setState({
      options: this.formatOptions(categories)
    })
    if (Array.isArray(initCategories) &&
      initCategories.length > 0) {
      this.setState({
        selectedOption: this.formatOptions(initCategories)
      })
    }
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    const {
      context: {
        categories: prevCategories,
        locale: prevLocale
      },
      reset: prevReset
    } = prevProps
    const { context: { categories, locale }, multiple, reset } = this.props
    const { selectedOption: prevSelectedOption } = prevState
    const { selectedOption, options } = this.state
    if (!_.isEqual(categories, prevCategories) || prevLocale !== locale) {
      this.setState({
        options: this.formatOptions(categories)
      })
    }
    if (!multiple && prevSelectedOption.length === 0 && selectedOption.length > 0) {
      const optionsWithAll = [ ...options ]
      optionsWithAll.splice(0, 0, this.getDefaultOption())
      this.setState({
        options: optionsWithAll,
        allOption: true
      })
    }
    if (prevReset === false && reset === true) {
      this.setState({ selectedOption: [] })
    }
    if (prevLocale !== locale) {
      this.translateSelection()
    }
  }

  getDefaultOption () {
    const { context: { locale } } = this.props
    return ({
      label: locale === 'fr' ? 'Toutes' : 'All',
      value: 'all',
      en: 'all',
      fr: 'toutes'
    })
  }

  translateSelection () {
    const { context: { locale } } = this.props
    const { selectedOption } = this.state
    const translatedOptions = []
    selectedOption.forEach(option => {
      const translatedOption = { ...option }
      translatedOption.label = translatedOption[locale]
      translatedOptions.push(translatedOption)
    })
    this.setState({
      selectedOption: translatedOptions
    })
  }

  formatOptions (categories) {
    const { allOption } = this.state
    const { context: { locale } } = this.props
    const options = []
    if (allOption) {
      options.push(this.getDefaultOption())
    }
    categories.forEach(category => options.push({
      label: locale === 'fr' ? category.fr : category.en,
      value: category.id,
      en: category.en,
      fr: category.fr
    }))
    return options
  }

  onSelect (options) {
    const { onCatSelected, context: { categories } } = this.props
    const selectedCategories = []
    let selectedOption = []
    if (Array.isArray(options)) {
      selectedOption = [ ...options ]
    } else {
      selectedOption.push(options)
    }
    if (typeof onCatSelected === 'function') {
      selectedOption.forEach(option => {
        const category = categories.find(category => category.id === option.value)
        if (category) {
          selectedCategories.push(category)
        }
      })
      onCatSelected(selectedCategories)
    }
    this.setState({
      selectedOption
    })
  }

  buildCustomStyle () {
    const { context: { theme }, customStyles } = this.props
    const { options, selectedOption } = this.state
    return ({
      control: (base, state) => ({
        ...base,
        backgroundColor: '#ffffff',
        border: 'solid 1px #e0e0e0 !important',
        boxShadow: state.isFocused ? 'none'
        : '0 1px 4px 0 rgba(32, 33, 38, 0.1)',
        borderRadius: 0,
        cursor: 'default',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontFamily: 'Lato',
        fontSize: 16
      }),
      dropdownIndicator: (base, state) => ({
        ...base,
        display: options.length !== selectedOption.length ? 'block' : 'none'
      }),
      indicatorSeparator: () => ({
        display: 'none'
      }),
      placeholder: () => ({
        fontFamily: 'Lato',
        fontSize: 16,
        fontStyle: 'normal',
        color: '#8b999f'
      }),
      menu: (base) => ({
        ...base,
        backgroundColor: '#ffffff',
        border: 'solid 1px #e0e0e0',
        boxShadow: '0 2px 4px 0 rgba(32, 33, 38, 0.2)',
        borderRadius: 0,
        fontFamily: 'Lato',
        fontSize: 16,
        color: '#5d5e62',
        display: options.length !== selectedOption.length ? 'block' : 'none'
      }),
      menuList: (base) => ({
        ...base,
        paddingTop: 0,
        paddingBottom: 0,
        marginRight: '9px !important'
      }),
      option: (base, state) => ({
        ...base,
        paddingLeft: 9,
        backgroundColor: 'white !important',
        borderLeft: '6px solid',
        borderColor: state.isFocused ? theme.primary : 'white'
      }),
      singleValue: (base) => ({
        ...base,
        color: '#5d5e62'
      }),
      multiValue: (base) => ({
        ...base,
        backgroundColor: theme.primary,
        color: '#5d5e62 !important',
        borderRadius: 2
      }),
      valueContainer: (base) => ({
        ...base,
        padding: '2px 0'
      }),
      multiValueLabel: (base) => ({
        ...base,
        fontFamily: 'Lato',
        color: '#fff'
      }),
      multiValueRemove: (base) => ({
        ...base,
        backgroundColor: `${theme.primary} !important`,
        color: '#ffffff !important'
      }),
      ...customStyles
    })
  }

  render () {
    const { selectedOption, options } = this.state
    const { multiple, placeholder, context: { tl } } = this.props
    return (
      <Select
        options={options}
        onChange={this.onSelect}
        value={selectedOption}
        placeholder={placeholder}
        styles={this.buildCustomStyle()}
        isSearchable={false}
        hideSelectedOptions
        isMulti={multiple}
        classNamePrefix='react-select'
        blurInputOnSelect
        noOptionsMessage={() => tl.noOptions}
      />
    )
  }
}

export default withForumContext(CategoryPicker)
