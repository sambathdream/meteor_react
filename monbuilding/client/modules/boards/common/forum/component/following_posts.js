import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import cx from 'classnames'
import './following_posts.less'
import { compose } from 'lodash/fp'
import withForumContext from './withForumContext'

class FollowingPosts extends Component {
  constructor (props) {
    super(props)
    this.state = {
      condoId: FlowRouter.getParam('condo_id'),
      showAll: false
    }
    this.handleLike = this.handleLike.bind(this)
    this.onToggleShow = this.onToggleShow.bind(this)
    this.filterByCategory = this.filterByCategory.bind(this)
  }

  handleLike (id) {
    let data = {}
    if (data) {
      data['condoId'] = this.state.condoId
      data['postId'] = id
      Meteor.call('unfollowPost', data)
    }
  }

  onToggleShow () {
    const { showAll } = this.state
    this.setState({showAll: !showAll}, () => {
      if (!showAll) {
        this.props.onShowAll()
      } else {
        this.props.onShowLess()
      }
    })
  }

  filterByCategory () {
    const { context: { forumType }, feed, category } = this.props
    if (forumType === 'normal' || !category) {
      return feed
    }
    return feed.filter(post => {
      if (Array.isArray(post.categoriesId)) {
        return post.categoriesId.find(cat => cat === category.id)
      } else {
        return false
      }
    })
  }

  render () {
    const { limit } = this.props
    let feed = this.filterByCategory()
    const length = feed.length
    if (limit > 0) {
      feed = feed.slice(0, limit)
    }
    return feed && feed.length > 0 ? (
      <div className='forum-following-wrapper'>
        <div className='forun-following-item-wrapper'>
          { _.map(feed, (item, i) => {
            return (
              <div className='followedPostBlock' key={i}>
                <div className='lastFollowedPost'>
                  <div className='followedPost'>
                    <div className='followedPostSubject' onClick={() => this.props.goToDetailPost(item._id)}>
                      <span className={cx({
                        pollSvg: item.type === 'poll',
                        postSvg: item.type === 'post'
                      })} />
                      {item.subject.length > 150 ? `${item.subject.slice(0, 150)}...` : item.subject}
                    </div>
                  </div>
                  <div className='followedStarButton' onClick={() => this.handleLike(item._id)}>
                    <div className='starButtonOff' />
                  </div>
                </div>
              </div>
            )
          })}
        </div>

        { !!this.props.limit && length > this.props.initialLimit &&
        <div className='following-more-btn'>
          <span className='btn bordered-btn' onClick={this.onToggleShow}>
            { this.props.dl.seeAll }
          </span>
        </div>
        }

        { !this.props.limit && this.props.initialLimit < length &&
        <div className='following-more-btn'>
          <span className='btn bordered-btn' onClick={this.onToggleShow}>
            { this.props.dl.seeLess }
          </span>
        </div>
        }
      </div>
    ) : (
      <div className='mb-widget-no-data-text'>
        { this.props.dl.noFollowing }
      </div>
    )
  }
}

const subAndQuery = props => {
  const listUnfollow = UnFollowForumPosts.find({}, {fields: {postId: true}}).fetch()
  const listPostUnfollowed = _.map(listUnfollow, (elem) => { return elem.postId })
  const feed = ForumPosts.find({_id: {$nin: listPostUnfollowed}}, {sort: {date: -1}}).fetch()
  return {
    feed: feed
  }
}

export default compose(
  withTracker(subAndQuery),
  withForumContext
)(FollowingPosts)
