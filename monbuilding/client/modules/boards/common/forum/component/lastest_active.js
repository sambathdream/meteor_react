import React, { Component } from 'react';
import Blaze from 'meteor/gadicc:blaze-react-component';
import { withTracker } from 'meteor/react-meteor-data';

class LastestActive extends Component {

	constructor(props) {
		super(props)
		this.state = {
		};				
	};

	render() {
		return(
			<div className="lastAtiveUserBlock">
				<div className="lastAtiveUser">
					<div className="lastAtiveUserPicture">
						<Blaze template="userIcon" type="U" userId={this.props.item.userId} />
					</div>
					<div className="lastAtiveUserName">
						{this.props.user === undefined ? (
                            <div>{this.props.dl.disabledAccount}</div>
                        ) : (
                            <div>{this.props.user.firstname} {this.props.user.lastname}</div>
                        )}
					</div>
				</div>
			</div>
		);
	};
};

export default LastestActiveContainer = withTracker((props) => {
	let user = UsersProfile.findOne({'_id': props.item.userId})

	return {
		user: user
	}
})(LastestActive);
