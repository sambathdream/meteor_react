import React from 'react'

import { ForumLang } from '/common/lang/lang'
import withForumContext from './withForumContext'

class MyPostWidget extends React.Component {
  constructor (props) {
    super(props)

    this.limitPostCount = 3
    this.lang = new ForumLang(FlowRouter.getParam('lang') || 'fr')

    this.state = {
      limited: true
    }

    this.toggleShowAllPosts = this.toggleShowAllPosts.bind(this)
    this.filterByCategory = this.filterByCategory.bind(this)
  }

  toggleShowAllPosts () {
    this.setState({limited: !this.state.limited})
  }

  hasNews (post) {
    let thisUserView = _.find(post.views, (date, viewUserId) => {
      if (viewUserId === Meteor.userId()) {
        return true
      }
    })
    if ((!!thisUserView === false || (thisUserView < post.updatedAt))) {
      return true
    }
  }

  filterByCategory () {
    const { context: { forumType }, posts, category } = this.props
    if (forumType === 'normal' || !category) {
      return posts
    }
    return posts.filter(post => {
      if (Array.isArray(post.categoriesId)) {
        return post.categoriesId.find(cat => cat === category.id)
      } else {
        return false
      }
    })
  }

  render () {
    const { context: { tl } } = this.props
    let posts = this.filterByCategory()
    if (posts.length === 0) {
      return <div className='mb-widget-no-data-text'>
        { tl.zeroPost }
      </div>
    }
    const length = posts.length
    posts = this.state.limited ? posts.slice(0, this.limitPostCount) : posts
    return (
      <div className='mb-widget'>
        <div className='mb-my-posts-wrapper'>
          { posts.map((post, index) => {
            return (
              <div className={(this.hasNews(post) ? 'mb-widget-item-new' : 'mb-widget-item')} key={post._id} onClick={() => this.props.goToDetailPost(post._id)}>
                <div className='mb-widget-subject break-line' style={(index + 1) !== posts.length ? { borderBottom: '1px solid #e0e0e0' } : {}}>
                  <div>
                    <span className={post.type === 'poll' ? 'pollSvg' : 'postSvg'} />
                    <div style={{maxWidth: '90%'}}>
                      {post.subject.length > 150 ? `${post.subject.slice(0, 150)}...` : post.subject}
                    </div>
                  </div>
                </div>
                <div className='mb-widget-item-action' style={(index + 1) !== posts.length ? {borderBottom: '1px solid #e0e0e0'} : {}}>
                  <span onClick={() => this.props.goToDetailPost(post._id)}>
                    <img style={{width: '20px', height: '20px'}} src='/img/icons/edit.png' alt='Edit' />
                  </span>
                </div>
              </div>
            )
          })}
        </div>
        {
          length > 3 &&
          <div className='mb-widget-action'>
            <div className='btn bordered-btn' onClick={this.toggleShowAllPosts}>
              { this.state.limited ? this.lang.forumLang.seeAll : this.lang.forumLang.seeLess }
            </div>
          </div>
        }
      </div>
    )
  }
}

export default withForumContext(MyPostWidget)
