import React, { Component } from 'react';
import AutoResizeTextArea from 'react-autosize-textarea';
import Blaze from 'meteor/gadicc:blaze-react-component';
import moment from 'moment';
import '../style/datetime.less';
import '../style/input-moment.less';
import '../style/popup.less';
import '../style/react-datetime.less';
import PollExpDate from './pollExpDate'
import withForumContext from './withForumContext'
import CategoryPicker from './categoryPicker'

class PollForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId : Meteor.userId(),
      inputNb: 2,
      m: moment(),
      date: "",
      hour: "",
      endDate: moment().add(1, 'days'),
      endDateSwitch: false,
      startDate: moment(),
      display: false,
      options: ['', ''],
      subject: props.title || "",
      inputs: {},
      files: [],
      writePoll: Meteor.userHasRight('forum', 'writePoll'),
      allowOtherAddOp: false,
      categories: [],
      resetCategoryPicker: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addOption = this.addOption.bind(this);
    this.displayDatetimPicker = this.displayDatetimPicker.bind(this);
    this.removeOption = this.removeOption.bind(this);
    this.onFilesChange = this.onFilesChange.bind(this);
    this.onFilesError = this.onFilesError.bind(this);
    this.isDisabled = this.isDisabled.bind(this);
    this.onExpDatePicked = this.onExpDatePicked.bind(this)
    this.onSwitchExpDate = this.onSwitchExpDate.bind(this)
    this.getSwitchIcon = this.getSwitchIcon.bind(this)
    this.onCatSelected = this.onCatSelected.bind(this)
    this.getCategoriesId = this.getCategoriesId.bind(this)
    this.resetForm = this.resetForm.bind(this)
  }

  resetForm () {
    if (this.props.titleUpdated) {
      this.props.titleUpdated('')
    }
    this.setState({
      subject: '',
      files: [],
      options: ['', ''],
      categories: [],
      resetCategoryPicker: true,
      endDate: moment().add(1, 'days'),
      endDateSwitch: false,
      allowOtherAddOp: false
    }, () => {
      this.setState({ resetCategoryPicker: false })
    })
  }

  getCategoriesId () {
    const { categories } = this.state
    const categoriesId = []
    categories.forEach(category => categoriesId.push(category.id))
    return categoriesId
  }

  onSwitchExpDate (value) {
    this.setState({
      endDateSwitch: value
    })
  }

  onExpDatePicked (expDate) {
    this.setState({
      endDate: expDate
    })
  }

  addOption(e) {
    this.setState({options: [...this.state.options, '']})
  }

  handleChange(event) {
    if (this.props.titleUpdated) {
      this.props.titleUpdated(event.target.value)
    }
    this.setState({subject: event.target.value})
  }

  displayDatetimPicker(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState(prevState => ({
      display: !prevState.display
    }));
  };

  handleOptionChange(e, index) {
    this.setState({
      options: _.map(this.state.options, (o, i) => i === index ? e : o)
    })
  }

  async handleSubmit(event) {
    event.preventDefault()
    const { context: { forumType } } = this.props
    const { endDate, endDateSwitch, categories } = this.state
    if (forumType === 'categories' && categories.length === 0) {
      return
    }
    if (endDate &&
      moment.isMoment(endDate) && !endDate.isAfter(moment().add(9, 'minutes'))) {
      sAlert.error(this.props.dl.expiry_date_error)
      return
    }
    const hasOptions = _.filter(this.state.options, o => o !== '')
    $('.btn-submit-post').button('loading')
    const { allowOtherAddOp } = this.state
    if (this.state.subject.trim() !== '' && hasOptions.length >= 2) {
      this.props.fm.batchUpload(this.state.files)
        .then(files => {
          Meteor.call('forumNewPost', {
            condoId: this.props.condoId,
            subject: this.state.subject.trim(),
            inputs: this.state.options,
            filesId: _.map(files, f => f._id),
            date: Date.now(),
            endDate: endDateSwitch ? parseInt(moment(endDate).format('x')) : null,
            views: {},
            type: 'poll',
            categoriesId: this.getCategoriesId(),
            isEditable: allowOtherAddOp
          }, (err) => {
            $('.btn-submit-post').button('reset');
            setTimeout(() => {
              $('.btn-submit-post').prop("disabled", true);
            }, 1);
            if (!err) {
              this.resetForm()
            }
          });
        })
        .catch(err => {
          sAlert.error(this.props.dl[err.reason])
          $('.btn-submit-post').button('reset');
        })
    }
  }

  removeOption (idx) {
    if (this.state.options.length < 3) {
      return
    }
    let options = this.state.options;

    // remove option from array
    options.splice(idx, 1);

    // update state
    this.setState({ options })
  }

  onFilesChange = (files) => {
    if (!files.length) {
      files = [files]
    }
    this.setState({
      files: [...this.state.files, ...files]
    })
  }

  onFilesError = (error, file) => {
    // console.log('error code ' + error.code + ': ' + error.message)
  }

  onFileRemoved = (index) => {
    this.setState({
      files: _.filter(this.state.files, (file) => file.fileId ? file.fileId !== fileId : file.id !== index)
    })
  }

  emptyOptions() {
    const options = _.filter(this.state.options, (opt) => {
      return opt === ''
    })
    return options.length !== 0
  }

  filesRemoveOne = (file) => {
    this.refs.files.removeFile(file)
  }

  isDisabled() {
    const { context: { forumType } } = this.props
    const { categories } = this.state
    return !!(this.state.subject.length === 0 ||
      this.emptyOptions() ||
      (forumType === 'category' && categories.length === 0))
  }

  onCatSelected (categories) {
    this.setState({
      categories
    })
  }

  static getCategoryPickerStyle () {
    return ({
      control: (base, state) => ({
        ...base,
        backgroundColor: '#ffffff !important',
        border: 'none !important',
        borderBottom: 'solid 1px #b9c1c4 !important',
        boxShadow: 'none',
        borderRadius: 0,
        fontFamily: 'Lato',
        fontSize: 16,
        color: '#5d5e62'
      }),
    })
  }

  getSwitchIcon () {
    const { context: { theme: { primary } }, switchForm } = this.props
    let color = '#69D2E7'
    if (primary) {
      color = primary
    }
    return (
      <div onClick={switchForm} id='poll-switch-icon'>
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
          <g
            fill="none"
            fillRule="evenodd"
            stroke={color}
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2">
            <path d="M10 3h4v18h-4zM18 8h4v13h-4zM2 13h4v8H2z"/>
          </g>
        </svg>
      </div>
    )
  }

  render() {
    const { context: { forumType, tl } } = this.props
    const {
      allowOtherAddOp,
      files,
      endDate,
      endDateSwitch,
      resetCategoryPicker
    } = this.state
    return this.state.writePoll ? (
      <form onSubmit={this.handleSubmit} className="poll-form">
        <div id="input_div" className="form-group">
          <div className="input-wrapper">
            <AutoResizeTextArea
              rows={3}
              value={this.state.subject}
              onChange={this.handleChange}
              placeholder={this.props.dl.pollPlaceholder}
              className="form-control borderless-textarea text-area-black"
            />
          </div>
          {this.state.options.map((item, idx) =>
            <div id="div_input_img" className="form-group form-inline forumTitles add-margin-bottom" key={idx + 1}>
              <Blaze
                value={this.state.options[idx]}
                template='MbInput'
                label={`Option ${idx + 1}`}
                required={true}
                onInput={e => this.handleOptionChange(e, idx)}
                placeholder={`${this.props.dl.write_poll_option} ${idx + 1} ${this.props.dl.here}`}
              />
              { this.state.options.length > 2 && <div className="deletePollOption" onClick={e => this.removeOption(idx)}/> }
            </div>
          )}
          <button
            type="button"
            className="btn bordered-btn pull-right"
            onClick={this.addOption}
            id="poll-add-option-button">
            {this.props.dl.addOption}
          </button>

          <div className="check-box-allow-other adjust-color">
            <Blaze
              template='MbCheckbox'
              checked={allowOtherAddOp}
              label={this.props.dl.poll_allow_other_to_add_opt}
              onClick={() => this.setState({allowOtherAddOp: !allowOtherAddOp})}
            />
          </div>
          <PollExpDate
            expDate={endDate}
            switched={endDateSwitch}
            onDatePicked={this.onExpDatePicked}
            onSwitch={this.onSwitchExpDate}
            dl={this.props.dl}
            containerStyle={{marginBottom: 15}}
          />
          {
            files.length > 0 &&
            <Blaze
              template="FileList"
              files={files}
              onFileAdded={this.onFilesChange}
              onRemove={this.onFileRemoved}
            />
          }
          { forumType === 'category' &&
          <div className='category-picker-wrapper' style={{marginTop: 20}}>
            <span className='category-picker-label'>
              {tl.selectCategories.toUpperCase()}
              <span className="image-icon mandatory-icon"/>
            </span>
            <CategoryPicker
              onCatSelected={this.onCatSelected}
              multiple={true}
              placeholder={tl.select}
              reset={resetCategoryPicker}
              customStyles={PollForm.getCategoryPickerStyle()}
            />
          </div>
          }
        </div>
        <div id="allAttachments">
          { this.getSwitchIcon() }
          <Blaze template="FileUploadButton" files={this.state.files} onFileAdded={this.onFilesChange} wrapperClass="mh16" />
          <div id="submitButton">
            <button
              id="button"
              className="btn pull-right redButton btn-submit-post"
              disabled={this.isDisabled()}
              type="submit"
              data-loading-text={`<i class='fa fa-spinner fa-spin'></i> ${this.props.dl.sending} ...`}
            >
              {this.props.dl.postSubmitButton}
            </button>
          </div>
        </div>
      </form>
    ) : (
      <div>{this.props.dl.rightPoll}</div>
    )
  }
}

export default withForumContext(PollForm)
