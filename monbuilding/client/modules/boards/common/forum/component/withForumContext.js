import ForumContext from './context'
import React from 'react'

export default function withForumContext (Component) {
  return function hydrateComponent (props) {
    return (
      <ForumContext.Consumer>
        {value => <Component {...props} context={value} />}
      </ForumContext.Consumer>
    )
  }
}
