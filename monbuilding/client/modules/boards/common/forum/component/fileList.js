import React from 'react'
import Files from 'react-files'

import './fileList.less'

class FileList extends React.Component {

  onFileAdded = (files) => {
    this.props.onFileAdded(files.pop())
  }

  isImageFile(type) {
    const re = /(\.|\/)(gif|jpe?g|png|bmp)$/i;

    return re.test(type);
  }

  render() {
    return this.props.files.length > 0 ? (
      <div className='files-list'>
        <ul className="ul_pics">
          {this.props.files.map((file, index) => {
            return (
              <li className='files-list-item li_pics' key={`files-${file.id}${index}`}>
                <div className='files-list-item-preview'>
                  { this.isImageFile(file.type)
                    ? <img className='files-list-item-preview-image attachments_size' src={file.preview.url}/>
                    : <div className='files-list-item-preview-extension'>{file.name}</div>}
                  <span className="files-list-item-delete" onClick={() => this.props.onFileRemoved(index)}>
                    <img src="/img/icons/svg/supPicto.svg" />
                  </span>
                </div>
              </li>
            )
          })}
          <li className='files-list-item li_pics'>
            <Files
              ref='fl'
              className='files-dropzone-list'
              onChange={this.onFileAdded}
              maxFileSize={10485760}
              minFileSize={0}
              clickable
              multiple={false}
            >
              <div className='files-list-item add-more'>
                <span className="icon-add"></span>
              </div>
            </Files>
          </li>
        </ul>
      </div>
    ) : null
  }
}

export default FileList;
