import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
import moment from 'moment'
import './poll.less'
import { Modal } from 'react-bootstrap'
import Blaze from 'meteor/gadicc:blaze-react-component'

class PollComponent extends Component {
  constructor (props) {
    super(props)
    this.state = {
      userId: Meteor.userId(),
      condoId: FlowRouter.getParam('condo_id'),
      now: parseInt(moment().format('x')),
      votersModal: false,
      votersDetail: {},
      option: '',
      isSubmittingOpt: false
    }
    this.handleVote = this.handleVote.bind(this)
    this.countVote = this.countVote.bind(this)
    this.parseVoters = this.parseVoters.bind(this)
    this.showVotersModal = this.showVotersModal.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.handleChangeOpt = this.handleChangeOpt.bind(this)
    this.submitOpt = this.submitOpt.bind(this)
    this.watchKeyDown = this.watchKeyDown.bind(this)
  };

  handleClose () {
    this.setState({votersModal: false})
  }

  countVote () {
    let tab = []
    let res = []

    for (let i = 0; i < this.props.nbChoices; i++) {
      tab[i] = 0
    }
    _.each(this.props.poll.votes, (item, i) => {
      tab[item - 1] = tab[item - 1] + 1
    })
    for (var i = 0; i < tab.length; i++) {
      res[i] = 100 * tab[i] / this.props.nbVotes
    }
  }

  handleVote (index) {
    Meteor.call('addVote', FlowRouter.getParam('condo_id'), this.props.pollId, index)
  };

  getPollStatus (index) {
    const { votes } = this.props.item
    if (!votes || _.isEqual(votes, {})) {
      return 0
    }
    return 100 * ((_.filter(votes, v => v === index).length) / Object.keys(votes).length)
  }

  getPollCount (index) {
    return _.filter(this.props.item.votes, v => v === index).length
  }

  parseVoters (index) {
    let votersId = []
    let template = ''
    _.forEach(this.props.item.votes, (v, k) => {
      if (v === index) {
        votersId.push(k)
      }
    })
    votersId.forEach(i => {
      let user = UsersProfile.findOne(i)
      if (user === undefined || user === null) return
      let avatar = Avatars.findOne({userId: user._id})
      template += '<div class="voter-detail">'
      if (avatar) {
        template += '<span class="userIcon-main">' +
          '<div style="background-image:url(' + avatar.avatar.original + '), url(\'/img/anonymous.png\')" class="smallprofilepic userIcon"></div>' +
          '</span>'
      } else {
        template += '<span class="userIcon-main">' +
          '<span class="userIcon">' + user.firstname[0] + user.lastname[0] + '</span>' +
          '</span>'
      }
      template += '<span class="voter-detail-username">' + user.firstname + ' ' + user.lastname + '</span>'
      template += '</div>'
      if (i !== votersId[votersId.length - 1]) {
        template += '<div class="hrStyle"><hr></div>'
      }
    })
    return {__html: template}
  }

  showVotersModal (optionName, index, count) {
    this.setState({
      votersModal: !this.state.votersModal,
      votersDetail: {
        description: optionName,
        count: count,
        list: this.parseVoters(index)
      }
    })
  }

  handleChangeOpt (event) {
    this.setState({option: event.target.value})
  }

  submitOpt () {
    const {option, isSubmittingOpt} = this.state
    if (option === '' || isSubmittingOpt || !this.props.poll) {
      return
    }
    const {poll: {condoId, _id}} = this.props
    const tmp = option
    this.setState({
      isSubmittingOpt: true,
      option: ''
    }, async () => {
      try {
        await Meteor.call('addOptionInForumPoll', _id, condoId, tmp)
      } catch (e) {
        console.warn('an error has occurred while trying to add a poll option')
      } finally {
        this.setState({isSubmittingOpt: false})
      }
    })
  }

  watchKeyDown (event) {
    const {option, isSubmittingOpt} = this.state
    if (event.keyCode === 13 && !isSubmittingOpt && option !== '') {
      this.submitOpt()
    } else if (event.keyCode === 13 && !isSubmittingOpt && option === '') {
      event.target.blur()
    }
  }

  render () {
    if (this.props.nbVotes > 0) {
      this.countVote()
    }
    let myVote = -1
    if (this.props.poll && !!this.props.poll.votes && this.props.poll.votes[Meteor.userId()] >= 0) {
      myVote = this.props.poll.votes[Meteor.userId()]
    }
    const {votersDetail, votersDetail: {description, count, list}, option, isSubmittingOpt} = this.state
    const {poll, dl, isEditing} = this.props
    let showAddOption = false
    if (poll && typeof poll.isEditable === 'boolean') {
      showAddOption = poll.isEditable
    }
    return (
      <div className='poll-wrapper'>
        {this.props.poll && this.props.item && Object.keys(this.props.poll).length > 0 && _.map(this.props.item.inputs, (item, i) => {
          return (
            <div key={i} className="poll-item">
              <div className="poll-title">
                <p>{item}</p>
              </div>
              <div className="voteBlockLayout">
                {(parseInt(moment().format('x')) < this.props.endDate || !this.props.endDate) &&
                <div
                  className="mb-checkbox"
                  value={this.props.choice}
                  onClick={(e) => this.handleVote(i)}
                >
                  <div className="checkboxred">
                    <input
                      type="checkbox"
                      checked={myVote === i}
                    />
                    <label/>
                  </div>
                </div>
                }
                <div className='vote-container'>
                  <div className="progress poll-progress">
                    <div className="progress-bar" role="progressbar" aria-valuenow={60} aria-valuemin={0}
                         aria-valuemax={100} style={{width: `${this.getPollStatus(i)}%`}}>
                    </div>
                  </div>
                  { this.getPollCount(i) > 0
                    ? <span
                      className='forumNbVotes votesNb-pointer'
                      id={`voters-count-${this.props.pollId}-${i}`}
                      onClick={() => this.showVotersModal(item, i, this.getPollCount(i))}
                    >
                      {this.getPollCount(i)} {this.props.dl.vote}
                    </span>
                    : <span className='forumNbVotes votesNb-default'>
                      {this.getPollCount(i)} {this.props.dl.vote}
                    </span>
                  }
                </div>
              </div>
            </div>
          )
        })}
        {showAddOption && !isEditing &&
        <div className="add-option-container">
          <Blaze
            template='MbCheckbox'
            checked={option !== ''}
            label={''}
            onClick={() => {}}
          />
          <input
            type='text'
            value={this.state.option}
            onKeyDown={this.watchKeyDown}
            onChange={this.handleChangeOpt}
            className='input-add-option'
            placeholder={dl.add_option}
            disabled={isSubmittingOpt}
          />
        </div>
        }
        <Modal
          className="poll-modal"
          show={this.state.votersModal}
          onHide={this.handleClose}
        >
          <div className="modal-header">
            {!_.isEmpty(votersDetail) && <div>
              <span className="voters-modal-title">{description}</span>
              <div className="forumNbVotesModal">
                {count} {this.props.dl.vote}
              </div>
            </div>}
            <button
              type="button"
              className="close"
              onClick={this.handleClose}
            >
              <span aria-hidden="true">
                <img src="/img/icons/svg/close.svg" alt=""/>
              </span>
            </button>
          </div>
          <Modal.Body>
            {!_.isEmpty(votersDetail) && <div dangerouslySetInnerHTML={list}/>}
          </Modal.Body>
        </Modal>
      </div>
    )
  }
}

export default PollComponentContainer = withTracker((props) => {
  let nbChoices = []
  let nbVotes = []

  const poll = ForumPosts.findOne({'_id': props.pollId})

  if (poll && poll.inputs) {
    nbChoices = Object.keys(poll.inputs).length
    if (poll.votes) {
      nbVotes = Object.keys(poll.votes).length
    }
    endDate = poll.endDate
  }

  return {
    poll: poll,
    nbChoices: nbChoices,
    nbVotes: nbVotes,
    endDate: endDate,
  }
})(PollComponent)
