import React, { Component } from 'react'

export default class SearchBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      value: ''
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange (event) {
    const { value } = event.target
    const { onFilterChange } = this.props
    this.setState({value: value})
    onFilterChange(value)
  }

  render () {
    const { placeholder, containerStyle } = this.props
    return (
      <div className={containerStyle}>
        <div className='icon-search-bar' />
        <input
          type='text'
          placeholder={placeholder}
          value={this.state.value}
          onChange={this.handleChange}
          className='search-input' />
      </div>
    )
  }
}
