import React, { Component } from 'react';
import AutoResizeTextArea from 'react-autosize-textarea';
import Blaze from 'meteor/gadicc:blaze-react-component';
import withForumContext from './withForumContext'
import CategoryPicker from './categoryPicker'

class PostForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId : Meteor.userId(),
      subject: props.title || "",
      files: [],
      writeSubject: Meteor.userHasRight('forum', 'writeSubject'),
      isClicked: false,
      categories: [],
      resetCategoryPicker: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.onCatSelected = this.onCatSelected.bind(this)
    this.getCategoriesId = this.getCategoriesId.bind(this)
    this.resetForm = this.resetForm.bind(this)
  }

  handleChange(event) {
    if (this.props.titleUpdated) {
      this.props.titleUpdated(event.target.value)
    }
    this.setState({subject: event.target.value});
    if (this.state.isClicked) {
      this.setState((prevState, props) => ({
        isClicked: !prevState.isClicked
      }))
    }
  }

  resetForm () {
    if (this.props.titleUpdated) {
      this.props.titleUpdated('')
    }
    this.setState({
      subject: '',
      files: [],
      categories: [],
      resetCategoryPicker: true
    }, () => {
      this.setState({ resetCategoryPicker: false })
    })
  }

  getCategoriesId () {
    const { categories } = this.state
    const categoriesId = []
    categories.forEach(category => categoriesId.push(category.id))
    return categoriesId
  }

  async handleSubmit(event) {
    event.preventDefault()
    const { context: { forumType } } = this.props
    const { categories } = this.state
    if (forumType === 'categories' && categories.length === 0) {
      return
    }
    if (!this.state.isClicked) {
      this.setState((prevState, props) => ({
        isClicked: !prevState.isClicked
      }))
      $('.btn-submit-post').button('loading');
      if (this.state.subject !== '') {
        let data = {};

      if (this.props.condoId) {
        data['condoId'] = this.props.condoId;
        data['date'] = Date.now();
      }
        if (this.state.subject) {
          data['subject'] = this.state.subject.trim();
        }

        this.props.fm.batchUpload(this.state.files)
          .then(files => {
            data = {
              ...data,
              views: {},
              categoriesId: this.getCategoriesId(),
              type: 'post',
              filesId: _.map(files, f => f._id),
            }
            Meteor.call('forumNewPost', data, (error, result) => {
              if (!error) {
                this.resetForm()
              }
              $('.btn-submit-post').button('reset');
              setTimeout(() => {
                $('.btn-submit-post').prop("disabled", true);
              }, 1);
            })
          })
          .catch(err => {
            sAlert.error(this.props.dl[err.reason])
            $('.btn-submit-post').button('reset');
            this.setState({isClicked: false })
          })
      }
    }
  }

  onFilesChange = (files) => {
    console.log('files', files)
    if (!files.length) {
      files = [files]
    }
    this.setState({
      files: [...this.state.files, ...files]
    })
  }

  onFileRemoved = (index) => {
    this.setState({
      files: _.filter(this.state.files, (file) => file.fileId ? file.fileId !== index : file.id !== index)
    })
  }

  onFilesError = (error, file) => {
    console.log('error code ' + error.code + ': ' + error.message)
  }

  filesRemoveOne = (file) => {
    this.refs.files.removeFile(file)
  }

  onCatSelected (categories) {
    this.setState({
      categories
    })
  }

  static getCategoryPickerStyle () {
    return ({
      control: (base, state) => ({
        ...base,
        backgroundColor: '#ffffff !important',
        border: 'none !important',
        borderBottom: 'solid 1px #b9c1c4 !important',
        boxShadow: 'none',
        borderRadius: 0,
        fontFamily: 'Lato',
        fontSize: 16,
        color: '#5d5e62'
      }),
    })
  }

  render() {
    const { context: { forumType, tl } } = this.props
    const { categories, resetCategoryPicker } = this.state
    return this.state.writeSubject ? (
      <form onSubmit={this.handleSubmit}>
        <div id="input_div" className="form-group">
          <div className="input-wrapper">
            <AutoResizeTextArea
              rows={3}
              value={this.state.subject}
              onChange={this.handleChange}
              placeholder={this.props.dl.postPlaceholder}
              className="form-control borderless-textarea text-area-black"
            />
          </div>
          <Blaze
            template="FileList"
            files={this.state.files}
            onFileAdded={this.onFilesChange}
            onRemove={this.onFileRemoved}
          />
          { forumType === 'category' &&
          <div className='category-picker-wrapper'>
            <span className='category-picker-label'>
              {tl.selectCategories.toUpperCase()}
              <span className="image-icon mandatory-icon"/>
            </span>
            <CategoryPicker
              onCatSelected={this.onCatSelected}
              multiple={true}
              placeholder={tl.select}
              reset={resetCategoryPicker}
              customStyles={PostForm.getCategoryPickerStyle()}
            />
          </div>
          }
        </div>
        <div className='form-bottom-bar'>
          <button
            className="btn pull-righ"
            style={Meteor.userHasRight('forum', 'writePoll') && Meteor.userHasRight('forum', 'seePoll') ? {} : { cursor: 'not-allowed' }}
            id="switch-to-poll-btn"
            type="button"
            onClick={this.props.switchForm}
          />
          <Blaze template="FileUploadButton" files={this.state.files} onFileAdded={this.onFilesChange} wrapperClass="mh16" />
          <div id="submitButton">
            <button
              id="button"
              className="btn pull-right redButton btn-submit-post"
              disabled={this.state.subject.trim() === '' || (forumType === 'category' && categories.length === 0)}
              type="submit"
              data-loading-text={`<i class='fa fa-spinner fa-spin'></i> ${this.props.dl.sending} ...`}
            >
              {this.props.dl.postSubmitButton}
            </button>
          </div>
        </div>
      </form>
    ) : (
      <div>{this.props.dl.rightPost}</div>
    )
  }
}

export default withForumContext(PostForm)
