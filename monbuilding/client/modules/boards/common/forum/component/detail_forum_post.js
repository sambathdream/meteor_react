import React, { Component } from 'react';
import Blaze from 'meteor/gadicc:blaze-react-component';
import moment from 'moment';
import { withTracker } from 'meteor/react-meteor-data';
import { FileManager } from '/client/components/fileManager/filemanager.js';
import ForumPostComment from './forum_post_comment';
import PollComponent from './poll';
import AutoResizeTextArea from 'react-autosize-textarea';
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import CategoryPicker from './categoryPicker'
import { compose } from 'lodash/fp'
import withForumContext from './withForumContext'
import _ from 'lodash';
import { Email } from '/common/lang/lang.js'

const PER_LOAD = 5;

class DetailsForumPost extends Component {
	constructor (props) {
    super(props)
		this.state = {
			comment: "",
			commentDate: "",
			postedAt : "",
			isToggleOn: false,
			newEdit: '',
			files: [],
			postFiles: [],
      allowOtherAddOp:  false,
      isSubmitting: false,
      categories: [],
      resetCategoryPicker: false,
      limit: PER_LOAD,
      comments: []
    }
		this.windowGrower = this.windowGrower.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleChange2 = this.handleChange2.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.removePostPoll = this.removePostPoll.bind(this)
		this.handleNewEdit = this.handleNewEdit.bind(this)
    this.hasChanged = this.hasChanged.bind(this)
    this.onCatSelected = this.onCatSelected.bind(this)
    this.getCategoriesId = this.getCategoriesId.bind(this)
    this.findCategoriesById = this.findCategoriesById.bind(this)
  }

  static getDerivedStateFromProps (props, state) {
    if (props.comments && !_.isEqual(state.comments, props.comments)) {
      return {
        comments: _.takeRight(props.comments, state.limit) || null
      }
    }
  }

  componentDidMount () {
    const data = {}
    data['condoId'] = FlowRouter.getParam('condo_id')
    data['postId'] = FlowRouter.getParam('postId') || FlowRouter.getParam('wildcard')
    Meteor.call('addView', data)


    const { post, context: { forumType }, comments } = this.props
    if (forumType === 'category' &&
      post &&
      Array.isArray(post.categoriesId)) {
      this.setState({ categories: this.findCategoriesById() })
    }
  }

  getCategoriesId () {
    const { categories } = this.state
    const categoriesId = []
    categories.forEach(category => categoriesId.push(category.id))
    return categoriesId
  }

  findCategoriesById () {
    const { post: { categoriesId }, context: { categories } } = this.props
    const selectedCategories = []
    categoriesId.forEach(linkedId => {
      const category = categories.find(({ id }) => id === linkedId)
      if (category) {
        selectedCategories.push(category)
      } else {
        console.warn(`the category with id "${linkedId}" is not found`)
      }
    })
    return selectedCategories
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevProps.post && this.props.post && prevProps.post.filesId.length !== this.props.post.filesId.length) {
      Meteor.subscribe('forumFiles', [prevProps.post.condoId])
    }
    if (this.props.post && prevProps.post !== this.props.post && typeof this.props.post.isEditable === 'boolean') {
      this.setState({allowOtherAddOp:  this.props.post.isEditable})
    }
    const { post, context: { forumType } } = this.props
    const { post: prevPost } = prevProps
    if (forumType === 'category' &&
      !prevPost &&
      post &&
      Array.isArray(post.categoriesId)) {
      this.setState({ categories: this.findCategoriesById() })
    }
  }

	handleChange(event) {
		this.setState({comment: event.target.value});
	}

	handleChange2(event) {
		this.setState({newEdit: event.target.value});
	}

	handleSubmit(e) {
		if (e) {
			e.preventDefault()
    }

    // Can we stop doing direct DOM manipulation ?
		$('.forumPostCommentButton').button('loading');

		if (this.state.comment.trim() === '' && this.state.files.length === 0) {
			$('.forumPostCommentButton').button('reset');
			return
		}

    this.props.fm.batchUpload(this.state.files)
      .then(files => {
        const data = {
          condoId: FlowRouter.getParam('condo_id'),
          postId: FlowRouter.getParam('postId') || FlowRouter.getParam('wildcard'),
          comment: this.state.comment,
          commentDate: parseInt(moment().format('x')),
          filesId: _.map(files, f => f._id)
        }

        Meteor.call('forumCommentPost', data, (error, result) => {
          $('.forumPostCommentButton').button('reset');

          if (!error) {
            setTimeout(() => {
              this.setState({ comment: '', files: [] })
            }, 0)
          } else {
            $('.forumPostCommentButton').button('reset');
          }
        });
      })
      .catch(err => {
        sAlert.error(this.props.dl[err.reason])
        $('.forumPostCommentButton').button('reset');
      })
	}

	windowGrower() {
		$(function(){
			$('.normal').autosize();
			$('.animated').autosize({append: "\n"});
		});
	}

	canSubmitComment() {
		return !!this.state.comment || this.state.files.length !== 0;
	}

	removePostPoll() {
		let self = this
		const condoId = FlowRouter.getParam('condo_id');
    const lang = FlowRouter.getParam('lang');

    let tr_commonDelete = ""
    if (this.props.post.type === "post") {
      tr_commonDelete = this.props.dl.delete_post_confirmation
    } else if (this.props.post.type === "poll") {
      tr_commonDelete = this.props.dl.delete_poll_confirmation
    }

    bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: tr_commonDelete,
			buttons: {
				'cancel': { label: this.props.dl.cancel, className: "btn-outline-red-confirm" },
				'confirm': { label: this.props.dl.delete, className: "btn-red-confirm" }
			},
			backdrop: true,
			callback: function (result) {
				if (result) {
					Meteor.call('removePostPoll', {
						condoId: condoId,
						postId: self.props.post._id
					}, (error, result) => {
						if (!!error) {
							console.log(error)
						}
					});

					self.props.goBackToForum();
				}
			}
		});
	}

	handleClick(id) {
			this.setState({
				isToggleOn: id,
				newEdit: this.props.post.subject,
				postFiles: [
          ...this.props.files ? _.map(this.props.files.fetch(), (file) => ({
            ...file,
            id: file._id,
            preview: {
                    url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
            }
          })) : [],
          ...this.props.newFiles
        ]
			})
	}

	cancelEditMode() {
    this.setState({
			isToggleOn: false,
      newEdit: '',
      postFiles: [],
      categories: this.findCategoriesById()
    })
	}

	hasChanged () {
    const { post: { subject, filesId, type, isEditable, categoriesId } } = this.props
    const { newEdit, postFiles, allowOtherAddOp } = this.state
    if (newEdit !== subject) {
      return true
    }
    if (!Array.isArray(filesId)) {
      return true
    }
    if (postFiles.length !== filesId.length) {
	    return true
    }
    if (type === 'poll' && allowOtherAddOp !== isEditable) {
      return true
    }
    postFiles.forEach(file => {
      if (!filesId.find((item) => item === file._id)) {
        return true
      }
    })
    const ids = this.getCategoriesId()
    if (ids && categoriesId && !_.isEqual(ids.sort(), categoriesId.sort())) {
      return true
    }
    ids.forEach(id => {
      if (!categoriesId.find(categoryId => categoryId === id)) {
        return true
      }
    })
	  return false
  }

	async handleNewEdit(e, id) {
	  const { context: { forumType } } = this.props
    const { categories } = this.state
    if (forumType === 'category' && categories.length === 0) {
      return
    }
	  const { isSubmitting } = this.state
    if (isSubmitting) {
	    return
    }
    this.setState({ isSubmitting: true })
    e.preventDefault();
    if (!this.hasChanged()) {
      this.cancelEditMode()
      this.setState({ isSubmitting: false })
      return
    }
    $('.editPostButton').button('loading');
    const previousFiles = _.filter(this.state.postFiles, f => f._id || f.fileId);
    const newFiles = _.filter(this.state.postFiles, f => !f._id && !f.fileId);

		this.props.fm.batchUpload(newFiles)
      .then(files => {
        const fileIds = files.map(file => file._id)

        const data = {
          condoId: FlowRouter.getParam('condo_id'),
          postId: id,
          newEdit: this.state.newEdit,
          filesId: [
            ...previousFiles.map(file => file._id ? file._id : file),
            ...fileIds
          ]
        }

        if (this.props.post && this.props.post.type === 'poll') {
          data.isEditable = this.state.allowOtherAddOp
        }
        if (forumType === 'category') {
          data.categoriesId = this.getCategoriesId()
        }
        Meteor.call('modifPost', data, (err) => {
          $('.editPostButton').button('reset');
          this.setState({ isSubmitting: false })
          if (!err) {
            this.handleClick();
            this.setState({
              newEdit: '',
              postFiles: []
            })
          } else {
            $('.editPostButton').button('reset');
          }
        });
      })
      .catch(err => {
        console.error(err);
        $('.editPostButton').button('reset');
        this.setState({ isSubmitting: false })
      })
	}

	onCommentFilesChange = (files) => {
    	this.setState({
			files: [...this.state.files, ...files]
	    })
	}

	onCommentFileRemoved = (index) => {
		this.setState({
			files: _.filter(this.state.files, (file) => file.id !== index)
		})
  }

  previewCommentFiles() {
    return this.state.files
  }

  showDatetime(timestamp) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  }

  showExpireDate({endDate}) {
		if (endDate && !isNaN(endDate)) {
      let timestamp = endDate
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translation = new Email(lang)
      const ts = parseInt(moment(timestamp).format('x'))
      const time_show =  moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
			return `${this.props.dl.expire_on} ${time_show}`
		}
	}

	onPostFileChange = (files) => {
		this.setState({
			postFiles: [...this.state.postFiles, ...files]
		})
	}

	onPostFileRemove = (fileId) => {
    	this.setState({
    		postFiles: _.filter(this.state.postFiles, (file) => file.fileId ? file.fileId !== fileId : file.id !== fileId)
      })

      // TODO: u will find more code like this -> file.fileId ? file.fileId !== fileId : ....
      // we need to refactor all of them when we have one way to handle all files
	}

	previewPostFiles() {
		return this.state.postFiles
	}

  onCatSelected (categories) {
    this.setState({
      categories
    })
  }

  static getCategoryPickerStyle () {
    return ({
      control: (base, state) => ({
        ...base,
        backgroundColor: '#ffffff !important',
        border: 'none !important',
        borderBottom: 'solid 1px #b9c1c4 !important',
        boxShadow: 'none',
        borderRadius: 0,
        fontFamily: 'Lato',
        fontSize: 16,
        color: '#5d5e62'
      }),
    })
  }

	render() {
    const { post, ForumCommentsSub, user, context: { forumType, tl } } = this.props
    const { allowOtherAddOp, isSubmitting, categories, resetCategoryPicker } = this.state
    let postId = FlowRouter.getParam('postId') || FlowRouter.getParam('wildcard')
    const isEdited = !!(post && post.editedAt)
    if (!postId) {
      $('#postDetailsContainer').css('display', 'none')
      $('#postFeedContainer').css('position', '')
      return <div></div>
    }
    if (!ForumCommentsSub) {
			return (
        <div style={ { 'marginTop': '70px' } }>
  				<Blaze template="loader" />
        </div>
			)
    }

    if (!post && ForumCommentsSub) {
      const condoId = FlowRouter.getParam('condo_id');
      let lang = FlowRouter.getParam("lang") || "fr";

      if (Meteor.isManager) {
        FlowRouter.go("app.gestionnaire.forum.postId", { lang, condo_id: condoId });
      } else {
        FlowRouter.go("app.board.resident.condo.module.forum", { lang, condo_id: condoId });
      }
      return null
    }

    const canSee = post.type === 'post'
      ? Meteor.userHasRight('forum', 'seeSubject', post.condoId)
      : Meteor.userHasRight('forum', 'seePoll', post.condoId)
    const canReply = post.type === 'post'
      ? Meteor.userHasRight('forum', 'writeSubject', post.condoId)
      : Meteor.userHasRight('forum', 'writePoll', post.condoId)
    const canEdit = (post.userId === Meteor.userId() && (post.type === 'post'
      ? Meteor.userHasRight('forum', 'writeSubject', post.condoId)
      : Meteor.userHasRight('forum', 'writePoll', post.condoId)))
    const canDelete = (post.userId === Meteor.userId() || (post.type === 'post'
      ? Meteor.userHasRight('forum', 'deleteSubject', post.condoId)
      : Meteor.userHasRight('forum', 'deletePoll', post.condoId)))
    let tr_commonDetails = ""
    if (post.type === 'post') {
      tr_commonDetails = this.props.dl.postDetails
    } else if (post.type === 'poll') {
      tr_commonDetails = this.props.dl.pollDetails
    }
    let author
    if (!user) {
	    author = this.props.dl.disabledAccount
    } else {
	    author = `${user.firstname} ${user.lastname}`
    }
		if (canSee) {
			return(
				<section className="blue_bg_content" id="mainContent">
					<div className="row">
						<div className="col-md-12 plpr23 plpr20">
							<div className={'panel mt80'}>
								<div className="panel-heading position_relative">
									<div className="sub-tool-bar clearfix">
										<h1 className="pull-left">
										{tr_commonDetails}
										</h1>
										<div className="pull-right action">
											{ !this.state.isToggleOn && canDelete &&
												<button className="btn bordered-btn" onClick={this.removePostPoll}>
													{ this.props.dl.delete }
												</button>
											}
											{ !this.state.isToggleOn && canEdit &&
												<button className="btn bordered-btn" onClick={() => this.handleClick(post._id)}>
													{ this.props.dl.modify }
												</button>
											}

											{ this.state.isToggleOn && canEdit &&
												<div>
													<button
                            className="btn bordered-btn post-edit-save-button"
                            onClick={(e) => this.cancelEditMode()}
                            disabled={isSubmitting}
                          >
														{ this.props.dl.cancel }
													</button>
													<button
                            className="btn bordered-btn editPostButton post-edit-save-button"
                            data-loading-text={`<i class='fa fa-spinner fa-spin'></i> ${this.props.dl.sending} ...`}
                            onClick={(e) => this.handleNewEdit(e, this.props.post._id)}
                            disabled={!this.hasChanged() || (forumType === 'category' && categories.length === 0)}
                          >
														{ this.props.dl.postEditButton }
													</button>
												</div>
											}
										</div>
									</div>
									<span className="header-back-btn" onClick={this.props.goBackToForum}>
										<img
											src='/img/icons/back.svg'
										/>
										{this.props.dl.back}
									</span>
								</div>
							</div>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12 plpr23">
							<div className="dada">
								<div className="card item centerDetailPostBody wwww">
									<div className="detailPostBody">
										<div className="headerPost">
											<div className="headerPostLeft">
												<div className="headerPostPicture">
													{post !== undefined &&
														<Blaze template="userIcon" type="U" userId={post.userId} />
													}
												</div>
												<div className="headerPostInfos">
													<div className="headerPostUsername">
                          <div>{author}</div>
													</div>
													<div className="headerPostSendTime">
                            {
                              !!categories && categories.length > 0 ? (
                                _.map(categories, (cat, i) => {
                                  const last = categories.length === i + 1
                                  const catString = cat[FlowRouter.getParam('lang') || 'fr'] ? `${cat[FlowRouter.getParam('lang') || 'fr']}${last ? '': ', '}`: ''
                                  return <span style={{marginRight: '5px'}} key={i}>{catString}{last && <span style={{marginLeft: '5px'}}>•</span>}</span>
                                })
                              ): null
                            }
                            {!!post && !!post.endDate && post.endDate <= Date.now() ?
                              <div>{this.props.dl.expiredPoll} {this.showDatetime(post.endDate)}</div>
                              // <div>{this.props.dl.expiredPoll} {moment(post.endDate).locale(FlowRouter.getParam('lang') || 'fr').format('lll').toLowerCase()}</div>
                              :
                              <div>{this.props.dl.posted} {this.showDatetime(post.date)}</div>
                              // <div>{this.props.dl.posted} {moment(post.date).locale(FlowRouter.getParam('lang') || 'fr').calendar().toLowerCase()}</div>
                            }{
                              isEdited && <span className="editedTag">({this.props.dl.edited.toLowerCase()})</span>
                            }
													</div>
												</div>
											</div>
										</div>

										{post !== undefined && !this.state.isToggleOn &&
											<div className="subjectPost forumTitles">
												{post.type === 'poll' &&
													<div className="pollSvg"></div>
												}
												<p className="ppp break-line">
													{reactAutoLink(post.subject)}
												</p>
											</div>
										}

										{post && this.state.isToggleOn === post._id &&
											<div className="forum-edit-message">
												<AutoResizeTextArea
													rows={3}
													defaultValue={post.subject}
													value={this.state.newEdit}
													onChange={this.handleChange2}
													placeholder={this.props.dl.changeMyPost}
													className="form-control borderless-textarea"
												/>
												<Blaze
													template="FileList"
													files={this.previewPostFiles()}
													onFileAdded={this.onPostFileChange}
													onRemove={this.onPostFileRemove}
													showDropBox={true}
												/>
                        { forumType === 'category' &&
                        <div className='category-picker-wrapper' style={{marginBottom: 10, marginTop: 15}}>
                          <span className='category-picker-label'>
                            {tl.selectCategories.toUpperCase()}
                            <span className="image-icon mandatory-icon"/>
                          </span>
                          <CategoryPicker
                            onCatSelected={this.onCatSelected}
                            multiple={true}
                            placeholder={tl.select}
                            reset={resetCategoryPicker}
                            customStyles={DetailsForumPost.getCategoryPickerStyle()}
                            initCategories={categories}
                          />
                        </div>
                        }
											</div>
										}

										{
                      post &&
                      post.filesId && post.filesId.length > 0 &&
                      this.state.isToggleOn !== post._id &&
											<div className="fileViewerTemplate">
												<MbFilePreview
													files={this.props.files}
													newFiles={this.props.newFiles}
													layout="5"
												/>
											</div>
										}

										{post && post.type === 'poll' &&
                    <div className="glou">
                      <PollComponent
                        item={post}
                        pollId={post._id}
                        dl={this.props.dl}
                        isEditing={post && post.type === 'poll' && this.state.isToggleOn === post._id}
                      />
                      <div className='poll-expire-date'>
                        {this.showExpireDate(post)}
                      </div>
                    </div>
										}
                    {post && post.type === 'poll' && this.state.isToggleOn === post._id &&
                    <div className="check-box-allow-other">
                      <Blaze
                        template='MbCheckbox'
                        checked={allowOtherAddOp}
                        label={this.props.dl.poll_allow_other_to_add_opt}
                        onClick={() => this.setState({allowOtherAddOp: !allowOtherAddOp})}
                      />
                    </div>
                    }
										<div className="hrStyle">
											<hr/>
										</div>
										<div className="interactionBlock">
											<div className="commentPost">
												{this.props.nbComments !== undefined &&
													<div className="comBlock">
														<div className="commentsSvg"></div>
														<div className="commentsViews">{this.props.nbComments} {this.props.dl.comment}</div>
													</div>
												}
											</div>
											<div className="forumViewPost">
												{this.props.nbViews !== null &&
													<div className="viewBlock">
														<div className="viewsSvg"></div>
														<div className="commentsViews">{this.props.nbViews} {this.props.dl.view}</div>
													</div>
												}
											</div>
										</div>
                    <div className="forumCommentLabelContainer">
                      <span className="forumCommentLabel">
                        {this.props.dl.comments}
                      </span>
										</div>
                    {
                      this.props.comments.length > this.state.limit ? <button className="btn bordered-btn" onClick={() => this.setState({limit: this.state.limit + PER_LOAD})}>
                        { this.props.dl.load_previous_comments }
                      </button>: null
                    }
										<div className="forumAllComments">
											{post &&
												<ForumPostComment
													dl={this.props.dl}
													type={this.props.post.type}
													deletePoll={this.props.deletePoll}
													deleteSubject={this.props.deleteSubject}
													comments={this.state.comments}
													fm={this.props.fm}
												/>
											}
										</div>
                    {canReply && !this.state.isToggleOn &&
                    <div>
                      <div className="mb-comment-message">
                        <div className="mb-comment-row">
                          <div className="forum-tab-content">
                            <AutoResizeTextArea
                              rows={3}
                              value={this.state.comment}
                              onChange={this.handleChange}
                              placeholder={this.props.dl.postAComment}
                              className="form-control borderless-textarea"
                            />
                            <Blaze
                              template="FileList"
                              files={this.previewCommentFiles()}
                              onFileAdded={this.onCommentFilesChange}
                              onRemove={this.onCommentFileRemoved}
                              showDropBox={this.state.files.length > 0}
                            />
                          </div>
                        </div>
                        <div className="mb-comment-actions-forum">
                          <Blaze template="FileUploadButton" files={this.previewCommentFiles()} onFileAdded={this.onCommentFilesChange} />
                          <button className="mb-comment-save btn btn-red forumPostCommentButton" data-loading-text={`<i class='fa fa-spinner fa-spin'></i> ${this.props.dl.sending} ...`} onClick={this.handleSubmit} disabled={this.state.comment === '' && this.state.files.length === 0}>
                            {this.props.dl.postSubmitButton}
                          </button>
                        </div>
                      </div>
                    </div>
                    }
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			);
		} else {
			return (
				<section className="blue_bg_content" id="mainContent">
					<div className="row">
						<div className="col-md-12 plpr23 plpr20">
							<div className={'mt80'}>
								<Blaze template="MbRestrictedAccess" />
							</div>
						</div>
					</div>
				</section>
			)
		}
	};
};

const subAndQuery = props => {
	let condoId = FlowRouter.getParam('condo_id')
	let postId = FlowRouter.getParam('postId') || FlowRouter.getParam('wildcard')
	let userId = Meteor.userId()
	let user = []
	let post = []
	let nbViews = null
	let files = null
	let newFiles = []
	let isItMyPost = false

	let ForumCommentsSub = Meteor.subscribe('forumCommentPosts', condoId, postId)
	const comments = ForumCommentPosts.find({
		postId: postId
	}, { sort: { commentDate: 1} } ).fetch()

	post = ForumPosts.findOne( { '_id': postId } )

	if (post !== undefined) {
		nbViews = Object.keys(post.views).length
		user = UsersProfile.findOne( { '_id': post.userId } )
	}

	if (post && post.filesId && post.filesId.length > 0) {
    files = ForumFiles.find({'_id': {$in: post.filesId.filter(file => !file.fileId)}})
    newFiles = post.filesId.filter(file => file && file.fileId)
  }

  let dicoLang = props.dicoLang.forumLang

	const deletePoll = Meteor.userHasRight('forum', 'deletePoll')
	const deleteSubject = Meteor.userHasRight('forum', 'deleteSubject')

	let fm = new FileManager(ForumFiles, {
		isForumFiles: true,
		condoId: FlowRouter.getParam('condo_id'),
		isValid: false
	})


	return {
		comments: comments,
		nbComments: comments.length,
		post: post,
		user: user,
		nbViews: nbViews,
    files: files,
    newFiles,
		dl: dicoLang,
		deletePoll: deletePoll,
		deleteSubject: deleteSubject,
		isItMyPost: isItMyPost,
		fm: fm,
		ForumCommentsSub: ForumCommentsSub.ready()
	}
}

export default compose(
  withTracker(subAndQuery),
  withForumContext
)(DetailsForumPost)
