import React from 'react';
import RDom from 'react-dom';
import Lightbox from 'react-images';

import './fileViewer.less';

class FileViewer extends React.Component {
  constructor(props) {
    super(props)

    this.itemPadding = 8

    this.state = {
      itemDimension: 0,
      previewIsOpen: false,
      visibleImage: 0
    }
  }

  calculateItemDimension() {
    const containerW = RDom.findDOMNode(this.imagesContainer).getBoundingClientRect().width;

    this.setState({
      itemDimension: (containerW / 2 ) - (this.itemPadding * 2)
    });
  }

  componentDidMount() {
    this.calculateItemDimension();

    $(window).resize(() => {
      this.calculateItemDimension()
    })
  }

  onClickNext = () => {
    this.setState({visibleImage: this.state.visibleImage + 1})
  }

  onClickPrev = () => {
    this.setState({visibleImage: this.state.visibleImage - 1})
  }

  parseImageUrl(o) {
    return `${o._downloadRoute}/${o._collectionName}/${o._id}/large/${o._id}${o.extensionWithDot}`;
  }

  openLightBox(index) {
    this.setState({
      visibleImage: index,
      previewIsOpen: true
    })
  }

  render() {
    const { files } = this.props;
    const images = _.filter(files.each(), f => f.type.match(/image/))
    const documents = _.filter(files.each(), f => !f.type.match(/image/))

    return (
      <div className="file-viewer-container">
        <Lightbox
          images={images.map(o => ({
            src: this.parseImageUrl(o)
          }))}
          isOpen={this.state.previewIsOpen}
          onClose={() => this.setState({previewIsOpen: false})}
          currentImage={this.state.visibleImage}
          onClickNext={this.onClickNext}
          onClickPrev={this.onClickPrev}
        />
        <div className="viewer-images" ref={ref => this.imagesContainer = ref}>
          { images.map((o, i) => {
            return (
              <div className="viewer-image-item" key={o._id} onClick={() => this.openLightBox(i)}>
                <div
                  className="image-viewable"
                  style={{
                    background: `url(${this.parseImageUrl(o)}) no-repeat center center`,
                    width: `${this.state.itemDimension}px`,
                    height: `${this.state.itemDimension}px`,
                  }}
                />
              </div>
            )
          })}
        </div>

        { !!documents.length &&
          <div className="viewer-files"  ref={ref => this.filesContainer = ref}>a</div>
        }
      </div>
    )
  }
}

export default FileViewer;