import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data'
import Feed from './feed'
import ForumContext from './context'

class ContextProvider extends Component {
  render () {
    const { contextData, dicoLang, condoId } = this.props
    return (
      <div>
        <ForumContext.Provider value={contextData}>
          <Feed dicoLang={dicoLang} condoId={condoId} />
        </ForumContext.Provider>
      </div>
    )
  }
}

export default withTracker(props => {
  const { dicoLang: { forumLang } } = props
  const rights = {}
  rights.seePoll = Meteor.userHasRight('forum', 'seePoll')
  rights.seePost = Meteor.userHasRight('forum', 'seeSubject')
  rights.writePoll = Meteor.userHasRight('forum', 'writePoll')
  rights.writePost = Meteor.userHasRight('forum', 'writeSubject')
  rights.deleteOtherPoll = Meteor.userHasRight('forum', 'deletePoll')
  rights.deleteOtherPost = Meteor.userHasRight('forum', 'deleteSubject')
  const condoId = FlowRouter.getParam('condo_id') || props.condoId
  const condo = Condos.findOne({ _id: condoId })
  const theme = {
    primary: '#69D2E7',
    secondary: '#FE0900'
  }
  if (condo &&
    condo.settings &&
    condo.settings.colorPalette &&
    condo.settings.colorPalette.primary &&
    condo.settings.colorPalette.secondary) {
    theme.primary = condo.settings.colorPalette.primary
    theme.secondary = condo.settings.colorPalette.secondary
  }
  let forumType = 'normal'
  let categories = []
  const forumModule = CondosModulesOptions.findOne({ condoId: condoId }, { fields: { 'forum.type': 1 } })
  if (forumModule && forumModule.forum && forumModule.forum.type) {
    forumType = forumModule.forum.type
  } else if (condoId) {
    console.warn(`Forum type could not be found for condo with id "${condoId}" ! This might cause potentials problems`)
  }
  let ready = true
  if (forumType === 'category') {
    const categoriesSub = Meteor.subscribe('forumCategories', condoId)
    const categoriesQuery = ForumCategories.findOne({})
    ready = categoriesSub.ready()
    if (ready &&
      categoriesQuery &&
      Array.isArray(categoriesQuery.categories)) {
      categories = categoriesQuery.categories
    }
    if (ready &&
      condoId &&
      (!categoriesQuery ||
        !Array.isArray(categoriesQuery.categories))) {
      console.warn(`Forum categories could not be found for condo with id "${condoId}" ! This might cause potentials problems`)
    }
  }
  return {
    contextData: {
      ready,
      forumType,
      categories,
      tl: forumLang,
      locale: FlowRouter.getParam('lang') || 'fr',
      user: {
        id: Meteor.userId(),
        rights,
        status: Meteor.isManager ? 'manager' : 'occupant'
      },
      condoId,
      theme
    },
    dicoLang: props.dicoLang
  }
})(ContextProvider)
