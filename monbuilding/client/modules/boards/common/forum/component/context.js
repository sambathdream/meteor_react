import React from 'react'

const ForumContext = React.createContext({
  forumType: 'normal',
  categories: [],
  tl: {},
  user: {
    id: '',
    rights: {},
    status: ''
  },
  condoId: '',
  theme: {
    primary: '#69D2E7',
    secondary: '#FE0900'
  },
  locale: 'fr'
})

export default ForumContext
