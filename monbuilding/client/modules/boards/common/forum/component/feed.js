import React, { Component } from 'react'
import cx from 'classnames'
import ForumFeed from './forumfeed'
import LastestActive from './lastest_active'
import FollowingPosts from './following_posts'
import MyPostWidget from './myPostWidget'
import DetailsForumPost from './detail_forum_post'
import Blaze from 'meteor/gadicc:blaze-react-component'
import { withTracker } from 'meteor/react-meteor-data'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import './forum.less'
import '../style/forumfeed.less'
import FormManager from './formManager'
import SearchBar from './searchBar'
import _ from 'lodash'
import withForumContext from './withForumContext'
import CategoryPicker from './categoryPicker'
import { compose } from 'lodash/fp'

const FOLLOWING_POST_LIMIT = 3

class Feed extends Component {
  constructor (props) {
    super(props)
    const condoId = FlowRouter.getParam('condo_id') || props.condoId
    const condo = Condos.findOne({ _id: condoId })
    this.state = {
      userId: Meteor.userId(),
      condoId: condoId,
      condoName: condo ? condo.getName() : '',
      lang: FlowRouter.getParam('lang') || 'fr',
      switchFormValue: true,
      listId: [],
      unfollow: null,
      limitFollowingPosts: FOLLOWING_POST_LIMIT,
      selectedPostId: null,
      filter: '',
      isAllSelected: condoId === 'all',
      authors: [],
      category: null
    }
    this.residentBadges = BadgesResident.findOne();
    this.gestionnaireBadges = BadgesGestionnaire.findOne({ _id: condoId });
    if(this.residentBadges!==undefined){
      this.forums = this.residentBadges.forum;
    } else {
      this.forums = this.gestionnaireBadges ? this.gestionnaireBadges.forum : 0;
    }

    let resident = Residents.findOne({ userId: Meteor.userId })
    if (resident) {
      const condo = resident.condos.find(c => c.condoId === condoId)
      if (condo) {
        this.joined = +moment(resident.condos.find(c => c.condoId === condoId).joined).format('x')
      }
    }
    this.removeDuplicates = this.removeDuplicates.bind(this)
    this.switchToPostForm = this.switchToPostForm.bind(this)
    this.switchToPollForm = this.switchToPollForm.bind(this)
    this.goToDetailPost = this.goToDetailPost.bind(this)
    this.goBackToForum = this.goBackToForum.bind(this)
    this.readAll = this.readAll.bind(this)
    this.onFilterChange = _.debounce(this.onFilterChange.bind(this), 500, true)

    this.filterPostsList = this.filterPostsList.bind(this)
    this.renderPostsList = this.renderPostsList.bind(this)
    this.onCatSelected = this.onCatSelected.bind(this)
    this.emptyStateName = domainConfig.name
    this.emptyStateSrc = '/logo/enterprise.png'

    Meteor.subscribe('unFollowForumPosts', (result, error) => {
      this.setState({unfollow: true})
    })
    Meteor.call('setNewAnalytics', {type: "module", module: "forum", accessType: "web", condoId: this.state.condoId})
  }

  getPostId() {
    return FlowRouter.getParam('postId') || FlowRouter.getParam('wildcard')
  }

  componentWillUnmount () {
    window.onpopstate = null
  }
  componentDidMount () {
    const { context: { categories } } = this.props
    window.onpopstate = () => {
      const selectedPostId = this.getPostId();
      this.setState({selectedPostId});
    }

    const { feed } = this.props
    let postId = this.getPostId()

    if (!!postId) {
      let condoId = this.state.condoId
      let lang = FlowRouter.getParam('lang')
      this.setState({ selectedPostId: postId })
      $('#postDetailsContainer').css('display', 'block')
      $('#postFeedContainer').css('position', 'fixed')
    }
    if (feed && Array.isArray(feed)) {
      const authors = []
      feed.forEach(post => {
        const author = UsersProfile.findOne({_id: post.userId})
        if (author && typeof author._id === 'string') {
          authors.push(author)
        }
      })
      if (authors.length > 0) {
        this.setState({ authors })
      }
    }
  }

  componentDidUpdate (prevProps, prevState) {
    const { feed } = this.props
    const { authors: authorsState } = this.state
    if (!_.isEqual(prevProps.feed, feed) && feed && Array.isArray(feed)) {
      const authors = []
      feed.forEach(post => {
        const author = UsersProfile.findOne({_id: post.userId})
        if (author && typeof author._id === 'string') {
          authors.push(author)
        }
      })
      if (!_.isEqual(authors, authorsState)) {
        this.setState({ authors })
      }
    }
  }

  removeDuplicates (arr, prop) {
    var obj = {}
    for (var i = 0, len = arr.length; i < len; i++) {
      if (!obj[arr[i][prop]]) obj[arr[i][prop]] = arr[i]
    }

    var newArr = [];
    for ( var key in obj ) newArr.push(obj[key]);

    return newArr.slice(0, 3);
  };

  switchToPostForm() {
    if (this.state.switchFormValue  === false)
      this.setState({switchFormValue : true})
  }

  switchToPollForm() {
    if (this.state.switchFormValue  === true)
      this.setState({switchFormValue : false})
  }

  readAll () {
    this.forums = 0;
    Meteor.call('markAllForumPostAsRead')
  }

  // CDD_cursor_forum () {
  //   return _.union(Meteor.listCondoUserHasRight("forum", "seePoll"), Meteor.listCondoUserHasRight("forum", "seeSubject"));
  // }

  // condoIdCB () {
  //   let self = this
  //   return (condoId) => {
  //     if (condoId) {
  //       if (condoId != FlowRouter.getParam('condo_id')) {
  //         this.props.condoId = condoId;
  //         const lang = FlowRouter.getParam("lang") || "fr";
  //         FlowRouter.setParams({ condo_id: condoId})
  //         this.setState({ condoName: Condos.findOne(condoId).getName() })
  //       }
  //     }
  //   }
  // }

  goToDetailPost (postId) {
    let condoId = this.state.condoId
    let lang = FlowRouter.getParam("lang") || "fr";

    if (Meteor.isManager) {
      FlowRouter.go("app.gestionnaire.forum.postId", { lang, condo_id: condoId, postId: postId })
    } else {
      FlowRouter.go("app.board.resident.condo.module.forum.post", { lang, condo_id: condoId, postId: postId })
    }

    this.setState({selectedPostId: this.getPostId()})
    $('#postDetailsContainer').css('display', 'block')
    $('#postFeedContainer').css('position', 'fixed')
  }

  goBackToForum () {
    let condoId = this.state.condoId
    let lang = FlowRouter.getParam("lang") || "fr";

    if (Meteor.isManager) {
      FlowRouter.go("app.gestionnaire.forum.postId", { lang, condo_id: condoId });
    } else {
      FlowRouter.go("app.board.resident.condo.module.forum", { lang, condo_id: condoId });
    }
    // this.setState({ selectedPostId: null })
    $('#postDetailsContainer').css('display', 'none')
    $('#postFeedContainer').css('position', '')
  }

  toggleFollowPosts() {
    Meteor.call('toggleAllForumNotification', this.state.condoId)
  }

  isNotFollowing(post) {
    const unfollowPost =  _.find(this.props.unfollowLists, un => {
      return un.postId === post._id
    });

    return !!unfollowPost;
  }

  onFilterChange (filter) {
    this.setState({filter})
  }

  filterPostsList () {
    const { feed, context: { forumType } } = this.props
    const { filter, authors, category } = this.state
    let filteredList = filter === '' ? feed : feed.filter(post => {
      if (post.subject.toLowerCase().includes(filter.toLowerCase())) {
        return true
      }
      const author = authors.find(author => author && author._id === post.userId)
      if (author) {
        const { firstname, lastname } = author
        if (typeof firstname === 'string' && typeof lastname === 'string') {
          const authorName = `${firstname} ${lastname}`
          if (authorName.toLowerCase().includes(filter.toLowerCase())) {
            return true
          }
        }
      }
      return false
    })
    if (forumType === 'category' && category) {
      filteredList = feed.filter(post => {
        return !!(post.categoriesId &&
          Array.isArray(post.categoriesId) &&
          post.categoriesId.find(categoryId => categoryId === category.id))
      })
    }
    return filteredList
  }

  renderPostsList (filteredList) {
    let list
    if (filteredList.length > 0) {
      list = filteredList.map(item => {
        return (
          <ForumFeed
            item={item}
            key={item._id}
            dl={this.props.dl}
            following={!this.isNotFollowing(item)}
            goToDetailPost={this.goToDetailPost}
            joined={this.joined}
          />
        )
      })
    } else {
      list = <div className='post-list-no-result'>{this.props.dl.no_result}</div>
    }
    return list
  }

  componentWillReceiveProps (nextProps) {
    if (Meteor.isManager) {
      FlowRouter.setParams({ condo_id: nextProps.condoId})
      const condo = Condos.findOne({ _id: nextProps.condoId })
      this.setState({ isAllSelected: nextProps.condoId === 'all', condoId: nextProps.condoId, condoName: condo ? condo.getName(): '' })
    }
  }

  onCatSelected (category) {
    this.setState({
      category: category[0] || null
    })
  }

  static getCategoryPickerStyle () {
    return ({
      container: (base) => ({
        ...base,
        width: 250
      }),
      valueContainer: (base) => ({
        ...base,
        padding: '2px 8px'
      })
    })
  }

  render () {
    const { context: { forumType, tl } } = this.props
    const { category } = this.state
    if (this.props.feed.length > 0) {
      this.state.listId = this.removeDuplicates(this.props.feed, 'userId')
    }
    // console.log('feed', feed)
    let isSubscribeReadyPosts = !!this.props.ForumPostsSub
    let isSubscribeReadyFollow = !!this.props.UnFollowSub
    let canSeePost = Meteor.userHasRight('forum', 'seeSubject', this.state.condoId, Meteor.userId())
    // let canSeePoll = Meteor.userHasRight('forum', 'seePoll', this.state.condoId, Meteor.userId())
    const filteredPostsList = this.filterPostsList()
    const postId = this.getPostId()

    if (!postId) {
      $('#postDetailsContainer').css('display', 'none')
      $('#postFeedContainer').css('position', '')
    } else {
      $('#postDetailsContainer').css('display', 'block')
      $('#postFeedContainer').css('position', 'fixed')
    }

    if (canSeePost) {
      return (
        <section className='blue_bg_content' style={{ position: 'relative' }} id='mainContent'>
          <div id={'postDetailsContainer'} className={Meteor.isManager ? 'post_details_container_manager' : 'post_details_container'}>
            { postId &&
            <DetailsForumPost
              postId={postId}
              dicoLang={this.props.dicoLang}
              goBackToForum={this.goBackToForum}
            />
            }
          </div>
          <div id={'postFeedContainer'} className='row'>
            <div className='col-md-12 plpr23 plpr20'>
              <div className={Meteor.isManager ? 'panel' : 'panel mt80'}>
                <div className='panel-heading position_relative centerHeaderDisplay'>
                  <div className="sub-tool-bar clearfix">
                    <h1 className="pull-left">
                      Forum {Meteor.isManager ? '' : '- ' + this.state.condoName }
                    </h1>
                    <div className="pull-right action vertical-centered">
                      <button className="btn-text" onClick={() => this.toggleFollowPosts()}>
                        { this.props.dl.followAll }
                      </button>
                      <span className="starButton" onClick={() => this.toggleFollowPosts()}>
                        <div
                          className={cx({
                            starButtonOn: this.props.unfollowLists.length !== 0,
                            starButtonOff: this.props.unfollowLists.length === 0
                          })}
                        />
                      </span>
                      <div
                        class='readAllWrapper'
                        onMouseEnter={() => this.setState({helper: true})}
                        onMouseLeave={() => this.setState({helper: false})}
                      >
                        <img className={this.forums !== 0 ? 'readAll' : 'readAllPointer'} src="/img/icons/read-all.png" height="24"   onClick={this.readAll}></img>

                        { this.state.helper && this.forums !== 0 &&
                          <div class="pop-up-ads-Helper">
                            <span>
                              {this.props.dl.read_all}
                            </span>
                          </div>
                        }
                      </div>
                      {
                        forumType === 'category' &&
                        <CategoryPicker
                          onCatSelected={this.onCatSelected}
                          placeholder={tl.filterBy}
                          customStyles={Feed.getCategoryPickerStyle()}
                        />
                      }
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='col-md-12 plpr23 plpr20'>
              <div className='row'>
                <div className='col-lg-9 col-md-8 col-sm-12'>
                  <div className='card item'>
                    <SearchBar
                      onFilterChange={this.onFilterChange}
                      placeholder={this.props.dl.search}
                      containerStyle={'search-input-wrapper'}
                    />
                    <FormManager
                      condoId={this.state.condoId}
                      dl={this.props.dl}
                      fm={this.props.fm}
                    />
                    {isSubscribeReadyPosts ?
                      <div className='card-block  small-padding alignDisplay'>
                        { this.props.feed && this.props.feed.length > 0 ? this.renderPostsList(filteredPostsList) : (
                          <div className="mb-no-content">
                            <div className="mb-no-content-header">
                              <div className="mb-no-content-row">
                                <div className="mb-no-content-avatar">
                                  <img src={this.emptyStateSrc} alt="MonBuilding" />
                                </div>
                                <div className="mb-no-content-info">
                                  <div className="mb-no-content-user">{this.emptyStateName}</div>
                                  <div className="mb-no-content-timestamp">
                                    {!!Meteor.user().createdAt && moment(Meteor.user().createdAt).locale(FlowRouter.getParam('lang') || 'fr').format('DD MMM YYYY[' + this.props.dl.at + ']HH:mm')}
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="mb-no-content-title">
                              {this.props.dl.no_post_title}
                            </div>
                            <div className="mb-no-content-desc break-line" style={{ marginTop: 10 }}>
                              {this.props.dl.no_post_desc}
                            </div>
                          </div>
                        )}
                      </div>
                      :
                      <Blaze template="loader" />
                    }
                  </div>
                </div>
                <div className="forumRightSide col-lg-3 col-md-4 col-sm-12">
                  <div className="forumItems">
                    <div className="forumRightSideTitle">{this.props.dl.lastestActive}</div>
                    <div className="card card-block no-pad">
                      {isSubscribeReadyPosts ?
                        <div>
                          { !!this.state.listId && this.state.listId.length ? _.map(this.state.listId, (item, i) => {
                            return (
                              <LastestActive item={item} key={i} dl={this.props.dl} />
                            )
                          }) : (
                            <div className="mb-widget-no-data-text">
                              { this.props.dl.noActivity }
                            </div>
                          )}
                        </div>
                        :
                        <Blaze template="loader" />
                      }
                    </div>
                    <div className="forumRightSideTitle">{this.props.dl.following}</div>
                    {isSubscribeReadyFollow ?
                      <div className="card card-block no-pad">
                        {this.props.feed !== null && this.state.unfollow != null &&
                        <FollowingPosts
                          limit={this.state.limitFollowingPosts}
                          initialLimit={FOLLOWING_POST_LIMIT}
                          onShowAll={() => this.setState({limitFollowingPosts: 0})}
                          onShowLess={() => this.setState({limitFollowingPosts: FOLLOWING_POST_LIMIT})}
                          dl={this.props.dl}
                          goToDetailPost={this.goToDetailPost}
                          category={category}
                        />
                        }
                      </div>
                      :
                      <Blaze template="loader" />
                    }
                    <div className="forumRightSideTitle">
                      { this.props.dl.myPosts.toUpperCase() }
                    </div>
                    <div className="card card-block no-pad">
                      { this.props.myPosts && !!this.props.myPosts.length ? (
                        <MyPostWidget
                          posts={this.props.myPosts}
                          goToDetailPost={this.goToDetailPost}
                          category={category}
                        />
                      ) : (
                        <div className="mb-widget-no-data-text">
                          { this.props.dl.noCreatedPost }
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      );
    } else {
      return (
        <section className="blue_bg_content" id="mainContent">
          <div style={{ display: 'block' }}>
            <div className=''>
              <div className={Meteor.isManager ? 'panel' : 'panel mt80'}>
                <div className='panel-heading position_relative centerHeaderDisplay'>
                  <div className="sub-tool-bar clearfix">
                    <h1 className="pull-left">
                      Forum { !this.state.isAllSelected && <div style={{display: 'inline'}}> - {this.state.condoName}</div> }
                    </h1>
                    <div className="pull-right action vertical-centered">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {!this.state.isAllSelected &&
            <div className='col-md-12 plpr23 plpr20'>
              <div className='row'>
                <div className='col-12'>
                  <Blaze template="MbRestrictedAccess" />
                </div>
              </div>
            </div>
            }
            {this.state.isAllSelected &&
            <div className="whiteFullBackground">
              <Blaze template="MbWarning" content={this.props.dl.select_building_first_forum} />
            </div>
            }
          </div>
        </section>
      )
    }
  }
}

const subAndQueries = props => {
  const condoId = FlowRouter.getParam('condo_id') || props.condoId
  let seePoll = Meteor.userHasRight('forum', 'seePoll')
  let seeSubject = Meteor.userHasRight('forum', 'seeSubject')
  let forumType = []
  let unFollowSub = Meteor.subscribe('unFollowForumPosts')

  let ForumPostsSub = Meteor.subscribe('fetchForumPosts', [condoId])
  let ForumNewPost = Meteor.subscribe('newForumPost', [condoId])

  if (seePoll === true) {
    forumType.push('poll')
  }
  if (seeSubject === true) {
    forumType.push('post')
  }
  let feed = ForumPosts.find( {type: {$in: forumType}}, {sort: {date: -1}} ).fetch()
  const myPosts = ForumPosts.find({
    type: {$in: forumType},
    userId: Meteor.userId()
  }, {
    sort: {date: -1}
  }).fetch()

  let fm = new FileManager(ForumFiles, {isForumFiles: true, condoId: condoId, isValid: false})

  const unfollowLists = UnFollowForumPosts.find({
    'postId': {$in: _.map(feed, f => f._id)},
    'unfollowList' : {
      $elemMatch:{
        "$in": [Meteor.userId()],
        "$exists": true
      }
    }
  }).fetch()

  let dicoLang = props.dicoLang.forumLang
  return {
    ForumPostsSub: ForumPostsSub.ready(),
    ForumNewPostSub: ForumNewPost.ready(),
    UnFollowSub: unFollowSub.ready(),
    feed: feed,
    fm: fm,
    dl: dicoLang,
    unfollowLists,
    myPosts,
    whitCategories: false
  }
}

export default compose(
  withTracker(subAndQueries),
  withForumContext
)(Feed)
