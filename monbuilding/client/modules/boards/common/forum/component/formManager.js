import React, { Component } from 'react';
import PostForm from './post_form'
import PollForm from './poll_form'

const formType = {
  post: "post",
  poll: "poll",
  none: "none"
}

export default class FormManager extends Component {
  constructor(props) {
    super(props)

    this.state = {
      userId : Meteor.userId(),
      condoId : FlowRouter.getParam('condo_id'),
      formType: formType.none
    }
    this.findForm = this.findForm.bind(this)
    this.switchForm = this.switchForm.bind(this)
  }

  getCondoId () {
    return this.state.condoId || this.props.condoId
  }

  canSeePost() {
    return Meteor.userHasRight('forum', 'seeSubject', this.getCondoId(), Meteor.userId())
  }

  canSeePoll() {
    return Meteor.userHasRight('forum', 'seePoll', this.getCondoId(), Meteor.userId())
  }

  canWritePost() {
    return Meteor.userHasRight('forum', 'writeSubject', this.getCondoId(), Meteor.userId()) && this.canSeePost()
  }

  canWritePoll() {
    return Meteor.userHasRight('forum', 'writePoll', this.getCondoId(), Meteor.userId()) && this.canSeePoll()
  }

  findForm() {
    if (this.canSeePost() && this.canWritePost()) {
      return formType.post
    }
    if (this.canSeePoll() && this.canWritePoll()) {
      return formType.poll
    }
    return formType.none
  }

  componentDidMount() {
    if (this.state.typeForm !== this.findForm()) {
      this.setState({formType: this.findForm()})
    }
  }

  switchForm() {
    if (this.state.formType === formType.poll && this.canWritePost()) {
      this.setState({
        formType: formType.post
      })
      return
    }
    if (this.state.formType === formType.post && this.canWritePoll()) {
      this.setState({
        formType: formType.poll
      })
      return
    }
    if (!this.canWritePost() && !this.canWritePoll()) {
      this.setState({
        formType: formType.none
      })
    }
  }

  render() {
    return (
      <div>
        {
          (this.canWritePost() || this.canWritePoll()) &&
          <div className='card-block small-padding feed_line1'>
            {
              <div className="forum-tab-content no-pad">
                {this.state.formType === formType.post &&
                <PostForm
                  condoId={this.getCondoId()}
                  switchForm={this.switchForm}
                  titleUpdated={title => this.setState({title})}
                  title={this.state.title}
                  fm={this.props.fm}
                  dl={this.props.dl}
                />
                }
                {this.state.formType === formType.poll &&
                <PollForm
                  condoId={this.getCondoId()}
                  switchForm={this.switchForm}
                  titleUpdated={title => this.setState({title})}
                  title={this.state.title}
                  fm={this.props.fm}
                  dl={this.props.dl}
                  theme={this.props.theme}
                />
                }
              </div>
            }
          </div>
        }
      </div>
    )
  }
}
