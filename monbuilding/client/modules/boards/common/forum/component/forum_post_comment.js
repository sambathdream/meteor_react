import React, { Component } from 'react';
import Blaze from 'meteor/gadicc:blaze-react-component';
import moment from 'moment';
import { withTracker } from 'meteor/react-meteor-data';
import AutoResizeTextArea from 'react-autosize-textarea';

import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import './following_posts.less';
import { Email } from '/common/lang/lang.js'

class ForumPostComment extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isToggleOn: null,
			newEdit : "",
			userId : Meteor.userId(),
			condoId : FlowRouter.getParam('condo_id'),
      tmpFiles: []
		};

		this.handleClick = this.handleClick.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
    this.removeComment = this.removeComment.bind(this)
	};

	handleClick(item) {
    this.setState({
      isToggleOn: item._id,
      newEdit: item.comment,
      tmpFiles: item.filesId || [],
      old: item.comment
    })
	}

	handleChange(e) {
		this.setState({newEdit: e.target.value})
	}

	allowSubmitComment() {
    // If return true, will disable the post button
    // If return false, will enable the post button
	  return (this.state.old === this.state.newEdit) || (this.state.newEdit === '' && this.state.tmpFiles.length === 0);
  }

	bindKeyUp(e, id) {
	    if (e && e.key === 'Enter' && !e.shiftKey) {
	        e.preventDefault();
	        this.handleSubmit(e, id);
      }
  }

  cancelEdit() {
    this.setState({
      isToggleOn: false,
      newEdit: '',
      tmpFiles: []
    })
  }

	async handleSubmit(e, id) {
    e.preventDefault()
    $('.buttonPostPoll').button('loading');

    const existingFiles = _.filter(this.state.tmpFiles, f => typeof f === 'string' || f.fileId);
    const newFiles = _.filter(this.state.tmpFiles, f => typeof f !== 'string' && !f.fileId);

    this.props.fm.batchUpload(newFiles)
      .then(files => {
        const data = {
          condoId: this.state.condoId,
          commentId: id,
          newEdit: this.state.newEdit,
          filesId: existingFiles.concat(_.map(files, f => f._id)),
        }

        Meteor.call('modifComment', data, (error, result) => {
          $('.buttonPostPoll').button('reset');
          if (!error) {
            this.setState({
              isToggleOn: false,
              newEdit: '',
              tmpFiles: []
            })
          } else {
            sAlert.error(error);
          }
        });
      })
      .catch(err => {
        console.error(err);
        sAlert.error(this.props.dl[err.reason])
        $('.buttonPostPoll').button('reset');
      })
  }

  removeComment(id) {
      const condoId = FlowRouter.getParam('condo_id');

      bootbox.confirm({
          size: "medium",
          title: "Confirmation",
          message: this.props.dl.delete_comment_confirmation,
          buttons: {
              'cancel': { label: this.props.dl.cancel, className: "btn-outline-red-confirm" },
              'confirm': { label: this.props.dl.delete, className: "btn-red-confirm" }
          },
          backdrop: true,
          callback: function (result) {
              if (result) {
                  Meteor.call('removeComment', {
                    condoId: condoId,
                    commentId: id
                  })
              }
          }
      })
  }

  onFileAdded(files) {
	  this.setState({
      tmpFiles: [...this.state.tmpFiles, ...files]
    })
  }

  onFileRemove(fileId) {
    this.setState({
      tmpFiles: _.filter(this.state.tmpFiles, f => f.fileId ? f.fileId !== fileId : (f.id ? f.id !== fileId : f !== fileId))
    })
  }

  getPreviewFiles() {
    const preFiles = _.filter(this.state.tmpFiles, f => typeof f !== 'string')
    let externalFiles = _.filter(this.state.tmpFiles, f => typeof f === 'string')

    const newFileIds = externalFiles.filter(file => file.fileId)
    externalFiles = externalFiles.filter(file => !file.fileId)

    if (externalFiles) {
      const fileCursor = ForumFiles.find({_id: {$in: externalFiles}})

      const filesWithPreview = _.map(fileCursor.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(preFiles),
        ...newFileIds
      ]
    }

    return []
  }

  haveDeletePrivilege(item) {
	  if (this.state.userId === item.userId) {
	    return false;
    } else {
	    return (this.props.deletePoll && this.props.type === 'poll') || (this.props.deleteSubject && this.props.type === 'post');
    }
  }

  filterFiles(fileIds) {
	  const files = ForumFiles.find({'_id': {$in: fileIds}});

	  return files;
  }

  showDatetime(timestamp) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  }

	render () {
    return this.props.comments && this.props.comments.length > 0 ? _.map(this.props.comments, (item, i) => {
      let author = this.props.dl.disabledAccount
      if (this.props.users && this.props.users[i] && this.props.users[i].firstname) {
        author = `${this.props.users[i].firstname} ${this.props.users[i].lastname}`
      }
      return (
        <div className="mb-comment-wrapper" key={i}>
          <div className="mb-comment-row">
            <div className="mb-comment-avatar">
              <Blaze template="userIcon" type="U" userId={item.userId}/>
            </div>
            <div className="mb-comment-content">
              <div className="mb-comment-username">
                <div>{author}</div>
              </div>
              <div className="mb-comment-timestamp">
                <div>{item.updatedAt ? this.props.dl.edited : this.props.dl.posted} {this.showDatetime(item.commentDate)}</div>
              </div>
              <div className="mb-comment-text">
                { this.state.isToggleOn === item._id ? (
                  <div className="mb-comment-text-edit">
                    <div className="mb-comment-edit-wrapper">
                      <AutoResizeTextArea
                        rows={3}
                        value={this.state.newEdit}
                        onChange={this.handleChange}
                        className="form-control mb-textarea"
                        onKeyUp={e => this.bindKeyUp(e, item._id)}
                      />

                      <Blaze
                        template="FileList"
                        files={this.getPreviewFiles(item)}
                        onFileAdded={(files) => this.onFileAdded(files)}
                        onRemove={(fileId) => this.onFileRemove(fileId)}
                        showDropBox={true}
                      />

                      <div className="mb-comment-edit-action">
                        <button className="btn btn-red" onClick={() => this.cancelEdit()}>
                          { this.props.dl.cancel }
                        </button>
                        <button
                          className="btn pull-right btn-red"
                          onClick={(e) => this.handleSubmit(e, item._id)}
                          data-loading-text={`<i class='fa fa-spinner fa-spin'></i> ${this.props.dl.sending} ...`}
                          disabled={this.allowSubmitComment()}
                        >
                          {this.props.dl.postSubmitButton}
                        </button>
                      </div>
                    </div>
                  </div>
                ) : (
                  <p className="break-line">
                    { reactAutoLink(item.comment) }
                  </p>
                )}
              </div>

              { this.state.isToggleOn !== item._id &&
                <div className="mb-comment-attachment">
                  {item.filesId && item.filesId.length > 0 &&
                    <MbFilePreview
                      files={this.filterFiles(item.filesId.filter(file => typeof file === 'string'))}
                      newFiles={item.filesId.filter(file => file && file.fileId)}
                      layout='5'
                    />
                  }
                </div>
              }
            </div>

            { this.state.isToggleOn !== item._id &&
              <div className="mb-comment-action">
                {this.state.userId === item.userId &&
                  <div className="editOrDelete">
                    <div className="deleteSvg cursorPointer" onClick={() => this.removeComment(item._id)}></div>
                    <div className="editSvg cursorPointer" onClick={() => this.handleClick(item)}></div>
                  </div>
                }

                { this.haveDeletePrivilege(item) &&
                  <div className="editOrDelete">
                    <div className="deleteSvg cursorPointer" onClick={() => this.removeComment(item._id)}></div>
                  </div>
                }
              </div>
            }

          </div>
        </div>
      )
    }) : null
	}
}

export default ForumPostCommentContainer = withTracker((props) => {
  let users = []
  if (users && props.comments.length > 0) {
      for (var i = 0; i < props.comments.length; i++) {
          users[i] = UsersProfile.findOne({'_id': props.comments[i].userId})
      }
  }

  return {
    users: users,
  }
})(ForumPostComment);
