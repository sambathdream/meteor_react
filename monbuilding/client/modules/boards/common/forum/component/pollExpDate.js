import React, { Component } from 'react'
import moment from 'moment'
import DateTime from 'react-datetime'
import Blaze from 'meteor/gadicc:blaze-react-component'

export default class PollExpDate extends Component {
  constructor (props) {
    super(props)
    this.switchExpiryDate = this.switchExpiryDate.bind(this)
    this.onExpDatePicked = this.onExpDatePicked.bind(this)
  }

  switchExpiryDate () {
    const { onSwitch, switched } = this.props
    if (typeof onSwitch === 'function') {
      onSwitch(!switched)
    }
  }

  onExpDatePicked (expDate) {
    if (!moment.isMoment(expDate)) {
      return
    }
    const { onDatePicked } = this.props
    const minDate = moment().add(10, 'minutes')
    if (!expDate.isAfter(minDate)) {
      expDate = minDate
    }
    if (typeof onDatePicked === 'function') {
      onDatePicked(expDate)
    }
  }

  render () {
    const { dl, expDate, switched, containerStyle } = this.props
    return (
      <div className='datePickerBlock' style={containerStyle}>
        <div className='adjust-color'>
          <Blaze
            template='MbCheckbox'
            checked={switched}
            label={dl.add_expiration_date}
            onClick={this.switchExpiryDate}
          />
        </div>
        {
          switched === true &&
          <div className='expiration-wrapper'>
            <div className='datePickerSvg' />
            <DateTime
              value={expDate}
              inputProps={{
                placeholder: dl.never,
                readOnly: true
              }}
              onChange={this.onExpDatePicked}
              dateFormat='DD-MM-YYYY'
              locale={FlowRouter.getParam('lang') || 'fr'}
              isValidDate={current => {
                return current.isAfter(moment().subtract(1, 'days'))
              }}
              closeOnSelect
              timeFormat
            />
          </div>
        }
      </div>
    )
  }
}
