import React, { Component } from 'react'
import moment from 'moment'
import Blaze from 'meteor/gadicc:blaze-react-component'
import renderHTML from 'react-render-html'
import { withTracker } from 'meteor/react-meteor-data'
import withForumContext from './withForumContext'
import AutoResizeTextArea from 'react-autosize-textarea'

import PollComponent from './poll'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import { Email } from '/common/lang/lang.js'

import '../style/forumfeed.less'

class ForumFeed extends Component {
  constructor (props) {
    super(props)
    this.state = {
      user: [],
      userId: Meteor.userId(),
      files: null,
      nbComments: null,
      nbViews: Object.keys(this.props.item.views).length,
      isToggleOn: false,
      condoId: FlowRouter.getParam('condo_id'),
      newEdit: '',
      like: ''
    }

    this.handleClick = this.handleClick.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.submitAndHide = this.submitAndHide.bind(this)
    this.handleLike = this.handleLike.bind(this)
    this.removePostPoll = this.removePostPoll.bind(this)
  };

  componentWillMount() {
    if (this.props.unfollowdPost === undefined)
      this.setState({ like: true })
    else
      this.setState({ like: false })

    let userPromise = new Promise((resolve, reject) => {
      this.setState({ user: UsersProfile.findOne({ '_id': this.props.item.userId }) })
    })
  }

  handleClick() {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn,
      newEdit: this.props.post.subject
    }))
  }

  handleChange(e) {
    this.setState({ newEdit: e.target.value })
  }

  handleSubmit() {
    let data = {}

    if (data) {
      data['condoId'] = this.state.condoId
      data['postId'] = this.props.item._id
      data['newEdit'] = this.state.newEdit

      Meteor.call('modifPost', data, () => {
        if (result) {
          this.handleClick()
        }
      })
    }
  }

  showDatetime(timestamp) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  }

  handleLike(event) {
    event.preventDefault()
    event.stopPropagation()
    this.setState(prevState => ({
      like: !this.props.following
    }), () => {
      let likeData = {}

      if (likeData) {
        likeData['condoId'] = this.state.condoId
        likeData['postId'] = this.props.item._id
      }

      if (!this.state.like) {
        Meteor.call('unfollowPost', likeData)
      }
      else {
        Meteor.call('followPost', likeData)
      }
    })
  }

  submitAndHide() {
    this.handleSubmit()
    this.handleClick()
  }

  removePostPoll() {
    toRemove = {}

    if (toRemove) {
      toRemove['condoId'] = FlowRouter.getParam('condo_id')
      toRemove['postId'] = this.props.item._id
      Meteor.call('removePostPoll', toRemove)
    }
  }

  showExpireDate({endDate}) {
		if (endDate && !isNaN(endDate)) {
      let timestamp = endDate
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translation = new Email(lang)
      const ts = parseInt(moment(timestamp).format('x'))
      const time_show =  moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
			return `${this.props.dl.expire_on} ${time_show}`
		}
	}

  render() {
    const { context: { categories } } = this.props
    const userId = Meteor.userId()
    const item = this.props.item
    let isNewForum = false
    if (userId && item.updatedAt > this.props.forumStartDate) {
      if (item.participants.includes(userId)) {
        isNewForum = !item.userUpdateView.includes(userId)
      } else {
        isNewForum = !item.userViews.includes(userId)
      }
    }

    const isEdited = !!this.props.item.editedAt

    return (
      <div className='blockPost' id={this.props.item._id} idx={this.props.item._id}>
        {isNewForum && <span className={"message-unread-manager"}></span>}
        <div className='headerPost cursorPointer' onClick={() => { this.props.goToDetailPost(this.props.item._id) }}>
          <div className='headerPostLeft'>
            <div className='headerPostPicture'>
              <Blaze template='userIcon' type='U' userId={this.props.item.userId} />
            </div>
            <div className='headerPostInfos'>
              <div className='headerPostUsername'>
                {this.state.user === undefined ?
                  (<div>{this.props.dl.disabledAccount}</div>) :
                  (<div>{this.state.user.firstname} {this.state.user.lastname}</div>)
                }
              </div>
              <div className='headerPostSendTime'>
                {
                  !!this.props.item.categoriesId && this.props.item.categoriesId.length > 0 ? (
                    _.map(this.props.item.categoriesId, (cat, i) => {
                      const last = this.props.item.categoriesId.length === i + 1
                      const catObj = _.find(categories, (c) => c.id === cat)
                      const catString = !!catObj && !!catObj[FlowRouter.getParam('lang') || 'fr'] ? `${catObj[FlowRouter.getParam('lang') || 'fr']}${last ? '': ', '}`: ''
                      return <span style={{marginRight: '5px'}} key={i}>{catString}{last && <span style={{marginLeft: '5px'}}>•</span>}</span>
                    })
                  ): null
                }
                {!!this.props.item.endDate && this.props.item.endDate <= Date.now() ?
                  // <div>{this.props.dl.expiredPoll} {moment(this.props.item.endDate).locale(FlowRouter.getParam('lang') || 'fr').format('lll').toLowerCase()}</div>
                  <div>{this.props.dl.expiredPoll} {this.showDatetime(this.props.item.endDate)}</div>
                  :
                  <div>{this.props.dl.posted} {this.showDatetime(this.props.item.date)}</div>
                  // <div>{this.props.dl.posted} {moment(this.props.item.date).locale(FlowRouter.getParam('lang') || 'fr').calendar().toLowerCase()}</div>
                }
                {
                  isEdited && <span className="editedTag">({this.props.dl.edited.toLowerCase()})</span>
                }
              </div>
            </div>
          </div>
          <div className='headerPostRight'>
            <div className='starButton' onClick={this.handleLike}>
              {!!this.props.following ? (
                <div className='starButtonOff'></div>
              ) : (
                  <div className='starButtonOn'></div>
                )
              }
            </div>
          </div>
        </div>
        <div className='subjectPost cursorPointer forumTitles' onClick={() => { this.props.goToDetailPost(this.props.item._id) }}>
          {this.props.item.type === 'poll' &&
            <div className='pollSvg'></div>
          }
          <p className='ppp truncated break-line'>
            {reactAutoLink(this.props.item.subject)}
          </p>
        </div>
        {this.state.isToggleOn ? (
          <div className='editComment'>
            <div className='textareaBorder'>
              <AutoResizeTextArea
                rows={3}
                value={this.state.newEdit}
                disabled={this.state.newEdit === this.props.item.comment}
                onChange={this.handleChange}
                placeholder={this.props.dl.changeMyPost}
                className='form-control borderless-textarea'
              />
            </div>
            <div id='submitButton2'>
              <button id='button' className='btn btn-info pull-right' disabled={this.state.newEdit === this.props.item.subject} onClick={this.submitAndHide}>{this.props.dl.postSubmitButton}</button>
            </div>
          </div>
        ) : (null)}
        {!!this.props.hasFile &&
        <div className='fileViewerTemplate'>
          <div className="fileViewerTemplate">
            <MbFilePreview
              files={this.props.files}
              newFiles={this.props.newFiles}
              layout="13"
              limit="4"
              onMore={() => this.props.goToDetailPost(this.props.item._id)}
            />
          </div>
        </div>
        }
        {this.props.item.type === 'poll' ? (
          <div className='glou'>
            <PollComponent item={this.props.item} pollId={this.props.item._id} dl={this.props.dl} />

            <div className='poll-expire-date'>
              {this.showExpireDate(this.props.item)}
            </div>

            <div className='hrStyle'>
              <hr />
            </div>
          </div>
        ) : (null)}
        <div className='interactionBlock' onClick={() => { this.props.goToDetailPost(this.props.item._id) }}>
          <div className='comBlock'>
            <div className='commentsSvg'></div>
            <div className='commentsViews'>{this.props.nbComments} {this.props.dl.comment}</div>
          </div>
          <div className='viewBlock'>
            <div className='viewsSvg'></div>
            <div className='commentsViews'>{this.state.nbViews} {this.props.dl.view}</div>
          </div>
        </div>
      </div>
    )
  };
};

export default ForumFeedContainer = withForumContext(withTracker((props) => {
  let ForumFilesSub = Meteor.subscribe('forumFiles', [FlowRouter.getParam('condo_id')])
  Meteor.subscribe('userLastReadAll')

  let newFiles = []
  let oldFiles = []

  if (props.item && props.item.filesId) {
    newFiles = props.item.filesId.filter(file => file && file.fileId)
    oldFiles = props.item.filesId.filter(file => typeof file === 'string')
  }

  let files = ForumFiles.find({ '_id': { $in: oldFiles } })
  let unFollowSub = Meteor.subscribe('unFollowForumPosts')
  let ForumCommentSub = Meteor.subscribe('forumCommentPosts', FlowRouter.getParam('condo_id'), props.item._id)
  let nbComments = ForumCommentPosts.find({ 'postId': props.item._id }).count()
  let unfollowdPost = UnFollowForumPosts.findOne({ 'postId': props.item._id })
  let isNewPost = !!ForumNewPost.findOne({ '_id': props.item._id })
  deletePoll = Meteor.userHasRight('forum', 'deletePoll')
  deleteSubject = Meteor.userHasRight('forum', 'deleteSubject')

  const readAll = UserLastReadAll.findOne({_id: Meteor.userId()})
  let forumStartDate = 0
  if (readAll && typeof readAll.forum === 'number') {
    forumStartDate = readAll.forum
  }

  return {
    forumFilesSub: ForumFilesSub.ready(),
    unFollowSub: unFollowSub.ready(),
    files: files,
    newFiles,
    hasFile: files.count() + newFiles.length > 0,
    nbComments: nbComments,
    unfollowdPost: unfollowdPost,
    deletePoll: deletePoll,
    deleteSubject: deleteSubject,
    forumStartDate,
    isNewPost
  }
})(ForumFeed))
