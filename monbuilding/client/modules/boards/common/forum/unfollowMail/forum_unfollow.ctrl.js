import { Template } from "meteor/templating";
import { Session } from 'meteor/session'
import { Unfollow, Main } from '/common/lang/lang.js'

var translation;
var forumPost;

Template.forum_unfollow.onCreated(function () {
	translation = {
		unfollow: new Unfollow((FlowRouter.getParam("lang") || "fr")),
		main: new Main((FlowRouter.getParam("lang") || "fr")),
	};

	let postId = FlowRouter.getParam("post_id");
	this.Post = new ReactiveVar({
		title: "",
		description: "",
	});
	let self = this;
	// this.subscribe('ForumPosts', {_id: postId}, function(){  /* CHANGE IT TO A METEOR CALL !!!!!!! */
	// 	self.Post.set(ForumPosts.findOne({}, {fields: {
	// 		title: true,
	// 		description: true,
	// 	}}));
	// });
});

Template.forum_unfollow.onRendered(function () {
});

Template.forum_unfollow.events({
	'click #Unfollow': (event, template) => {
		bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: translation.unfollow.unfollow["noFollowConfirm"],
			buttons : {
				'cancel' : {label: translation.main.main["cancel"], className: "btn-outline-red-confirm"},
				'confirm' : {label: translation.main.main["confirm"], className: "btn-red-confirm"}
			},
			backdrop: true,
			callback: function(result) {
				if (result) {
					Meteor.call('addUnfollower', FlowRouter.getParam("post_id"), Meteor.userId());
					sAlert.success(translation.unfollow.unfollow["wellUnfollow"]);
					setTimeout(function(){
						FlowRouter.go('app.board.resident.home', {lang: FlowRouter.getParam("lang")});
					}, 3000);
				}
			}
		});
	},
});

Template.forum_unfollow.helpers({
	getTitle: () => {
		return Template.instance().Post.get().title;
	},
	getMessage: () => {
		return Template.instance().Post.get().description;
	},
});
