import { Template } from "meteor/templating";

Template.module_forum_feed.onCreated(function() {
});

Template.module_forum_feed.events({
    'click .go-to-forum' (event, template) {
       const params = {
        lang: (FlowRouter.getParam("lang") || "fr"),
        condo_id: FlowRouter.getParam('condo_id'),
        postId: event.currentTarget.getAttribute("idx")
    }
        FlowRouter.go('app.board.resident.condo.module.forum.post', params);
}
});

Template.module_forum_feed.helpers({
    post : () => {
        return Template.currentData().post;
    },
    isNewPost: (post) => {
        let newState = true;
        let exist = _.findWhere(post.views, {userId : Meteor.userId()});
        if (exist !== undefined) {
            if (post.updatedAt <= exist.date) {
                newState = false;
            }
        }

        return newState;
    },
});
