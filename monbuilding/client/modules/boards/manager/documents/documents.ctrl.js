import { FileManager } from "/client/components/fileManager/filemanager.js";
import { ForumLang, CommonTranslation } from '/common/lang/lang'

function createPdfViewer(condoId, type) {
	let condo = Condos.findOne({_id: condoId});
	if (condo && (condo.manual || condo.map)) {
		let file = (type == 0) ? CondoDocuments.findOne({_id: condo.manual.data}) : CondoDocuments.findOne({_id: condo.map.data});
		let url = file.link().replace("localhost", location.hostname);
		PDFJS.workerSrc = '/packages/pascoual_pdfjs/build/pdf.worker.js';

		PDFJS.getDocument(url).then(function renderPages(pdf) {
			for (let currentPage = 1; currentPage <= pdf.numPages; currentPage++) {
				pdf.getPage(currentPage).then(function renderPage(page){
					var scale = 1;
					var viewport = page.getViewport(scale);

					var canvas = document.createElement('canvas');
					var context = canvas.getContext('2d');
					canvas.style.border = '1px solid #000'
					canvas.style.margin = '5px auto';
					canvas.width = viewport.width;
					canvas.height = viewport.height;

					page.render({canvasContext: context, viewport: viewport});
					document.getElementById('pdfCanvas').appendChild(canvas);
				});
			}
		});
	}
}

function callManual(e, t) {
	// t.condoId.set(e.currentTarget.getAttribute("data-condo-id"));
	let action = e.currentTarget.getAttribute("action");
	Session.set('DocumentCondoId', e.currentTarget.getAttribute("data-condo-id"));
	t.currentPage.set("manuel");
	const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");
	const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
	if (action == "modifyText")
		t.isEditing.set(true);
	else if (action == "changePdf") {
		$("#fileUploadInputMan").val(null).click();
		t.isEditing.set(false);
	}
	else if (action == "deleteCurrent") {
		bootbox.confirm({
			title: translation.commonTranslation["delete_manual"],
			message: translation.commonTranslation["sure_delete_manual"],
			buttons: {
				confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
				cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
			},
			callback: function (result) {
				if (result)
					Meteor.call('deleteManual', e.currentTarget.getAttribute("data-condo-id"));
			}
		});
	}
	else {
		Meteor.call('setDocumentHasView', e.currentTarget.getAttribute("data-condo-id"), 'manual');
	}
}

function callMap(e, t) {
  // localStorage.setItem('selectedCondoId', e.currentTarget.getAttribute("data-condo-id"))
	// t.condoId.set(e.currentTarget.getAttribute("data-condo-id"));
	Session.set('DocumentCondoId', e.currentTarget.getAttribute("data-condo-id"));
	t.currentPage.set("map");
	let action = e.currentTarget.getAttribute("action");
	const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");
	const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
	if (action == "changePdf") {
		$("#fileUploadInputMap").click();
		t.isEditing.set(false);
	}
	else if (action == "deleteCurrent") {
		bootbox.confirm({
			title: translation.commonTranslation["delete_modal_title_map"],
			message: translation.commonTranslation["delete_modal_content_map"],
			buttons: {
				confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
				cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
			},
			callback: function (result) {
				if (result)
					Meteor.call('deleteMap', e.currentTarget.getAttribute("data-condo-id"));
			}
		});
	}
	else {
		Meteor.call('setDocumentHasView', e.currentTarget.getAttribute("data-condo-id"), 'map');
	}
	t.currentPage.set("map")
	setTimeout(() => {
		$('#defaultOpen').removeAttr('style')
		$('#defaultOpen').removeAttr('id')
		$('button[data-id="map"]').attr('id', 'defaultOpen')
    // $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--tab-color)')
    $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--primary-color)')
	}, 50);

}

Template.documentsGestionnaire.onCreated(function() {
	this.fm = new FileManager(CondoDocuments);
	this.uploadReady = new ReactiveVar(false);

	this.condoId = new ReactiveVar("all");

	this.isEditing = new ReactiveVar(false);
	this.currentPage = new ReactiveVar("liste");
	Meteor.call('setNewAnalytics', {type: "module", module: "manual", accessType: "web", condoId: ''});
});

Template.documentsGestionnaire.onRendered(function() {
});

Template.documentsGestionnaire.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "manual", accessType: "web", condoId: ''});
})

Template.documentsGestionnaire.events({
	'click .tablink': function(e, t) {
		let dataId = $(e.target).data('id')
		let idTarget = e.target.id

		if (dataId === 'manual') {
			if (idTarget === '') {
				$('#defaultOpen').removeAttr('style')
				$('#defaultOpen').removeAttr('id')
				$('button[data-id="manual"]').attr('id', 'defaultOpen')
        // $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--tab-color)')
        $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--primary-color)')
			}
			t.currentPage.set("manuel")
		} else {
			if (idTarget === '') {
				$('#defaultOpen').removeAttr('style')
				$('#defaultOpen').removeAttr('id')
				$('button[data-id="map"]').attr('id', 'defaultOpen')
        // $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--tab-color)')
        $('#defaultOpen').attr('style', 'border-bottom: 5px solid var(--primary-color)')
			}
			t.currentPage.set("map")
		}
	},
	'change #fileUploadInputMan': function(e, t) {
    $("#drop").button("loading");

		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (e.currentTarget.files && e.currentTarget.files.length == 1) {
			if (e.currentTarget.files[0].type != "application/pdf") {
        sAlert.error(translation.commonTranslation["pdf_only"]);
        $("#drop").button("reset")
      }
			else {
				t.fm.insert(e.currentTarget.files[0], function(err, file) {
					if(!err && file) {
            Meteor.call('setManual', Session.get('DocumentCondoId'), file._id, "file", function(error) {
							$("#drop").button("reset");
						});
						t.fm.clearFiles();
					}
					else {
						$("#drop").button("reset");
						sAlert.error(err.message);
					}
				});
			}
		}
		else {
			$("#drop").button("reset");
		}
	},
	'change #fileUploadInputMap': function(e, t) {
    $("#drop").button("loading");

    let lang = FlowRouter.getParam("lang") || "fr"
    const translation = new CommonTranslation(lang)

		if (e.currentTarget.files && e.currentTarget.files.length == 1) {
      if (e.currentTarget.files[0].type != "application/pdf") {
        sAlert.error(translation.commonTranslation["pdf_only"]);
        $("#drop").button("reset")
      }
      else {
        t.fm.insert(e.currentTarget.files[0], function (err, file) {
          if (!err && file) {
            Meteor.call('setMap', Session.get('DocumentCondoId'), file._id, "file", function (error) {
              $("#drop").button("reset");
            });
            t.fm.clearFiles();
          }
          else
            sAlert.error(err.message);
          $("#drop").button("reset");
        });
      }
		}
	},
	'click #modifyText': function(e, t) {
		t.isEditing.set(true);
	},
	'click #changePdf': function(e, t) {
    $("#fileUploadInputMan").val(null).click();
		t.isEditing.set(false);
	},
	'click #changePdfMap': function(e, t) {
		$("#fileUploadInputMap").click();
	},
	'click #deleteCurrent': function(e, t) {
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");
		const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		bootbox.confirm({
			title: translation.commonTranslation["delete_manual"],
			message: translation.commonTranslation["sure_delete_manual"],
			buttons: {
				confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
				cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
			},
			callback: function (result) {
				if (result)
          Meteor.call('deleteManual', Session.get('DocumentCondoId'));
			}
		});
	},
	'click #deleteCurrentMap': function(e, t) {
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");
		const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		bootbox.confirm({
			title: translation.commonTranslation["delete_modal_title_map"],
			message: translation.commonTranslation["delete_modal_content_map"],
			buttons: {
				confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
				cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
			},
			callback: function (result) {
				if (result)
          Meteor.call('deleteMap', Session.get('DocumentCondoId'));
			}
		});
	},
	'click .manaction': function(e, t) {
		callManual(e, t)
	},
	'click .mapaction': function(e, t) {
		callMap(e, t)
	},
	'click #goBack': function(e, t) {
    const condoId = Session.get('selectedCondo') || "all"
		t.condoId.set(condoId);
		Session.set('DocumentCondoId', condoId);
		t.currentPage.set("liste");
		t.isEditing.set(false);
	}

});

Template.documentsGestionnaire.helpers({
  getOpenedCondo: () => {
    return Condos.findOne({_id: Session.get('DocumentCondoId')})
  },
	isSmallerDevice: () => {
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			return true;
		} else {
			return false;
		}
	},
	manualReady: (condoId) => {
		return (Template.instance().uploadReady.get());
	},
	condos: () => {
		let condoId = Template.instance().condoId.get();
		if (condoId == "all") {
      return Condos.find({_id: {$in: Meteor.listCondoUserHasRight('view', 'documents')}})
    }
		return (Condos.find(condoId));
	},
	getCondoId: () => {
		return Template.instance().condoId.get()
	},
	currentCondo: () => {
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		let condoId = Template.instance().condoId.get();
		if (condoId == "all")
			return translation.commonTranslation["all_buildings"];
		let condo = Condos.findOne(condoId);
		if (condo) {
			if (condo.info.id != -1)
				return condo.info.id + ' - ' + condo.getName();
			else
				return condo.getName();
		}
	},
	hasManual: (id) => {
		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			let condo = Condos.findOne(condoId);
			if (condo && condo.manual) {
				let data = condo.manual.data;
				if (data) {
					return true;
				}
				return false;
			}
		}
		return false;
	},
	hasMap: (id) => {
		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			let condo = Condos.findOne(condoId);
			if (condo && condo.map) {
				let data = condo.map.data;
				if (data) {
					return true;
				}
				return false;
			}
		}
		return false;
	},
	manualFile: (id) => {
		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			createPdfViewer(condoId, 0);
		}
		return true;
	},
	mapFile: (id) => {
		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			createPdfViewer(condoId, 1);
		}
		return true;
	},
	manualData: () => {
    let condoId = Session.get('DocumentCondoId')
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo && condo.manual) {
				if (condo.manual.type == "file") {
					let file = CondoDocuments.findOne({_id: condo.manual.data});
					if (file) {
						return file.link().replace("localhost", location.hostname);
					}
				}
				else
					return condo.manual.data;
			}
		}
		return translation.commonTranslation["data_unknown"];
	},
	mapData: () => {
    let condoId = Session.get('DocumentCondoId')
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo && condo.map) {
				let file = CondoDocuments.findOne({_id: condo.map.data});
				if (file) {
					return file.link().replace("localhost", location.hostname);
				}
			}
		}
		return translation.commonTranslation["data_unknown"];
	},
	manualType: (id) => {
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.type) {
					return condo.manual.type;
				}
			}

		}
		return (id ? translation.commonTranslation["no_manual"] : translation.commonTranslation["type_unknown"]);
	},
	mapType: (id) => {
		let condoId = (id ? id : Session.get('DocumentCondoId'));
		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.map && condo.map.type) {
					return condo.map.type;
				}
			}

		}
		return (id ? translation.commonTranslation["no_plan"] : translation.commonTranslation["type_unknown"]);
	},
	previousManual: () => {
		let condoId = Template.instance().condoId.get();
		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.type == "text" && condo.manual.data) {
					return condo.manual.data;
				}
			}
		}
	},
	getFm: () => {
		return Template.instance().fm;
	},
	getCb: () => {
    let condoId = Session.get('DocumentCondoId')
		let isEditing = Template.instance().isEditing;

		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		return (message, files) => {
			if (message != "<p><br></p>") {
				Meteor.call('setManual', condoId, message, "text");
				sAlert.success(translation.commonTranslation["validation_success"]);
				isEditing.set(false);
			}
			else {
				sAlert.error(translation.commonTranslation["fill_in_text_field"]);
			}
		};
  },
  getCancelCb: () => {
    let isEditing = Template.instance().isEditing

    return () => {
      isEditing.set(false)
    }
  },
	isEditing: () => {
		return Template.instance().isEditing.get();
	},
	lastUpdate: () => {
		let condoId = Template.instance().condoId.get();
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.manualUpdate) {
					return condo.manual.manualUpdate;
				}
			}

		}
		return translation.commonTranslation["data_unknown"];
	},
	lastUpdateMap: () => {
		let condoId = Template.instance().condoId.get();
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.map && condo.map.mapUpdate) {
					return condo.map.mapUpdate;
				}
			}

		}
		return translation.commonTranslation["data_unknown"];
	},
	lastUser: () => {
		let condoId = Template.instance().condoId.get();
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.userId) {
					let profile = Meteor.users.findOne(condo.manual.userId).profile;
					return profile.firstname + " " + profile.lastname;
				}
			}

		}
		return translation.commonTranslation["unknown"];
	},
	lastUserMap: () => {
		let condoId = Template.instance().condoId.get();
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.map && condo.map.userId) {
					let profile = Meteor.users.findOne(condo.map.userId).profile;
					return profile.firstname + " " + profile.lastname;
				}
			}

		}
		return translation.commonTranslation["unknown"];
	},
	getFileList: () => {
		return Template.instance().fm.getFileList();
	},
	currentPage: () => {
		return Template.instance().currentPage.get();
  },
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const t = Template.instance()
    console.log('condoId', condoId)
    if (condoId !== t.condoId.get()) {
      t.condoId.set(condoId)
      // if (condoId == 'all') {
      //   t.currentPage.set('liste')
      // }
    }
    return true

  },
	CDD_cursor: () => {
		return _.uniq([
			...Meteor.listCondoUserHasRight("manual", "see"),
			...Meteor.listCondoUserHasRight("map", "see"),
			...Meteor.listCondoUserHasRight("view", "documents")
		]);
	},
	getRestrictedAccess: () => {
		return ((Template.instance().condoId.get() === 'all') && (Meteor.listCondoUserHasRight("view", "documents").length > 0)) || Meteor.userHasRight("view", "documents")
	}
});
