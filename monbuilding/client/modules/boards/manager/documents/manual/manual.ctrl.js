Template.document_manual.onCreated(function() {
	this.condoId = new ReactiveVar("all");
})


Template.document_manual.helpers({
	hasManual: (id) => {
		let condoId = (id ? id : Template.instance().data.condoId);
		if (condoId != "") {
			let condo = Condos.findOne(condoId);
			if (condo && condo.manual) {
				let data = condo.manual.data;
				if (data) {
					return true;
				}
				return false;
			}
		}
		return false;
	},
	manualType: (id) => {
		let condoId = (id ? id : Template.instance().data.condoId);
		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.type) {
					return condo.manual.type;
				}
			}

		}

		return (id ? "Aucun manuel." : "Type unknown");
	},
	getFm: () => {
		return Template.instance().data.fm
	},
	onSend: () => {
		return Template.instance().data.getCb;
	},
	previousManual: () => {
		let condoId = Template.instance().condoId.get();
		if (condoId != "") {
			let condo = Condos.findOne({_id: condoId});
			if (condo) {
				if (condo.manual && condo.manual.type == "text" && condo.manual.data) {
					return condo.manual.data;
				}
			}
		}
	},
	manualFile: (id) => {
		let condoId = (id ? id : Template.instance().data.condoId);
		if (condoId != "") {
			Template.instance().data.onCreatePdf(condoId, 0)
		}
		return true;
	},
})