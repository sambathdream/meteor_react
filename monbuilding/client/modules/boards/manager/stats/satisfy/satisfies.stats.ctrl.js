import { Chart, CommonTranslation } from 'chart.js';
import 'chart.piecelabel.js';

let myChart;
const howKnowContainer = 'how-know-chart-container';
const howKnowId = 'how-know-chart';

const whyResidenceContainer = 'why-residence-chart-container';
const whyResidenceId = 'why-residence-chart';

const howBookContainer = 'how-book-chart-container';
const howBookId = 'how-book-chart';

const recommendContainer = 'recommend-chart-container';
const recommendId = 'recommend-chart';

function buildDatasetHowKnow(start, end, condo) {
	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.howknow;
			});
		}
  })).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return {
		labels: [
			"Internet",
			"École",
			"Presse",
			"Amis",
			"Signalétique/Passage",
			"Parrainage",
			"Autre"
		],
		datasets: [{
			data: [evaluation.internet, evaluation.school, evaluation.press, evaluation.friends, evaluation.passage, evaluation.sponsorship, evaluation.other],
			backgroundColor: [
				'rgb(228, 102, 107)',
				'rgb(220, 128, 134)',
				'rgb(211, 166, 173)',
				'rgb(220, 220, 221)',
				'rgb(163, 180, 187)',
				'rgb(142, 152, 159)',
				'rgb(184, 210, 218)',
			]
		}]
	}
}

function buildDatasetWhyResidence(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.whyresidence;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return {
		labels: [
			"Près de votre école",							// "nearSchool",
			"Près des transports",							// "nearTransports",
			"Accessibilité",								// "accessibility",
			"Centre ville",									// "downtown",
			"Calme",										// "calm",
			"Des ami(e)s y habitent déjà",					// "friends",
			"Services proposés",							// "services",
			"Confort des logememts",						// "comfort",
			"Entretien de la résidence",					// "upkeep",
			"Réactivité des équipes",						// "reactivity",
			"Qualité de l'accueil et horraires adaptés",	// "qualityReception",
			"Qualité de la gestion",						// "qualityManagement",
			"Architecture et conception de la résidence", 	// "architecture",
			"Autre",										// "other",
		],
		datasets: [{
			data: [
				evaluation.nearSchool,
				evaluation.nearTransports,
				evaluation.accessibility,
				evaluation.downtown,
				evaluation.calm,
				evaluation.friends,
				evaluation.services,
				evaluation.comfort,
				evaluation.upkeep,
				evaluation.reactivity,
				evaluation.qualityReception,
				evaluation.qualityManagement,
				evaluation.architecture,
				evaluation.other
			],
			backgroundColor: [
				'rgb(228, 102, 107)',
				'rgb(220, 128, 134)',
				'rgb(211, 166, 173)',
				'rgb(220, 220, 221)',
				'rgb(163, 180, 187)',
				'rgb(142, 152, 159)',
				'rgb(184, 210, 218)',
				'rgb(197, 232, 241)',
				'rgb(174, 223, 236)',
				'rgb(132, 208, 228)',
				'rgb(200, 90, 90)',
				'rgb(110, 90, 90)',
				'rgb(120, 90, 90)',
				'rgb(130, 90, 90)',
			]
		}]
	}
}

function buildDatasetHowBook(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.howbook;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return {
		labels: ["Sur internet", "Par courrier", "En résidence"],
		datasets: [{
			data: [evaluation.internet, evaluation.direct, evaluation.mail],
			backgroundColor: [
				'rgb(228, 102, 107)',
				'rgb(220, 128, 134)',
				'rgb(211, 166, 173)',
			]
		}]
	}
}

function buildDatasetRecommend(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.recommendation;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));
	return {
		labels: ['oui', 'non'],
		datasets: [{
			data: [evaluation.yes, evaluation.no],
			backgroundColor: ['#8b999f', '#69d2e7']
		}]
	}
}

function buildDataQuality(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.howWasReception;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return evaluation;
}

function buildDataHowInventory(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction, function(eval) {
				return eval.howWasReception;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return evaluation;
}

function buildDataHowInventory(start, end, condo) {

	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let evaluation = _.countBy(_.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(start.format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(end.format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return _.map(archives.satisfaction.haveYouBeenSatisfied, function(eval) {
				return eval.receptionTeam;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined}));

	return evaluation;
}

function renderTooltipHowKnow (tooltipModel) {
	if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
		return ;
	var tooltipEl = document.getElementById('chartjs-satisfy-chart-tooltips');
	if (!tooltipEl) {
		tooltipEl = document.createElement('div');
		tooltipEl.id = 'chartjs-messenger-chart-tooltips';
		tooltipEl.innerHTML = "<table></table>"
		document.body.appendChild(tooltipEl);
	}
	if (tooltipModel.opacity === 0) {
		tooltipEl.style.opacity = 0;
		return;
	}
	tooltipEl.classList.remove('above', 'below', 'no-transform');
	if (tooltipModel.yAlign) {
		tooltipEl.classList.add(tooltipModel.yAlign);
	} else {
		tooltipEl.classList.add('no-transform');
	}
	function getBody(bodyItem) {
		return bodyItem.lines;
	}
	if (tooltipModel.body) {
		var titleLines = tooltipModel.title || [];
		var bodyLines = tooltipModel.body.map(getBody);
		var innerHtml = '<thead>';
		titleLines.forEach(function(title) {
			innerHtml += '<tr><th></th></tr>';
		});
		innerHtml += '</thead><tbody>';
		bodyLines.forEach(function(body, i) {
			if (body[0].split(': ')[1]) {
				let value = body[0].split(': ')[0];
				innerHtml += '<tr><td>' + value + '</td></tr>';
			}
		});
		innerHtml += '</tbody>';
		var tableRoot = tooltipEl.querySelector('table');
		tableRoot.innerHTML = innerHtml;
	}
	// `this` will be the overall tooltip
	var position = this._chart.canvas.getBoundingClientRect();
	tooltipEl.style.opacity = 1;
	tooltipEl.style.left = (tooltipModel.caretX + 115 )+ 'px';
	tooltipEl.style.top = (tooltipModel.caretY + 170) + 'px';
}

function renderTooltipWhyResidence(tooltipModel) {
	if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
		return ;
	var tooltipEl = document.getElementById('chartjs-satisfy-chart-tooltips');
	if (!tooltipEl) {
		tooltipEl = document.createElement('div');
		tooltipEl.id = 'chartjs-messenger-chart-tooltips';
		tooltipEl.innerHTML = "<table></table>"
		document.body.appendChild(tooltipEl);
	}
	if (tooltipModel.opacity === 0) {
		tooltipEl.style.opacity = 0;
		return;
	}
	tooltipEl.classList.remove('above', 'below', 'no-transform');
	if (tooltipModel.yAlign) {
		tooltipEl.classList.add(tooltipModel.yAlign);
	} else {
		tooltipEl.classList.add('no-transform');
	}
	function getBody(bodyItem) {
		return bodyItem.lines;
	}
	if (tooltipModel.body) {
		var titleLines = tooltipModel.title || [];
		var bodyLines = tooltipModel.body.map(getBody);
		var innerHtml = '<thead>';
		titleLines.forEach(function(title) {
			innerHtml += '<tr><th></th></tr>';
		});
		innerHtml += '</thead><tbody>';
		bodyLines.forEach(function(body, i) {
			if (body[0].split(': ')[1]) {
				let value = body[0].split(': ')[0];
				innerHtml += '<tr><td>' + value + '</td></tr>';
			}
		});
		innerHtml += '</tbody>';
		var tableRoot = tooltipEl.querySelector('table');
		tableRoot.innerHTML = innerHtml;
	}
	// `this` will be the overall tooltip
	var position = this._chart.canvas.getBoundingClientRect();
	tooltipEl.style.opacity = 1;
	tooltipEl.style.left = (tooltipModel.caretX + 335 )+ 'px';
	tooltipEl.style.top = (tooltipModel.caretY + 170) + 'px';
}

function renderTooltipHowBook(tooltipModel) {
	if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
		return ;
	var tooltipEl = document.getElementById('chartjs-satisfy-chart-tooltips');
	if (!tooltipEl) {
		tooltipEl = document.createElement('div');
		tooltipEl.id = 'chartjs-messenger-chart-tooltips';
		tooltipEl.innerHTML = "<table></table>"
		document.body.appendChild(tooltipEl);
	}
	if (tooltipModel.opacity === 0) {
		tooltipEl.style.opacity = 0;
		return;
	}
	tooltipEl.classList.remove('above', 'below', 'no-transform');
	if (tooltipModel.yAlign) {
		tooltipEl.classList.add(tooltipModel.yAlign);
	} else {
		tooltipEl.classList.add('no-transform');
	}
	function getBody(bodyItem) {
		return bodyItem.lines;
	}
	if (tooltipModel.body) {
		var titleLines = tooltipModel.title || [];
		var bodyLines = tooltipModel.body.map(getBody);
		var innerHtml = '<thead>';
		titleLines.forEach(function(title) {
			innerHtml += '<tr><th></th></tr>';
		});
		innerHtml += '</thead><tbody>';
		bodyLines.forEach(function(body, i) {
			if (body[0].split(': ')[1]) {
				let value = body[0].split(': ')[0];
				innerHtml += '<tr><td><span>' + value + '</span></td></tr>';
			}
		});
		innerHtml += '</tbody>';
		var tableRoot = tooltipEl.querySelector('table');
		tableRoot.innerHTML = innerHtml;
	}
	// `this` will be the overall tooltip
	var position = this._chart.canvas.getBoundingClientRect();
	tooltipEl.style.opacity = 1;
	tooltipEl.style.left = "calc(" +tooltipModel.caretX + 'px' + " + 41%)";
	tooltipEl.style.top = (tooltipModel.caretY + 190) + 'px';
}

function renderTooltipRecommend (tooltipModel) {
	if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
		return ;
	var tooltipEl = document.getElementById('chartjs-satisfy-chart-tooltips');
	if (!tooltipEl) {
		tooltipEl = document.createElement('div');
		tooltipEl.id = 'chartjs-messenger-chart-tooltips';
		tooltipEl.innerHTML = "<table></table>"
		document.body.appendChild(tooltipEl);
	}
	if (tooltipModel.opacity === 0) {
		tooltipEl.style.opacity = 0;
		return;
	}
	tooltipEl.classList.remove('above', 'below', 'no-transform');
	if (tooltipModel.yAlign) {
		tooltipEl.classList.add(tooltipModel.yAlign);
	} else {
		tooltipEl.classList.add('no-transform');
	}
	function getBody(bodyItem) {
		return bodyItem.lines;
	}
	if (tooltipModel.body) {
		var titleLines = tooltipModel.title || [];
		var bodyLines = tooltipModel.body.map(getBody);
		var innerHtml = '<thead>';
		titleLines.forEach(function(title) {
			innerHtml += '<tr><th></th></tr>';
		});
		innerHtml += '</thead><tbody>';
		bodyLines.forEach(function(body, i) {
			if (body[0].split(': ')[1]) {
				let value = body[0].split(': ')[0];
				innerHtml += '<tr><td>' + value + '</td></tr>';
			}
		});
		innerHtml += '</tbody>';
		var tableRoot = tooltipEl.querySelector('table');
		tableRoot.innerHTML = innerHtml;
	}
	// `this` will be the overall tooltip
	var position = this._chart.canvas.getBoundingClientRect();
	tooltipEl.style.opacity = 1;
	tooltipEl.style.left = (tooltipModel.caretX )+ 'px';
	tooltipEl.style.top = (tooltipModel.caretY) + 'px';
}


function renderChart(start, end, condo) {

	if ($('#'+howKnowContainer).length != 0) {
		$('#'+howKnowContainer + ' .chartjs-size-monitor').remove();
		$('#'+howKnowId).remove();
		$('#'+howKnowContainer).append('<canvas id="'+howKnowId+'" width="50" height="50"><canvas>');

		let data = buildDatasetHowKnow(start, end, condo);
		let isFill = false;

		_.each(data.datasets[0].data, function(dataset) {
			if (dataset != undefined) {
				isFill = true;
			}
		})
		if (!isFill) {
			data.labels = ["Aucune donnée"];
			data.datasets = [{
				backgroundColor: ["#afafaf"],
				data: [1],
			}];
		}

		let ctx = document.getElementById(howKnowId).getContext('2d');
		myChart = new Chart(ctx, {
			type: 'pie',
			data: data,
			options: {
				maintainAspectRatio: true,
				responsive: true,
				rotation: 0.5 * Math.PI,
				plugins: {
					datalabels: {
						enabled: false,
						display: false,
					},
				},
				legend: {
					display: false
				},
				tooltips: {
					bodyFontSize: 12,
					displayColors: false,
				}
			}
		});
	}

	if ($('#'+whyResidenceContainer).length != 0) {
		$('#'+whyResidenceContainer + ' .chartjs-size-monitor').remove();
		$('#'+whyResidenceId).remove();
		$('#'+whyResidenceContainer).append('<canvas id="'+whyResidenceId+'" width="50" height="50"><canvas>');

		data = buildDatasetWhyResidence(start, end, condo);
		isFill = false;

		_.each(data.datasets[0].data, function(dataset) {
			if (dataset != undefined) {
				isFill = true;
			}
		})
		if (!isFill) {
			data.labels = ["Aucune donnée"];
			data.datasets = [{
				backgroundColor: ["#afafaf"],
				data: [1],
			}];
		}

		ctx = document.getElementById(whyResidenceId).getContext('2d');
		myChart = new Chart(ctx, {
			type: 'pie',
			data: data,
			options: {
				maintainAspectRatio: true,
				responsive: true,
				rotation: 0.5 * Math.PI,
				plugins: {
					datalabels: {
						enabled: false,
						display: false,
					},
				},
				legend: {
					display: false
				},
				tooltips: {
					bodyFontSize: 12,
					displayColors: false,
				}
			}
		})
	}

	if ($('#'+howBookContainer).length != 0) {
		$('#'+howBookContainer + ' .chartjs-size-monitor').remove();
		$('#'+howBookId).remove();
		$('#'+howBookContainer).append('<canvas id="'+howBookId+'" width="50" height="50"><canvas>');

		data = buildDatasetHowBook(start, end, condo);
		isFill = false;

		_.each(data.datasets[0].data, function(dataset) {
			if (dataset != undefined) {
				isFill = true;
			}
		})
		if (!isFill) {
			data.labels = ["Aucune donnée"];
			data.datasets = [{
				backgroundColor: ["#afafaf"],
				data: [1],
			}];
		}

		ctx = document.getElementById(howBookId).getContext('2d');
		myChart = new Chart(ctx, {
			type: 'pie',
			data: data,
			options: {
				maintainAspectRatio: true,
				responsive: true,
				rotation: 0.5 * Math.PI,
				plugins: {
					datalabels: {
						enabled: false,
						display: false,
					},
				},
				legend: {
					display: false
				},
				tooltips: {
					bodyFontSize: 12,
					displayColors: false,
				}
			}
		})
	}

	if ($('#'+recommendContainer).length != 0) {
		$('#'+recommendContainer + ' .chartjs-size-monitor').remove();
		$('#'+recommendId).remove();
		$('#'+recommendContainer).append('<canvas id="'+recommendId+'"><canvas>');

		data = buildDatasetRecommend(start, end, condo);
		isFill = false;

		_.each(data.datasets[0].data, function(dataset) {
			if (dataset != undefined) {
				isFill = true;
			}
		})
		let opt = {};
		if (!isFill) {
			data.labels = ["Aucune donnée"];
			data.datasets = [{
				backgroundColor: ["#afafaf"],
				data: [1],
			}];
			opt = {
				rotation: 0.5 * Math.PI,
				plugins: {
					datalabels: {
						enabled: false,
						display: false,
					},
				},
				legend: {
					display: false
				},
				tooltips: {
					enabled: false,
				}
			}
		} else {
			opt = {
				rotation: 0.5 * Math.PI,
				plugins: {
					datalabels: {
						enabled: false,
						display: false,
					},
				},
				legend: {
					display: false
				},
				pieceLabel: {
					render: 'label',
					fontSize: 14,
					fontStyle: 'bold',
					fontColor: '#ffffff',
				},
				tooltips: {
					enabled: false,
				}
			}
		}
		ctx = document.getElementById(recommendId).getContext('2d');
		myChart = new Chart(ctx, {
			type: 'pie',
			data: data,
			options: opt
		});
	}
}


function getTestimoniesOfRecommendation() {
	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let comment = _.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}, {sort: {deletedAt: -1}}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return {
				name: archives.firstname + " " + archives.lastname,
				initial: archives.firstname[0] + archives.lastname[0],
				value: 	_.map(archives.satisfaction, function(eval) {
							if (eval.recommendation[1]) {
								return (eval.recommendation[1]);
							}
						}),
			};
		}
	})).filter(function(comments, index) { return comments != undefined});
	return comment;
}

function getTestimoniesOfIncident(limited) {
	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}


	let comment = _.flatten(_.map(Incidents.find({
		$and: [
			{createdAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
			{createdAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
			{condoId: {$in: condoIds}},
		]
	}).fetch(), function(incident) {
		if (incident && incident.eval && incident.eval.length) {
			return _.map(incident.eval, function(eval) {
				if (eval.comment && eval.comment != "")
					return {...eval, condoId: incident.condoId};
			});
		}
	})).filter(function(comments, index) { return comments != undefined});
	comment.sort(function(a, b) {
		return moment(b.date).diff(a.date, 'seconde');
	});

    Template.instance().incidentReviewCount.set(comment.length);

	return !!limited ? _.first(comment, 2) : comment;
}
function getTestimoniesOfCondo() {
	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let comment = _.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}, {sort: {deletedAt: -1}}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return {
				name: archives.firstname + " " + archives.lastname,
				initial: archives.firstname[0] + archives.lastname[0],
				value: 	_.map(archives.satisfaction, function(eval) {
							return parseInt(eval.globalMarkResidence);
						}),
			};
		}
	})).filter(function(comments, index) { return comments != undefined});
	return comment;
}
function getTestimoniesOfApartment() {
	let condoIds = [];
	if (localStorage.getItem('condoIdSelected') == "all") {
		condoIds = _.map(Condos.find().fetch(), function(condo) {
			return condo._id;
		});
	} else {
		condoIds = [localStorage.getItem('condoIdSelected')];
	}

	let comment = _.flatten(_.map(Archives.find({
		$and: [
			{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
			{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
			{"condos.id": {$in: condoIds}},
		]
	}, {sort: {deletedAt: -1}}).fetch(), function(archives) {
		if (archives && archives.satisfaction &&  archives.satisfaction.length) {
			return {
				name: archives.firstname + " " + archives.lastname,
				initial: archives.firstname[0] + archives.lastname[0],
				value: 	_.map(archives.satisfaction, function(eval) {
							return parseInt(eval.globalMarkApartment);
						}),
			};
		}
	})).filter(function(comments, index) { return comments != undefined});

	return comment;
}

Template.satisfies_stats.onCreated(function() {
	this.satisfyFilter = new ReactiveVar({
		startDate: moment().subtract(29, 'days'),
		endDate: moment()
	});
    this.incidentRateCount = new ReactiveVar(0);
    this.incidentReviewCount = new ReactiveVar(0);
});

let maxHeight = function(elems) {
	return Math.max.apply(null, elems.map(function ()
	{
		return $(this).height();
	}).get());
};

function recalculateHeigh() {
	let elem = $(".equal-height-elem");
	elem.css("height", "");
	let height = maxHeight(elem);
	elem.height(height);

	let elem2 = $(".equal-height-chart");
	elem2.css("height", "");
	let height2 = maxHeight(elem2);
	elem2.height(height2);
}



let myInterval;

Template.satisfies_stats.onRendered(function() {
	let start = moment(Session.get('startDate'), 'DD[/]MM[/]YY');
	let end   = moment(Session.get('endDate'), 'DD[/]MM[/]YY');
	let render = true;
	let template = Template.instance();
	let condoId = Template.currentData().condoId;

	// if (Template.currentData().isReady) {
		myInterval = setInterval(function() {
				if ((!render && (start.valueOf() != moment(Session.get('startDate'), 'DD[/]MM[/]YY') || end.valueOf() != moment(Session.get('endDate'), 'DD[/]MM[/]YY'))) || (!render && (condoId != localStorage.getItem('condoIdSelected')))) {
					start   = moment(Session.get('startDate'), 'DD[/]MM[/]YY');
				end     = moment(Session.get('endDate'), 'DD[/]MM[/]YY');
				condoId = localStorage.getItem('condoIdSelected');
				render  = true;
			}
			if (render) {
        recalculateHeigh();
				renderChart(start.clone(), end.clone(), condoId);
				render = false;
			}
		}, 800);
	// }
	$(window).on('resize', function(){
		recalculateHeigh();
	});
});

Template.satisfies_stats.onDestroyed(function() {
	clearInterval(myInterval);
});

Template.satisfies_stats.events({
	'click .open-bootbox-incident': function(event, template) {
		let comment = getTestimoniesOfIncident();
		let message = "";
		let user = {};
		let initial = "";
		let testimony = "";
		let lang = FlowRouter.getParam("lang") || "fr"
		const translation = new CommonTranslation(lang)

		_.each(comment, function(com) {
			user = GestionnaireUsers.findOne(com.userId);
			initial = user.profile.firstname[0] + user.profile.lastname[0];
			testimony = com.comment;
			message +=	"		<div class=\"testimony-container\">" +
						"			<div class=\"t-head\">" +
						"				<span class=\"userIcon-main\">  " +
						"					<span class=\"userIcon\">"+ initial +"</span>" +
						"				</span>" +
						"				<div class=\"t-star\">" +
						"					<span class=\"fa fa-star "+ (com.value >= 1 ? "color-yellow" : "") +"\" data-rating=\"1\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 2 ? "color-yellow" : "") +"\" data-rating=\"2\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 3 ? "color-yellow" : "") +"\" data-rating=\"3\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 4 ? "color-yellow" : "") +"\" data-rating=\"4\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 5 ? "color-yellow" : "") +"\" data-rating=\"5\"></span>" +
						"				</div>" +
						"			</div>" +
						"			<div class=\"t-body\">" +
						"				<div class=\"t-comment\">" +
						"					<p class=\"mb30\">"+ testimony +"</p>" +
						"				</div>" +
						"			</div>" +
						"		</div>";
		})
		if (message) {
			bootbox.alert({
				size: "medium",
				title: "<h4 class=\"mb30\">" + translation.commonTranslation["incidents_management"] + "</h4>",
				message: message,
				backdrop: true
			});
		}
	},
	'click .open-bootbox-condo': function(event, template) {
		let comment = getTestimoniesOfCondo();
		let message = "";

		_.each(comment, function(com) {
			message +=	"		<div class=\"testimony-container\">" +
						"			<div class=\"t-head\">" +
						"				<span class=\"userIcon-main\">  " +
						"					<span class=\"userIcon\" title="+ com.name +">"+ com.initial +"</span>" +
						"				</span>" +
						"				<div class=\"t-star\">" +
						"					<span class=\"fa fa-star "+ (com.value >= 1 ? "color-yellow" : "") +"\" data-rating=\"1\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 2 ? "color-yellow" : "") +"\" data-rating=\"2\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 3 ? "color-yellow" : "") +"\" data-rating=\"3\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 4 ? "color-yellow" : "") +"\" data-rating=\"4\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 5 ? "color-yellow" : "") +"\" data-rating=\"5\"></span>" +
						"				</div>" +
						"			</div>" +
						"		</div>";
		})
		if (message) {
			bootbox.alert({
				size: "small",
				title: "<h4 class=\"mb30\">" + translation.commonTranslation["household_satisfaction"] + "</h4>",
				message: message,
				backdrop: true
			});
		}
	},
	'click .open-bootbox-apartment': function(event, template) {
		let comment = getTestimoniesOfApartment();
		let message = "";

		_.each(comment, function(com) {
			message +=	"		<div class=\"testimony-container\">" +
						"			<div class=\"t-head\">" +
						"				<span class=\"userIcon-main\">  " +
						"					<span class=\"userIcon\" title="+ com.name +">"+ com.initial +"</span>" +
						"				</span>" +
						"				<div class=\"t-star\">" +
						"					<span class=\"fa fa-star "+ (com.value >= 1 ? "color-yellow" : "") +"\" data-rating=\"1\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 2 ? "color-yellow" : "") +"\" data-rating=\"2\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 3 ? "color-yellow" : "") +"\" data-rating=\"3\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 4 ? "color-yellow" : "") +"\" data-rating=\"4\"></span>" +
						"					<span class=\"fa fa-star "+ (com.value >= 5 ? "color-yellow" : "") +"\" data-rating=\"5\"></span>" +
						"				</div>" +
						"			</div>" +
						"		</div>";
		})
		if (message) {
			bootbox.alert({
				size: "small",
				title: "<h4 class=\"mb30\">" + translation.commonTranslation["appartment_management"] + "</h4>",
				message: message,
				backdrop: true
			});
		}
	},
	'click .open-bootbox-recommend': function(event, template) {
		let comment = getTestimoniesOfRecommendation();
		let message = "";

		_.each(comment, function(com) {
			message +=	"		<div class=\"testimony-container\">" +
						"			<div class=\"t-head\">" +
						"				<span class=\"userIcon-main\">  " +
						"					<span class=\"userIcon\" title=\""+com.name+"\">"+ com.initial +"</span>" +
						"				</span>" +
						"				<span class=\"t-name\">"+com.name+"</span>" +
						"			</div>" +
						"			<div class=\"t-body\">" +
						"				<div class=\"t-comment\">" +
						"					<p class=\"mb30\">"+com.value+"</p>" +
						"				</div>" +
						"			</div>" +
						"		</div>";
		})
		if (message) {
			bootbox.alert({
				size: "medium",
				title: "<h4 class=\"mb30\">" + translation.commonTranslation["recommendation_management"] + "</h4>",
				message: message,
				backdrop: true
			});
		}
	},
	'click #more-incident': function (e, t) {
        Session.set('statsSeeMore', true);
        Session.set('statsSeeMoreIncident', true);
    }
});

Template.satisfies_stats.helpers({
	barWidth : (val) => {
		return '0 0 calc('+(val * 100)+'% - 70px)';
	},
	calculRealPercentage: (value) => {
    // console.log('(value * 100).toFixed(0) ', (value * 100).toFixed(0))
		return (value * 100).toFixed(0);
	},
	getFirstLastName: (userId) => {
		let user = GestionnaireUsers.findOne(userId);
		if (user && user.profile) {
			return user.profile.firstname + ' ' + user.profile.lastname;
		}
	},
	starOfHowWasReception: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.howWasReception == "good")
						return 3;
					if (eval.howWasReception == "normal")
						return 2;
					if (eval.howWasReception == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfHowWasReception"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfHowWasReception"+j).removeClass('active');
			}
		}, 500);
	},
	starOfHowInventory: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.howInventory == "good")
						return 3;
					if (eval.howInventory == "normal")
						return 2;
					if (eval.howInventory == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);

		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfHowInventory"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfHowInventory"+j).removeClass('active');
			}
		}, 500);
	},
	starOfReceptionTeam: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.receptionTeam == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.receptionTeam == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.receptionTeam == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfReceptionTeam"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfReceptionTeam"+j).removeClass('active');
			}
		}, 500);
	},
	starOfCleanliness: () => {
		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.cleanliness == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.cleanliness == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.cleanliness == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfCleanliness"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfCleanliness"+j).removeClass('active');
			}
		}, 500);
	},

	starOfAmbience: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.ambience == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.ambience == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.ambience == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfAmbience"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfAmbience"+j).removeClass('active');
			}
		}, 500);
	},
	starOfBreakfast: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.breakfast == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.breakfast == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.breakfast == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfBreakfast"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfBreakfast"+j).removeClass('active');
			}
		}, 500);
	},
	starOfLaundromat: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.laundromat == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.laundromat == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.laundromat == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfLaundromat"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfLaundromat"+j).removeClass('active');
			}
		}, 500);
	},
	starOfChangeOfLinen: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.changeOfLinen == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.changeOfLinen == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.changeOfLinen == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfChangeOfLinen"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfChangeOfLinen"+j).removeClass('active');
			}
		}, 500);
	},
	starOfSportsHall: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.sportsHall == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.sportsHall == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.sportsHall == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfSportsHall"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfSportsHall"+j).removeClass('active');
			}
		}, 500);
	},
	starOfInternetConnection: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					if (eval.haveYouBeenSatisfied.internetConnection == "good")
						return 3;
					if (eval.haveYouBeenSatisfied.internetConnection == "normal")
						return 2;
					if (eval.haveYouBeenSatisfied.internetConnection == "bad")
						return 1;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$(".starOfInternetConnection"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$(".starOfInternetConnection"+j).removeClass('active');
			}
		}, 500);
	},

	starOfIncident: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Incidents.find({
			$and: [
				{createdAt: {$gte: (parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{createdAt: {$lt: (parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{condoId: {$in: condoIds}}
			]
		}).fetch(), function(incident) {
			if (incident.eval.length) {
				return _.map(incident.eval, function(eval) {
					return eval.value;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		Template.instance().incidentRateCount.set(evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$("#starOfIncident"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$("#starOfIncident"+j).removeClass('active');
			}
		}, 500);
	},
	calculatePercentageOfIncident: (range) => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
    }

    let evaluation = _.flatten(
      _.without(
        // _.map(Incidents.find({condoId: {$in: condoIds}}).fetch(), 
        _.map(Incidents.find({
          	$and: [
          		{createdAt: {$gte: parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x'))}},
          		{createdAt: {$lt: parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x'))}},
          		{condoId: {$in: condoIds}}
          	]
          }).fetch(), 
        function (incident) {
          // return (incident.eval.length) ? incident.eval : null
          return (incident.eval.length) ? incident.eval.map(el => {
            return el.value
          }) : null
        })
      , null, undefined)
    ).filter(function(eval) { return eval != 0 })

		let evaluationOfRange = _.filter(evaluation, function(e) {
			return e == range;
    })
		return (evaluationOfRange.length / evaluation.length) || 0;
	},
	testimoniesOfIncident: (limited) => {
		// let comment = getTestimoniesOfIncident(limited);
		// comment.splice(10);
		// if (comment.length > 10) {
		// 	$('.seeMoreSatisfy.open-bootbox-incident').show();
		// } else {
		// 	$('.seeMoreSatisfy.open-bootbox-incident').hide();
		// }
		return getTestimoniesOfIncident(limited);
	},
	testimoniesOfRecommendation: () => {
		let comment = getTestimoniesOfRecommendation();
		comment.splice(10);
		if (comment.length > 10) {
			$('.seeMoreSatisfy.open-bootbox-recommend').show();
		} else {
			$('.seeMoreSatisfy.open-bootbox-recommend').hide();
		}
		return comment;
	},
	starOfCondo: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					return parseInt(eval.globalMarkResidence);
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$("#starOfCondo"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$("#starOfCondo"+j).removeClass('active');
			}
		}, 500);
	},
	calculatePercentageOfCondo: (range) => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					return parseInt(eval.globalMarkResidence);
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let evaluationOfRange = _.filter(evaluation, function(e) {
			return e == range;
		})
		return (evaluationOfRange.length / evaluation.length) || 0;
	},
	starOfApartment: () => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					return parseInt(eval.globalMarkApartment);
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})
		let average = Math.round(total / evaluation.length);
		setTimeout(function() {
			for (var i = 0; i < average; i++) {
				$("#starOfApartment"+i).addClass('active');
			}
			for (var j = i; j < 5; j++) {
				$("#starOfApartment"+j).removeClass('active');
			}
		}, 500);

	},
	calculatePercentageOfApartment: (range) => {

		let condoIds = [];
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}

		let evaluation = _.flatten(_.map(Archives.find({
			$and: [
				{deletedAt: {$gte: new Date(parseInt(moment(Session.get("startDate"), "DD[/]MM[/]YY").format('x')))}},
				{deletedAt: {$lt: new Date(parseInt(moment(Session.get("endDate"), "DD[/]MM[/]YY").format('x')))}},
				{"condos.id": {$in: condoIds}}
			]
		}).fetch(), function(archives) {
			if (archives && archives.satisfaction &&  archives.satisfaction.length) {
				return _.map(archives.satisfaction, function(eval) {
					return parseInt(eval.globalMarkApartment);
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
		let evaluationOfRange = _.filter(evaluation, function(e) {
			return e == range;
		})
		return (evaluationOfRange.length / evaluation.length) || 0;
	},
	doDisplaySatisfactionStats: () => {
		let condoIds;
		if (localStorage.getItem('condoIdSelected') == "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [localStorage.getItem('condoIdSelected')];
		}
		let archives = Archives.find({
			$and: [
				{"condos.id": {$in: condoIds}},
				{"satisfaction": {$exists: true}},
			]
		}).fetch();

		return archives.length > 0;
	},
    getIncidentRateCount: () => {
        return Template.instance().incidentRateCount.get();
    },
    isIncidentHaveRating: () => {
        return Template.instance().incidentRateCount.get() > 0;
    },
    getIncidentReviewCount: () => {
        return Template.instance().incidentReviewCount.get();
	},
	showIncidentMoreButton: () => {
        return Template.instance().incidentReviewCount.get() > 2;
	},
	isSeeMoreIncident: () => {
		return !!Session.get('statsSeeMoreIncident');
	}
});
