import { Chart } from 'chart.js'
import { Stats, CommonTranslation } from '/common/lang/lang'
import moment from 'moment'

import 'chartjs-plugin-datalabels'

let myChart
const chartContainer = 'booked-by-time-chart-container'
const chartId = 'booked-by-time-chart'

const COLOR = ['#fe0900', '#69d2e7', '#cff1f8', '#fdb813', '#8b999f', '#e0e0e0', '#ffe199', '#5d5e62'];

export function buildDatasetBookedByTime (start, end, condo, timeType, lang, instance = false) {
  const typeFilter = prepareData(condo, instance)
  let dataset = _.map(typeFilter.data, (o, i) => {
    return setDefaultData(o, i)
  })

  let theDataSet = []
  let theLabel = []

  if (timeType === 'date') {
    // DATE TYPE
    const book = Reservations.find({
      $and: [
        {resourceId: {$in: typeFilter.resourceIds}},
        {start: {$gte: +start.locale(lang).format('x')}},
        {start: {$lte: +end.locale(lang).format('x')}},
        condo === 'all' ? {} : {condoId: condo},
        {
          $or: [
            {pending: { $exists: false }},
            {pending: false}
          ]
        },
        {
          $or: [
            {rejected: { $exists: false }},
            {rejected: false}
          ]
        }
      ]
    }).fetch();
    // console.log('book ', book )

    if (end.diff(start, 'year') > 1) {
      const obj = yearChartData(book, start, end, lang, dataset, instance)
      theDataSet = obj.theDataSet
      theLabel = obj.theLabel
    }
    else if (end.diff(start, 'month') > 2) {
      const obj = monthChartData(book, start, end, lang, dataset, instance)
      theDataSet = obj.theDataSet
      theLabel = obj.theLabel
    }
    else if (end.diff(start, 'days') > 31) {
      const obj = weekChartData(book, start, end, lang, dataset, instance)
      theDataSet = obj.theDataSet
      theLabel = obj.theLabel
    }
    else {
      const obj = dayChartData(book, start, end, lang, dataset, instance)
      theDataSet = obj.theDataSet
      theLabel = obj.theLabel
    }
  } else {
    // TIME TYPE
    theLabel = ['0:00', '2:00','4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '24:00' ];
    const utcOffset = Math.floor(moment().utcOffset()/60);

    theDataSet = _.map(dataset, (d) => {
      const book = Reservations.find({
        $and: [
          {resourceId: {$in: d.ids}},
          {start: {$gte: +start.locale(lang).format('x')}},
          {start: {$lte: +end.locale(lang).format('x')}},
          condo === 'all' ? {} : {condoId: condo},
          {
            $or: [
              {pending: { $exists: false }},
              {pending: false}
            ]
          },
          {
            $or: [
              {rejected: { $exists: false }},
              {rejected: false}
            ]
          }
        ]
      }).fetch();
      const counter = _.countBy(_.flatten([ _.map(book, function(book) {
        return moment(book.start).locale(lang).format("h");
      }) ]));

      const newData = _.map(theLabel, (time) => {
        const hour = parseInt(time.split(':')[0], 10);
        let count = !!counter[hour - utcOffset] ? counter[hour - utcOffset] : 0;
        //counter for prev hour
        count += !!counter[hour - utcOffset - 1] ? counter[hour - utcOffset - 1] : 0;
        if (instance && count > 0) {
          instance.isEmpty.set(false);
        }

        return count;
      });

      return {
        ...d,
        data: newData
      }
    })
  }

  if (instance) {
    instance.dataset.set(theDataSet);
    instance.label.set(theLabel);
  } else {
    return {
      datasets: theDataSet,
      labels: theLabel
    }
  }
}

function dayChartData (book, start, end, lang, ds, instance) {
  let dataset = ds
  const label = [];

  let duration = end.diff(start, 'days')
  for (let i = 0; i < duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return _.contains(d.ids, b.resourceId);
      });
      const count = _.countBy(_.flatten([ _.map(resourceBook, function(book) {
        return moment(book.start).locale(lang).format("DD[/]MM[/]YY");
      }) ]))[start.locale(lang).format("DD[/]MM[/]YY")];

      const newData = d.data;
      if (instance && !!count && count > 0) {
        instance.isEmpty.set(false);
      }
      newData.push(!!count ? count : 0);
      return {
        ...d,
        data: newData
      }
    })

    label.push(start.locale(lang).format('ddd DD-MMM'));
    start.add(1, "d");
  }

  return {
    theDataSet: dataset,
    theLabel: label
  }
}

function weekChartData (book, start, end, lang, ds, instance) {
  let dataset = ds
  const label = [];

  const tr_common = new CommonTranslation(lang)

  let duration = end.diff(start, 'weeks')
  for (let i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone();
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return _.contains(d.ids, b.resourceId);
      });
      const count = _.countBy(_.flatten([ _.map(resourceBook, function(book) {
        return moment(book.start).locale(lang).format("W YYYY");
      }) ]))[start.locale(lang).format("W YYYY")];

      const newData = d.data;
      if (instance && !!count && count > 0) {
        instance.isEmpty.set(false);
      }
      newData.push(!!count ? count : 0);
      return {
        ...d,
        data: newData
      }
    })

    label.push(tr_common.commonTranslation["week"] + ' ' + start.locale(lang).format('W') + ' ' + (!(start.startOf('weeks').locale(lang).format('MM[/]YYYY') == tmpDate.add(1, "weeks").locale(lang).format('MM[/]YYYY')) ? (start.startOf('weeks').locale(lang).format('DD MMM')  + ' ' + start.endOf('week').locale(lang).format('DD MMM')) : (start.startOf('weeks').locale(lang).format('DD')+'-'+start.endOf('week').locale(lang).format('DD MMM'))));
    start.add(1, "weeks");
  }

  return {
    theDataSet: dataset,
    theLabel: label
  }
}

function monthChartData (book, start, end, lang, ds, instance) {
  let dataset = ds
  const label = [];

  let duration = end.diff(start, 'month')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return _.contains(d.ids, b.resourceId);
      });
      const count = _.countBy(_.flatten([ _.map(resourceBook, function(book) {
        return moment(book.start).locale(lang).format("MM[/]YYYY");
      }) ]))[start.locale(lang).format("MM[/]YYYY")];

      const newData = d.data;
      if (instance && !!count && count > 0) {
        instance.isEmpty.set(false);
      }
      newData.push(!!count ? count : 0);
      return {
        ...d,
        data: newData
      }
    })

    label.push(!(start.locale(lang).format('YYYY') === moment().locale(lang).format('YYYY')) ? start.locale(lang).format('MMMM YYYY') :  start.locale(lang).format('MMMM'));
    start.add(1, "M");
  }

  return {
    theDataSet: dataset,
    theLabel: label
  }
}

function yearChartData (book, start, end, lang, ds, instance) {
  let dataset = ds
  const label = [];

  let duration = end.diff(start, 'year')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return _.contains(d.ids, b.resourceId);
      });
      const count = _.countBy(_.flatten([ _.map(resourceBook, function(book) {
        return moment(book.start).locale(lang).format("YYYY");
      }) ]))[start.locale(lang).format("YYYY")];

      const newData = d.data;
      if (!!count && count > 0) {
        instance.isEmpty.set(false);
      }
      newData.push(!!count ? count : 0);
      return {
        ...d,
        data: newData
      }
    })

    label.push(start.locale(lang).format("YYYY"));
    start.add(1, "y");
  }

  return {
    theDataSet: dataset,
    theLabel: label
  }
}

function setDefaultData (o, i) {
  return {
    ids: o.ids,
    label: o.name,
    backgroundColor: COLOR[i % 8],
    borderColor: COLOR[i % 8],
    borderWidth: 4,
    fill: false,
    pointRadius: 0,
    lineTension: 0,
    pointHitRadius: 10,
    data: [],
    yAxisID: 'line',
    type: 'line',
    datalabels: {
      display: false
    }
  }
}

function prepareData (condoId, instance = false) {
  const resourcesType = getResourceTypes(condoId, instance);
  const data = _.map(resourcesType.data, (d, i) => {
    return {
      type: i,
      ...d
    }
  })
  if (instance) {
    instance.typeFilter.set(data);
  }

  return {
    data,
    resourceIds: resourcesType.resourceIds
  }
}

function getResourceTypes (condoId) {
  const resources = attachResourceType(Resources.find(condoId === 'all' ? {} : {condoId: condoId}).fetch());
  const data = {};
  const ids = [];

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id]
      }
    } else {
      data[res.type].ids.push(res._id)
    }
    ids.push(res._id)
  });

  return {
    data,
    resourceIds: ids
  };
};

Template.bookedByTime_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
  let lang = FlowRouter.getParam("lang") || "fr"

    this.typeFilter = new ReactiveVar([]);
    this.dataset = new ReactiveVar([]);
    this.label = new ReactiveVar([]);
    this.isEmpty = new ReactiveVar(true);
    this.timeType = new ReactiveVar('time');
    this.resourceIds = new ReactiveVar([]);

  this.filter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
  });
  this.autorun(() => {
    this.filter.set({
      startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
      endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
    })
  });

  this.prepareData = (condoId) => {
    prepareData(condoId, this)
  };

    this.renderChart = (start, end, condo, timeType) => {
		let translation = new Stats((FlowRouter.getParam("lang") || "fr"));
        const ctx = document.getElementById(chartId).getContext('2d');
        const noDataElement = $('#'+chartId+'-no-data');
        const that = this;

        noDataElement.hide();
        this.isEmpty.set(true);

        buildDatasetBookedByTime(start, end, condo, timeType, lang, that);

        if (that.isEmpty.get()) {
            noDataElement.show();
        }
        const theData = {
          labels: that.label.get(),
          datasets: that.dataset.get()
        }

        if (typeof this.data.dataCallback === 'function') {
          this.data.dataCallback('bookedByTime', theData)
        }

        myChart = new Chart(ctx, {
            type: 'line',
            data: theData,
            options: {
                defaultFontColor: "#8b999f",
                responsive: true,
                maintainAspectRatio: false,
                plugins: {
                    datalabels: {
                        enabled: true,
                        display: function(context) {
                            return context.dataset.data[context.dataIndex] > 0;
                        },
                        font: {
                            weight: 'bold'
                        }
                    },
                },
                layout: {
                    padding: {
                        left: 20,
                        right: 0,
                        top: 30,
                        bottom: 0
                    }
                },
                scales: {
                    yAxes: [{
                        id: 'line',
                        gridLines: {
                            display: false,
                            color: "#e0e0e0",
                            lineWidth: 2,
                            zeroLineWidth: 2,
                            zeroLineColor: "#e0e0e0",
                            drawTicks: true,
                            tickMarkLength: 3
                        },
                        ticks: {
                            padding: 10,
                            beginAtZero: true,
                            fontColor: "#8b999f",
                            maxRotation: 0,
                            callback: function(value, index, values) {
                                if (Math.floor(value) === value) {
                                    return value;
                                }
                            }
                        },
                        type: 'linear',
                        position: 'left',
                        scaleLabel: {
                            display: true,
                            labelString: translation.stats["n_resa"],
                            fontColor: '#8b999f'
                        }
                    }],

                    xAxes: [{
                        maxBarThickness: 50,
                        afterSetDimensions: (axis) => {
                            setTimeout(function() {
                                $('#x-axis-arrow-messenger').css("top", (axis.top + 55) + "px");
                                $('#x-axis-arrow-messenger').css("left", (axis.right + 45) + "px");
                                $('#x-axis-arrow-messenger').css("opacity", 1);
                            }, 50);
                        },
                        stacked: true,
                        gridLines: {
                            display: false,
                            color: "#e0e0e0",
                            lineWidth: 2,
                            zeroLineWidth: 2,
                            zeroLineColor: "#e0e0e0",
                            drawTicks: true,
                            tickMarkLength: 3
                        },
                        ticks: {
                            padding: 10,
                            autoSkip: false,
                            fontColor: "#8b999f",
                            maxRotation: 0,
                            callback: function(value, index, values) {
                                let val = value.split(' ');
                                if (values.length > 10) {
                                    if (index % (parseInt(values.length / 10) + 1) == 0) {
                                        if (val.length == 6) {
                                            return [
                                                val[0] + ' ' + val[1],
                                                val[2] + ' ' + val[3],
                                                val[4] + ' ' + val[5]
                                            ];
                                        }
                                        if (val.length == 4) {
                                            return [
                                                val[0] + ' ' + val[1],
                                                val[2] + '-' + val[3]
                                            ];
                                        }
                                        if (val.length == 3) {
                                            return [
                                                val[0].substring(0, val[0].length - 1),
                                                val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                            ];
                                        }
                                        else if (val.length == 2) {
                                            return [
                                                val[0],
                                                val[1]
                                            ]
                                        }
                                        else if (val.length == 1) {
                                            return val[0];
                                        }
                                    }
                                }
                                else {
                                    if (val.length == 6) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + ' ' + val[3],
                                            val[4] + ' ' + val[5]
                                        ];
                                    }
                                    else if (val.length == 4) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + '-' + val[3]
                                        ];
                                    }
                                    else if (val.length == 3) {
                                        return [
                                            val[0].substring(0, val[0].length - 1),
                                            val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                        ];
                                    }
                                    else if (val.length == 2) {
                                        return [
                                            val[0],
                                            val[1]
                                        ]
                                    }
                                    else if (val.length == 1) {
                                        return val[0];
                                    }
                                }
                            }
                        }
                    }]
                },
                tooltips: {
                    enabled: false,
                    custom: function(tooltipModel) {
                        if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
                            return ;
                        // Tooltip Element
                        var tooltipEl = document.getElementById('chartjs-'+chartId+'-tooltips');

                        // Create element on first render
                        if (!tooltipEl) {
                            tooltipEl = document.createElement('div');
                            tooltipEl.id = 'chartjs-'+chartId+'-tooltips';
                            tooltipEl.innerHTML = "<table></table>"
                            document.body.appendChild(tooltipEl);
                        }

                        // Hide if no tooltip
                        if (tooltipModel.opacity === 0) {
                            tooltipEl.style.opacity = 0;
                            return;
                        }

                        // Set caret Position
                        tooltipEl.classList.remove('above', 'below', 'no-transform');
                        if (tooltipModel.yAlign) {
                            tooltipEl.classList.add(tooltipModel.yAlign);
                        } else {
                            tooltipEl.classList.add('no-transform');
                        }

                        function getBody(bodyItem) {
                            return bodyItem.lines;
                        }

                        // Set Text
                        if (tooltipModel.body) {
                            var titleLines = tooltipModel.title || [];
                            var bodyLines = tooltipModel.body.map(getBody);

                            var innerHtml = '<thead>';

                            titleLines.forEach(function(title) {
                                innerHtml += '<tr><th></th></tr>';
                            });
                            innerHtml += '</thead><tbody>';
                            bodyLines.forEach(function(body, i) {
                                if (body[0].split(': ')[1]) {
                                    let value = parseInt(body[0].split(': ')[1]);

                                    var colors = tooltipModel.labelColors[i];
                                    var style = 'background:' + colors.backgroundColor;
                                    style += '; border-color:' + colors.borderColor;
                                    style += '; border-width: 2px';
                                    var div = '';
                                    innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
                                }
                            });
                            innerHtml += '</tbody>';

                            var tableRoot = tooltipEl.querySelector('table');
                            tableRoot.innerHTML = innerHtml;
                        }

                        // `this` will be the overall tooltip
                        var position = this._chart.canvas.getBoundingClientRect();

                        // Display, position, and set styles for font
                        tooltipEl.style.opacity = 1;
                        tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
                        tooltipEl.style.top = (tooltipModel.caretY - 30) + 'px';
                        // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                    }
                },
                legend: false
            }
        });

      this.isLoading.set(false)
        // this.data.onChartRendered('bookedByTime');
    }
});

Template.bookedByTime_stats.events({
    'click .switch-option' (e, t) {
        t.timeType.set($(e.currentTarget).data("type"))
    }
});

Template.bookedByTime_stats.helpers({
    renderChart: () => {
        const filter = Template.instance().filter.get();
        const condo = Template.currentData().condoId;
        const instance = Template.instance();
        const timeType = Template.instance().timeType.get();

        setTimeout(() => {
            $('#'+chartContainer + ' .chartjs-size-monitor').remove();
            $('#'+chartId).remove();
            $('#'+chartContainer).append('<canvas id="'+chartId+'" height="290"><canvas>');
            instance.renderChart(filter.startDate.clone(), filter.endDate.clone(), condo, timeType);
        }, 800);
    },
    prepareData: () => {
        const condoId = Template.currentData().condoId;
        Template.instance().prepareData(condoId);
    },
    getActiveType: (type) => {
        return Template.instance().timeType.get() === type ? 'active' : '';
    },
    ready: () => {
        return !!Template.instance().data.isReady;
    },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
