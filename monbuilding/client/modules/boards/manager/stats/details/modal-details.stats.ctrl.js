import {Pagination} from '/client/components/pagination/pagination.js';
import { ForumLang } from '/common/lang/lang'

Template.details_stats_modal.onCreated(function() {
});

Template.details_stats_modal.onDestroyed(function() {
})

Template.details_stats_modal.onRendered(function() {
});

Template.details_stats_modal.events({
	'hidden.bs.modal #modal_details_stats': function(event, template) {
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_details_stats': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_details_stats";
		window.onhashchange = function() {
			if (location.hash != "#modal_details_stats"){
				modal.modal('hide');
			}
		}
	},
	'change .setApartmentInput': function(event, template) {
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");

		if (parseInt(event.currentTarget.value) == NaN) {
			$(event.currentTarget).addClass('errorModalInput');
			return false;
		}
		else {
			let archive = Archives.findOne({"userId": Template.currentData().userId});
			if (archive && archive != undefined) {
				bootbox.confirm({
					size: "medium",
					title: "Confirmation",
					message: 'Le numéro d\'appartement de '+archive.firstname+' '+archive.lastname+' est le <br>'+parseInt(event.currentTarget.value),
					buttons: {
						confirm: {label: "OK", className: "btn-red-confirm"}
					},
					backdrop: true,
					callback: function(result) {
						if (result) {
							Meteor.call('setApartment', archive._id, parseInt(event.currentTarget.value), event.currentTarget.getAttribute('condoId'));
						}
					}
				});
			}
		}
	},
	'change .movedInDate': function(event, template) {
		let date = moment(event.currentTarget.value, "DD[/]MM[/]YYYY");
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");

		if (date._isValid == false) {
			$(event.currentTarget).addClass('errorModalInput');
			return false;
		}
		else {
			let archive = Archives.findOne({"userId": Template.currentData().userId});
			if (archive && archive != undefined) {
				bootbox.confirm({
					size: "medium",
					title: "Confirmation",
					message: 'La date d\'arrivée de '+archive.firstname+' '+archive.lastname+' est le <br>'+moment(event.currentTarget.value, "DD[/]MM[/]YYYY").format("DD MMM YYYY"),
					buttons: {
						confirm: {label: "OK", className: "btn-red-confirm"},
					},
					backdrop: true,
					callback: function(result) {
						if (result) {
							Meteor.call('updateMovedIn', archive._id, parseInt(date.format('x')));
						}
					}
				});
			}
		}
	},
	'click .movedInLabels':function(event, template) {
		let element = event.currentTarget;
		$(event.currentTarget).remove();
		$('#movedInContainer').append(element);
	},
	'change .deletedAtDate': function(event, template) {
		let date = moment(event.currentTarget.value, "DD[/]MM[/]YYYY");
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");
		
		if (date._isValid == false) {
			$(event.currentTarget).addClass('errorModalInput');
			return false;
		}
		else {
			let archive = Archives.findOne({"userId": Template.currentData().userId});
			if (archive && archive != undefined) {
				bootbox.confirm({
					size: "medium",
					title: "Confirmation",
					message: 'La date de départ de '+archive.firstname+' '+archive.lastname+' est le <br>'+moment(event.currentTarget.value, "DD[/]MM[/]YYYY").format("DD MMM YYYY"),
					buttons: {
						confirm: {label: "OK", className: "btn-red-confirm"},
					},
					backdrop: true,
					callback: function(result) {
						if (result) {
							Meteor.call('updateDeletedAt', archive._id, parseInt(date.format('x')));
						}
					}
				});
			}
		}
	},
});

Template.details_stats_modal.helpers({
	getAllCondo: () => {
		let archive = Archives.findOne({"userId": Template.currentData().userId});
		if (archive && archive != undefined) {
			let condos = Condos.find({"_id": {$in: _.map(archive.condos, function(condo) {
				return condo.id;
			})}}).fetch();
			return condos;
		}
	},
	getCondoName: (condoId) => {
		let condo = Condos.findOne({"_id": condoId});
		if (condo) {
			return condo.getName();
		}
	},
	getCity: (condoId) => {
		let condo = Condos.findOne({"_id": condoId});
		if (condo) {
			return condo.info.code + ' ' + condo.info.city;
		}
	},
	getApartment: (condoId) => {
		let archive = Archives.findOne({"userId": Template.currentData().userId});
		if (archive && archive != undefined) {
			let apartment;
			_.each(archive.condos, function(condo) {
				if (condo.id == condoId) {
					apartment = condo.apartment;
				}
			})
			if (apartment)
				return apartment;
		}
	},
	getMoveIn: () => {
		let archive = Archives.findOne({"userId": Template.currentData().userId});
		if (archive && archive != undefined) {
			return archive.movedInAt;
		}
	},
	getDeletedAt: () => {
		let archive = Archives.findOne({"userId": Template.currentData().userId});
		if (archive && archive != undefined) {
			return archive.deletedAt;
		}
	},
	globalSatisfy: (index) => {
		let archives = Archives.findOne({"userId": Template.currentData().userId});
		if (archives && archives != undefined) {
			return archives.satisfaction[index];
		}
	},
	getHowKnow: (howknow) => {
		if (howknow == "internet")
			return "Internet";
		else if (howknow == "school")
			return "École";
		else if (howknow == "press")
			return "Presse";
		else if (howknow == "friends")
			return "Amis";
		else if (howknow == "passage")
			return "Signalétique/Passage";
		else if (howknow == "sponsorship")
			return "Parrainage";
		else if (howknow == "other")
			return "Autre";
		else 
			return "-";
	},
	getWhyResidence: (whyresidence) => {
		if (whyresidence == "nearSchool")
			return "Près de votre école";							// "nearSchool";
		else if (whyresidence == "nearTransports")
			return "Près des transports";							// "nearTransports";
		else if (whyresidence == "accessibility")
			return "Accessibilité";									// "accessibility";
		else if (whyresidence == "downtown")
			return "Centre ville";									// "downtown";
		else if (whyresidence == "calm")
			return "Calme";											// "calm";
		else if (whyresidence == "friends")
			return "Des ami(e)s y habitent déjà";					// "friends";
		else if (whyresidence == "services")
			return "Services proposés";								// "services";
		else if (whyresidence == "comfort")
			return "Confort des logememts";							// "comfort";
		else if (whyresidence == "upkeep")
			return "Entretien de la résidence";						// "upkeep";
		else if (whyresidence == "reactivity")
			return "Réactivité des équipes";						// "reactivity";
		else if (whyresidence == "qualityReception")
			return "Qualité de l'accueil et horraires adaptés";		// "qualityReception";
		else if (whyresidence == "qualityManagement")
			return "Qualité de la gestion";							// "qualityManagement";
		else if (whyresidence == "architecture")
			return "Architecture et conception de la résidence"; 	// "architecture"; 
		else
			return "Autre";											// "other";
	},
	getHowBook: (howbook) => {
		if (howbook == "internet")
			return "Internet";
		else if (howbook == "mail")
			return "Email";
		else if (howbook == "direct")
			return "Directement auprès de la residence";
		else
			return '-';
	},
	convertYesAndNo: (yesOrNo) => {
		if (yesOrNo[0] == 'yes' || yesOrNo[0] == "Yes,")
			return "Oui" + (yesOrNo[1] ? ', '+yesOrNo[1]: '');
		else 
			return "Non" + (yesOrNo[1] ? ', '+yesOrNo[1]: '');
	},
	getYesOrNo: (yesOrNo) => {
		if (yesOrNo[0] == 'yes' || yesOrNo[0] == "Yes,")
			return "Oui";
		return "Non";
	},
	convertGoodAndBad: (goodOrBad) => {
		if (goodOrBad == 'good')
			return "Bien";
		if (goodOrBad == 'normal')
			return "Moyen";
		return "Mauvais";
	},
	getNbStars: (goodOrBad) => {
		if (goodOrBad == 'good')
			return 1;
		if (goodOrBad == 'normal')
			return 2;
		return 3;
	},
	
});