import {Pagination} from '/client/components/pagination/pagination.js';

Template.details_stats.onCreated(function() {
	this.userId = new ReactiveVar('');
	this.search = new ReactiveVar("");
});

Template.details_stats.onDestroyed(function() {
})

Template.details_stats.onRendered(function() {
});

Template.details_stats.events({
	'click .userLine': function(event, template) {
		$('#modal_details_stats').modal('show');
		template.userId.set(event.currentTarget.getAttribute('userid'));
	},
});

Template.details_stats.helpers({
	getArchive: () => {
		let archives = [];
		let condoIds = [];
		let regexp = new RegExp(Template.currentData().search, "i");
		if (Template.currentData().condoId === "all") {
			condoIds = _.map(Condos.find().fetch(), function(condo) {
				return condo._id;
			});
		} else {
			condoIds = [Template.currentData().condoId];
		}
		if (Template.currentData().isReady) {

			if (Template.currentData().search != undefined) {

				searchCondoIds = _.map(Condos.find({
					$or : [
						{"name": regexp},
						{"info.address": regexp},
						{"info.id": regexp},
						{"info.code": regexp},
						{"info.city": regexp}
					]
					}).fetch(), function(condo) {return condo._id});

				if (searchCondoIds.length != 0) {

					archives = _.map(Archives.find({
						"condos.id": {$in: searchCondoIds},
						"satisfaction": {$exists: true}
					}).fetch(), function(archive) {
						return {
							_id: archive._id,
							userId: archive.userId,
							date: archive.deletedAt,
							firstname: archive.firstname,
							lastname: archive.lastname,
							condos: archive.condos,
							markApartment: _.map(archive.satisfaction, function(eval) {return eval.globalMarkApartment}),
							markResidence: _.map(archive.satisfaction, function(eval) {return eval.globalMarkResidence}),
						};
					})
				} else {
					archives = _.map(Archives.find({
						$and: [{
							$or: [
								{"lastname": regexp},
								{"firstname": regexp},
							]},
							{"condos.id": {$in: condoIds}},
							{"satisfaction": {$exists: true}}
						]
					}).fetch(), function(archive) {
						return {
							_id: archive._id,
							userId: archive.userId,
							date: archive.deletedAt,
							firstname: archive.firstname,
							lastname: archive.lastname,
							condos: archive.condos,
							markApartment: _.map(archive.satisfaction, function(eval) {return eval.globalMarkApartment}),
							markResidence: _.map(archive.satisfaction, function(eval) {return eval.globalMarkResidence}),
						};
					})
				}
			} else {
				archives = _.map(Archives.find({
					"condos.id": {$in: condoIds},
					"satisfaction": {$exists: true}
				}).fetch(), function(archive) {
					return {
						_id: archive._id,
						userId: archive.userId,
						date: archive.deletedAt,
						firstname: archive.firstname,
						lastname: archive.lastname,
						condos: archive.condos,
						markApartment: _.map(archive.satisfaction, function(eval) {return eval.globalMarkApartment}),
						markResidence: _.map(archive.satisfaction, function(eval) {return eval.globalMarkResidence}),
					};
				})
			}
			return archives;
		}
	},
	getCondoName: (condoId) => {
		let condo = Condos.findOne({_id: condoId});
		if (condo) {
			if (condo.info.id != -1)
				return condo.info.id + ' - ' + condo.getName();
			else
				return condo.getName();
		}
	},
	showStars: (eval) => {
		let stars = "";
		_.each(eval, function(star) {
			stars += "<span class=\"fa fa-star "+ (star >= 1 ? "color-yellow" : "") +"\" data-rating=\"1\"></span>" +
					"<span class=\"fa fa-star "+ (star >= 2 ? "color-yellow" : "") +"\" data-rating=\"2\"></span>" +
					"<span class=\"fa fa-star "+ (star >= 3 ? "color-yellow" : "") +"\" data-rating=\"3\"></span>" +
					"<span class=\"fa fa-star "+ (star >= 4 ? "color-yellow" : "") +"\" data-rating=\"4\"></span>" +
					"<span class=\"fa fa-star "+ (star >= 5 ? "color-yellow" : "") +"\" data-rating=\"5\"></span><br>"
		})
		return stars;
	},
	getUserId: () => {
		return Template.instance().userId.get();
	},
});