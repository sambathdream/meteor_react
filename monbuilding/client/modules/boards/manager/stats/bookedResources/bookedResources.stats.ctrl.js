/**
 * Created by kuyarawa on 02/04/18.
 */
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';
import { Stats, CommonTranslation, ContactManagement } from "/common/lang/lang";

const chartContainer = 'booked-resources-chart-container';
const chartId = 'booked-resources-chart';

export function buildDatasetBookedResources (start, end, condoId, lang, instance = false) {

  Meteor.call('getStatsGraphBookedResources', condoId, parseInt(start.locale(lang).format('x')), parseInt(end.locale(lang).format('x')), lang, (error, result) => {
    if (!error) {
      instance.didReceiveDatas.set(true)
      instance.dataset.set(result)

      // check for empty state
      const sum = _.reduce(result, (memo, d) => {
        return memo + parseInt(d.value)
      }, 0);
      if (sum === 0) {
        instance.isEmpty.set(true);
      } else {
        instance.isEmpty.set(false);
      }
    } else {
      sAlert.error(error)
    }
  })
}

Template.bookedResources_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
  this.didReceiveDatas = new ReactiveVar(false)
  this.dataset = new ReactiveVar([]);
  let lang = FlowRouter.getParam("lang") || "fr"
  let condoId = Template.currentData().condoId

  this.filter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
  });
  buildDatasetBookedResources(this.filter.get().startDate, this.filter.get().endDate, condoId, lang, this)
  this.autorun(() => {
    const dateFilter = this.filter.get()
    if (condoId !== Template.currentData().condoId || !dateFilter.startDate.isSame(moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang)) || !dateFilter.endDate.isSame(moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang))) {
      const sessionVar = {
        startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
        endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
      }
      this.filter.set(sessionVar)
      condoId = Template.currentData().condoId
      buildDatasetBookedResources(sessionVar.startDate, sessionVar.endDate, condoId, lang, this)
    }
  });
  this.isEmpty = new ReactiveVar(true);

  this.getResourceTypes = (condoId) => {
    const resources = attachResourceType(Resources.find(condoId === 'all' ? {} : {condoId: condoId}).fetch());
    const data = {}

    _.each(resources, (res) => {
      if (!data[res.type]) {
        data[res.type] = {
          name: res.typeName,
          ids: [res._id]
        }
      } else {
        data[res.type].ids.push(res._id)
      }
    });

    return data;
  };

  this.renderChart = () => {
    const data = this.dataset.get();
    const theData = {
      datasets: [{
        data: _.map(data, (d) => {return d.value}),
        backgroundColor: _.map(data, (d) => {return d.color}),
        hoverBorderColor: _.map(data, (d) => {return d.color}),
        borderColor: "white"
      }],
      labels: _.map(data, (d) => {return d.label}),
    }

    if (typeof this.data.dataCallback === 'function') {
      this.data.dataCallback('bookedResources', theData, true)
    }

    let myChart = new Chart($('#'+chartId), {
      type: 'pie',
      data: theData,
      options: {
        animation: {
          animateScale: true
        },
        maintainAspectRatio: false,
        responsive: true,
        rotation: 0.5 * Math.PI,
        legend: false,
        tooltips: {
          enabled: false,
          custom: function(tooltipModel) {
            if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
              return ;
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-'+chartId+'-tooltips');

            // Create element on first render
            if (!tooltipEl) {
              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-'+chartId+'-tooltips';
              tooltipEl.innerHTML = "<table></table>"
              document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity === 0) {
              tooltipEl.style.opacity = 0;
              return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
              tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
              tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
              return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
              let lang = FlowRouter.getParam("lang") || "fr"
              const translate = new ContactManagement(lang)
              const common = new CommonTranslation(lang)

              var titleLines = tooltipModel.title || [];
              var bodyLines = tooltipModel.body.map(getBody);

              var innerHtml = '<thead>';

              titleLines.forEach(function(title) {
                innerHtml += '<tr><th></th></tr>';
              });
              innerHtml += '</thead><tbody>';
              bodyLines.forEach(function(body, i) {
                if (body[0].split(': ')[1]) {
                  let value = parseInt(body[0].split(': ')[1]);

                  if (parseInt(value) >= 10080) {
                    if (parseInt(value) % 10080 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s)]';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s) ' + parseInt((parseInt(value) % 10080) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
                  }
                  else if (parseInt(value) >= 1440) {
                    if (parseInt(value) % 1440 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s) ' + parseInt((parseInt(value) % 1440) / 60) + 'h]';
                  }
                  else if (parseInt(value) >= 60) {
                    if (parseInt(value) % 60 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
                  }
                  else if (parseInt(value) != 0) {
                    value =  parseInt(value) + 'min';
                  }

                  var colors = tooltipModel.labelColors[i];
                  var style = 'background:' + colors.backgroundColor;
                  style += '; border-color:' + colors.borderColor;
                  style += '; border-width: 2px';
                  var div = '';
                  innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
                }
              });
              innerHtml += '</tbody>';

              var tableRoot = tooltipEl.querySelector('table');
              tableRoot.innerHTML = innerHtml;
            }

            // `this` will be the overall tooltip
            var position = this._chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
            tooltipEl.style.top = (tooltipModel.caretY - 30) + 'px';
            // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
          }
        },
        plugins: {
          datalabels: {
            enabled: false,
            display: false,
          },
        },
      }
    });
    this.isLoading.set(false)
    $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
    // this.data.onChartRendered('bookedResources');
  }
});

Template.bookedResources_stats.onRendered(function() {

});

Template.bookedResources_stats.helpers({
  renderChart: () => {
    const instance = Template.instance();

    let date = instance.filter.get();
    let condoId = Template.currentData().condoId;

    if (Template.instance().didReceiveDatas.get()) {
      setTimeout(() => {
        $('#' + chartContainer + ' .chartjs-size-monitor').remove();
        $('#' + chartId).remove();
        $('#' + chartContainer).append('<canvas id="' + chartId + '" width="266" height="266"><canvas>');
        instance.renderChart();
      }, 800);
    }
  },
  isEmpty: () => {
    return Template.instance().isEmpty.get();
  },
  emptyClass: () => {
    return Template.instance().isEmpty.get() ? 'empty' : '';
  },
  ready: () => {
    return !!Template.instance().didReceiveDatas.get();
  },
  loadingData: () => {
    return !Template.instance().didReceiveDatas.get() && Template.instance().isLoading.get()
  }
});
