/**
 * Created by kuyarawa on 02/04/18.
 */
import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';
import { Stats, CommonTranslation, ContactManagement } from "/common/lang/lang";

const chartContainer = 'resources-use-chart-container';
const chartId = 'resources-use-chart';

const COLOR = ['#fe0900', '#69d2e7', '#cff1f8', '#fdb813', '#8b999f', '#e0e0e0', '#ffe199', '#5d5e62'];

export function buildDatasetResourceUse(start, end, condoId, lang, instance = false) {

  const companies = collectCompany(condoId);
  const resourcesType = getResourceTypes(condoId);
  const { selectedType } = prepareData(condoId, instance)

  let dataset = _.map(companies, (type, i) => {
    const userQuery = collectUserQuery(type._id, condoId)
    const data = Reservations.find({
      $and: [
        selectedType === 'all' ? {} : {resourceId: {$in: resourcesType[selectedType].ids}},
        {start: {$gte: +start.locale(lang).format('x')}},
        {start: {$lte: +end.locale(lang).format('x')}},
        condoId === 'all' ? {} : {condoId: condoId},
        {
          $or: [
            {pending: { $exists: false }},
            {pending: false}
          ]
        },
        {
          $or: [
            {rejected: { $exists: false }},
            {rejected: false}
          ]
        },
        userQuery
      ]
    }).fetch();

    const dataCount = _.reduce(data, (count, d) => {
      return count + moment(d.end).locale(lang).diff(moment(d.start), 'm');
    }, 0);

    const colorIndex = i % 8;
    return {
      label: type.name,
      value: dataCount,
      color: COLOR[colorIndex]
    }
  });

  if (instance) {
    instance.dataset.set(dataset);

    // check for empty state
    const sum = _.reduce(dataset, (memo, d) => {
      return memo + parseInt(d.value)
    }, 0);
    if (sum === 0) {
      instance.isEmpty.set(true);
    } else {
      instance.isEmpty.set(false);
    }
  } else {
    return dataset
  }
}

function collectCompany (condoId) {
  const resident = Residents.find({
    'condos.userInfo.company': {$exists: true},
    ...condoId === 'all' ? {}: {'condos.condoId': condoId}
  }).fetch()

  const companyIds = _.compact(_.uniq(_.flatten(resident.map((r) => {
    return r.condos.map((c) => {
      const status = condoId === 'all' ? true : c.condoId === condoId
      if (status && !!c.userInfo && !!c.userInfo.company) {
        return c.userInfo.company
      }
    })
  }))))

  return CompanyName.find({_id: {$in: companyIds}}).fetch()
}

function getResourceTypes (condoId) {
  const resources = attachResourceType(Resources.find(condoId === 'all' ? {} : {condoId: condoId}).fetch());
  const data = {}

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id]
      }
    } else {
      data[res.type].ids.push(res._id)
    }
  });

  return data;
}

function collectUserQuery (companyId, condoId) {
  const resident = Residents.find({
    'condos.userInfo.company': companyId,
    ...condoId === 'all' ? {}: {'condos.condoId': condoId}
  }).fetch()

  const or = _.flatten(resident.map((r) => {
    const condo = r.condos.map((c) => {
      const status = condoId === 'all' ? true : c.condoId === condoId
      if (status && !!c.userInfo && !!c.userInfo.company && c.userInfo.company === companyId) {
        return {
          $and: [
            {condoId: c.condoId},
            {origin: r.userId}
          ]
        }
      }
    })

    return _.compact(condo)
  }))

  return or.length > 0 ? {
    $or: [...or]
  } : {}
}

function prepareData(condoId, instance = false) {

  const resourcesType = getResourceTypes(condoId, instance);

  if (instance) {
    instance.typeFilter.set(_.size(resourcesType) > 0 ? [
      {
        type: 'all',
        name: 'all',
        ids: []
      },
      ..._.map(resourcesType, (d, i) => {
        return {
          type: i,
          ...d
        }
      })
    ]: [{
      type: 'empty',
      name: 'empty',
      ids: []
    }]);
    if (_.size(resourcesType) > 0) {
      if (instance.selectedType.get() === '') {
        instance.selectedType.set('all')
      }
    } else {
      instance.selectedType.set('empty')
    }

    return {
      selectedType: instance.selectedType.get()
    }
  } else {
    return {
      typeFilter: _.size(resourcesType) > 0 ? [
        {
          type: 'all',
          name: 'all',
          ids: []
        },
        ..._.map(resourcesType, (d, i) => {
          return {
            type: i,
            ...d
          }
        })
      ]: [{
        type: 'empty',
        name: 'empty',
        ids: []
      }],
      selectedType: 'all'
    }
  }
}

Template.resourcesUse_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
  let lang = FlowRouter.getParam("lang") || "fr"
  this.filter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
  });
  this.autorun(() => {
    this.filter.set({
      startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').startOf('d').locale(lang),
      endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').endOf('d').locale(lang),
    })
  });
  this.isEmpty = new ReactiveVar(true);
  this.dataset = new ReactiveVar([]);
  this.selectedType = new ReactiveVar('');
  this.typeFilter = new ReactiveVar([]);

  this.prepareData = (condoId) => {
    prepareData(condoId, this)
  };

  this.prepareDataset = (start, end, condoId) => {
    buildDatasetResourceUse(start, end, condoId, lang, this)
  };

  this.renderChart = () => {
    const data = this.dataset.get();
    const theData = {
      datasets: [{
        data: _.map(data, (d) => {return d.value}),
        backgroundColor: _.map(data, (d) => {return d.color}),
        hoverBorderColor: _.map(data, (d) => {return d.color}),
        borderColor: "white"
      }],
      labels: _.map(data, (d) => {return d.label}),
    }

    if (typeof this.data.dataCallback === 'function') {
      this.data.dataCallback('resourcesUse', theData, true)
    }

    let myChart = new Chart($('#'+chartId), {
      type: 'pie',
      data: theData,
      options: {
        animation: {
          animateScale: true
        },
        maintainAspectRatio: false,
        responsive: true,
        rotation: 0.5 * Math.PI,
        legend: false,
        tooltips: {
          enabled: false,
          custom: function(tooltipModel) {
            if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
              return ;
            // Tooltip Element
            var tooltipEl = document.getElementById('chartjs-'+chartId+'-tooltips');

            // Create element on first render
            if (!tooltipEl) {
              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-'+chartId+'-tooltips';
              tooltipEl.innerHTML = "<table></table>"
              document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity === 0) {
              tooltipEl.style.opacity = 0;
              return;
            }

            // Set caret Position
            tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
              tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
              tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
              return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
              var titleLines = tooltipModel.title || [];
              var bodyLines = tooltipModel.body.map(getBody);

              var innerHtml = '<thead>';

              titleLines.forEach(function(title) {
                innerHtml += '<tr><th></th></tr>';
              });
              innerHtml += '</thead><tbody>';
              bodyLines.forEach(function(body, i) {
                let lang = FlowRouter.getParam("lang") || "fr"
                const translate = new ContactManagement(lang)
                const common = new CommonTranslation(lang)

                if (body[0].split(': ')[1]) {
                  let value = parseInt(body[0].split(': ')[1]);

                  if (parseInt(value) >= 10080) {
                    if (parseInt(value) % 10080 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s)]';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s) ' + parseInt((parseInt(value) % 10080) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
                  }
                  else if (parseInt(value) >= 1440) {
                    if (parseInt(value) % 1440 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s) ' + parseInt((parseInt(value) % 1440) / 60) + 'h]';
                  }
                  else if (parseInt(value) >= 60) {
                    if (parseInt(value) % 60 == 0)
                      value =  parseInt(parseInt(value) / 60) + 'h';
                    else
                      value =  parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
                  }
                  else if (parseInt(value) != 0) {
                    value =  parseInt(value) + 'min';
                  }

                  var colors = tooltipModel.labelColors[i];
                  var style = 'background:' + colors.backgroundColor;
                  style += '; border-color:' + colors.borderColor;
                  style += '; border-width: 2px';
                  var div = '';
                  innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
                }
              });
              innerHtml += '</tbody>';

              var tableRoot = tooltipEl.querySelector('table');
              tableRoot.innerHTML = innerHtml;
            }

            // `this` will be the overall tooltip
            var position = this._chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = 1;
            tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
            tooltipEl.style.top = (tooltipModel.caretY - 30) + 'px';
            // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
          }
        },
        plugins: {
          datalabels: {
            enabled: false,
            display: false,
          },
        },
      }
    });
    this.isLoading.set(false)
    $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
  }
});

Template.resourcesUse_stats.onRendered(function() {

});

Template.resourcesUse_stats.events({
  'click .typeOption' (e, t) {
    t.selectedType.set(e.currentTarget.getAttribute('filter'));
  }
});

Template.resourcesUse_stats.helpers({
  prepareData: () => {
    const instance = Template.instance()
    const date = instance.filter.get();
    const condoId = Template.currentData().condoId;
    instance.prepareData(condoId);
    instance.prepareDataset(date.startDate.clone(), date.endDate.clone(), condoId);
  },
  renderChart: () => {
    const instance = Template.instance();

    let date = instance.filter.get();
    let condoId = Template.currentData().condoId;
    const type = instance.selectedType.get();
    setTimeout(() => {
      $('#'+chartContainer + ' .chartjs-size-monitor').remove();
      $('#'+chartId).remove();
      $('#'+chartContainer).append('<canvas id="'+chartId+'" width="266" height="266"><canvas>');
      instance.renderChart();
    }, 800);
  },
  isEmpty: () => {
    return Template.instance().isEmpty.get();
  },
  emptyClass: () => {
    return Template.instance().isEmpty.get() ? 'empty' : '';
  },
  ready: () => {
    return !!Template.instance().data.isReady;
  },
  getOptions: () => {
    return Template.instance().typeFilter.get()
  },
  getActiveClass: (type) => {
    return Template.instance().selectedType.get() === type ? 'active' : ''
  },
  selectedFilter: () => {
    const options = Template.instance().typeFilter.get();
    if (!!options) {
      const selected = _.find(options, (o) => {
        return o.type === Template.instance().selectedType.get()
      });
      return !!selected ? selected.type : '';
    }
  },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
