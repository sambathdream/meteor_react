import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';
import { CommonTranslation } from "/common/lang/lang.js"

let myChart;
const chartContainer = 'compare-chart-container';
const chartId = 'compare-chart';

export function checkModuleForTypeFilter(lang) {
	let dropdown = [];
	let condoIds = [];
	if (Template.currentData().condoId == 'all') {
		condoIds = GestionnaireCondosInCharge(Meteor.user());
	} else {
		condoIds = [Template.currentData().condoId];
	}
	let modules = _.map(Condos.find({"_id": {$in: condoIds}}).fetch(), function(condo) {
		return condo.settings.options;
	});
	const translation = new CommonTranslation(lang)
	_.each(modules, function(module) {
		if (module["incidents"] == true && !_.find(dropdown, function(d) { return d.name == "incident" }) &&  Meteor.listCondoUserHasRight('incident', 'see').length > 0) {
			dropdown.push({name: "incident", displayName: translation.commonTranslation["manager_incidents"]});
    }
		if (module["informations"] == true && !_.find(dropdown, function(d) { return d.name == "ads" }) &&  Meteor.listCondoUserHasRight('actuality', 'see').length > 0) {
			dropdown.push({name: "informations", displayName: translation.commonTranslation["manager_information"]});
		}
		if (module["incidents"] == true && !_.find(dropdown, function(d) { return d.name == "satisfy" }) &&  Meteor.listCondoUserHasRight('incident', 'see').length > 0) {
			dropdown.push({name: "satisfy", displayName: translation.commonTranslation["satisfaction"]});
		}
		if (module["messenger"] == true && !_.find(dropdown, function(d) { return d.name == "messenger" }) &&  Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise').length > 0) {
			dropdown.push({name: "messenger", displayName: translation.commonTranslation["chat"]});
		}
		if (module["forum"] == true && !_.find(dropdown, function(d) { return d.name == "forum" }) &&  Meteor.listCondoUserHasRight('forum', 'seeSubject').length > 0) {
			dropdown.push({name: "forum", displayName: translation.commonTranslation["manager_forum"]});
		}
		if (module["classifieds"] == true && !_.find(dropdown, function(d) { return d.name == "ads" }) &&  Meteor.listCondoUserHasRight('ads', 'see').length > 0) {
			dropdown.push({name: "ads", displayName: translation.commonTranslation["manager_ads"]});
		}
		if (module["conciergerie"] == true && !_.find(dropdown, function(d) { return d.name == "concierge" }) &&  Meteor.listCondoUserHasRight('view', 'concierge').length > 0) {
			dropdown.push({name: "concierge", displayName: translation.commonTranslation["manager_concierge_services"]});
		}
		if (module["reservations"] == true && !_.find(dropdown, function(d) { return d.name == "resa" }) &&  Meteor.listCondoUserHasRight('reservation', 'seeAll').length > 0) {
			dropdown.push({name: "resa", displayName: translation.commonTranslation["manager_reservation"]});
		}
  })

	return dropdown;
}

function GestionnaireCondosInCharge(gestionnaire) {
	if (gestionnaire) {
    const enterprise = Enterprises.findOne({ _id: gestionnaire.identities.gestionnaireId }, { fields: { users: true } })
    const mapUser = enterprise.users
    const findUser = _.find(mapUser, function (elem) {
      return elem.userId == Meteor.userId()
    })
    const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId)

		if (condoIds) {
			return condoIds;
		}
	}
}

function getCompareIncident(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
  }));

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {
		let count = Incidents.find({
			$and: [
				{condoId: {$in:
					_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
						return c._id;
					})
				}},
				{createdAt: {$gte: new Date(parseInt(start.format('x')))}},
				{createdAt: {$lt: new Date(parseInt(end.format('x')))}},
			]
    }).count();

		cols.push({
			type: cType,
			count: count,
		});
	});

	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
  })
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
  }
	return {cols: cols, total: total};
}

function getCompareInformation(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let count = ActuPosts.find({
			$and: [
				{condoId: {$in:
					_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
						return c._id;
					})
				}},
				{createdAt: {$gte: parseInt(start.format('x'))}},
				{createdAt: {$lt: parseInt(end.format('x'))}},
			]
    }).count();
    // console.log('count ', count )

		cols.push({
			type: cType,
			count: count,
		});
	})


	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareSatisfy(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let evaluation = _.flatten(_.map(Incidents.find({
			$and: [
			{condoId: {$in:
				_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
					return c._id;
				})
			}},
			{createdAt: {$gte: new Date(parseInt(start.format('x')))}},
			{createdAt: {$lt: new Date(parseInt(end.format('x')))}},
			]
		}).fetch(), function(incident) {
			if (incident.eval.length) {
				return _.map(incident.eval, function(eval) {
					return eval.value;
				});
			}
		})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});

		let total = 0;
		_.each(evaluation, function(e) {
			total += parseInt(e);
		})

		cols.push({
			type: cType,
			count: parseInt(total / evaluation.length),
		});
	})

	evaluation = _.flatten(_.map(Incidents.find({
		$and: [
		{createdAt: {$gte: new Date(parseInt(start.format('x')))}},
		{createdAt: {$lt: new Date(parseInt(end.format('x')))}},
		]
	}).fetch(), function(incident) {
		if (incident.eval.length) {
			return _.map(incident.eval, function(eval) {
				return eval.value;
			});
		}
	})).filter(function(eval) { return eval != 0}).filter(function(eval) { return eval != undefined});
	total = 0;
	_.each(evaluation, function(e) {
		total += parseInt(e);
  })
	cols.push({
		type: 'moyenne',
		count: parseInt((total / evaluation.length) / Condos.find().fetch().length)
	});

	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareMessenger(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let conversationIds = _.map(Messages.find({
			"condoId": {$in:
				_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
					return c._id;
				})
			}
		}).fetch(), function(conv) {
			return conv._id;
		})
		let count = MessagesFeed.find({
			$and: [
				{messageId: {$in: conversationIds}},
				{date: {$gte: parseInt(start.format('x'))}},
				{date: {$lt: parseInt(end.format('x'))}},
			]
		}).count();

		cols.push({
			type: cType,
			count: count,
		});
	})


	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareForum(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let condosId = _.flatten(_.pluck(Condos.find({"settings.condoType": cType}).fetch(), '_id')).filter(function(postId) { return postId != undefined});

		let forumPost = ForumPosts.find({ condoId: {$in: condosId}}).fetch();

		let postOfForum = _.filter(forumPost, function(post) {
				return moment(post.data).diff(start) >= 0 && moment(post.data).diff(end) <= 0;
		});

		let replyOfForum = _.flatten(_.map(forumPost, (post) => {
			let forumComments = ForumCommentPosts.find({postId: post._id}).fetch()
			return _.filter(forumComments, function(reply) {
				return moment(reply.commentDate).diff(start) >= 0 && moment(reply.commentDate).diff(end) <= 0;
			})
		}));
		cols.push({
			type: cType,
			count: postOfForum.length + replyOfForum.length,
		});
	})

	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareAds(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let count = ClassifiedsAds.find({
			$and: [
				{condoId: {$in:
					_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
						return c._id;
					})
				}},
				{createdAt: {$gte: parseInt(start.format('x'))}},
				{createdAt: {$lt: parseInt(end.format('x'))}},
			]
		}).count();

		cols.push({
			type: cType,
			count: count,
		});
	})


	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareConcierge(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let count = ConciergeMessage.find({
			$and: [
				{condoId: {$in:
					_.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
						return c._id;
					})
				}},
				{date: {$gte: parseInt(start.format('x'))}},
				{date: {$lt: parseInt(end.format('x'))}},
			]
		}).count();

		cols.push({
			type: cType,
			count: count,
		});
	})


	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

function getCompareResa(compares, start, end, condoIds) {
	let cols = [];
	let total = 0;
	let condoType = [];

	condoType = _.uniq(_.map(Condos.find({"_id": {$in: GestionnaireCondosInCharge(Meteor.user())}}).fetch(), function(condo) {
		return condo.settings.condoType;
	}))

    const resourceIds = _.map(Resources.find(condoIds === 'all' ? {} : {condoId: condoId}).fetch(), (r) => {return r._id});

	_.each(_.without(_.uniq(condoType), undefined, null), function(cType) {

		let count = Reservations.find({
            $and: [
                {resourceId: {$in: resourceIds}},
                {condoId: {$in:
                    _.map(Condos.find({"settings.condoType": cType}).fetch(), function(c) {
                        return c._id;
                    })
                }},
                {type: {$ne: "edl"}},
                {start: {$gte: +start.format('x')}},
                {start: {$lte: +end.format('x')}},
                {
                    $or: [
                        {pending: { $exists: false }},
                        {pending: false}
                    ]
                },
                {
                    $or: [
                        {rejected: { $exists: false }},
                        {rejected: false}
                    ]
                }
            ]
        }).count();

        cols.push({
            type: cType,
            count: count,
        });
	})

	let total_count = 0;
	_.each(cols, function(c) {
		total_count += c.count;
	})
	cols.push({
		type: 'moyenne',
		count: parseInt(total_count / condoType.length)
	})
	if (condoIds != "all") {
		let condoTypeToSearch = Condos.findOne(condoIds).settings.condoType;
		_.each(cols, function(c, i) {
			if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
				cols.splice(i, 1);
		});
	}
	return {cols: cols, total: total};
}

export function buildDatasetCompare(compares, start, end, condoIds) {
	let data = [];
	let label = "";

	let lang = FlowRouter.getParam("lang") || "fr"
	const translation = new CommonTranslation(lang)

	if (compares == "incident") {
		label = translation.commonTranslation["incident_nbr"];
		data = getCompareIncident(compares, start, end, condoIds);
	} else if (compares == "informations") {
		label = translation.commonTranslation["information_nbr"];
		data = getCompareInformation(compares, start, end, condoIds);
	} else if (compares == "satisfy"){
		label = translation.commonTranslation["average_stats"];
		data = getCompareSatisfy(compares, start, end, condoIds);
	} else if (compares == "messenger"){
		label = translation.commonTranslation["message_nbr"];
		data = getCompareMessenger(compares, start, end, condoIds);
	} else if (compares == "forum"){
		label = translation.commonTranslation["post_nbr"];
		data = getCompareForum(compares, start, end, condoIds);
	} else if (compares == "ads"){
		label = translation.commonTranslation["ads_nbr"];
		data = getCompareAds(compares, start, end, condoIds);
	} else if (compares == "concierge"){
		label = translation.commonTranslation["concierge_services_nbr"];
		data = getCompareConcierge(compares, start, end, condoIds);
	} else if (compares == "resa"){
		label = translation.commonTranslation["reservation_nbr"];
		data = getCompareResa(compares, start, end, condoIds);
  }
  // console.log('data ', data )

	let dataset = [{
		label: label,
		backgroundColor: '#69d2e7',
		borderColor: '#69d2e7',
		data: _.map(data.cols, function(col) { return col.count; }),
		datalabels: {
			display: false,
		}
	}];

	dataset.push({
		label: '',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		data: _.map(data.cols, function(col) { return col.count; }),
		type: 'line',
		datalabels: {
			color: '#88898e',
			align: 'end'
		}
	})

	return {
		labels: _.map(data.cols, function(col) {
			if (col.type == "copro")
				return translation.commonTranslation["co_ownership"];
			if (col.type == "mono")
				return translation.commonTranslation["sole_ownership"];
			if (col.type == "etudiante")
				return translation.commonTranslation["student_residence"];
			if (col.type == "office")
				return translation.commonTranslation["office"];
			else
				return translation.commonTranslation["average"]
		}),
		datasets: dataset
	}
}

function renderChartCompare(compares, start, end, condoIds, instance) {
	let ctx = document.getElementById(chartId).getContext('2d');

	$('#compare-chart-no-data').hide();
	let data = buildDatasetCompare(compares, start, end, condoIds);

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('comparison', data)
  }

	let tester = [];
	_.each(data.datasets, function(d) {
		tester.push(_.filter(d.data, function (data_) {
			return data_ != 0;
		}))
	})
	tester = tester.filter(function(test) {return test.length != 0});

	if (tester.length == 0) {
		$('#compare-chart-no-data').show();
	}

    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontColor: "#5d5e62",
            plugins: {
                datalabels: {
                    enabled: true,
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    }
                },
            },
            layout: {
                padding: {
                    left: 20,
                    right: 23,
                    top: 20,
                    bottom: 0
                },
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true,
                        display: true,
                        padding: 10,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    },
                    gridLines: {
                        display: false
                    }
                }, {
                    id: 'littlebar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        max: 50,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }],

                xAxes: [{
                    maxBarThickness: 50,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        padding: 10,
                        autoSkip: false,
                        fontColor: '#8b999f',
                        maxRotation: 0,
                        minRotation: 0
                    }
                }]
            },
            tooltips: {
                enabled: false,
                custom: function(tooltipModel) {
                    if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
                        return ;
                    // Tooltip Element
                    var tooltipEl = document.getElementById('chartjs-'+chartId+'-tooltips');

                    // Create element on first render
                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-'+chartId+'-tooltips';
                        tooltipEl.innerHTML = "<table></table>"
                        document.body.appendChild(tooltipEl);
                    }

                    // Hide if no tooltip
                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    // Set caret Position
                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    // Set Text
                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function(title) {
                            innerHtml += '<tr><th></th></tr>';
                        });
                        innerHtml += '</thead><tbody>';
                        bodyLines.forEach(function(body, i) {
                            if (body[0].split(': ')[1]) {
                                let value = parseInt(body[0].split(': ')[1]);

                                var colors = tooltipModel.labelColors[i];
                                var style = 'background:' + colors.backgroundColor;
                                style += '; border-color:' + colors.borderColor;
                                style += '; border-width: 2px';
                                var div = '';
                                innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
                            }
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    // `this` will be the overall tooltip
                    var position = this._chart.canvas.getBoundingClientRect();

                    // Display, position, and set styles for font
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
                    tooltipEl.style.top = (tooltipModel.caretY) + 'px';
                    // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                }
            },
            legend: false,
        }
    });
  instance.isLoading.set(false)
}

Template.compares_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
	let lang = FlowRouter.getParam("lang") || "fr"

  this.typeFilter = new ReactiveVar(checkModuleForTypeFilter(lang));
  this.compareTypeFilter = new ReactiveVar(this.typeFilter.get()[0].name);
    this.compareDate = new ReactiveVar({
        startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
        endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
    });
    this.autorun(() => {
        this.compareDate.set({
            startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
            endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
        })
    });
});

Template.compares_stats.onRendered(function() {
});

Template.compares_stats.onDestroyed(function() {
})

Template.compares_stats.events({
	'click .compareTypeFilter' (e, t) {
		t.compareTypeFilter.set(e.currentTarget.getAttribute('filter'));
	}
});

Template.compares_stats.helpers({
	renderChartCompare : () => {
	  const instance = Template.instance();
    let typeFilter = instance.compareTypeFilter.get();
    let filter = instance.compareDate.get();
		let condoId = Template.currentData().condoId
    const ready = instance.data.isReady;

    if (ready) {
			setTimeout(() => {
				$('#'+chartContainer + ' .chartjs-size-monitor').remove();
				$('#'+chartId).remove();
				$('#'+chartContainer).append('<canvas id="'+chartId+'" height="340"><canvas>');
				renderChartCompare(typeFilter, filter.startDate.clone(), filter.endDate.clone(), condoId, instance);
			}, 800);
		}
	},
	typeFilters: () => {
		if (Template.instance().typeFilter.get())
			return Template.instance().typeFilter.get();
	},
	compareTypeFilter: () => {
		if (Template.instance().typeFilter.get()) {
			let filter = _.find(Template.instance().typeFilter.get(), (x) => { return x.name === Template.instance().compareTypeFilter.get()})
			// console.log("filter:")
      // console.log(filter)
			if (filter) {
				return filter.displayName
			}
		}
	},
    ready: () => {
        return !!Template.instance().data.isReady;
    },
    getActiveClass: (type) => {
        return Template.instance().compareTypeFilter.get() === type ? 'active' : ''
    },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
