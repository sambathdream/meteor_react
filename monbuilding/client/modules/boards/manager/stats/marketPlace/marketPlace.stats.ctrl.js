import { Chart } from 'chart.js';
import { Stats, CommonTranslation, ContactManagement } from '/common/lang/lang.js'
import moment from 'moment';
import 'chartjs-plugin-datalabels';

const chartContainer = 'marketPlace-chart-container';
const chartId = 'marketPlace-chart';

const backgroundColor = ['#fe0900', '#69d2e7', '#cff1f8', '#fdb813', '#8b999f', '#d9d9d9', '#007aff']

function DatasMarketPlaces (marketPlaces, start, end, state) {
  const lang = FlowRouter.getParam('lang')
  const translation = new CommonTranslation(lang)
  let cols = []
  let total = 0

  let tempCols = {}
  if (marketPlaces && marketPlaces.datas) {
    marketPlaces.datas.forEach(m => {
      const parsedDate = moment(`${m._id.year} ${m._id.month} ${m._id.day}`, 'YYYY MM DD').locale(lang)

      let condition = false
      let name = ''
      if (state === 'year') {
        condition = parsedDate.isSameOrAfter(start.local(lang), 'year') && parsedDate.isSameOrBefore(end.local(lang), 'year')
        name = state === 'year' && m._id.year.toString()
      }
      if (state === 'month') {
        condition = parsedDate.isSameOrAfter(start.local(lang), 'month') && parsedDate.isSameOrBefore(end.local(lang), 'month')
        name = state === 'month' && !(m._id.year == moment().format('YYYY')) ? moment(m._id.month, 'MM').locale(lang).format('MMMM YYYY') : moment(m._id.month, 'MM').locale(lang).format('MMMM')
      }
      if (state === 'weeks') {
        condition = parsedDate.isSameOrAfter(start.local(lang), 'day') && parsedDate.isSameOrBefore(end.local(lang), 'day')
        let tmpDate = parsedDate.startOf('weeks').clone();
        name = translation.commonTranslation["week"] + ' ' +
          parsedDate.format('W') + ' ' +
          (!(parsedDate.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY'))
          ? (parsedDate.startOf('weeks').format('DD MMM') + ' ' +
            parsedDate.endOf('week').format('DD MMM'))
          : (parsedDate.startOf('weeks').format('DD') + '-' +
            parsedDate.endOf('week').format('DD MMM')))
      }
      if (state === 'days') {
        condition = parsedDate.isSameOrAfter(start.local(lang), 'day') && parsedDate.isSameOrBefore(end.local(lang), 'day')
        name = parsedDate.format('ddd DD-MMM')
      }

      if (condition) {
        if (!tempCols[name]) {
          tempCols[name] = {
            name,
            types: {},
            total: 0,
            totalAmount: 0
          }
        }
        tempCols[name].total += m.count
        m.reservations.forEach(r => {
          if (!tempCols[name].types[r.serviceTypeKey]) {
            tempCols[name].types[r.serviceTypeKey] = {
              turnover: 0,
              command_type: 0
            }
          }
          const sum = (parseInt(r.price) || 0) * (parseInt(r.quantity) || 0)
          tempCols[name].types[r.serviceTypeKey].turnover += sum
          tempCols[name].types[r.serviceTypeKey].command_type += 1
          tempCols[name].totalAmount += sum
        })
      }
    })

    let duration = end.diff(start, state)
    for (var i = 0; i <= duration; i++) {
      let month = {}

      let name = ''
      if (state === 'year') {
        name = start.format("YYYY")
      }
      if (state === 'month') {
        name = !(start.format('YYYY') == moment().format('YYYY')) ? start.locale(lang).format('MMMM YYYY') : start.locale(lang).format('MMMM')
      }
      if (state === 'weeks') {
        let tmpDate = start.startOf('weeks').clone();
        name = translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').format('DD MMM')))
      }
      if (state === 'days') {
        name = start.locale(lang).format('ddd DD-MMM')
      }

      if (tempCols[name]) {
        month = tempCols[name]
      } else {
        month = {
          name,
          types: {},
          total: 0,
          totalAmount: 0
        };
      }
      cols.push(month);
      start.add(1, (state === 'year' && 'y') || (state === 'month' && 'M') || (state === 'days' && 'd') || (state === 'weeks' && 'weeks'));
    }
  }

  return { cols: cols, total: total };
}

function getReservationsMP (start, end, condo, lang, marketDatas) {
  let res = {
    cols: [],
    total: 0
  };

  if ((duration = end.diff(start, 'year')) > 1) {
    res = DatasMarketPlaces(marketDatas, start, end, 'year')
  }
  else if ((duration = end.diff(start, 'month')) > 2) {
    res = DatasMarketPlaces(marketDatas, start, end, 'month')
  }
  else if ((duration = end.diff(start, 'days')) > 31) {
    res = DatasMarketPlaces(marketDatas, start, end, 'weeks')
  }
  else {
    res = DatasMarketPlaces(marketDatas, start, end, 'days')
  }
  return res;
}

export function buildDatasetMarketPlace (start, end, condo, type, lang, marketDatas) {
  // TODO change dummy data
  const data = getReservationsMP(start, end, condo, lang, marketDatas);

  let dataset = [];

  const translation = new CommonTranslation(lang)

  let i = 0
  if (marketDatas && marketDatas.servicesType) {
    marketDatas.servicesType.forEach(s => {
      const serviceType = s._id
      const label = translation.commonTranslation[`mp_service_${serviceType}`]
      dataset.push({
        label,
        backgroundColor: backgroundColor[i % 7],
        borderColor: 'white',
        borderWidth: 1,
        data: _.map(data.cols, (col) => col.types[serviceType] ? col.types[serviceType][type] : 0),
        yAxisID: 'bar',
        datalabels: {
          display: false
        }
      });
      i++
    })
  }
  return {
    labels: _.map(data.cols, function (col) { return col.name; }),
    datasets: dataset
  }
}

function renderChartMarketPlace (start, end, condo, type, lang, instance, marketDatas) {

  let ctx = document.getElementById(chartId).getContext('2d');
  $('#marketPlace-chart-no-data').hide();
  let data = buildDatasetMarketPlace(start, end, condo, type, lang, marketDatas);

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('marketPlace', data, type === 'command_type')
  }
  const tr_common = new CommonTranslation(lang)

  let tester = [];
  _.each(data.datasets, function (d) {
    tester.push(_.filter(d.data, function (data_) {
      return data_ != 0;
    }))
  });

  tester = tester.filter(function (test) { return test != 0 });

  if (tester.length === 1 && type !== 'command_type') {
    $('#marketPlace-chart-no-data').show();
  }
  myChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      defaultFontColor: "#8b999f",
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        datalabels: {
          enabled: true,
          display: function (context) {
            return context.dataset.data[context.dataIndex] > 0;
          },
          font: {
            weight: 'bold'
          }
        },
      },
      layout: {
        padding: {
          left: 20,
          right: 0,
          top: 20,
          bottom: 0
        }
      },
      scales: {
        yAxes: [{
          id: 'bar',
          display: false,
          stacked: true,
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
          ticks: {
            beginAtZero: true,
            autoSkip: false,
          },
          type: 'linear',
          position: 'right',
        }, {
          id: 'line',
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
          ticks: {
            display: false
          },
          type: 'linear',
          position: 'left',
        }, {
          id: 'littlebar',
          display: false,
          stacked: true,
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
          ticks: {
            beginAtZero: true,
            max: 50,
            autoSkip: false,
          },
          type: 'linear',
          position: 'right',
        }],

        xAxes: [{
          categorySpacing: 16,
          afterSetDimensions: (axis) => {
            setTimeout(function () {
              $('#x-axis-arrow-marketPlace').css("top", (axis.top + 55) + "px");
              $('#x-axis-arrow-marketPlace').css("left", (axis.right + 45) + "px");
              $('#x-axis-arrow-marketPlace').css("opacity", 1);
            }, 50);
          },
          stacked: true,
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
        }]
      },
      tooltips: {
        enabled: false,
        custom: function (tooltipModel) {
          if (!!(tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
            return;
          // Tooltip Element
          var tooltipEl = document.getElementById('chartjs-marketPlace-chart-tooltips');

          // Create element on first render
          if (!tooltipEl) {
            tooltipEl = document.createElement('div');
            tooltipEl.id = 'chartjs-marketPlace-chart-tooltips';
            tooltipEl.innerHTML = "<table></table>"
            document.body.appendChild(tooltipEl);
          }

          // Hide if no tooltip
          if (tooltipModel.opacity === 0) {
            tooltipEl.style.opacity = 0;
            return;
          }

          // Set caret Position
          tooltipEl.classList.remove('above', 'below', 'no-transform');
          if (tooltipModel.yAlign) {
            tooltipEl.classList.add(tooltipModel.yAlign);
          } else {
            tooltipEl.classList.add('no-transform');
          }

          function getBody (bodyItem) {
            return bodyItem.lines;
          }

          // Set Text
          if (tooltipModel.body) {
            var titleLines = tooltipModel.title || [];
            var bodyLines = tooltipModel.body.map(getBody);

            var innerHtml = '<thead>';

            titleLines.forEach(function (title) {
              innerHtml += '<tr><th></th></tr>';
            });
            innerHtml += '</thead><tbody>';
            bodyLines.forEach(function (body, i) {
              if (body[0].split(': ')[1]) {
                let value = parseInt(body[0].split(': ')[1]);

                var colors = tooltipModel.labelColors[i];
                var style = 'background:' + colors.backgroundColor;
                style += '; border-color:' + colors.borderColor;
                style += '; border-width: 2px';
                var div = '';
                innerHtml += '<tr style="border-top: ' + colors.backgroundColor + ' solid 2px"><td style="padding: ' + tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px' + '">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
              }
            });
            innerHtml += '</tbody>';

            var tableRoot = tooltipEl.querySelector('table');
            tableRoot.innerHTML = innerHtml;
          }

          // `this` will be the overall tooltip
          var position = this._chart.canvas.getBoundingClientRect();

          // Display, position, and set styles for font
          tooltipEl.style.opacity = 1;
          tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
          tooltipEl.style.top = (tooltipModel.caretY - 30) + 'px';
          // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
        }
      },
      legend: false
    }
  });

  instance.isLoading.set(false)
   $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
}

function updateDatas (instance, condoId, start, end) {
  Meteor.call('getStatsGraphMarketPlace', condoId, parseInt(start.format('x')), parseInt(end.format('x')), (error, result) => {
    if (!error) {
      instance.didReceiveDatas.set(true)
      instance.marketDatas.set(result)
    } else {
      sAlert.error(error)
    }
  })
}

Template.marketPlace_stats.onCreated(function () {
  this.isLoading = new ReactiveVar(true)
  this.didReceiveDatas = new ReactiveVar(false)
  let lang = FlowRouter.getParam("lang") || "fr"
  let translation = { stats: new Stats(lang) }
  this.marketDatas = new ReactiveVar(null)
  this.marketPlaceFilter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
  });
  let i = 0
  let condoId = Template.currentData().condoId
  updateDatas(this, condoId, this.marketPlaceFilter.get().startDate, this.marketPlaceFilter.get().endDate)
  this.autorun(() => {
    const dateFilter = this.marketPlaceFilter.get()
    if (condoId !== Template.currentData().condoId || !dateFilter.startDate.isSame(moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang)) || !dateFilter.endDate.isSame(moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang))) {
      const sessionVar = {
        startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
        endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
      }
      this.marketPlaceFilter.set(sessionVar)
      condoId = Template.currentData().condoId
      updateDatas(this, condoId, sessionVar.startDate, sessionVar.endDate)
    }
  });
  this.selectedType = new ReactiveVar('turnover');
  this.options = [
    {
      type: 'turnover',
      name: translation.stats.stats["turnover"]
    }, {
      type: 'command_type',
      name: translation.stats.stats["command_type"]
    }
  ]
});

Template.marketPlace_stats.onRendered(function () {
});

Template.marketPlace_stats.events({
  'click .typeOption' (e, t) {
    t.selectedType.set(e.currentTarget.getAttribute('filter'));
  }
});

Template.marketPlace_stats.onDestroyed(function () {
});

Template.marketPlace_stats.helpers({
  renderChartMarketPlace: () => {
    const instance = Template.instance()
    const filter = instance.marketPlaceFilter.get();
    const condo = Template.currentData().condoId
    const type = instance.selectedType.get();
    let lang = FlowRouter.getParam("lang") || "fr"
    const marketDatas = Template.instance().marketDatas.get()
    if (Template.instance().didReceiveDatas.get()) {
      setTimeout(() => {
        $('#' + chartContainer + ' .chartjs-size-monitor').remove();
        $('#' + chartId).remove();
        $('#' + chartContainer).append('<canvas id="' + chartId + '" height="340"><canvas>');
        renderChartMarketPlace(filter.startDate.clone(), filter.endDate.clone(), condo, type, lang, instance, marketDatas);
      }, 800);
    }
  },
  getOptions: () => {
    return Template.instance().options
  },
  selectedFilter: () => {
    const instance = Template.instance();
    return _.find(instance.options, (o) => { return o.type === instance.selectedType.get() }).name;
  },
  ready: () => {
    return !!Template.instance().didReceiveDatas.get();
  },
  getActiveClass: (type) => {
    return Template.instance().selectedType.get() === type ? 'active' : ''
  },
  loadingData: () => {
    return !Template.instance().didReceiveDatas.get() && Template.instance().isLoading.get()
  }
});
