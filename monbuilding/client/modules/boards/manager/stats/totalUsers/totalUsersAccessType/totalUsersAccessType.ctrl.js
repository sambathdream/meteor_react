import { Chart } from 'chart.js';
import { CommonTranslation } from '/common/lang/lang.js'
import 'chartjs-plugin-datalabels';

let myChart;
const chartContainer = 'total-users-access-type-chart-container';
const chartId = 'total-users-access-type-chart';

function GestionnaireCondosInCharge(gestionnaire) {
  if (gestionnaire) {
    const enterprise = Enterprises.findOne({ _id: gestionnaire.identities.gestionnaireId }, { fields: { users: true } })
    const mapUser = enterprise.users
    const findUser = _.find(mapUser, function (elem) {
      return elem.userId == Meteor.userId()
    })
    const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId)

    if (condoIds) {
      return condoIds;
    }
  }
}

function getUsersByDay(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('DD[/]MM[/]YY')} && moment n: ${moment(u.loginDate).format('DD[/]MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('DD[/]MM[/]YY') == moment(u.loginDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByWeek(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('W YYYY')} && moment n: ${moment(u.loginDate).format('W YYYY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('W YYYY') == moment(u.loginDate).format('W YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByMonth(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('MM[/]YY')} && moment n: ${moment(u.loginDate).format('MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('MM[/]YY') == moment(u.loginDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByYear(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('YYYY')} && moment n: ${moment(u.loginDate).format('YYYY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('YYYY') == moment(u.loginDate).format('YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function DayTotalUsersAccessType(start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByDay(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByDay(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByDay(collection.others)
  // console.log("mobile users: ", otherUsers)

  const duration = end.diff(start, 'days');
	for (var i = 0; i < duration; i++) {
		let tmpCol = {
			types: [],
			name: start.locale(lang).format('ddd DD-MMM'),
			total: 0
    }

    let webOfDay = _.filter(webUsers, (a) => {
			return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		})
    let mobileOfDay = _.filter(mobileUsers, (a) => {
			return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		})
		// let iosOfDay = _.filter(iosUsers, (a) => {
		// 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		// })
		// let androidOfDay = _.filter(androidUsers, (a) => {
		// 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		// })
		// let othersOfDay = _.filter(othersUsers, (a) => {
		// 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		// })
		tmpCol.types.push({value: webOfDay.length, category: "web"})
		tmpCol.types.push({value: mobileOfDay.length, category: "mobile"})
		// tmpCol.types.push({value: iosOfDay.length, category: "ios"})
		// tmpCol.types.push({value: androidOfDay.length, category: "android"})
		// tmpCol.types.push({value: othersOfDay.length, category: "others"})
		tmpCol.total = (webOfDay.length + mobileOfDay.length)
		cols.push(tmpCol)
		total += (webOfDay.length + mobileOfDay.length)
		start.add(1, 'd')
	}
	return {cols: cols, total: total}
}

function WeekTotalUsersAccessType(start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = []
	let total = 0
  const translation = new CommonTranslation(lang)

  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByWeek(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByWeek(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByWeek(collection.others)
  // console.log("mobile users: ", otherUsers)

	const duration = end.diff(start, 'weeks');
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
    let tmpCol = {
			types: [],
			name: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
			total: 0
		};

    let webOfWeek = _.filter(webUsers, (a) => {
			return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
		});
    let mobileOfWeek = _.filter(mobileUsers, (a) => {
			return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
		});
		// let iosOfWeek = _.filter(iosUsers, (a) => {
		// 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
		// })
		// let androidOfWeek = _.filter(androidUsers, (a) => {
		// 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
		// })
		// let othersOfWeek = _.filter(othersUsers, (a) => {
		// 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
		// })
		tmpCol.types.push({value: webOfWeek.length, category: "web"});
		tmpCol.types.push({value: mobileOfWeek.length, category: "mobile"});
		// tmpCol.types.push({value: iosOfWeek.length, category: "ios"})
		// tmpCol.types.push({value: androidOfWeek.length, category: "android"})
		// tmpCol.types.push({value: othersOfWeek.length, category: "others"})
		tmpCol.total = (webOfWeek.length + mobileOfWeek.length);
		cols.push(tmpCol);
		total += (webOfWeek.length + mobileOfWeek.length);
		start.add(1, 'weeks');
	}
	return {cols: cols, total: total};
}

function MonthTotalUsersAccessType(start, end, collection) {
	let cols = [];
	let total = 0;
  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByMonth(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByMonth(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByMonth(collection.others)
  // console.log("mobile users: ", otherUsers)

  const duration = end.diff(start, 'month');
	for (var i = 0; i <= duration; i++) {
		let tmpCol = {
			types: [],
			name: !(start.format('YYYY') == moment().format('YYYY')) ? start.format('MMMM YYYY') :  start.format('MMMM'),
			total: 0
		};

    let webOfMonth = _.filter(webUsers, (a) => {
			return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
		});
    let mobileOfMonth = _.filter(mobileUsers, (a) => {
			return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
		});
		// let iosOfMonth = _.filter(iosUsers, (a) => {
		// 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
		// })
		// let androidOfMonth = _.filter(androidUsers, (a) => {
		// 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
		// })
		// let othersOfMonth = _.filter(othersUsers, (a) => {
		// 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
		// })
		tmpCol.types.push({value: webOfMonth.length, category: "web"});
		tmpCol.types.push({value: mobileOfMonth.length, category: "mobile"})
		// tmpCol.types.push({value: iosOfMonth.length, category: "ios"})
		// tmpCol.types.push({value: androidOfMonth.length, category: "android"})
		// tmpCol.types.push({value: othersOfMonth.length, category: "others"})
		tmpCol.total = (webOfMonth.length + mobileOfMonth.length);
		cols.push(tmpCol);
		total += (webOfMonth.length + mobileOfMonth.length);
		start.add(1, 'M');
	}
	return {cols: cols, total: total};
}

function YearTotalUsersAccessType(start, end, collection) {
	let cols = [];
	let total = 0;
  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByYear(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByYear(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByYear(collection.others)
  // console.log("mobile users: ", otherUsers)

	const duration = end.diff(start, 'year');
	for (var i = 0; i <= duration; i++) {
		let tmpCol = {
			types: [],
			name: start.format('YYYY'),
			total: 0
		};

    let webOfYear = _.filter(webUsers, (a) => {
			return moment(a.loginDate).format('YYYY') == start.format('YYYY');
		});
    let mobileOfYear = _.filter(mobileUsers, (a) => {
			return moment(a.loginDate).format('YYYY') == start.format('YYYY');
		});
		// let iosOfYear = _.filter(iosUsers, (a) => {
		// 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
		// })
		// let androidOfYear = _.filter(androidUsers, (a) => {
		// 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
		// })
		// let othersOfYear = _.filter(othersUsers, (a) => {
		// 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
		// })
		tmpCol.types.push({value: webOfYear.length, category: "web"});
		tmpCol.types.push({value: mobileOfYear.length, category: "mobile"});
		// tmpCol.types.push({value: iosOfYear.length, category: "ios"})
		// tmpCol.types.push({value: androidOfYear.length, category: "android"})
		// tmpCol.types.push({value: othersOfYear.length, category: "others"})
		tmpCol.total = (webOfYear.length + mobileOfYear.length);
		cols.push(tmpCol);
		total += (webOfYear.length + mobileOfYear.length);
		start.add(1, 'y');
	}
	return {cols: cols, total: total};
}

function getTotalUsersAccessType(start, end, condoId) {
  let res;

  const condos = condoId === 'all' ? GestionnaireCondosInCharge(Meteor.user()) : [condoId]

  const usersId = _.map(GestionnaireUsers.find({
    '_id': { $ne: Meteor.userId() },
    'resident.condos.condoId': { $in: condos }
  }, { fields: { userId: true } }).fetch(), u => u._id)
  // console.log('usersId', usersId)

  // console.log('condos ', condos )

  // Retrieve web users
  // console.log('+start.format(\'x\')', +start.format('x'))
  // console.log('+end.format(\'x\')', +end.format('x'))

  const web = Analytics.find({
    $and: [
      {loginDate: {$gte: +start.format('x')}},
      {loginDate: {$lte: +end.format('x')}},
      {userId: { $in: usersId }},
      {condoId:{ $in: condos }},
      {accessType: "web"},
      {type: "login"}
    ]
  }).fetch()

  // Retrieve Mobile users
  const mobile = Analytics.find({
    $and: [
      {loginDate: {$gte: +start.format('x')}},
      {loginDate: {$lte: +end.format('x')}},
      {userId: { $in: usersId }},
      {
        $or: [{
          accessType: "ios"
        }, {
          accessType: "android"
        }]
      },
      {type: "login"}
    ]
  }).fetch()

  // Retrieve IOS users
  // const ios = Analytics.find({
  //   $and: [
  //     {loginDate: {$gte: +start.format('x')}},
  //     {loginDate: {$lte: +end.format('x')}},
  //     {userId: { $in: usersId }},
  //     {accessType: "ios"},
  //     {type: "login"}
  //   ]
  // }).fetch()

  // // Retrieve Android users
  // const android = Analytics.find({
  //   $and: [
  //     {loginDate: {$gte: +start.format('x')}},
  //     {loginDate: {$lte: +end.format('x')}},
  //     {userId: { $in: usersId }},
  //     {accessType: "android"},
  //     {type: "login"}
  //   ]
  // }).fetch()

  // Retrieve Other OS users
  // const others = Analytics.find({
  //   userId: { $in: usersId },
  //   accessType: undefined,
  //   type: "login"
  // }).fetch()

  // console.log("web: ", web)
  // console.log("mobile: ", mobile)
	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearTotalUsersAccessType(start, end, {
			"web": web,
			"mobile": mobile,
			// "ios": ios,
			// "android": android,
			// "others": others
		})
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthTotalUsersAccessType(start, end, {
			"web": web,
			"mobile": mobile,
			// "ios": ios,
			// "android": android,
			// "others": others
		})
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekTotalUsersAccessType(start, end, {
			"web": web,
			"mobile": mobile,
			// "ios": ios,
			// "android": android,
			// "others": others
		})
	}
	else {
		res = DayTotalUsersAccessType(start, end, {
			"web": web,
			"mobile": mobile,
			// "ios": ios,
			// "android": android,
			// "others": others
		})
	}
	let translation = {stats: new CommonTranslation((FlowRouter.getParam("lang") || "fr"))}
	let data = {
		cols: res.cols,
		total: res.total,
		categories: [
			{
				name: translation.stats.commonTranslation["total_web_users"],
				slug: "web",
				color: "#BDC3C7"
			},
			{
				name: translation.stats.commonTranslation["total_mobile_users"],
				slug: "mobile",
				color: "#4183D7"
			},
			// {
			// 	name: translation.stats.commonTranslation["total_ios_users"],
			// 	slug: "ios",
			// 	color: "#4183D7"
			// },
			// {
			// 	name: translation.stats.commonTranslation["total_android_users"],
			// 	slug: "android",
			// 	color: "#1BBC9B"
			// },
			// {
			// 	name: translation.stats.commonTranslation["total_other_os_users"],
			// 	slug: "other",
			// 	color: "#F5AB35"
			// }
		]
	};
	return data;
}

export function buildDatasetTotalUsersAccessType(start, end, condoId) {
  const data = getTotalUsersAccessType(start, end, condoId);
  // console.log("data: ", data)

	let label = [];
	let dataset = [];

	_.each(data.cols, (val) => {
		label.push(val.name);
	});

	let totalDs = {
		label: '',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		data: [],
		type: 'line',
		datalabels: {
			color: '#88898e',
			align: 'end'
		}
  };

  _.each(data.categories, (val, key) => {
      let ds = {
          label: val.name,
          backgroundColor: val.color,
          borderColor: 'white',
          borderWidth: 1,
          data: [],
          datalabels: {
              display: false,
          }
      };

      _.each(data.cols, (v, index) => {
          if (v.types.length > 0) {
              const dsData = v.types[key];
              if (dsData !== undefined) {
                  if (totalDs.data[index] == undefined) {
                      totalDs.data[index] = 0;
                  }
                  totalDs.data[index] += dsData.value;
                  ds.data.push(dsData.value);
              } else {
                  ds.data.push(0);
              }
          } else {
              ds.data.push(0);
          }
      });
      dataset.push(ds);
  });

  dataset.push(totalDs);

  // Maybe this code has a purpose
  // I can't figure it ou now
  // It causes display but so I've commented it
  // Uncoment it if you find it purpose
	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.cols, function(col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: label,
		datasets: dataset
	}
}

function renderChartTotalUsersAccessType(start, end, condoId, instance) {
	let ctx = document.getElementById(chartId).getContext('2d');

	$('#total-users-access-type-chart-no-data').hide();
	let data = buildDatasetTotalUsersAccessType(start, end, condoId);

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('totalUsersAccessType', data)
  }

	let tester = [];
	_.each(data.datasets, function(d) {
		tester.push(_.filter(d.data, function (data_) {
			return data_ != 0;
		}))
	})
	tester = tester.filter(function(test) {return test.length != 0});

	if (tester.length == 1) {
		$('#total-users-access-type-chart-no-data').show();
	}

    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontSize: 12,
            defaultFontColor: "#8b999f",
            plugins: {
                datalabels: {
                    enabled: true,
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    }
                },
            },
            layout: {
                padding: {
                    left: 20,
                    right: 23,
                    top: 20,
                    bottom: 0
                },
            },
            tooltips: {
                enabled: false,
                custom: function(tooltipModel) {
                    if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
                        return ;
                    var tooltipEl = document.getElementById('chartjs-total-users-access-type-chart-tooltips');

                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-tooltip';
                        tooltipEl.innerHTML = "<table></table>"
                        document.body.appendChild(tooltipEl);
                    }

                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function(title) {
                            innerHtml += '<tr><th></th></tr>';
                        });
                        innerHtml += '</thead><tbody>';

                        bodyLines.forEach(function(body, i) {
                            if (body[0].split(':')[1]) {
                                var colors = tooltipModel.labelColors[i];
                                var style = 'background:' + colors.backgroundColor;
                                style += '; border-color:' + colors.borderColor;
                                style += '; border-width: 2px';
                                var div = '';
                                innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
                            }
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    var position = this._chart.canvas.getBoundingClientRect();

                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = (tooltipModel.caretX + 40) + 'px';
                    tooltipEl.style.top = (tooltipModel.caretY - 10) + 'px';
                    // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                }
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true,
                        display: true,
                        autoSkip: false,
                        padding: 10,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    },
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                }, {
                    id: 'littlebar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        max: 50,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }],

                xAxes: [{
                    categorySpacing: 16,
                    afterSetDimensions: (axis) => {
                        setTimeout(function() {
                            $('#x-axis-arrow-utilisation').css("top", (axis.top + 35) + "px");
                            $('#x-axis-arrow-utilisation').css("left", (axis.right + 45) + "px");
                            $('#x-axis-arrow-utilisation').css("opacity", 1);
                        }, 50);
                    },
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        padding: 10,
                        autoSkip: false,
                        maxRotation: 0,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            let val = value.split(' ');
                            if (values.length > 10) {
                                if (index % (parseInt(values.length / 10) + 1) == 0) {
                                    if (val.length == 6) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + ' ' + val[3],
                                            val[4] + ' ' + val[5]
                                        ];
                                    }
                                    if (val.length == 4) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + '-' + val[3]
                                        ];
                                    }
                                    if (val.length == 3) {
                                        return [
                                            val[0].substring(0, val[0].length - 1),
                                            val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                        ];
                                    }
                                    else if (val.length == 2) {
                                        return [
                                            val[0],
                                            val[1]
                                        ]
                                    }
                                    else if (val.length == 1) {
                                        return val[0];
                                    }
                                }
                            }
                            else {
                                if (val.length == 6) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + ' ' + val[3],
                                        val[4] + ' ' + val[5]
                                    ];
                                }
                                else if (val.length == 4) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + '-' + val[3]
                                    ];
                                }
                                else if (val.length == 3) {
                                    return [
                                        val[0].substring(0, val[0].length - 1),
                                        val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                    ];
                                }
                                else if (val.length == 2) {
                                    return [
                                        val[0],
                                        val[1]
                                    ]
                                }
                                else if (val.length == 1) {
                                    return val[0];
                                }
                            }
                        }
                    }
                }]
            },
            legend: false
        }
    });
  instance.isLoading.set(false)
    $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
}

Template.totalUsersAccessType_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
  let lang = FlowRouter.getParam("lang") || "fr"
    this.totalUsersAccessTypeFilter = new ReactiveVar({
      startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
      endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
    });
    this.autorun(() => {
        this.totalUsersAccessTypeFilter.set({
            startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
            endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
        })
    });
});

Template.totalUsersAccessType_stats.onRendered(function() {
});

Template.totalUsersAccessType_stats.onDestroyed(function() {
});

Template.totalUsersAccessType_stats.events({
});

Template.totalUsersAccessType_stats.helpers({
    renderChartTotalUsersAccessType : () => {
      const instance = Template.instance();
      let filter = instance.totalUsersAccessTypeFilter.get();
      let condoId = Template.currentData().condoId
      const ready = Template.currentData().isReady

      if (ready) {
        setTimeout(() => {
          $('#'+chartContainer + ' .chartjs-size-monitor').remove();
          $('#'+chartId).remove();
          $('#'+chartContainer).append('<canvas id="'+chartId+'" height="340"><canvas>');
          renderChartTotalUsersAccessType(filter.startDate.clone(), filter.endDate.clone(), condoId, instance);
        }, 800);
      }
    },
    ready: () => {
        return !!Template.currentData().isReady;
    },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
