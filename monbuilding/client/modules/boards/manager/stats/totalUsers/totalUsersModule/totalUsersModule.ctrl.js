import { Chart } from 'chart.js';
import { CommonTranslation } from '/common/lang/lang.js'
import 'chartjs-plugin-datalabels';

let myChart
const chartContainer = 'total-users-module-chart-container'
const chartId = 'total-users-module-chart'

function getUsersByDay (collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('DD[/]MM[/]YY')} && moment n: ${moment(u.loginDate).format('DD[/]MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('DD[/]MM[/]YY') == moment(u.endDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByWeek (collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('W YYYY')} && moment n: ${moment(u.loginDate).format('W YYYY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('W YYYY') == moment(u.endDate).format('W YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByMonth (collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.endDate).format('MM[/]YY')} && moment n: ${moment(u.endDate).format('MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('MM[/]YY') == moment(u.endDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByYear (collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.endDate).format('YYYY')} && moment n: ${moment(u.endDate).format('YYYY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('YYYY') == moment(u.endDate).format('YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function containCategory (categories, categoryName) {
  return _.find(categories, (c) => { return c.slug === categoryName }) !== undefined
}

function DayTotalUsersModule (start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
  let cols = []
  let total = 0
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)
  let marketPlace = getUsersByDay(collection.marketPlace)

  const duration = end.diff(start, 'days')
  for (var i = 0; i < duration; i++) {
    let tmpCol = {
      types: [],
      name: start.locale(lang).format('ddd DD-MMM'),
      total: 0
    }

    let feedOfDay = _.filter(feed, (a) => {
      return moment(a.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let forumOfDay = _.filter(forum, (a) => {
      return moment(a.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let incidentsOfDay = _.filter(incidents, (i) => {
      return moment(i.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let classifiedsOfDay = _.filter(classifieds, (c) => {
      return moment(c.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let actusOfDay = _.filter(actus, (a) => {
      return moment(a.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let resourcesOfDay = _.filter(resources, (r) => {
      if (r != undefined) {
        return moment(r.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
      }
    })
    let conciergeOfDay = _.filter(concierge, (c) => {
      return moment(c.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let messengerOfDay = _.filter(messenger, (m) => {
      return moment(m.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let trombiOfDay = _.filter(trombi, (t) => {
      return moment(t.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    let marketOfDay = _.filter(marketPlace, (t) => {
      return moment(t.endDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY')
    })
    tmpCol.types.push({ value: feedOfDay.length, category: "feed" })
    tmpCol.types.push({ value: forumOfDay.length, category: "forum" })
    tmpCol.types.push({ value: incidentsOfDay.length, category: "incidents" })
    tmpCol.types.push({ value: classifiedsOfDay.length, category: "classifieds" })
    tmpCol.types.push({ value: actusOfDay.length, category: "informations" })
    tmpCol.types.push({ value: resourcesOfDay.length, category: "reservations" })
    tmpCol.types.push({ value: conciergeOfDay.length, category: "conciergerie" })
    tmpCol.types.push({ value: messengerOfDay.length, category: "messenger" })
    tmpCol.types.push({ value: trombiOfDay.length, category: "trombi" })
    tmpCol.types.push({ value: marketOfDay.length, category: "marketPlace" })
    tmpCol.total = (feedOfDay.length + forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + trombiOfDay.length + marketOfDay.length)
    cols.push(tmpCol)
    total += (feedOfDay.length + forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + trombiOfDay.length + marketOfDay.length)
    start.add(1, 'd')
  }
  return { cols: cols, total: total }
}

function WeekTotalUsersModule (start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
  let cols = []
  let total = 0
  const translation = new CommonTranslation(lang)
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)
  let marketPlace = getUsersByDay(collection.marketPlace)

  const duration = end.diff(start, 'weeks')
  for (var i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone();
    let tmpCol = {
      types: [],
      name: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').format('DD MMM'))),
      total: 0
    }

    let feedOfWeek = _.filter(feed, (a) => {
      return moment(a.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let forumOfWeek = _.filter(forum, (a) => {
      return moment(a.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let incidentsOfWeek = _.filter(incidents, (i) => {
      return moment(i.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let classifiedsOfWeek = _.filter(classifieds, (c) => {
      return moment(c.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let actusOfWeek = _.filter(actus, (a) => {
      return moment(a.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let resourcesOfWeek = _.filter(resources, (r) => {
      if (r != undefined) {
        return moment(r.endDate).format('W YYYY') == start.format('W YYYY')
      }
    })
    let conciergeOfWeek = _.filter(concierge, (c) => {
      return moment(c.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let messengerOfWeek = _.filter(messenger, (m) => {
      return moment(m.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let trombiOfWeek = _.filter(trombi, (t) => {
      return moment(t.endDate).format('W YYYY') == start.format('W YYYY')
    })
    let marketOfWeek = _.filter(marketPlace, (t) => {
      return moment(t.endDate).format('W YYYY') == start.format('W YYYY')
    })
    tmpCol.types.push({ value: feedOfWeek.length, category: "feed" })
    tmpCol.types.push({ value: forumOfWeek.length, category: "forum" })
    tmpCol.types.push({ value: incidentsOfWeek.length, category: "incidents" })
    tmpCol.types.push({ value: classifiedsOfWeek.length, category: "classifieds" })
    tmpCol.types.push({ value: actusOfWeek.length, category: "informations" })
    tmpCol.types.push({ value: resourcesOfWeek.length, category: "reservations" })
    tmpCol.types.push({ value: conciergeOfWeek.length, category: "conciergerie" })
    tmpCol.types.push({ value: messengerOfWeek.length, category: "messenger" })
    tmpCol.types.push({ value: trombiOfWeek.length, category: "trombi" })
    tmpCol.types.push({ value: marketOfWeek.length, category: "marketPlace" })
    tmpCol.total = (feedOfWeek.length + forumOfWeek.length + incidentsOfWeek.length + classifiedsOfWeek.length + actusOfWeek.length + resourcesOfWeek.length + conciergeOfWeek.length + messengerOfWeek.length + trombiOfWeek.length + marketOfWeek.length)
    cols.push(tmpCol)
    total += (feedOfWeek.length + forumOfWeek.length + incidentsOfWeek.length + classifiedsOfWeek.length + actusOfWeek.length + resourcesOfWeek.length + conciergeOfWeek.length + messengerOfWeek.length + trombiOfWeek.length + marketOfWeek.length)
    start.add(1, 'weeks')
  }
  return { cols: cols, total: total }
}

function MonthTotalUsersModule (start, end, collection) {
  let cols = []
  let total = 0
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)
  let marketPlace = getUsersByDay(collection.marketPlace)

  const duration = end.diff(start, 'month')
  for (var i = 0; i <= duration; i++) {
    let tmpCol = {
      types: [],
      name: !(start.format('YYYY') == moment().format('YYYY')) ? start.format('MMMM YYYY') : start.format('MMMM'),
      total: 0
    }

    let feedOfMonth = _.filter(feed, (a) => {
      return moment(a.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let forumOfMonth = _.filter(forum, (a) => {
      return moment(a.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let incidentsOfMonth = _.filter(incidents, (i) => {
      return moment(i.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let classifiedsOfMonth = _.filter(classifieds, (c) => {
      return moment(c.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let actusOfMonth = _.filter(actus, (a) => {
      return moment(a.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let resourcesOfMonth = _.filter(resources, (r) => {
      if (r != undefined) {
        return moment(r.endDate).format('MM[/]YY') == start.format('MM[/]YY')
      }
    })
    let conciergeOfMonth = _.filter(concierge, (c) => {
      return moment(c.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let messengerOfMonth = _.filter(messenger, (i) => {
      return moment(i.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let trombiOfMonth = _.filter(trombi, (t) => {
      return moment(t.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    let marketOfMonth = _.filter(marketPlace, (t) => {
      return moment(t.endDate).format('MM[/]YY') == start.format('MM[/]YY')
    })
    tmpCol.types.push({ value: feedOfMonth.length, category: "feed" })
    tmpCol.types.push({ value: forumOfMonth.length, category: "forum" })
    tmpCol.types.push({ value: incidentsOfMonth.length, category: "incidents" })
    tmpCol.types.push({ value: classifiedsOfMonth.length, category: "classifieds" })
    tmpCol.types.push({ value: actusOfMonth.length, category: "informations" })
    tmpCol.types.push({ value: resourcesOfMonth.length, category: "reservations" })
    tmpCol.types.push({ value: conciergeOfMonth.length, category: "conciergerie" })
    tmpCol.types.push({ value: messengerOfMonth.length, category: "messenger" })
    tmpCol.types.push({ value: trombiOfMonth.length, category: "trombi" })
    tmpCol.types.push({ value: marketOfMonth.length, category: "marketPlace" })
    tmpCol.total = (feedOfMonth.length + forumOfMonth.length + incidentsOfMonth.length + classifiedsOfMonth.length + actusOfMonth.length + resourcesOfMonth.length + conciergeOfMonth.length + messengerOfMonth.length + trombiOfMonth.length + marketOfMonth.length)
    cols.push(tmpCol)
    total += (feedOfMonth.length + forumOfMonth.length + incidentsOfMonth.length + classifiedsOfMonth.length + actusOfMonth.length + resourcesOfMonth.length + conciergeOfMonth.length + messengerOfMonth.length + trombiOfMonth.length + marketOfMonth.length)
    start.add(1, 'M')
  }
  return { cols: cols, total: total }
}

function YearTotalUsersModule (start, end, collection) {
  let cols = []
  let total = 0
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)
  let marketPlace = getUsersByDay(collection.marketPlace)

  const duration = end.diff(start, 'year')
  for (var i = 0; i <= duration; i++) {
    let tmpCol = {
      types: [],
      name: start.format('YYYY'),
      total: 0
    }

    let feedOfYear = _.filter(feed, (a) => {
      return moment(a.endDate).format('YYYY') == start.format('YYYY')
    })
    let forumOfYear = _.filter(forum, (a) => {
      return moment(a.endDate).format('YYYY') == start.format('YYYY')
    })
    let incidentsOfYear = _.filter(incidents, (i) => {
      return moment(i.endDate).format('YYYY') == start.format('YYYY')
    })
    let classifiedsOfYear = _.filter(classifieds, (c) => {
      return moment(c.endDate).format('YYYY') == start.format('YYYY')
    })
    let actusOfYear = _.filter(actus, (a) => {
      return moment(a.endDate).format('YYYY') == start.format('YYYY')
    })
    let resourcesOfYear = _.filter(resources, (r) => {
      if (r != undefined) {
        return moment(r.endDate).format('YYYY') == start.format('YYYY')
      }
    })
    let conciergeOfYear = _.filter(concierge, (c) => {
      return moment(c.endDate).format('YYYY') == start.format('YYYY')
    })
    let messengerOfYear = _.filter(messenger, (m) => {
      return moment(m.endDate).format('YYYY') == start.format('YYYY')
    })
    let trombiOfYear = _.filter(trombi, (t) => {
      return moment(t.endDate).format('YYYY') == start.format('YYYY')
    })
    let marketOfYear = _.filter(marketPlace, (t) => {
      return moment(t.endDate).format('YYYY') == start.format('YYYY')
    })
    tmpCol.types.push({ value: feedOfYear.length, category: "feed" })
    tmpCol.types.push({ value: forumOfYear.length, category: "forum" })
    tmpCol.types.push({ value: incidentsOfYear.length, category: "incidents" })
    tmpCol.types.push({ value: classifiedsOfYear.length, category: "classifieds" })
    tmpCol.types.push({ value: actusOfYear.length, category: "informations" })
    tmpCol.types.push({ value: resourcesOfYear.length, category: "reservations" })
    tmpCol.types.push({ value: conciergeOfYear.length, category: "conciergerie" })
    tmpCol.types.push({ value: messengerOfYear.length, category: "messenger" })
    tmpCol.types.push({ value: trombiOfYear.length, category: "trombi" })
    tmpCol.types.push({ value: marketOfYear.length, category: "marketPlace" })
    tmpCol.total = (feedOfYear.length + forumOfYear.length + incidentsOfYear.length + classifiedsOfYear.length + actusOfYear.length + resourcesOfYear.length + conciergeOfYear.length + messengerOfYear.length + trombiOfYear.length + marketOfYear.lemgth)
    cols.push(tmpCol)
    total += (feedOfYear.length + forumOfYear.length + incidentsOfYear.length + classifiedsOfYear.length + actusOfYear.length + resourcesOfYear.length + conciergeOfYear.length + messengerOfYear.length + trombiOfYear.length + marketOfYear.lemgth)
    start.add(1, 'y')
  }
  return { cols: cols, total: total }
}

function getModulesForByAccessType (platform, usersId, rightsModules) {
  let modules = []
  switch (platform) {
    case "all":
      modules = Analytics.find({
        userId: { $in: usersId },
        $or: [
          {
            module: 'feed',
          },
          {
            module: 'forum',
            condoId: { $in: rightsModules.forumRightList || [] }
          },
          {
            module: 'incident',
            condoId: { $in: rightsModules.incidentRightList || [] }
          },
          {
            module: 'ads',
            condoId: { $in: rightsModules.adsRightList || [] }
          },
          {
            module: 'info',
            condoId: { $in: rightsModules.infoRightList || [] }
          },
          {
            module: 'resa',
            condoId: { $in: rightsModules.resaRightList || [] }
          },
          {
            module: 'concierge',
            condoId: { $in: rightsModules.conciergeRightList || [] }
          },
          {
            module: 'messenger',
            condoId: { $in: rightsModules.messengerRightList || [] }
          },
          {
            module: 'trombi',
            condoId: { $in: rightsModules.trombiRightList || [] }
          },
          {
            module: 'marketPlace',
            condoId: { $in: rightsModules.marketRightList || [] }
          }
        ],
        type: "module"
      }).fetch()
      break;
    // case "web":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "web",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "ios":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "ios",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "android":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "android",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "others":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: undefined,
    //     type: "module"
    //   }).fetch()
    //   break;
    default:
      break;
  }
  // modules = modules.filter(f => condos.includes(f.condoId))
  // console.log('modules ', modules )
  return modules
}

function getTotalUsersModule (platform, start, end, condoId) {
  let modules = {
    feed: [],
    forum: [],
    incidents: [],
    classifieds: [],
    actus: [],
    resources: [],
    concierge: [],
    messenger: [],
    trombi: [],
    marketPlace: []
  }

  let res;

  let usersId = _.map(GestionnaireUsers.find({
    _id: { $ne: Meteor.userId() },
    'resident.condos.condoId': { $in: condoId }
  }, { fields: { userId: true } }).fetch(), u => u._id)

  let categories = []
  const translation = { stats: new CommonTranslation((FlowRouter.getParam("lang") || "fr")) }

  const userId = Meteor.userId()

  const rightsModules = {
    forumRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('forum', 'seeSubject') : Meteor.userHasRight('forum', 'seeSubject', condoId[0], userId) && condoId,
    incidentRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('incident', 'see') : Meteor.userHasRight('incident', 'see', condoId[0], userId) && condoId,
    adsRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('ads', 'see') : Meteor.userHasRight('ads', 'see', condoId[0], userId) && condoId,
    infoRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('actuality', 'see') : Meteor.userHasRight('actuality', 'see', condoId[0], userId) && condoId,
    resaRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('reservation', 'seeAll') : Meteor.userHasRight('reservation', 'seeAll', condoId[0], userId) && condoId,
    conciergeRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('view', 'concierge') : Meteor.userHasRight('view', 'concierge', condoId[0], userId) && condoId,
    messengerRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise') : Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId[0], userId) && condoId,
    trombiRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('trombi', 'see') : Meteor.userHasRight('trombi', 'see', condoId[0], userId) && condoId,
    marketRightList: condoId.length > 1 ? Meteor.listCondoUserHasRight('marketPlace', 'see') : Meteor.userHasRight('marketPlace', 'see', condoId[0], userId) && condoId,
  }

  // const forumRight = condoId.length === 1 && Meteor.userHasRight('forum', 'seeSubject', condoId[0], userId)
  // const incidentRight = condoId.length === 1 && Meteor.userHasRight('incident', 'see', condoId[0], userId)
  // const adsRight = condoId.length === 1 && Meteor.userHasRight('ads', 'see', condoId[0], userId)
  // const infoRight = condoId.length === 1 && Meteor.userHasRight('actuality', 'see', condoId[0], userId)
  // const resaRight = condoId.length === 1 && Meteor.userHasRight('reservation', 'seeAll', condoId[0], userId)
  // const conciergeRight = condoId.length === 1 && Meteor.userHasRight('view', 'concierge', condoId[0], userId)
  // const messengerRight = condoId.length === 1 && Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId[0], userId)
  // const trombiRight = condoId.length === 1 && Meteor.userHasRight('trombi', 'see', condoId[0], userId)

  getModulesForByAccessType(platform, usersId, rightsModules).forEach((user) => {
    switch (user.module) {
      case "feed":
        modules.feed.push(user)
        if (!containCategory(categories, 'feed')) {
          categories.push({
            name: translation.stats.commonTranslation["total_feed_users"],
            slug: "feed",
            color: "#674172"
          })
        }
        break;
      case "forum":
        if (!!rightsModules.forumRightList.length) {
          modules.forum.push(user)
          if (!containCategory(categories, 'forum')) {
            categories.push({
              name: translation.stats.commonTranslation["total_forum_users"],
              slug: "forum",
              color: "#fe0900"
            })
          }
        }
        break;
      case "incident":
        if (!!rightsModules.incidentRightList.length) {
          modules.incidents.push(user)
          if (!containCategory(categories, 'incidents')) {
            categories.push({
              name: translation.stats.commonTranslation["total_incidents_users"],
              slug: "incidents",
              color: "#5d5e62"
            })
          }
        }
        break;
      case "ads":
        if (!!rightsModules.adsRightList.length) {
          modules.classifieds.push(user)
          if (!containCategory(categories, 'classifieds')) {
            categories.push({
              name: translation.stats.commonTranslation["total_ads_users"],
              slug: "classifieds",
              color: "#fdb813"
            })
          }
        }
        break;
      case "info":
        if (!!rightsModules.infoRightList.length) {
          modules.actus.push(user)
          if (!containCategory(categories, 'informations')) {
            categories.push({
              name: translation.stats.commonTranslation["total_informations_users"],
              slug: "informations",
              color: "#ffe199"
            })
          }
        }
        break;
      case "resa":
        if (!!rightsModules.resaRightList.length) {
          modules.resources.push(user)
          if (!containCategory(categories, 'reservations')) {
            categories.push({
              name: translation.stats.commonTranslation["total_reservations_users"],
              slug: "reservations",
              color: "#69d2e7"
            })
          }
        }
        break;
      case "concierge":
        if (!!rightsModules.conciergeRightList.length) {
          modules.concierge.push(user)
          if (!containCategory(categories, 'conciergerie')) {
            categories.push({
              name: translation.stats.commonTranslation["total_concierge_users"],
              slug: "conciergerie",
              color: "#8b999f"
            })
          }
        }
        break;
      case "messenger":
        if (!!rightsModules.messengerRightList.length) {
          modules.messenger.push(user)
          if (!containCategory(categories, 'messenger')) {
            categories.push({
              name: translation.stats.commonTranslation["total_messenger_users"],
              slug: "messenger",
              color: "#e0e0e0"
            })
          }
        }
        break;
      case "trombi":
        if (!!rightsModules.trombiRightList.length) {
          modules.trombi.push(user)
          if (!containCategory(categories, 'trombi')) {
            categories.push({
              name: translation.stats.commonTranslation["total_trombi_users"],
              slug: "trombi",
              color: "#AEE4EF"
            })
          }
        }
        break;
      case "marketPlace":
        if (!!rightsModules.marketRightList.length) {
          modules.marketPlace.push(user)
          if (!containCategory(categories, 'marketPlace')) {
            categories.push({
              name: translation.stats.commonTranslation["total_market_users"],
              slug: "marketPlace",
              color: "#4183D7"
            })
          }
        }
        break;
      default:
        break;
    }
  })

  // console.log('categories : ', categories)

  // console.log("modules: ", modules)
  if ((duration = end.diff(start, 'year')) > 1) {
    res = YearTotalUsersModule(start, end, {
      "feed": modules.feed,
      "forum": modules.forum,
      "incidents": modules.incidents,
      "ads": modules.classifieds,
      "actus": modules.actus,
      "resources": modules.resources,
      "concierge": modules.concierge,
      "messenger": modules.messenger,
      "trombi": modules.trombi,
      "marketPlace": modules.marketPlace
    })
  }
  else if ((duration = end.diff(start, 'month')) > 2) {
    res = MonthTotalUsersModule(start, end, {
      "feed": modules.feed,
      "forum": modules.forum,
      "incidents": modules.incidents,
      "ads": modules.classifieds,
      "actus": modules.actus,
      "resources": modules.resources,
      "concierge": modules.concierge,
      "messenger": modules.messenger,
      "trombi": modules.trombi,
      "marketPlace": modules.marketPlace
    })
  }
  else if ((duration = end.diff(start, 'days')) > 31) {
    res = WeekTotalUsersModule(start, end, {
      "feed": modules.feed,
      "forum": modules.forum,
      "incidents": modules.incidents,
      "ads": modules.classifieds,
      "actus": modules.actus,
      "resources": modules.resources,
      "concierge": modules.concierge,
      "messenger": modules.messenger,
      "trombi": modules.trombi,
      "marketPlace": modules.marketPlace
    })
  }
  else {
    res = DayTotalUsersModule(start, end, {
      "feed": modules.feed,
      "forum": modules.forum,
      "incidents": modules.incidents,
      "ads": modules.classifieds,
      "actus": modules.actus,
      "resources": modules.resources,
      "concierge": modules.concierge,
      "messenger": modules.messenger,
      "trombi": modules.trombi,
      "marketPlace": modules.marketPlace
    })
  }

  let data = {
    cols: res.cols,
    total: res.total,
    categories: categories
  };
  return data;
}

export function buildDatasetTotalUsersModule (platform, start, end, condoId) {
  if (condoId === "all") {
    condoId = _.map(Condos.find().fetch(), c => { return c._id })
  } else {
    condoId = [condoId]
  }
  // console.log('condoId', condoId)

  const data = getTotalUsersModule(platform, start, end, condoId)
  // console.log('data', data)

  let label = []
  let dataset = []

  _.each(data.cols, (val) => {
    label.push(val.name)
  })

  let totalDs = {
    label: '',
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    data: [],
    type: 'line',
    datalabels: {
      color: '#88898e',
      align: 'end'
    }
  }

  let modules = _.map(Condos.find({ "_id": { $in: condoId } }).fetch(), function (condo) {
    return condo.settings.options
  });

  let isInCondo = false

  _.each(data.categories, val => {
    isInCondo = false
    let ds = {
      label: val.name,
      backgroundColor: val.color,
      borderColor: 'white',
      borderWidth: 1,
      data: [],
      datalabels: {
        display: false,
      }
    }

    _.each(modules, function (module) {
      if (module[val.slug] === true || val.slug === "feed")
        isInCondo = true
    })

    _.each(data.cols, (v, index) => {
      if (isInCondo) {
        if (v.types.length > 0) {
          const dsData = v.types.find(t => t.category === val.slug)
          if (dsData !== undefined) {
            if (totalDs.data[index] === undefined) {
              totalDs.data[index] = 0
            }
            totalDs.data[index] += dsData.value
            ds.data.push(dsData.value)
          } else {
            ds.data.push(0)
          }
        } else {
          ds.data.push(0)
        }
      }
    })

    if (isInCondo) {
      dataset.push(ds)
    }
  })

  dataset.push(totalDs)

  // Maybe this code has a purpose
  // I can't figure it ou now
  // It causes display but so I've commented it
  // Uncoment it if you find it purpose
  dataset.push({
    label: '',
    backgroundColor: '#88898e',
    borderColor: 'white',
    borderWidth: 1,
    data: _.map(data.cols, function (col) { return 1 }),
    yAxisID: 'littlebar',
    type: 'bar',
    datalabels: {
      display: false
    }
  })

  return {
    labels: label,
    datasets: dataset
  }
}

function renderChartTotalUsersModule (platform, start, end, condoId, instance) {
  let ctx = document.getElementById(chartId).getContext('2d');

  $('#total-users-module-chart-no-data').hide();
  let data = buildDatasetTotalUsersModule(platform, start, end, condoId)

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('totalUsersSection', data)
  }

  let tester = [];
  _.each(data.datasets, function (d) {
    tester.push(_.filter(d.data, function (data_) {
      return data_ != 0;
    }))
  })
  tester = tester.filter(function (test) { return test.length != 0 });

  if (tester.length == 1) {
    $('#total-users-module-chart-no-data').show();
  }

  myChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      maintainAspectRatio: false,
      defaultFontSize: 12,
      defaultFontColor: "#8b999f",
      plugins: {
        datalabels: {
          enabled: true,
          display: function (context) {
            return context.dataset.data[context.dataIndex] > 0;
          },
          font: {
            weight: 'bold'
          }
        },
      },
      layout: {
        padding: {
          left: 20,
          right: 23,
          top: 20,
          bottom: 0
        },
      },
      tooltips: {
        enabled: false,
        custom: function (tooltipModel) {
          if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
            return;
          var tooltipEl = document.getElementById('chartjs-total-users-module-chart-tooltips');

          if (!tooltipEl) {
            tooltipEl = document.createElement('div');
            tooltipEl.id = 'chartjs-tooltip';
            tooltipEl.innerHTML = "<table></table>"
            document.body.appendChild(tooltipEl);
          }

          if (tooltipModel.opacity === 0) {
            tooltipEl.style.opacity = 0;
            return;
          }

          tooltipEl.classList.remove('above', 'below', 'no-transform');
          if (tooltipModel.yAlign) {
            tooltipEl.classList.add(tooltipModel.yAlign);
          } else {
            tooltipEl.classList.add('no-transform');
          }

          function getBody (bodyItem) {
            return bodyItem.lines;
          }

          if (tooltipModel.body) {
            var titleLines = tooltipModel.title || [];
            var bodyLines = tooltipModel.body.map(getBody);

            var innerHtml = '<thead>';

            titleLines.forEach(function (title) {
              innerHtml += '<tr><th></th></tr>';
            });
            innerHtml += '</thead><tbody>';

            bodyLines.forEach(function (body, i) {
              if (body[0].split(':')[1]) {
                var colors = tooltipModel.labelColors[i];
                var style = 'background:' + colors.backgroundColor;
                style += '; border-color:' + colors.borderColor;
                style += '; border-width: 2px';
                var div = '';
                innerHtml += '<tr style="border-top: ' + colors.backgroundColor + ' solid 2px"><td style="padding: ' + tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px' + '">' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
              }
            });
            innerHtml += '</tbody>';

            var tableRoot = tooltipEl.querySelector('table');
            tableRoot.innerHTML = innerHtml;
          }

          var position = this._chart.canvas.getBoundingClientRect();

          tooltipEl.style.opacity = 1;
          tooltipEl.style.left = (tooltipModel.caretX + 40) + 'px';
          tooltipEl.style.top = (tooltipModel.caretY - 10) + 'px';
          // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
        }
      },
      scales: {
        yAxes: [{
          stacked: true,
          ticks: {
            beginAtZero: true,
            display: true,
            autoSkip: false,
            padding: 10,
            fontColor: "#8b999f",
            callback: function (value, index, values) {
              if (Math.floor(value) === value) {
                return value;
              }
            }
          },
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
        }, {
          id: 'littlebar',
          display: false,
          stacked: true,
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
          ticks: {
            beginAtZero: true,
            max: 50,
            autoSkip: false,
          },
          type: 'linear',
          position: 'right',
        }],

        xAxes: [{
          categorySpacing: 16,
          afterSetDimensions: (axis) => {
            setTimeout(function () {
              $('#x-axis-arrow-utilisation').css("top", (axis.top + 35) + "px");
              $('#x-axis-arrow-utilisation').css("left", (axis.right + 45) + "px");
              $('#x-axis-arrow-utilisation').css("opacity", 1);
            }, 50);
          },
          stacked: true,
          gridLines: {
            display: false,
            color: "#e0e0e0",
            lineWidth: 2,
            zeroLineWidth: 2,
            zeroLineColor: "#e0e0e0",
            drawTicks: true,
            tickMarkLength: 3
          },
          ticks: {
            padding: 10,
            autoSkip: false,
            maxRotation: 0,
            fontColor: "#8b999f",
            callback: function (value, index, values) {
              let val = value.split(' ');
              if (values.length > 10) {
                if (index % (parseInt(values.length / 10) + 1) == 0) {
                  if (val.length == 6) {
                    return [
                      val[0] + ' ' + val[1],
                      val[2] + ' ' + val[3],
                      val[4] + ' ' + val[5]
                    ];
                  }
                  if (val.length == 4) {
                    return [
                      val[0] + ' ' + val[1],
                      val[2] + '-' + val[3]
                    ];
                  }
                  if (val.length == 3) {
                    return [
                      val[0].substring(0, val[0].length - 1),
                      val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                    ];
                  }
                  else if (val.length == 2) {
                    return [
                      val[0],
                      val[1]
                    ]
                  }
                  else if (val.length == 1) {
                    return val[0];
                  }
                }
              }
              else {
                if (val.length == 6) {
                  return [
                    val[0] + ' ' + val[1],
                    val[2] + ' ' + val[3],
                    val[4] + ' ' + val[5]
                  ];
                }
                else if (val.length == 4) {
                  return [
                    val[0] + ' ' + val[1],
                    val[2] + '-' + val[3]
                  ];
                }
                else if (val.length == 3) {
                  return [
                    val[0].substring(0, val[0].length - 1),
                    val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                  ];
                }
                else if (val.length == 2) {
                  return [
                    val[0],
                    val[1]
                  ]
                }
                else if (val.length == 1) {
                  return val[0];
                }
              }
            }
          }
        }]
      },
      legend: false
    }
  });

  instance.isLoading.set(false)
  $("#chartjs-" + chartId + "-legend").html(myChart.generateLegend());
}

Template.totalUsersModule_stats.onCreated(function () {
  let lang = FlowRouter.getParam("lang") || "fr"
  this.isLoading = new ReactiveVar(true)

  // this.compareTypeFilter = new ReactiveVar("all");
  const tr_common = new CommonTranslation(lang)

  let dropdown = [{
    name: "all",
    displayName: tr_common.commonTranslation["all_platforms"]
  }, {
    name: "web",
    displayName: "Web"
  }, {
    name: "ios",
    displayName: "IOS"
  }, {
    name: "android",
    displayName: "Android"
  }
    // , {
    //   name: "other",
    //   displayName: "Other OS"
    // }
  ];

  this.typeFilter = new ReactiveVar(dropdown)

  this.compareTypeFilter = new ReactiveVar("all");

  this.totalUsersModuleFilter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
  })
  this.autorun(() => {
    this.totalUsersModuleFilter.set({
      startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
      endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
    })
  })
})

Template.totalUsersModule_stats.onRendered(function () {
})

Template.totalUsersModule_stats.onDestroyed(function () {
})

Template.totalUsersModule_stats.events({
  'click .compareTypeFilter' (e, t) {
    t.compareTypeFilter.set(e.currentTarget.getAttribute('filter'));
  }
})

Template.totalUsersModule_stats.helpers({
  renderChartTotalUsersModule: () => {
    const instance = Template.instance()
    const typeFilter = instance.compareTypeFilter.get()
    let filter = instance.totalUsersModuleFilter.get()
    let condoId = Template.currentData().condoId
    const ready = Template.currentData().isReady

    if (ready) {
      setTimeout(() => {
        $('#' + chartContainer + ' .chartjs-size-monitor').remove()
        $('#' + chartId).remove()
        $('#' + chartContainer).append('<canvas id="' + chartId + '" height="340"><canvas>')
        renderChartTotalUsersModule(typeFilter, filter.startDate.clone(), filter.endDate.clone(), condoId, instance)
      }, 800)
    }
  },
  typeFilters: () => {
    if (Template.instance().typeFilter.get())
      return Template.instance().typeFilter.get();
  },
  compareTypeFilter: () => {
    if (Template.instance().typeFilter.get()) {
      let filter = _.find(Template.instance().typeFilter.get(), (x) => { return x.name === Template.instance().compareTypeFilter.get() })
      if (filter) {
        return filter.displayName
      }
    }
  },
  ready: () => {
    return !!Template.currentData().isReady
  },
  getActiveClass: (type) => {
    return Template.instance().compareTypeFilter.get() === type ? 'active' : ''
  },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
})
