import { CommonTranslation } from "/common/lang/lang.js"

Template.stats.onCreated(function () {
  this.currentBoard = new ReactiveVar("stats")
  // this.condoId = new ReactiveVar("all");
  this.condoSearch = new ReactiveVar("");
  this.listSelectedTab = new ReactiveVar("incidentsTab");
  this.search = new ReactiveVar("");
  this.renderedChart = new ReactiveVar([]);
  this.chartData = new ReactiveDict();

  // DONT CHANGE THE ORDER OR IT WILL SLOWING DOWN THE PERFORMANCE!!
  let handle1 = Meteor.subscribe("module_incidents_gestionnaire");
  let handle9 = Meteor.subscribe('reservations');
  let handle10 = Meteor.subscribe('resources');
  let handle11 = Meteor.subscribe('managerResident');
  let handle6 = Meteor.subscribe('statsMessengerFeed');
  Meteor.subscribe('classifiedsFeedStat');
  let handle2 = Meteor.subscribe('forumGestionnaireView');
  let handle3 = Meteor.subscribe("actus_gestionnaire");
  let handle4 = Meteor.subscribe("classifiedsGestionnaireView");
  let handle5 = Meteor.subscribe('managerStatsForumPosts');
  let handle7 = Meteor.subscribe('archives');
  let handle8 = Meteor.subscribe('conciergeMessageGestionnnaire');
  let handle13 = Meteor.subscribe("managerAnalytics");
  let handle12 = Meteor.subscribe('forumCommentPosts');

  let lang = FlowRouter.getParam("lang") || "fr"
  const translation = new CommonTranslation(lang)
  this.isReady = new ReactiveVar(false);
  this.resaReady = new ReactiveVar(false);
  this.resaResidentReady = new ReactiveVar(false);
  this.messageReady = new ReactiveVar(false);
  this.incidentReady = new ReactiveVar(false);
  this.analyticReady = new ReactiveVar(false);
  this.options = new ReactiveDict()
  this.options.setDefault({
    'incidents_stats': true,
    'resourcesUse_stats': false,
    'bookedResources_stats': false,
    'occupancyRate_stats': false,
    'resourcesDistribution_stats': false,
    'bookedByTime_stats': false,
    'messengers_stats': false,
    'utilisation_stats': false,
    'totalUsersAccessType_stats': false,
    'totalUsersModule_stats': false,
    'compares_stats': false,
    'marketPlace_stats': false
  });

  self = this
  Tracker.autorun(() => {
    if (handle1.ready() && handle3.ready() && handle4.ready() && handle5.ready() && handle6.ready() && handle7.ready() && handle8.ready() && handle9.ready() && handle10.ready()) {
      this.isReady.set(true);
    }
    if (handle9.ready() && handle10.ready()) {
      this.resaReady.set(true);
    }
    if (handle9.ready() && handle10.ready() && handle11.ready()) {
      this.resaResidentReady.set(true);
    }
    if (handle6.ready()) {
      this.messageReady.set(true);
    }
    if (handle1.ready()) {
      this.incidentReady.set(true);
    }
    if (handle13.ready()) {
      this.analyticReady.set(true)
    }
  });
  Session.set('startDate', moment().locale(lang).startOf('year').format('DD[/]MM[/]YY'))
  Session.set('endDate', moment().locale(lang).endOf('year').subtract(1, 'days').format('DD[/]MM[/]YY'))
  Session.set('condoId', 'all')
  Meteor.call('setNewAnalytics', { type: "module", module: "stats", accessType: "web", condoId: '' });

});

Template.stats.onDestroyed(function () {
  // Meteor.call('updateAnalytics', { type: "module", module: "stats", accessType: "web", condoId: '' });
  Session.set('startDate', undefined)
  Session.set('endDate', undefined)
  Session.set('condoId', undefined)
  Session.set('statsSeeMore', undefined)
  Session.set('statsSeeMoreIncident', undefined)
})

Template.stats.onRendered(function () {
  // Template.instance().initDatePicker();
  this.initDatePicker = (translation, lang) => {
    setTimeout(function () {
      $('#daterange-stats').daterangepicker({
        "ranges": {
          [translation.commonTranslation["today"]]: [
            moment(),
            moment().locale(lang).endOf('days')
          ],
          [translation.commonTranslation["yesterday"]]: [
            moment().locale(lang).subtract(1, 'days'),
            moment().locale(lang).subtract(1, 'days')
          ],
          [translation.commonTranslation["this_week"]]: [
            moment().locale(lang).startOf('weeks'),
            moment().locale(lang).endOf('weeks'),
          ],
          [translation.commonTranslation["last_week"]]: [
            moment().locale(lang).startOf('weeks').subtract(1, "week"),
            moment().locale(lang).endOf('weeks').subtract(1, "week")
          ],
          [translation.commonTranslation["this_month"]]: [
            moment().locale(lang).startOf('month'),
            moment().locale(lang).endOf('month'),
          ],
          [translation.commonTranslation["last_month"]]: [
            moment().locale(lang).subtract(1, "month").startOf('month'),
            moment().locale(lang).subtract(1, "month").endOf('month')
          ],
          [translation.commonTranslation["this_year"]]: [
            moment().locale(lang).startOf('year'),
            moment().locale(lang).endOf('year').subtract(1, 'days')
          ],
          [translation.commonTranslation["last_year"]]: [
            moment().locale(lang).startOf('year').subtract(1, 'year'),
            moment().locale(lang).startOf('year').subtract(1, 'year').add(11, 'month')
          ]
        },
        "alwaysShowCalendars": true,
        "startDate": Session.get('startDate'),
        "endDate": moment(Session.get('endDate'), 'DD[/]MM[/]YY').subtract(1, 'days'),
        "opens": "left",
        "showCustomRangeLabel": false,
        "autoApply": true,
        "locale": $.fn.datepicker.dates[lang]
      }, function (start, end, label) {
        let endDate = end.clone();

        let start_month = start.clone().startOf('month');
        let end_month = end.clone().endOf('month');
        if (label != null) {
          setTimeout(function () {
            $('#daterange-stats').val(label);
          }, 10);
        } else {
          setTimeout(function () {

            if (start.format('DD MM YY') == end.format('DD MM YY')) {
              if (start.format('YYYY') == moment().format('YYYY')) {
                $('#daterange-stats').val(start.locale(lang).format('DD MMM'));
              }
              else {
                $('#daterange-stats').val(start.locale(lang).format('DD MMM YYYY'));
              }
            }

            else if (start.format('MM YY') == end.format('MM YY')) {
              if (start.format('DD MM YY') == start_month.startOf('month').format('DD MM YY') && end.format('DD MM YY') == end_month.endOf('month').format('DD MM YY')) {

                if (start.format('YYYY') == moment().format('YYYY')) {
                  $('#daterange-stats').val(start.locale(lang).format('MMMM'));
                }

                else {
                  $('#daterange-stats').val(start.locale(lang).format('MMMM YYYY'));
                }
              }

              else {

                if (start.format('YYYY') == moment().format('YYYY')) {
                  $('#daterange-stats').val(start.locale(lang).format('DD') + ' - ' + end.locale(lang).format('DD MMM'));
                }
                else {
                  $('#daterange-stats').val(start.locale(lang).format('DD') + ' - ' + end.locale(lang).format('DD MMM YYYY'));
                }
              }
            }
            else {
              let ret = "";
              if (start.format('YYYY') == moment().format('YYYY')) {
                ret = (start.locale(lang).format('DD MMM') + ' - ');
              }
              else {
                ret = (start.locale(lang).format('DD MMM YYYY') + ' - ');
              }
              if (end.format('YYYY') == moment().format('YYYY')) {
                ret += (end.locale(lang).format('DD MMM'));
              }
              else {
                ret += (end.locale(lang).format('DD MMM YYYY'));
              }
              $('#daterange-stats').val(ret);
            }
          }, 10)
        }
        Session.set('startDate', start.locale(lang).format('DD[/]MM[/]YY'));
        Session.set('endDate', endDate.locale(lang).add(1, 'days').format('DD[/]MM[/]YY'));
      });
      $('#daterange-stats').val(translation.commonTranslation["this_year"]);
    }, 2000);
  }
})

Template.stats.events({
  'input #condoSearch': function (e, t) {
    t.condoSearch.set(e.currentTarget.value);
  },
  'click .dropdown-condo': function (event, template) {
    Template.instance().condoId.set(event.currentTarget.getAttribute("data-id"));
    Session.set('condoId', event.currentTarget.getAttribute('data-id'))
    Session.set('statsSeeMore', undefined)
    Session.set('statsSeeMoreIncident', undefined)
  },

  'click button[name="listTabSwitch"]': function (event, template) {
    template.listSelectedTab.set($(event.currentTarget).attr('id'));
  },
  'click .form-tab': function (event, template) {
    const element = $(event.currentTarget);
    const tab = element.data('type');
    template.currentBoard.set(tab);

    let lang = FlowRouter.getParam("lang") || "fr"
    const translation = new CommonTranslation(lang)

    if (event.currentTarget.getAttribute("tab") == "stats") {

    }
  },
  'input .detailsSearch': function (event, template) {
    template.search.set(event.currentTarget.value);
  },
  'click #back-btn': (e, t) => {
    Session.set('statsSeeMore', undefined)
    Session.set('statsSeeMoreIncident', undefined)
  }
});

Template.stats.helpers({
  condos: () => {
    let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
    return (Condos.find({ $or: [{ name: regexp }, { "info.id": regexp }, { "info.address": regexp }, { "info.code": regexp }, { "info.city": regexp }] }).fetch());
  },
  currentCondo: () => {
    let condoId = Template.currentData().selectedCondoId
    if (condoId === "all")
      return translation.commonTranslation["all_buildings"];
    let condo = Condos.findOne({ _id: condoId });
    if (condo)
      return condo.name;
  },
  currentBoard: () => {
    return Template.instance().currentBoard.get();
  },
  currentBoardIs: (board) => {
    return Template.instance().currentBoard.get() === board ? 'tab-active' : '';
  },
  getCondo: () => {
    return Template.currentData().selectedCondoId
  },
  // ready: () => {
  // 	return Template.instance().subscriptionsReady();
  // },
  getListActiveTab: () => {
    return Template.instance().listSelectedTab.get();
  },
  countUserInDetails: () => {

  },
  getSearch: () => {
    return Template.instance().search.get();
  },
  doDisplaySatisfactionStatsForArchive: () => {
    let condoIds;
    if (!localStorage.getItem('selectedCondoId') || localStorage.getItem('selectedCondoId') === "all") {
      condoIds = _.map(Condos.find().fetch(), function (condo) {
        return condo._id;
      });
    } else {
      condoIds = [localStorage.getItem('selectedCondoId')];
    }
    let archives = Archives.find({
      $and: [
        { "condos.id": { $in: condoIds } },
        { "satisfaction": { $exists: true } },
      ]
    }).fetch();
    if (archives.length === 0) {
      Template.instance().currentBoard.set("stats");
    }
    return archives.length > 0;
  },
  doDisplaySatisfactionStatsForIncident: () => {
    let userHasRight = false
    const condoId = Template.currentData().selectedCondoId
    if (condoId !== 'all') {
      userHasRight = Meteor.userHasRight('incident', 'see', condoId)
    } else {
      userHasRight = Meteor.listCondoUserHasRight("incident", "see").length > 0
    }
    return userHasRight
  },
  ready: () => {
    return Template.instance().isReady.get();
  },
  resaReady: () => {
    return Template.instance().resaReady.get();
  },
  resaResidentReady: () => {
    return Template.instance().resaResidentReady.get();
  },
  messageReady: () => {
    return Template.instance().messageReady.get();
  },
  incidentReady: () => {
    return Template.instance().incidentReady.get();
  },
  analyticReady: () => {
    return Template.instance().analyticReady.get();
  },
  moreThanOneCondoType: () => {
    const enterprise = Enterprises.findOne({ _id: Meteor.user().identities.gestionnaireId }, { fields: { users: true } })
    const mapUser = enterprise.users
    const findUser = _.find(mapUser, function (elem) {
      return elem.userId == Meteor.userId();
    })
    condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);

    const condoType = _.map(Condos.find({ "_id": { $in: condoIds } }).fetch(), function (condo) {
      return condo.settings.condoType;
    });

    return _.uniq(condoType).length > 1
  },
  condoIsOffice: () => {
    const condoId = Template.currentData().selectedCondoId
    if (condoId !== 'all') {
      const condo = Condos.findOne(condoId);

      return (!!condo && !!condo.settings && !!condo.settings.condoType && condo.settings.condoType === 'office')
    } else {
      const enterprise = Enterprises.findOne({ _id: Meteor.user().identities.gestionnaireId }, { fields: { users: true } })
      const mapUser = enterprise.users
      const findUser = _.find(mapUser, function (elem) {
        return elem.userId == Meteor.userId();
      })
      condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);

      const condoType = _.map(Condos.find({ "_id": { $in: condoIds } }).fetch(), function (condo) {
        return condo.settings.condoType;
      });

      return _.contains(condoType, 'office')
    }
  },
  renderDatePicker: () => {
    let lang = FlowRouter.getParam("lang") || "fr"
    const translation = new CommonTranslation(lang)
    const instance = Template.instance();
    setTimeout(() => {
      instance.initDatePicker(translation, lang);
    }, 500);
  },
  isSeeMore: () => {
    return !!Session.get('statsSeeMore');
  },
  updateState: () => {
    const instance = Template.instance();

    return (val) => {
      instance.renderedChart.set(_.uniq([
        ...instance.renderedChart.get(),
        val
      ]));
    }
  },
  isDone: (key) => {
    return _.contains(Template.instance().renderedChart.get(), key)
  },
  getChartData: () => {
    const instance = Template.instance()
    return (type, data, duration = false) => {
      instance.chartData.set(type, {
        ...data,
        isDuration: duration
      })
    }
  },
  collectChartData: () => {
    return Template.instance().chartData.all()
  },
  checkRightInStat: (key) => {
    const condoId = Template.currentData().selectedCondoId
    switch (key) {
      case 'seeIncident':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('incident', 'see').length > 0 : Meteor.userHasRight('incident', 'see', condoId, Meteor.userId())
      case 'seeResources':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('reservation', 'seeResources').length > 0 : Meteor.userHasRight('reservation', 'seeResources', condoId, Meteor.userId())
      case 'seeReservation':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('reservation', 'seeAll').length > 0 : Meteor.userHasRight('reservation', 'seeAll', condoId, Meteor.userId())
      case 'seeMessenger':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise').length > 0 : Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, Meteor.userId())
      case 'marketPlace.see':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('marketPlace', 'see').length > 0 : Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())
      default:
        return false
    }
  },
  getOption: (option) => {
    return Template.instance().options.get(option) === true
  },
  onCheckboxClick: (option) => {
    const t = Template.instance()

    return () => () => {
      t.options.set(option, !t.options.get(option))
    }
  }
// seeIncident
// seeResources
// seeReservation
// seeResources
// seeMessenger
});

