import { Chart } from 'chart.js';
import 'chartjs-plugin-datalabels';
import { CommonTranslation } from "/common/lang/lang.js"
import { merge } from 'rxjs';

let myChart;
const chartContainer = 'incident-chart-container';
const chartId = 'incident-chart';

function DayIncidents(start, end, incidents) {
  let lang = (FlowRouter.getParam("lang") || "fr");
	let cols = [];
	let total = 0;

	let duration = end.diff(start, 'day')
	for (var i = 0; i < duration; i++) {
		let tmpCol = {
			types: [],
			name: start.locale(lang).format('ddd DD-MMM'),
			total: 0
		};
		let incidentsOfDay = _.filter(incidents, (i) => {
			return moment(i.createdAt).format("DD[/]MM[/]YY") == start.format('DD[/]MM[/]YY');
		});
		_.each(incidentsOfDay, (i) => {
			if (_.find(tmpCol.types, (t) => {return t.category === i.info.type;})) {
				for (let idx = 0; idx < tmpCol.types.length; idx++) {
					if (tmpCol.types[idx].category === i.info.type) {
						tmpCol.types[idx].value += 1;
						break;
					}
				}
			}
			else {
				tmpCol.types.push({value: 1, category: i.info.type});
			}
			tmpCol.total += 1;
			total++;
		});
		cols.push(tmpCol);
		start.add(1, 'd');
	}
	return {cols: cols, total: total};
}

function WeekIncidents(start, end, incidents, lang) {
	let cols = [];
	let total = 0;

	const translation = new CommonTranslation(lang)

	let duration = end.diff(start, 'weeks');
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		let tmpCol = {
			types: [],
			name: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').locale(lang).format('DD MMM'))),
			total: 0
		};
		let incidentsOfDay = _.filter(incidents, (i) => {
			return moment(i.createdAt).format("W YYYY") == start.format('W YYYY');
		});
		_.each(incidentsOfDay, (i) => {
			if (_.find(tmpCol.types, (t) => {return t.category === i.info.type;})) {
				for (let idx = 0; idx < tmpCol.types.length; idx++) {
					if (tmpCol.types[idx].category === i.info.type) {
						tmpCol.types[idx].value += 1;
						break;
					}
				}
			}
			else {
				tmpCol.types.push({value: 1, category: i.info.type});
			}
			tmpCol.total += 1;
			total++;
		});
		cols.push(tmpCol);
		start.add(1, 'weeks');
	}
	return {cols: cols, total: total};
}

function MonthIncidents(start, end, incidents) {
  let lang = (FlowRouter.getParam("lang") || "fr");
	let cols = [];
	let total = 0;

	let duration = end.diff(start, 'month');
	for (var i = 0; i <= duration; i++) {
		let tmpCol = {
			types: [],
			name: !(start.format('YYYY') == moment().format('YYYY')) ? start.locale(lang).format('MMMM YYYY') :  start.locale(lang).format('MMMM'),
			total: 0
		}

    let incidentsOfDay = _.filter(incidents, (i) => {
			return moment(i.createdAt).format("MM[/]YYYY") === start.format('MM[/]YYYY');
    })

    _.each(incidentsOfDay, (i) => {
			if (_.find(tmpCol.types, (t) => {return t.category === i.info.type;})) {
				for (let idx = 0; idx < tmpCol.types.length; idx++) {
					if (tmpCol.types[idx].category === i.info.type) {
						tmpCol.types[idx].value += 1;
						break;
					}
				}
			}
			else {
				tmpCol.types.push({value: 1, category: i.info.type});
			}
			tmpCol.total += 1;
			total++;
    });
		cols.push(tmpCol);
		start.add(1, 'M');
  }
  // console.log('cols ', cols )
	return {cols: cols, total: total};
}

function YearIncidents(start, end, incidents) {
	let cols = [];
	let total = 0;

	let duration = end.diff(start, 'year')
	for (var i = 0; i <= duration; i++) {
		let tmpCol = {
			types: [],
			name: start.format('YYYY'),
			total: 0
		};
		let incidentsOfDay = _.filter(incidents, (i) => {
			return moment(i.createdAt).format("YYYY") == start.format('YYYY');
		});
		_.each(incidentsOfDay, (i) => {
			if (_.find(tmpCol.types, (t) => {return t.category === i.info.type;})) {
				for (let idx = 0; idx < tmpCol.types.length; idx++) {
					if (tmpCol.types[idx].category === i.info.type) {
						tmpCol.types[idx].value += 1;
						break;
					}
				}
			}
			else {
				tmpCol.types.push({value: 1, category: i.info.type});
			}
			tmpCol.total += 1;
			total++;
		});
		cols.push(tmpCol);
		start.add(1, 'y');
	}
	return {cols: cols, total: total};
}


function getIncidents(start, end, condo, lang) {
	const categories = getCategories(condo);
	let incidents = (condo === "all" ? Incidents.find().fetch() : Incidents.find({condoId: condo}).fetch());
	let res = {
		cols: [],
		total: 0
	};

	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearIncidents(start, end, incidents)
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthIncidents(start, end, incidents)
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekIncidents(start, end, incidents, lang)
	}
	else {
		res = DayIncidents(start, end, incidents)
  }
  // console.log("res:")
  // console.log(res)
  // console.log('categories ', categories )
  let mergedCategories = []
  categories.forEach(ct => {
    const d = _.find(mergedCategories, x => x.defaultDetail === ct.defaultDetail)
    if (d) {
      let _id = []
      mergedCategories.forEach(mc => {
        if (mc.defaultDetail === ct.defaultDetail) {
          _id = _id.concat(mc._id)
          _id.push(ct._id)
          mc._id = _id
        }
      })
    } else {
      ct._id = [ct._id]
      mergedCategories.push(ct)
    }
  })
  // console.log('mergedCategories ', mergedCategories )
  // let catFiltered = {}
  // categories.forEach(cat => {
  //   if (!catFiltered[cat['value-fr']]) {
  //     catFiltered[cat['value-fr']] = [cat]
  //   } else {
  //     catFiltered[cat['value-fr']].push(cat)
  //   }
  // })
  // console.log('catFiltered ', catFiltered )
  // console.log('res.cols ', res.cols )
	return {
		cols: res.cols,
		total: res.total,
    categories: mergedCategories
	};
}

function getCategories(condoId) {
  if ((!condoId || condoId === undefined) || condoId !== "" || condoId !== "all")
    return IncidentDetails.find({
      $or:
        [
          { isNewEmpty: { $exists: false } },
          { isNewEmpty: { $ne: true } }
        ]
    }).fetch();
  else if (condoId != "" && condoId != "all") {
    let getNotDefault = IncidentDetails.find(
      {
        $and: [
          { condoId: condoId },
          {
            $or:
              [
                { isNewEmpty: { $exists: false } },
                { isNewEmpty: { $ne: true } }
              ]
          }
        ]
      }).fetch();
    if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
      return IncidentDetails.find(
        {
          $and: [
            { condoId: "default" },
            {
              $or:
                [
                  { isNewEmpty: { $exists: false } },
                  { isNewEmpty: { $ne: true } }
                ]
            }
          ]
        }).fetch();
    else
      return getNotDefault;
  }
  return [];
}

function buildDatasetIncident(start, end, condo, lang) {

	const data = getIncidents(start, end, condo, lang);
	let label = [];
	let dataset = [];

//	const lang = (FlowRouter.getParam("lang") || "fr");

	let totalDs = {
		label: '',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		data: [],
		type: 'line',
		datalabels: {
			color: '#88898e',
			align: 'end'
		}
	};

	_.each(data.cols, (val) => {
		label.push(val.name);
		totalDs.data.push(0);
	});

	const color = [
		'#fe0900',
		'#ff9f9b',
		'#fdb813',
		'#ffe199',
		'#69d2e7',
		'#cff1f8',
		'#5d5e62',
		'#8b999f',
		'#e0e0e0',
    '#84d0e4',
    '#4682B4',
  ];

  let i = 0
  data.categories.forEach(val => {
    let keyMod = i % 11;
    i++
		let ds = {
			label: val["value-" + lang],
			backgroundColor: color[keyMod],
			hoverBackgroundColor: color[keyMod],
			borderColor: 'white',
			borderWidth: 1,
			data: [],
			datalabels: {
				display: false
			}
		};

		_.each(data.cols, (v, k) => {
			let total = 0;
			if (v.types.length > 0) {
        let resTot = 0
        let found = false
        v.types.forEach(x => {
          val._id.forEach(cid => {
            if (cid === x.category) {
              resTot += x.value
              found = true
            }
          })
        })
				if (found === true) {
					ds.data.push(resTot);
					total = total + resTot;
				} else {
					ds.data.push(0);
				}
			} else {
				ds.data.push(0);
			}
			totalDs.data[k] = totalDs.data[k] + total;
    });
		dataset.push(ds);
	});

	dataset.push(totalDs);
	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.cols, function(col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});


	// dataset = dataset.reverse();

	return {
		labels: label,
		datasets: dataset
	}
}

function renderChartIncident(start, end, condo, lang, instance) {
	let ctx = document.getElementById(chartId).getContext('2d');
	$('#incident-chart-no-data').hide();

  let data = buildDatasetIncident(start, end, condo, lang);
  // console.log('data ', data )

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('incidents', data)
  }

	let tester = [];
	_.each(data.datasets, function(d) {
		tester.push(_.filter(d.data, function (data_) {
			return data_ != 0;
		}))
	})
	tester = tester.filter(function(test) {return test.length != 0});
	if (tester.length === 1) {
		$('#incident-chart-no-data').show();
  }
  // console.log("data:")
  // console.log(data)
  // console.log('data', data)
    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontSize: 12,
            defaultFontColor: "#8b999f",
            plugins: {
                datalabels: {
                    enabled: true,
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    },
                },
            },
            layout: {
                padding: {
                    left: 20,
                    right: 23,
                    top: 20,
                    bottom: 0
                },
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero: true,
                        display: true,
                        padding: 10,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    },
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                }, {
                    id: 'littlebar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        max: 50,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }],

                xAxes: [{
                    categorySpacing: 16,
                    afterSetDimensions: (axis) => {
                        setTimeout(function() {
                            $('#x-axis-arrow-incident').css("top", (axis.top + 55) + "px");
                            $('#x-axis-arrow-incident').css("left", (axis.right + 45) + "px");
                            $('#x-axis-arrow-incident').css("opacity", 1);
                        }, 50);
                    },
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        padding: 10,
                        autoSkip: false,
                        maxRotation: 0,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            let val = value.split(' ');
                            if (values.length > 10) {
                                if (index % (parseInt(values.length / 10) + 1) == 0) {
                                    if (val.length == 6) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + ' ' + val[3],
                                            val[4] + ' ' + val[5]
                                        ];
                                    }
                                    if (val.length == 4) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + '-' + val[3]
                                        ];
                                    }
                                    if (val.length == 3) {
                                        return [
                                            val[0].substring(0, val[0].length - 1),
                                            val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                        ];
                                    }
                                    else if (val.length == 2) {
                                        return [
                                            val[0],
                                            val[1]
                                        ]
                                    }
                                    else if (val.length == 1) {
                                        return val[0];
                                    }
                                }
                            }
                            else {
                                if (val.length == 6) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + ' ' + val[3],
                                        val[4] + ' ' + val[5]
                                    ];
                                }
                                else if (val.length == 4) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + '-' + val[3]
                                    ];
                                }
                                else if (val.length == 3) {
                                    return [
                                        val[0].substring(0, val[0].length - 1),
                                        val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                    ];
                                }
                                else if (val.length == 2) {
                                    return [
                                        val[0],
                                        val[1]
                                    ]
                                }
                                else if (val.length == 1) {
                                    return val[0];
                                }
                            }
                        }
                    }
                }]
            },
            tooltips: {
                enabled: false,
                custom: function(tooltipModel) {
                    if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] === undefined))
                        return ;
                    // Tooltip Element
                    var tooltipEl = document.getElementById('chartjs-incident-chart-tooltips');

                    // Create element on first render
                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-tooltip';
                        tooltipEl.innerHTML = "<table></table>"
                        document.body.appendChild(tooltipEl);
                    }

                    // Hide if no tooltip
                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    // Set caret Position
                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    // Set Text
                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function(title) {
                            innerHtml += '<tr><th></th></tr>';
                        });
                        innerHtml += '</thead><tbody>';

                        bodyLines.forEach(function(body, i) {
                            if (body[0].split(':')[1]) {
                                var colors = tooltipModel.labelColors[i];
                                var style = 'background:' + colors.backgroundColor;
                                style += '; border-color:' + colors.borderColor;
                                style += '; border-width: 2px';
                                var div = '';
                                innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
                            }
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    // `this` will be the overall tooltip
                    var position = this._chart.canvas.getBoundingClientRect();

                    // Display, position, and set styles for font
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = (tooltipModel.caretX + 40) + 'px';
                    tooltipEl.style.top = (tooltipModel.caretY - 10) + 'px';
                    // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                }
            },
            legend: false
        }
    });

  instance.isLoading.set(false)
    $("#chartjs-incident-chart-legend").html(myChart.generateLegend());
    // instance.data.onChartRendered('incident');
}

Template.incidents_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
	this.incidentFilter = new ReactiveVar({
		startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
		endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
	});

    this.autorun(() => {
        this.incidentFilter.set({
            startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
            endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
        })
    });
});


Template.incidents_stats.onRendered(function() {
});

Template.incidents_stats.onDestroyed(function() {
})

Template.incidents_stats.events({
});

Template.incidents_stats.helpers({
	renderChartIncident : () => {
	  const instance = Template.instance();
		let incidentDate = Template.instance().incidentFilter.get();
    let condoId = Template.currentData().condoId
		const ready = Template.instance().data.isReady;

		let lang = FlowRouter.getParam("lang") || "fr"

		if (ready) {
			setTimeout(() => {
				$('#'+chartContainer + ' .chartjs-size-monitor').remove();
				$('#'+chartId).remove();
				$('#'+chartContainer).append('<canvas id="'+chartId+'" height="340"><canvas>');
        renderChartIncident(incidentDate.startDate.clone(), incidentDate.endDate.clone(), condoId, lang, instance);
			}, 800);
		}
	},
	ready: () => {
		return !!Template.instance().data.isReady;
	},
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
