import { Chart } from 'chart.js';
import { Stats, CommonTranslation } from '/common/lang/lang.js'
import 'chartjs-plugin-datalabels';

let myChart;
const chartContainer = 'utilisation-chart-container';
const chartId = 'utilisation-chart';

async function getStatsMarketPlace(condoId = null, start = null, end = null) {
  let stats = await new Promise(function (resolve, reject) {
    Meteor.call('getStatsMarketPlace', condoId, parseInt(start), parseInt(end), (error, result) => {
      if (!error) {
        resolve(result)
      } else {
        reject(error)
      }
    })
  })

  return stats
}

function DayUtilisations(start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'days');
	for (var i = 0; i < duration; i++) {
		tmpCol = {
			types: [],
			name: start.locale(lang).format('ddd DD-MMM'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let actusOfDay = _.filter(collection.actus, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let resourcesOfDay = _.filter(collection.resources, (a) => {
			if (a != undefined)
				return moment(a.start).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let messengerOfDay = _.filter(collection.messenger, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
    let marketPlaceOfDay = _.filter(collection.marketPlace, (i) => {
      return moment(i.validatedAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    });
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "informations"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "reservations"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "conciergerie"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
    tmpCol.types.push({value: marketPlaceOfDay.length, category: "marketPlace"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		start.add(1, 'd');
	}
	return {cols: cols, total: total};
}

function WeekUtilisations(start, end, collection) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
	let duration = 0;
  let tmpCol = [];
  const translation = new CommonTranslation(lang)

	duration = end.diff(start, 'weeks');
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		tmpCol = {
			types: [],
			name: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('W YYYY') == start.format('W YYYY');
		});
		let replyOfForum = _.flatten(_.map(collection.forum, (a) => {
			return _.filter(a.message, function(b) {
				return moment(b.createdAt).format('W YYYY') == start.format('W YYYY');
			})
		}));
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
            if (i != undefined)
			return moment(i.start).format('W YYYY') == start.format('W YYYY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('W YYYY') == start.format('W YYYY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('W YYYY') == start.format('W YYYY');
		});
    let marketPlaceOfDay = _.filter(collection.marketPlace, (i) => {
      return moment(i.validatedAt).format('W YYYY') == start.format('W YYYY');
    });
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "informations"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "reservations"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "conciergerie"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
    tmpCol.types.push({value: marketPlaceOfDay.length, category: "marketPlace"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		start.add(1, 'weeks');
	}
	return {cols: cols, total: total};
}

function MonthUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'month');
	for (var i = 0; i <= duration; i++) {
		tmpCol = {
			types: [],
			name: !(start.format('YYYY') == moment().format('YYYY')) ? start.format('MMMM YYYY') :  start.format('MMMM'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('MM[/]YY') == start.format('MM[/]YY');
    });
		let replyOfForum = _.flatten(_.map(collection.forum, (a) => {
			return _.filter(a.message, function(b) {
				return moment(b.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
			})
		}));
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
    });
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
            if (i != undefined)
			return moment(i.start).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('MM[/]YY') == start.format('MM[/]YY');
		});
    let marketPlaceOfDay = _.filter(collection.marketPlace, (i) => {
      return moment(i.validatedAt).format('MM[/]YY') == start.format('MM[/]YY');
    });
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "informations"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "reservations"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "conciergerie"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
    tmpCol.types.push({value: marketPlaceOfDay.length, category: "marketPlace"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		start.add(1, 'M');
	}
	return {cols: cols, total: total};
}

function YearUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'year');
	for (var i = 0; i <= duration; i++) {
		tmpCol = {
			types: [],
			name: start.format('YYYY'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('YYYY') == start.format('YYYY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
            if (i != undefined)
			return moment(i.start).format('YYYY') == start.format('YYYY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('YYYY') == start.format('YYYY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('YYYY') == start.format('YYYY');
		});
    let marketPlaceOfDay = _.filter(collection.marketPlace, (i) => {
      return moment(i.validatedAt).format('YYYY') == start.format('YYYY');
    });
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "informations"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "reservations"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "conciergerie"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
    tmpCol.types.push({value: marketPlaceOfDay.length, category: "marketPlace"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + marketPlaceOfDay.length);
		start.add(1, 'y');
	}
	return {cols: cols, total: total};
}

async function getUtilisation(start, end, condoId) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let forum = {};
  let actus = (condoId == "all" ? ActuPosts.find().fetch() : ActuPosts.find({condoId: condoId}).fetch());
  let classifieds = (condoId == "all" ?
      ClassifiedsAds.find().fetch().concat(ClassifiedsFeed.find().fetch()) :
      ClassifiedsAds.find({condoId: condoId}).fetch().concat(ClassifiedsFeed.find().fetch()));
  // console.log('classifieds ', classifieds )
	let incidents = (condoId == "all" ? Incidents.find().fetch() : Incidents.find({condoId: condoId}).fetch());
	const resourceIds = _.map(Resources.find(condoId === 'all' ? {} : {condoId: condoId}).fetch(), (r) => {return r._id});
	let resources = Reservations.find({
        $and: [
            {resourceId: {$in: resourceIds}},
            {start: {$gte: +start.locale(lang).format('x')}},
            {start: {$lte: +end.locale(lang).format('x')}},
            condoId === 'all' ? {} : {condoId: condoId},
            {
                $or: [
                    {pending: { $exists: false }},
                    {pending: false}
                ]
            },
            {
                $or: [
                    {rejected: { $exists: false }},
                    {rejected: false}
                ]
            }
        ]
    }).fetch();
	let concierge = (condoId == "all" ? ConciergeMessage.find().fetch() : ConciergeMessage.find({condoId: condoId}).fetch());
	if (condoId === "all") {
    forum = ForumPosts.find().fetch().concat(ForumCommentPosts.find().fetch())
	}
	else {
		forum = ForumPosts.find({condoId: condoId}).fetch().concat(ForumCommentPosts.find({condoId: condoId}).fetch())
  }
	let conversationIds = (condoId == "all" ? _.map(Messages.find().fetch(), function(conv) {return conv._id}) : _.map(Messages.find({condoId: condoId}).fetch(), function(conv) {return conv._id}))
	let managerIds =[];
	if (condoId == "all") {
		managerIds = _.pluck(Meteor.users.find({"identities.gestionnaireId": {$exists: true}}).fetch(), '_id')
	}
	else {
		managerIds = _.map(Enterprises.findOne({condos: condoId}).users, function(user) {
			inCharge = false;
			_.each(user.condosInCharge, function(condo) { if (condo.condoId == condoId) inCharge = true; });
			if (inCharge == true)
				return user.userId;
		});
  }
  const marketPlace = await getStatsMarketPlace(condoId, start.format('x'), end.format('x'))
	let messenger = MessagesFeed.find({messageId: {$in: conversationIds}, userId: {$in: managerIds}}).fetch();
	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
      "messenger": messenger,
      'marketPlace': marketPlace
		})
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
      "messenger": messenger,
      'marketPlace': marketPlace
		})
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
      "messenger": messenger,
      'marketPlace': marketPlace
		})
	}
	else {
		res = DayUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
      "messenger": messenger,
      'marketPlace': marketPlace
		})
	}
	let translation = {stats: new Stats((FlowRouter.getParam("lang") || "fr"))}
	let data = {
		cols: res.cols,
		total: res.total,
		categories: [
			{
				name: translation.stats.stats["n_posts"],
				slug: "forum",
				color: "#fe0900"
			},
			{
				name: translation.stats.stats["n_incidents"],
				slug: "incidents",
				color: "#5d5e62"
			},
			{
				name: translation.stats.stats["n_ads"],
				slug: "classifieds",
				color: "#fdb813"
			},
			{
				name: translation.stats.stats["new_info"],
				slug: "informations",
				color: "#ffe199"
			},
			{
				name: translation.stats.stats["n_resa"],
				slug: "reservations",
				color: "#69d2e7"
			},
			{
				name: translation.stats.stats["n_keeper"],
				slug: "conciergerie",
				color: "#8b999f"
			},
			{
				name: translation.stats.stats["n_sent"],
				slug: "messenger",
				color: "#e0e0e0"
			},
			{
        name: translation.stats.stats["n_marketPlace"],
				slug: "marketPlace",
        color: "#4183D7"
			}
		]
	};
	return data;
}

export async function buildDatasetUtilisation(start, end, condoId) {
  const data = await getUtilisation(start, end, condoId);
  // console.log('data ', data )

	let label = [];
	let dataset = [];

	_.each(data.cols, (val) => {
		label.push(val.name);
	});

	let totalDs = {
		label: '',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		data: [],
		type: 'line',
		datalabels: {
			color: '#88898e',
			align: 'end'
		}
	};

	if (condoId === "all") {
		condoId = _.map(Condos.find().fetch(), c => {return c._id})
	} else {
		condoId = [condoId];
	}
	let modules = _.map(Condos.find({"_id": {$in: condoId}}).fetch(), function(condo) {
		return condo.settings.options;
	});

	let isInCondo = false;

	_.each(data.categories, (val, key) => {
		isInCondo = false;
		let ds = {
			label: val.name,
			backgroundColor: val.color,
			borderColor: 'white',
			borderWidth: 1,
			data: [],
			datalabels: {
				display: false,
			}
		};

		_.each(modules, module => {
			if (module[val.slug] == true)
				isInCondo = true;
		})

    _.each(data.cols, (v, index) => {
			if (isInCondo) {
				if (v.types.length > 0) {
          // console.log('val ', val )
					const dsData = v.types.find(t => t.category === val.slug)
					if (dsData !== undefined) {
						if (totalDs.data[index] == undefined) {
							totalDs.data[index] = 0;
						}
						totalDs.data[index] += dsData.value;
						ds.data.push(dsData.value);
					} else {
						ds.data.push(0);
					}
				} else {
					ds.data.push(0);
				}
			}
		});
		if (isInCondo) {
			dataset.push(ds);
		}
	});

	dataset.push(totalDs);
	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.cols, function(col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: label,
		datasets: dataset
	}
}

async function renderChartUtilisation(start, end, condoId, instance) {

	let ctx = document.getElementById(chartId).getContext('2d');

	$('#utilisation-chart-no-data').hide();
	let data = await buildDatasetUtilisation(start, end, condoId);

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('utilization', data)
  }

	let tester = [];
	_.each(data.datasets, function(d) {
		tester.push(_.filter(d.data, function (data_) {
			return data_ != 0;
		}))
  })
	tester = tester.filter(function(test) {return test.length != 0});

	if (tester.length == 1) {
		$('#utilisation-chart-no-data').show();
	}

    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            responsive: true,
            maintainAspectRatio: false,
            defaultFontSize: 12,
            defaultFontColor: "#8b999f",
            plugins: {
                datalabels: {
                    enabled: true,
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    }
                },
            },
            layout: {
                padding: {
                    left: 20,
                    right: 23,
                    top: 20,
                    bottom: 0
                },
            },
            tooltips: {
                enabled: false,
                custom: function(tooltipModel) {
                    if (((tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
                        return ;
                    var tooltipEl = document.getElementById('chartjs-utilisation-chart-tooltips');

                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-tooltip';
                        tooltipEl.innerHTML = "<table></table>"
                        document.body.appendChild(tooltipEl);
                    }

                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function(title) {
                            innerHtml += '<tr><th></th></tr>';
                        });
                        innerHtml += '</thead><tbody>';

                        bodyLines.forEach(function(body, i) {
                            if (body[0].split(':')[1]) {
                                var colors = tooltipModel.labelColors[i];
                                var style = 'background:' + colors.backgroundColor;
                                style += '; border-color:' + colors.borderColor;
                                style += '; border-width: 2px';
                                var div = '';
                                innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
                            }
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    var position = this._chart.canvas.getBoundingClientRect();

                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = (tooltipModel.caretX + 40) + 'px';
                    tooltipEl.style.top = (tooltipModel.caretY - 10) + 'px';
                    // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                }
            },
            scales: {
                yAxes: [{
                    stacked: true,
                    ticks: {
                        beginAtZero:true,
                        display: true,
                        autoSkip: false,
                        padding: 10,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            if (Math.floor(value) === value) {
                                return value;
                            }
                        }
                    },
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                }, {
                    id: 'littlebar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        max: 50,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }],

                xAxes: [{
                    categorySpacing: 16,
                    afterSetDimensions: (axis) => {
                        setTimeout(function() {
                            $('#x-axis-arrow-utilisation').css("top", (axis.top + 35) + "px");
                            $('#x-axis-arrow-utilisation').css("left", (axis.right + 45) + "px");
                            $('#x-axis-arrow-utilisation').css("opacity", 1);
                        }, 50);
                    },
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        padding: 10,
                        autoSkip: false,
                        maxRotation: 0,
                        fontColor: "#8b999f",
                        callback: function(value, index, values) {
                            let val = value.split(' ');
                            if (values.length > 10) {
                                if (index % (parseInt(values.length / 10) + 1) == 0) {
                                    if (val.length == 6) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + ' ' + val[3],
                                            val[4] + ' ' + val[5]
                                        ];
                                    }
                                    if (val.length == 4) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + '-' + val[3]
                                        ];
                                    }
                                    if (val.length == 3) {
                                        return [
                                            val[0].substring(0, val[0].length - 1),
                                            val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                        ];
                                    }
                                    else if (val.length == 2) {
                                        return [
                                            val[0],
                                            val[1]
                                        ]
                                    }
                                    else if (val.length == 1) {
                                        return val[0];
                                    }
                                }
                            }
                            else {
                                if (val.length == 6) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + ' ' + val[3],
                                        val[4] + ' ' + val[5]
                                    ];
                                }
                                else if (val.length == 4) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + '-' + val[3]
                                    ];
                                }
                                else if (val.length == 3) {
                                    return [
                                        val[0].substring(0, val[0].length - 1),
                                        val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                    ];
                                }
                                else if (val.length == 2) {
                                    return [
                                        val[0],
                                        val[1]
                                    ]
                                }
                                else if (val.length == 1) {
                                    return val[0];
                                }
                            }
                        }
                    }
                }]
            },
            legend: false
        }
    });

  instance.isLoading.set(false)
    $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
}


Template.utilisation_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
	this.utilisationFilter = new ReactiveVar({
		startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
		endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
	});
    this.autorun(() => {
        this.utilisationFilter.set({
            startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY'),
            endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY'),
        })
    });
});

Template.utilisation_stats.onRendered(function() {
});

Template.utilisation_stats.onDestroyed(function() {
});

Template.utilisation_stats.events({
});

Template.utilisation_stats.helpers({
	renderChartUtilisation : () => {
	  const instance = Template.instance();
		let filter = instance.utilisationFilter.get();
		let condoId = Template.currentData().condoId
    const ready = instance.data.isReady;

		if (ready) {
			setTimeout(async () => {
				$('#'+chartContainer + ' .chartjs-size-monitor').remove();
				$('#'+chartId).remove();
				$('#'+chartContainer).append('<canvas id="'+chartId+'" height="340"><canvas>');
				await renderChartUtilisation(filter.startDate.clone(), filter.endDate.clone(), condoId, instance);
			}, 800);
		}
	},
    ready: () => {
        return !!Template.instance().data.isReady;
    },
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
