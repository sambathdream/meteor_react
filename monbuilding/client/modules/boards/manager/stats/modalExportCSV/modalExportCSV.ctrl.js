
import { CommonTranslation, Stats, ContactManagement } from "/common/lang/lang.js";
import { WSAEALREADY, SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION } from "constants";
import JSZip from 'jszip'
import { buildDatasetMessenger } from '../messenger/messengers.stats.ctrl'
import { buildDatasetBookedResources } from '../bookedResources/bookedResources.stats.ctrl'
import { buildDatasetOccupancy } from '../occupancyRate/occupancyRate.stats.ctrl'
import { buildDatasetBookedByTime } from '../bookedByTime/bookedByTime.stats.ctrl'
import { buildDatasetResourceDistribution } from '../resourcesDistribution/resourcesDistribution.stats.ctrl'
import { buildDatasetUtilisation } from '../utilisation/utlisation.stats.ctrl'
import { buildDatasetTotalUsersAccessType } from '../totalUsers/totalUsersAccessType/totalUsersAccessType.ctrl'
import { buildDatasetTotalUsersModule } from '../totalUsers/totalUsersModule/totalUsersModule.ctrl'
import { buildDatasetCompare, checkModuleForTypeFilter } from '../compare/compares.stats.ctrl'
import { buildDatasetResourceUse } from '../resourcesUse/resourcesUse.stats.ctrl'
import { buildDatasetMarketPlace } from '../marketPlace/marketPlace.stats.ctrl'

function closeModal(modalId) {
  Meteor.setTimeout(function () {
    Session.set("modal", false);
  }, 500);
}

function convertToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = '';

  for (let i = 0; i < array.length; i++) {
    let line = '';
    for (let index in array[i]) {
      if (line !== '') line += ';'

      line += array[i][index];
    }

    str += line + '\r\n';
  }

  return str;
}

function convertMinute(value, lang) {
  const translate = new ContactManagement(lang)
  const common = new CommonTranslation(lang)

  if (parseInt(value) >= 10080) {
    if (parseInt(value) % 10080 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s)]';
    else
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s) ' + parseInt((parseInt(value) % 10080) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
  }
  else if (parseInt(value) >= 1440) {
    if (parseInt(value) % 1440 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
    else
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s) ' + parseInt((parseInt(value) % 1440) / 60) + 'h]';
  }
  else if (parseInt(value) >= 60) {
    if (parseInt(value) % 60 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h';
    else
      value =  parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
  }
  else if (parseInt(value) !== 0) {
    value =  parseInt(value) + 'min';
  }

  return value
}

function exportCSVFile(headers, items) {
  if (headers) {
    items.unshift(headers);
  }

  // Convert Object to JSON
  const jsonObject = JSON.stringify(items);

  // Return csv for zipped
  return convertToCSV(jsonObject);

  // const csv = convertToCSV(jsonObject);
  //
  // const exportedFilenmae = fileTitle + '.csv' || 'export.csv';
  //
  // const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
  // if (navigator.msSaveBlob) { // IE 10+
  //   navigator.msSaveBlob(blob, exportedFilenmae);
  // } else {
  //   const link = document.createElement("a");
  //   if (link.download !== undefined) { // feature detection
  //     // Browsers that support HTML5 download attribute
  //     const url = URL.createObjectURL(blob);
  //     link.setAttribute("href", url);
  //     link.setAttribute("download", exportedFilenmae);
  //     link.style.visibility = 'hidden';
  //     document.body.appendChild(link);
  //     link.click();
  //     document.body.removeChild(link);
  //   }
  // }
}

download = (meta) => {
  let element = document.createElement('a')
  element.setAttribute('href', meta)
  element.setAttribute('target', '_blank')

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

Template.modal_export_csv.onCreated(function () {
  this.exportStats = new ReactiveDict()
  this.exportStats.setDefault({
    incidents: false,
    resourcesUse: false,
    bookedResources: false,
    occupancyRate: false,
    bookedByTime: false,
    resourcesDistribution: false,
    messenger: false,
    utilization: false,
    totalUsersAccessType: false,
    totalUsersSection: false,
    comparison: false,
    satisfaction: false,
    marketPlace: false,
  })

  this.timeType = new ReactiveVar('time')

  this.stats = [
    {
      name: 'incidents',
      right: 'seeIncident',
      translation: 'incidents',
      subtitle: 'incidents_sub'
    },
    {
      name: 'resourcesUse',
      right: 'seeResources',
      translation: 'resources_use',
      subtitle: ''
    },
    {
      name: 'bookedResources',
      right: 'seeResources',
      translation: 'booked_resources',
      subtitle: 'booked_resources_sub'
    },
    {
      name: 'occupancyRate',
      right: 'seeReservation',
      translation: 'occupancy_rate',
      subtitle: ''
    },
    {
      name: 'resourcesDistribution',
      right: 'seeResources',
      translation: 'resource_distribution',
      subtitle: ''
    },
    {
      name: 'bookedByTime',
      right: 'seeResources',
      translation: 'booked_by_time',
      subtitle: ''
    },
    {
      name: 'messenger',
      right: 'seeMessenger',
      translation: 'messages',
      subtitle: ''
    },
    {
      name: 'utilization',
      right: '',
      translation: 'util',
      subtitle: 'util_sub'
    },
    {
      name: 'totalUsersAccessType',
      right: '',
      translation: 'total_users_access_type',
      subtitle: 'total_users_access_type_sub'
    },
    {
      name: 'totalUsersSection',
      right: '',
      translation: 'total_users_module',
      subtitle: 'total_users_module_sub'
    },
    {
      name: 'comparison',
      right: '',
      translation: 'comparison',
      subtitle: 'comparison_sub'
    },
    {
      name: 'marketPlace',
      right: '',
      translation: 'market_place',
      subtitle: ''
    },
    // {
    //   name: 'satisfaction',
    //   right: '',
    //   translation: 'satisfaction',
    //   subtitle: 'comparison_sub'
    // }
  ]

  this.incidentFilter = new ReactiveVar({
    startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').format('x'),
    endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').format('x'),
  });

  this.getData = async (module, lang, start, end, condo, type = '') => {
    switch (module) {
      case 'messenger':
        return buildDatasetMessenger(start, end, condo, type || 'send-receive', lang)
      case 'bookedResources':
        await Meteor.call('getStatsGraphBookedResources', condo, parseInt(start.locale(lang).format('x')), parseInt(end.locale(lang).format('x')), lang, (error, data) => {
          if (!error) {
            return {
              datasets: [{
                data: _.map(data, (d) => {return d.value}),
              }],
              labels: _.map(data, (d) => {return d.label}),
              isDuration: true
            }
          } else {
            sAlert.error(error)
            return false
          }
        })
      case 'occupancyRate':
        return buildDatasetOccupancy(start, end, condo, 'meeting', 'date', lang)
      case 'bookedByTime':
        return buildDatasetBookedByTime(start, end, condo, 'time', lang)
      case 'resourcesDistribution':
        return buildDatasetResourceDistribution(start, end, condo, 'meeting', 'date', lang)
      case 'utilization':
        return buildDatasetUtilisation(start, end, condo)
      case 'totalUsersAccessType':
        return buildDatasetTotalUsersAccessType(start, end, condo)
      case 'totalUsersSection':
        return buildDatasetTotalUsersModule('all', start, end, condo)
      case 'comparison':
        const types = checkModuleForTypeFilter(lang)
        if (!!types[0] && !!types[0].name) {
          return buildDatasetCompare(!!types[0].name, start, end, condo)
        } else {
          return false
        }
      case 'resourcesUse':
        const dataResource = buildDatasetResourceUse(start, end, condo, lang)
        return {
          datasets: [{
            data: _.map(dataResource, (d) => {return d.value}),
          }],
          labels: _.map(dataResource, (d) => {return d.label}),
          isDuration: true
        }
      case 'marketPlace':
        await Meteor.call('getStatsGraphMarketPlace', condo, parseInt(start.format('x')), parseInt(end.format('x')), (error, result) => {
          if (!error) {
            return buildDatasetMarketPlace(start, end, condo, 'turnover', lang, result)
          } else {
            sAlert.error(error)
            return false
          }
        })
      default:
        return false
    }
  }

  this.autorun(() => {
    this.incidentFilter.set({
      startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').format('x'),
      endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').format('x'),
    })
  });
})

Template.modal_export_csv.onDestroyed(function () {
  // console.log("exportStats: ", exportStats)
})

Template.modal_export_csv.onRendered(function () {
})

Template.modal_export_csv.events({
  'hidden.bs.modal #export_csv_modal': (event, template) => {
    for (let exp in template.exportStats.all()) {
      template.exportStats.set(exp, false)
    }
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #export_csv_modal': (event, template) => {
    let modal = $(event.currentTarget);
    window.location.hash = "#export_csv_modal";
    window.onhashchange = function () {
      if (location.hash != "#export_csv_modal") {
        modal.modal('hide');
      }
    }
  },
  'click .labelForCheckbox': (e, t) => {
    const stat = $(e.currentTarget).attr("for")
    t.exportStats.set(stat, !t.exportStats.get(stat))
  },
  'click .labelForCheckboxAll': (e, t) => {
    const checks = t.exportStats.all()
    const unCheck = _.filter(checks, (u) => {
      return !u
    })

    if (unCheck.length > 0) {
      _.each(checks, (c, i) => {
        t.exportStats.set(i, true)
      })
    } else {
      _.each(checks, (c, i) => {
        t.exportStats.set(i, false)
      })
    }
  },
  'click .confirmAction': async (e, t) => {
		$('.confirmAction').button('loading')
    const incidentDate = t.incidentFilter.get()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Stats(lang)
    const from = moment(+incidentDate.startDate).format('DDMMYYYY')
    const to = moment(+incidentDate.endDate).format('DDMMYYYY')
    const start = moment(+incidentDate.startDate)
    const end = moment(+incidentDate.endDate)
    let checked = false

    const exportStats = t.exportStats.all()
    const zip = new JSZip();
    for (key in exportStats) {
      if (!!exportStats[key]) {
        let chartData = t.data.chartData[key]
        if (!chartData) {
          // if there is no data, try fo fetch it
          chartData = await t.getData(key, lang, start, end, localStorage.getItem('condoIdSelected'))
        }

        if (!!chartData) {
          const linear = !!chartData.datasets[0].label

          let headers = []
          let content = []

          checked = true
          const config = _.find(t.stats, (s) => s.name === key) || {}

          if (linear) {
            let indexColumn = 0
            headers[indexColumn.toString()] = translation.stats.period
            const loop = _.map(chartData.datasets, (d) => {
              if (!!d.label) {
                indexColumn++
                headers[indexColumn.toString()] = d.label.replace(/;/g, ',')
                let indexRow = 0
                const rowLoop = _.map(d.data, (row) => {
                  if (!content[indexRow]) {
                    content[indexRow] = {}
                  }
                  content[indexRow][indexColumn] = chartData.isDuration? convertMinute(row, lang): row.toString().replace(/;/g, ',')
                  indexRow++

                  return true
                })
              }

              return true
            })

            content = _.map(content, (c, i) => {
              return {
                ...c,
                '0': chartData.labels[i].replace(/;/g, ',')
              }
            })
          } else {
            headers = ['Variable', 'Value']
            content = _.map(chartData.labels, (l, i) => {
              return [l.replace(/;/g, ','), chartData.isDuration? convertMinute(chartData.datasets[0].data[i], lang): chartData.datasets[0].data[i].toString().replace(/;/g, ',')]
            })
          }

          const fileTitle = `${translation.stats[config.translation]} ${from} - ${to}.csv`
          const csv = exportCSVFile(headers, content);
          zip.file(fileTitle, csv);
        }
      }
    }

    if (checked) {
      zip.generateAsync({type:"blob"})
        .then(function(content) {
          saveAs(content, `stats-${moment().format('x')}.zip`);
        });
    }

    $('#export_csv_modal').modal('hide');

    // OLD METHOD
    // Meteor.call('exportToCSV', {
    //   stats: exportStats,
    //   startDate: incidentDate.startDate,
    //   endDate: incidentDate.endDate,
    //   condoId: localStorage.getItem('condoIdSelected'),
    //   lang,
    // }, (err, res) => {
    //   if (err) sAlert.error(err)
    //   download(res.zip)
    //   $('#export_csv_modal').modal('hide')
    // })
  },
  'click .switch-option' (e, t) {
    console.log('e.currentTarget ', e.currentTarget)
    t.timeType.set($(e.currentTarget).data("type"))
  }
})

Template.modal_export_csv.helpers({
  getChecked: (key) => {
    const t = Template.instance()
    const value = t.exportStats.get(key)
    return value ? 'checked' : ''
  },
  getCheckedAll: () => {
    const t = Template.instance()
    const checks = t.exportStats.all()
    const unCheck = _.filter(checks, (u) => {
      return !u
    })

    return unCheck.length === 0 ? 'checked' : ''
  },
  cantExportStats: () => {
    const t = Template.instance()
    const values = _.values(t.exportStats.all())
    if (_.find(values, v => { return v })) {
      return false
    }
    return true
  },
  checkRightInStat: (key) => {
    const condoId = localStorage.getItem('condoIdSelected')
    if (key === '') { return true }
    switch (key) {
      case 'seeIncident':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('incident', 'see').length > 0 : Meteor.userHasRight('incident', 'see', condoId, Meteor.userId())
      case 'seeResources':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('reservation', 'seeResources').length > 0 : Meteor.userHasRight('reservation', 'seeResources', condoId, Meteor.userId())
      case 'seeReservation':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('reservation', 'seeAll').length > 0 : Meteor.userHasRight('reservation', 'seeAll', condoId, Meteor.userId())
      case 'seeMessenger':
        return condoId === 'all' ? Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise').length > 0 : Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, Meteor.userId())
      default:
        return false
    }
  },
  condoIsOffice: () => {
    const condoId = localStorage.getItem('condoIdSelected')
    if (condoId !== 'all') {
      const condo = Condos.findOne(condoId);

      return (!!condo && !!condo.settings && !!condo.settings.condoType && condo.settings.condoType === 'office')
    } else {
      const enterprise = Enterprises.findOne({ _id: Meteor.user().identities.gestionnaireId }, { fields: { users: true } })
      const mapUser = enterprise.users
      const findUser = _.find(mapUser, (elem) => {
        return elem.userId == Meteor.userId();
      })
      condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);

      const condoType = _.map(Condos.find({ "_id": { $in: condoIds } }).fetch(), (condo) => {
        return condo.settings.condoType;
      });

      return _.contains(condoType, 'office')
    }
  },
  getStatsToDisplay: () => {
    const t = Template.instance()
    return t.stats
  },
  isEven: (idx) => {
    if (idx % 2 === 0) {
      return 'background-color: #f1f5f6;'
    }
  },
  getActiveType: (type) => {
    const tpl = Template .instance()
    return tpl.timeType.get() === type ? 'active' : '';
  },
  hasSwitcher: (name) => {
    return name === 'bookedByTime' || name === 'resourcesDistribution'
  }
})
