import { Chart } from 'chart.js';
import { Stats, CommonTranslation, ContactManagement } from '/common/lang/lang.js'
import moment from 'moment';
import 'chartjs-plugin-datalabels';

let divisor = 0;
let myChart;
const chartContainer = 'messenger-chart-container';
const chartId = 'messenger-chart';
let tester_width;

function DayMessengers(messengers, start, end) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
	let divisor = 0;

	let receive = _.filter(messengers, function(msg) {
		return !GestionnaireUsers.findOne(msg.userId);
	});
	let send = _.filter(messengers, function(msg) {
		return GestionnaireUsers.findOne(msg.userId);
	})
	let receiveEachDay = _.countBy(_.flatten([ _.map(receive, function(msgOfGestionnaire) {
		return moment(msgOfGestionnaire.date).format("DD[/]MM[/]YY");
	}) ]));
	let sendEachDay = _.countBy(_.flatten([ _.map(send, function (msgOfOccupant) {
		return moment(msgOfOccupant.date).format("DD[/]MM[/]YY");
	}) ]));

	let tmp = 0;
	let duration = end.diff(start, 'days')
	for (var i = 0; i < duration; i++) {
		divisor = 0;
		let day = {
			types: {
				"send": sendEachDay[start.format("DD[/]MM[/]YY")] || 0,
				"receive": receiveEachDay[start.format("DD[/]MM[/]YY")] || 0,
				"time": 0,
			},
			name: start.locale(lang).format('ddd DD-MMM'),
			total: (receiveEachDay[start.format("DD[/]MM[/]YY")] || 0) + (sendEachDay[start.format("DD[/]MM[/]YY")] || 0)
		};
		let msgOfDay = _.filter(receive, function(response) { return moment(response.date).format("DD[/]MM[/]YY") == start.format("DD[/]MM[/]YY")});
		_.each(msgOfDay, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, {sort: {date: -1}});
			if (conv && GestionnaireUsers.findOne(conv.userId)) {
				day.types["time"] += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});
		day.types["time"] /= divisor;
		if (!day.types["time"]) {
			if (cols[i-1]) {
				day.types["time"] = cols[i-1].types["time"];
			}
		}
		day.types["time"] = day.types["time"] || 0; // for remove NaN;
		total += day.total;
		cols.push(day);
		start.add(1, "d");
	}
	return {cols: cols, total: total};
}

function WeekMessengers(messengers, start, end, lang) {
	let cols = [];
	let total = 0;
	let divisor = 0;

	let receive = _.filter(messengers, function(msg) {
		return !GestionnaireUsers.findOne(msg.userId);
	});
	let send = _.filter(messengers, function(msg) {
		return GestionnaireUsers.findOne(msg.userId);
	})
	let receiveEachWeek = _.countBy(_.flatten([ _.map(receive, function(msgOfGestionnaire) {
		return moment(msgOfGestionnaire.date).format("W YYYY");
	}) ]));
	let sendEachWeek = _.countBy(_.flatten([ _.map(send, function (msgOfOccupant) {
		return moment(msgOfOccupant.date).format("W YYYY");
    }) ]));

    const translation = new CommonTranslation(lang)

  let duration = end.diff(start, 'weeks')
  let tmpTime = start
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		divisor = 0;
		let week = {
			types: {
				"send": sendEachWeek[start.format("W YYYY")] || 0,
				"receive": receiveEachWeek[start.format("W YYYY")] || 0,
				"time": 0,
			},
			name: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
			total: (receiveEachWeek[start.format("W YYYY")] || 0) + (sendEachWeek[start.format("W YYYY")] || 0)
		};
		let msgOfWeek = _.filter(receive, function(response) { return moment(response.date).format("W YYYY") == start.format("W YYYY")});
		_.each(msgOfWeek, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, {sort: {date: -1}});
			if (conv && GestionnaireUsers.findOne(conv.userId)) {
				week.types["time"] += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});
		week.types["time"] /= divisor;
		if (!week.types["time"]) {
			if (cols[i-1]) {
				week.types["time"] = cols[i-1].types["time"];
			}
		}
		week.types["time"] = week.types["time"] || 0; // for remove NaN;
		total += week.total;
		cols.push(week);
		start.add(1, "weeks");
	}
	return {cols: cols, total: total};
}

function MonthMessengers(messengers, start, end) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
	let divisor = 0;

	let receive = _.filter(messengers, function(msg) {
		return !GestionnaireUsers.findOne(msg.userId);
  });
	let send = _.filter(messengers, function(msg) {
		return GestionnaireUsers.findOne(msg.userId);
	})
	let receiveEachMonth = _.countBy(_.flatten([ _.map(receive, function(msgOfGestionnaire) {
		return moment(msgOfGestionnaire.date).format("MM[/]YYYY");
  }) ]));
  // console.log('receiveEachMonth ', receiveEachMonth )
	let sendEachMonth = _.countBy(_.flatten([ _.map(send, function (msgOfOccupant) {
		return moment(msgOfOccupant.date).format("MM[/]YYYY");
	}) ]));
  // console.log('sendEachMonth ', sendEachMonth )

	let duration = end.diff(start, 'month')
	for (var i = 0; i <= duration; i++) {
		divisor = 0;
		let month = {
			types: {
				"send": sendEachMonth[start.format("MM[/]YYYY")] || 0,
				"receive": receiveEachMonth[start.format("MM[/]YYYY")] || 0,
				"time": 0,
			},
			name: !(start.format('YYYY') == moment().format('YYYY')) ? start.locale(lang).format('MMMM YYYY') :  start.locale(lang).format('MMMM'),
			total: (receiveEachMonth[start.format("MM[/]YYYY")] || 0) + (sendEachMonth[start.format("MM[/]YYYY")] || 0)
		};
		let msgOfMonth = _.filter(receive, function(response) { return moment(response.date).format("MM[/]YYYY") == start.format("MM[/]YYYY")});
		_.each(msgOfMonth, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, {sort: {date: -1}});
			if (conv && GestionnaireUsers.findOne(conv.userId)) {
				month.types["time"] += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});
		month.types["time"] /= divisor;
		if (!month.types["time"]) {
			if (cols[i-1]) {
				month.types["time"] = cols[i-1].types["time"];
			}
		}
		month.types["time"] = month.types["time"] || 0; // for remove NaN;
		total += month.total;
		cols.push(month);
		start.add(1, "M");
	}
	return {cols: cols, total: total};
}


function YearMessengers(messengers, start, end) {
  let lang = FlowRouter.getParam("lang") || "fr"
	let cols = [];
	let total = 0;
	let divisor = 0;

	let receive = _.filter(messengers, function(msg) {
		return !GestionnaireUsers.findOne(msg.userId);
  });
	let send = _.filter(messengers, function(msg) {
		return GestionnaireUsers.findOne(msg.userId);
	})
	let receiveEachYear = _.countBy(_.flatten([ _.map(receive, function(msgOfGestionnaire) {
		return moment(msgOfGestionnaire.date).format("YYYY");
	}) ]));
	let sendEachYear = _.countBy(_.flatten([ _.map(send, function (msgOfOccupant) {
		return moment(msgOfOccupant.date).format("YYYY");
	}) ]));

	let duration = end.diff(start, 'year')
	for (var i = 0; i <= duration; i++) {
		divisor = 0;
		let year = {
			types: {
				"send": sendEachYear[start.format("YYYY")] || 0,
				"receive": receiveEachYear[start.format("YYYY")] || 0,
				"time": 0,
			},
			name: start.format("YYYY"),
			total: (receiveEachYear[start.format("YYYY")] || 0) + (sendEachYear[start.format("YYYY")] || 0)
		};
		let msgOfYear = _.filter(receive, function(response) { return moment(response.date).format("YYYY") == start.format("YYYY")});
		_.each(msgOfYear, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, {sort: {date: -1}});
			if (conv && GestionnaireUsers.findOne(conv.userId)) {
				year.types["time"] += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});
		year.types["time"] /= divisor;
		if (!year.types["time"]) {
			if (cols[i-1]) {
				year.types["time"] = cols[i-1].types["time"];
			}
		}
		year.types["time"] = year.types["time"] || 0; // for remove NaN;
		total += year.total;
		cols.push(year);
		start.add(1, "y");
	}
	return {cols: cols, total: total};
}

function getMessages(start, end, condo, lang) {
	let res = {
		cols: [],
		total: 0
	};
	let conversationIds = [];
	let messengers = [];

	let duration  = 0;

	if (condo == "all") {
		conversationIds = _.map(Messages.find({
      target: 'manager'
		}).fetch(), function(conv) { return conv._id; });
	} else {
		conversationIds = _.map(Messages.find({
			condoId: condo,
      target: 'manager'
		}).fetch(), function(conv) { return conv._id; });
  }
  // console.log('conversationIds ', conversationIds )
  // console.log('parseInt(start.format(\'x\')) ', parseInt(start.format('x')))
  // console.log('parseInt(end.format(\'x\')) ', parseInt(end.format('x')) )

	messengers = MessagesFeed.find({
		$and: [
		{"messageId": {$in: conversationIds}},
		{"date": {$gte: parseInt(start.format('x')), $lt: parseInt(end.format('x'))}}
		]
	}).fetch();
  // console.log('messengers ', messengers )

	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearMessengers(messengers, start, end)
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthMessengers(messengers, start, end)
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekMessengers(messengers, start, end, lang)
	}
	else {
		res = DayMessengers(messengers, start, end)
  }
  console.log('res', res)
	return res;
}

export function buildDatasetMessenger(start, end, condo, type, lang) {
	// TODO change dummy data
	const data = getMessages(start, end, condo, lang);

	let label = [];
    let dataset = [];

    const translation = new CommonTranslation(lang)

	if (type === 'response') {
        dataset.push({
            label: translation.commonTranslation["average_response_time"],
            backgroundColor: '#88898e',
            borderColor: '#88898e',
            fill: false,
            pointRadius: 0,
            pointHitRadius: 10,
            data: _.map(data.cols, function(col) { return parseFloat(col.types["time"].toFixed(1)) || 0; }),
            yAxisID: 'line',
            type: 'line',
            datalabels: {
                display: false
            }
        });
	}

	if (type === 'send-receive') {
        dataset.push({
            label: translation.commonTranslation["message_received"],
            backgroundColor: "#69d2e7",
            borderColor: 'white',
            borderWidth: 1,
            data: _.map(data.cols, function(col) { return col.types["receive"]; }),
            yAxisID: 'bar',
            datalabels: {
                display: false
            }
        });

        dataset.push({
            label: translation.commonTranslation["message_sent"],
            backgroundColor: "#fdb813",
            borderColor: 'white',
            borderWidth: 1,
            data: _.map(data.cols, function(col) { return col.types["send"]; }),
            yAxisID: 'bar',
            datalabels: {
                display: false
            }
        });

        dataset.push({
            label: '',
            backgroundColor: 'transparent',
            borderColor: 'transparent',
            data: _.map(data.cols, function(col) { return col.total; }),
            yAxisID: 'bar',
            type: 'line',
            datalabels: {
                color: '#88898e',
                align: 'end'
            }
        });

        dataset.push({
            label: '',
            backgroundColor: '#88898e',
            borderColor: '#88898e',
            data: _.map(data.cols, function(col) { return 1 }),
            yAxisID: 'littlebar',
            type: 'bar',
            datalabels: {
                display: false
            }
        });
	}

	return {
		labels: _.map(data.cols, function(col) { return col.name; }),
		datasets: dataset
	}
}

function convertMinute(value, lang) {
  const translate = new ContactManagement(lang)
  const common = new CommonTranslation(lang)

  if (parseInt(value) >= 10080) {
    if (parseInt(value) % 10080 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s)]';
    else
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 10080) + ' ' + common.commonTranslation['week'] + '(s) ' + parseInt((parseInt(value) % 10080) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
  }
  else if (parseInt(value) >= 1440) {
    if (parseInt(value) % 1440 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s)]';
    else
      value =  parseInt(parseInt(value) / 60) + 'h [' + parseInt(parseInt(value) / 1440) + ' ' + translate.contactManagement['day'].toLowerCase() + '(s) ' + parseInt((parseInt(value) % 1440) / 60) + 'h]';
  }
  else if (parseInt(value) >= 60) {
    if (parseInt(value) % 60 === 0)
      value =  parseInt(parseInt(value) / 60) + 'h';
    else
      value =  parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
  }
  else if (parseInt(value) !== 0) {
    value =  parseInt(value) + 'min';
  }

  return value
}

function renderChartMessenger(start, end, condo, type, lang, instance) {

  let ctx = document.getElementById(chartId).getContext('2d');
	$('#messenger-chart-no-data').hide();
  let data = buildDatasetMessenger(start, end, condo, type, lang);

  if (typeof instance.data.dataCallback === 'function') {
    instance.data.dataCallback('messenger', data, type === 'response')
  }
  const tr_common = new CommonTranslation(lang)

	let tester = [];
	_.each(data.datasets, function(d) {
		tester.push(_.filter(d.data, function (data_) {
			return data_ != 0;
		}))
	});

	tester = tester.filter(function(test) {return test != 0});

	if (tester.length === 1 && type !== 'response') {
		$('#messenger-chart-no-data').show();
	}
    myChart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            defaultFontColor: "#8b999f",
            responsive: true,
            maintainAspectRatio: false,
            plugins: {
                datalabels: {
                    enabled: true,
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] > 0;
                    },
                    font: {
                        weight: 'bold'
                    }
                },
            },
            layout: {
                padding: {
                    left: 20,
                    right: 0,
                    top: 20,
                    bottom: 0
                }
            },
            scales: {
                yAxes: [{
                    id: 'bar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }, {
                    id: 'line',
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                    	display: (type === 'response'),
                        padding: 10,
                        beginAtZero: true,
                        fontColor: "#8b999f",
                        maxRotation: 0,
                        callback: function(value, index, values) {
                            if (value >= 1440) {
                                if (value % 1440 == 0)
                                    return parseInt(value / 1440) + tr_common.commonTranslation["day_short"];
                                else {
                                    if (value % 60 == 0) {
                                        return parseInt(value / 1440) + tr_common.commonTranslation["day_short"];
                                    }
                                    return parseInt(value / 1440) + tr_common.commonTranslation["day_short"] + ' ' + parseInt(value % 1440 / 60) + 'h';
                                }
                            }
                            else if (value >= 60) {
                                if (value % 60 == 0)
                                    return parseInt(value / 60) + 'h';
                                else
                                    return parseInt(value / 60) + 'h' + parseInt(value % 60);
                            }
                            else if (value >= 1)
                                return parseInt(value) + ' min';
                        }
                    },
                    type: 'linear',
                    position: 'left',
                }, {
                    id: 'littlebar',
                    display: false,
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        beginAtZero: true,
                        max: 50,
                        autoSkip: false,
                    },
                    type: 'linear',
                    position: 'right',
                }],

                xAxes: [{
                    categorySpacing: 16,
                    afterSetDimensions: (axis) => {
                        setTimeout(function() {
                            $('#x-axis-arrow-messenger').css("top", (axis.top + 55) + "px");
                            $('#x-axis-arrow-messenger').css("left", (axis.right + 45) + "px");
                            $('#x-axis-arrow-messenger').css("opacity", 1);
                        }, 50);
                    },
                    stacked: true,
                    gridLines: {
                        display: false,
                        color: "#e0e0e0",
                        lineWidth: 2,
                        zeroLineWidth: 2,
                        zeroLineColor: "#e0e0e0",
                        drawTicks: true,
                        tickMarkLength: 3
                    },
                    ticks: {
                        padding: 10,
                        autoSkip: false,
                        fontColor: "#8b999f",
                        maxRotation: 0,
                        callback: function(value, index, values) {
                            let val = value.split(' ');
                            if (values.length > 10) {
                                if (index % (parseInt(values.length / 10) + 1) == 0) {
                                    if (val.length == 6) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + ' ' + val[3],
                                            val[4] + ' ' + val[5]
                                        ];
                                    }
                                    if (val.length == 4) {
                                        return [
                                            val[0] + ' ' + val[1],
                                            val[2] + '-' + val[3]
                                        ];
                                    }
                                    if (val.length == 3) {
                                        return [
                                            val[0].substring(0, val[0].length - 1),
                                            val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                        ];
                                    }
                                    else if (val.length == 2) {
                                        return [
                                            val[0],
                                            val[1]
                                        ]
                                    }
                                    else if (val.length == 1) {
                                        return val[0];
                                    }
                                }
                            }
                            else {
                                if (val.length == 6) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + ' ' + val[3],
                                        val[4] + ' ' + val[5]
                                    ];
                                }
                                else if (val.length == 4) {
                                    return [
                                        val[0] + ' ' + val[1],
                                        val[2] + '-' + val[3]
                                    ];
                                }
                                else if (val.length == 3) {
                                    return [
                                        val[0].substring(0, val[0].length - 1),
                                        val[1] + ' ' + val[2].substring(0, val[0].length - 1)
                                    ];
                                }
                                else if (val.length == 2) {
                                    return [
                                        val[0],
                                        val[1]
                                    ]
                                }
                                else if (val.length == 1) {
                                    return val[0];
                                }
                            }
                        }
                    }
                }]
            },
            tooltips: {
                enabled: false,
                custom: function(tooltipModel) {
                    if (!!(tooltipModel &&  tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined))
                        return ;
                    // Tooltip Element
                    var tooltipEl = document.getElementById('chartjs-messenger-chart-tooltips');

                    // Create element on first render
                    if (!tooltipEl) {
                        tooltipEl = document.createElement('div');
                        tooltipEl.id = 'chartjs-messenger-chart-tooltips';
                        tooltipEl.innerHTML = "<table></table>"
                        document.body.appendChild(tooltipEl);
                    }

                    // Hide if no tooltip
                    if (tooltipModel.opacity === 0) {
                        tooltipEl.style.opacity = 0;
                        return;
                    }

                    // Set caret Position
                    tooltipEl.classList.remove('above', 'below', 'no-transform');
                    if (tooltipModel.yAlign) {
                        tooltipEl.classList.add(tooltipModel.yAlign);
                    } else {
                        tooltipEl.classList.add('no-transform');
                    }

                    function getBody(bodyItem) {
                        return bodyItem.lines;
                    }

                    // Set Text
                    if (tooltipModel.body) {
                        var titleLines = tooltipModel.title || [];
                        var bodyLines = tooltipModel.body.map(getBody);

                        var innerHtml = '<thead>';

                        titleLines.forEach(function(title) {
                            innerHtml += '<tr><th></th></tr>';
                        });
                        innerHtml += '</thead><tbody>';
                        bodyLines.forEach(function(body, i) {
                            if (body[0].split(': ')[1]) {
                                let value = parseInt(body[0].split(': ')[1]);

                                // sorry I can't search with other variable ... It's for display hour.
                                if (tooltipModel.labelColors[0].backgroundColor == "rgb(121, 123, 134)") {
                                  value = convertMinute(value, lang)
                                }

                                var colors = tooltipModel.labelColors[i];
                                var style = 'background:' + colors.backgroundColor;
                                style += '; border-color:' + colors.borderColor;
                                style += '; border-width: 2px';
                                var div = '';
                                innerHtml += '<tr style="border-top: '+colors.backgroundColor+' solid 2px"><td style="padding: '+tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'+'">' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
                            }
                        });
                        innerHtml += '</tbody>';

                        var tableRoot = tooltipEl.querySelector('table');
                        tableRoot.innerHTML = innerHtml;
                    }

                    // `this` will be the overall tooltip
                    var position = this._chart.canvas.getBoundingClientRect();

                    // Display, position, and set styles for font
                    tooltipEl.style.opacity = 1;
                    tooltipEl.style.left = (tooltipModel.caretX + 15) + 'px';
                    tooltipEl.style.top = (tooltipModel.caretY - 30) + 'px';
                    // tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
                }
            },
            legend: false
        }
    });

  instance.isLoading.set(false)
    // $("#chartjs-"+chartId+"-legend").html(myChart.generateLegend());
}

Template.messengers_stats.onCreated(function() {
  this.isLoading = new ReactiveVar(true)
  let lang = FlowRouter.getParam("lang") || "fr"
	let translation = {stats: new Stats(lang)}
	this.messengerFilter = new ReactiveVar({
		startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
		endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').locale(lang),
	});
    this.autorun(() => {
        this.messengerFilter.set({
            startDate: moment(Session.get('startDate'), 'DD[/]MM[/]YY').locale(lang),
            endDate: moment(Session.get('endDate'), 'DD[/]MM[/]YY').local(lang),
        })
    });
	this.selectedType = new ReactiveVar('send-receive');
	this.options = [
        {
            type: 'send-receive',
            name: translation.stats.stats["sent_received"]
        }, {
            type: 'response',
            name: translation.stats.stats["response"]
        }
	]
});

Template.messengers_stats.onRendered(function() {
});

Template.messengers_stats.events({
    'click .typeOption' (e, t) {
        t.selectedType.set(e.currentTarget.getAttribute('filter'));
    }
});

Template.messengers_stats.onDestroyed(function() {
});

Template.messengers_stats.helpers({
	renderChartMessenger : () => {
	  const instance = Template.instance()
		const filter = instance.messengerFilter.get();
    const condo = Template.currentData().condoId
    const type = instance.selectedType.get();
    let lang = FlowRouter.getParam("lang") || "fr"
		if (Template.currentData().isReady) {
			setTimeout(() => {
				$('#'+chartContainer + ' .chartjs-size-monitor').remove();
				$('#'+chartId).remove();
				$('#'+chartContainer).append('<canvas id="'+chartId+'" height="340"><canvas>');
				renderChartMessenger(filter.startDate.clone(), filter.endDate.clone(), condo, type, lang, instance);
			}, 800);
		}
	},
    getOptions: () => {
		return Template.instance().options
	},
    selectedFilter: () => {
		const instance = Template.instance();
		return _.find(instance.options, (o) => {return o.type === instance.selectedType.get()}).name;
	},
    ready: () => {
        return !!Template.instance().data.isReady;
    },
    getActiveClass: (type) => {
		return Template.instance().selectedType.get() === type ? 'active' : ''
	},
  loadingData: () => {
    return Template.instance().isLoading.get()
  }
});
