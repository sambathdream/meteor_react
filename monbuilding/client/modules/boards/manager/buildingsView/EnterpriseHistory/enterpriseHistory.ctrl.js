const regexpSearch = {
  'a': '[a|à|â|ä]',
  'e': '[e|é|è|ê|ë]',
  'i': '[i|î|ï|ì]',
  'o': '[o|ô|ö|ò]',
  'u': '[u|ù|ü|û]',
  'y': '[y|ý|ŷ|ÿ|ỳ]'
};

Template.enterpriseHistory.onCreated(function () {
  this.subscribe('enterpriseHistory', Meteor.user().identities.gestionnaireId, 15)
  this.counter = 15
  this.hasReachedEnd = false
  this.searchHistory = new ReactiveVar('')
  this.actions = [
    'deleteEvent',
    'deleteInfo',
    'deleteManual',
    'deleteMap',
    'deletedResource',
    'deletedOccupant',
    'deletedManager',
    'removedContact',
    'removedEmergencyContact',
  ]
  this.actionsWithName = [
    'addedContact',
    'removedContact',
    'addedEmergencyContact',
    'removedEmergencyContact',
  ]
})

Template.enterpriseHistory.onRendered(function () {
})

Template.enterpriseHistory.events({
  'input #historySearch': function (event, template) {
    let input = event.currentTarget.value
    for (let i = 0; i != Object.keys(regexpSearch).length; i++) {
      input = input.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]])
    }
    template.searchHistory.set(input)
  },
  'scroll .body': function (event, template) {
    if (template.hasReachedEnd == true)
      return
    if (event.currentTarget.offsetHeight + event.currentTarget.scrollTop == event.currentTarget.scrollHeight) {
      template.counter += 15;
      template.subscribe('enterpriseHistory', Meteor.user().identities.gestionnaireId, template.counter)
    }
  },
  'click .goToElemModule': (event, template) => {
    const lang = FlowRouter.getParam('lang') || 'fr';
    const action = $(event.currentTarget).data('action')
    const condoId = $(event.currentTarget).data('condoid')
    const module = $(event.currentTarget).data('module')
    const modulePostId = $(event.currentTarget).data('modulepostid')
    if (!!!_.find(template.actions, a => { return a === action })) {
      if (module === 'info') {
        FlowRouter.go('app.gestionnaire.informations.infoId', {
          lang,
          infoId: modulePostId
        });
      } else if (module === 'event') {
        FlowRouter.go('app.gestionnaire.evenements.eventId', {
          lang,
          eventId: modulePostId
        });
      } else if (module === 'messenger') {
        FlowRouter.go("app.gestionnaire.messenger.detail", { lang, tab: 'enterprise', msgId: modulePostId });
      } else if (module === 'incident') {
        FlowRouter.go('app.gestionnaire.incidents.id', { lang, incidentId: modulePostId })
      } else if (module === 'manual') {
        FlowRouter.go('app.gestionnaire.documents', { lang })
      } else if (module === 'map') {
        FlowRouter.go('app.gestionnaire.documents', { lang })
      } else if (module === 'ads') {
        FlowRouter.go('app.gestionnaire.classifieds.condo', {
          lang,
          condo_id: condoId
        }, { ad: modulePostId })
      } else if (module === 'forum') {
        FlowRouter.go('app.gestionnaire.forum.postId', {
          lang,
          condo_id: condoId,
          postId: modulePostId
        })
      } else if (module === 'reservation') {
        FlowRouter.go('app.gestionnaire.planningid', {
          lang,
          condo_id: condoId,
          eventId: {
            _str: modulePostId
          }
        })
      } else if (module === 'trombi') {
        FlowRouter.go('app.gestionnaire.occupantList.occupantProfile', {
          lang,
          tab: 'buildingsOccupant',
          userId: modulePostId
        })
      } else if (module === 'managerList') {
        FlowRouter.go('app.gestionnaire.managerList.managerProfile', {
          lang,
          tab: 'buildings',
          userId: modulePostId
        })
      } else if (module === 'setContact') {
        FlowRouter.go('app.gestionnaire.contactManagement.tab', {
          lang,
          tab: 'messenger',
          condoId
        })
      } else if (module === 'emergencyContact') {
        FlowRouter.go('app.gestionnaire.emergencyContact.detail', {
          tab: 'details',
          condo_id: condoId,
          contactId: modulePostId
        })
      }
    }
  },
})

Template.enterpriseHistory.helpers({
  getUserNameShort: (userId) => {
    let user = UsersProfile.findOne(userId)
    if (!user)
      return 'Unknown'
    else if (!user.firstname)
      return 'Unknown'
    let name = user.firstname[0] + ". " + user.lastname;
    return name.toUpperCase();
  },
  getHistory: () => {
    let condosInCharge = Template.currentData().condosInCharge
    if (condosInCharge === undefined) {
      return
    }
    let condos = Condos.find({ _id: { $in: condosInCharge } }).fetch()
    let cf = Template.currentData().condoFilter

    let history = []
    if (cf === 'all') {
      history = EnterpriseHistory.find({}, { sort: { date: -1 } }).fetch()
    } else {
      history = EnterpriseHistory.find({ condoId: cf }, { sort: { date: -1 } }).fetch()
    }

    if (history === undefined || history.length === 0) {
      return false
    }
    let searchHistory = Template.instance().searchHistory.get()
    if (history.lenght < Template.instance().hasReachedEnd) {
      Template.instance().hasReachedEnd = true
    }
    history = _.filter(history, (elem) => {
      let user = UsersProfile.findOne(elem.userId)
      if (!user) {
        return false
      } else {
        if (!searchHistory)
          return elem
        let regexp = new RegExp(searchHistory, "i")
        if (user) {
          let thisCondo = _.find(condos, function (condo) { return condo._id == elem.condoId }).getNameWithAddress()
          let firstTest = elem.detail.match(regexp) || user.firstname.match(regexp)
            || user.lastname.match(regexp) || (user.firstname + ' ' + user.lastname).match(regexp)
            || (user.lastname + ' ' + user.firstname).match(regexp) || thisCondo.match(regexp)
          let lang = FlowRouter.getParam('lang') || 'fr'
          let value = 'value-' + lang
          let actionVal = EnterpriseHistoryAction.findOne({ key: elem.action, [value]: regexp })
          let secondTest = false
          if (actionVal !== undefined && actionVal) {
            secondTest = true
          }
          return firstTest || secondTest
        }
      }
    })
    return history
  },
  getHistoryAction: (action, modulePostId, detail) => {
    const tpl = Template.instance()

    let lang = FlowRouter.getParam('lang') || 'fr'
    let actionVal = EnterpriseHistoryAction.findOne({ key: action })
    if (tpl.actionsWithName.includes(action)) {
      let user = Meteor.users.findOne({ _id: modulePostId })
      // console.log('user ', user )
      let contactName = ''
      if (user) {
        contactName = user.profile.firstname + " " + user.profile.lastname
      }
      if (actionVal !== undefined && actionVal) {
        return contactName + ' ' + actionVal['value-' + lang] + " " + detail
      }
    } else {
      if (actionVal !== undefined && actionVal) {
        return actionVal['value-' + lang]
      }
    }
  },
  canSeeElem: (elem) => {
    if (!elem) return false
    switch (elem.module) {
      case 'info':
      case 'event':
        return Meteor.userHasRight('actuality', 'see', elem.condoId, Meteor.userId())
      case 'messenger':
        return Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', elem.condoId, Meteor.userId())
      case 'incident':
        return Meteor.userHasRight('incident', 'see', elem.condoId, Meteor.userId())
      case 'manual':
      case 'map':
        return Meteor.userHasRight("view", "documents")
      case 'ads':
        return Meteor.userHasRight('ads', 'see', elem.condoId, Meteor.userId())
      case 'forum':
        return Meteor.userHasRight('forum', 'seePoll', elem.condoId, Meteor.userId()) && Meteor.userHasRight('forum', 'seeSubject', elem.condoId, Meteor.userId())
      case 'reservation':
        return Meteor.userHasRight('reservation', 'see', elem.condoId, Meteor.userId())
      case 'trombi':
        return Meteor.userHasRight('trombi', 'see', elem.condoId, Meteor.userId())
      case 'managerList':
        return Meteor.userHasRight('managerList', 'seeManager', elem.condoId, Meteor.userId())
      case 'setContact':
        return Meteor.userHasRight('setContact', 'seeMessenger', elem.condoId, Meteor.userId())
      case 'emergencyContact':
        return Meteor.userHasRight('emergencyContact', 'see', elem.condoId, Meteor.userId())
      default:
        break;
    }
  },
  getDeclarationName(declarationDetailId) {
    let lang = (FlowRouter.getParam("lang") || "fr");
    let detail = DeclarationDetails.findOne(declarationDetailId);
    if (detail)
      return detail["value-" + lang];
  },
  showDetail(action) {
    const tpl = Template.instance()

    return !(tpl.actionsWithName.includes(action))
  },
  canGoToElem(action) {
    const t = Template.instance()
    return !!!_.find(t.actions, a => { return a === action })
  },
  getCondoNameWithId: (condoId) => {
    const condo = Condos.findOne({ _id: condoId })

    return condo.getNameWithId()
  }
})
