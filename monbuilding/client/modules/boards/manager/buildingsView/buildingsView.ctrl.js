import { Template } from 'meteor/templating';
import { Chart } from 'chart.js';
import { setTimeout } from 'timers';
import { BuildingsView } from '/common/lang/lang.js';

let donutsValue = {
	nbvalid: null,
	nbencours: null,
	nbresolu: null
}

function isSameData(nbvalid, nbencours, nbprives) {
	if (donutsValue.nbvalid == nbvalid && donutsValue.nbencours == nbencours && donutsValue.nbprives == nbprives)
  return true;
	donutsValue.nbvalid = nbvalid;
	donutsValue.nbencours = nbencours;
	donutsValue.nbprives = nbprives;
	return false;
}

/* Donut chart */
function create_donut(dataset, instance, render) {
	// check for empty state incident
	const sum = _.reduce(dataset, (memo, d) => {
    return memo + parseInt(d.value)
  }, 0);
	if (sum === 0) {
		instance.incidentEmpty.set(true);
	} else {
    instance.incidentEmpty.set(false);
	}

	if (render) {
    $('#pie').remove();
    $('#pie-chart').append('<canvas id="pie" width="132" height="132"><canvas>');
    let myChart = new Chart($('#pie'), {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            dataset[0].value,
            dataset[1].value,
            dataset[2].value
          ],
          backgroundColor: [
            dataset[0].color,
            dataset[1].color,
            dataset[2].color
          ],
          hoverBorderColor: [
            dataset[0].color,
            dataset[1].color,
            dataset[2].color
          ],
          borderColor: "white"
        }],
        labels: [
          dataset[0].name,
          dataset[1].name,
          dataset[2].name
        ],
      },
      options: {
        animation: {
          animateScale: true
        },
        // cutoutPercentage: 60,
        maintainAspectRatio: true,
        responsive: true,
        rotation: 0.5 * Math.PI,
        legend: false,
        tooltips: {
          enabled: true,
        },
        // pieceLabel: {
        // 	render: 'value',
        // 	fontSize: 18,
        // 	fontStyle: 'bold',
        // 	fontColor: '#ffffff',
        // },
        plugins: {
          datalabels: {
            enabled: false,
            display: false,
          },
        },
      }
    });
    $("#the-legend").html(myChart.generateLegend());
	}
}

$(function () {
	$('[data-toggle="popover"]').popover();
});


// table click msg function starts here

$('.msg-trigger').click(function(e) {
	e.preventDefault();
	$(this).parent().parent().next('tr.msg-box').toggle();
});
// table click msg function ends here
// Input number spinner starts here
$(function(){
	$('.spinner .btn:first-of-type').on('click', function() {
		let btn = $(this);
		let input = btn.closest('.spinner').find('input');
		if (input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
			input.val(parseInt(input.val(), 10) + 1);
		} else {
			btn.next("disabled", true);
		}
	});
	$('.spinner .btn:last-of-type').on('click', function() {
		let btn = $(this);
		let input = btn.closest('.spinner').find('input');
		if (input.attr('min') === undefined || parseInt(input.val()) > parseInt(input.attr('min'))) {
			input.val(parseInt(input.val(), 10) - 1);
		} else {
			btn.prev("disabled", true);
		}
	});

});

const regexpSearch = {
	'a': '[a|à|â|ä]',
	'e': '[e|é|è|ê|ë]',
	'i': '[i|î|ï|ì]',
	'o': '[o|ô|ö|ò]',
	'u': '[u|ù|ü|û]',
	'y': '[y|ý|ŷ|ÿ|ỳ]'
};

Template.buildingsView.onRendered(function() {
	setInterval(function() {
		$('.d-results').height($("#left-panel").height() - 117)
	}, 1000)
});

Template.buildingsView.onCreated(function() {
	this.condosWithBuildingViewRight = new ReactiveVar([]);
	this.selectedTab = new ReactiveVar("gestion"); /* gestion, actualite */
	this.adaptHeight = new ReactiveVar(false);
	this.type = new ReactiveVar("actu");
	this.searchCondo = new ReactiveVar("");
	this.searchHistory = new ReactiveVar("");
	this.filterYear = new ReactiveVar(new Date().getFullYear());
	this.filterMonth = new ReactiveVar(new Date().getMonth());
	this.filterDay = new ReactiveVar(null);
	this.showPastille = new ReactiveVar(true);
	this.sortInfo = new ReactiveVar(true);
	this.loadModal = new ReactiveVar(true);
	this.incidentEmpty = new ReactiveVar(true);
  this.counter = 20;
  this.hasReachedEnd = false;
  this.chartReady = new ReactiveVar(false);
  let handler = this.subscribe("module_incidents_gestionnaire");
	let self = this;

  Tracker.autorun(() => {
    if (handler.ready()) {
      self.chartReady.set(true);
		}
	});
	this.subscribe("messagerie");

	this.subscribe("resources");

	this.subscribe("actus_gestionnaire", this.counter);
});

Template.buildingsView.events({
	'click .tabGestion': function(event, template) {
		template.selectedTab.set(event.currentTarget.getAttribute("tab"));
	},
	'click .publishView': function(event, template) {
		const type = event.currentTarget.getAttribute('type');
		template.type.set(type);
		template.loadModal.set(!template.loadModal.get());
	},
	'click .buttonCondoView': function(event, template) {
		$('#inputSearch').val("");
		template.searchCondo.set("");
	},
	'input #inputSearch': function(event, template) {
		let input = event.currentTarget.value;
		for (let i = 0; i != Object.keys(regexpSearch).length; i++) {
			input = input.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]]);
		}
		template.searchCondo.set(input);
	},
	'click .date-link': function(event, template) {
		template.sortInfo.set(!template.sortInfo.get());
	},
	'click #backToMonth': function(event, template) {
		template.filterDay.set(null);
	},
  'click #goMessenger': function (e, t) {
    e.preventDefault()
		const lang =  FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.messenger', { lang, tab: 'global' })
	},
  'click #goIncidents': function(e, t) {
    const lang =  FlowRouter.getParam('lang') || 'fr';
    FlowRouter.go('app.gestionnaire.incidents', { lang });
  },
  'click #goOccupant': function(e, t) {
    const lang =  FlowRouter.getParam('lang') || 'fr';
    FlowRouter.go('app.gestionnaire.occupantList.tab', { lang, tab: 'active' });
  },
  'click #goResa': function(e, t) {
    const lang = FlowRouter.getParam('lang') || 'fr';
    FlowRouter.go('app.gestionnaire.planning', { lang });
  },

	'click .goToEvent': function(event, template) {
		const lang = FlowRouter.getParam('lang') || 'fr';
		FlowRouter.go('app.gestionnaire.evenements.eventId', {
			lang,
			eventId: $(event.currentTarget).data("eventid")
		});
	},
	'click .goToInfo': function(event, template) {
		const lang = FlowRouter.getParam('lang') || 'fr';
		FlowRouter.go('app.gestionnaire.informations.infoId', {
			lang,
			infoId: $(event.currentTarget).data("id")
		});
	},
  'scroll .info-scroller': function (event, template) {
    if (template.hasReachedEnd == true)
    return
    if (event.currentTarget.offsetHeight + event.currentTarget.scrollTop == event.currentTarget.scrollHeight) {
      template.counter += 15;
      template.subscribe('actus_gestionnaire', template.counter)
    }
  }
});

Template.buildingsView.helpers({
	getUserNameShort: (userId) => {
		let user = Meteor.users.findOne(userId);
		if (!user)
    user = GestionnaireUsers.findOne(userId);
		if (!user)
    return ;
		else if (!user.profile || !user.profile.firstname)
    return ;
		let name = user.profile.firstname.substr(0, 1) + ". " + user.profile.lastname;
		return name.toUpperCase();
	},
	getCondoFilter: () => {
    return Template.currentData().selectedCondoId
	},
	selectedTab: () => {
		return (Template.instance().selectedTab.get());
	},
	condos: () => {
		return (Condos.find().fetch());
	},
	getType: () => {
		return Template.instance().type.get();
	},
	getCondoList: () => {
		const gestionnaire = Meteor.user();
		if (gestionnaire) {
      const enterprise = Enterprises.findOne({ _id: gestionnaire.identities.gestionnaireId }, { fields: { users: true } })
      const mapUser = enterprise.users
      const findUser = _.find(mapUser, function (elem) {
        return elem.userId == Meteor.userId();
      })
      const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);
			if (condoIds) {
				const condos = Condos.find({'_id': {$in: condoIds}}).fetch();
				if (condos) {
					return Template.instance().searchCondo.get() ? _.filter(condos, function(elem) {
						let regexp = new RegExp(Template.instance().searchCondo.get(), "i");
						return elem.name.match(regexp);
					}) : condos;
				}
			}
		}
		let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
		return (Condos.find({$or : [{name: regexp}, {"info.id": regexp}, {"info.address": regexp}, {"info.code": regexp}, {"info.city": regexp}]}).fetch());
	},
	getActu: () => {
		const instance = Template.instance();

		return ActuPosts.find({
      $and: [
        {condoId: {$in: Template.currentData().selectedCondoId === 'all'? Meteor.listCondoUserHasRight("actuality", "see"): [Template.currentData().selectedCondoId]}},
        {
          $or: [
            {startDate: {$eq: ''}},
            {
              $and: [
                {startDate: {$ne: ''}},
                {startUtcTimestamp: {$gt: Date.now()}}
              ]
            }
          ]
        }
      ]
    }).fetch();
	},
	sameDate: (startDate, endDate) => {
		if (startDate && endDate) {
			return new Date(startDate.split('/')[2], startDate.split('/')[1] - 1, startDate.split('/')[0]).valueOf() ==
			new Date(endDate.split('/')[2], endDate.split('/')[1] - 1, endDate.split('/')[0]).valueOf();
		}
	},
	getCondoName: (condoId) => {
		let condo = Condos.findOne(condoId);
		if (condo) {
			if (condo.info.id != -1)
      return condo.info.id + ' - ' + condo.getName();
			else
      return condo.getName();
		}
	},
	loadModal: () => {
		return Template.instance().loadModal.get();
	},
	linkedCondo: (elem) => {
		const condo = Condos.findOne(elem.condoId);
		if (condo) {
			return condo;
		}
	},
	linkedCondoId: (elem) => {
		const condo = Condos.findOne(elem.condoId);
		if (condo && condo.info.id != -1) {
			return condo.info.id + ' - '
		}
	},
	filterDay: () => {
		return Template.instance().filterDay.get();
	},
	selectedDate: () => {
		return `${(Template.instance().filterDay.get() < 10 ? '0' + Template.instance().filterDay.get() : Template.instance().filterDay.get())}/${(Template.instance().filterMonth.get() + 1 < 10 ? '0' + (Template.instance().filterMonth.get() + 1) : Template.instance().filterMonth.get() + 1)}/${Template.instance().filterYear.get()}`;
	},
	newMessages: () => {
    const condoId = Template.currentData().selectedCondoId;
		const badges = BadgesGestionnaire.find(condoId === 'all' ? {} : { _id: condoId }).fetch();
		let nb = 0
		for (const badge of badges) {
			nb += parseInt(badge.messenger)
		}
		return nb
	},
	CDD_cursor: () => {
		return Template.instance().condosWithBuildingViewRight.get();
	},
	isEnableAccount: () => {
		const user = Meteor.user();
		if (user && user.identities) {
			const gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
			if (gestionnaire) {
				return !gestionnaire.inactive;
			}
		}
	},
	createBeautifulDonut: (render) => {
		const instance = Template.instance();
    const condosEnable = Meteor.listCondoUserHasRight("view", "buildingView");
    instance.condosWithBuildingViewRight.set(condosEnable);

    let condoId = Template.currentData().selectedCondoId
    let translation = new BuildingsView((FlowRouter.getParam("lang") || "fr"));

    let nbvalid = Incidents.find({"state.status": 0, condoId: condoId === 'all' ? {$in: condosEnable}: condoId}).count();
    let nbencours = Incidents.find({"state.status": 1, condoId: condoId === 'all' ? {$in: condosEnable}: condoId}).count();
    let nbprives = Incidents.find({"state.status": 3, condoId: condoId === 'all' ? {$in: condosEnable}: condoId}).count();

    if (isSameData(nbvalid, nbencours, nbprives) && $("#donut")[0] != undefined)
    return;
    const color = Session.get('currentColorPalette');
    let dataset = [
      {"name": translation.buildingsView['incidentsToValidate'], "color": !(nbvalid + nbencours + nbprives) ? "#d3d3d3" : (!!color && !!color.secondary ? color.secondary : "#fe0900"), "percent": !(nbvalid + nbencours + nbprives) ? 1 : nbvalid * 100 / (nbvalid + nbencours + nbprives), "value": nbvalid},
      {"name": translation.buildingsView['incidentsProgress'], "color": !(nbvalid + nbencours + nbprives) ? "#d3d3d3" : (!!color && !!color.primary ? color.primary : "#69d2e7"), "percent": !(nbvalid + nbencours + nbprives) ? 0 : nbencours * 100 / (nbvalid + nbencours + nbprives), "value": nbencours},
      {"name": translation.buildingsView['incidentsPrivate'], "color": !(nbvalid + nbencours + nbprives) ? "#d3d3d3" : "#84898f", "percent": !(nbvalid + nbencours + nbprives) ? 0 : nbprives * 100 / (nbvalid + nbencours + nbprives), "value": nbprives},
    ];
    setTimeout(function() {
      create_donut(dataset, instance, render);
    }, 500);
	},
	allReady: () => {
		if (Template.instance().subscriptionsReady() == true) {
			let condosEnable = Meteor.listCondoUserHasRight("view", "buildingView");
			Template.instance().condosWithBuildingViewRight.set(condosEnable);
			return true;
		}
		return false;
	},
  isNotRestricted: () => {
    const condoId = Template.currentData().selectedCondoId
    if (condoId === 'all') {
      return !!Meteor.listCondoUserHasRight("view", "buildingView").length
    } else {
      return !!Meteor.userHasRight('view', 'buildingView', condoId)
    }
	},
	needValidation: () => {
		let needValidation = false;
    const resources = Resources.find(Template.currentData().selectedCondoId !== 'all' ? { condoId: Template.currentData().selectedCondoId } : {}).fetch();
		_.each(resources, (r) => {
			if (r.needValidation && r.needValidation.status === true && r.needValidation.managerIds && r.needValidation.managerIds.find((u) => u === Meteor.userId())) {
				needValidation = true
			}
		})

		return needValidation;
	},
	getBadges: () => {
    const condoId = Template.currentData().selectedCondoId
    const badges = BadgesGestionnaire.find(condoId === 'all' ? {} : {_id: condoId}).fetch();
    let data = {
      resa: 0,
			occupant: 0
		};

    if (!!badges) {
      _.each(badges, (b) => {
        data.resa += isInt(b['reservation_pending']) ? parseInt(b['reservation_pending']): 0;
        data.occupant += parseInt(b['newUser']);
      });
		}

		return data;
	},
	isIncidentEmpty: function () {
		return Template.instance().incidentEmpty.get()
  },
  getNavigateClass: function (actu) {
		return !!actu.startDate ? "goToEvent" : "goToInfo";
  },
  chartReady: () => {
		return Template.instance().chartReady.get()
  },
  checkRightInBuildingView: function (module) {
    const condoId = Template.currentData().selectedCondoId
    let right = null
    switch (module) {
      case 'incident':
      right = 'see'
      break;
      case 'messenger':
      right = 'seeAllMsgEnterprise'
      break;
      case 'trombi':
      right = 'see'
      break;
    case 'actuality':
      right = 'see'
      break;
    case 'reservation':
      right = 'see'
      break;
    }
    if (condoId === 'all') {
      return Meteor.listCondoUserHasRight(module, right).length > 0
    } else {
      return Meteor.userHasRight(module, right, condoId)
    }
    return true
  }
});
