/* globals _ */
/* globals $ */
/* globals Enterprises */
/* globals Condos */
/* globals ContactManagement */
/* globals IncidentType */
/* globals DeclarationDetails */
/* globals CondoContact */

import { CommonTranslation } from '/common/lang/lang.js'
import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'

let timers = {
  reminder: null,
  defaultContact: null,
  messengerSet: null
}

Template.contactManagement_messenger.onCreated(function () {
  this.condoId = new ReactiveVar(FlowRouter.getParam('condoId'))

  let self = this

  this.autorun(function () {
    FlowRouter.watchPathChange()

    if (FlowRouter.getParam('condoId')) {
      self.condoId.set(FlowRouter.getParam('condoId'))
    } else {
      self.condoId.set(null)
    }
  })
})

Template.contactManagement_messenger.onDestroyed(function () {
})

Template.contactManagement_messenger.onRendered(function () {
  $('#allRender').hide()
})

function initiateSaver () {
  const lang = FlowRouter.getParam('lang') || 'fr'
  const trCommon = new CommonTranslation(lang)

  if (!($('#noty_topRight_layout_container')[0])) {
    $('#noty_topRight_layout_container').remove()
    $('.display-modules').after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">' + trCommon.commonTranslation['registering'] + '...</span></div></div></li></ul>')
    $('#noty-li').animate({ 'height': '34.4502px' }, 'fast')
  }
}

function updateSaver (ret) {
  const lang = FlowRouter.getParam('lang') || 'fr'
  const trCommon = new CommonTranslation(lang)

  if (!ret) {
    $('#noty_topRight_layout_container').remove()
  } else {
    if (ret.error && ret.error === 403) {
      if (ret.reason === 'Forbidden') {
        $('.noty_text').html(trCommon.commonTranslation['value_thousand'])
      } else {
        $('.noty_text').html(ret.reason)
      }
    } else {
      $('.noty_text').html(ret)
    }
    $('#noty-li').css(
      {
        'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
        'border-radius': '5px',
        'border': '1px solid red',
        'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
        'color': 'darkred',
        'width': '310px',
        'cursor': 'pointer',
        'height': '34.4502px'
      }
    )
    setTimeout(function () {
      $('#noty_topRight_layout_container').remove()
    }, 3000)
  }
}

function saveElement (name, condoId, elementId, elementValues, messageTypeId) {
  if (name === 'contactReminder') {
    Meteor.call('updateCondoReminder', condoId, elementId, elementValues, function (error, result) {
      updateSaver(error)
    })
  } else if (name === 'defaultContact') {
    Meteor.call('updateCondoDefault', condoId, elementValues, function (error, result) {
      updateSaver(error)
    })
  } else if (name === 'contactSet') {
    Meteor.call('updateCondoContact', condoId, elementId, elementValues, messageTypeId, 'Messenger', function (error, result) {
      console.log('error', error)
      console.log(result)
      updateSaver(error)
    })
  }
}

Template.contactManagement_messenger.events({
  'change [name="reminderInput"], keyup [name="reminderInput"], focusout [name="reminderInput"]': function (event, template) {
    initiateSaver()
    clearTimeout(timers.reminder)
    timers.reminder = null
    timers.reminder = setTimeout(function () {
      let reminderId = $(event.currentTarget).attr('id')
      let reminderValue = $(event.currentTarget).val()
      if (reminderValue > 1000) {
        $(event.currentTarget).removeClass('cm-reminder-no-error').addClass('cm-reminder-error')
      } else {
        $(event.currentTarget).removeClass('cm-reminder-error').addClass('cm-reminder-no-error')
      }
      if ($('#typeReminder' + reminderId).val() === 'day') {
        reminderValue *= 24
      }
      saveElement('contactReminder', template.condoId.get(), reminderId, reminderValue)
    }, 1000)
  },
  'change [name="reminderInputType"], keyup [name="reminderInputType"], focusout [name="reminderInputType"]': function (event, template) {
    initiateSaver()
    clearTimeout(timers.reminder)
    timers.reminder = null
    timers.reminder = setTimeout(function () {
      let reminderId = $(event.currentTarget).attr('priorityId')
      let reminderValue = $('#' + reminderId).val()
      if ($('#mb-select-typeReminder' + reminderId).val() === 'day') {
        reminderValue *= 24
      }
      saveElement('contactReminder', template.condoId.get(), reminderId, reminderValue)
    }, 1000)
  }
})

let select2Timeout = null

Template.contactManagement_messenger.helpers({
  managerMembersIncident: () => {
    let gestUsers = Enterprises.findOne({ 'users.userId': Meteor.userId() })
    if (gestUsers) {
      gestUsers = gestUsers.users
      let idGest = _.map(gestUsers, function (elem) {
        var isInCharge = false
        _.each(elem.condosInCharge, function (condosInCharge) {
          if (condosInCharge.condoId === Template.instance().condoId.get()) {
            isInCharge = true
          }
        })
        let userHasRight = Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', Template.instance().condoId.get(), elem.userId) && Meteor.userHasRight('incident', 'see', Template.instance().condoId.get(), elem.userId)
        return (isInCharge && userHasRight && elem.isAdmin !== true) ? (elem.userId) : ''
      })
      return Meteor.users.find({ _id: { $in: idGest } })
    }
  },
  defaultContactUserId: () => {
    let condoContact = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (!condoContact) {
      return []
    }
    return [condoContact.defaultContact.userId]
  },
  getReminderValueBlaze: (priorityId) => {
    let condo = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (condo) {
      let reminder = _.find(condo.contactReminder, function (priority) { return priority.priorityId === priorityId })
      if (reminder) {
        if (reminder.timeValue % 24 === 0 && reminder.timeValue > 0) {
          return reminder.timeValue / 24
        } else {
          return reminder.timeValue
        }
      }
    }
  },
  isHourValue: (priorityId) => {
    let condo = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (condo) {
      let reminder = _.find(condo.contactReminder, function (priority) { return priority.priorityId === priorityId })
      if (reminder) {
        if (reminder.timeValue % 24 === 0 && reminder.timeValue > 0) {
          return false
        } else {
          return true
        }
      }
    }
  },
  isDayValue: (priorityId) => {
    let condo = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (condo) {
      let reminder = _.find(condo.contactReminder, function (priority) { return priority.priorityId === priorityId })
      if (reminder) {
        if (reminder.timeValue % 24 === 0 && reminder.timeValue > 0) {
          return true
        } else {
          return false
        }
      }
    }
  },
  getDeclarations: () => {
    let listDropdown = ContactManagement.findOne({ condoId: Template.instance().condoId.get() })
    if (!listDropdown) {
      let condoType = Condos.findOne(Template.instance().condoId.get()).settings.condoType
      listDropdown = ContactManagement.findOne({ forCondoType: condoType })
    }
    if (listDropdown) {
      return _.filter(listDropdown.messengerSet, function (list) {
        let incidentType = IncidentType.findOne(list.id)
        if (!incidentType.isNewEmpty || incidentType.isNewEmpty === undefined || incidentType.isNewEmpty !== true) {
          return true
        }
      })
    }
  },
  saveDefaultContact: () => {
    const instance = Template.instance()
    return (userId) => {
      saveElement('updateCondoDefault', instance.condoId.get(), null, userId)
    }
  },
  saveContactSet: (detailId, messageTypeId) =>{
    const instance = Template.instance();
    return () => selectedUser=>{
      if (_.isArray(selectedUser)) {
        saveElement('contactSet', instance.condoId.get(), detailId, selectedUser, messageTypeId)
      }
    }
  },
  getDeclarationName: (declarationDetailId) => {
    let lang = (FlowRouter.getParam('lang') || 'fr')
    let detail = DeclarationDetails.findOne(declarationDetailId)
    if (detail) {
      return detail['value-' + lang]
    }
  },
  alreadySelected: (userId, selectedUsers) => {
    return _.contains(selectedUsers, userId)
  },
  getElemValue: (detailId, declarationId) => {
    let condo = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (condo) {
      let userIds = _.find(condo.contactSet, function (elem) {
        return (elem.declarationDetailId === detailId && declarationId === elem.messageTypeId)
      })
      if (userIds === undefined) {
        return false
      }
      userIds = userIds.userIds
      return userIds
    }
    return false
  },
  getCondoId: () => {
    return Template.instance().condoId.get()
  },
  renderSelect2: () => {
    if (select2Timeout != null) {
      clearTimeout(select2Timeout)
      select2Timeout = null
    } else {
      $('#allRender').hide()
      $('.text-center.load').css('display', 'flex')
      $('#loading').show()
    }
    select2Timeout = setTimeout(function () {
      $('.select-contacts').select2({
        minimumResultsForSearch: 10
      })
      clearTimeout(select2Timeout)
      select2Timeout = null
      $('#loading').hide()
      $('.text-center.load').css('display', 'none')
      $('#allRender').show()
    }, 100)
  }
})
