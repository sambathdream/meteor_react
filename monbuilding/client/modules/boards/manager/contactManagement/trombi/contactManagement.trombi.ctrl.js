import { FileManager } from "/client/components/fileManager/filemanager"
import { CommonTranslation } from "/common/lang/lang"
import { ContactManagement } from "/common/lang/lang"
import { sAlert } from 'meteor/juliancwirko:s-alert'
let timer = null;
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const mimeType = (type) => {
  return type.split('/')[0]
}

function initiateSaver() {
  const lang = FlowRouter.getParam("lang") || "fr"
  const tr_common = new CommonTranslation(lang)

  if (!($("#noty_topRight_layout_container")[0])) {
    $("#noty_topRight_layout_container").remove();
    $(".display-modules").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">' + tr_common.commonTranslation["registering"] + '...</span></div></div></li></ul>');
    $("#noty-li").animate({ "height": "34.4502px" }, "fast");
  }
};

function updateSaver(ret) {
  if (!ret) {
    $("#noty_topRight_layout_container").remove();
  }
  else {
    if (ret.error && ret.error == 403)
      $(".noty_text").html(ret.reason);
    else
      $(".noty_text").html(ret);
    $("#noty-li").css(
      {
        'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
        'border-radius': '5px',
        'border': '1px solid red',
        'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
        'color': 'red',
        'width': '310px',
        'cursor': 'pointer',
        'height': '34.4502px'
      }
    );
    setTimeout(function () {
      $("#noty_topRight_layout_container").remove();
    }, 3000)
  }
};
Template.contactManagement_trombi.onCreated(function () {
  this.subscribe('PhoneCode');
  this.condoId = new ReactiveVar(FlowRouter.getParam("condoId"));
  this.files = new ReactiveVar([])
  this.newManager = new ReactiveArray([{ firstname: "", lastname: "", phone: "", phoneCode:"33", function: "", picture: { name: "", id: "" } }]);
  this.fm = new FileManager(UserFiles, { userId: "", publicPicture: true, isUnregisteredManager: true });
  let self = this;

  this.autorun(function () {
    FlowRouter.watchPathChange();

    if (FlowRouter.getParam("condoId")) {
      self.condoId.set(FlowRouter.getParam("condoId"));
    }
    else
      self.condoId.set(null);
  });

});

Template.contactManagement_trombi.onDestroyed(function () {
})

Template.contactManagement_trombi.onRendered(function () {
  $("#allRender").hide();
  $("#errorWrongPhoneNumberText").hide()
})

let select2Timeout = null;

Template.contactManagement_trombi.events({
  'hidden.bs.modal #modal_addManager': function (event, template) {
    let unsavedManager = template.formEntity.get();
    if(unsavedManager.picture&& unsavedManager.picture.id && unsavedManager.picture.id != ""){
      Meteor.call("removePictureForUnregiteredManager", unsavedManager.picture.id);
    }
    template.formEntity.clear()
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_addManager': function (event, template) {
    let modal = $(event.currentTarget);
    window.location.hash = "#modal_addManager";
    window.onhashchange = function () {
      if (location.hash != "#modal_addManager") {
        modal.modal('hide');
      }
    }
  },
  'click .trashColumn': function (event, template) {
    event.stopPropagation();
    event.preventDefault();
    let $userId = $(event.currentTarget).attr('userId')
    if ($userId) {
      initiateSaver();
      Meteor.call("removeUneregisteredManager", $userId, function (error) {
        if (!error) {
          updateSaver(error);
        }
      });
    };
  },
  'click #validNewManager': function (event, template) {
    event.preventDefault();
    event.stopPropagation();
    initiateSaver();
    let newManager = template.newManager.list();
    Meteor.call('saveNewUnregisteredManager', template.condoId.get(), newManager, (err) => {
      updateSaver(err)
      $('#modal_addManager').hide()
      $('body').removeClass('modal-open')
      $('.modal-backdrop').remove()
    });
  },
  'click #mb-button-edit-manager-no-account': (e, t) => {
    $('#modal_addManager').modal('show')
  }
});
function saveExistingManager(id,  condoId, detailId, values, tabId){
  clearTimeout(timer);
  timer = null;
  timer = setTimeout(function () {
    if(!String(id).indexOf('existingManager')){
      Meteor.call("updateCondoContact", condoId, detailId, values, null, tabId, function (error, result) {
        updateSaver(error);
      })
    } 
  }, 1000);
}
Template.contactManagement_trombi.helpers({
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.newManager.list()[0]['phone']
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.formEntity.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          }
        } catch (e) {
          t.goodPhone = false
        }
      }
     // updateSubmit(t)
    }
  },
  onInputDetails: (key) =>{
    const t = Template.instance()
    return () => value => {
      if (key === 'phone') {
        let number = '+' + t.newManager.list()[0]['phoneCode'] + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.newManager.list()[0][key] = phoneUtil.format(number, PNF.ORIGINAL)
            t.goodPhone = true
          } else {
            t.newManager.list()[0][key] =!value ? null : value
            t.goodPhone = false
            $("#errorWrongPhoneNumberText").show().delay(3000).fadeOut()
          }
        } catch (e) {
          t.newManager.list()[0][key] =!value ? null : value
          t.goodPhone = false
          $("#errorWrongPhoneNumberText").show().delay(3000).fadeOut()
        }
      } else{
        t.newManager.list()[0][key]=value
      }
    console.log(t.newManager);
    }
  },
  getFormEntity: (key) => {
    const t = Template.instance()
    return () => t.newManager.list()[0][key] || ''
  },
  getAddLabel: () => {
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new ContactManagement(lang)
    return tr_common.contactManagement['trombiAddBtn']
    // return getDicoVar ('contactManagement', 'trombiAddBtn')
  },
  getAvatar: (userId) =>{
    let pictureProfile = Avatars.findOne({userId: userId});
    if(pictureProfile != undefined && pictureProfile.avatar != undefined && pictureProfile.avatar.original){
      return pictureProfile.avatar.original
    }else {
      return "/img/anonymous.png"
    }
  },
  saveManager: (detailId)=>{
     const instance = Template.instance();
    return () =>userIds =>{
      if(_.isArray(userIds)){
        saveExistingManager('existingManager', instance.condoId.get(), detailId, userIds, null, "WhosWho")
      }
    }
  },
  managerMembers: () => {
    let selectedCondoId = Template.instance().condoId.get();
    let gestUsers = Enterprises.findOne({ "users.userId": Meteor.userId() });
    if (gestUsers) {
      gestUsers = gestUsers.users;
      let idGest = _.map(gestUsers, function (elem) {
        var isInCharge = false;
        _.each(elem.condosInCharge, function (condosInCharge) {
          if (condosInCharge.condoId == selectedCondoId)
            isInCharge = true;
        })
        return (isInCharge && elem.isAdmin != true) ? (elem.userId) : "";
      });
      return Meteor.users.find({ _id: { $in: idGest } });
    }
  },
  managerMembersUnregistered: () => {
    console.log(GestionnaireUnregistered.find({ condoId: Template.instance().condoId.get() }).fetch())
    return GestionnaireUnregistered.find({ condoId: Template.instance().condoId.get() }).fetch();
  },
  getDeclarationTrombiId: () => {
    let declarationId = DeclarationDetails.findOne({ key: "managerWAccount" });
    return declarationId ? declarationId._id : false;
  },
  getUserDeclarationTrombi: () => {
    let declarationId = DeclarationDetails.findOne({ key: "managerWAccount" });
    let selectedCondoId = Template.instance().condoId.get();
    if (declarationId && selectedCondoId) {
      declarationId = declarationId._id;
      let condoContact = CondoContact.findOne({ condoId: selectedCondoId });
      if (condoContact) {
        condoContact = condoContact.contactSet;
        let contacts = _.find(condoContact, function (contact) {
          if (contact.declarationDetailId == declarationId)
            return true;
          return false;
        });
        if (contacts)
          return contacts.userIds;
      }
      return false;
    }
    return false;
  },
  alreadySelected: (userId, selectedUsers) => {
    return _.contains(selectedUsers, userId);
  },
  newManager: () => {
    return Template.instance().newManager.list();
  },
  renderSelect2: () => {
    if (select2Timeout != null) {
      clearTimeout(select2Timeout);
      select2Timeout = null;
    }
    else {
      $("#allRender").hide();
      $('.text-center.load').css('display', 'flex')
      $("#loading").show();
    }
    select2Timeout = setTimeout(function () {
      $('.select-contacts').select2({
        minimumResultsForSearch: 10
      });
      clearTimeout(select2Timeout);
      select2Timeout = null;
      $("#loading").hide();
      $('.text-center.load').css('display', 'none')
      $("#allRender").show();
    }, 100);
  },
  canValidateNewUnregisteredManager: () => {
    let managerInfo = Template.instance().newManager.list()[0]
    let canValidate = true;
    if ( !managerInfo.firstname || !managerInfo.lastname || !managerInfo.function){
      canValidate = false
    }
    console.log(managerInfo)
    return canValidate
  },
  getButtonAction: () => {
    return () => {
      $('#modal_addManager').modal('show')
    }
  },
  previewFiles: () => {
    const reactiveFiles = Template.instance().files.get()
    return reactiveFiles
  },
  onFileAdded: () => {
    const t = Template.instance()

    return (files) => {
      files = files.filter(f => {
        if (f.type.split('/')[0] !== 'image') {
          const lang = FlowRouter.getParam('lang') || 'fr'
          const translate = new CommonTranslation(lang)
          sAlert.error(translate.commonTranslation.only_image_accepted)
          return false
        } else {
          return true
        }
      })
      if (files && files.length) {
        t.files.set([
          ...t.files.get(),
          ...files
        ])
        console.log(t)
        let newManager = t.newManager[0];
        if(newManager.picture.id && newManager.picture.id != undefined){
          Meteor.call("removePictureForUnregiteredManager", newManager.picture.id);
        }
        t.fm.insert(files[0], function (err, file) {
          if (!err && file) {
            let picture = { name: file.name, id: file._id };
            newManager.picture = picture;
          }
        })
      }
    }
  },
  onFileRemoved: () => {
    const t = Template.instance()

    return (fileId) => {
      t.files.set(_.filter(t.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
      Meteor.call("removePictureForUnregiteredManager", t.newManager[0].picture.id);
    }
  },
});