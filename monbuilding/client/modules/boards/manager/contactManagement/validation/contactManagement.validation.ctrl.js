/* globals _ */
/* globals $ */
/* globals Enterprises */
/* globals Condos */
/* globals DeclarationDetails */
/* globals CondoContact */

import { CommonTranslation } from '/common/lang/lang'
import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'

let timer = null

function initiateSaver () {
  const lang = FlowRouter.getParam('lang') || 'fr'
  const trCommon = new CommonTranslation(lang)

  if (!($('#noty_topRight_layout_container')[0])) {
    $('#noty_topRight_layout_container').remove()
    $('.display-modules').after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">' + trCommon.commonTranslation['registering'] + '...</span></div></div></li></ul>')
    $('#noty-li').animate({ 'height': '34.4502px' }, 'fast')
  }
}

function updateSaver (ret) {
  if (!ret) {
    $('#noty_topRight_layout_container').remove()
  } else {
    if (ret.error && ret.error === 403) {
      $('.noty_text').html(ret.reason)
    } else {
      $('.noty_text').html(ret)
    }
    $('#noty-li').css(
      {
        'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
        'border-radius': '5px',
        'border': '1px solid red',
        'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
        'color': 'red',
        'width': '310px',
        'cursor': 'pointer',
        'height': '34.4502px'
      }
    )
    setTimeout(function () {
      $('#noty_topRight_layout_container').remove()
    }, 3000)
  }
}

Template.contactManagement_validation.onCreated(function () {
  this.condoId = new ReactiveVar(FlowRouter.getParam('condoId'))
  let self = this

  this.autorun(function () {
    FlowRouter.watchPathChange()

    if (FlowRouter.getParam('condoId')) {
      self.condoId.set(FlowRouter.getParam('condoId'))
    } else {
      self.condoId.set(null)
    }
  })
})

Template.contactManagement_validation.onDestroyed(function () {
})

Template.contactManagement_validation.onRendered(function () {
  $('#allRender').hide()
})

let select2Timeout = null

Template.contactManagement_validation.events({
  'change [name="validEntry"], change [name="edl"]': function (event, template) {
    initiateSaver()
    clearTimeout(timer)
    timer = null
    timer = setTimeout(function () {
      let values = $(event.currentTarget).val()
      let declarationDetailId = $(event.currentTarget).attr('detailId')
      Meteor.call('updateCondoContact', template.condoId.get(), declarationDetailId, values, null, 'Validation', function (error, result) {
        updateSaver(error)
      })
    }, 1000)
  }
})

Template.contactManagement_validation.helpers({
  saveChanges: (detailId) =>{
    return ()=>selected=> {
      if(_.isArray(selected)){
        clearTimeout(timer)
        timer = null
        timer = setTimeout(function () {
          Meteor.call('updateCondoContact', template.condoId.get(), detailId, values, null, 'Validation', function (error, result) {
            updateSaver(error)
          })
        }, 1000)
      }
    }
  },
  managerMembers: () => {
    let selectedCondoId = Template.instance().condoId.get()
    let gestUsers = Enterprises.findOne({ 'users.userId': Meteor.userId() })
    if (gestUsers) {
      gestUsers = gestUsers.users
      let idGest = _.map(gestUsers, function (elem) {
        var isInCharge = false
        _.each(elem.condosInCharge, function (condosInCharge) {
          if (condosInCharge.condoId === selectedCondoId) {
            isInCharge = true
          }
        })
        let userHasRight = Meteor.userHasRight('setContact', 'validAwaitingOccupant', Template.instance().condoId.get(), elem.userId)
        return (isInCharge && userHasRight && elem.isAdmin !== true) ? (elem.userId) : ''
      })
      return Meteor.users.find({ _id: { $in: idGest } })
    }
  },
  condoOptions: (option) => {
    let condo = Condos.findOne(Template.instance().condoId.get())
    if (condo) {
      return condo.settings.options[option]
    }
    return false
  },
  ManagerCanValidate: () => {
    let gestionnaire = Enterprises.findOne({ 'users.userId': Meteor.userId() })
    let condoId = Template.instance().condoId.get()
    let condo = Condos.findOne(condoId)
    if (gestionnaire && gestionnaire !== undefined && condo) {
      let condos = condo.settings.condoType
      return gestionnaire.settings.validUsers[condos]
    }
  },
  getDeclarationId: (name) => {
    let declarationId = DeclarationDetails.findOne({ key: name })
    return declarationId ? declarationId._id : false
  },
  getUserDeclaration: (name) => {
    let declarationId = DeclarationDetails.findOne({ key: name })
    let selectedCondoId = Template.instance().condoId.get()
    if (declarationId && selectedCondoId) {
      declarationId = declarationId._id
      let condoContact = CondoContact.findOne({ condoId: selectedCondoId })
      if (condoContact) {
        condoContact = condoContact.contactSet
        let contacts = _.find(condoContact, function (contact) {
          if (contact.declarationDetailId === declarationId) {
            return true
          }
          return false
        })
        if (contacts) {
          return contacts.userIds
        }
      }
      return false
    }
    return false
  },
  alreadySelected: (userId, selectedUsers) => {
    return _.contains(selectedUsers, userId)
  },
  renderSelect2: () => {
    if (select2Timeout != null) {
      clearTimeout(select2Timeout)
      select2Timeout = null
    } else {
      $('#allRender').hide()
      $('.text-center.load').css('display', 'flex')
      $('#loading').show()
    }
    select2Timeout = setTimeout(function () {
      $('.select-contacts').select2({
        minimumResultsForSearch: 10
      })
      clearTimeout(select2Timeout)
      select2Timeout = null
      $('.text-center.load').css('display', 'none')
      $('#loading').hide()
      $('#allRender').show()
    }, 100)
  }
})
