/* globals _ */
/* globals $ */
/* globals Enterprises */
/* globals Condos */
/* globals UserFiles */
/* globals ContactManagement */
/* globals CondoContact */

import { FileManager } from '/client/components/fileManager/filemanager'
import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'

function checkTabDisplay (template, condoId, options, incidentActive, trombiActive) {
  let condo = Condos.findOne(condoId)
  if (condo) {
    var active = false
    _.each(options, function (option) {
      if (condo.settings.options[option] === true) {
        active = true
      }
    })
    console.log('options, active', options, active)
    if ((active === false || Meteor.userHasRight('setContact', 'seeMessenger', condoId) !== true) && options[0] === 'messengerGestionnaire') {
      active = false
      if (template.tab.get() === 'messenger') {
        template.tab.set('trombi')
      }
    } else if ((active === false || Meteor.userHasRight('setContact', 'seeWhosWho', condoId) !== true) && options[0] === 'trombi') {
      active = false
      if (template.tab.get() === 'trombi') {
        if (incidentActive === true) {
          template.tab.set('messenger')
        } else {
          template.tab.set('validation')
        }
      }
    } else if ((active === false || Meteor.userHasRight('setContact', 'seeValidation', condoId) !== true) && options[0] === 'reservations') {
      console.log('wesh!')
      active = false
      if (template.tab.get() === 'validation') {
        if (incidentActive === true) {
          template.tab.set('messenger')
        } else if (trombiActive === true) {
          template.tab.set('trombi')
        }
      }
    } else if ((active === true || Meteor.userHasRight('setContact', 'seeValidation', condoId) === true) && options[0] === 'reservations') {
      let gestionnaire = Enterprises.findOne({ 'users.userId': Meteor.userId() })
      let condoId = template.condoId.get()
      if (gestionnaire && gestionnaire !== undefined && condoId && condoId !== undefined) {
        let condos = Condos.findOne(condoId).settings.condoType
        let canValidate = gestionnaire.settings.validUsers[condos]
        if (!canValidate) {
          active = false
          if (template.tab.get() === 'validation') {
            if (incidentActive === true) {
              template.tab.set('messenger')
            } else if (trombiActive === true) {
              template.tab.set('trombi')
            }
          }
        }
      }
    }
    console.log('active', active)
    return active
  }
  return false
}

Template.contactManagement.onCreated(function () {
  this.handler = null
  this.isAllSelected = new ReactiveVar(FlowRouter.getParam('condoId') === 'all')
  this.condoId = new ReactiveVar(FlowRouter.getParam('condoId') || null)
  this.tab = new ReactiveVar(FlowRouter.getParam('tab') || 'messenger')
  this.displayableTab = new ReactiveVar({ 'messenger': false, 'trombi': false, 'validation': false })

  this.fm = new FileManager(UserFiles, { userId: Meteor.userId(), publicPicture: true })
  this.subscribe('GestionnaireUnregistered')

  Meteor.call('setNewAnalytics', { type: 'module', module: 'setContacts', accessType: 'web', condoId: '' })

  if (this.condoId.get() != null) {
    if (this.handler) {
      this.handler.stop()
      this.handler = null
    }
    this.handler = this.subscribe('contactManagementFromManager', this.condoId.get(), function (error, result) {
      if (error) {
        console.log(error, result)
      }
    })
  }

  let self = this
  this.autorun(function () {
    FlowRouter.watchPathChange()

    setTimeout(function () {
      $('.select-contacts').select2({
        minimumResultsForSearch: 10
      })
    }, 50)
    if (FlowRouter.getParam('condoId')) {
      self.condoId.set(FlowRouter.getParam('condoId'))
      if (self.handler) {
        self.handler.stop()
        self.handler = null
      }
      self.handler = self.subscribe('contactManagementFromManager', self.condoId.get(), function (error, result) {
        if (error) {
          console.log(error, result)
        }
      })
    } else {
      self.condoId.set(null)
    }
  })
})

Template.contactManagement.onDestroyed(function () {
  // Meteor.call('updateAnalytics', {type: 'module', module: 'setContacts', accessType: 'web', condoId: ''})
})

Template.contactManagement.onRendered(function () {
})

Template.contactManagement.events({
  'click .tabContactManagement': function (event, template) {
    template.tab.set(event.currentTarget.getAttribute('tab'))

    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: Template.instance().condoId.get(), tab: event.currentTarget.getAttribute('tab') })
  }
})

let select2Timeout = null

Template.contactManagement.helpers({
  isEmptyState: () => {
    let displayableTab = Template.instance().displayableTab.get()
    if (displayableTab.messenger === false && displayableTab.trombi === false && displayableTab.validation === false) {
      return true
    }
    return false
  },
  isAllReady: () => {
    let condoId = Template.instance().condoId.get()

    const lang = FlowRouter.getParam('lang') || 'fr'
    /* if (condoId === null) {
      condoId = Condos.findOne()._id
      let incidentResult = checkTabDisplay(Template.instance(), condoId, ['messengerGestionnaire', 'incidents'], null, null)
      let trombiResult = checkTabDisplay(Template.instance(), condoId, ['trombi'], incidentResult, null)
      checkTabDisplay(Template.instance(), condoId, ['reservations'], incidentResult, trombiResult)
      FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: condoId, tab: Template.instance().tab.get() })
    } else  */if (condoId && condoId !== 'all') {
      let selectedTab = Template.instance().tab.get()
      let incidentResult = checkTabDisplay(Template.instance(), condoId, ['messengerGestionnaire', 'incidents'], null, null)
      let trombiResult = checkTabDisplay(Template.instance(), condoId, ['trombi'], incidentResult, null)
      let reservationsResult = checkTabDisplay(Template.instance(), condoId, ['reservations'], incidentResult, trombiResult)
      Template.instance().displayableTab.set({ 'messenger': incidentResult, 'trombi': trombiResult, 'validation': reservationsResult })
      if (selectedTab !== Template.instance().tab.get()) {
        FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: condoId, tab: Template.instance().tab.get() })
      }
    }
    if (ContactManagement.find().count() > 0 && CondoContact.find().count() > 0) {
      return true
    }
  },
  isDisplayable: (name) => {
    return Template.instance().displayableTab.get()[name]
  },
  selectedTab: (name) => {
    return Template.instance().tab.get() === name
  },
  isDefaultContact: (userId) => {
    let condoContact = CondoContact.findOne({ condoId: Template.instance().condoId.get() })
    if (!condoContact) {
      return false
    }
    return (condoContact.defaultContact.userId && condoContact.defaultContact.userId === userId)
  },
  renderSelect2: () => {
    if (select2Timeout != null) {
      clearTimeout(select2Timeout)
      select2Timeout = null
    }
    select2Timeout = setTimeout(function () {
      $('.select-contacts').select2({
        minimumResultsForSearch: 10
      })
      clearTimeout(select2Timeout)
      select2Timeout = null
    }, 100)
  },
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const t = Template.instance()
    if (condoId !== FlowRouter.getParam('condoId')) {
      const lang = FlowRouter.getParam('lang') || 'fr'
      t.isAllSelected.set(condoId === 'all')
      FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: condoId, tab: t.tab.get() })
    }
    return true
  },
  isAllSelected: () => Template.instance().isAllSelected.get(),
  condoDropdown: () => {
    let template = Template.instance()
    return (condoId) => {
      if (condoId !== 'all') {
        const lang = FlowRouter.getParam('lang') || 'fr'
        FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: condoId, tab: template.tab.get() })
      }
    }
  },
  ManagerCanValidate: () => {
    let gestionnaire = Enterprises.findOne({ 'users.userId': Meteor.userId() })
    let condoId = Template.instance().condoId.get()
    let condo = Condos.findOne(condoId)
    if (gestionnaire && gestionnaire !== undefined && condo) {
      let condos = condo.settings.condoType
      return gestionnaire.settings.validUsers[condos]
    }
  }
})
