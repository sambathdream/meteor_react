Template.buildingsListManager.onCreated(function () {
  this.userId = Meteor.userId()
  this.searchText = new ReactiveVar('')
})

Template.buildingsListManager.onDestroyed(function () {
})

Template.buildingsListManager.onRendered(() => {
})

Template.buildingsListManager.events({
})

Template.buildingsListManager.helpers({
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  getManagerCondos: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    let allowedCondoIds = Meteor.listCondoUserHasRight("managerList", "seeManager")
    let userId = Template.instance().userId

    let thisUserCondos = []
    Enterprises.find({'users.userId': userId}).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (user.userId === userId) {
          thisUserCondos = _.pluck(user.condosInCharge, 'condoId')
        }
      })
    })

    let listCondoId = _.intersection(allowedCondoIds, thisUserCondos)

    let listCondos = []

    Condos.find({ _id: { $in: listCondoId } }).forEach(condo => {
      let condoName = !!condo.info.address ? condo.info.address : '-'
      if (condo.name && condo.name !== "" && (condo.info.address && condo.name !== condo.info.address)) {
        condoName = condo.name
        if (condo.info.id && condo.info.id !== '-1') {
          condoName = condo.info.id + ' - ' + condoName
        }
      }
      let adress = condo.info.address + ", " + condo.info.code + " " + condo.info.city
      let userRight = UsersRights.findOne({
        $and: [
          { "userId": userId },
          { "condoId": condo._id }
        ]
      })
      let defaultRoleName = 'non défini'
      if (userRight && userRight.defaultRoleId) {
        let defaultRole = DefaultRoles.findOne({ _id: userRight.defaultRoleId })
        if (defaultRole && defaultRole.name) {
          defaultRoleName = defaultRole.name
        }
      }
      if (condoName.match(regexp) || adress.match(regexp) || defaultRoleName.match(regexp)) {
        listCondos.push({
          _id: condo._id,
          name: condoName,
          adress: adress,
          role: defaultRoleName
        })
      }
    })
    return listCondos
  },
  getTotalCondos: () => {
    return 1
  },
})
