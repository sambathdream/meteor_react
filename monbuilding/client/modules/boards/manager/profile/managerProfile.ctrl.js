import { FileManager } from '/client/components/fileManager/filemanager.js';
import { CommonTranslation } from "/common/lang/lang.js"

const mimeType = (type) => {
  return type.split('/')[0]
}

Template.managerProfile.onCreated(function () {
  this.userId = Meteor.userId()
  this.fm = new FileManager(UserFiles, { userId: Meteor.userId(), publicPicture: true });

  let tab = FlowRouter.getParam('tab')
})

Template.managerProfile.onDestroyed(function () {
})

Template.managerProfile.onRendered(() => {
})

Template.managerProfile.events({
  'click .tabBarContainer > div': (event, template) => {
    let tabName = $(event.currentTarget).data('tab');
    FlowRouter.setParams({tab: tabName})
  },
  'click': (event, template) => {
    if ($(event.currentTarget).hasClass("editProfilePicture") || $(event.currentTarget).hasClass("cancelDropdown")) {
      $('.dropdownProfilePicture').toggleClass('displayNone')
    } else if ($(event.currentTarget).hasClass("uploadPhoto")) {
      $('#inputAvatar').click();
      $('.dropdownProfilePicture').addClass('displayNone')
    } else if ($(event.currentTarget).hasClass("removePhoto")){
      Meteor.call('setAvatarId', null)
    } else {
      $('.dropdownProfilePicture').addClass('displayNone')
    }
  },
	'change #inputAvatar' (event, template) {
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (event.currentTarget.files && event.currentTarget.files.length === 1 && mimeType(event.currentTarget.files[0].type) === 'image') {
			template.fm.insert(event.currentTarget.files[0], function(err, file) {
        event.currentTarget.value = ''
        if (!err && file) {
          template.fm.clearFiles()
          Meteor.call('setAvatarId', file._id);
				}
			});
		} else {
      sAlert.error(tr_common.commonTranslation['image_only'])
    }
	},
})

Template.managerProfile.helpers({
  getUserProfile: () => {
    let profile = UsersProfile.findOne({ _id: Template.instance().userId })
    if (profile === undefined || !FlowRouter.getParam('tab')) {
      let params = {
        tab: FlowRouter.getParam('tab')
      }
      FlowRouter.go('app.gestionnaire.profile.tab', params)
    }
    return profile
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().userId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  selectedTab: () => {
    return FlowRouter.getParam('tab')
  },
  getTabTemplate: () => {
    switch (FlowRouter.getParam('tab')) {
      case 'preferences':
        return 'settingsListManager'
        break;
      case 'password':
        return 'changePassword'
        break;
      case 'buildingsListManager':
        return 'buildingsListManager'
        break;
      default:
        break;
    }
  }
})
