import { CommonTranslation } from "/common/lang/lang.js"

function initiateSaver(name) {
  const lang = FlowRouter.getParam("lang") || "fr"
  const tr_common = new CommonTranslation(lang)

  if (!($("#noty_topRight_layout_container")[0])) {
    $("#noty_topRight_layout_container").remove();
    $(".display-modules").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">' + tr_common.commonTranslation["saving"] + '..</span></div></div></li></ul>');
    $("#noty-li").animate({ "height": "34.4502px" }, "fast");
  }
};

function updateSaver(ret) {
  Meteor.setTimeout(function() {
    if (!ret) {
      $("#noty_topRight_layout_container").remove();
    }
    else {
      if (ret.error && ret.error == 403)
        $(".noty_text").html(ret.reason);
      else
        $(".noty_text").html(ret);
      $("#noty-li").css(
        {
          'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
          'border-radius': '5px',
          'border': '1px solid red',
          'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
          'color': 'red',
          'width': '310px',
          'cursor': 'pointer',
          'height': '34.4502px'
        }
      );
      setTimeout(function () {
        $("#noty_topRight_layout_container").remove();
      }, 3000)
    }
  }, 500);
};


Template.settingsListManager.onCreated(function () {
  this.condoId = new ReactiveVar(null)
})

Template.settingsListManager.onDestroyed(function () {
})

Template.settingsListManager.onRendered(() => {
})

Template.settingsListManager.events({
  'click [name="notification_input"]': (e, t) => {
    const notif_name = $(e.currentTarget).attr('id')
    const condoId = Template.instance().condoId.get()
    const value = $(e.currentTarget).prop('checked')
    initiateSaver(notif_name);

    Meteor.call('saveNotificationManager', condoId, notif_name, value, (error, result) => {
      updateSaver(error);
    });
  }
})

Template.settingsListManager.helpers({
  getUserCondos: () => {
    const userId = Meteor.userId()
    let condosInCharge = []
    Enterprises.find({ 'users.userId': userId }).forEach(enterprise => {
      const thisUser = enterprise.users.find(user => { return user.userId === userId })
      if (thisUser) {
        condosInCharge.push(..._.pluck(thisUser.condosInCharge, 'condoId'))
      }
    });
    return condosInCharge
  },
  condoIdCb: () => {
    let t = Template.instance()
    return (condoId) => {
      t.condoId.set(condoId)
    }
  },
  getCondoManager: () => {
    const userId = Meteor.userId()
    const condoId = Template.instance().condoId.get()
    if (condoId) {
      let thisCondo = null
      Enterprises.find({ 'users.userId': userId }).forEach(enterprise => {
        const thisUser = enterprise.users.find(user => { return user.userId === userId })
        if (thisUser) {
          thisCondo = thisUser.condosInCharge.find(condo => { return condo.condoId === condoId })
        }
      })
      return thisCondo
    }
    return false
  },
  getNotifications: (notifications) => {
    let retNotif = []
    const condoId = Template.instance().condoId.get()
    const condo = Condos.findOne({ _id: condoId })
    if (notifications && condo && condo.settings && condo.settings.options) {
      const options = condo.settings.options
      _.each(options, (value, key) => {
        switch (key) {
          case 'incidents':
            if (value === true && Meteor.userHasRight("incident", "see", condoId)) {
              retNotif.push({ name: 'incidents', value: notifications.incidents, index: 1 })
            }
            break;
            case 'informations':
            if (value === true && Meteor.userHasRight("actuality", "see", condoId)) {
              retNotif.push({ name: 'actualites', value: notifications.actualites, index: 2 })
            }
            break;
          case 'forum':
            if (value === true && (Meteor.userHasRight("forum", "seeSubject", condoId) || Meteor.userHasRight("forum", "seePoll", condoId))) {
              retNotif.push({ name: 'forum_forum', value: notifications.forum_forum, index: 3 })
            }
            break;
          case 'classifieds':
            if (value === true && Meteor.userHasRight("ads", "see", condoId)) {
              retNotif.push({ name: 'classifieds', value: notifications.classifieds, index: 4 })
            }
            break;
          case 'reservations':
            if (value === true && Meteor.userHasRight("reservation", "seeAll", condoId)) {
              retNotif.push({ name: 'reservations', value: notifications.reservations, index: 5 })
            }
            break;
          case 'messengerGestionnaire':
            if (value === true && Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId)) {
              retNotif.push({ name: 'messenger', value: notifications.messenger, index: 6 })
            }
            break;
          default:
            break;
        }
      })
    }
    return retNotif.sort((a, b) => a.index > b.index)
  }
})
