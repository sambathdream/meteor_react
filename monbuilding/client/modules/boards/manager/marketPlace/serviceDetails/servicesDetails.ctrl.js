/* globals $ */

import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import { FlowRouter } from 'meteor/kadira:flow-router'
// import { MarketPlaceFiles } from '/common/userFiles'

import { MarketServices } from '/common/collections/marketPlace'

Template.manager_marketPlace_servicesDetails.onCreated(function () {
  this.servicesHandler = Meteor.subscribe('market_place_service_details', FlowRouter.getParam('serviceId'))
})

Template.manager_marketPlace_servicesDetails.onCreated(function () {

})

Template.manager_marketPlace_servicesDetails.events({
  'click #backbutton': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.marketPlace.tab', { lang, tab: 'services' })
  },
  'click .editItem': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const serviceId = $(e.currentTarget).data('serviceid')
    FlowRouter.go('app.gestionnaire.marketPlace.editService', { lang, tab: 'services', serviceId })
  }
})

Template.manager_marketPlace_servicesDetails.helpers({
  isSubscribeReady: () => {
    return Template.instance().servicesHandler ? Template.instance().servicesHandler.ready() : true
  },
  getService: () => {
    const serviceId = FlowRouter.getParam('serviceId')
    return MarketServices.findOne({ _id: serviceId })
  },
  MbFilePreview: () => MbFilePreview,
  getFiles: (files) => {
    const mappedFiles = files.filter(file => !file.fileId).map(file => {
      // const thisFile = MarketPlaceFiles.findOne({ _id: file })
      if (file) {
        return {
          lowRes: file.link,
          preview: file.link,
          large: file.link,
          original: file.link,
          filename: file.name,
          type: file.type.split('/')[0],
          downloadPath: file.link
        }
      }
    })
    return mappedFiles
  },
  getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : []
})
