/* globals BadgesGestionnaire */
/* globals $ */

import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'

Template.manager_marketPlace.onCreated(function () {
  this.tabRouter = Meteor.isManager ? 'tab' : 'wildcard' // Not useful here, just here to be able to copy / paster this module and use it easily everywhere (occupant and manager)
  let tab = FlowRouter.getParam(this.tabRouter)
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), [this.tabRouter]: 'services' })
  }
})

Template.manager_marketPlace.onRendered(function () {

})

Template.manager_marketPlace.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ [t.tabRouter]: tabName })
  },
  'click .addItem': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.marketPlace.newService', { lang })
  }
})

Template.manager_marketPlace.helpers({
  checkTab: () => {
    const tabRouter = Template.instance().tabRouter
    const tab = FlowRouter.getParam(tabRouter)
    const availableTab = ['services', 'upcoming', 'history']

    if (!tab || (!availableTab.includes(tab))) {
      Meteor.defer(() => {
        FlowRouter.setParams({ [tabRouter]: availableTab[0] })
      })
    }
  },
  condoIds: () => {
    return Meteor.listCondoUserHasRight('marketPlace', 'see')
  },
  getTemplateName: () => {
    const tab = FlowRouter.getParam(Template.instance().tabRouter)
    return 'manager_marketPlace_' + tab
  },
  getData: () => {
    const condoId = Template.currentData().selectedCondoId
    const tab = FlowRouter.getParam(Template.instance().tabRouter)
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('marketPlace', 'see')
    } else if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    return { condoIds: condoIds, isAll: condoId === 'all' }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam(Template.instance().tabRouter)
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), [Template.instance().tabRouter]: 'services' })
    }
    return tab
  },
  userHasRightToAddItem: () => {
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('marketPlace', 'addItem')
    } else if (Meteor.userHasRight('marketPlace', 'addItem', condoId)) {
      condoIds = [condoId]
    }
    return condoIds.length
  },
  getCondoId: () => {
    return Meteor.isManager ? Template.currentData().selectedCondoId : FlowRouter.getParam('condo_id')
  },
  shouldDisplayRestrictedAccess: () => {
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('marketPlace', 'see')
    } else if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
  getUpcomingCounter: () => {
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('marketPlace', 'see')
    } else if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    let counter = 0
    BadgesGestionnaire.find({ _id: { $in: condoIds } }).forEach(condoBadge => {
      counter += condoBadge.marketPlace || 0
    })
    return counter
  }
})
