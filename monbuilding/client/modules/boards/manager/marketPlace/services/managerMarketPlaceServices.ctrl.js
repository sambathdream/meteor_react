/* globals $ */
/* globals Condos */

// import { Meteor } from 'meteor/meteor'
import { CommonTranslation } from '/common/lang/lang'
import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'
import moment from 'moment'
// import { MarketPlaceFiles } from '/common/userFiles'

import { MarketServices, MarketServiceTypes } from '/common/collections/marketPlace'

Template.manager_marketPlace_services.onCreated(function () {
  this.search = new ReactiveVar('')
  this.servicesHandler = Meteor.subscribe('market_place_feed')
})

Template.manager_marketPlace_services.onCreated(function () {

})

Template.manager_marketPlace_services.events({
  'click .services-card': function (e, t) {
    const serviceId = $(e.currentTarget).data('serviceid')
    const lang = FlowRouter.getParam('lang') || 'fr'

    if (Meteor.userHasRight('marketPlace', 'editItem', this.condoId)) {
      FlowRouter.go('app.gestionnaire.marketPlace.editService', { lang, tab: 'services', serviceId })
    }
    // FlowRouter.go('app.gestionnaire.marketPlace.servicesDetails', { lang, serviceId })
  }

})

Template.manager_marketPlace_services.helpers({
  isSubscribeReady: () => {
    return Template.instance().servicesHandler.ready()
  },
  searchCallback: () => {
    const t = Template.instance()

    return (value) => {
      t.search.set(value)
    }
  },
  addNewItem: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    return () => {
      FlowRouter.go('app.gestionnaire.marketPlace.newService', { lang })
    }
  },
  getCondoIds: () => {
    return Template.currentData().condoIds
  },
  getServices: () => {
    const condoIds = Template.currentData().condoIds
    const searchText = Template.instance().search.get()
    const regexp = new RegExp(searchText.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    const services = MarketServiceTypes.find({ condoId: { $in: condoIds } }).fetch()
    const serviceTypeKey = []
    const serviceType = []
    services.forEach(s => {
      serviceTypeKey.push(s.key)
      serviceType.push(translate.commonTranslation[`mp_service_${s.key}`])
    })

    let matchedType = []
    serviceType.forEach((type, index) => {
      if (type.match(regexp)) {
        matchedType.push(serviceTypeKey[index])
      }
    })
    return MarketServices.find({
      condoId: { $in: condoIds },
      $or: [
        { title: regexp },
        { price: regexp },
        { type: { $in: matchedType } },
        { description: regexp }
      ]
    }, {
      sort: { updatedAt: -1 }
    })
  },
  getFileUrl: (files) => {
    if (files && files[0]) {
      return files[0].link
      // const thisFile = MarketPlaceFiles.findOne({ _id: files[0] })
      // return thisFile && thisFile.link()
    }
  },
  getCondoNames: () => {
    const condoIds = Template.currentData().condoIds
    const condos = Condos.find({ _id: { $in: condoIds } }, { fields: { info: true, name: true } })
    const mappedCondos = {}
    condos.forEach(condo => {
      mappedCondos[condo._id] = condo.getNameWithId()
    })
    return mappedCondos
  },
  getCondoName: (condoNames, condoId) => {
    return (condoNames && condoNames[condoId]) || ''
  },
  isSearchActive: () => {
    return Template.instance().search.get() !== ''
  },
  isNewService: (createdAt) => {
    const createdMoment = moment(createdAt).format('YYYY-MM-DD')
    const diffDay = moment().diff(createdMoment, 'days')

    if (diffDay <= 3) {
      return true
    } else {
      return false
    }
  }
})
