import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { MarketServices, MarketServiceTypes, MarketBasket, MarketReservations } from '/common/collections/marketPlace'
import { CommonTranslation } from '/common/lang/lang.js';
import { Meteor } from 'meteor/meteor'
import _ from 'lodash'
import moment from 'moment'

function getDate (t, validatedAt, time = null, lang) {
  moment.locale(lang)
  const translate = new CommonTranslation(lang)
  const newDayMoment = moment(time || validatedAt)
  const newDay = newDayMoment.format('YYYY-MM-DD')
  let date = newDayMoment.format('LL')

  if (newDay === t.today) {
    date = translate.commonTranslation.today
  } else if (newDay === t.yesterday) {
    date = translate.commonTranslation.yesterday
  }
  t.lastDay = date
  let dateLong = null
  if (newDayMoment.isBefore(t.today)) {
    const diffDay = moment().diff(newDay, 'days')
    let ret = translate.commonTranslation.due + (lang === 'fr' ? 'IL Y A ' : '')
    if (diffDay !== 0) {
      if (diffDay > 3) {
        ret = translate.commonTranslation.outdated.toLowerCase()
      } else {
        ret += (diffDay ? diffDay + ' ' + translate.commonTranslation.days.toLowerCase() : '') + translate.commonTranslation.dates_ago
      }
    }

    dateLong = `${date}  <span class="red-panel">(${ret})</span>`
  }
  return { timestamp: newDayMoment.format('x'), dateShort: date, dateLong: dateLong }
}

Template.manager_marketPlace_upcoming.onCreated(function () {
  this.search = new ReactiveVar('')
  this.clicked = new ReactiveVar('')
  this.selectedReservationId = new ReactiveVar(null)
  this.handler = Meteor.subscribe('market_place_reservations_details_manager')

  this.today = moment().format('YYYY-MM-DD')
  this.yesterday = moment().add(-1, 'days').format('YYYY-MM-DD')
  this.lastDay = null
})

Template.manager_marketPlace_upcoming.onRendered(function () {
  const reservationId = FlowRouter.getParam('reservationId')

  if (reservationId) {
    this.selectedReservationId.set(reservationId)
    $('#modal_market_place_reservation').modal('show')
  }
})

Template.manager_marketPlace_upcoming.events({
  'click .clickToToggle': (e, t) => {
    const collapseSectionParentIndex = $(e.currentTarget).data('parentindex')
    const collapseSectionIndex = $(e.currentTarget).data('index')
    var section = document.querySelector(`.section-${collapseSectionParentIndex}-${collapseSectionIndex}`);
    var isCollapsed = section.getAttribute('data-collapsed') === 'true';

    $(`.chevron-container-${collapseSectionParentIndex}-${collapseSectionIndex}`).toggleClass('opened closed')
    if (isCollapsed) {
      expandSection(section)
      section.setAttribute('data-collapsed', 'false')
    } else {
      collapseSection(section)
    }
  },
  'click .clickToPreview': (e, t) => {
    const reservationId = $(e.currentTarget).data('reservationid')

    t.selectedReservationId.set(reservationId)
    $('#modal_market_place_reservation').modal('show')
  }
})

Template.manager_marketPlace_upcoming.helpers({
  isSubscribeReady: () => {
    return Template.instance().handler && Template.instance().handler.ready()
  },
  servicesReservations: () => {
    const t = Template.instance()
    const condoIds = Template.currentData().condoIds
    const isAll = Template.currentData().isAll
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    const regexp = new RegExp(Template.instance().search.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    let reservations = []
    if (isAll) {
      reservations = MarketReservations.find({ validatedAt: { $ne: null }, archivedAt: { $eq: null }
      }, { sort: { validatedAt: 1 } }).fetch()
    } else {
      reservations = MarketReservations.find({ condoId: { $in: condoIds }, validatedAt: { $ne: null }, archivedAt: { $eq: null }
      }, { sort: { validatedAt: 1 } }).fetch()
    }

    const servicesDb = []
    const serviceIndex = {}
    const serviceTypeKey = []
    const serviceType = []
    if (reservations.length) {
      MarketServiceTypes.find().forEach(s => {
        if (!servicesDb.find(sDb => sDb.key === s.key)) {
          serviceTypeKey.push(s.key)
          serviceType.push(translate.commonTranslation[`mp_service_${s.key}`])

          servicesDb.push({key: s.key, reservations: []})
          serviceIndex[s.key] = servicesDb.length - 1
        }
      })
    }

    let matchedType = []
    serviceType.forEach((type, index) => {
      if (type.match(regexp)) {
        matchedType.push(serviceTypeKey[index])
      }
    })

    const returnedObject = []

    const dateIndex = {}

    let dates = {}
    if (reservations && reservations.length === 1) {
      const timeSlot = reservations[0].use_time_slot ? (parseInt(moment(`${reservations[0].day_selected} ${reservations[0].time_slot}`, 'DD/MM/YYYY HH:mm').format('x'))) : null
      dates[reservations[0]._id] = getDate(t, reservations[0].validatedAt, timeSlot, lang)
    } else {
      reservations.sort((rA, rB) => {
        if (!dates[rA._id]) {
          const timeSlot = rA.use_time_slot ? (parseInt(moment(`${rA.day_selected} ${rA.time_slot}`, 'DD/MM/YYYY HH:mm').format('x'))) : null
          dates[rA._id] = getDate(t, rA.validatedAt, timeSlot, lang)
        }
        if (!dates[rB._id]) {
          const timeSlot = rB.use_time_slot ? (parseInt(moment(`${rB.day_selected} ${rB.time_slot}`, 'DD/MM/YYYY HH:mm').format('x'))) : null
          dates[rB._id] = getDate(t, rB.validatedAt, timeSlot, lang)
        }
        return parseInt(dates[rA._id].timestamp) - parseInt(dates[rB._id].timestamp)
      })
    }
    reservations.forEach((reservation, index) => {
      const date = dates[reservation._id]

      if (reservation.title.match(regexp) || reservation.price.match(regexp) || date.dateShort.match(regexp) || (date.dateLong && date.dateLong.match(regexp)) || matchedType.includes(reservation.serviceTypeKey)) {
        if (!returnedObject.find(s => s.date === date.dateShort)) {
          returnedObject.push({
            date: date.dateShort,
            displayDate: date.dateLong || date.dateShort,
            services: _.cloneDeep(servicesDb)
          })
          dateIndex[date.shortDate] = returnedObject.length - 1
        }
        returnedObject[dateIndex[date.shortDate]].services[serviceIndex[reservation.serviceTypeKey]].reservations.push(reservation)
      }
    })
    return returnedObject
  },
  isSearchActive: () => {
    return !!Template.instance().search.get()
  },
  onSearch: () => {
    const t = Template.instance()

    return (search) => {
      t.search.set(search)
    }
  },
  onCheckboxClick: (resaId, condoId) => {
    const t = Template.instance()

    return () => () => {
      // event.stopPropagation();
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      if (!t.clicked.get()) {
        t.clicked.set(resaId)

        bootbox.confirm({
          title: translate.commonTranslation.bootbox_validation_mp_service_title,
          message: translate.commonTranslation.bootbox_validation_mp_service_subtitle,
          buttons: {
            'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
            'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
          },
          callback: function (result) {
            t.clicked.set('')
            if (result) {
              Meteor.call('archiveMarketReservation', resaId, condoId, (error) => {
                if (error) {
                  sAlert.error(translate.commonTranslation.error_occured)
                }
              });
            }
          }
        });
      }
    }
  },
  isClicked: (resaId) => {
    return Template.instance().clicked.get() === resaId
  },
  getUserName: (userId) => {
    const profile = UsersProfile.findOne({ _id: userId })

    return profile.firstname + ' ' + profile.lastname
  },
  getLocation: (reservation) => {
    if (reservation.delivered_by_staff) {
      return 'delivered_by_staff'
    }
    if (reservation.drop_off) {
      return 'drop_off'
    }
    if (reservation.pick_up) {
      return 'pick_up'
    }
    if (reservation.picked_up_by_staff) {
      return 'picked_up_by_staff'
    }
  },
  getLocationDetail: (reservation) => {
    if (reservation.picked_up_by_staff || reservation.delivered_by_staff) {
      const userId = reservation.userId
      const condoId = reservation.condoId
      const user = GestionnaireUsers.findOne({ _id: userId })
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      const condoOption = CondosModulesOptions.findOne({ condoId: condoId })
      const isOfficeProfile = condoOption && condoOption.profile && condoOption.profile.office

      let room = (isOfficeProfile ? translate.commonTranslation.office : translate.commonTranslation.room) + ': '

      if (user) {
        let userCondo = user.resident[0].condos.find(c => c.condoId === condoId)
        if (userCondo && userCondo.userInfo) {
          return room + (userCondo.userInfo.office || userCondo.userInfo.porte || (isOfficeProfile ? translate.commonTranslation.user_office : translate.commonTranslation.user_room))
        }
      }
      return room + (isOfficeProfile ? translate.commonTranslation.user_office : translate.commonTranslation.user_room)
    } else {
      if (reservation.drop_off) {
        return reservation.drop_off_location
      }
      if (reservation.pick_up) {
        return reservation.pick_up_location
      }
    }
  },
  getDataModal: () => {
    const t = Template.instance()
    return {
      reservationId: t.selectedReservationId.get(),
      closeCb: () => {
        t.selectedReservationId.set(null)
      }
    }
  },
})


function collapseSection (element) {
  var sectionHeight = element.scrollHeight;

  var elementTransition = element.style.transition;
  element.style.transition = '';
  requestAnimationFrame(function () {
    element.style.height = sectionHeight + 'px';
    element.style.transition = elementTransition;

    requestAnimationFrame(function () {
      element.style.height = 0 + 'px';
    });
  });
  element.setAttribute('data-collapsed', 'true');
}

function expandSection (element) {
  var sectionHeight = element.scrollHeight;

  element.style.height = sectionHeight + 'px';
  element.addEventListener('transitionend', function (e) {
    element.removeEventListener('transitionend', arguments.callee);
    element.style.height = null;
  });
  element.setAttribute('data-collapsed', 'false');
}
