/* globals $ */
/* globals history */
/* globals Meteor */
/* globals ReactiveDict */

import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import { Template } from 'meteor/templating'
import { BasketFiles } from '/common/userFiles'
import { ReactiveVar } from 'meteor/reactive-var'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import { FlowRouter } from 'meteor/kadira:flow-router'
import _ from 'lodash'
import { CommonTranslation } from '/common/lang/lang'
import DateTime from 'react-datetime'
import moment from 'moment'

Template.newService_preview_modal.onCreated(function () {
  this.isOpen = new ReactiveVar(false)
  this.servicesHandler = Meteor.subscribe('market_place_service_details', FlowRouter.getParam('serviceId'))
  this.fm = new FileManager(BasketFiles, { condoId: FlowRouter.getParam('condo_id'), userId: Meteor.userId() })
  this.form = new ReactiveDict()
  this.form.setDefault({
    pick_up: false,
    drop_off: false,
    delivered_by_staff: false,
    picked_up_by_staff: false,
    special_request: '',
    quantity: '1',
    day_selected: null,
    time_slot: ''
  })

  this.hasOneDelivery = new ReactiveVar(false)
  this.singleDelivery = null
  this.files = new ReactiveVar([])

  this.canSubmit = new ReactiveVar(false)
})

Template.newService_preview_modal.onRendered(function () {
})

Template.newService_preview_modal.events({
  'hidden.bs.modal #modal_newService_preview': function (event, template) {
    template.isOpen.set(false)
    history.replaceState('', document.title, window.location.pathname)
  },
  'show.bs.modal #modal_newService_preview': function (event, template) {
    let modal = $(event.currentTarget)
    window.location.hash = '#modal_newService_preview'
    window.onhashchange = function () {
      if (window.location.hash !== '#modal_newService_preview') {
        modal.modal('hide')
      }
    }
  },
  'shown.bs.modal #modal_newService_preview': function (event, template) {
    template.isOpen.set(true)
  }
})

const inputCheckbox = [
  'pick_up',
  'drop_off',
  'delivered_by_staff',
  'picked_up_by_staff'
]

Template.newService_preview_modal.helpers({
  isOpen: () => Template.instance().isOpen.get(),
  // isSubscribeReady: () => {
  //   return Template.instance().servicesHandler ? Template.instance().servicesHandler.ready() : true
  // },
  // getService: () => {
  //   const serviceId = FlowRouter.getParam('serviceId')
  //   return MarketServices.findOne({ _id: serviceId })
  // },
  // getCondoName: () => {
  //   const condoId = FlowRouter.getParam('condo_id')
  //   return Condos.findOne({ _id: condoId }).getName()
  // },
  MbTextArea: () => MbTextArea,
  previewFiles: () => {
    const reactiveFiles = Template.instance().files.get()

    return reactiveFiles
  },
  onFileAdded: () => {
    const t = Template.instance()

    return (files) => {
      t.files.set([
        ...t.files.get(),
        ...files
      ])
    }
  },
  onFileRemoved: () => {
    const t = Template.instance()

    return (fileId) => {
      t.files.set(_.filter(t.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  MbFilePreview: () => MbFilePreview,
  getFiles: (files) => {
    const mappedFiles = files.filter(file => !file.fileId).map(file => {
      // const thisFile = MarketPlaceFiles.findOne({ _id: file })
      if (file) {
        return {
          lowRes: file.link || file.preview ? file.preview.url : null,
          preview: file.link || file.preview ? file.preview.url : null,
          large: file.link || file.preview ? file.preview.url : null,
          original: file.link || file.preview ? file.preview.url : null,
          filename: file.name,
          type: file.type.split('/')[0],
          downloadPath: file.link || file.preview ? file.preview.url : null
        }
      }
    })
    return mappedFiles
  },
  getForm: (key) => {
    const t = Template.instance()

    return t.form.get(key) || ''
  },
  onInput: (key) => {
    const t = Template.instance()

    return () => (value) => {
      if (inputCheckbox.includes(key)) {
        const deliveryObject = {
          pick_up: false,
          drop_off: false,
          delivered_by_staff: false,
          picked_up_by_staff: false
        }
        deliveryObject[key] = !t.form.get(key)
        t.form.set(deliveryObject)
      } else {
        t.form.set(key, value)
      }
      // updateSubmit(t)
    }
  },
  getDeliveryOptions: function () {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const service = this.form
    const ret = []

    const isOffice = (Condos.findOne({ _id: this.condoId }) || {settings: { condoType: 'copro' } }).settings.condoType === 'office'

    if (service.pick_up) {
      ret.push({
        id: 'pick_up',
        subLabel: service.pick_up_location
      })
    }
    if (service.drop_off) {
      ret.push({
        id: 'drop_off',
        subLabel: service.drop_off_location
      })
    }
    if (service.delivered_by_staff) {
      ret.push({
        id: 'delivered_by_staff',
        subLabel: isOffice ? translate.commonTranslation.delivered_by_staff_detail_occupant_office : translate.commonTranslation.delivered_by_staff_detail_occupant
      })
    }
    if (service.picked_up_by_staff) {
      ret.push({
        id: 'picked_up_by_staff',
        subLabel: isOffice ? translate.commonTranslation.picked_up_by_staff_detail_occupant_office : translate.commonTranslation.picked_up_by_staff_detail_occupant
      })
    }
    if (ret.length === 1) {
      Template.instance().hasOneDelivery.set(true)
      Template.instance().singleDelivery = ret[0].id
    } else {
      Template.instance().hasOneDelivery.set(false)
      Template.instance().singleDelivery = null
    }
    // updateSubmit(Template.instance())
    return ret
  },
  getClassesSubmit: function () {
    let classes = 'classic-service-btn btn btn-red-confirm'
    if (!Template.instance().canSubmit.get()) {
      classes += ' disabled'
    }
    if (this.form.price && this.form.price !== '0' && this.form.price !== 0) {
      classes += ' margin-top-23'
    }
    return classes
  },
  getLabel: function () {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    let label = ''
    if (this.form.price && this.form.price !== '0' && this.form.price !== 0) {
      label = translate.commonTranslation.add_to_cart
    } else {
      label = translate.commonTranslation.validate
    }
    return label
  },
  priceIsNotNull: (price) => {
    return price && price !== '0' && price !== 0
  },
  onDescChange: () => {
    const t = Template.instance()

    return () => (value) => {
      t.form.set('special_request', value)
    }
  },
  getDescriptionValue: () => {
    const t = Template.instance()
    return t.form.get('special_request')
  },
  shouldDisplaySecondBloc: (price, useTimeSlot) => {
    const t = Template.instance()

    const hasOneDelivery = t.hasOneDelivery.get()
    return !hasOneDelivery || !!useTimeSlot || (price && price !== '0' && price !== 0)
  },
  getDateTimeComponent: () => DateTime,
  getDatePlaceholder: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    return {
      placeholder: translate.commonTranslation.please_select_date,
      readOnly: true
    }
  },
  handleChangeDate: function () {
    const t = Template.instance()
    const service = this.form

    return (date) => {
      if (date.isSameOrAfter(moment(), 'day')) {
        t.form.set({ 'day_selected': date.format('DD/MM/YYYY'), 'time_slot': '' })
        const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
      }
    }
  },
  getLocal: () => {
    return FlowRouter.getParam('lang') || 'fr'
  },
  isValidDate: (openedDays) => {
    const avilableDays = Object.keys(openedDays).filter(e => !!openedDays[e].length)
    return () => (current) => {
      if (current.isAfter(moment().subtract(1, 'day'))) {
        const currentDay = current.locale('en').format('dddd').toLowerCase()
        return avilableDays.includes(currentDay)
      }
      return false
    }
  },
  getTimeSlots: (service, selected) => {
    const t = Template.instance()
    const now = moment()
    const isSelectedToday = selected === now.format('DD/MM/YYYY')
    const nowHour = isSelectedToday && parseInt(now.format('HH'))
    const nowMinutes = isSelectedToday && parseInt(now.format('mm'))
    const daySelected = moment(selected, 'DD/MM/YYYY').locale('en').format('dddd').toLowerCase()
    const hours = service.openedDays[daySelected]

    if (hours) {
      let slotDuration = parseInt(service.time_slot_duration)
      const scale = service.time_slot_scale

      let mappedHours = []
      if (scale === 'days') {
        slotDuration = (slotDuration * 60) * 24
      } else if (scale === 'hours') {
        slotDuration = (slotDuration * 60)
      }
      hours.forEach(interval => {
        let start = interval.start.split(':')
        let end = interval.end.split(':')

        start[0] = parseInt(start[0])
        start[1] = parseInt(start[1])
        end[0] = parseInt(end[0])
        end[1] = parseInt(end[1])

        const endStamp = moment(`${selected} ${end[0]}:${end[1]}`, 'DD/MM/YYYY HH:mm').add(slotDuration, 'minutes')
        while (start[0] <= end[0]) {
          if (isSelectedToday && start[0] < nowHour) {
            start[0]++
          } else {
            while (start[1] < 60) {
              const nextSlot = moment(`${selected} ${start[0]}:${start[1]}`, 'DD/MM/YYYY HH:mm').add(slotDuration, 'minutes')
              const nextNextSlot = moment(nextSlot).add(slotDuration, 'minutes')
              if (nextNextSlot > endStamp) return

              const currentTimeSlot = moment(`${start[0]}:${start[1]}`, 'HH:mm').format('HH:mm')
              if (!((reservedTimeSlots && reservedTimeSlots[currentTimeSlot] >= parseInt(interval.simultaneous_time_slot)) || (isSelectedToday && start[0] === nowHour && start[1] <= nowMinutes))) {
                mappedHours.push(moment(start[0], 'HH').format('HH') + ':' + moment(start[1], 'mm').format('mm'))
              }
              const formatNextSlot = nextSlot.format('HH:mm').split(':')
              start[0] = parseInt(formatNextSlot[0])
              start[1] = parseInt(formatNextSlot[1])
            }
          }
        }
      })
      return mappedHours
    }
  },
  isSelectedSlot: (timeSlot) => {
    const t = Template.instance()

    return timeSlot === t.form.get('time_slot')
  },
  onSelectTimeSlot: function (timeSlot) {
    const t = Template.instance()
    const service = this.form

    return () => () => {
      t.form.set({ time_slot: timeSlot })
      const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
    }
  },
})
