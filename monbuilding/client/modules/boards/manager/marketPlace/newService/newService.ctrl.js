/* globals $ */

import { Template } from 'meteor/templating'
import { CommonTranslation } from '/common/lang/lang'
import { ReactiveDict } from 'meteor/reactive-dict'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { MarketServiceTypes, TermsOfServices } from '/common/collections/marketPlace'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { MarketPlaceFiles } from '/common/userFiles'
import _ from 'lodash'
import { sAlert } from 'meteor/juliancwirko:s-alert'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import { Meteor } from 'meteor/meteor'

import { payzenApiId } from '/common/collections/API'

Template.manager_marketPlace_newService.onCreated(function () {
  this.servicesHandler = Meteor.subscribe('market_place_create_service')

  Meteor.subscribe('get-terms-of-services_manager')
  this.formItem = new ReactiveDict()
  this.fm = new FileManager(MarketPlaceFiles)
  this.formItem.setDefault({
    type: null,
    title: '',
    description: '',
    third_party: false,
    link_third_party: '',
    price: '',
    quantity_available: '',
    unlimited_quantity: false,
    vat: '20',
    pick_up: false,
    drop_off: false,
    delivered_by_staff: false,
    picked_up_by_staff: false,
    pick_up_location: '',
    drop_off_location: '',
    pay_on_platform: true,
    pay_outside_platform: false,
    payment_location: '',
    use_time_slot: false,
    same_opened_hours: false,
    allow_recurring_service: false,
    time_slot_scale: 'minutes',
    time_slot_duration: '15',
    product_code: ''
    // simultaneous_time_slot: '1',
  })
  this.globalHours = new ReactiveVar([])
  this.openedDays = new ReactiveDict()
  this.openedDays.setDefault({
    monday: [],
    tuesday: [],
    wednesday: [],
    thursday: [],
    friday: [],
    saturday: [],
    sunday: [],
  })
  this.edited_cgv = new ReactiveVar(null)
  this.isMore = new ReactiveVar(false)
  this.files = new ReactiveVar([])
  this.canSubmit = new ReactiveVar(false)
  this.initTimePicker = () => {
    $('.number-picker').datetimepicker({
      locale: 'fr',
      format: "HH:mm",
      widgetPositioning: {
        vertical: 'bottom',
        horizontal: 'auto'
      }
    })
  }
})

function updateSubmit (t) {
  const form = t.formItem.all()
  const files = t.files.get()
  const edited_cgv = t.edited_cgv.get()
  const existingCgv = TermsOfServices.findOne({ condoId: t.data.selectedCondoId })
  const openedDays = t.openedDays.all()
  const globalHours = t.globalHours.get()

  const canSubmit =
    !!form.type &&
    !!form.title &&
    !!form.description &&
    !!files && !!files.length &&
    (form.third_party ? true : (edited_cgv !== null ? edited_cgv.length > 0 : (!!existingCgv && !!existingCgv.value && existingCgv.value.length > 0)))

  const canSubmitThirdParty = !!form.link_third_party
  const canSubmitOther =
    (!!form.pick_up || !!form.drop_off || !!form.delivered_by_staff || !!form.picked_up_by_staff) &&
    (form.pick_up ? !!form.pick_up_location : true) &&
    (form.drop_off ? !!form.drop_off_location : true) &&
    ((!form.price || form.price === '0' || form.price === 0) || (form.pay_on_platform || (form.pay_outside_platform && !!form.payment_location))) &&
    (form.unlimited_quantity || (!!form.quantity_available && form.quantity_available !== 0 && form.quantity_available !== '0'))

  const canSubmitWithTimeSlot = form.use_time_slot
    ? ((form.same_opened_hours
      ? (!!globalHours.length)
      : (true))
      && (
        !!openedDays.monday.length ||
        !!openedDays.tuesday.length ||
        !!openedDays.wednesday.length ||
        !!openedDays.thursday.length ||
        !!openedDays.friday.length ||
        !!openedDays.saturday.length ||
        !!openedDays.sunday.length
      ))
    : true

  const thisCondo = Condos.findOne({ _id: t.data.selectedCondoId })
  const settings = thisCondo && thisCondo.settings && thisCondo.settings.accounting
  let canSubmitAccountingOption = true
  if (!!settings && settings.product_code !== undefined) {
    canSubmitAccountingOption = settings.product_code ? !!form.product_code : true
  }

  t.canSubmit.set(canSubmit && (form.third_party ? canSubmitThirdParty : canSubmitOther) && canSubmitWithTimeSlot && canSubmitAccountingOption)
}

Template.manager_marketPlace_newService.onRendered(function () {
})

Template.manager_marketPlace_newService.events({
  'click #backbutton': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.marketPlace.tab', { lang, tab: 'services' })
  },
  'click .saveItem': (e, t) => {
    const form = t.formItem.all()
    const openedDays = t.openedDays.all()
    const globalHours = t.globalHours.get()
    const __files = t.files.get()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoId = Template.currentData().selectedCondoId

    if (t.canSubmit.get()) {
      $('.saveItem').button('loading')
      t.fm.setCustomFields({condoId})

      t.fm.batchUpload(__files)
        .then(results => {
          let methodName = ''
          let sendForm = {}
          if (form.third_party) {
            methodName = 'createNewMarketService_thirdParty'
            sendForm = _.pick(form, ['title', 'type', 'description', 'link_third_party', 'price', 'product_code'])
          } else {
            methodName = 'createNewMarketService_classic'
            sendForm = _.pick(form, ['title', 'type', 'description', 'price', 'quantity_available', 'unlimited_quantity', 'vat', 'pick_up', 'drop_off', 'delivered_by_staff', 'picked_up_by_staff', 'pick_up_location', 'drop_off_location', 'pay_on_platform', 'pay_outside_platform', 'payment_location', 'use_time_slot', 'same_opened_hours', 'allow_recurring_service', 'time_slot_scale', 'time_slot_duration', 'product_code'])
          }
          if (t.edited_cgv.get() !== null) {
            Meteor.call('update-terms-of-uses', condoId, t.edited_cgv.get())
          }
          const timeSlots = {
            globalHours,
            openedDays
          }
          Meteor.call(methodName, condoId, sendForm, timeSlots, results, (error, result) => {
            $('.saveItem').button('reset')
            if (!error) {
              sAlert.success(translate.commonTranslation.wallet_success)
              FlowRouter.go('app.gestionnaire.marketPlace.tab', { lang, tab: 'services' })
            } else {
              sAlert.error(error)
            }
          })
          // } else {
          //   $('.saveItem').button('reset')
          //   sAlert.error('Still in progress...')
          // }
        })
        .catch((err) => {
          $('.saveItem').button('reset')
          sAlert.error(translate.commonTranslation[err.reason])
        })
    }
  },
  'input .price-input': (e, t) => {
    const val = $(e.currentTarget).val()
    if (!val || val === 0 || val === '0') {
      t.formItem.set({
        'price': val,
        'pay_on_platform': false,
        'pay_outside_platform': false,
        'payment_location': ''
      })
    } else {
      t.formItem.set('price', val)
    }
    updateSubmit(t)
  },
  'input .time-slot': (e, t) => {
    const val = $(e.currentTarget).val()
    if (!!val && parseInt(val) > 0) {
      t.formItem.set({
        time_slot_duration: val
      })
    }
    updateSubmit(t)
  },
  'input .simultaneous-time-slot': function (e, t) {
    const index = $(e.currentTarget).attr('index')
    const parent = this
    if (parent.name) {
      const actual = t.openedDays.get(parent.name)
      const value = $(e.currentTarget).val()
      if (!!value && parseInt(value) > 0) {
        actual[index].simultaneous_time_slot = value
        t.openedDays.set(parent.name, actual)
      }
    } else {
      const actual = t.globalHours.get()
      const value = $(e.currentTarget).val()
      if (!!value && parseInt(value) > 0) {
        actual[index].simultaneous_time_slot = value
        t.globalHours.set(actual)
      }
    }
    updateSubmit(t)
  },
  'input .quantity-input': (e, t) => {
    const val = $(e.currentTarget).val()
    t.formItem.set('quantity_available', val)
    updateSubmit(t)
  },
  'click .read-more-terms': (e, t) => {
    const val = t.isMore.get()
    t.isMore.set(!val)
  },
  'click .remove-hours': function (e, t) {
    const index = $(e.currentTarget).attr('index')
    const parent = this
    if (parent.name) {
      const actual = t.openedDays.get(parent.name)
      actual.splice(index, 1)
      t.openedDays.set(parent.name, actual)
    } else {
      const actual = t.globalHours.get()
      actual.splice(index, 1)
      if (actual.length === 0) {
        t.formItem.set({ same_opened_hours: false })
      }
      t.globalHours.set(actual)
    }
    updateSubmit(t)
  },
  'click .add-hours': function (e, t) {
    const index = $(e.currentTarget).attr('index')
    const parent = this
    if (parent.name) {
      const actual = t.openedDays.get(parent.name)
      actual.splice(parseInt(index) + 1, 0, {
        start: '08:00',
        end: '17:00',
        simultaneous_time_slot: '1'
      })
      t.openedDays.set(parent.name, actual)
    } else {
      const actual = t.globalHours.get()
      actual.splice(parseInt(index) + 1, 0, {
        start: '08:00',
        end: '17:00',
        simultaneous_time_slot: '1'
      })
      t.globalHours.set(actual)
    }
    updateSubmit(t)
  },
  'blur .start-hours-input': function (e, t) {
    const index = $(e.currentTarget).attr('index')
    const parent = this
    if (parent.name) {
      const actual = t.openedDays.get(parent.name)
      const value = $(e.currentTarget).val()
      actual[index].start = value
      t.openedDays.set(parent.name, actual)
    } else {
      const actual = t.globalHours.get()
      const value = $(e.currentTarget).val()
      actual[index].start = value
      t.globalHours.set(actual)
    }
    updateSubmit(t)
  },
  'blur .end-hours-input': function (e, t) {
    const index = $(e.currentTarget).attr('index')
    const parent = this
    if (parent.name) {
      const actual = t.openedDays.get(parent.name)
      const value = $(e.currentTarget).val()
      actual[index].end = value
      t.openedDays.set(parent.name, actual)
    } else {
      const actual = t.globalHours.get()
      const value = $(e.currentTarget).val()
      actual[index].end = value
      t.globalHours.set(actual)
    }
    updateSubmit(t)
  }
})

const inputCheckbox = [
  'third_party',
  'unlimited_quantity',
  'pick_up',
  'drop_off',
  'delivered_by_staff',
  'picked_up_by_staff',
  'pay_on_platform',
  'pay_outside_platform',
  'allow_recurring_service',
  'use_time_slot'
]

Template.manager_marketPlace_newService.helpers({
  isSubscribeReady: () => {
    const servicesHandler = !!Template.instance().servicesHandler && Template.instance().servicesHandler.ready()
    return servicesHandler
  },
  getTypeOptions: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoId = Template.currentData().selectedCondoId
    const services = MarketServiceTypes.find({ condoId }).fetch()
    return services.map(s => {
      return {
        id: s.key,
        text: translate.commonTranslation[`mp_service_${s.key}`]
      }
    })
  },
  getTimeSlotScale: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    return [
      {
        id: 'minutes',
        text: translate.commonTranslation.minutes
      },
      {
        id: 'hours',
        text: translate.commonTranslation.hours
      },
      {
        id: 'days',
        text: translate.commonTranslation.days
      }
    ]
  },
  getVAT: () => {
    return [
      { id: '2.1', text: '2.1%' },
      { id: '5.5', text: '5.5%' },
      { id: '10', text: '10%' },
      { id: '20', text: '20%' },
    ]
  },
  getCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    return condoId
  },
  getForm: (key) => {
    const t = Template.instance()
    if (!!key) {
      return t.formItem.get(key)
    } else {
      let form = t.formItem.all()
      let openedDays = t.openedDays.all()
      const globalHours = t.globalHours.get()
      if (!!form.use_time_slot && form.same_opened_hours) {
        openedDays.monday = !!openedDays.monday.length ? globalHours : []
        openedDays.tuesday = !!openedDays.tuesday.length ? globalHours : []
        openedDays.wednesday = !!openedDays.wednesday.length ? globalHours : []
        openedDays.thursday = !!openedDays.thursday.length ? globalHours : []
        openedDays.friday = !!openedDays.friday.length ? globalHours : []
        openedDays.saturday = !!openedDays.saturday.length ? globalHours : []
        openedDays.sunday = !!openedDays.sunday.length ? globalHours : []
      }
      form.openedDays = openedDays
      return form
    }
  },
  onClickSameHours: () => {
    const t = Template.instance()
    return () => {
      const newValue = !t.formItem.get('same_opened_hours')
      if (newValue) {
        t.globalHours.set([{
          start: '08:00',
          end: '17:00',
          simultaneous_time_slot: '1'
        }])
        t.openedDays.set({
          monday: [{
            start: '08:00',
            end: '17:00',
            simultaneous_time_slot: '1'
          }],
          tuesday: [{
            start: '08:00',
            end: '17:00',
            simultaneous_time_slot: '1'
          }],
          wednesday: [{
            start: '08:00',
            end: '17:00',
            simultaneous_time_slot: '1'
          }],
          thursday: [{
            start: '08:00',
            end: '17:00',
            simultaneous_time_slot: '1'
          }],
          friday: [{
            start: '08:00',
            end: '17:00',
            simultaneous_time_slot: '1'
          }],
        })
      } else {
        t.globalHours.set([])
      }
      t.formItem.set({ same_opened_hours: newValue })
      updateSubmit(t)
    }
  },
  getOpenedDays: () => {
    const t = Template.instance()
    return _.map(t.openedDays.all(), (d, i) => {
      return { name: i, hours: d }
    })
  },
  onInputDays: (key) => {
    const t = Template.instance()

    return () => () => {
      if (t.openedDays.get(key).length > 0) {
        t.openedDays.set(key, [])
      } else {
        t.openedDays.set(key, [{
          start: '08:00',
          end: '17:00',
          simultaneous_time_slot: '1'
        }])
      }
      updateSubmit(t)
    }
  },
  initTimePicker: () => {
    const t = Template.instance()

    setTimeout(() => {
      t.initTimePicker()
    }, 500);
  },
  getGlobalHours: () => {
    return Template.instance().globalHours.get()
  },
  checkCGV: () => {

  },
  onInput: (key) => {
    const t = Template.instance()

    return () => (value) => {
      if (inputCheckbox.includes(key)) {
        if (key === 'unlimited_quantity') {
          t.formItem.set('quantity_available', '')
        }
        value = !t.formItem.get(key)
      }
      if (key === 'pay_on_platform' && value === true) {
        t.formItem.set('pay_outside_platform', false)
      } else if (key === 'pay_outside_platform' && value === true) {
        t.formItem.set('pay_on_platform', false)
      }
      t.formItem.set(key, value)
      updateSubmit(t)
    }
  },
  MbTextArea: () => MbTextArea,
  previewFiles: () => {
    const reactiveFiles = Template.instance().files.get()
    return reactiveFiles
  },
  onFileAdded: () => {
    const t = Template.instance()

    return (files) => {
      files = files.filter(f => {
        if (f.type.split('/')[0] !== 'image') {
          const lang = FlowRouter.getParam('lang') || 'fr'
          const translate = new CommonTranslation(lang)
          sAlert.error(translate.commonTranslation.only_image_accepted)
          return false
        } else {
          return true
        }
      })
      if (files && files.length) {
        t.files.set([
          ...t.files.get(),
          ...files
        ])
        updateSubmit(t)
      }
    }
  },
  onFileRemoved: () => {
    const t = Template.instance()

    return (fileId) => {
      t.files.set(_.filter(t.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
      updateSubmit(t)
    }
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  isMore: () => Template.instance().isMore.get(),
  editTermsCb: () => {
    const t = Template.instance()

    return (value) => {
      t.edited_cgv.set(value)
      updateSubmit(t)
    }
  },
  getTermsOfServices: (forceAll = false) => {
    const condoId = Template.currentData().selectedCondoId
    const terms = Template.instance().edited_cgv.get() !== null ? Template.instance().edited_cgv.get() : (TermsOfServices.findOne({ condoId }) || {value: ''}).value
    let displayReadMore = false
    let ret = ''
    if (terms) {
      if (terms.length > 500) {
        if (Template.instance().isMore.get() || forceAll) {
          ret = terms
        } else {
          ret = terms.substring(0, 500)
        }
        displayReadMore = true
      } else {
        displayReadMore = false
        ret = terms
      }
    }
    return { text: ret, displayReadMore }
  },
  payzenAvailable: () => {
    const t = Template.instance()
    const condoId = Template.currentData().selectedCondoId
    const integrationConfig = CondoIntegrationConfig.findOne({ condoId: condoId, integrationId: payzenApiId })
    if (!integrationConfig || !integrationConfig.active) {
      t.formItem.set({
        pay_on_platform: false,
        pay_outside_platform: true
      })
    }
    return !!integrationConfig && integrationConfig.active
  },
  getSubLabel: (label) => {
    const condoId = Template.currentData().selectedCondoId
    const isOffice = (Condos.findOne({ _id: condoId }) || { settings: { condoType: 'copro' } }).settings.condoType === 'office'
    return isOffice ? label + '_office' : label
  }
})
