/* globals $ */
/* globals history */

import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { TermsOfServices } from '/common/collections/marketPlace'

Template.termsOfServices_modal.onCreated(function () {
  this.isOpen = new ReactiveVar(false)
  this.terms = new ReactiveVar('')
})

Template.termsOfServices_modal.onRendered(function () {
})

Template.termsOfServices_modal.events({
  'hidden.bs.modal #modal_edit_terms_of_services': function (event, template) {
    template.isOpen.set(false)
    history.replaceState('', document.title, window.location.pathname)
  },
  'show.bs.modal #modal_edit_terms_of_services': function (event, template) {
    let modal = $(event.currentTarget)
    window.location.hash = '#modal_edit_terms_of_services'
    window.onhashchange = function () {
      if (window.location.hash !== '#modal_edit_terms_of_services') {
        modal.modal('hide')
      }
    }
  },
  'shown.bs.modal #modal_edit_terms_of_services': function (event, template) {
    template.isOpen.set(true)
  },
  'click .button-1': function (e, t) {
    const condoId = Template.currentData().condoId
    const value = t.terms.get()
    $(e.currentTarget).button('loading')

    // Meteor.call('update-terms-of-uses', condoId, value, (error, result) => {
    //   $(e.currentTarget).button('reset')
    //   $('#modal_edit_terms_of_services').modal('hide')
    // })
    if (typeof Template.currentData().editTermsCb === 'function') {
      Template.currentData().editTermsCb(value)
      $(e.currentTarget).button('reset')
    }
    $('#modal_edit_terms_of_services').modal('hide')
  }
})

Template.termsOfServices_modal.helpers({
  isOpen: () => Template.instance().isOpen.get(),
  MbTextArea: () => MbTextArea,
  onTermsEdit: () => {
    const t = Template.instance()

    return () => (value) => {
      t.terms.set(value)
    }
  },
  getCurrentTerms: () => {
    let terms = Template.currentData().terms
    terms = terms && terms.text
    return terms
  }
})
