
import { CommonTranslation } from "/common/lang/lang.js"
import { MarketReservations } from '/common/collections/marketPlace'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import moment from 'moment'

Template.marketPlaceReservationModal.onCreated(function () {
  this.reservationId = new ReactiveVar(null)
  this.isOpen = new ReactiveVar(false)

  this.form = new ReactiveDict()
  this.form.setDefault({
    company: null,
    diploma: null,
    school: null,
    studies: null,
    door: null,
    floor: null,
    office: null,
    wifi: null
  })
  this.bootboxOpen = new ReactiveVar(false)
})

Template.marketPlaceReservationModal.onDestroyed(() => {
})

Template.marketPlaceReservationModal.onRendered(() => {
})

Template.marketPlaceReservationModal.events({
  'hidden.bs.modal #modal_market_place_reservation': (event, template) => {
    template.isOpen.set(false)
    template.reservationId.set(null)
    Template.currentData().closeCb()
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_market_place_reservation': (event, template) => {
    template.isOpen.set(false)
    template.reservationId.set(null)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_market_place_reservation";

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_market_place_reservation") {
        modal.modal('hide');
      }
    }
  },
  'click .button-2': (e, t) => {
    e.stopPropagation();
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoId = $(e.currentTarget).data('condoid')
    const resaId = $(e.currentTarget).data('resaid')

    if (!t.bootboxOpen.get()) {
      t.bootboxOpen.set(true)
      bootbox.confirm({
        title: translate.commonTranslation.bootbox_validation_mp_service_title,
        message: translate.commonTranslation.bootbox_validation_mp_service_subtitle,
        buttons: {
          'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
          'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
        },
        callback: function (result) {
          t.bootboxOpen.set(false)
          if (result) {
            Meteor.call('archiveMarketReservation', resaId, condoId, (error) => {
              if (error) {
                sAlert.error(translate.commonTranslation.error_occured)
              }
            });
          }
        }
      })
    }
  },
  'click .button-1': (e, t) => {
    $(e.currentTarget).button('loading')

    const lang = FlowRouter.getParam('lang') || 'fr'
    const userId = $(e.currentTarget).data('userid')
    const condoId = $(e.currentTarget).data('condoid')

    if (Meteor.isManager) {
      Meteor.call('checkManagerRoomExist', [userId], condoId, function (error, result) {
        $('#modal_market_place_reservation').modal('hide')

        Meteor.setTimeout(function () {
          if (result) {
            const params = {
              lang,
              tab: 'global',
              msgId: result
            }
            FlowRouter.go('app.gestionnaire.messenger', params)
          } else {
            const params = {
              lang,
              tab: 'global'
            }
            FlowRouter.go('app.gestionnaire.messenger.new', params)
          }
        }, 500)
      })
    } else {
      $('#modal_market_place_reservation').modal('hide')
      Meteor.setTimeout(function () {
        const params = {
          lang,
          condo_id: condoId,
          module_slug: 'messenger',
          wildcard: 'manager/new'
        }
        FlowRouter.go('app.board.resident.condo.module', params)
      }, 500)
    }
  }
})

Template.marketPlaceReservationModal.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get() && Template.instance().reservationId.get()
  },
  updateData: () => {
    const reservationId = Template.currentData().reservationId
    Template.instance().reservationId.set(reservationId)
  },
  getReservation: () => {
    const reservationId = Template.currentData().reservationId
    return MarketReservations.findOne({ _id: reservationId })
  },
  getUserInfo: (userId, archivedBy) => {
    const userProfile = UsersProfile.findOne({ _id: archivedBy || userId })
    const avatar = Avatars.findOne({ _id: archivedBy || userId })
    let ppUrl = null
    let initial = ''
    let fullName = ''

    if (userProfile) {
      fullName = userProfile.firstname + ' ' + userProfile.lastname
      initial = userProfile.firstname[0] + userProfile.lastname[0]

      if (avatar) {
        ppUrl = avatar.avatar.original
      }
    }

    return {
      ppUrl,
      initial,
      fullName
    }
  },
  getRecurring: (reservation) => {
    return ''
  },
  getStatusReservation: (reservation) => {
    if (!reservation.archivedAt) {
      const today = moment().format('YYYY-MM-DD HH:mm')
      const timeSlot = reservation.use_time_slot ? (parseInt(moment(`${reservation.day_selected} ${reservation.time_slot}`, 'DD/MM/YYYY HH:mm').format('x'))) : null
      const newDayMoment = moment(timeSlot || reservation.validatedAt)
      if (newDayMoment.isBefore(today)) {
        const newDay = newDayMoment.format('YYYY-MM-DD HH:mm')
        const diffDay = moment().diff(newDay, 'milliseconds')
        const duration = moment.duration(diffDay)

        if (duration.days() > 3) {
          return 'outdated'
        }
        return 'due'
      }
      return 'upcoming'
    } else {
      return 'complete'
    }
  },
  getDueTime: (reservation) => {
    if (!reservation.archivedAt && reservation.validatedAt) {
      const today = moment().format('YYYY-MM-DD HH:mm')
      const timeSlot = reservation.use_time_slot ? (parseInt(moment(`${reservation.day_selected} ${reservation.time_slot}`, 'DD/MM/YYYY HH:mm').format('x'))) : null
      const newDayMoment = moment(timeSlot || reservation.validatedAt)
      if (newDayMoment.isBefore(today)) {
        const lang = FlowRouter.getParam('lang') || 'fr'
        const translate = new CommonTranslation(lang)
        const newDay = newDayMoment.format('YYYY-MM-DD HH:mm')
        const diffDay = moment().diff(newDay, 'milliseconds')
        const duration = moment.duration(diffDay)

        const days = duration.days()
        const hours = duration.hours()
        const minutes = duration.minutes()

        let ret = lang === 'fr' ? 'IL Y A ' : ''

        if (days === 0) {
          ret += (hours ? hours + ' ' + translate.commonTranslation.hours.toLowerCase() + ' ' : ' ') +
            (minutes ? minutes + ' ' + translate.commonTranslation.minutes.toLowerCase() : '') + translate.commonTranslation.dates_ago
        } else {
          if (days > 3) {
            ret = ''
          } else {
            ret += (days ? days + ' ' + translate.commonTranslation.days.toLowerCase() : '') + translate.commonTranslation.dates_ago
          }
        }
        return ret
      }
    }
  },
  getLocation: (reservation) => {
    if (reservation.delivered_by_staff) {
      return 'delivered_by_staff'
    }
    if (reservation.drop_off) {
      return 'drop_off'
    }
    if (reservation.pick_up) {
      return 'pick_up'
    }
    if (reservation.picked_up_by_staff) {
      return 'picked_up_by_staff'
    }
  },
  getLocationDetail: (reservation) => {
    if (reservation.picked_up_by_staff || reservation.delivered_by_staff) {
      const userId = reservation.userId
      const condoId = reservation.condoId
      const user = GestionnaireUsers.findOne({ _id: userId })
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      const condoOption = CondosModulesOptions.findOne({ condoId: condoId })
      const isOfficeProfile = condoOption && condoOption.profile && condoOption.profile.office
      if (!Meteor.isManager) {
        return isOfficeProfile ? translate.commonTranslation.your_office : translate.commonTranslation.your_room
      }

      let room = (isOfficeProfile ? translate.commonTranslation.office : translate.commonTranslation.room) + ': '

      if (user) {
        let userCondo = user.resident[0].condos.find(c => c.condoId === condoId)
        if (userCondo && userCondo.userInfo) {
          return room + (userCondo.userInfo.office || userCondo.userInfo.porte || (isOfficeProfile ? translate.commonTranslation.user_office : translate.commonTranslation.user_room))
        }
      }
      return room + (isOfficeProfile ? translate.commonTranslation.user_office : translate.commonTranslation.user_room)
    } else {
      if (reservation.drop_off) {
        return reservation.drop_off_location
      }
      if (reservation.pick_up) {
        return reservation.pick_up_location
      }
    }
  },
  getSpecialRequest: (specialRequest) => {
    return specialRequest || '-'
  },
  getLastActionDate: (archivedAt, validatedAt) => archivedAt || validatedAt,
  getFiles: (files) => {
    if (!files) return []
    const mappedFiles = files.filter(file => !file.fileId).map(file => {
      // const thisFile = MarketPlaceFiles.findOne({ _id: file })
      if (file) {
        return {
          lowRes: file.link,
          preview: file.link,
          large: file.link,
          original: file.link,
          filename: file.name,
          type: file.type.split('/')[0],
          downloadPath: file.link
        }
      }
    })
    return mappedFiles
  },
  MbFilePreview: () => MbFilePreview,
  userHasRightMessenger: (condoId) => {
    if (Meteor.isManager) {
      return Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId) && Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId)
    } else {
      return Meteor.userHasRight('messenger', 'writeToManager')
    }
  },
  getPaymentDetails: (reservation) => {
    if (reservation.payment_location) {
      if (Meteor.isManager) {
        return {
          iconClass: 'warning-icon',
          text: 'payment_has_to_be_requested'
        }
      } else {
        return {
          iconClass: 'warning-icon',
          text: 'you_will_have_to_pay',
          subText: 'payment_location',
          location: reservation.payment_location
        }
      }
    }
    return {
      iconClass: `paymentIcon-${!!reservation.cardType ? reservation.cardType : 'visa'} margin-right-16`,
      text: 'payment_has_already_proceed'
    }
  },
  isOffered: (price) => {
    return !(!!price && price !== '0' && price !== 0)
  },
  getEndTime: (reservation) => {
    let timeToAdd = parseInt(reservation.time_slot_duration)
    if (reservation.time_slot_scale === 'days') {
      timeToAdd = (parseInt(reservation.time_slot_duration) * 60) * 24
    } else if (reservation.time_slot_scale === 'hours') {
      timeToAdd = (parseInt(reservation.time_slot_duration) * 60)
    }

    const time = moment(reservation.time_slot, 'HH:mm').add(timeToAdd, 'minutes').format('HH:mm')

    return time
  }
})
