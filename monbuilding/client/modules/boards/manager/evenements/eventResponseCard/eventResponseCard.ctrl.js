/**
 * Created by kuyarawa on 07/06/18.
 */

Template.eventResponseCard.onCreated(function () {
  this.seeAll = new ReactiveVar(false)

})

Template.eventResponseCard.helpers({
  getTitle: () => {
    return Template.instance().data.label + ' (' + Template.instance().data.participants.length + ')'
  },
  participants: () => {
    if (Template.instance().seeAll.get()) {
      return Template.instance().data.participants
    }
    return _.first(Template.instance().data.participants, 3)
  },
  showSeeAll: () => {
    return Template.instance().data.participants.length > 3
  },
  toggle: () => {
    const t = Template.instance()

    return () => {
      t.seeAll.set(!t.seeAll.get())
    }
  },
  getVarTranslate: () => {
    if (Template.instance().seeAll.get() === true) {
      return 'see_less'
    } else {
      return 'see_all'
    }
  }
})
