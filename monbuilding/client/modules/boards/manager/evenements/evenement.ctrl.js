import { Template } from "meteor/templating";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { DeletePost, ModuleReservationIndex, ModuleActualityIndex, MonthOfYear, CommonTranslation } from "/common/lang/lang.js";
import moment from 'moment';
import 'moment/locale/fr';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';

let calendar;
let templ;

const toolbarOptions = [
  ['bold', 'italic', 'underline'],
  ['link', 'blockquote'],
  [{
    'list': 'ordered'
  }, {
    'list': 'bullet'
  }],
  [
  ]
];

var nEvent;
function filterPosts(elemSDate, elemEDate, template) {
  whichDay = parseInt(elemSDate.getDate()) <= parseInt(elemEDate.getDate()) || Template.instance().filterMonth.get() == elemSDate.getMonth() ? elemSDate.getDate() : elemEDate.getDate();
  selectedDay = Template.instance().filterDay.get() ? new Date(`${Template.instance().filterMonth.get() + 1}/${Template.instance().filterDay.get()}/${Template.instance().filterYear.get()}`)
    : new Date(`${Template.instance().filterMonth.get() + 1}/${whichDay}/${Template.instance().filterYear.get()}`);
  return elemSDate.valueOf() <= selectedDay.valueOf() && selectedDay.valueOf() <= elemEDate.valueOf();
};

function focusEventOnCalendar()
{
  let condoEvent = _.find(nEvent, function(e){
    return (e._id == FlowRouter.getParam("eventId"));
  });
  if (!condoEvent)
    return false;
  let startDate = moment(condoEvent.startDate, "DD/MM/YYYY");
  let calendarDay = $('span[data-cal-date=' + startDate.format("YYYY-MM-DD") + ']');
  calendarDay.parent().addClass('focus');
}

function GestionnaireCondosInCharge(gestionnaire) {
  if (gestionnaire) {
    const condoIds = _.map(_.find(_.filter(_.map(Enterprises.findOne(gestionnaire.identities.gestionnaireId), function(value, key) {
      return key == 'users' ? value : _.noop;
    }), function(elem) {
      return elem !== undefined;
    })[0], function(elem) {
      return elem.userId == Meteor.userId();
    }).condosInCharge, function(value, key) {
      return value.condoId;
    });
    if (condoIds) {
      return condoIds;
    }
  }
}

const insertDateHeader = (array) => {
  const lang = (FlowRouter.getParam("lang") || "fr");
  const translate = new ModuleReservationIndex(lang);
  let currentDate = '';
  return array.map((m) => {
    const newDay = moment(m.day).format('DDMMYY') === moment().format('DDMMYY') ? translate.moduleReservationIndex['today_book'] : moment(m.day).format('dddd, D MMM YYYY');

    let timeString = '';
    let isAfter = false;
    const start = moment(m.start);
    const end = moment(m.end);
    const startHour = (!!m.startHour && m.startHour !== '00:00' && m.startHour != -1) ? m.startHour : translate.moduleReservationIndex['all_day'];
    const endHour = (!!m.endHour && m.endHour !== '00:00' && m.endHour != -1) && m.startHour !== m.endHour ? ' - ' + end.format('HH:mm') : '';
    const startDate = (!!m.startHour && m.startHour !== '00:00' && m.startHour != -1) ? `${start.format('D MMM YYYY,')} ${m.startHour}` : start.format('D MMM YYYY');
    const endDate = ' - ' +  ((!!m.endHour && m.endHour !== '00:00' && m.endHour != -1) ? `${end.format('D MMM YYYY,')} ${m.endHour}` : end.format('D MMM YYYY'));
    if (start.format('YYMMDD') === end.format('YYMMDD')) {
      timeString = `${startHour}${endHour}`
    } else {
      const day = moment(m.day);
      if (start.format('YYMMDD') === day.format('YYMMDD')) {
        timeString = `${startHour}${endDate}`
      } else {
        isAfter = true;
        timeString = `${startDate}${endDate}`
      }
    }

    const newData = {
      ...m,
      string: timeString,
      isAfter
    };

    if (currentDate !== newDay) {
      currentDate = newDay;
      return {...newData, label: newDay}
    } else {
      return {...newData}
    }
  })
}

let translationOfWeek = null

Template.evenementsGestionnaire.onCreated(function () {
  this.oldEvent = new ReactiveVar({
    title: null,
    description: null,
    condoId: null,
    startDate: null});
  this.title = new ReactiveVar(null);
  this.description = new ReactiveVar(null);
  this.startDate = new ReactiveVar(null);
  this.endDate = new ReactiveVar(null);

  this.locate = new ReactiveVar(null);

  this.visibility = new ReactiveVar('public');
  this.participants = new ReactiveVar([]);
  this.allowComment = new ReactiveVar(false);
  this.allowAction = new ReactiveVar(false);
  this.isLimited = new ReactiveVar(false);
  this.participantLimit = new ReactiveVar(20);
  this.modalOpen = new ReactiveVar(false);

  this.eventSearch = new ReactiveVar("");
  this.description = new ReactiveVar('');
  this.viewMode = new ReactiveVar('calendar');

  this.eventState = new ReactiveDict();

  this.files = new ReactiveVar([]);
  this.existingFiles = new ReactiveVar([]);

  this.filterYear = new ReactiveVar(new Date().getFullYear());
  this.filterMonth = new ReactiveVar(new Date().getMonth());
  this.filterDay = new ReactiveVar(null);
  this.showPastille = new ReactiveVar(true);

  this.eventLength = new ReactiveVar(0);
  this.eventData = new ReactiveVar([]);
  this.condoUsers = new ReactiveVar([])

  let self = this;
  let condoIds = GestionnaireCondosInCharge(Meteor.user())
  let actuIds = []
  _.each(condoIds, function (cId) {
    let c = Condos.findOne(cId)
    if (c) {
      let actu = _.find(c.modules, function (mod) {
        return mod.name === "actuality"
      })
      if (actu) {
        actuIds.push(actu.data.actualityId)
      }
    }
  })
  console.time('actuFiles')
  this.subscribe('actuFiles', actuIds, (error) => {
    console.timeEnd('actuFiles')
  });
  console.time('eventCommentsFeedCounter')
  this.subscribe('eventCommentsFeedCounter', condoIds, (error) => {
    console.timeEnd('eventCommentsFeedCounter')
  });

  console.time('actus_gestionnaire')
  this.subscribe("actus_gestionnaire", function() {
    console.timeEnd('actus_gestionnaire')
    setTimeout(function() {
      initCalendar(self);
    }, 500)
  });
  this.condoIdFilter = new ReactiveVar('all');
  this.editingPost = new ReactiveVar(null); // used to know if the user edit (and which post) or create an actuality
  this.condoId = new ReactiveVar(null);
  this.isActuError = new ReactiveVar(false);
  this.condoError = new ReactiveVar(false);
  this.isHourError = new ReactiveVar(false);
  this.isDateError = new ReactiveVar(false);
  this.sameDateError = new ReactiveVar(false);
  this.startDateError = new ReactiveVar(false);
  this.DateFromNowError = new ReactiveVar(false);
  this.endDateError = new ReactiveVar(false);
  this.searchCondo = new ReactiveVar("");
  this.eventViewId = new ReactiveVar();
  this.open_modal = new ReactiveVar(0);
  this.fm = new FileManager(UserFiles);
  Meteor.call('setNewAnalytics', {type: "module", module: "info", accessType: "web", condoId: ''});

  this.eventId = new ReactiveVar(FlowRouter.getParam("eventId"));

  this.setUser = (ids) => {
    const users = _.uniq([...this.condoUsers.get(), ..._.map(UsersProfile.find({_id: {$in: ids}}).fetch(), (u) => {
      return {
        id: u._id,
        text: `${u.firstname} ${u.lastname}`
      }
    })], (u) => {return u.id});

    this.condoUsers.set(users);
  }
});

Template.evenementsGestionnaire.onDestroyed(function() {
  $('.tooltip').remove();
  // Meteor.call('updateAnalytics', {type: "module", module: "info", accessType: "web", condoId: ''});
})

function putInSelectedCondo(template) {
  const condo = Condos.findOne({_id: {$eq: Template.instance().condoId.get()}});
  const actu = _.filter(_.map(condo, function(value, key) {
    return key == 'modules' ? _.find(value, function(elem) {
      return elem.name == 'actuality';
    }) : _.noop;
  }), function(elem) {
    return elem != undefined;
  })[0];
  return {actuId: actu.data.actualityId, condoId: condo._id};
}

function fillActuObject(createActu, template) {
  const lang = FlowRouter.getParam("lang") || "fr"
  const tr_common = new CommonTranslation(lang)

  $(".submitActu").button('loading');
  createActu = {
    title: $('.titleActu').val(),
    locate: $('.locateActu').val(),
    description: $('.mb__textarea > textarea').val(),
    startDate: $('#startDateActu').val(),
    startHour: ($('.startHourActu').val() ? ($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.startHourActu').val() : "00:00") : '-1'),
    endDate: $('#endDateActu').val(),
    endHour: ($('.endHourActu').val() ? ($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.endHourActu').val() : "00:00") : '-1'),
    actualityId: putInSelectedCondo().actuId,
    userId: Meteor.user()._id,
    createdAt: Date.now(),
    views: [],
    history: [],
    files: [],
    condoId: template.condoId.get(),
    visibility: template.visibility.get(),
    participants: template.participants.get(),
    allowComment: template.allowComment.get(),
    allowAction: template.allowAction.get(),
    isLimited: template.isLimited.get(),
    participantLimit: template.participantLimit.get()
  };
  if (!createActu.actualityId || createActu.condoId == 'all') {
    $(".submitActu").button('reset');
    return;
  }

  const __files = template.files.get();

  template.fm.batchUpload(__files)
    .then(results => {
      if (template.editingPost.get()) {
        createActu.isUpdate = new Date();
        createActu.updatedAt = Date.now();
        createActu.files = [
          ...template.existingFiles.get(),
          ..._.map(results, (f) => f._id),
        ];

        Meteor.call('module-actu-edit-post', template.editingPost.get(), createActu, function(error) {
          if (error) {
            sAlert.error(error);
            $(".submitActu").button('reset');
          }
          else {
            $('#modalAddEvenement').modal('hide');
          }
          initCalendar(template);
        });
      } else {
        createActu.files = _.map(results, (f) => f._id);

        Meteor.call('module-actu-create-post', createActu, function(error) {
          if (error) {
            sAlert.error(error);
            $(".submitActu").button('reset');
          }
          else {
            $('#modalAddEvenement').modal('hide');
          }
          initCalendar(template);
        });
      }
    })
    .catch((err) => {
      $(".submitActu").button('reset');
      sAlert.error(tr_common.commonTranslation[err.reason]);
    });
}

function isDateHourError(template) {
  if (moment($('#startDateActu').val(), 'DD/MM/YYYY').set({ hour: parseInt(($('.startHourActu').val()).split(':')[0], 10), minute: (parseInt(($('.startHourActu').val()).split(':')[1], 10) - 2)}).isBefore(moment())) {
    template.DateFromNowError.set(true);
    return template;
  }
  if ($('#endDateActu').val() && $('.endHourActu').val()) {
    if (moment($('#endDateActu').val(), 'DD/MM/YYYY').isSameOrAfter(moment($('#startDateActu').val(), 'DD/MM/YYYY'))) {
      if ($('#endDateActu').val() == $('#startDateActu').val()) {
        if ($('.startHourActu').val() > $('.endHourActu').val())
          template.isHourError.set(true);
        else if ($('.startHourActu').val() == $('.endHourActu').val())
          template.sameDateError.set(true);
      }
    }
    else
      template.isDateError.set(true);
  }
  if (!!$('#endDateActu').val() && moment($('#startDateActu').val(), 'DD/MM/YYYY').isAfter(moment($('#endDateActu').val(), 'DD/MM/YYYY'))) {
    template.isHourError.set(true);
  }
  return template;
}

function initDatePicker(template, eventDates, lang) {
  const self = template;
  const selector = $("#mini-calendar");
  selector.datepicker("destroy");
  selector.datepicker({
    maxViewMode: 0,
    language: lang,
    weekStart: 1,
    autoclose: true,
    todayHighlight: true,
    defaultDate: '0d',
    beforeShowDay: function(date) {
      const formattedDate = moment(date).add(12, 'h').startOf('day').format('x');
      if (_.isArray(eventDates) && eventDates.includes(formattedDate)) {
        return {
          classes: 'booked-date'
        };
      }
    },
    templates: {
      leftArrow: '<i class="fa fa-chevron-left"></i>',
      rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
  })
    .on('changeMonth', function(event) {
      self.filterYear.set(new Date(event.date).getFullYear());
      self.filterMonth.set(new Date(event.date).getMonth());
      self.filterDay.set(firstActuInMonth(self));
      self.showPastille.set(!self.showPastille.get());
    })
    .on("changeDate", function(event) {
      self.filterDay.set(new Date(event.date).getDate());
      self.showPastille.set(!self.showPastille.get());
      self.filterMonth.set(new Date(event.date).getMonth());
    });
  selector.datepicker("setDate", new Date());
  self.showPastille.set(!self.showPastille.get());
  setTimeout(function () {
    self.filterDay.set(firstActuInMonth(self));
  }, 30);
}

function firstActuInMonth(template) {
  if (template.filterMonth.get() == new Date().getMonth() && template.filterYear.get() == new Date().getFullYear()) return new Date().getDate()
  const posts = _.sortBy(_.filter(ActuPosts.find({condoId: FlowRouter.current().params.condo_id, startDate: {$ne: ''}}).fetch(), function(elem) {
    elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
    elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
    return elemSDate.getMonth() == template.filterMonth.get() && elemSDate.getFullYear() == template.filterYear.get()
      || elemEDate.getMonth() == template.filterMonth.get() && elemEDate.getFullYear() == template.filterYear.get();
  }), function(elem) {
    return new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`)
      && new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
  });
  if (!posts.length)
    return 1;
  return posts[0].startDate.split('/')[1] == template.filterMonth.get() + 1 ? posts[0].startDate.split('/')[0] : posts[0].endDate.split('/')[0];
};

function resetErrorHandler(template) {
  $('.buttonCondoView').removeClass('errorModalInput');
  $('.titleActu').removeClass('errorModalInput');
  $('.locateActu').removeClass('errorModalInput');
  $('.mb__textarea > textarea').val("");
  // $($('.descrActu').children()[2]).removeClass('errorModalInput');
  $('#startDateActu').removeClass('errorModalInput');
  $('.startHourActu').removeClass('errorModalInput');
  $('#endDateActu').removeClass('errorModalInput');
  $('.endHourActu').removeClass('errorModalInput');
  template.isActuError.set(false);
  template.isHourError.set(false);
  template.isDateError.set(false);
  template.condoError.set(false);
  template.sameDateError.set(false);
  template.endDateError.set(false);
  template.startDateError.set(false);
  template.DateFromNowError.set(false);
}

function constructAttachmentElement(data)  {
  var progress = $('<span></span>')
    .addClass('progress')
    .css('width', '0%')

  var meter = $('<div></div>')
    .addClass('meter')
    .append(progress)

  var iconFile = $('<span></span>')
    .addClass('icon-file')
    .append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'))

  var checkmark = $('<span class="checkmark"></span>')
    .append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'))

  var fileSize = $('<span class="file-size"></span>')
    .html(data.size)
    .append(checkmark)

  var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name)

  var fileRemove = $(`<span class="remove-item">`)
    .append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'))

  var file = $('<div></div>')
    .addClass('file')
    .append(iconFile)
    .append(fileName)
    .append(fileSize)
    .append(fileRemove)
    .append(meter)

  var list = $('<li></li>')
    .attr('id', data.id)
    .addClass('attachment-item')
    .append(file)

  return list;
}

function initCalendar(templ, l) {

  // let focusOn = ActuPosts.findOne({
  //     _id: FlowRouter.getParam("eventId")
  // },
  // {
  //     fields: {startDate: true}
  // });
  // var focusMoment = moment(focusOn.startDate, "DD/MM/YYYY");
  const lang = !!l ? l: (FlowRouter.getParam("lang") || "fr");
  const translate = new ReactiveVar(new MonthOfYear(lang).month).get();

  let month = [];
  month['January'] = translate.long[0];
  month['February'] = translate.long[1];
  month['March'] = translate.long[2];
  month['April'] = translate.long[3];
  month['May'] = translate.long[4];
  month['June'] = translate.long[5];
  month['July'] = translate.long[6];
  month['August'] = translate.long[7];
  month['September'] = translate.long[8];
  month['October'] = translate.long[9];
  month['November'] = translate.long[10];
  month['December'] = translate.long[11];

  const commonTranslate = new CommonTranslation(lang)
  calendar = $("#custom-calendar").calendar(
    {
      //// monbuilding/public/calendar
      tmpl_path: "/calendar/",
      events_source: (start, end) => {
        let startDate = moment(start);
        let endDate = moment(end);
        // create a new moment based on the original one
        let startFix = startDate.clone();
        let endFix = endDate.clone();

        const utcOffset = new Date().getTimezoneOffset();
        // console.log(-1 * utcOffset, 7 * 60)
        const msOffset = utcOffset * 60000 ;
        // console.log(utcOffset, 'offset')
        // console.log(startFix.utcOffset(), 'moment offset')
        // change the time zone of the new moment
        // startFix.tz(moment.tz.guess());
        // endFix.tz(moment.tz.guess());

        // shift the moment by the difference in offsets
        // startFix.add(utcOffset, 'minutes');
        // endFix.add(utcOffset, 'minutes');
        startFix.add(startFix.utcOffset(), 'minutes');
        endFix.add(endFix.utcOffset(), 'minutes');

        // use +startFix.format('x') and +endFix.format('x') to handle query
        // this will fix timezone issue

        let query = {
          $or: [
            {"startUtcTimestamp": {$gte: +startFix.format('x'), $lt: +endFix.format('x')}},
            {"endUtcTimestamp": {$gte: +startFix.format('x'), $lt: +endFix.format('x')}},
            {"startUtcTimestamp": {$lt: +startFix.format('x')}, "endUtcTimestamp": {$gte: +endFix.format('x')}}
          ]
        };

        if (templ !== null) {
          if (templ.eventSearch && templ.eventSearch.get() != '') {
            let regexp = new RegExp(templ.eventSearch.get(), "i");
            query = {
              $and: [
                {
                  $or: [
                    {"startUtcTimestamp": {$gte: +startFix.format('x'), $lt: +endFix.format('x')}},
                    {"endUtcTimestamp": {$gte: +startFix.format('x'), $lt: +endFix.format('x')}},
                    {"startUtcTimestamp": {$lt: +startFix.format('x')}, "endUtcTimestamp": {$gte: +endFix.format('x')}}
                  ]
                },
                {
                  $or: [
                    {"title": regexp},
                    {"location": regexp},
                    {"description": regexp},
                  ]
                }
              ]
            };
          }
          let condo = templ.condoIdFilter.get();
          if (condo !== 'all') {
            query.condoId = condo;
          }
        }
        let resources = ActuPosts.find(query).fetch();
        let events = _.map(resources, (e) => {
          if (Meteor.userHasRight("actuality", "see", e.condoId))
          {
            let condo = Condos.findOne(e.condoId);
            let user = GestionnaireUsers.findOne(e.userId);
            if (!user) {
              let gUser = Enterprises.findOne({"users.userId": e.userId});
              if (gUser) {
                user = _.find(gUser.users, function(u) {return u.userId == e.userId});
                user.profile.firstname = user.firstname;
                user.profile.lastname = user.lastname;
              } else {
                user = {
                  profile: {
                    firstname: 'User',
                    lastname: 'Not Found'
                  }
                }
              }
            }
            if (condo && user) {
              e.id = e._id;
              e.address = (condo.name == condo.info.address ?
                (condo.info.id != "-1" ? condo.info.id + ' - ' + condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city
                  : condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city) :
                (condo.info.id != "-1" ? condo.info.id + ' - ' + condo.name + '<br>' + condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city
                  : condo.name + '<br>' + condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city));
              e.author = user.profile.firstname + ' ' + user.profile.lastname;
              e.start = +e.startUtcTimestamp + msOffset;
              e.end = +(e.endUtcTimestamp ? e.endUtcTimestamp : e.startUtcTimestamp + 1) + msOffset;
              e.humanStartDate = e.startDate;
              e.humanEndDate = e.endDate;
              e.humanStartTime = e.startHour;
              e.humanEndTime = e.end ? e.endHour : null;
              // e.editRight = Meteor.userHasRight("actuality", "write", e.condoId) && e.userId === Meteor.user()._id;
              // e.deleteRight = Meteor.userHasRight("actuality", "delete", e.condoId);

              return (e);
            }
          }
          return false;
        });
        nEvent = events;

        return events;
      },
      display_week_numbers: false,
      weekbox: false,
      // language: 'fr-FR',
      onAfterViewLoad: function(view) {
        $('.monthname .m').text(month[this.getMonth()]);
        $('.monthname .y').text(this.getYear());
      },
      translation: commonTranslate,
      views: {
        year: {
          slide_events: 1,
          enable: 0
        },
        month: {
          slide_events: 1,
          enable: 1
        },
        week: {
          enable: 0
        },
        day: {
          enable: 0
        }
      }
    });
  focusEventOnCalendar();
  $('body').tooltip({
    selector: '[rel="tooltip"]'
  });
}
Template.evenementsGestionnaire.events({
  'show.bs.modal #modalAddEvenement': function(event, template) {
    let modal = $(event.currentTarget);
    const oldEvent = ActuPosts.findOne(template.editingPost.get());
    window.location.hash = "#modalAddEvenement";
    // template.condoId.set(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || 'all');
    window.onhashchange = function() {
      if (location.hash !== "#modalAddEvenement") {
        modal.modal('hide');
      }
    }
    $(".submitActu").button('reset');

    if (!template.modalOpen.get()) {
      template.modalOpen.set(true)
      if (!!template.editingPost.get()) {
        if (!!oldEvent) {
          template.title.set(oldEvent.title);
          template.description.set(oldEvent.description);
          template.startDate.set(oldEvent.startDate);
          template.endDate.set(oldEvent.endDate);
          template.locate.set(oldEvent.locate);
          template.condoId.set(oldEvent.condoId);
          template.visibility.set(oldEvent.visibility);
          template.participants.set(oldEvent.participants);
          template.allowComment.set(oldEvent.allowComment);
          template.allowAction.set(oldEvent.allowAction);
          template.isLimited.set(oldEvent.isLimited);
          template.participantLimit.set(oldEvent.participantLimit);
          template.existingFiles.set(oldEvent.files);
          Session.set('selectedCondo', oldEvent.condoId);
        }
      } else {
        template.participants.set([]);
        template.visibility.set('public');
        template.allowComment.set(false);
        template.allowAction.set(false);
        template.isLimited.set(false);
      }
    }
  },
  'keyup .locateActu': function (event, template) {
    template.locate.set(event.currentTarget.value)
  },
  'hidden.bs.modal #modalAddEvenement': function(event, template) {
    $('#mb-condo-picker').removeClass('mb-condo-picker-error');
    $('.titleActu').removeClass('mb-input-error');
    $('.info-msg-box').removeClass('mb-input-error');

    template.modalOpen.set(false)
    if (!Template.instance().editingPost.get()) {
      if ($('#attachments li')[0]) {
        $('#attachments li').remove();
      }
      $('.buttonCondoView').val('');
      $('.titleActu').val("");
      $('.locateActu').val("");
      // $('.ql-editor').html('');
      // template.editor.root.innerHTML = "";
      template.fm.clearFiles();
      $('#startDateActu').val("");
      $('.startHourActu').val("");
      $('#endDateActu').val("");
      $('.endHourActu').val("");
    }
    template.title.set(null);
    template.description.set(null);
    template.startDate.set(null)
    template.endDate.set(null)
    template.files.set([]);
    template.existingFiles.set([]);
    template.editingPost.set(null)
    $(".submitActu").button('reset');
    resetErrorHandler(template);
    history.replaceState('', document.title, window.location.pathname);
  },
  'click .submitActu': function(event, template) {
    const descField = $('.mb__textarea > textarea');
    descField.parents('.info-msg-box').removeClass('mb-input-error');

    template.isActuError.set(false);
    template.isHourError.set(false);
    template.isDateError.set(false);
    template.sameDateError.set(false);
    template.DateFromNowError.set(false);
    template.startDateError.set(false);
    template.endDateError.set(false);
    $(".submitActu").button('chargement');
    let createActu = {};

    if (Template.instance().condoId.get() == 'all') {
      $('#mb-condo-picker').addClass('mb-condo-picker-error');
      template.condoError.set(true);
      $(".submitActu").button('reset');
    }
    else {
      $('#mb-condo-picker').removeClass('mb-condo-picker-error');
      template.condoError.set(false);
    }
    if (!$('.titleActu').val()) {
      $('.titleActu').addClass("mb-input-error");
      template.isActuError.set(true);
      $(".submitActu").button('reset');
    }
    else {
      $('.titleActu').removeClass("mb-input-error");
      template.isActuError.set(false);
    }
    if (descField.val().trim() === '') {
      descField.parents('.info-msg-box').addClass('mb-input-error');
      template.isActuError.set(template.isActuError.get() & 0b10);
      $(".submitActu").button('reset');
    }
    // if (!$('.descrActu')[0].children[2].textContent) {
    // 	$($('.descrActu').children()[2]).addClass("errorModalInput");
    // 	template.isActuError.set(true);
    // 	$(".submitActu").button('reset');
    // }
    // else {
    // 	$($('.descrActu').children()[2]).removeClass("errorModalInput");
    // 	template.isActuError.set(false);
    // }
    if (!$('#startDateActu').val()) {
      template.startDateError.set(true);
      $('#startDateActu').addClass('errorModalInput');
      $(".submitActu").button('reset');
    }
    else {
      template.startDateError.set(false);
      $('#startDateActu').removeClass('errorModalInput');
    }
    if (template.startDateError.get() == false && template.endDateError.get() == false) {
      if ($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') && !$('#startDateActu').val())
        $('#startDateActu').val(`${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`);
      if ($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') && !$('#endDateActu').val())
        $('#endDateActu').val(`${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`);
      if ($('#startDateActu').val())
        template = isDateHourError(template);
    }
    if (template.sameDateError.get() == true) {
      $('#endDateActu').val('');
      $('.endHourActu').val('');
    }
    if (template.isActuError.get() === false
      && template.isHourError.get() == false
      && template.isDateError.get() == false
      && template.startDateError.get() == false
      && template.endDateError.get() == false
      && template.DateFromNowError.get() == false
      && template.condoError.get() == false) {
      $('.titleActu').removeClass('mb-error-input');
      $('.mb__textarea > textarea').removeClass("mb-error-input");
      $('.buttonCondoView').removeClass('errorModalInput');
      $('#startDateActu').removeClass('errorModalInput');
      $('#endDateActu').removeClass('errorModalInput');
      fillActuObject(createActu, template, Template.currentData().type);
    }
    else {
      $(".submitActu").button('reset');
    }
  },
  'click li .timeSlotStart': function(event, template) {
    $('.startHourActu').val(this.toString());
  },
  'click li .timeSlotEnd': function(event, template) {
    $('.endHourActu').val(this.toString());
  },
  'click .cal-prev': function(event, template) {
    calendar.navigate('prev');
    focusEventOnCalendar();
  },
  'click .cal-next': function(event, template) {
    calendar.navigate('next');
    focusEventOnCalendar();
  },
  'click .open-file': function(event, template) {
    const id = event.currentTarget.getAttribute('data-id');
    const title = event.currentTarget.getAttribute('data-title');

    template.$("#modal_file .modal-title").html(title);
    Template.instance().eventViewId.set(id);
  },
  'click .btn-week-toggle.expand': function(event, template) {
    template.$(".expanded .cal-cell1").css('height', '');
    template.$(".collapsed").removeClass('collapsed');
    template.$(".expanded").removeClass('expanded');
    template.$(".btn-week-toggle.expand").css('display', 'block');
    template.$(".btn-week-toggle.colapse").css('display', 'none');
    let index = event.currentTarget.getAttribute("data-week-id");
    template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").css('display', 'none');
    template.$(".btn-week-toggle.colapse[data-week-id='"+index+"']").css('display', 'block');

    let heights = template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").parent().parent().find('.event-list-custom').map(function ()
    {
      return $(this).height() + 30;
    }).get()
    maxHeight = Math.max.apply(null, heights);
    template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").parent().parent().siblings().addClass('collapsed');
    template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").parent().parent().addClass('expanded');
    if (maxHeight > 172) {
      template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").parent().parent().find('.cal-cell1').css('height', maxHeight);
    }
  },
  'click .btn-week-toggle.colapse': function(event, template) {
    template.$(".expanded .cal-cell1").css('height', '');
    let index = event.currentTarget.getAttribute("data-week-id");
    template.$(".btn-week-toggle.colapse[data-week-id='"+index+"']").css('display', 'none');
    template.$(".btn-week-toggle.expand[data-week-id='"+index+"']").css('display', 'block');
    template.$(".collapsed").removeClass('collapsed');
    template.$(".expanded").removeClass('expanded');
  },
  'change #fileUploadInput': function(event, template) {
    var attachmentListUL = $('#attachments');
    if (event.currentTarget.files) {
      for (let i = 0; i < event.currentTarget.files.length; i++) {
        var data = {};

        data.name = event.currentTarget.files[i].name;
        data.size = (event.currentTarget.files[i].size / 1000).toFixed(2) + ' kb';
        data.id = 'file-' + event.currentTarget.files[i].size;

        template.fm.insert(event.currentTarget.files[i]);
        attachmentListUL.append(constructAttachmentElement(data));

        var progress = $(document).find('#file-' + event.currentTarget.files[i].size +' .progress')
        var elapsed = 0;

        var timer = setInterval(function() {
          var wrapper = $('#file-' + event.currentTarget.files[i].size);

          if (elapsed < 100) {
            elapsed += 5;
          }
          if (template.fm.getFileList()[i].done) {
            elapsed = 100;
            wrapper.find('.meter').remove();
            wrapper.addClass('completed');
            clearInterval(timer);
          }

          progress.animate({
            width: elapsed + '%'
          }, 200);
        }, 100)
      }
    }
  },
  'click .remove-item': function(event, template) {
    let idx = parseInt(event.currentTarget.getAttribute("index"));
    for (let i = 0; i != template.fm.getFileList().length; ++i) {
      if ($(event.currentTarget.parentElement)[0].innerText.match(template.fm.getFileList()[i].file.name)) {
        index = i;
        break;
      }
    }
    $(event.currentTarget.parentElement.parentElement).remove();
    template.fm.remove(index);
    $("#fileUploadInput").val('');
    initCalendar(template);
  },
  'click [name="attachmentWrapper"]': function(event, template) {
    var fileInput = $('#fileUploadInput');
    fileInput.click();
  },
  'click .cal-cell1': function(e, t) {
    $(".tooltip").tooltip("hide");
  },
  'click #back-btn': function (e, t) {
    t.eventId.set(undefined);
    const lang = FlowRouter.getParam("lang") || "fr"
    FlowRouter.go('app.gestionnaire.evenements', { lang })
    initCalendar(t)
  },
  'click .see-detail': function (e, t) {
    const id = e.currentTarget.getAttribute('data-id')
    t.eventId.set(id);
    const lang = FlowRouter.getParam("lang") || "fr";
    Meteor.call('module-actu-view-by-id', null, id);
    FlowRouter.go('app.gestionnaire.evenements.eventId', {lang, eventId: id});
  },
  'click .list-group-item': function (e, t) {
    $(".tooltip").tooltip("hide");
    const id = e.currentTarget.getAttribute('data-event-id')
    t.eventId.set(id);
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.gestionnaire.evenements.eventId', {lang, eventId: id});
  },
  'click .post-edit': function(event, template) {
    // v-- Don't remove, it's for reload the same edit
    template.editingPost.set(null);
    // set the editingPost used for the modal when you want to edit an actuality
    const id = event.currentTarget.getAttribute('data-id');
    template.editingPost.set(id);
    let a = ActuPosts.findOne(template.editingPost.get())
    template.oldEvent.set({
      title: a.title,
      description: a.description,
      condoId: a.condoId,
      startDate: a.startDate,
      endDate: a.endDate
    });
  },
  'click #new-post': function(event, template) {
    // reset the editingPost each time you click on a button linked to a modal
    template.editingPost.set(null);
  },
  'click .post-delete': function(event, template) {
    // event to delete an actuality. Update the calendar's bubbles and call the method to remove an actuality
    const id = event.currentTarget.getAttribute('data-id');
    const post = ActuPosts.findOne(id);
    const lang = (FlowRouter.getParam("lang") || "fr")
    let translation = new DeletePost(lang)
    const tr_common = new CommonTranslation(lang)
    bootbox.confirm({
      title: translation.deletePost["title_delete_post"],
      message: post.userId == Meteor.userId() ? translation.deletePost["message_delete_post"] : translation.deletePost["message_delete_post_other"],
      buttons: {
        confirm: {label: translation.deletePost["yes"], className: "btn-red-confirm"},
        cancel: {label: translation.deletePost["no"], className: "btn-outline-red-confirm"}
      },
      callback: function(res) {
        if (res) {
          template.eventId.set(undefined);
          const lang = FlowRouter.getParam("lang") || "fr";
          FlowRouter.go('app.gestionnaire.evenements', { lang });
          Meteor.call('module-actu-post-delete', id, function () {
            initCalendar(template);
            sAlert.success(tr_common.commonTranslation.success_delete_event);
          });
        }
      }
    });
  },
  'click #goToCgu': function(event, template) {
    event.preventDefault();
    event.stopPropagation();
    Session.set('backFromCgu', FlowRouter.current().path);
    let lang = FlowRouter.getParam("lang") || "fr"
    const win = window.open("/" + lang + "/services/cgu/", '_blank');
    win.focus();
  },
  'input #eventSearch': function(event, template) {
    template.eventSearch.set(event.currentTarget.value);
    initCalendar(template);
  },
  'input .titleActu': function (event, template) {
    template.title.set(event.currentTarget.value)
  },
  // 'input .mb__textarea > textarea': function (event, template) {
  //   template.description.set(event.currentTarget.value)
  // },
  'change #startDateActu': function (event, template) {
    template.startDate.set(event.currentTarget.value)
  },
  'change #endDateActu': function (event, template) {
    template.endDate.set(event.currentTarget.value)
  },


  'click .seeMoreOrLess': function(event, template) {
    let contentObj = $(event.currentTarget.parentElement).find('.info-content');
    let translation = new ModuleActualityIndex((FlowRouter.getParam("lang") || "fr"));
    contentObj.find('.ellipsisedText').toggleClass('show');
    contentObj.find('.notEllipsisedText').toggleClass('show');
    contentObj.toggleClass('ellipsis');
    $(event.currentTarget).text(contentObj.hasClass('ellipsis') ? translation.moduleActualityIndex["see_more"] : translation.moduleActualityIndex["see_less"]);
  },
  'click .change-view': (e, t) => {
    const target = $(e.currentTarget).data("target");
    $('.tooltip').remove();
    t.viewMode.set(target);
    if (target === 'calendar') {
      setTimeout(() => {
        initCalendar(t);
      }, 200)
    }
  }
});

Template.evenementsGestionnaire.rendered = function() {
  // change day to D L M M J V S
  Template.instance().isActuError.set(false);
  Template.instance().isHourError.set(false);
  Template.instance().isDateError.set(false);
  Template.instance().sameDateError.set(false);
  Template.instance().startDateError.set(false);
  Template.instance().endDateError.set(false);

  let lang = FlowRouter.getParam("lang") || "fr"
  const translation = new CommonTranslation(lang)

  // this.editor = new Quill(this.find('.editor'), {
  //   theme: 'snow',
  //   placeholder: translation.commonTranslation["description"],
  //   modules: {
  //     toolbar: toolbarOptions,
  //   },
  // });
  // this.editor.clipboard.dangerouslyPasteHTML(this.data.html || '');
};


Template.evenementsGestionnaire.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  getCondoName: (condoId) => {
    let condo = Condos.findOne(condoId);
    if (condo) {
      if (condo.info.id != -1)
        return condo.info.id + ' - ' + condo.getName();
      else
        return condo.getName();
    }
  },
  currentCondo: () => {
    let condoId = Template.instance().condoId.get();
    let condo = Condos.findOne(condoId);
    if (condo) {
      if (condo.info.id != -1)
        return condo.info.id + ' - ' + condo.getName();
      else
        return condo.getName();
    }
  },
  getCondoFilter: () => {
    return Template.instance().condoIdFilter.get()
  },
  isNumberError: () => {
    const value = Template.instance().participantLimit.get()
    return value !== null && !isInt(value);
  },
  currentCondoFilter: () => {
    let condo = Condos.findOne(Template.instance().condoIdFilter.get());
    if (condo) {
      if (condo.info.id != -1)
        return condo.info.id + ' - ' + condo.getName();
      else
        return condo.getName();
    }
  },
  dateToday: () => {
    // helper to display the today's date with the wanted format
    let date = new Date(Date('now'));
    return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
  },
  timeSlot: () => {
    // dropdown for the inputs startHour and endHour
    let slots = [];
    for (let i = 0; i < 24; i++) {
      let hour = (i < 10) ? "0" + i.toString() : i.toString();
      slots.push(hour + ":" + "00");
      slots.push(hour + ":" + "15");
      slots.push(hour + ":" + "30");
      slots.push(hour + ":" + "45");
    }
    return slots;
  },
  isActuError: () => {
    return Template.instance().isActuError.get();
  },
  isHourError: () => {
    return Template.instance().isHourError.get() || Template.instance().isDateError.get()
      || Template.instance().startDateError.get()  || Template.instance().endDateError.get()
      || Template.instance().DateFromNowError.get();
  },
  isError: () => {
    return Template.instance().isActuError.get()    || Template.instance().isHourError.get()
      || Template.instance().isDateError.get()    || Template.instance().condoError.get()
      || Template.instance().startDateError.get() || Template.instance().endDateError.get()
      || Template.instance().DateFromNowError.get();
  },
  condoError: () => {
    return Template.instance().condoError.get();
  },
  linkedErrorSentence: () => {
    let translate = new CommonTranslation(FlowRouter.getParam("lang") || "fr")
    let errorSentence = "";
    if (Template.instance().startDateError.get() == true)
      errorSentence = errorSentence + translate.commonTranslation['start_date_missing']
    else if (Template.instance().endDateError.get() == true)
      errorSentence = errorSentence + translate.commonTranslation['end_date_missing']
    else if (Template.instance().isDateError.get() == true)
      errorSentence = errorSentence + translate.commonTranslation['end_before_begin']
    else if (Template.instance().isHourError.get() == true)
      errorSentence = errorSentence + translate.commonTranslation['end_hour_before_begin']
    else if (Template.instance().DateFromNowError.get() == true)
      errorSentence = errorSentence + translate.commonTranslation['hourPast']
    return errorSentence;
  },
  ifEditPost: (input) => {
    if (Template.instance().editingPost.get()) {
      const post = ActuPosts.findOne(Template.instance().editingPost.get());
      if (post && !Template.instance().fm.getFileList().length) {
        if ($('#attachments li')[0]) {
          $('#attachments li').remove();
        }
        const files = UserFiles.find({ _id: { $in: post.files }});
        Template.instance().fm.addFiles(files.fetch());
        if (Template.instance().fm.getFileList().length) {
          const post = ActuPosts.findOne(Template.instance().editingPost.get());
          // var files = UserFiles.find({ _id: { $in: post.files }});
          var attachmentListUL = $('#attachments');
          for (let i = 0; i < files.fetch().length; i++) {
            let data = {};
            data.name = files.fetch()[i].name;
            data.size = (files.fetch()[i].size / 1000).toFixed(2) + ' kb';
            data.id = 'file-' + files.fetch()[i].size;

            attachmentListUL.append(constructAttachmentElement(data));
            var wrapper = $('#file-' + files.fetch()[i].size);
            wrapper.find('.meter').remove();
            wrapper.addClass('completed');
          }
        }
      }
      if (_.has(ActuPosts.findOne(Template.instance().editingPost.get()), input)) {
        if (input === 'startHour' || input === 'endHour') {
          if (ActuPosts.findOne(Template.instance().editingPost.get())[input] !== '-1')
            return ActuPosts.findOne(Template.instance().editingPost.get())[input];
          else
            return ''
        } else if (input === 'condoId') {
          Session.set("currentCondo", ActuPosts.findOne(Template.instance().editingPost.get())[input]);
          return ActuPosts.findOne(Template.instance().editingPost.get())[input]
        } else {
          return ActuPosts.findOne(Template.instance().editingPost.get())[input];
        }
      }
    }
    else {
      Template.instance().fm.clearFiles();
      if ($('#attachments li')[0]) {
        $('#attachments li').remove();
      }
    }
    return Template.instance()[input] ? Template.instance()[input].get() : "";
  },
  getFileList: () => {
    return Template.instance().fm.getFileList();
  },
  errorMessage: () => {
    return Template.instance().fm.getErrorMessage();
  },
  getIconOfFile: (type) => {
    let arrType = type.split("/");
    if (arrType[0] == "image") {
      if (["png", "PNG"].indexOf(arrType[1]) != -1)
        return "png.jpg";
      if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
        return "jpeg.jpg";
      if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
        return "bmp.jpg";
      if (["gif", "GIF"].indexOf(arrType[1]) != -1)
        return "gif.jpg";
      return "image.jpg";
    }
    if (arrType[0] == "application") {
      if (arrType[1] == "pdf")
        return "pdf.jpg";
      let appType = arrType[1].split(".");
      if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
        if (appType[2] == "presentation" || appType[2] == "presentationml")
          return "ppt.jpg"
        if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
          return "xlsx.jpg"
        if (appType[2] == "wordprocessingml" || appType[2] == "document")
          return "docx.jpg"
      }
    }
    if (arrType[0] == "text")
      return "txt.jpg";
    return "file.png"
  },
  topButton: (index) => {
    return 7 + index * 33;
  },
  getFiles: () => {
    const post = ActuPosts.findOne(Template.instance().eventViewId.get());
    let fileIds = post && post.files ? post.files : [];
    return UserFiles.find({ _id: { $in: fileIds.filter(file => !file.fileId) }})
  },
  getNewFiles: () => {
    const post = ActuPosts.findOne(Template.instance().eventViewId.get());
    return post && post.files ? post.files.filter(file => file.fileId) : []
  },
  getFileFromId: (fileIds) => {
    const files = UserFiles.find({ _id: { $in: fileIds }});

    return files;
  },
  getType: () => {
    return Template.currentData().type;
  },
  getNameType: () => {
    let lang = FlowRouter.getParam("lang") || "fr"
    const translation = new CommonTranslation(lang)
    return Template.currentData().type == 'actu' ? translation.commonTranslation["news"] : translation.commonTranslation["information"];
  },
  getCondoList: (condos) => {
    return Template.instance().searchCondo.get() ? _.filter(condos, function(elem) {
      let regexp = new RegExp(Template.instance().searchCondo.get(), "i");
      return elem.name.match(regexp);
    }) : condos;
  },
  editingPost: () => {
    // return the value of editingPost
    const instance = Template.instance();
    const state = instance.editingPost.get();
    if (!!state) {

    }
    return state;
  },
  editPost: () => {
    // helper to know if the user is editing a post or not
    return Template.instance().editingPost.get();
  },
  CDD_cursor: () => {
    return Meteor.listCondoUserHasRight("actuality", "write");
  },
  refreshCondoId : () => {
    const condoId = Template.currentData().selectedCondoId
    const templ = Template.instance()
    if (condoId !== templ.condoId.get()) {
      templ.condoIdFilter.set(condoId);
      initCalendar(templ);
    }
    return true
  },
  modalCondoIdCb : () => {
    var templ = Template.instance();
    return (condoId) => {
      if (condoId) {
        templ.condoId.set(condoId);
      }
    }
  },
  MbTextArea: () => MbTextArea,
  onDescChange: () => {
    const t = Template.instance();

    return () => v => t.description.set(v);
  },
  visibility: (visibility) => {
    return Template.instance().visibility.get() === visibility
  },
  changeVisibility: () => {
    const instance = Template.instance()
    return () => () => {
      const newVal = instance.visibility.get() === 'private' ? 'public' : 'private'
      instance.visibility.set(newVal)
      if (newVal === 'private') {
        instance.isLimited.set(false)
      }
    }
  },
  getValue: (key) => {
    return Template.instance()[key].get()
  },
  setValue: (key) => {
    const instance = Template.instance()

    return () => value => {
      instance[key].set(value)
    }
  },
  toggleCheck: (key) => {
    const instance = Template.instance()

    return () => () => {
      value = instance[key].get()
      instance[key].set(!value)
    }
  },
  getUsersList: () => {
    return Template.instance().condoUsers.get()
  },
  setUsers: () => {
    const instance = Template.instance()
    instance.condoUsers.set([])
    Meteor.call('getResidentsOfCondoWithRight', instance.condoId.get(), 'actuality', 'see', (err, res) => {
      if (!err) {
        Meteor.call('getGestionnaireOfCondoInChargeWithRight', instance.condoId.get(), 'actuality', 'see', (err2, res2) => {
          if (!err2) {
            instance.setUser([...res, ...res2].filter((u) => u !== Meteor.userId()))
          }
        })
      }
    });
  },
  cantSendEvent: () => {
    const instance = Template.instance()
    let title = instance.title.get()
    let description = instance.description.get()
    let condoId = instance.condoId.get()
    let startDate = instance.startDate.get()
    let endDate = instance.endDate.get()
    let visibility = instance.visibility.get()
    let locate = instance.locate.get()
    let participantLimit = instance.participantLimit.get()
    let isLimited = instance.isLimited.get()
    let allowComment = instance.allowComment.get()
    let allowAction = instance.allowAction.get()

    if (condoId && condoId !== 'all' && title && title.trim() !== '' && description && description.trim() !== '' && startDate && startDate.trim() !== '') {
      if (instance.visibility.get() === 'private' && instance.participants.get().length === 0) {
        return true
      }
      if (isLimited && (!isInt(participantLimit) || parseInt(participantLimit) < 1)) {
        return true
      }
      const old = ActuPosts.findOne(instance.editingPost.get());
      if (!!old) {
        let isSame = true;
        let filesNew = instance.files.get()
        let filesLoaded = instance.existingFiles.get()
        filesHasChange = old
        isSame = isSame && old.title === title
        isSame = isSame && old.description === description
        isSame = isSame && old.condoId === condoId
        isSame = isSame && old.startDate === startDate
        isSame = isSame && old.endDate === endDate
        isSame = isSame && !filesNew.length
        isSame = isSame && old.files.length === filesLoaded.length
        isSame = isSame && old.visibility === visibility
        isSame = isSame && old.locate === locate
        isSame = isSame && old.participantLimit === participantLimit
        isSame = isSame && old.isLimited === isLimited
        isSame = isSame && old.allowComment === allowComment
        isSame = isSame && old.allowAction === allowAction

        if (isLimited && (!isInt(participantLimit) || (parseInt(participantLimit) < 1 || !!old.participantResponse && !!old.participantResponse.going && parseInt(participantLimit) < old.participantResponse.going.length))) {
          isSame = true
        }

        return isSame // Must be edited
      } else {
        return false
      }
    } else {
      const old = ActuPosts.findOne(instance.editingPost.get());
      if (!!old) {
        let isSame = true;
        isSame = isSame && old.title === title
        isSame = isSame && old.description === description
        isSame = isSame && old.condoId === condoId
        isSame = isSame && old.startDate === startDate
        return isSame // Must be edited
      } else {
        return true
      }
    }
  },
  initCalendar: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const instance = Template.instance();
    initCalendar(instance, lang);
  },
  isCalendarMode: () => {
    return Template.instance().viewMode.get() === 'calendar'
  },
  initDatePicker: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const instance = Template.instance();
    const events = ActuPosts.find(instance.condoIdFilter.get() !== 'all' ? {condoId: instance.condoIdFilter.get()}:{}).fetch();

    const eventDate = _.uniq(_.reduce(events, (arr, e) => {
      if (!!e.endDate) {
        const start = moment(e.startDate, 'DD/MM/YYYY').startOf('day');
        const end = moment(e.endDate, 'DD/MM/YYYY').startOf('day');
        const diff = end.diff(start, 'days');
        for (let i = 0; i <= diff; i++) {
          arr.push(start.add(i === 0 ? 0 : 1, 'd').format('x'))
        }
      } else {
        arr.push(moment(e.startDate, 'DD/MM/YYYY').startOf('day').format('x'))
      }

      return arr
    }, []))
    setTimeout(() => {
      initDatePicker(instance, eventDate, lang);
    }, 10);
  },
  prepareData: () => {
    let startDate = moment();

    // create a new moment based on the original one
    let startFix = startDate.clone();

    // shift the moment by the difference in offsets
    startFix.add(startFix.utcOffset(), 'minutes');

    // use +startFix.format('x') and +endFix.format('x') to handle query
    // this will fix timezone issue

    let query = {
      $or: [
        {"startUtcTimestamp": {$gte: +startFix.format('x')}},
        {"endUtcTimestamp": {$gte: +startFix.format('x')}}
      ]
    };

    const templ = Template.instance();

    if (templ !== null) {
      if (templ.eventSearch && templ.eventSearch.get() != '') {
        let regexp = new RegExp(templ.eventSearch.get(), "i");
        query = {
          $and: [
            {
              $or: [
                {"startUtcTimestamp": {$gte: +startFix.format('x')}},
                {"endUtcTimestamp": {$gte: +startFix.format('x')}}
              ]
            },
            {
              $or: [
                {"title": regexp},
                {"location": regexp},
                {"description": regexp},
              ]
            }
          ]
        };
      }
      let condo = templ.condoIdFilter.get();
      if (condo !== 'all') {
        query.condoId = condo;
      }
    }
    let resources = ActuPosts.find(query).fetch();
    let events = [];
    templ.eventLength.set(0);
    templ.eventData.set(events);
    const lang = (FlowRouter.getParam("lang") || "fr");
    moment.locale(lang);

    const mapped = _.map(resources, (y, index) => {
      if (Meteor.userHasRight("actuality", "see", y.condoId)){

        const start = moment.utc(y.startUtcTimestamp).startOf('day');
        const end = moment.utc(y.endUtcTimestamp || y.startUtcTimestamp).startOf('day');
        const diff = end.diff(start, 'days');

        for (let i = 0; i <= diff; i++) {
          const day = start.clone();
          const dayTimestamp = +day.add(i, 'd').format('x');
          if (dayTimestamp >= +moment().format('x')) {
            events.push({
              _id: y._id + i,
              id: y._id,
              title: y.title,
              condoId: y.condoId,
              startDate: y.startDate,
              startHour: y.startHour,
              endDate: y.endDate,
              endHour: y.endHour,
              start: y.startUtcTimestamp,
              end: y.endUtcTimestamp ? y.endUtcTimestamp : y.startUtcTimestamp,
              day: dayTimestamp,
              editRight: Meteor.userHasRight("actuality", "write", y.condoId) && y.userId === Meteor.user()._id,
              deleteRight: Meteor.userHasRight("actuality", "delete", y.condoId)
            });
          }
        }
      }

      if (index + 1 === resources.length) {
        const enchantedEvents = insertDateHeader(_.chain(events).sortBy('creation').sortBy('start').sortBy('day').value());
        templ.eventLength.set(events.length);
        templ.eventData.set(enchantedEvents);
      }
    });


  },
  getFutureEvents: () => {
    return Template.instance().eventData.get()
  },
  emptyData: () => {
    return Template.instance().eventData.get().length === 0
  },
  noResult: () => {
    return Template.instance().eventData.get().length === 0 && Template.instance().eventSearch.get() !== ''
  },
  onSearch: () => {
    const template = Template.instance();
    return (val) => {
      let input = val;
      template.eventSearch.set(input);
    }
  },
  isDetail: () => {
    return !!Template.instance().eventId.get();
  },
  editId: () => {
    return Template.instance().eventId.get();
  },
  canEdit: () => {
    const event = ActuPosts.findOne(Template.instance().eventId.get());
    if (!!event) {
      return Meteor.userHasRight("actuality", "write", event.condoId) && event.userId === Meteor.user()._id;
    }

    return false;
  },
  canDelete: () => {
    const event = ActuPosts.findOne(Template.instance().eventId.get());
    if (!!event) {
      return Meteor.userHasRight("actuality", "delete", event.condoId);
    }

    return false;
  },
  sameDate: (startDate, endDate) => {
    // compare the first date with the second one
    /* @param = startDate: first date. String in french format. ex: '08/06/2017'
     endDate: second date. String in french format. ex: '08/06/2017' */
    if (startDate && endDate) {
      return new Date(startDate.split('/')[2], startDate.split('/')[1] - 1, startDate.split('/')[0]).valueOf() ==
        new Date(endDate.split('/')[2], endDate.split('/')[1] - 1, endDate.split('/')[0]).valueOf();
    }
  },
  actuPosts: () => {
    const instance = Template.instance();
    const day = instance.filterDay.get();
    const month = instance.filterMonth.get();

    // filter the ActuPosts collection for returning those one which match
    const data = _.sortBy(_.filter(_.filter( ActuPosts.find(instance.condoIdFilter.get() !== 'all' ? {condoId: instance.condoIdFilter.get()}:{}).fetch(), function(elem) {
      return elem.startDate;
    }), function(elem) {
      // filter actualities according to the current month in the calendar or the selected day
      elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
      elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
      if (elemSDate === 'Invalid Date')
        return true;
      if (elem.startDate && !elem.endDate)
        return month + 1 == elem.startDate.split('/')[1] && (day ? elem.startDate.split('/')[0] == day : true);
      return filterPosts(elemSDate, elemEDate, Template.instance());
    }), function(elem) {
      // sort informations by date
      return new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`).getDate();
    });

    return _.map(data, (a) => {
      return {
        editRight: Meteor.userHasRight('actuality', 'write', a.condoId, Meteor.userId()) && a.userId === Meteor.userId(),
        deleteRight: Meteor.userHasRight('actuality', 'delete', a.condoId, Meteor.userId()),
        ...a
      }
    });
  },
  selectedDate: () => {
    let lang = (FlowRouter.getParam("lang") || "fr")
    moment.locale(lang);
    // return the selected date
    return moment((Template.instance().filterDay.get() < 10 ? '0' : '') + Template.instance().filterDay.get() + '/' + (Template.instance().filterMonth.get() + 1 < 10 ? '0' : '') + (Template.instance().filterMonth.get() + 1) + '/' + Template.instance().filterYear.get(), 'DD/MM/YYYY').format('D MMMM Y');
  },
  previewFiles: () => {
    let fileIds = Template.instance().existingFiles.get()
    const reactiveFiles = Template.instance().files.get()

    const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onFileAdded: () => {
    const tpl = Template.instance();
    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  canResponseDetail: () => {
    const event = ActuPosts.findOne(Template.instance().eventId.get())

    return !!event && !!event.allowAction
  },
  getButtonClass: (key) => {
    const event = ActuPosts.findOne(Template.instance().eventId.get())
    return (!!event && !!event.participantResponse && !!event.participantResponse[key] && !!event.participantResponse[key].find((p) => { return p === Meteor.userId()})) ? 'red' : 'danger'
  },
  onFileRemoved: () => {
    const tpl = Template.instance()
    return (fileId) => {
      tpl.existingFiles.set(_.filter(tpl.existingFiles.get(), f => f.fileId ? f.fileId !== fileId : f !== fileId))
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  MbFilePreview: () => MbFilePreview,
  getDescriptionValue: () => {
    return Template.instance().description.get();
  }
});
