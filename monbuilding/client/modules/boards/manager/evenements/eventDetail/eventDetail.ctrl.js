/**
 * Created by kuyarawa on 06/06/18.
 */

import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import { ModuleClassifiedsViewAd, ModuleClassifiedsList, ForumLang, CommonTranslation } from "/common/lang/lang.js"

Template.eventDetail.onCreated(function() {
  this.fm = new FileManager(UserFiles, {
    fromEventComment: true,
    eventId: this.data.eventId
  });

  this.condoId = new ReactiveVar(null)

  this.isEmpty = new ReactiveVar(true)
  this.isEmptyEdit = new ReactiveVar(true)

  this.files = new ReactiveVar([])
  this.message = new ReactiveVar('')

  this.editedCommentTmpFiles = new ReactiveVar([])

  this.editedComment = new ReactiveDict()
  this.editedComment.setDefault({
    id: null,
    message: null,
    files: []
  })

  this.autorun(() => {
    Meteor.subscribe('eventCommentsFeed', this.condoId.get(), this.data.eventId);
  });

  this.saveComment = (t) => {
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)
    const __files = t.files.get()

    t.fm.batchUpload(__files)
      .then(results => {
        Meteor.call("postEventComment",
          t.condoId.get(),
          t.data.eventId,
          t.message.get(),
          _.map(results, f => f._id), function (error, result) {
          $('.mb-comment-save').button('reset')
          setTimeout(function() {
            $(".mb-comment-save").prop("disabled", true);
          }, 0);
          if (error) {
            sAlert.error(error)
          } else {
            t.files.set([])
            t.message.set('')
            t.isEmpty.set(true)
          }
        })
      })
      .catch(err => {
        $(".mb-comment-save").button('reset');
        sAlert.error(tr_common.commonTranslation[err.reason]);
      })
  }

  this.updateComment = (t, cb) => {
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)
    const __files = t.editedCommentTmpFiles.get()

    t.fm.batchUpload(__files)
      .then(files => {
        Meteor.call('editEventComment',
          t.condoId.get(),
          t.editedComment.get('id'),
          t.editedComment.get('message'),
          t.editedComment.get('files').concat(_.map(files, (f) => f._id)),
          (e) => cb(e)
        )
      })
      .catch(err => {
        $('#commit-update').button('reset')
        sAlert.error(tr_common.commonTranslation[err.reason]);
      })
  }
})

Template.eventDetail.events({
  'click .mb-comment-save': (e, t) => {
    if (!t.isEmpty.get()) {
      $('.mb-comment-save').button('loading')
      e.preventDefault()
      t.saveComment(t)
    }
  },
  'click .delete-comment': (e, t) => {
    const feedId = e.currentTarget.getAttribute('data-comment-id')
    const lang = FlowRouter.getParam("lang") || "fr";
    const tl_deletePost = new ForumLang(lang);
    const tl_common = new CommonTranslation(lang)
    const tl_classsified = new ModuleClassifiedsViewAd(lang)

    bootbox.confirm({
      title: tl_common.commonTranslation["confirmation"],
      message: tl_classsified.moduleClassifiedsViewAd["delete_answer"],
      buttons: {
        confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
        cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
      },
      callback: function(res) {
        if (res) {
          Meteor.call('deleteEventComment', t.condoId.get(), feedId, function (error, result) {
            if (!error) {
              sAlert.success(tl_classsified.moduleClassifiedsViewAd["success_delete_answer"])
            }
          })
        }
      }
    });
  },
  'click .add-to-calendar': (e, t) => {
    const element = $(e.currentTarget);
    const type = element.data('type');
    const data = ActuPosts.findOne(t.data.eventId);
    if (data) {
      const condo = Condos.findOne(data.condoId);

      let start = moment(data.startUtcTimestamp);
      let end = moment(!!data.endUtcTimestamp ? data.endUtcTimestamp : data.startUtcTimestamp + 1);

      // create a new moment based on the original one
      let startFix = start.clone();
      let endFix = end.clone();

      // change the time zone of the new moment
      startFix.tz('Europe/Paris'); // or whatever time zone you desire
      endFix.tz('Europe/Paris'); // or whatever time zone you desire

      // shift the moment by the difference in offsets
      startFix.add(start.utcOffset() - startFix.utcOffset(), 'minutes');
      endFix.add(end.utcOffset() - endFix.utcOffset(), 'minutes');

      const eventName = data.title;
      const common = {
        location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code,
        start: startFix.toDate(),
        end: endFix.toDate()
      };

      if (type === 'gcal') {
        const generateUrl = require('generate-google-calendar-url');
        const googleLink = generateUrl({
          title: eventName,
          details: '',
          ...common
        });
        window.open(googleLink, '_blank');
      } else {
        const cal = ics();
        cal.addEvent(eventName, '', common.location, common.start, common.end);
        cal.download();
      }
    }
  },
  'click .edit-comment': (e, t) => {
    const commentId = e.currentTarget.getAttribute('data-comment-id')
    const comment = EventCommentsFeed.findOne(commentId)

    if (comment) {
      t.editedComment.set('id', comment._id)
      t.editedComment.set('message', comment.message)
      t.editedComment.set('files', comment.files)
    }
  },
  'click #cancel-update': (e, t) => {
    t.editedComment.set('id', null)
    t.editedComment.set('message', '')
    t.editedComment.set('files', [])
    t.editedCommentTmpFiles.set([])
  },
  'click #commit-update': (e, t) => {
    if (t.isEmptyEdit.get() !== true) {
      $('#commit-update').button('loading')
      t.updateComment(t, (e) => {
        $('#commit-update').button('reset')
        if (!e) {
          t.editedComment.set('id', null)
          t.editedComment.set('message', '')
          t.editedComment.set('files', [])
          t.editedCommentTmpFiles.set([]);

          t.isEmptyEdit.set(true);
        } else {
          console.log(e)
        }
      })
    }
  },
  'input .mb-comment-textarea > div > textarea': (e, t) => {
    let text = e.currentTarget.value.trim()
    t.isEmpty.set(!(!!text && text.length > 0))
  },
  'input .mb-comment-text-edit > div > div > textarea': (e, t) => {
    let text = e.currentTarget.value.trim()
    t.isEmptyEdit.set(!(!!text && text.length > 0))
  }
})

Template.eventDetail.helpers({
  getEventDetail: () => {
    let actu = ActuPosts.findOne(Template.instance().data.eventId)
    if (actu) {
      Template.instance().condoId.set(actu.condoId)
      return actu
    }
  },
  sameDate: (startDate, endDate) => {
    // compare the first date with the second one
    /* @param = startDate: first date. String in french format. ex: '08/06/2017'
     endDate: second date. String in french format. ex: '08/06/2017' */
    if (startDate && endDate) {
      return new Date(startDate.split('/')[2], startDate.split('/')[1] - 1, startDate.split('/')[0]).valueOf() ===
        new Date(endDate.split('/')[2], endDate.split('/')[1] - 1, endDate.split('/')[0]).valueOf();
    }
  },
  MbFilePreview: () => MbFilePreview,
  getFileFromId: (fileIds) => {
    return UserFiles.find({ _id: { $in: fileIds.filter(file => !file.fileId) }})
  },
  getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  getCommentsCount: (postId) => {
    let ret = EventCommentsFeedCounter.findOne({ _id: postId })
    return ret ? ret.counter : 0
  },
  getViewCount: (post) => {
    return post.viewsCount || 0
  },
  haveResponse: (post) => {
    return !!post.participantResponse && (
      (!!post.participantResponse.going && !!post.participantResponse.going.length > 0) ||
      (!!post.participantResponse.maybe && !!post.participantResponse.maybe.length > 0) ||
      (!!post.participantResponse.not && !!post.participantResponse.not.length > 0)
    )
  },
  MbTextArea: () => MbTextArea,
  getMessage: () => Template.instance().message.get(),
  bindKeyDown: () => {
    const t = Template.instance()

    return (e) => {
      if (e.key === 'Enter' && !e.shiftKey && t.isEmpty.get() !== true) {
        e.preventDefault()
        $('.mb-comment-save').button('loading')
        t.saveComment(t)
      }
    }
  },
  updateMessage: () => {
    const t = Template.instance()

    return (value) => {
      t.message.set(value)
    }
  },
  getFiles: () => Template.instance().files.get(),
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      const files = _.filter(tpl.files.get(), file => {
        return file.fileId ? file.fileId !== fileId : file.id !== fileId
      })

      tpl.files.set(files)
    }
  },
  onFileAdded: () => {
    const tpl = Template.instance()
    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  disableSubmit: () => {
    return Template.instance().isEmpty.get()
  },
  getMessages: () => {
    return EventCommentsFeed.find({}, {sort: {date: -1}}).fetch()
  },
  isManager: () => Meteor.isManager,
  getPostedState: (feed) => {
    const tl_classsified = new ModuleClassifiedsViewAd(FlowRouter.getParam("lang") || "fr")
    return feed.isEdited ? tl_classsified.moduleClassifiedsViewAd["edited"] : tl_classsified.moduleClassifiedsViewAd["posted"]
  },
  parseCommentCreated: (timestamp) => {
    return moment(timestamp).locale(FlowRouter.getParam('lang') || 'fr').format('lll')
  },
  isCurrentlyEdit: (commentId) => {
    return Template.instance().editedComment.get('id') === commentId
  },
  getEditedCommentMessage: () => {
    return Template.instance().editedComment.get('message') || ''
  },
  onEditedCommentChange: () => {
    const t = Template.instance()

    return value => t.editedComment.set('message', value)
  },
  onEditCommentKeyDown: () => {
    const t = Template.instance()

    return (e) => {
      if (e.key === 'Enter' && !e.shiftKey && t.isEmptyEdit.get() !== true) {
        e.preventDefault()
        $('#commit-update').button('loading')

        t.updateComment(t, (e) => {
          $('#commit-update').button('reset')
          if (!e) {
            t.editedComment.set('id', null)
            t.editedComment.set('message', '')
            t.editedComment.set('files', [])
            t.editedCommentTmpFiles.set([]);

            t.isEmptyEdit.set(true);
          } else {
            console.log(e)
          }
        })
      }
    }
  },
  getPreviewFiles: () => {
    let fileIds = Template.instance().editedComment.get('files')
    const reactiveFiles = Template.instance().editedCommentTmpFiles.get()

    const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onEditCommentFileRemoved: () => {
    const t = Template.instance()
    return (fileId) => {
      t.isEmptyEdit.set(false);

      // Remove file from existing files set
      t.editedComment.set('files', _.filter(t.editedComment.get('files'), f => f.fileId ? f.fileId !== fileId : f !== fileId))

      // Remove file from pre-uploaded files
      t.editedCommentTmpFiles.set(_.filter(t.editedCommentTmpFiles.get('files'), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  onEditCommentFileAdded: () => {
    const t = Template.instance()

    return files => {
      t.isEmptyEdit.set(false);

      t.editedCommentTmpFiles.set([
        ...t.editedCommentTmpFiles.get(),
        ...files
      ])
    }
  },
  disableSubmitEdit: () => {
    //return true
    return Template.instance().isEmptyEdit.get()
  },
  getFilesCursor: (fileIds) => {
    return UserFiles.find({_id: {$in: fileIds}})
  },
  newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  canModify: (comment) => {
    return comment.userId === Meteor.userId()
  },
  canResponseDetail: () => {
    let event = ActuPosts.findOne(Template.instance().data.eventId)

    return !!event && !!event.allowAction
  },
  getButtonClass: (key) => {
    let event = ActuPosts.findOne(Template.instance().data.eventId)
    return (!!event && !!event.participantResponse && !!event.participantResponse[key] && !!event.participantResponse[key].find((p) => { return p === Meteor.userId()})) ? 'red sm' : 'danger sm'
  },
  eventResponse: (response) => {
    const instance = Template.instance()
    return () => () => {
      Meteor.call('eventResponse', instance.data.eventId, response, (err, res) => {
        if (!!err && err.error === 602) {
          const translation = new CommonTranslation(FlowRouter.getParam("lang") || "fr")

          bootbox.alert({
            size: "large",
            title: translation.commonTranslation.laundryRoom_availability_alert,
            message: translation.commonTranslation.no_more_even_space,
            backdrop: true
          });
        }
      })
    }
  },
})
