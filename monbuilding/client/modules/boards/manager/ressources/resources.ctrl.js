import { Pagination } from "/client/components/pagination/pagination.js"
import { ModuleReservationIndex, ForumLang, CommonTranslation } from '/common/lang/lang.js';

Template.ressources.onCreated(function() {
	this.condoId = new ReactiveVar(FlowRouter.getParam('condo_id'));
    this.resourceSearch = new ReactiveVar("");
	this.pagination = new Pagination(Resources, 10);
    this.canSeeResources = new ReactiveVar(false);
    this.canAddResources = new ReactiveVar(false);
	this.pagination.setQuery({condoId: this.condoId.get()});
	self = this;
	Meteor.subscribe("resources");
	Meteor.call('setNewAnalytics', {type: "module", module: "resources", accessType: "web", condoId: ''});

    this.autorun(() => {
        this.canSeeResources.set(this.condoId.get() === 'all' ? Meteor.listCondoUserHasRight("reservation", "seeResources").length > 0 : Meteor.userHasRight('reservation', 'seeResources', this.condoId.get()))
        this.canAddResources.set(this.condoId.get() === 'all' ? Meteor.listCondoUserHasRight("reservation", "addResources").length > 0 : Meteor.userHasRight('reservation', 'addResources', this.condoId.get()))
    });
});

Template.ressources.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "resources", accessType: "web", condoId: ''});
})

Template.ressources.events({
    'click .action-edit': function(e, t) {
        const lang = (FlowRouter.getParam("lang") || "fr");
        let id = e.currentTarget.getAttribute("data-id");
//        let currentContext = FlowRouter.current();
//		currentContext.resourceId = id;
//		console.log("context: ", currentContext);
        FlowRouter.go("app.gestionnaire.ressources.form", { lang, resourceId: id, condo_id:  t.condoId.get()});
    },
    'click .action-delete': function(e, t) {
      let id = e.currentTarget.getAttribute("data-id");
      const lang = (FlowRouter.getParam("lang") || "fr");
      const tl_deletePost = new ForumLang(lang);
      const translation = new CommonTranslation(lang);

      bootbox.confirm({
        title  : translation.commonTranslation["confirm_resource_delete"],
        message: translation.commonTranslation["confirm_resource_delete_explain"],
        buttons: {
          confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
          cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
        },
        callback: function (result) {
          if (result) {
            const resource = Resources.findOne(id)
            if (!!resource) {
              Meteor.call('removeResource', id, resource.condoId, (err) => {
                if (!err) {
                  sAlert.success(translation.commonTranslation["resource_deleted"]);
                }
              });
            }
          }
        }
      });
    },
	'click #backbutton': function (e, t) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    FlowRouter.go("app.gestionnaire.planningid", { lang, condo_id:  t.condoId.get()});
  },
	'click #add-resource': function (e, t) {
//        let currentContext = FlowRouter.current();
//		currentContext.resourceId = 'new';
//		console.log("context: ", currentContext);
        const lang = FlowRouter.getParam("lang") || "fr";
        FlowRouter.go("app.gestionnaire.ressources.form", { lang, condo_id:  t.condoId.get(), resourceId: 'new' });
    },
	'input #searchRes': function(e, t) {
		e.preventDefault();
		t.pagination.setPage(1);
		let regexp = new RegExp(e.currentTarget.value, "i");
		t.resourceSearch.set(e.currentTarget.value);
		let condoId = t.condoId.get();
		if (condoId === "all") {
			let condos = Condos.find({$and: [{$or : [{name: regexp}, {"info.id": regexp}, {"info.address": regexp}, {"info.code": regexp}, {"info.city": regexp}], "settings.options.reservations": true}]}).fetch();
			if (condos.length === 0) {
				condos = Condos.find({"settings.options.reservations": true}).fetch();
				let condosIds = _.map(condos, (c) => {return c._id;});
				t.pagination.setQuery({condoId: {$in: condosIds}, $or: [{type: regexp}, {name: regexp}]});
				return;
			}
			let condosIds = _.map(condos, (c) => {return c._id;});
			t.pagination.setQuery({condoId: {$in: condosIds}});
			return ;
		}
		else
			t.pagination.setQuery({condoId: condoId, $or: [{type: regexp}, {name: regexp}]});
	},
});

Template.ressources.helpers({
    getResourceTypeByKey: (key) => {
    	const type = ResourceType.findOne({key: key})
		return type ? getTranslatedValue(type) : '-';
	},
	getFloorString: (floor) => {
    	return translateEtage(floor);
	},
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const t = Template.instance()
    if (condoId && condoId !== t.condoId.get()) {
      const lang = FlowRouter.getParam('lang') || 'fr';
      FlowRouter.go('app.gestionnaire.ressources', { lang, condo_id: condoId });
      t.condoId.set(condoId)
    }
    if (condoId) {
      t.pagination.setPage(1);

      let searchVal = $('#searchRes').val();
      let regexp = new RegExp(searchVal, "i");
      if (condoId === "all") {
        let condos = Condos.find({$and: [{$or : [{name: regexp}, {"info.id": regexp}, {"info.address": regexp}, {"info.code": regexp}, {"info.city": regexp}], "settings.options.reservations": true}]}).fetch();
        if (condos.length === 0) {
          condos = Condos.find({"settings.options.reservations": true}).fetch();
          let condosIds = _.map(condos, (c) => {return c._id;});
          t.pagination.setQuery({condoId: {$in: condosIds}, $or: [{type: regexp}, {name: regexp}]});
          return;
        }
        let condosIds = _.map(condos, (c) => {return c._id;});
        t.pagination.setQuery({condoId: {$in: condosIds}});
        return ;
      }
      else
        t.pagination.setQuery({condoId: t.condoId.get(), $or: [{type: regexp}, {name: regexp}]});
    }
    return true
  },
	condoName: (id) => {
		let condoId = (id ? id : Template.instance().condoId.get());
		if (condoId === "all")
			return "Tous les immeubles";
		let condo = Condos.findOne({_id: condoId});
		if (condo) {
			if (condo.info.id != -1)
				return condo.info.id + ' - ' + condo.getName();
			else
				return condo.getName();
		}
	},
	selectedCondo: () => {
		return Template.instance().condoId.get();
	},
	resources: () => {
		return attachResourceType(Template.instance().pagination.getResults().fetch());
	},
  resNum: (res) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
		return res.number ? (
			res.type === 'coworking' ? translate.moduleReservationIndex['place'] : translate.moduleReservationIndex['availability']) + ': ' + res.number
			: false;
	},
	resPersons: (res) => {
		return res.persons ? res.persons : false;
	},
	resSeats: (res) => {
		return res.seats ? res.seats : false;
	},
	isSearchActive: () => {
		return Template.instance().resourceSearch.get() !== ''
	},
	CDD_cursor: () => {
		return Meteor.listCondoUserHasRight("reservation", "seeResources");
	},
	getPagination: () => {
		return Template.instance().pagination;
	},
  getEmptyProps: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    const instance = Template.instance();
    const canAddResources = (instance.condoId.get() === 'all' ? Meteor.listCondoUserHasRight("reservation", "addResources").length > 0 : Meteor.userHasRight('reservation', 'addResources', instance.condoId.get()));
    let extraProps = {};
    if (canAddResources) {
      extraProps = {
        buttonLabel: translate.moduleReservationIndex["empty_button_label"],
        onButtonClick: () => {
          Session.set('selectedCondo', instance.condoId.get());
          FlowRouter.go("app.gestionnaire.ressources.form", { lang, resourceId: 'new', condo_id:  instance.condoId.get() });
        }
      }
    }
    return {
      title: translate.moduleReservationIndex["empty_title_no_ressource"],
      subTitle: translate.moduleReservationIndex["empty_subtitle"],
      ...extraProps
    }
  },
    canSeeResources: () => {
        return !!Template.instance().canSeeResources.get()
    },
    canAddResources: (condoId) => {
        return condoId === 'all' ? Meteor.listCondoUserHasRight("reservation", "addResources").length > 0 : !!Meteor.userHasRight('reservation', 'addResources', condoId);
    }
});
