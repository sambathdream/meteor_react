import { ModuleReservationIndex, ResourceForm, CommonTranslation } from '/common/lang/lang.js';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import { stat } from 'fs';
import { TermsOfServices } from '/common/collections/marketPlace'

let WEEKDAYS = []

const DEFAULT_TIME = {
    start: '08:00',
    end: '20:00'
}

const updateValue = (field) => {
    const instance = Template.instance();
    return (val) => {
        const current = instance.form.get(field);
        if (val !== current) {
            instance.form.set(field, val);
            validateForm(instance);
        }
    }
};

const initTimePicker = () => {
    $('.startTimeG, .endTimeG').datetimepicker({
        locale: 'fr',
        format: "HH:mm",
        widgetPositioning: {
            vertical: 'bottom',
            horizontal: 'auto'
        }
    });
};

const getServiceState = (service, instance) => {
    const availableServices = instance.form.get('availableServices');
    return !!_.find(availableServices, (s, i) => i === service.key)
};

const getChildServiceState = (service, child, custom, instance) => {
    const availableServices = instance.form.get('availableServices');
    const theService = _.find(availableServices, (s, i) => i === service.key);

    if (theService) {
        if (custom) {
            return theService.custom.indexOf(child) !== -1
        } else {
            return theService.data.indexOf(child.key) !== -1
        }
    }

    return false;
};

const getChildServiceStateAll = (service, child, custom, instance) => {
    const availableServices = instance.form.get('availableServices');
    const theService = _.find(availableServices, (s, i) => i === service.key);

    if (theService) {
        if (custom) {
            return theService.custom.length > 0
        } else {
            return theService.data.length > 0
        }
    }

    return false;
};

const validateForm = (instance) => {
    const form = instance.form;
    const cateringIsSelected = instance.cateringIsSelected.get()
    const cateringChildIsSelected = instance.cateringChildIsSelected.get()
    const cateringChildCustomIsSelected = instance.cateringChildCustomIsSelected.get()
    const equipmentIsSelected = instance.equipmentIsSelected.get()
    const equipmentChildIsSelected = instance.equipmentChildIsSelected.get()
    const equipmentChildCustomIsSelected = instance.equipmentChildCustomIsSelected.get()
    const paid = form.get('paid');
    const price = form.get('price');
    const type = form.get('type');
    const needValidation = form.get('needValidation') || {};
    let requiredFields = ['name', 'type', 'etage', 'condoId'];

    if (type === 'coworking') {
        requiredFields = [...requiredFields, 'number']
    } else if (type === 'edl') {
        requiredFields = ['type', 'number', 'edlDuration', 'name']
    } else if (type === 'restaurant') {
        requiredFields = [...requiredFields, 'number']
    } else if (type === 'equipment') {
        requiredFields = [...requiredFields, 'availability']
    }

    if (needValidation.status) {
        requiredFields.push('needValidation.managerIds')
    }

    const invalidFields = _.filter(requiredFields, (req) => {
        if (req === 'needValidation.managerIds') {
            return !needValidation || !needValidation.managerIds || needValidation.managerIds.length === 0
        }
        const val = form.get(req);
        return _.isNull(val) || val === '' || typeof val === 'undefined' || (req === 'edlDuration' && !isInt(val))
    });

    let valid = false
    if (invalidFields.length === 0) {
        valid = true;
    }

    if (cateringIsSelected) {
      valid = cateringChildIsSelected || cateringChildCustomIsSelected
    }

    if (equipmentIsSelected) {
      valid = equipmentChildIsSelected || equipmentChildCustomIsSelected
    }

    if (!!paid && (!price || price === 0 || price === '0')) {
      valid = false
    }

    if (instance.isValid.get() !== valid) {
        instance.isValid.set(valid)
    }
};

function setWeekDay() {
  const lang = (FlowRouter.getParam("lang") || "fr");
  const translate = new ModuleReservationIndex(lang);

  WEEKDAYS = [
    {
      dow: 1,
      day: translate.moduleReservationIndex['monday']
    }, {
      dow: 2,
      day: translate.moduleReservationIndex['tuesday']
    }, {
      dow: 3,
      day: translate.moduleReservationIndex['wednesday']
    }, {
      dow: 4,
      day: translate.moduleReservationIndex['thursday']
    }, {
      dow: 5,
      day: translate.moduleReservationIndex['friday']
    }, {
      dow: 6,
      day: translate.moduleReservationIndex['saturday']
    }, {
      dow: 0,
      day: translate.moduleReservationIndex['sunday']
    }
  ];
}

Template.resources_form.onCreated(function() {
    this.id = FlowRouter.getParam("resourceId") === 'new' ? '' : FlowRouter.getParam("resourceId");
    this.isValid = new ReactiveVar(false);
    this.isBusy = new ReactiveVar(false);
    this.customText = new ReactiveDict;
    this.form = new ReactiveDict;
    this.canSeeResources = new ReactiveVar(false);
    this.canAddResources = new ReactiveVar(false);
    this.cateringIsSelected = new ReactiveVar(false)
    this.cateringChildIsSelected = new ReactiveVar(false)
    this.cateringChildCustomIsSelected = new ReactiveVar(false)
    this.equipmentIsSelected = new ReactiveVar(false)
    this.equipmentChildIsSelected = new ReactiveVar(false)
    this.equipmentChildCustomIsSelected = new ReactiveVar(false)
    this.edited_cgv = new ReactiveVar(null)
    this.isMore = new ReactiveVar(false)
    setWeekDay()
    let setDefault = true;
    if (this.id !== '') {
        const resource = Resources.findOne(this.id);
        if (resource) {
            setDefault = false;
            Session.set('selectedCondo', resource.condoId);
            this.form.setDefault({
                condoId: resource.condoId,
                type: resource.type,
                name: resource.name,
                etage: resource.etage,
                note: resource.note || '',
                allDay: false,
                seats: resource.seats || undefined ,
                persons: resource.persons || undefined,
                number: resource.number || undefined,
                availability: resource.number || undefined,
                availableServices: resource.availableServices,
                needValidation: {
                  ...resource.needValidation,
                  ...resource.type === 'edl' ? {status: true}: {}
                },
                horaires: resource.horaires,
                edlDuration: resource.edlDuration || 45,
                paid: resource.paid || false,
                price: resource.price || 1,
                vat: resource.vat || '20',
                paidDuration: resource.paidDuration || 60, // minutes
                allowMessage: resource.allowMessage || false
            });
        }
    }

    if (setDefault) {
        this.form.setDefault({
            condoId: !!Session.get('selectedCondo') && Session.get('selectedCondo') !== 'all' ? Session.get('selectedCondo'): null,
            type: null,
            name: null,
            etage: null,
            note: '',
            allDay: false,
            seats: undefined,
            persons: undefined,
            number: undefined,
            availability: undefined,
            availableServices: {},
            edlDuration: 45,
            needValidation: {
                status: false,
                managerIds: []
            },
            horaires: _.map(WEEKDAYS, (wd) => ({
                dow: [wd.dow],
                open: wd.dow > 0 && wd.dow < 6,
                openHours: [DEFAULT_TIME],
                ...DEFAULT_TIME
            })),
            paid: false,
            price: 1,
            vat: '20',
            paidDuration: '60',
            allowMessage: false
        });
    }

    this.getMinMaxHour = (ot) => {
      const min = _.min(ot, oh => {
        return moment(oh.start, 'HH:mm').format('x')
      })

      const max = _.max(ot, oh => {
        return moment(oh.end, 'HH:mm').format('x')
      })

      return {min, max}
    }

    Meteor.subscribe('get-terms-of-services_manager')
    this.autorun(() => {
        this.canSeeResources.set(!this.form.get('condoId') ? Meteor.listCondoUserHasRight("reservation", "seeResources").length > 0 : Meteor.userHasRight('reservation', 'seeResources', this.form.get('condoId')))
        this.canAddResources.set(!this.form.get('condoId') ? Meteor.listCondoUserHasRight("reservation", "addResources").length > 0 : Meteor.userHasRight('reservation', 'addResources', this.form.get('condoId')))
    });
});

Template.resources_form.events({
    'click #backbutton' (e, t) {
        const current = FlowRouter.current();
        FlowRouter.go("app.gestionnaire.ressources", current.params);
    },
    'click #save-resource' (e, t) {
        t.isBusy.set(true);
        const current = FlowRouter.current();
        let lang = FlowRouter.getParam("lang") || "fr"
        const translation = new CommonTranslation(lang)
        const resource = {
            type: t.form.get('type'),
            name: t.form.get('name'),
            etage: parseInt(t.form.get('etage')),
            needValidation: t.form.get('needValidation'),
            availableServices: t.form.get('availableServices'),
            seats: parseInt(t.form.get('seats')),
            persons: parseInt(t.form.get('persons')),
            number: t.form.get('type') !== 'equipment' ? parseInt(t.form.get('number')) : parseInt(t.form.get('availability')),
            availability: parseInt(t.form.get('availability')),
            horaires: t.form.get('horaires'),
            note: t.form.get('note'),
            condoId: t.form.get('condoId'),
            allowMessage: t.form.get('allowMessage'),
            paid: t.form.get('paid'),
            price: t.form.get('price'),
            vat: t.form.get('vat'),
            paidDuration: t.form.get('paidDuration')
        };

        let insert = resource;
        if (resource.type === 'edl') {
            const data = _.pick(resource, 'type', 'horaires', 'condoId', 'needValidation', 'note', 'number', 'name');
            insert = {
                ...data,
                edlDuration: t.form.get('edlDuration')
            }
        }

        if (t.edited_cgv.get() !== null) {
          Meteor.call('update-terms-of-uses', resource.condoId, t.edited_cgv.get())
        }

        if (t.id !== '') {
            Meteor.call('editResource', t.id, insert, (err, res) => {
                if (err) {
                    t.isBusy.set(false);
                    sAlert.error(err.reason);
                } else {
                    t.isBusy.set(false);
                    FlowRouter.go("app.gestionnaire.ressources", current.params);
                    sAlert.success(translation.commonTranslation["resource_edited"]);
                }
            });

            t.isBusy.set(false);
        } else {
            Meteor.call('addResource', insert, (err, res) => {
                if (err) {
                    t.isBusy.set(false);
                    sAlert.error(err.reason);
                } else {
                    let lang
                    t.isBusy.set(false);
                    FlowRouter.go("app.gestionnaire.ressources", current.params);
                    sAlert.success(translation.commonTranslation["resource_added"]);
                }
            });
        }
    },
    'click .add-time-slot' (e, t) {
        const isAllDay = t.form.get('allDay');

        if (isAllDay) {
            t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;

                if (newData.openHours && newData.open) {
                    newData.openHours.push(DEFAULT_TIME)
                }
                const {min, max} = t.getMinMaxHour(newData.openHours);
                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
            }));
        } else {
            const dow = parseInt(e.currentTarget.getAttribute("data-dow"));

            t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;
                if (h.dow[0] === dow) {
                    if (newData.openHours) {
                        newData.openHours.push(DEFAULT_TIME)
                    }
                }
                const {min, max} = t.getMinMaxHour(newData.openHours);
                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
            }));
        }

        setTimeout(() => {
            initTimePicker()
        }, 500);
    },
    'click .remove-time-slot' (e, t) {
        const isAllDay = t.form.get('allDay');
        const index = parseInt(e.currentTarget.getAttribute("data-index"));

        if (isAllDay) {
            t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;

                if (newData.openHours && newData.open) {
                    newData.openHours = _.reject(newData.openHours, (h, i) => {return i === index})
                }
                const {min, max} = t.getMinMaxHour(newData.openHours);
                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
            }));
        } else {
            const dow = parseInt(e.currentTarget.getAttribute("data-dow"));

            t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;
                if (h.dow[0] === dow) {
                    if (newData.openHours) {
                        newData.openHours = _.reject(newData.openHours, (h, i) => {return i === index})
                    }
                }
                const {min, max} = t.getMinMaxHour(newData.openHours);
                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
            }));
        }

        setTimeout(() => {
            initTimePicker()
        }, 500);
    },
    'click .new-custom-button' (e, t) {
        const key = e.currentTarget.getAttribute("data-key");
        const availableServices = t.form.get('availableServices');
        const item = _.find(availableServices, (s, i) => i === key);
        const newCustom = t.customText.get(key);
        const childExist = item.custom.indexOf(newCustom) !== -1;
        if (!!newCustom && !childExist) {
            if (item) {
                t.form.set('availableServices', {
                    ...availableServices,
                    [key]: {
                        ...item,
                        custom: item.custom.concat(newCustom)
                    }
                });
                t.customText.set(key, '')

                if (key === 'catering') {
                  t.cateringChildCustomIsSelected.set(true)
                } else {
                  t.equipmentChildCustomIsSelected.set(true)
                }
                validateForm(t);
            }
        }
    },
    'blur .hp' (e, t) {
        const isAllDay = t.form.get('allDay');
        const index = parseInt(e.currentTarget.getAttribute("data-index"));
        const type = e.currentTarget.getAttribute("data-type");
        const dow = parseInt(e.currentTarget.getAttribute("data-dow"));
        if (isAllDay) {
            const day = t.form.get('horaires').find(h => h.dow[0] === dow);
            if (!!day && !!day.open) {
              t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;
                if (newData.openHours && newData.openHours[index] && newData.openHours[index][type] && newData.open) {
                  newData.openHours[index][type] = $(e.currentTarget).val();
                  newData.open24 = (newData.openHours[0].start === '00:00' && (newData.openHours[0].end === '00:00' || newData.openHours[0].end === '24:00'))
                }
                const {min, max} = t.getMinMaxHour(newData.openHours);

                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
              }));
            }
        } else {

            t.form.set('horaires', _.map(t.form.get('horaires'), (h) => {
                let newData = h;
                if (newData.openHours && newData.openHours[index] && newData.openHours[index][type] && h.dow[0] === dow) {
                    newData.openHours[index][type] = $(e.currentTarget).val();
                    newData.open24 = (newData.openHours[0].start === '00:00' && (newData.openHours[0].end === '00:00' || newData.openHours[0].end === '24:00'))
                }

                const {min, max} = t.getMinMaxHour(newData.openHours);
                return {
                  ...newData,
                  start: min.start,
                  end: max.end,
                };
            }));
        }

        setTimeout(() => {
            initTimePicker()
        }, 500);
    },
    'input .price-input': (e, t) => {
      const val = $(e.currentTarget).val()
      t.form.set('price', val)
      validateForm(t)
    },
});

Template.resources_form.helpers({
    getId: () => {
        return Template.instance().id
    },
    getTypeOptions: () => {
        const resourceType = ResourceType.find({}).fetch();
        return _.map(resourceType, (r) => {
            return {
                id: r.key,
                text: getTranslatedValue(r)
            }
        });
    },
    getNumberOptions: (length, isFloor, startFrom) => {
        const options = _.range(startFrom , length + 1);
        return _.map(options, (o) => {
            return {
                id: o,
                text: !isFloor ? o : translateEtage(o)
            }
        })
    },
    getFormData: (key) => {
        if (key === 'etage') {
          return Template.instance().form.get(key) && isInt(Template.instance().form.get(key)) ? Template.instance().form.get(key) : '0';
        } else {
          return Template.instance().form.get(key) ? Template.instance().form.get(key) : '';
        }
    },
    getWeekdays: () => {
        setWeekDay()
        return _.map(WEEKDAYS, (d) => {
            return {
                key: d.dow,
                value: d.day
            }
        })
    },
    needValidationStatus: () => {
        const needValidation = Template.instance().form.get('needValidation');
        return needValidation && needValidation.status;
    },
    managerMembers: () => {
        let options = [];
        let selectedCondoId = Template.instance().form.get('condoId');
        let selectedType = Template.instance().form.get('type');
        let gestUsers = Enterprises.findOne({"users.userId": Meteor.userId()});
        if (gestUsers) {
            gestUsers = gestUsers.users;
            let idGest = _.map(gestUsers, function(elem) {
                let isInCharge = false;
                _.each(elem.condosInCharge, function(condosInCharge) {
                    if (condosInCharge.condoId === selectedCondoId)
                        isInCharge = true;
                })
                let userHasRight = false
                if (selectedType === 'edl') {
                  userHasRight = Meteor.userHasRight("setContact", "validAwaitingOccupant", selectedCondoId, elem.userId);
                } else {
                  userHasRight = Meteor.userHasRight("reservation", "see", selectedCondoId, elem.userId);
                }
                return (isInCharge && userHasRight && elem.isAdmin !== true) ? (elem.userId) : "";
            });
            const managerCollection = Meteor.users.find({_id: {$in : idGest}}).fetch();

            options = _.map(managerCollection, (m) => {
                return {
                    id: m._id,
                    text: `${m.profile.firstname} ${m.profile.lastname}`
                }
            })
        }

        return options;
    },
    isDurationError: () => {
      const duration = Template.instance().form.get('edlDuration')
      return !duration || !isInt(duration)
    },
    mainCondoIdCb: () => {
        const instance = Template.instance();
        return (condoId) => {
            if (condoId !== instance.form.get('condoId')) {
                instance.form.set('condoId', condoId);
                instance.form.set('needValidation', {
                    status: false,
                    managerIds: []
                });
                validateForm(instance);
            }
        }
    },
    getOpenHours: (day) => {
        const horaires = Template.instance().form.get('horaires');
        const horaire = _.find(horaires, (h) => {
            return h.dow[0] === day.key
        });
        if (!!horaire.openHours && horaire.openHours.length > 0) {
            return horaire.openHours;
        } else {
            return [{start: horaire.start, end: horaire.end}];
        }
    },
    isMoreThanOneSlot: (day) => {
        const horaires = Template.instance().form.get('horaires');
        const horaire = _.find(horaires, (h) => {
            return h.dow[0] === day.key
        });
        return !!horaire.openHours && horaire.openHours.length > 1;
    },
    isLastTimeSlotAndOpen: (day, index) => {
        const horaires = Template.instance().form.get('horaires');
        const horaire = _.find(horaires, (h) => {
            return h.dow[0] === day.key
        });
        return !!horaire.openHours && horaire.openHours.length === index + 1 && horaire.open;
    },
    getSelectedManager: () => {
        const needValidation = Template.instance().form.get('needValidation');
        return needValidation.managerIds;
    },
    initTimePicker: () => {
        setTimeout(() => {
            initTimePicker()
        }, 500);
    },
    isDayClose: (day) => {
        const instance = Template.instance();
        const horaire = _.find(instance.form.get('horaires'), (h) => {
            return h.dow[0] === day.key
        });
        return !horaire.open;
    },
    selectedType: () => {
        const instance = Template.instance();
        return ResourceType.findOne({key: instance.form.get('type')});
    },
    isFormValid: () => {
        const instance = Template.instance();
        return instance.isValid.get() && !instance.isBusy.get();
    },
    getAvailableServices: () => {
        return ResourceServices.find({}).fetch();
    },
    getServiceState: (service) => {
        const instance = Template.instance();
        return getServiceState(service, instance);
    },
    getServiceChild: (service) => {
        const theService = ResourceServices.findOne({key: service.key});
        const availableServices = Template.instance().form.get('availableServices');
        const customChild = !!availableServices && !!availableServices[service.key] && !!availableServices[service.key].custom ? availableServices[service.key].custom : [];
        return theService && theService.children ? [...theService.children, ...customChild] : customChild;
    },
    // ----------------------------- form component attribute ----------------------------
    onBlurName: () => {
        return updateValue('name');
    },
    onBlurEdlDuration: () => {
        return updateValue('edlDuration');
    },
    onSelectSeat: () => {
        return updateValue('seats');
    },
    onSelectPerson: () => {
        return updateValue('persons');
    },
    onSelectVat: () => {
        return updateValue('vat');
    },
    onSelectDuration: () => {
        return updateValue('paidDuration');
    },
    onSelectEtage: () => {
        return updateValue('etage');
    },
    onSelectNumber: () => {
        return updateValue('number');
    },
    onSelectAvailability: () => {
        return updateValue('availability');
    },
    onInputNote: () => {
        return updateValue('note');
    },
    onSelectType: () => {
        const instance = Template.instance();
        return (type) => {
            if (type !== instance.form.get('type')) {
                const validation = instance.form.get('needValidation')
                instance.form.set({
                    type : type,
                    seats: undefined,
                    person: undefined,
                    number: undefined,
                    availability: undefined,
                    needValidation: {
                        ...(type !== 'edl'? {managerIds: []}: validation),
                        status: type === 'edl'
                    }
                });
                validateForm(instance);
            }
        }
    },
    onSelectManager: () => {
        const instance = Template.instance();
        const needValidation = instance.form.get('needValidation');
        return (val) => {
            if (!_.isEqual(val, needValidation.managerIds)) {
                instance.form.set('needValidation', {
                    status: needValidation.status,
                    managerIds: val
                });
                validateForm(instance);
            }
        }
    },
    getNewCustomText: (service) => {
        return Template.instance().customText.get(service.key)
    },
    onInputNewCustom: (service) => {
        const instance = Template.instance();
        return () => (val) => {
            instance.customText.set(service.key, val);
        }
    },
    childServiceArgs: (service, child) => {
      const lang = (FlowRouter.getParam("lang") || "fr");
        const custom = !child.key;
        const instance = Template.instance();
        const state = getChildServiceState(service, child, custom, instance);
        const availableServices = instance.form.get('availableServices');
        const item = _.find(availableServices, (s, i) => i === service.key);

        return {
            checked: state,
            label: custom ? child : child[`value-${lang}`] ? child[`value-${lang}`] : (child['value-fr'] ? child['value-fr'] : ''),
            onClick: () => {

                if (custom) {
                    const childExist = item.custom.indexOf(child) !== -1;

                    if (item) {
                        instance.form.set('availableServices', {
                            ...availableServices,
                            [service.key]: {
                                ...item,
                                custom: childExist
                                    ? _.filter(item.custom, d => d !== child)
                                    : item.custom.concat(child)
                            }
                        });
                    }
                } else {

                    const childExist = item.data.indexOf(child.key) !== -1

                    if (item) {
                        instance.form.set('availableServices', {
                            ...availableServices,
                            [service.key]: {
                                ...item,
                                data: childExist
                                    ? _.filter(item.data, d => d !== child.key)
                                    : item.data.concat(child.key)
                            }
                        });
                    }
                }
                if (service.key === 'catering') {
                  if (custom) {
                    instance.cateringChildCustomIsSelected.set(getChildServiceStateAll(service, child, custom, instance))
                  } else {
                    instance.cateringChildIsSelected.set(getChildServiceStateAll(service, child, custom, instance))
                  }
                } else {
                  if (custom) {
                    instance.equipmentChildCustomIsSelected.set(getChildServiceStateAll(service, child, custom, instance))
                  } else {
                    instance.equipmentChildIsSelected.set(getChildServiceStateAll(service, child, custom, instance))
                  }
                }
                validateForm(instance);
            }
        }
    },
    serviceArgs: (service) => {
      const lang = (FlowRouter.getParam("lang") || "fr");
        const instance = Template.instance();
        const state = getServiceState(service, instance);
        const availableServices = instance.form.get('availableServices');

        return {
            checked : state,
            label: service[`value-${lang}`] ? service[`value-${lang}`] : (service['value-fr'] ? service['value-fr'] : ''),
            onClick: () => {
                const newAvailableServices = !state ? {
                    ...availableServices,
                    [service.key]: {
                        data: [],
                        custom: []
                    }
                } : _.omit(availableServices, service.key);

                instance.form.set('availableServices', newAvailableServices);
                service.key === 'catering'
                  ? instance.cateringIsSelected.set(getServiceState(service, instance))
                  : instance.equipmentIsSelected.set(getServiceState(service, instance))
                validateForm(instance);
            }
        }
    },
    dayArgs: (day) => {
        const instance = Template.instance();
        const horaire = _.find(instance.form.get('horaires'), (h) => {
            return h.dow[0] === day.key
        });
        return {
            checked : horaire.open,
            label: day.value,
            onClick: () => {
                instance.form.set('horaires', _.map(instance.form.get('horaires'), (h) => {
                    let newData = h;
                    if (h.dow[0] === day.key) {
                        newData.open = !newData.open;
                        if (!newData.open) {
                          newData.open24 = false;
                        }
                    }
                    return newData;
                }));
            }
        }
    },
    day24Args: (day) => {
      const instance = Template.instance();
      const horaire = _.find(instance.form.get('horaires'), (h) => {
        return h.dow[0] === day.key
      });
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ResourceForm(lang);
      return {
        checked : horaire.open24,
        label: '24h/24h',
        onClick: () => {
          const horaire = _.find(instance.form.get('horaires'), (h) => {
            return h.dow[0] === day.key
          });
          if (!!horaire.open) {
            instance.form.set('horaires', _.map(instance.form.get('horaires'), (h) => {
              let newData = h;
              if (h.open && (h.dow[0] === day.key || instance.form.get('allDay'))) {
                newData.open24 = !horaire.open24;
                if (!!newData.open24) {
                  newData = {
                    ...newData,
                    ...{
                      start: '00:00',
                      end: '24:00',
                      openHours: [{
                        start: '00:00',
                        end: '24:00'
                      }]
                    }
                  }
                }
              }
              return newData;
            }));
          }
        }
      }
    },
    allDayArgs: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ResourceForm(lang);
        const instance = Template.instance();
        return {
            checked : instance.form.get('allDay'),
            label: translate.resourceForm['all_days'],
            onClick: () => {
                const status = instance.form.get('allDay');
                instance.form.set('allDay', !status);
                if (!status) {
                    instance.form.set('horaires', _.map(WEEKDAYS, (wd) => ({
                        dow: [wd.dow],
                        open: wd.dow > 0 && wd.dow < 6,
                        openHours: [DEFAULT_TIME],
                        ...DEFAULT_TIME
                    })));
                }
            }
        };
    },
    getEdlHelp: (type) => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ResourceForm(lang);

      return type === 'edl' ? translate.resourceForm['edl_help'] : false
    },
    needValidationArgs: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ResourceForm(lang);
        const instance = Template.instance();
        const needValidation = instance.form.get('needValidation');
        return {
            checked: !!needValidation && needValidation.status,
            label: translate.resourceForm['manager_validation'],
            onClick: () => {
                // make edl type cannot un check this option
                if (instance.form.get('type') !== 'edl') {
                    const status = !(!!needValidation && needValidation.status);
                    const ids = !!needValidation ? needValidation.managerIds : [];
                    instance.form.set('needValidation', {
                        status: status,
                        managerIds: ids
                    });
                    if (status) {
                      instance.form.set('paid', false)
                    }
                    validateForm(instance);
                }
            }
        }
    },
    toggleCheck: (field) => {
      const instance = Template.instance()
      return () => () => {
        const current = instance.form.get(field);
        instance.form.set(field, !current);
        validateForm(instance);
      }
    },
    getVAT: () => {
      return [
        { id: '2.1', text: '2.1%' },
        { id: '5.5', text: '5.5%' },
        { id: '10', text: '10%' },
        { id: '20', text: '20%' },
      ]
    },
    getDurationOption: () => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new CommonTranslation(lang);
      return [
        { id: '60', text: translate.commonTranslation.hour },
        { id: '30', text: '30 ' + translate.commonTranslation.minute + 's' },
      ]
    },
    canSeeResources: () => {
        return !!Template.instance().canSeeResources.get()
    },
    MbTextArea: () => MbTextArea,
    canAddResources: () => {
        return !!Template.instance().canAddResources.get()
    },
    CDD_cursor: () => {
        return Meteor.listCondoUserHasRight("reservation", "addResources");
    },
    editTermsCb: () => {
      const t = Template.instance()

      return (value) => {
        t.edited_cgv.set(value)
      }
    },
    getCondoId: () => {
      return Template.instance().form.get('condoId')
    },
    getTermsOfServices: (forceAll = false) => {
      const condoId = Template.instance().form.get('condoId')
      const terms = Template.instance().edited_cgv.get() !== null ? Template.instance().edited_cgv.get() : (TermsOfServices.findOne({ condoId }) || {value: ''}).value
      let displayReadMore = false
      let ret = ''
      if (terms) {
        if (terms.length > 500) {
          if (Template.instance().isMore.get() || forceAll) {
            ret = terms
          } else {
            ret = terms.substring(0, 500)
          }
          displayReadMore = true
        } else {
          displayReadMore = false
          ret = terms
        }
      }
      return { text: ret, displayReadMore }
    },
});
