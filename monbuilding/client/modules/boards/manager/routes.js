import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import PageForumContainer from '/client/modules/boards/common/forum/component/forum'
import DetailsForumPost from '/client/modules/boards/common/forum/component/detail_forum_post'

let gestionnaireRoutes = FlowRouter.group({
	prefix: '/:lang/gestionnaire',
	name: 'app.gestionnaire',
	triggersEnter: [
	function (context, redirect) {
    SEO.set(domainConfig.SEOconfig)
		Session.set("target_url", null);
		if (!Meteor.user() && !Meteor.loggingIn()){
			Session.set("target_url", context.path);
			FlowRouter.go("app.login.login", {lang: 'fr'});
		}
		else {
			// Meteor.call('user_isGestionnaire', function (err) {
			// 	if (err) {
			// 		if (err.error === 300)
			// 			FlowRouter.go("app.errorNotAuthenticated");
			// 		else
			// 			FlowRouter.go("app.errorAccessDenied");
			// 	}
			// });
		}
	}
	]
});

gestionnaireRoutes.route('/', {
	name: 'app.gestionnaire',
	action () {
		BlazeLayout.render('gestionnaire');
	}
});


gestionnaireRoutes.route('/buildingsview', {
  name: 'app.gestionnaire.buildingsview',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'buildingsView' });
  }
});

gestionnaireRoutes.route('/emergencyContact/:condo_id', {
  name: 'app.gestionnaire.emergencyContact',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'emergencyContact' });
  }
});

gestionnaireRoutes.route('/emergencyContact/:condo_id/detail/:contactId', {
  name: 'app.gestionnaire.emergencyContact.detail',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'emergencyContactDetail' });
  }
});

gestionnaireRoutes.route('/condosview/:condoId?', {
  name: 'app.gestionnaire.condosview',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'condosView' });
	}
});

gestionnaireRoutes.route('/managerList', {
	name: 'app.gestionnaire.managerList',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'managerList' });
	}
});

gestionnaireRoutes.route('/managerList/createManager/personalDetails', {
	name: 'app.gestionnaire.managerList.personalDetails',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'personalDetails' });
	}
});

gestionnaireRoutes.route('/managerList/createManager/personalDetails/:condoId', {
	name: 'app.gestionnaire.managerList.personalDetails.condoId',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'personalDetails' });
	}
});

gestionnaireRoutes.route('/managerList/:userId/managerProfile/:tab', {
	name: 'app.gestionnaire.managerList.managerProfile',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'profileDetails' });
	}
});

// gestionnaireRoutes.route('/usersview', {
//   name: 'app.gestionnaire.usersview',
// 	action () {
//     BlazeLayout.render('gestionnaire', { template: 'usersView' });
// 	}
// });

// gestionnaireRoutes.route('/usersview/:tab', {
//   name: 'app.gestionnaire.usersview.tab',
//     action () {
//       BlazeLayout.render('gestionnaire', { template: 'usersView' });
//   }
// });

// gestionnaireRoutes.route('/usersview/:userId/occupantProfile/:tab', {
//   name: 'app.gestionnaire.usersview.occupantProfile',
//   action () {
//     BlazeLayout.render('gestionnaire', { template: 'profileDetails' });
//   }
// });

// gestionnaireRoutes.route('/usersview/createOccupant/personalDetails', {
//   name: 'app.gestionnaire.usersview.personalDetails',
//   action () {
//     BlazeLayout.render('gestionnaire', { template: 'personalDetailsOccupant' });
//   }
// });

// gestionnaireRoutes.route('/occupantList', {
//   name: 'app.gestionnaire.occupantList',
//   action () {
//     BlazeLayout.render('gestionnaire', { template: 'manager_occupantList' });
//   }
// });

gestionnaireRoutes.route('/occupantList/:tab?', {
  name: 'app.gestionnaire.occupantList.tab',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'manager_occupantList' });
  }
});

gestionnaireRoutes.route('/occupantList/:userId/occupantProfile/:tab', {
  name: 'app.gestionnaire.occupantList.occupantProfile',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'profileDetails' });
  }
});

gestionnaireRoutes.route('/occupantList/createOccupant/personalDetails', {
  name: 'app.gestionnaire.occupantList.personalDetails',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'personalDetailsOccupant' });
  }
});

gestionnaireRoutes.route('/messenger/:tab/new', {
  name: 'app.gestionnaire.messenger.new',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'messengerManager' })
  }
})

gestionnaireRoutes.route('/messenger/:tab/details/:msgId', {
  name: 'app.gestionnaire.messenger.detail',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'messageDetailsManager' })
  }
})

gestionnaireRoutes.route('/messenger/:tab/:msgId?', {
  name: 'app.gestionnaire.messenger',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'messengerManager' })
  }
})

gestionnaireRoutes.route('/marketPlace/servicesDetails/:serviceId', {
  name: 'app.gestionnaire.marketPlace.servicesDetails',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'manager_marketPlace_servicesDetails' })
  }
})

gestionnaireRoutes.route('/marketPlace/newService/', {
  name: 'app.gestionnaire.marketPlace.newService',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'manager_marketPlace_newService' })
  }
})

gestionnaireRoutes.route('/marketPlace/editService/:serviceId', {
  name: 'app.gestionnaire.marketPlace.editService',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'manager_marketPlace_editService' })
  }
})

gestionnaireRoutes.route('/marketPlace/:tab/:reservationId?', {
  name: 'app.gestionnaire.marketPlace.tab',
  action () {
    BlazeLayout.render('gestionnaire', { template: 'manager_marketPlace' })
  }
})

gestionnaireRoutes.route('/laundryRoom/:tab', {
  name: 'app.gestionnaire.laundryRoom.tab',
  action() {
    BlazeLayout.render('gestionnaire', { template: 'laundryRoom' })
  }
})

gestionnaireRoutes.route('/profile/:tab?', {
	name: 'app.gestionnaire.profile.tab',
	action () {
    BlazeLayout.render('gestionnaire', { template: 'managerProfile' });
	}
});

gestionnaireRoutes.route('/invitations', {
	name: 'app.gestionnaire.invitations',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'invite_residents'});
	}
});

gestionnaireRoutes.route('/contact', {
	name: 'app.gestionnaire.contact',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'contactGestionnaire' });
	}
});

gestionnaireRoutes.route("/incidents", {
	name: 'app.gestionnaire.incidents',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'incident_gestionnaire_index'})
	}
});

gestionnaireRoutes.route("/incidents/:incidentId", {
	name: 'app.gestionnaire.incidents.id',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'incident_gestionnaire_index'})
	}
});

gestionnaireRoutes.route('/contactManagement', {
	name: 'app.gestionnaire.contactManagement',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'contactManagement' });
	}
});

gestionnaireRoutes.route('/contactManagement/:condoId/:tab', {
	name: 'app.gestionnaire.contactManagement.tab',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'contactManagement' });
	}
});

gestionnaireRoutes.route('/planning/:condo_id/ressources', {
	name: 'app.gestionnaire.ressources',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'ressources' });
	}
});

gestionnaireRoutes.route("/planning/:condo_id/ressources/form/:resourceId", {
    name: 'app.gestionnaire.ressources.form',
    action () {
        BlazeLayout.render('gestionnaire', { template: 'resources_form'})
    }
});

gestionnaireRoutes.route('/planning/:condo_id?', {
	name: 'app.gestionnaire.planningid',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'planning' });
	}
});

gestionnaireRoutes.route('/planning/', {
	name: 'app.gestionnaire.planning',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'planning' });
	}
});

gestionnaireRoutes.route('/documents', {
	name: 'app.gestionnaire.documents',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'documentsGestionnaire' });
	}
});

gestionnaireRoutes.route('/concierge', {
	name: 'app.gestionnaire.concierge',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'module_concierge'});
	}
});

gestionnaireRoutes.route('/concierge/:condoId', {
	name: 'app.gestionnaire.concierge.condo',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'module_concierge'});
	}
});

gestionnaireRoutes.route('/forum', {
	name: 'app.gestionnaire.forum',
	action: function(params, query) {
		BlazeLayout.render('gestionnaire', { template: 'forum'});
	}
});

gestionnaireRoutes.route('/forum/:condo_id/:postId?', {
	name: 'app.gestionnaire.forum.postId',
	reactComponent: function () { return DetailsForumPost; },
	action: function(params, query) {
		BlazeLayout.render('gestionnaire', { template: 'forum'});
	}
});

gestionnaireRoutes.route('/classifieds', {
	name: 'app.gestionnaire.classifieds',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'classifieds'});
	}
});

gestionnaireRoutes.route('/classifieds/:condo_id', {
	name: 'app.gestionnaire.classifieds.condo',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'classifieds'});
	}
});

gestionnaireRoutes.route('/classifieds/:condo_id/?ad=:ad_id&back=:isBack', {
	name: 'app.gestionnaire.classifieds.ad',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'module_classifieds_view_ad'});
	}
});

gestionnaireRoutes.route('/informations', {
	name: 'app.gestionnaire.informations',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'informationsGestionnaire' });
	}
});

gestionnaireRoutes.route('/stats', {
	name: 'app.gestionnaire.stats',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'stats' });
	}
});

gestionnaireRoutes.route('/evenements', {
	name: 'app.gestionnaire.evenements',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'evenementsGestionnaire' });
	}
});

gestionnaireRoutes.route('/evenements/:eventId', {
	name: 'app.gestionnaire.evenements.eventId',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'evenementsGestionnaire' });
	}
});

gestionnaireRoutes.route('/informations/:infoId', {
	name: 'app.gestionnaire.informations.infoId',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'informationsGestionnaire' });
	}
});

gestionnaireRoutes.route('/integrations', {
	name: 'app.gestionnaire.integrations',
	action () {
		BlazeLayout.render('gestionnaire', {template: 'integrationsM' });
	}
});

gestionnaireRoutes.route('/integrations/tab/:tab', {
	name: 'app.gestionnaire.integrations.tab',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'integrationsM' });
	}
});

gestionnaireRoutes.route('/integrations/detail/:integrationId', {
	name: 'app.gestionnaire.integrationDetail',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'integrationDetailM' });
	}
});

gestionnaireRoutes.route('/integrations/condo/:condoId', {
	name: 'app.gestionnaire.integrationCondo',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'integrationCondoM' });
	}
});

gestionnaireRoutes.route('/integrations/config/:condoId/:integrationId', {
	name: 'app.gestionnaire.integrationConfig',
	action () {
		BlazeLayout.render('gestionnaire', { template: 'integrationConfigM' });
	}
});
