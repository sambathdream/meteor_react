/**
 * Created by kuyarawa on 10/05/18.
 */
import { CommonTranslation } from "/common/lang/lang.js"

Template.emergencyContact.onCreated(function () {
  this.subscribe('PhoneCode');
  this.subscribe('emergencyContactPhotos');
  this.search = new ReactiveVar("");
  this.rights = new ReactiveVar()
  this.isAllSelected = new ReactiveVar(FlowRouter.getParam('condo_id') === 'all')

  let self = this
  let firstLoad = true
  this.autorun(() => {
    const condoId = Template.currentData().selectedCondoId
    let params = FlowRouter.current().params
    if (params.condo_id !== condoId || firstLoad) {
      firstLoad = false
      params.condo_id = condoId
      FlowRouter.setParams(params)
      self.isAllSelected.set(condoId === 'all')
      this.subscribe('emergencyContacts', !!condoId && condoId !== 'all' ? condoId : null);
    }
  });
})

Template.emergencyContact.events({
  'click .dropdown-condo' (event, template) {
    let id = event.currentTarget.getAttribute('data-id');
    FlowRouter.go("app.gestionnaire.emergencyContact", { lang: (FlowRouter.getParam("lang") || "fr"), condo_id: id});
    template.condoSelected.set(id);
  },
  'click .remove' (event, template) {
    const selectedCondo = Template.currentData().selectedCondoId
    const contactId = event.currentTarget.getAttribute('contactid');
    const name = event.currentTarget.getAttribute('contactname');
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)
    bootbox.confirm({
      size: "medium",
      title: tr_common.commonTranslation["confirmation"],
      message: tr_common.commonTranslation["confirm_delete_mrs"] + ' "' + name + '" ' + tr_common.commonTranslation["as_emergency_contact"],
      buttons : {
        'cancel' : {label: tr_common.commonTranslation["cancel"], className: "btn-outline-red-confirm"},
        'confirm' : {label: tr_common.commonTranslation["confirm"], className: "btn-red-confirm"}
      },
      backdrop: true,
      callback: function(result) {
        if (result) {
          Meteor.call('deleteEmergencyContact', selectedCondo, contactId);
        }
      }
    });
  },
  'click .contactDetail' (e, t) {
    const contactId = e.currentTarget.getAttribute('contactid');
    FlowRouter.go("app.gestionnaire.emergencyContact.detail", { lang: (FlowRouter.getParam("lang") || "fr"), condo_id: Template.currentData().selectedCondoId, contactId: contactId});
  }
})

Template.emergencyContact.helpers({
  isSubscribeReady: () => {
    const isReady = Template.instance().subscriptionsReady()
    return isReady
  },
  isNotRestricted: () => {
    const condoId = Template.currentData().selectedCondoId
    if (condoId === 'all') {
      return !!Meteor.listCondoUserHasRight('emergencyContact', 'see')
    } else {
      return Meteor.userHasRight('emergencyContact', 'see', condoId)
   }
  },
  canAddNumber: () => {
    const condoId = Template.currentData().selectedCondoId
    if (condoId === 'all') {
      return !!Meteor.listCondoUserHasRight('emergencyContact', 'add')
    } else {
      return Meteor.userHasRight('emergencyContact', 'add', condoId)
   }
  },
  EmergencyContacts: () => {
    const instance = Template.instance();
    const regexp = new RegExp(instance.search.get(), "i");
    return EmergencyContact.find({$or: [
      {"firstName": regexp},
      {"lastName": regexp},
      {"companyName": regexp},
      {"phone": regexp},
      {"landLine": regexp},
      {"inChargeOf": regexp}
    ]}, {sort: {firstName: 1}}).fetch()
  },
  isSearchMode: () => {
    return Template.instance().search.get() !== ""
  },
  getAvatar: (contactId) => {
    let avatar = EmergencyContactPhotos.findOne({ contactId: contactId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.avatar
    }
    return null
  },
  getContactName: (contactId, initial) => {
    let contact = EmergencyContact.findOne(contactId);
    if (contact) {
      return !!initial ? `${contact.firstName[0]}${contact.lastName[0]}`: `${contact.firstName} ${contact.lastName}`;
    }
  },
  onSearch: () => {
    const instance = Template.instance();

    return (text) => {
      instance.search.set(text);
    }
  },
  getSelectedCondo: () => {
    return Template.currentData().selectedCondoId
  },
  canDelete: () => {
    return (Template.currentData().selectedCondoId === "all" ? Meteor.listCondoUserHasRight("emergencyContact", "delete").length > 0 : Meteor.userHasRight('emergencyContact', 'delete', Template.currentData().selectedCondoId))
  },
  getPhone: (contact) => {
    if (!!contact) {
      let html = ''

      if (!!contact.phone) {
        html += '<p>' + '+' + contact.phoneCode + ' ' + contact.phone + '</p>'
      }

      if (!!contact.landLine) {
        html += '<p>' + '+' + contact.landLineCode + ' ' + contact.landLine + '</p>'
      }

      return html
    }
  },
  getInChargeOf: (contact) => {
    const lang = FlowRouter.getParam("lang") || "fr"

    return !!contact ? (!!contact['inChargeOf-' + lang] ? contact['inChargeOf-' + lang] : contact.inChargeOf) : ''
  },
  isAllSelected: () => Template.instance().isAllSelected.get()
})
