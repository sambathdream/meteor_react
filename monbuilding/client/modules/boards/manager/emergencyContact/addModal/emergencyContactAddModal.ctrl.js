/**
 * Created by kuyarawa on 11/05/18.
 */
import { CommonTranslation, ResourceForm, ModuleReservationIndex } from "/common/lang/lang.js"

import { FileManager } from "/client/components/fileManager/filemanager.js";

const PNF = require('google-libphonenumber').PhoneNumberFormat;
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

const DEFAULT_TIME = {
  start: '08:00',
  end: '20:00'
}

Template.emergencyContactAddModal.onCreated(function () {
  this.subscribe('PhoneCode');
  this.isOpen = new ReactiveVar(false)
  this.weekDays = () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);

    return [
      {
        dow: 1,
        day: translate.moduleReservationIndex['monday']
      }, {
        dow: 2,
        day: translate.moduleReservationIndex['tuesday']
      }, {
        dow: 3,
        day: translate.moduleReservationIndex['wednesday']
      }, {
        dow: 4,
        day: translate.moduleReservationIndex['thursday']
      }, {
        dow: 5,
        day: translate.moduleReservationIndex['friday']
      }, {
        dow: 6,
        day: translate.moduleReservationIndex['saturday']
      }, {
        dow: 0,
        day: translate.moduleReservationIndex['sunday']
      }
    ];
  }

  this.getDefaultCondoData = () => {
    return {
      id: null,
      schedule: _.map(this.weekDays(), (wd) => ({
        dow: wd.dow,
        open: wd.dow > 0 && wd.dow < 6,
        openHours: [DEFAULT_TIME]
      }))
    }
  }

  this.fm = new FileManager(EmergencyContactPhotoFiles, {uploader: Meteor.userId()});
  this.form = new ReactiveDict();
  this.id = new ReactiveVar('');
  this.defaultData = {
    civility: '-',
    firstName: null,
    lastName: null,
    companyName: null,
    inChargeOf: null,
    "inChargeOf-fr": null,
    email: null,
    phoneCode: '33',
    phone: null,
    phoneOk: true,
    landLineCode: '33',
    landLine: null,
    landLineOk: true,
    isMbUser: false,
    condos: [this.getDefaultCondoData()]
  }
  this.form.setDefault(this.defaultData)
  this.canSubmit = new ReactiveVar(false);

  this.validateForm = () => {
    const form = this.form;
    const requiredFields = ['firstName', 'lastName', 'companyName', 'inChargeOf', 'inChargeOf-fr'];

    const email = this.form.get('email')
    const phone = this.form.get('phone')
    const landLine = this.form.get('landLine')

    const invalidFields = _.filter(requiredFields, (req) => {
      const val = form.get(req);
      return _.isNull(val) || val === '' || typeof val === 'undefined'
    });

    const validCondo = !_.contains(_.map(form.get('condos'), c => {
      return _.isNull(c.id)
    }), true)

    let valid = false
    if (invalidFields.length === 0 && (!email || (!!email && (Isemail.validate(email) === true)) &&
      (phone === null || phone === '' || form.get('phoneOk') === true) &&
      (landLine === null || landLine === '' || form.get('landLineOk') === true)) && validCondo) {
      valid = true;
    }

    if (this.canSubmit.get() !== valid) {
      this.canSubmit.set(valid)
    }
  }

  this.getCondoByIndex = (index) => {
    const condos = this.form.get('condos');
    return condos[index];
  }

  this.initTimePicker = () => {
    $('.startTimeG, .endTimeG').datetimepicker({
      locale: 'fr',
      format: "HH:mm",
      widgetPositioning: {
        vertical: 'bottom',
        horizontal: 'auto'
      }
    });
  };
})

const sample = {
  _id: 'xxx',
  civility: 'mr',
  firstName: 'kuya',
  lastName: 'rawa',
  companyName: 'universe',
  inChargeOf: 'life',
  "inChargeOf-fr": 'life',
  email: 'kuyarawa@gmail.com',
  phoneCode: '62',
  phone: '85624667561',
  landLineCode: '62',
  landLine: '2286615501',
  condos: [{
    id: 'condo1',
    schedule: [{
      dow: 0,
      open: true,
      openHours: [{
        start: '08:00',
        end: '10:00'
      }, {
        start: '12:00',
        end: '20:00'
      }]
    }]
  }]
}

Template.emergencyContactAddModal.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      let number = '+' + selected + t.form.get(key)
      t.form.set(key + 'Code', selected)
      try {
        number = phoneUtil.parse(number, "FR")
        if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
          t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL));
          t.form.set(key + 'Ok', true);
        } else {
          t.form.set(key + 'Ok', false);
        }
      } catch (e) {
        t.form.set(key + 'Ok', false);
      }

      t.validateForm()
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      let number = '+' + t.form.get(key + 'Code') + value
      try {
        number = phoneUtil.parse(number, "FR")
        if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
          t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL));
          t.form.set(key + 'Ok', true);
        } else {
          t.form.set(key, !value ? null : value);
          t.form.set(key + 'Ok', false);
        }
      } catch (e) {
        t.form.set(key, !value ? null : value);
        t.form.set(key + 'Ok', false);
      }

      t.validateForm()
    }
  },
  isFieldError: (key) => {
    const t = Template.instance();
    const value = t.form.get(key);
    switch (key) {
      case 'email': {
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        let number = '+' + t.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.form.set(key + 'Ok', res);
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.form.set(key + 'Ok', false);
          return value !== null && value !== ''
        }
      }
      case 'landLine': {
        let number = '+' + t.form.get('landLineCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.form.set(key + 'Ok', res);
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.form.set(key + 'Ok', false);
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  getValue: (key) => {
    return Template.instance().form.get(key);
  },
  setValue: (key) => {
    const instance = Template.instance();

    return () => val => {
      instance.form.set(key, val);
      instance.validateForm()
    }
  },
  getCiviliteOptions: () => {
    const lang = (FlowRouter.getParam("lang") || "fr")
    const translation = new CommonTranslation(lang);
    return [
      {text: translation.commonTranslation['mister'], id: 'mister'},
      {text: translation.commonTranslation['miss'], id: 'miss'}
    ]
  },
  getProfileUrl: () => {
    let avatar = EmergencyContactPhotos.findOne({ contactId: Template.instance().id.get() })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  selectedCondo: () => {
    return Template.instance().form.get('condos')
  },
  canAddMoreCondo: () => {
    const instance = Template.instance()
    const selectedCondos = instance.form.get('condos')
    const empty = _.filter(selectedCondos, c => c.id === null).length
    const alreadySelectedCondo = _.map(selectedCondos, c => c.id);
    const condos = []
    Condos.find({}).forEach(condo => {
      if (!_.contains(alreadySelectedCondo, condo._id) && Meteor.userHasRight('emergencyContact', 'add', condo._id)) {
        condos.push(condo._id)
      }
    })

    return empty === 0 && condos.length > 0
  },
  onSelectCondo: (index) => {
    const t = Template.instance()

    return () => selected => {
      let formCondo = t.form.get('condos')
      formCondo[index].id = !selected ? null : selected
      t.form.set('condos', formCondo)
      t.validateForm()
    }
  },
  moreThanOneCondoSelected: () => {
    return Template.instance().form.get('condos').length > 1
  },
  getCondoOptions: (index) => {
    const selectedCondos = Template.instance().form.get('condos');
    if (!!selectedCondos[index]) {
      const indexId = selectedCondos[index].id;
      const alreadySelectedCondo = _.map(selectedCondos, c => c.id);
      let condos = []
      Condos.find({}).forEach(condo => {
        if ((indexId === condo._id || !_.contains(alreadySelectedCondo, condo._id)) && Meteor.userHasRight('emergencyContact', 'add', condo._id)) {
          condos.push({
            text: condo.getName(),
            id: condo._id
          })
        }
      })
      return condos
    }
  },
  getWeekdays: () => {
    return _.map(Template.instance().weekDays(), (d) => {
      return {
        key: d.dow,
        value: d.day
      }
    })
  },
  allDayArgs: (index) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ResourceForm(lang);
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);

    if (!!data) {
      return {
        checked : !!data.allDay,
        label: translate.resourceForm['all_days'],
        onClick: () => {
          const status = !!data.allDay;
          instance.form.set('condos', _.map(instance.form.get('condos'), (c, i) => {
            return i !== index ? c : {
              ...c,
              allDay: !status,
              ...(!status ? {
                schedule: _.map(instance.weekDays(), (wd) => ({
                  dow: wd.dow,
                  open: wd.dow > 0 && wd.dow < 6,
                  openHours: [DEFAULT_TIME]
                }))
              }: {})
            }
          }));
        }
      };
    }
  },
  dayArgs: (day, index) => {
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);
    if (!!data) {
      const schedule = _.find(data.schedule, (h) => {
        return h.dow === day.key
      });
      return {
        checked : !!schedule.open,
        label: day.value,
        onClick: () => {
          const status = !!schedule.open;
          instance.form.set('condos', _.map(instance.form.get('condos'), (c, i) => {
            return i !== index ? c : {
              ...c,
              schedule: _.map(c.schedule, s => {
                return s.dow !== day.key ? s : {
                  ...s,
                  open: !status
                }
              })
            }
          }));
        }
      }
    }
  },
  getOpenHours: (day, index) => {
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);
    if (!!data) {
      const schedule = _.find(data.schedule, (h) => {
        return h.dow === day.key
      });
      if (!!schedule.openHours && schedule.openHours.length > 0) {
        return schedule.openHours;
      } else {
        return [{start: DEFAULT_TIME.start, end: DEFAULT_TIME.end}];
      }
    }
  },
  isDayClose: (day, index) => {
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);
    if (!!data) {
      const schedule = _.find(data.schedule, (h) => {
        return h.dow === day.key
      });
      return !schedule.open;
    }
  },
  isMoreThanOneSlot: (day, index) => {
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);
    if (!!data) {
      const schedule = _.find(data.schedule, (h) => {
        return h.dow === day.key
      });
      return !!schedule.openHours && schedule.openHours.length > 1;
    }
  },
  isLastTimeSlotAndOpen: (day, i, index) => {
    const instance = Template.instance();
    const data = instance.getCondoByIndex(index);
    if (!!data) {
      const schedule = _.find(data.schedule, (h) => {
        return h.dow === day.key
      });
      return !!schedule.openHours && schedule.openHours.length === i + 1 && schedule.open;
    }
  },
  initTimePicker: () => {
    const instance = Template.instance();

    setTimeout(() => {
      instance.initTimePicker()
    }, 500);
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get();
  },
  managers: function() {
    const condoIds = Meteor.listCondoUserHasRight("emergencyContact", "add")
    let users = []
    Enterprises.find({ condos: { $in: condoIds } }).forEach(enterprise => {
      enterprise.users.forEach(user => {
        let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
        users.push({
          id: user.userId,
          value: thisUserProfile.firstname
        })
      })
    })
    return users
  },
  selectedManagers: function(event, suggestion, datasetName) {
    const user = UsersProfile.findOne({ _id: suggestion.id })
    const instance = Template.instance()
    if (user) {
      instance.form.set({
        firstName: user.firstname,
        lastName: user.lastname,
        email: user.email,
        phoneCode: user.telCode,
        phone: user.tel,
        landLineCode: user.tel2Code,
        landLine: user.tel2,
        isMbUser: true,
      })
      instance.validateForm();
    }
  },
  isEdit: () => {
    return !!Template.instance().data.editId
  }
})

Template.emergencyContactAddModal.rendered = function () {
  Meteor.typeahead.inject();
}

Template.emergencyContactAddModal.events({
  'click #save' (e, t) {
    const data = _.pick(t.form.all(), 'civility', 'firstName', 'lastName', 'companyName', 'inChargeOf', 'inChargeOf-fr', 'email', 'phoneCode', 'phone', 'landLineCode', 'landLine', 'condos', 'isMbUser');
    Meteor.call('submitEmergencyContact', t.id.get(), data, (error, result) => {
      if (!error) {
        t.form.set(t.defaultData);
        $('#emergency-contact-add-modal').modal('hide');
      }
    });
  },
  'input #mb-input-first-name' (e, t) {
    t.form.set('firstName', e.currentTarget.value);
    t.validateForm();
  },
  'shown.bs.modal #emergency-contact-add-modal': function(event, t) {
    if (!!t.data.editId) {
      t.id.set(t.data.editId);
      const editData = EmergencyContact.findOne(t.data.editId)
      const defaultData = {
        ...t.defaultData,
        ...(!!editData ? editData: {})
      }
      t.form.set(defaultData);
    } else {
      const newID = new Mongo.ObjectID
      t.form.set(t.defaultData);
      t.id.set(newID._str)
    }
    let modal = $(event.currentTarget);
    window.location.hash = "#emergency-contact-add-modal";
    window.onhashchange = function () {
      if (location.hash != "#emergency-contact-add-modal") {
        modal.modal('hide');
      }
    }
    t.isOpen.set(true)
  },
  'hidden.bs.modal #emergency-contact-add-modal': function(event, t) {
    t.isOpen.set(false)
    t.id.set('');
    t.form.set(t.defaultData);
    history.replaceState('', document.title, window.location.pathname);
  },
  'blur .hp' (e, t) {
    const index = parseInt(e.currentTarget.getAttribute("data-index"));
    const cIndex = parseInt(e.currentTarget.getAttribute("data-cindex"));
    const type = e.currentTarget.getAttribute("data-type");
    const isAllDay = !!(t.form.get('condos') && t.form.get('condos')[cIndex] && t.form.get('condos')[cIndex].allDay);
    if (isAllDay) {
      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (newData.openHours && newData.openHours[index] && newData.openHours[index][type] && newData.open) {
              newData.openHours[index][type] = $(e.currentTarget).val();
            }
            return newData;
          })
        }
      }));
    } else {
      const dow = parseInt(e.currentTarget.getAttribute("data-dow"));

      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (newData.openHours && newData.openHours[index] && newData.openHours[index][type] && s.dow === dow) {
              newData.openHours[index][type] = $(e.currentTarget).val();
            }
            return newData;
          })
        }
      }));
    }

    setTimeout(() => {
      t.initTimePicker()
    }, 500);
  },
  'click .add-time-slot' (e, t) {
    const cIndex = parseInt(e.currentTarget.getAttribute("data-cindex"));
    const isAllDay = !!(t.form.get('condos') && t.form.get('condos')[cIndex] && t.form.get('condos')[cIndex].allDay);

    if (isAllDay) {
      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (newData.openHours && newData.open) {
              newData.openHours.push(DEFAULT_TIME)
            }
            return newData;
          })
        }
      }));
    } else {
      const dow = parseInt(e.currentTarget.getAttribute("data-dow"));
      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (s.dow === dow) {
              if (newData.openHours) {
                newData.openHours.push(DEFAULT_TIME)
              }
            }
            return newData;
          })
        }
      }));
    }

    setTimeout(() => {
      t.initTimePicker()
    }, 500);
  },
  'click .remove-time-slot' (e, t) {
    const index = parseInt(e.currentTarget.getAttribute("data-index"));
    const cIndex = parseInt(e.currentTarget.getAttribute("data-cindex"));
    const isAllDay = !!(t.form.get('condos') && t.form.get('condos')[cIndex] && t.form.get('condos')[cIndex].allDay);

    if (isAllDay) {
      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (newData.openHours && newData.open) {
              newData.openHours = _.reject(newData.openHours, (h, i) => {return i === index})
            }
            return newData;
          })
        }
      }));
    } else {
      const dow = parseInt(e.currentTarget.getAttribute("data-dow"));
      t.form.set('condos', _.map(t.form.get('condos'), (c, i) => {
        return i !== cIndex ? c : {
          ...c,
          schedule: _.map(c.schedule, (s) => {
            let newData = s;

            if (s.dow === dow) {
              if (newData.openHours) {
                newData.openHours = _.reject(newData.openHours, (h, i) => {return i === index})
              }
            }
            return newData;
          })
        }
      }));
    }

    setTimeout(() => {
      t.initTimePicker()
    }, 500);
  },
  'click #add-condo' (e, t) {
    let condos = t.form.get('condos');
    condos.push(t.getDefaultCondoData());
    t.form.set('condos', condos);

    setTimeout(() => {
      t.initTimePicker()
    }, 500);
  },
  'click .delete-condo' (e, t) {
    let condos = t.form.get('condos');
    const index = parseInt(e.currentTarget.getAttribute("data-index"));

    condos.splice(index, 1);
    t.form.set('condos', condos);
  },
  'click' (e, t) {
    $('#dropdownProfilePictureModal').addClass('displayNone')
  },
  'click .editProfilePicture' (event, template) {
    $('#dropdownProfilePictureModal').toggleClass('displayNone')
  },
  'click .uploadPhoto' (event, template) {
    $('#inputAvatar').click();
    $('#dropdownProfilePictureModal').addClass('displayNone')
  },
  'click .removePhoto' (event, template) {
    Meteor.call('removeEmergencyContactPhoto', template.id.get())
    $('#dropdownProfilePictureModal').addClass('displayNone')
  },
  'change #inputAvatar' (event, template) {
    if (event.currentTarget.files && event.currentTarget.files.length === 1) {
      template.fm.setCustomFields({
        uploader: Meteor.userId(),
        contactId: template.id.get()
      });
      template.fm.insert(event.currentTarget.files[0], function(err, file) {
        event.currentTarget.value = '';
        if (!err && file) {
          template.fm.clearFiles();
          Meteor.call('updateEmergencyContactPhoto', template.id.get(), file._id, Meteor.userId(), FlowRouter.current().params.condo_id);
        }
      });
    }
  }
})
