/**
 * Created by kuyarawa on 15/05/18.
 */
import { ModuleReservationIndex, CommonTranslation } from "/common/lang/lang.js"
import { FileManager } from "/client/components/fileManager/filemanager.js";

Template.emergencyContactDetail.onCreated(function () {
  this.subscribe('PhoneCode');
  this.subscribe('emergencyContactPhotos');
  this.search = new ReactiveVar("");
  this.fm = new FileManager(EmergencyContactPhotoFiles, {uploader: Meteor.userId()});
  // this.condoSelected = new ReactiveVar(FlowRouter.current().params.condo_id);
  this.subscribe('emergencyContacts', FlowRouter.getParam('condo_id'))
})

Template.emergencyContactDetail.events({
  'click #back-btn' (e, t) {
    FlowRouter.go("app.gestionnaire.emergencyContact", { lang: (FlowRouter.getParam("lang") || "fr"), condo_id: Template.currentData().selectedCondoId});
  },
  'click .remove' (event, template) {
    const condoId = event.currentTarget.getAttribute('condoid');
    const name = event.currentTarget.getAttribute('condoname');
    const lang = FlowRouter.getParam("lang") || "fr";
    const translate = new CommonTranslation(lang);
    bootbox.confirm({
      size: "medium",
      title: "Confirmation",
      message: 'Are you sure you want to delete "' + name + '" from this contact?',
      buttons : {
        'cancel' : {label: translate.commonTranslation['cancel'], className: "btn-outline-red-confirm"},
        'confirm' : {label: translate.commonTranslation['confirm'], className: "btn-red-confirm"}
      },
      backdrop: true,
      callback: function(result) {
        if (result) {
          Meteor.call('deleteEmergencyContact', condoId, FlowRouter.getParam("contactId"), () => {
            FlowRouter.go("app.gestionnaire.emergencyContact", { lang: lang, condo_id: FlowRouter.getParam("condo_id")});
          });
        }
      }
    });
  },
  // 'click' (e, t) {
  //   $('#dropdownProfilePictureDetail').addClass('displayNone')
  // },
  'click #editProfilePictureDetail, click .cancelDropdownDetail' (event, template) {
    $('#dropdownProfilePictureDetail').toggleClass('displayNone')
  },
  'click .uploadPhotoDetail' (event, template) {
    $('#inputAvatarDetail').click();
    $('#dropdownProfilePictureDetail').addClass('displayNone')
  },
  'click .removePhotoDetail' (event, template) {
    Meteor.call('removeEmergencyContactPhoto', FlowRouter.getParam("contactId"))
    $('#dropdownProfilePictureDetail').addClass('displayNone')
  },
  'change #inputAvatarDetail' (event, template) {
    if (event.currentTarget.files && event.currentTarget.files.length === 1) {
      template.fm.setCustomFields({
        uploader: Meteor.userId(),
        contactId: FlowRouter.getParam("contactId")
      });
      template.fm.insert(event.currentTarget.files[0], function(err, file) {
        event.currentTarget.value = '';
        if (!err && file) {
          template.fm.clearFiles();
          Meteor.call('updateEmergencyContactPhoto', FlowRouter.getParam("contactId"), file._id, Meteor.userId(), FlowRouter.current().params.condo_id);
        }
      });
    }
  }
})

Template.emergencyContactDetail.helpers({
  getContact: () => {
    return EmergencyContact.findOne(FlowRouter.getParam("contactId"))
  },
  getProfileUrl: () => {
    let avatar = EmergencyContactPhotos.findOne({ contactId: FlowRouter.getParam("contactId") })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  getPhone: (contact, isPhone) => {
    if (!!contact) {
      let phone = contact.phone
      let code = contact.phoneCode
      if (!!isPhone) {
        phone = contact.landLine
        code = contact.landLineCode
      }

      return !!phone ? '+' + code + ' ' + phone : ''
    }
  },
  onSearch: () => {
    const instance = Template.instance();

    return (text) => {
      instance.search.set(text);
    }
  },
  isSearchMode: () => {
    return Template.instance().search.get() !== ""
  },
  getCondoName: (condoId, initial) => {
    let condo = Condos.findOne(condoId);
    if ((condo && condo.info && condo.info.address && condo.name && condo.info.address === condo.name) || !condo.name)
      return !!initial ? condo.name[0]: condo.info.address;
    else
      return !!initial ? condo.name[0] : condo.name + ' ( ' + condo.info.address + ' )';
  },
  getAvatar: (condoId) => {
    let avatar = CondoPhotos.findOne({ condoId: condoId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.avatar
    }
    return null
  },
  filteredCondos: (condos) => {
    const instance = Template.instance();
    const regexp = new RegExp(instance.search.get(), "i");
    return _.filter(condos, (c) => {
      const condo = Condos.findOne(c.id);
      if (!!condo) {
        return (condo.name.match(regexp))
      } else {
        return false
      }
    })
  },
  renderSchedule: (condo) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);

    const label = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

    const scheduleComponent = []
    if (!!condo) {
      if (!!condo.alwaysOpen) {
        scheduleComponent.push(
          '24/24'
        )
      } else {
        let currentLabel = {
          start: null, end: null, hours: ''
        }

        // group the schedule
        condo.schedule.forEach((s, i) => {
          const dayLabel = translate.moduleReservationIndex[label[s.dow]].substring(0, 3)
          let hours = translate.moduleReservationIndex.close
          if (!!s.open) {
            hours = s.openHours.map((o) => { return o.start + ' - ' + o.end }).join(' / ')
          }

          if (_.isEqual(currentLabel.hours, hours)) {
            currentLabel.end = dayLabel
          } else {
            if (!!currentLabel.start) {
              scheduleComponent.push(
                `${currentLabel.start}${!!currentLabel.end ? '-' + currentLabel.end : ''}: ${currentLabel.hours}`
              )
            }
            currentLabel = {
              start: dayLabel,
              end: null,
              hours: hours
            }
          }

          // push the last data
          if (i === condo.schedule.length - 1) {
            scheduleComponent.push(
              `${currentLabel.start}${!!currentLabel.end ? '-' + currentLabel.end : ''}: ${currentLabel.hours}`
            )
          }
        })
      }
    }

    return !!condo && !!condo.schedule ? (
      scheduleComponent
    ) : null
  },
  canDelete: (condoId) => {
    return Meteor.userHasRight('emergencyContact', 'delete', condoId)
  },
  getContactId: () => {
    return FlowRouter.getParam("contactId")
  },
  getInChargeOf: (contact) => {
    const lang = FlowRouter.getParam("lang") || "fr"

    return !!contact ? (!!contact['inChargeOf-' + lang] ? contact['inChargeOf-' + lang] : contact.inChargeOf) : ''
  },
  getSelectedCondo: () => {
    return FlowRouter.getParam('condo_id')
  }
})
