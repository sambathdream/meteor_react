import {Pagination} from '/client/components/pagination/pagination.js';
import { FileManager } from "/client/components/fileManager/filemanager.js";

Template.condosView.onCreated(function() {
  this.search = new ReactiveVar("");
  this.condoIdFilter = new ReactiveVar(Template.currentData().selectedCondoId || null)
  this.condoId = new ReactiveVar(FlowRouter.getParam('condoId') || "");
  this.colors = new ReactiveDict();
  this.appDownloadUrl = new ReactiveDict();
  this.socialMediaUrl = new ReactiveDict();
  this.showCheck = new ReactiveVar(false);
  this.fm = new FileManager(CondoPhotoFiles, {uploader: Meteor.userId()});
  this.logo = new FileManager(CondoLogoFiles, {uploader: Meteor.userId()});
  this.changedAccountingOptions = []
  Meteor.call('setNewAnalytics', {type: "module", module: "condoView", accessType: "web", condoId: ''});

  this.setTheme = () => {
    document.body.style.setProperty("--primary-color", this.colors.get('primary'));
    document.body.style.setProperty("--secondary-color", this.colors.get('secondary'));
  }
});

Template.condosView.onRendered(() => {
});

Template.condosView.events({
  'input #searchUser' (event, template) {
    template.search.set(event.currentTarget.value);
  },
  'click .condoDetail' (e, t) {
    let id = e.currentTarget.getAttribute('condoId');
    FlowRouter.setParams({ condoId: id })
    t.condoId.set(id);
  },
  'click #update-condo' (e, t) {
    const data = {
      showPicture: t.showCheck.get(),
      colorPalette: t.colors.all(),
      appDownloadUrl: t.appDownloadUrl.all(),
      socialMediaUrl: t.socialMediaUrl.all(),
    }

    const changedAccountingOptions = t.changedAccountingOptions

    Meteor.call('updateCondoSettings', t.condoId.get(), data, () => {
      if (changedAccountingOptions.length) {
        Meteor.call('saveCondoAccountingValue', t.condoId.get(), changedAccountingOptions, () => {
          FlowRouter.setParams({ condoId: '' })
          t.condoId.set('')
        })
      } else {
        FlowRouter.setParams({ condoId: '' })
        t.condoId.set('')
      }
    });
  },
  'click #back-btn-condo' (e, t) {
    FlowRouter.setParams({ condoId: '' })
    t.condoId.set('')
  },
  'click' (e, t) {
    $('.dropdownProfilePicture').addClass('displayNone')
    $('.dropdownLogoPicture').addClass('displayNone')
  },
  'click .editProfilePicture' (event, template) {
    $('.dropdownProfilePicture').toggleClass('displayNone')
  },
  'click .uploadPhoto' (event, template) {
    $('#inputAvatar').click();
    $('.dropdownProfilePicture').addClass('displayNone')
  },
  'click .removePhoto' (event, template) {
    Meteor.call('removeCondoPhoto', template.condoId.get())
    $('.dropdownProfilePicture').addClass('displayNone')
  },
  'change #inputAvatar' (event, template) {
    if (event.currentTarget.files && event.currentTarget.files.length === 1) {
      let img = new Image();
      img.src = window.URL.createObjectURL( event.currentTarget.files[0] );

      img.onload = function() {
        const width = img.naturalWidth;
        const height = img.naturalHeight;

        window.URL.revokeObjectURL( img.src );

        if( width > 100 && height > 100 ) {
          template.fm.setCustomFields({
            uploader: Meteor.userId(),
            condoId: template.condoId.get()
          });
          template.fm.insert(event.currentTarget.files[0], function(err, file) {
            event.currentTarget.value = '';
            if (!err && file) {
              template.fm.clearFiles();
              Meteor.call('updateCondoPhoto', template.condoId.get(), file._id, Meteor.userId());
            }
          });
        }
        else {
          sAlert.error("Image size must be greater 100px width and 100px height")
        }
      };
    }
  },
  'click .logoPicture' (event, template) {
    $('.dropdownLogoPicture').toggleClass('displayNone')
  },
  'click .uploadLogo' (event, template) {
    $('#inputLogo').click();
    $('.dropdownLogoPicture').addClass('displayNone')
  },
  'click .removeLogo' (event, template) {
    Meteor.call('removeCondoPhoto', template.condoId.get(), true)
    $('.dropdownLogoPicture').addClass('displayNone')
  },
  'change #inputLogo' (event, template) {
    if (event.currentTarget.files && event.currentTarget.files.length === 1) {
      template.logo.setCustomFields({
        uploader: Meteor.userId(),
        condoId: template.condoId.get()
      });
      template.logo.insert(event.currentTarget.files[0], function(err, file) {
        event.currentTarget.value = '';
        if (!err && file) {
          template.logo.clearFiles();
          Meteor.call('updateCondoPhoto', template.condoId.get(), file._id, Meteor.userId(), true);
        }
      });
    }
  },
});

Template.condosView.helpers({
  updateCondoFilter: () => {
    const t = Template.instance()
    if (t.condoIdFilter.get() !== Template.currentData().selectedCondoId) {
      t.condoIdFilter.set(Template.currentData().selectedCondoId)
    }
    if (t.condoId.get() !== '' && t.condoId.get() !== Template.currentData().selectedCondoId && Template.currentData().selectedCondoId !== 'all') {
      FlowRouter.setParams({ condoId: Template.currentData().selectedCondoId })
      t.condoId.set(Template.currentData().selectedCondoId)
    }
  },
  Condos: () => {
    const condoId = Template.instance().condoIdFilter.get()
    const searchText = Template.instance().search.get()
    let regexp = new RegExp(searchText, "i");
    let query = {}

    if (condoId === 'all') {
      query = {$or: [
        {"info.address": regexp},
        {"info.city": regexp},
        {"info.code": regexp},
        {"name": regexp}
      ]}
    } else {
      query = {
        $and: [
          { _id: condoId },
          { $or: [
            { "info.address": regexp },
            { "info.city": regexp },
            { "info.code": regexp },
            { "name": regexp }
          ] }
        ],
      }
    }

    return Condos.find(query);
  },
  getCondoName: (condoId, initial) => {
    let condo = Condos.findOne(condoId);
    if (!!condo) {
      return !!initial ? condo.name[0] : condo.getName();
    }
  },
  isDetail: () => {
    return Template.instance().condoId.get() !== ''
  },
  getCondo: () => {
    const instance = Template.instance();
    const condo = Condos.findOne(instance.condoId.get());

    let appDownloadUrl = {
      ios: '',
      android: ''
    }
    let socialMediaUrl = {
      facebook: '',
      twitter: '',
      instagram: ''
    }

    if (!!condo && !!condo.settings) {
      if (!!condo.settings.colorPalette) {
        instance.colors.set(condo.settings.colorPalette);
      } else {
        instance.colors.set(Session.get('defaultColorPalette'));
      }

      if (!!condo.settings.socialMediaUrl) {
        socialMediaUrl = {...socialMediaUrl, ...condo.settings.socialMediaUrl}
      }

      if (!!condo.settings.appDownloadUrl) {
        appDownloadUrl = {...appDownloadUrl, ...condo.settings.appDownloadUrl}
      }

      instance.showCheck.set(!!condo.settings.showPicture);
    } else {
      instance.colors.set(Session.get('defaultColorPalette'));
    }

    instance.socialMediaUrl.set(socialMediaUrl);
    instance.appDownloadUrl.set(appDownloadUrl);

    return condo;
  },
  onSearch: () => {
    const instance = Template.instance();

    return (text) => {
      instance.search.set(text);
    }
  },
  isSearchMode: () => Template.instance().search.get() !== "",
  setColor: (key) => {
    const instance = Template.instance();

    return () => (val) => {
      instance.colors.set(key, '#' + val)
    }
  },
  getColor: (key) => {
    return Template.instance().colors.get(key);
  },
  setDownloadUrl: (key) => {
    const instance = Template.instance();

    return () => (val) => {
      instance.appDownloadUrl.set(key, val)
    }
  },
  getDownloadUrl: (key) => {
    return Template.instance().appDownloadUrl.get(key);
  },
  setUrl: (key) => {
    const instance = Template.instance();

    return () => (val) => {
      instance.socialMediaUrl.set(key, val)
    }
  },
  getUrl: (key) => {
    return Template.instance().socialMediaUrl.get(key);
  },
  reset: () => {
    const instance = Template.instance();

    return () => {
      instance.colors.set(Session.get('defaultColorPalette'));

      instance.setTheme();
    }
  },
  getProfileUrl: () => {
    let avatar = CondoPhotos.findOne({ condoId: Template.instance().condoId.get() })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  getLogoUrl: () => {
    let avatar = CondoLogos.findOne({ condoId: Template.instance().condoId.get() })
    if (avatar && avatar.avatar) {
      return avatar.avatar.preview
    }
    return null
  },
  getAvatar: (condoId) => {
    let avatar = CondoPhotos.findOne({ condoId: condoId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.avatar
    }
    return null
  },
  preview: () => {
    const instance = Template.instance();

    return () => {
      instance.setTheme();
    }
  },
  getPicCheck: () => {
    return Template.instance().showCheck.get()
  },
  setPicCheck: () => {
    const instance = Template.instance();

    return () => {
      instance.showCheck.set(!instance.showCheck.get())
    }
  },
  isRequired: () => {
    const instance = Template.instance()

    return !!instance.appDownloadUrl.get('ios') || !!instance.appDownloadUrl.get('android')
  },
  isNotValid: () => {
    const instance = Template.instance()
    let valid = false
    if (
      (!instance.appDownloadUrl.get('ios') && !instance.appDownloadUrl.get('android')) ||
      (!!instance.appDownloadUrl.get('ios') && !!instance.appDownloadUrl.get('android'))
    ) {
      valid = true
    }

    return !valid
  },
  shouldDisplayRestrictedAccess: () => {
    const condoId = Template.instance().condoId.get() !== '' ? Template.instance().condoId.get() : Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('view', 'buildingList')
    } else if (Meteor.userHasRight('view', 'buildingList', condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
  getAccountingOptions: () => {
    const condoId = Template.instance().condoId.get()
    const thisCondo = Condos.findOne({ _id: condoId })
    if (thisCondo && thisCondo.settings && thisCondo.settings.accounting) {
      return Object.keys(thisCondo.settings.accounting)
              .filter(key => !!thisCondo.settings.accounting[key].isActive)
              .map(key => {
                return {
                  key,
                  ...thisCondo.settings.accounting[key]
                }
              })
    } else {
      return []
    }
  },
  hasOneAccountingOption: (accountingOptions) => {
    if (accountingOptions) {
      return accountingOptions.find(o => !!o.isActive)
    }
    return false
  },
  setAccountingOption: (key) => {
    const t = Template.instance()

    return () => (value) => {
      const index = t.changedAccountingOptions.findIndex(o => o.key === key)
      if (index === -1) {
        t.changedAccountingOptions.push({
          key,
          value
        })
      } else {
        t.changedAccountingOptions[index] = {
          key,
          value
        }
      }
    }
  }
})
