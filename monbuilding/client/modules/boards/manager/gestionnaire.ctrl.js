import Forum from '/client/modules/boards/common/forum/component/contextProvider'
import DetailsForumPost from '/client/modules/boards/common/forum/component/detail_forum_post'
import { ForumLang } from '/common/lang/lang'
import MbCondoDropdown from '/client/components/MbCondoDropdown/MbCondoDropdown.js'

const reactModules = [
  'forum'
]

function onPathChange () {
	FlowRouter.watchPathChange();
	const module_name = FlowRouter.current().path.split('/')[3];
	$("#menu-content li").removeClass("highlight");
	$("#menu-content li a").removeClass("highlight");

	if (module_name) {
		let element = $(`#menu-content li a[name='${module_name}']`);
		module_name == 'buildingsview' || module_name == 'stats' ? element.addClass('highlight') : element.parent().addClass('highlight');
	}
	else {
		$("#menu-content li a[name='buildingsview']").addClass('highlight');
	}
};

Template.gestionnaire.onRendered(function () {
	var template = Template.instance();
	Meteor.call('isAdmin', Meteor.userId(), function (error, result) {
		template.isGestionnaireAsAdmin.set(result);
	});
	setTimeout(function() {
		$(document).ready(function(){
			$('[data-toggle="popover"]').popover();
			let logo_width = $('.other-logo-mobile').width();
			$('.logo-mobile-container').width(155 + logo_width);
		});
	}, 100);
});

Template.gestionnaire.onDestroyed(function () {
});

Template.gestionnaire.onCreated(function () {
  Meteor.isManager = true;
  Meteor.isAdmin = false
  this.selectedTab = new ReactiveVar("gestion"); /* resume, edit, resident, admin */
	this.condoId = new ReactiveVar("all"); /* resume, edit, resident, admin */
	let module = FlowRouter.current().path.substring(1).split('/')[2];
	if (!module) {
		const lang = FlowRouter.getParam("lang") || "fr";
		FlowRouter.go("app.gestionnaire.buildingsview", { lang });
	}
	let self = this;
  this.currentModule = new ReactiveVar((module !== undefined ? module : ''));
  this.subscribe("old-data", () => {
    this.subscribe("board_gestionnaire", function(error) {
      if (!error) {
        let usersEnterprise = Enterprises.findOne().users;
        let condoIds = _.map(_.find(usersEnterprise, (user) => { return user.userId == Meteor.userId() }).condosInCharge, (condo) => { return condo.condoId })
        self.subscribe('usersProfile', condoIds);

        // get enterprise color palette
        // const user = Meteor.user();
        // if (user && user.identities) {
        //   const gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
        //   if (gestionnaire) {
        //     if (!!gestionnaire.colorPalette) {
        //       Session.set('colorPalette', gestionnaire.colorPalette);
        //     }
        //   }
        // }
      }
    });
  });
	this.subscribe('condodocuments');
	this.subscribe('resources');
	this.subscribe("avatarLinksGestionnaire");
	this.subscribe('getDefaultRoles');
  this.subscribe('getCondosRole');
	this.isGestionnaireAsAdmin = new ReactiveVar(false);
	this.autorun(onPathChange);
	Meteor.subscribe('badges');

	this.autorun(() => {
    // set color palette based on condo settings
    const id = FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || localStorage.getItem('condoIdSelected')

    let palette = Session.get('defaultColorPalette');
    if (!!id && id !== 'all') {
      const condo = Condos.findOne(id);
      if (!!condo && !!condo.settings && !!condo.settings.colorPalette) {
        palette = condo.settings.colorPalette
      }
      Session.set('currentColorPalette', palette);
    } else {

      // check if all condo is selected, and all specific condo has the same color
      const condoIds = Meteor.getManagerCondoIds()
      const condos = Condos.find({_id: {$in: condoIds}}).fetch()
      const firstItem = condos[0]
      if (firstItem && firstItem.settings && firstItem.settings.colorPalette) {
        const firstColor = firstItem.settings.colorPalette
        const sameColorCondo = _.filter(condos, (c) => {
          return !!c.settings && !!c.settings.colorPalette && _.isEqual(c.settings.colorPalette, firstColor)
        });
        if (sameColorCondo.length === condos.length) {
          Session.set('currentColorPalette', firstColor);
        }
      }
      // if no condo selected don't change the color
      palette = !!Session.get('currentColorPalette') ? Session.get('currentColorPalette'): Session.get('defaultColorPalette');
    }

    // document.body.style.setProperty("--column-background-color", palette.columnBackground);
    // document.body.style.setProperty("--menu-selected-item-color", palette.menuSelectedItem);
    // document.body.style.setProperty("--accent-color", palette.accent);
    // document.body.style.setProperty("--button-color", palette.button);
    // document.body.style.setProperty("--tab-color", palette.tab);
    // document.body.style.setProperty("--notification-color", palette.notification);

    document.body.style.setProperty("--primary-color", palette.primary);
    document.body.style.setProperty("--secondary-color", palette.secondary);
    document.body.style.setProperty("--third-color", Session.get('defaultColorPalette').third || '#fdb813');
  })
});

Template.gestionnaire.events({
	'click': function(event, template){
		var $wrapper =  $('.helpContent.helpButton');
		$target  =  $(event.currentTarget);
		if ($(event.currentTarget).hasClass("menu-two")) {
			if ($(".helpContent.helpButton").css("display") == "none")
				$(".helpContent.helpButton").css({display: "flex"});
			else
				$(".helpContent.helpButton").css({display: "none"});
		}
		else if( $(event.currentTarget).hasClass("helpButton") == true && $wrapper[0] != undefined && $target[0] != undefined && !$.contains($wrapper[0], $target[0]) ) {
			$(".helpContent.helpButton").css({display: "none"});
		}
	},
	'click #mailHelp': function(event, template){
		event.stopPropagation();
	},
	'click #goCgu': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Session.set('backFromCgu', FlowRouter.current().path);
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},

	'click .menu-close-cross, click .btn-close-menu': function(event, template) {
		$(".helpContent.helpButton").css({display: "none"});
	},


	'click .tab': function(event, template) {
    const lang = FlowRouter.getParam("lang") || "fr";
    const condoId = localStorage.getItem('condoIdSelected')
		if (event.currentTarget.getAttribute("name") == "planning") {
      FlowRouter.go("app.gestionnaire.planningid", { lang, condo_id: condoId });
		}
    else if (event.currentTarget.getAttribute("name") == "forum") {
      FlowRouter.go("app.gestionnaire.forum.postId", { lang, condo_id: condoId });
    }
    else if (event.currentTarget.getAttribute("name") === "emergencyContact") {
      FlowRouter.go("app.gestionnaire.emergencyContact", { lang, condo_id: condoId });
    }
		else if (event.currentTarget.getAttribute("name") == "classifieds") {
      FlowRouter.go("app.gestionnaire.classifieds.condo", { lang, condo_id: condoId });
		}
		else if (event.currentTarget.getAttribute("name") == "contactManagement") {
      FlowRouter.go('app.gestionnaire.contactManagement.tab', { lang, condoId: condoId, tab: "messenger"});
		}
		else if (event.currentTarget.getAttribute("name") == "occupantList") {
			FlowRouter.go("app.gestionnaire.occupantList.tab", { lang, tab: 'active' });
		}
		else if (event.currentTarget.getAttribute("name") == "concierge") {
      FlowRouter.go('app.gestionnaire.concierge.condo', { lang, condoId: condoId });
    }
    else if (event.currentTarget.getAttribute("name") == "integrations") {
      FlowRouter.go('app.gestionnaire.integrations', { lang });
    }
    else if (event.currentTarget.getAttribute("name") == "messenger") {
      FlowRouter.go('app.gestionnaire.messenger', { lang, tab: 'enterprise', msgId: undefined });
    }
    else if (event.currentTarget.getAttribute("name") == "marketPlace") {
      FlowRouter.go('app.gestionnaire.marketPlace.tab', { lang, tab: 'services' });
    }
    else if (event.currentTarget.getAttribute("name") == "laundryRoom") {
      FlowRouter.go('app.gestionnaire.laundryRoom.tab', { lang, condoId: condoId, tab: 'empty' });
    }
		else {
			template.currentModule.set(event.currentTarget.getAttribute("name"));
			FlowRouter.go("app.gestionnaire." + event.currentTarget.getAttribute("name"), { lang });
		}
	},
	'click .notSubMenu': function(event, template) {
		$('#nav_side_menu li.highlight').removeClass('highlight');
	},
	'click .logOut': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Meteor.call('isAdmin', Meteor.userId(), function(error, result) {
			if (result == true) {
				FlowRouter.go('app.backoffice.manager');
			}
			else {
        const lang = (FlowRouter.getParam("lang") || "fr");
        $('.disconnecting').css('display', 'block')
        Session.set('target_url', null);
        Meteor.logout(() => {
          $('.disconnecting').css('display', 'none')
          FlowRouter.go('app.home', { lang });
        });
			}
		})

    const palette = Session.get('defaultColorPalette');
    // document.body.style.setProperty("--column-background-color", palette.columnBackground);
    // document.body.style.setProperty("--menu-selected-item-color", palette.menuSelectedItem);
    // document.body.style.setProperty("--accent-color", palette.accent);
    // document.body.style.setProperty("--button-color", palette.button);
    // document.body.style.setProperty("--tab-color", palette.tab);
    // document.body.style.setProperty("--notification-color", palette.notification);

    document.body.style.setProperty("--primary-color", palette.primary);
    document.body.style.setProperty("--secondary-color", palette.secondary);
    document.body.style.setProperty("--third-color", palette.third || '#fdb813');
	},
	'click .closeNav': function(event, template) {
		if ($(window).innerWidth() < 1025) {
			$("#nav_side_menu").toggleClass("appear");
			$("#mainContent").toggleClass("move");
		}
		else {
		}
	},
	'click .toggle': function(event, template) {
		var Event = event;
		setTimeout(function() {
			if ($(Event.target.parentElement).hasClass('collapsed') || $(Event.target.parentElement.parentElement).hasClass('collapsed')) {
				$($(Event.target).find('.arrow-down')[0]).addClass('arrow-right');
				$($(Event.target).find('.arrow-right')[0]).removeClass('arrow-down');
				if ($(Event.target).hasClass('arrow')){
					$(Event.target).addClass('arrow-right');
					$(Event.target).removeClass('arrow-down');
				}
			}
			else {
				$($(Event.target).find('.arrow-right')[0]).addClass('arrow-down');
				$($(Event.target).find('.arrow-down')[0]).removeClass('arrow-right');
				if ($(Event.target).hasClass('arrow')){
					$(Event.target).addClass('arrow-down');
					$(Event.target).removeClass('arrow-right');
				}
			}
		}, 100);
	},
	'click #goProfile': function(event, template) {
		const lang = FlowRouter.getParam("lang") || "fr";
		FlowRouter.go('app.gestionnaire.profile.tab', { lang, tab: 'preferences' });
	},
	'click .logoGoHome': function(event, template) {
		const lang = FlowRouter.getParam("lang") || "fr";
		FlowRouter.go('app.gestionnaire.buildingsview', { lang });
	},
	'click #langFr': (event, template) => {
    Meteor.users.update(Meteor.userId(), {$set: {"profile.lang": "fr"}});
		FlowRouter.setParams({ lang: 'fr' });
	},
	'click #langEn': (event, template) => {
    Meteor.users.update(Meteor.userId(), {$set: {"profile.lang": "en"}});
		FlowRouter.setParams({ lang: 'en' });
	}
});

Template.gestionnaire.helpers({
	selectedTab: () => {
		return (Template.instance().selectedTab.get());
	},
	firstLetters: () => {
		return Meteor.user() ? Meteor.user().profile.firstname[0].toUpperCase() + Meteor.user().profile.lastname[0].toUpperCase() : "";
	},
	user: () => {
		return (Meteor.user() && Meteor.user().profile) ? Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname : "";
	},
	currentModule: () => {
		let module_name = Template.instance().currentModule.get();
		Meteor.defer(function() {
			$("#menu-content li").removeClass("highlight");
			$("#menu-content li a").removeClass("highlight");

			if (module_name) {
				let element = $(`#menu-content li a[name='${module_name}']`);
				module_name == 'buildingsview' || module_name == 'stats' ? element.addClass('highlight') : element.parent().addClass('highlight');
			}
			else {
				$("#menu-content li a[name='buildingsview']").addClass('highlight');
			}
		});
		return Template.instance().currentModule.get() && Template.instance().currentModule != 'buildingsview';
	},
	getAccess: () => {
		let gestionnaireId = Meteor.user();
		if (gestionnaireId && gestionnaireId.identities) {
			let gestionnaire = Enterprises.findOne(gestionnaireId.identities.gestionnaireId);
			if (gestionnaire && gestionnaire.users.length) {
				gestionnaire = _.map(_.find(gestionnaire.users, function(user) {
					return user.userId == gestionnaireId._id;
				}).condosInCharge, function(elem) {
					return elem.access;
				});
				var totalLength = 0;
				for (access of gestionnaire) {
					totalLength += access.length;
				}
				return totalLength;
			}
		}
	},
	getUserId: () => {
		return Meteor.userId();
	},
	getName: () => {
		if (Meteor.user() && Meteor.user().profile)
			return Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname;
	},
	moduleVisible: (name) => {
		var totalModules = 0;
		for (modules of name) {
			let obj = "settings.options." + modules;
			let query = {};
			let queryTypo = {};
			query[obj] = true;
			totalModules += Condos.findOne(query) !== undefined ? 1 : 0;
		}
		return totalModules;
	},
	moduleVisibleAndAccess: (name) => {
		var total = 0;
		let obj = "settings.options." + name;
		let query = {};
		query[obj] = true;
		let condos = Condos.find(query).fetch();
		if (!condos.length) {
			return false;
		}
		_.each(condos, function(condo) {
			let gestionnaireId = Meteor.user();
			if (gestionnaireId && gestionnaireId.identities) {
				let gestionnaire = Enterprises.findOne(gestionnaireId.identities.gestionnaireId);
				if (gestionnaire && gestionnaire.users.length) {
					gestionnaire = _.find(_.find(gestionnaire.users, function(user) {
						return user.userId == gestionnaireId._id;
					}).condosInCharge, function(elem) {
						return elem.condoId == condo._id;
					});
					if (gestionnaire)
						total += gestionnaire.access.length;
				}
			}
		});
		return total;
  },
  integrationVisibleAndAccess: (name) => {
    var total = 0;
		let obj = "integrations." + name;
		let query = {};
    query[obj] = true;
    let condos = Condos.find(query).fetch();
		if (!condos.length) {
			return false;
		}
		_.each(condos, function(condo) {
			let gestionnaireId = Meteor.user();
			if (gestionnaireId && gestionnaireId.identities) {
				let gestionnaire = Enterprises.findOne(gestionnaireId.identities.gestionnaireId);
				if (gestionnaire && gestionnaire.users.length) {
					gestionnaire = _.find(_.find(gestionnaire.users, function(user) {
						return user.userId == gestionnaireId._id;
					}).condosInCharge, function(elem) {
						return elem.condoId == condo._id;
					});
					if (gestionnaire)
						total += gestionnaire.access.length;
				}
			}
		});
		return total;
  },
	gestResidences: () => {
		let rCondo = [];
		if (gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()})) {
			if (user = _.find(gestionnaire.users, (g) => {return g.userId == Meteor.userId()})) {
				_.each(user.condosInCharge, (condo) => {
					if (condo_ = Condos.findOne(condo.condoId))
						rCondo.push(condo_);
				});
				if (rCondo != [])
					return rCondo;
			}
		}
	},
	isUserLoaded: () => {
		let user = Meteor.user();
		if (user && user.identities) {
			return true;
		}
		return false;
	},
	isEnableAccount: () => {
		let user = Meteor.user();
		if (user && user.identities) {
			let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
			if (gestionnaire) {
				let gestionnaireUser = _.find(gestionnaire.users, function(elem) {
					return elem.userId == user._id;
				});
				if (!gestionnaireUser.condosInCharge.length)
					return false;
				return !gestionnaire.inactive;
			}
			return false;
		}
		return false;
	},
	showLogo: () => {
    const condoId = Template.instance().condoId.get()
    const condoLogos = CondoLogos.findOne({ condoId: condoId })

    if (condoLogos && condoLogos.avatar && condoLogos.avatar.original) {
      return condoLogos.avatar.original
    }
    return ''
	},
	isAdminConnected: () => {
		return Template.instance().isGestionnaireAsAdmin.get();
	},
	getBadge: (module) => {
    let condoId = Template.instance().condoId.get()
    let badges = null
    if (condoId && condoId !== 'all') {
      badges = BadgesGestionnaire.find({ _id: condoId }).fetch();
    } else {
      badges = BadgesGestionnaire.find().fetch();
    }
		if (!badges)
			return "";
		let nbBadges = 0;
		if (module === 'forum') {
			_.each(badges, function (badge) {
				nbBadges += parseInt(badge['forum']) + parseInt(badge['myforum']) || 0;
      });
		} else if (module === 'classifieds') {
			_.each(badges, function (badge) {
				nbBadges += (badge['ads'] ? parseInt(badge['ads']) : 0) + (badge['myads'] ? parseInt(badge['myads']) : 0);
			});
		} else if (module === 'documents') {
			_.each(badges, function (badge) {
				nbBadges += (badge['manual'] ? parseInt(badge['manual']) : 0) + (badge['map'] ? parseInt(badge['map']) : 0);
			});
		} else if (module === 'reservation') {
			_.each(badges, function (badge) {
				nbBadges += (badge['reservation_pending'] ? parseInt(badge['reservation_pending']) : 0) + (badge['reservation_validated'] ? parseInt(badge['reservation_validated']) : 0);
			});
		} else {
			_.each(badges, function(badge) {
				nbBadges += badge[module] || 0;
			});
		}
		return (nbBadges > 0) ? nbBadges : "";
	},
	isSubscribeReady: () => {
		return Template.instance().subscriptionsReady();
	},
	isReactModule: (templateName) => {
    if (reactModules.includes(templateName))
			return true
		else
			return false;
	},
	reactComponent: () => {
		return Forum;
	},
	getDicoLang: () => {
		return new ForumLang(FlowRouter.getParam('lang') || 'fr')
	},
	getSrcFR: () => {
		// const lang = FlowRouter.getParam('lang') || 'fr'
		// if (lang === 'fr') {
		// 	return '/img/icons/svg/flag-fr-sel.svg'
		// } else {
			return '/img/icons/svg/flag-fr-unsel.svg'
		// }
	},
	getSrcEN: () => {
		// const lang = FlowRouter.getParam('lang') || 'fr'
		// if (lang === 'en') {
		// 	return '/img/icons/svg/flag-en-sel.svg'
		// } else {
			return '/img/icons/svg/flag-en-unsel.svg'
		// }
  },
  MbCondoDropdown: () => MbCondoDropdown,
  condoIdCb: () => {
    const template = Template.instance()
    return (condoId) => {
      template.condoId.set(condoId)
    }
  },
  getCondoId: () => Template.instance().condoId.get(),
  getData: () => ({
    selectedCondoId: Template.instance().condoId.get()
  }),
  langSelected: (lang) => {
    return lang === (FlowRouter.getParam('lang') || 'fr')
  }
});
