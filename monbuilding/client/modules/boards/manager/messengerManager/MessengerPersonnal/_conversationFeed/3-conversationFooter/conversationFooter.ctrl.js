import { CommonTranslation, MessageInput, ModuleMessagerieIndex } from '/common/lang/lang.js'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'

let typingTimeout = null

Template.manager_conversationFooter.onCreated(function () {
  this.messages = new ReactiveVar('')
  this.files = new ReactiveVar([])
  this.sendNotif = new ReactiveVar(true)
  this.fm = new FileManager(MessengerFiles, { userId: Meteor.userId(), condoId: this.data.condoId, msgId: Template.currentData().currentConversation })

  let self = this
  this.autorun(function () {
    FlowRouter.watchPathChange()
    self.messages.set('')
  })
})

Template.manager_conversationFooter.onRendered(function () {

})

Template.manager_conversationFooter.helpers({
  isSameMessage: () => Template.instance().messageId === Template.currentData().currentConversation,
  setMessageIdAndResetMessageInput: () => {
    Template.instance().messageId = Template.currentData().currentConversation
    Template.instance().messages.set('')
    Template.instance().files.set([])
  },
  getUsersInformation: () => Template.currentData().usersInformation,
  getSelectedTab: () => Template.currentData().currentTab,
  getCurrentConversation: () => Template.currentData().currentConversation,
  getMessageIdForEditing: () => Template.currentData().messageIdForEditing,
  MbTextArea: () => MbTextArea,
  getMessageValue: () => Template.instance().messages.get(),
  setMessageValue: () => {
    let template = Template.instance()
    return (value) => template.messages.set(value)
  },
  disabledButton: () => !((Template.instance().messages.get() && Template.instance().messages.get() !== '') || (Template.instance().files.get() && Template.instance().files.get().length > 0)),
  getFiles: () => {
    const files = Template.instance().files.get()
    if (!!files) {
      files.forEach((file, index) => {
        if (typeof file === 'string') {
          files[index] = MessengerFiles.findOne({ _id: file })
        }
      })
    }
    return files
  },
  onFileAdded: () => {
    const template = Template.instance()
    let oldFiles = template.files.get()
    return (newFiles) => {
      template.files.set([
        ...oldFiles,
        ...newFiles
      ])
    }
  },
  onFileRemoved: () => {
    const template = Template.instance()
    return (fileId) => {
      template.files.set(template.files.get().filter(f => f.fileId ? f.fileId !== fileId : (f._id ? f._id !== fileId : f.id !== fileId)))
    }
  },
  refreshTextAndFile: (messageId) => {
    let message = MessagesFeed.findOne({ _id: messageId })
    if (!!message === true) {
      Template.instance().messages.set(message.text)
      Template.instance().files.set(message.files)
    }
  },
  checkManagerGlobalRoomExist: (event) => {
    const isNewMessage = Template.currentData().isNewMessage
    const userForNewConversation = Template.currentData().userForNewConversation
    const condoId = Template.currentData().condoId
    const selectConversationCb = Template.currentData().selectConversationCb
    const setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    const onSelectUser = Template.currentData().onSelectUser
    const conversationId = Template.currentData().currentConversation

    return (event) => {
      if (!!isNewMessage === true) {
        Meteor.call('checkManagerRoomExist', userForNewConversation, condoId, function (error, result) {
          if (!error && result) {
            selectConversationCb(result)
            setIsNewMessageAsFalse()
            onSelectUser([])
          }
        })
      }
      if (!!conversationId === true) {
        Meteor.call('updateLastVisit', conversationId)
      }
    }
  },
  validMessage: (event) => {
    const template = Template.instance()
    const conversationId = Template.currentData().currentConversation

    const messageIdForEditing = Template.currentData().messageIdForEditing
    const setMessageIdForEditing = Template.currentData().setMessageIdForEditing

    const isNewMessage = Template.currentData().isNewMessage
    const condoId = Template.currentData().condoId
    const setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    const userForNewConversation = Template.currentData().userForNewConversation
    const onSelectUser = Template.currentData().onSelectUser

    const selectConversationCb = Template.currentData().selectConversationCb

    const messages = Template.instance().messages.get().trim()
    const files = Template.instance().files.get()

    return (event) => {
      Meteor.call('userIsTyping', conversationId)
      if (typingTimeout !== null) {
        clearTimeout(typingTimeout)
      }
      typingTimeout = setTimeout(function () {
        Meteor.call('userIsNotTyping', conversationId)
      }, 3000)
      if (event.key === 'Enter' && !event.shiftKey) {
        event.preventDefault()
        template.messages.set('')
        template.files.set([])

        const lang = FlowRouter.getParam('lang') || 'fr'
        const translationModuleMessagerieIndex = new ModuleMessagerieIndex(lang)
        const translationCommonTranslation = new CommonTranslation(lang)

        if (files && files.length && !template.fm.validateFiles(files)) {
          sAlert.error(translationCommonTranslation.commonTranslation['VIDEO_Files_not_alowed'])
          return
        }

        // Editing a message :
        if (messageIdForEditing) {
          if ((!!messages === false || messages === '') && (!!files === false || files.length === 0)) {


            bootbox.confirm({
              size: 'medium',
              title: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message'],
              message: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message_explain'],
              buttons: {
                confirm: { label: translationCommonTranslation.commonTranslation['confirm'], className: 'btn-red-confirm' },
                cancel: { label: translationCommonTranslation.commonTranslation['cancel'], className: 'btn-outline-red-confirm' }
              },
              backdrop: true,
              callback: function (res) {
                if (res) {
                  let messageSave = MessagesFeed.findOne({ _id: messageIdForEditing })
                  Meteor.call('removeMessageFeed', messageIdForEditing, function (error, result) {
                    if (!error) {
                      let existMessageAfterDeletion = MessagesFeed.findOne()
                      if (!!existMessageAfterDeletion === false) {
                        Meteor.call('removeConversation', messageSave.messageId, function (error, result) {
                          if (!error) {
                            sAlert.success(translationModuleMessagerieIndex.moduleMessagerieIndex['conversation_deleted'])
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          } else {
            if (!!files) {
              template.fm.batchUpload(files)
                .then(results => {
                  let filesId = results.map(f => f._id)
                  Meteor.call('updateMessageFeed', messageIdForEditing, messages, filesId, function (error, result) {
                    if (!error) {
                      if (result === 0) {
                      }
                    } else {
                      template.messages.set(messages)
                    }
                  })
                })
                .catch(e => {
                  sAlert.error(e)
                })
            } else {
              Meteor.call('updateMessageFeed', messageIdForEditing, messages, [], function (error, result) {
                if (!error) {
                  if (result === 0) {
                  }
                } else {
                  template.messages.set(messages)
                }
              })
            }
          }
          setMessageIdForEditing(null)
          return
        }

        if (!messages && (!files || files.length === 0)) return

        Meteor.call('userIsNotTyping', conversationId)

        // Send a message in existing conversation :
        if (!!isNewMessage === false) {
          template.fm.batchUpload(files)
          .then(results => {
            let filesId = results.map(f => f._id)
            let conversation = Messages.findOne(conversationId)
            if (!!conversation === false) {
              template.messages.set(messages)
              template.files.set(files)
              sAlert.error('Error: Conversation not found.')
            }
            Meteor.call('addComment', conversationId, messages, filesId, function (error, result) {
              if (!error) {
              } else {
                template.messages.set(messages)
                template.files.set(files)
                sAlert.error(error)
              }
            })
          })
          .catch(e => {
            sAlert.error(e)
          })
        } else {
          // Send a message in new conversation :
          template.fm.batchUpload(files)
          .then(results => {
            let filesId = results.map(f => f._id)
            let data = {
              condoId,
              message: messages,
              files: filesId,
              users: userForNewConversation
            }
            userForNewConversation.push(Meteor.userId())
            Meteor.call('addMessageFromManagerMessengerGlobal', data, (error, result) => {
              if (!error && result) {
                selectConversationCb(result)
                setIsNewMessageAsFalse()
                onSelectUser([])
              } else {
                template.messages.set(messages)
                template.files.set(files)
                sAlert.error(error)
              }
            })
          })
          .catch(e => {
            sAlert.error(e)
          })
        }
      }
    }
  }
})

Template.manager_conversationFooter.events({
})
