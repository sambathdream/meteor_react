import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
* Created by vmariot on 30/04/18.
*/

Template.manager_messengerConversation.onCreated(function () {
  this.userForNewConversation = new ReactiveVar([])
  this.messageIdForEditing = new ReactiveVar(null)
})

Template.manager_messengerConversation.onRendered(function () {
})

Template.manager_messengerConversation.helpers({
  refreshSubscribe: () => {
    Template.instance().subscribe('isTyping', Template.currentData().currentConversation)
  },
  getCondoId: () => Template.currentData().condoId,
  getCurrentConversation: () => Template.currentData().currentConversation,
  selectConversationCb: () => Template.currentData().selectConversationCb,
  getIsNewMessage: () => Template.currentData().isNewMessage,
  setIsNewMessageAsFalse: () => Template.currentData().setIsNewMessageAsFalse,
  isEmptyState: (msgId) => msgId === 'MbEmptyMessage',
  getUsersInformation: () => {
    const condoId = Template.currentData().condoId
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    const currentTab = Template.currentData().currentTab
    const currentConversation = Template.currentData().currentConversation
    const conversation = Messages.findOne({ _id: currentConversation, condoId: condoId })
    if (!!conversation === true) {
      const userOfConversation = conversation.users.filter(u => u.userId !== Meteor.userId())
      const newUserOfConversation = []
      for (let index = 0; index < userOfConversation.length; index++) {
        const user = userOfConversation[index];
        if (!!newUserOfConversation.find(u => u._id === user.userId) === false) {
          let profile = UsersProfile.findOne({ _id: user.userId })
          if (!!profile === true) {
            let profilePicture = Avatars.findOne({ _id: profile._id })
            let usersRights = UsersRights.findOne({ userId: profile._id, condoId: condoId })
            newUserOfConversation.push({
              _id: user.userId,
              isCreator: !!user.creator,
              firstname: profile.firstname,
              lastname: profile.lastname,
              fullname: `${profile.firstname} ${profile.lastname}`,
              initial: `${profile.firstname[0]}${profile.lastname[0]}`,
              fullname_short: `${profile.firstname[0]}. ${profile.lastname}`,
              avatarURL: !!profilePicture === true ? profilePicture.avatar.avatar : null,
              roleId: profile.role === 'manager' ? 'manager' : !!usersRights === true ? usersRights.defaultRoleId : null
            })
          } else {
            let deactivatedAccount = translation.commonTranslation['deactivated_account']
            let firstname = deactivatedAccount.split(' ')[0]
            let lastname = deactivatedAccount.split(' ')[1]
            newUserOfConversation.push({
              _id: user.userId,
              isCreator: null,
              firstname: firstname,
              lastname: lastname,
              fullname: `${firstname} ${lastname}`,
              initial: '?',
              fullname_short: `${firstname} ${lastname}`,
              avatarURL: null,
              roleId: null
            })
          }
        }
      }
      return _.unique(newUserOfConversation)
    } else {
      return []
    }
  },
  onSelectUser: () => {
    let userForNewConversation = Template.instance().userForNewConversation
    return (newUser) => {
      if (!!newUser === true && newUser.length >= 0 && newUser.length !== userForNewConversation.get().length) {
        userForNewConversation.set(newUser)
      }
    }
  },
  getUserForNewConversation: () => Template.instance().userForNewConversation.get(),
  getMessageIdForEditing: () => Template.instance().messageIdForEditing.get(),
  setMessageIdForEditing: () => {
    const messageIdForEditing = Template.instance().messageIdForEditing
    return (messageId) => {
      messageIdForEditing.set(messageId)
    }
  },
  renderIsTyping: () => {
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    const isTypingList = IsTyping && IsTyping.find({ 'userId': { $ne: Meteor.userId() } })
    const length = isTypingList.count()
    let div = '<div class="conversation-body-footer">'
    div += '<div class="is-typing">'
    div += '<div class="is-typing-wrap">'
    if (length > 0) {
      div += '<div class="is-typing-label">'
      isTypingList.forEach((isTyping, index) => {
        if (index === (length - 1)) {
          div += `<p class="is-typing-item">${isTyping.firstname}</p>`
        } else {
          div += `<p class="is-typing-item">${isTyping.firstname}</p>, `
        }
      })
      div += length === 1 ? translation.commonTranslation['is_typing'] : translation.commonTranslation['are_typing']
      div += '</div>'
    }
    div += '</div>'
    div += '</div>'
    div += '</div>'
    return div
  }
})

Template.manager_messengerConversation.events({
})
