import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import { CommonTranslation } from "/common/lang/lang.js"

/**
* Created by vmariot on 30/04/18.
*/
const mimeType = (type) => {
  return type.split('/')[0]
}

Template.manager_conversationHeader.onCreated(function () {
  this.condoId = new ReactiveVar(this.data.condoId)
  this.userForNewConversation = new ReactiveVar([])
  this.oldConversationName = new ReactiveVar(null)
  this.fm = new FileManager(MessengerFiles, { condoId: this.condoId.get(), msgId: Template.currentData().currentConversation })

  let self = this
  $('body').on('click', function (e) {
    const currentConversationId = self.data.currentConversation
    if ($(e.target).hasClass("editConversationPicture") || $(e.target).hasClass("cancelDropdown")) {
      $('.dropdownConversationPicture').toggleClass('displayNone')
    } else if ($(e.target).hasClass("uploadPhoto")) {
      $('#inputAvatar').click();
      $('.dropdownConversationPicture').addClass('displayNone')
    } else if ($(e.target).hasClass("removePhoto")) {
      Meteor.call('setConversationPicture', currentConversationId, null)
    } else if ($(e.target).hasClass("conversation-header-item-pencil")) {
      $('.conversation-header-title').toggleClass('displayNone')
      self.oldConversationName.set($('.conversation-header-title-input').val())
      $('.conversation-header-title-input').focus()
    } else {
      $('.dropdownConversationPicture').addClass('displayNone')
    }
  })
})

Template.manager_conversationHeader.onRendered(function () {
})

Template.conversation_header.onDestroyed(function () {
  $('body').off('click')
})

Template.manager_conversationHeader.helpers({
  checkCondoId: () => {
    let oldCondoId = Template.instance().condoId.get()
    let newCondoId = Template.currentData().condoId
    return oldCondoId === newCondoId
  },
  setNewCondoId: () => Template.instance().condoId.set(Template.currentData().condoId),
  getUsersInformation: () => Template.currentData().usersInformation,
  getSelectedTab: () => Template.currentData().currentTab,
  getIsNewMessage: () => Template.currentData().isNewMessage,
  getCurrentConversation: () => Messages.findOne({ _id: Template.currentData().currentConversation }),
  getConversationInitial: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      if (conversation.picture) {
        let conversationFile = MessengerFiles.findOne({ _id: conversation.picture })
        if (!!conversationFile === true && conversationFile.isImage === true) {
          let file = conversationFile
          return { isPicture: !!file === true, src: file.link('avatar') }
        }
        return {}
      } else if (conversation.name && conversation.name !== '') {
        return { isPicture: false, initial: conversation.name[0] }
      } else {
        let firstUser = conversationUsers.find(user => (user || {}).isCreator)
        if (!firstUser) {
          firstUser = conversationUsers[0]
        }
        if (!!firstUser === true) {
          return { isPicture: !!firstUser.avatarURL === true, src: firstUser.avatarURL, initial: firstUser.initial }
        }
      }
    }
    return { isPicture: false, initial: '?' }
  },
  getConversationName: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      if (conversation.name && conversation.name !== '') {
        return conversation.name
      } else {
        let firstUser = conversationUsers.find(user => (user || {}).isCreator)
        if (!firstUser) {
          firstUser = conversationUsers[0]
        }
        if (!!firstUser === true) {
          return firstUser.fullname
        } else {
          // return !!firstUser === true ? firstUser[0].fullname : '' // === deactivated_account
        }
      }
    }
  },
  getUsersOfConv: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      let tmpConversationUsers = conversationUsers
      let users = []
      if (!(conversation.name && conversation.name !== '')) {
        let firstUser = tmpConversationUsers.find(user => (user || {}).isCreator)
        if (!firstUser) {
          firstUser = tmpConversationUsers[0]
        }
        let indexOf = tmpConversationUsers.indexOf(firstUser)
        if (indexOf !== -1) {
          delete tmpConversationUsers[indexOf] // It's the conversation name /!\
        }
      }
      users = tmpConversationUsers.map(u => u.fullname)
      return _.without(users, undefined, null)
    }
  },
  resetSelect: (target) => Template.instance().userForNewConversation.set([]),
  getUserForNewConversation: () => Template.instance().userForNewConversation.get(),
  onSelectUser: () => {
    let onSelectUser = Template.currentData().onSelectUser
    let userForNewConversation = Template.instance().userForNewConversation
    return (userInConv) => {
      userForNewConversation.set(userInConv)
      if (!!onSelectUser && typeof onSelectUser === 'function') {
        onSelectUser(userForNewConversation.get())
      }
    }
  },
  getUserInBuilding: () => {
    const condoId = Template.currentData().condoId
    let inConvUsers = [Meteor.userId()]
    
    let userInCondo = UsersRights.find({ condoId: condoId, userId: { $nin: inConvUsers } }, { fields: { userId: true, defaultRoleId: true } }).fetch()
    userInCondo = userInCondo.sort((a, b) => a.defaultRoleId.localeCompare(b.defaultRoleId))
    const thisCondo = Condos.findOne({ _id: condoId }, { fields: { settings: true } })
    let availableRole = ['manager']
    if (thisCondo && thisCondo.settings && thisCondo.settings.options && thisCondo.settings.options.messenger && thisCondo.settings.options.messengerGestionnaire) {
      availableRole.push('occupant')
    }

    let hasManager = false
    let hasOccupant = false
    let hasRightOccupant = false
    let hasRightManager = false

    inConvUsers = UsersRights.find({condoId: condoId, userId: {$in: inConvUsers}}, { fields: { userId: true, defaultRoleId: true, for: true } }).fetch();

    // Check if there is any manager in the convo
    hasManager = (inConvUsers.filter((el) => {
      let role = DefaultRoles.findOne({_id: el.defaultRoleId})
      let isManager = (role.for === 'manager')
      return isManager
    }).length > 0)

    // Check if there is any occupant in the convo
    hasOccupant = (inConvUsers.filter((el) => {
      let role = DefaultRoles.findOne({_id: el.defaultRoleId})
      let isOccupant = (role.for === 'occupant')
      return !isOccupant
    }).length > 0)

    // Check if everybody as the right to speak to occupant
    hasRightManager = (inConvUsers.filter((el) => {
      let convoCheck = false
      let isManager  = DefaultRoles.findOne({_id: el.defaultRoleId}).for === 'manager'
      if (isManager) {
        convoCheck = hasManager &&
          Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, el.userId) &&
          Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, el.userId)
      } else {
        convoCheck = ((hasManager && Meteor.userHasRight('messenger', 'writeToManager', condoId, el.userId)) || !hasManager) &&
        ((hasOccupant && Meteor.userHasRight('messenger', 'writeToResident', condoId, el.userId)) || !hasOccupant)
      }
      return (convoCheck)
    }).length === inConvUsers.length)

    // Check if everybody as the right to speak to occupant
    hasRightOccupant = (inConvUsers.filter((el) => {
      return Meteor.userHasRight('messenger', 'writeToResident', condoId, el)
    }).length === inConvUsers.length)
    userInCondo = userInCondo.filter((user) => {
      let role = DefaultRoles.findOne({_id: user.defaultRoleId})
      let isManager = (role.for === 'manager') // Checking if current user is Manager
      let isOccupant = (role.for === 'occupant') // Checking if current user is Occupant

      // Checking the current member of the conversation rights
      let convoCheck = false
      if (isManager) {
        convoCheck = hasManager &&
          Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, user.userId) &&
          Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, user.userId)
      } else {
        convoCheck = ((hasManager && Meteor.userHasRight('messenger', 'writeToManager', condoId, user.userId)) || !hasManager) &&
        ((hasOccupant && Meteor.userHasRight('messenger', 'writeToResident', condoId, user.userId)) || !hasOccupant)
      }

      // Checking the element in the select menu
      let selectCheck = ((isManager && hasRightManager) || hasRightManager) && 
          ((isOccupant && hasRightOccupant) || hasOccupant)
      return selectCheck && convoCheck
    })

    return _.without(userInCondo.map(user => {
      let userProfile = UsersProfile.findOne({ _id: user.userId, role: { $in: availableRole } })
      if (userProfile) {
        let role = DefaultRoles.findOne({ _id: user.defaultRoleId })
        // console.log(userProfile)
        return {
          id: user.userId,
          text: `${userProfile.firstname} ${userProfile.lastname}${role ? '_//_' + role.name : ''}`
        }
      }
    }), null, undefined)
  }
})

Template.manager_conversationHeader.events({
  // 'click': (event, template) => {
  //   const currentConversationId = template.data.currentConversation
  //   if ($(event.currentTarget).hasClass("editConversationPicture") || $(event.currentTarget).hasClass("cancelDropdown")) {
  //     $('.dropdownConversationPicture').toggleClass('displayNone')
  //   } else if ($(event.currentTarget).hasClass("uploadPhoto")) {
  //     $('#inputAvatar').click();
  //     $('.dropdownConversationPicture').addClass('displayNone')
  //   } else if ($(event.currentTarget).hasClass("removePhoto")){
  //     Meteor.call('setConversationPicture', currentConversationId, null)
  //   } else if ($(event.currentTarget).hasClass("conversation-header-item-pencil")) {
  //     $('.conversation-header-title').toggleClass('displayNone')
  //     template.oldConversationName.set($('.conversation-header-title-input').val())
  //     $('.conversation-header-title-input').focus()
  //   } else {
  //     $('.dropdownConversationPicture').addClass('displayNone')
  //   }
  // },
  'change #inputAvatar'(event, template) {
    const currentConversationId = template.data.currentConversation

    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (event.currentTarget.files && event.currentTarget.files.length === 1 && mimeType(event.currentTarget.files[0].type) === 'image') {
      template.fm.insert(event.currentTarget.files[0], function (err, file) {
        event.currentTarget.value = ''
        if (!err && file) {
          template.fm.clearFiles()
          Meteor.call('setConversationPicture', currentConversationId, file._id)
        }
      });
    } else {
      sAlert.error(tr_common.commonTranslation['image_only'])
    }
  },
  'keydown .conversation-header-title-input': function (event, template) {
    if (event.keyCode === 13) {
      event.preventDefault();
      const currentConversationId = template.data.currentConversation
      const oldConversationName = template.oldConversationName.get()
      const newConversationName = event.currentTarget.value

      if (oldConversationName !== newConversationName) {
        if (!!newConversationName === false) {
          Meteor.call('setConversationName', currentConversationId, null)
        } else {
          Meteor.call('setConversationName', currentConversationId, newConversationName)
        }
      }
      $('.conversation-header-title-input').blur()
    }
  }
})
