import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.MessengerPersonnalManagerContactList.onCreated(function () {
  this.searchText = new ReactiveVar('')
})

Template.MessengerPersonnalManagerContactList.onRendered(function () {
})

Template.MessengerPersonnalManagerContactList.helpers({
  onSearch: () => {
    let template = Template.instance()
    return (searchText) => {
      template.searchText.set(searchText)
    }
  },
  searchIsActive: () => Template.instance().searchText.get() !== '',
  getCurrentConversation: () => Template.currentData().currentConversation,
  getSelectConversationCb: () => Template.currentData().selectConversationCb,
  getContact: () => {
    let condoId = Template.currentData().condoId
    let conversation = Messages.find({
      target: 'manager',
      'users.userId': Meteor.userId(),
      condoId: condoId,
      global: true
    }, { sort: { lastMsg: -1 } }).fetch()
    let searchText = Template.instance().searchText.get()
    if (!!searchText === true) {
      let regexp = new RegExp(searchText, 'i');
      conversation = conversation.filter(item => {
        let founded = false
        for (let j = item.users.length - 1; j >= 0; j--) {
          if (item.users[j].userId !== Meteor.userId()) {
            let user = UsersProfile.findOne({ _id: item.users[j].userId })
            if (user) {
              if (regexp.test(`${user.firstname} ${user.lastname}`)) {
                founded = true
              }
            }
          }
        }
        return founded
      })
    }
    return conversation
  },
  condoId: () => {
    return Template.currentData().condoId
  }
})

Template.MessengerPersonnalManagerContactList.events({
})
