import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
* Created by vmariot on 27/04/18.
*/

Template.MessengerPersonnalManagerContactListItem.onCreated(function () {

})

Template.MessengerPersonnalManagerContactListItem.onRendered(function () {
})

Template.MessengerPersonnalManagerContactListItem.helpers({
  getContactCardInfo: () => Template.currentData().conversation,
  getCurrentConversation: () => Template.currentData().currentConversation,
  getConversationInitial: (conversation) => {
    if (!!conversation === true) {
      if (conversation.picture) {
        let conversationFile = MessengerFiles.findOne({ _id: conversation.picture })
        if (!!conversationFile === true && conversationFile.isImage === true) {
          let file = conversationFile
          return { isPicture: !!file === true, src: file.link('avatar') }
        }
        return { isPicture: false, initial: '?' }
      } else if (conversation.name && conversation.name !== '') {
        return { isPicture: false, initial: conversation.name[0] }
      } else {
        let user = conversation.users.find(user => {
          return user.userId !== Meteor.userId()
        })
        let profile = UsersProfile.findOne({ _id: (user || {}).userId })
        if (!!profile === true) {
          let profilePicture = Avatars.findOne({ _id: profile._id })
          if (profilePicture) {
            return { isPicture: true, src: profilePicture.avatar.avatar }
          } else {
            return { isPicture: false, initial: profile.firstname[0] + profile.lastname[0] }
          }
        }
      }
    }
    return { isPicture: false, initial: '?' }
  },
  getConversationName: (conversation) => {
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    if (conversation && conversation.name) {
      return conversation.name
    } else if (conversation) {
      let user = conversation.users
      const displayAllName = user.length < 4
      let title = user.reduce((title, current) => {
        if (current.userId !== Meteor.userId()) {
          let lastUser = UsersProfile.findOne({ _id: current.userId })
          let userName = ''
          if (!!lastUser === true) {
            if (!displayAllName) {
              userName = `${lastUser.firstname[0]}. ${lastUser.lastname}, `
            } else {
              userName = `${lastUser.firstname} ${lastUser.lastname}, `
            }
          } else {
            userName = `${translation.commonTranslation['deactivated_account']}, `
          }
          return title + userName
        } else {
          return title
        }
      }, '')
      title = title.slice(0, -2)

      return title
    }
    return translation.commonTranslation['deactivated_account']
  },
  getConversationLastMessage: (userId, msg) => {
    let lastUser = UsersProfile.findOne({ _id: userId })
    let lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new CommonTranslation(lang)
    let userName = translation.commonTranslation['deactivated_account']
    if (!!lastUser === true) {
      userName = `${lastUser.firstname} ${lastUser.lastname[0]}.`
    }
    if (msg && msg !== '') {
      return `${userName} : ${msg}`
    } else {
      return `${userName} : ${translation.commonTranslation['attached_file_available']}`
    }
  },
  conversationHasNewElement: (conversation) => {
    let user = conversation && conversation.users.find(user => user.userId === Meteor.userId())
    let lastConnect = user.lastVisit
    if (!lastConnect) {
      return true
    } else {
      return conversation.lastMsg > lastConnect
    }
  }
})

Template.MessengerPersonnalManagerContactListItem.events({
  'click .messenger-item': function (event, template) {
    let selectConversationCb = template.data.selectConversationCb
    let msgId = event.currentTarget.getAttribute('msgid')
    if (selectConversationCb && typeof selectConversationCb === 'function' && !!msgId) {
      Meteor.call('updateLastVisit', msgId)
      selectConversationCb(msgId)
    }
  }
})
