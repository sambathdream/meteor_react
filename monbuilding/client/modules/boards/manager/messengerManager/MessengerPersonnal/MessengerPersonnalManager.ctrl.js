import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.MessengerPersonnalManager.onCreated(function () {
  this.subscribe('messagerie')
  this.subscribe('usersProfile')
  this.currentTab = new ReactiveVar(null)
  this.currentConversation = new ReactiveVar(null)
})

Template.MessengerPersonnalManager.onRendered(function () {

})

Template.MessengerPersonnalManager.helpers({
  getCondoId: () => Template.currentData().condoId,
  checkTab: () => {
    let newCurrentConversation = null
    let condoId = Template.currentData().condoId
    let oldMsgId = FlowRouter.getParam('msgId')
    let lang = FlowRouter.getParam('lang')
    let isNewMessage = Template.currentData().isNewMessage

    // Search if selectMessage is good
    if (!!isNewMessage === false) {
      if (oldMsgId) {
        let messageExist = Messages.findOne({
          _id: oldMsgId,
          condoId: condoId,
          target: 'manager',
          global: true })
        if (!messageExist) {
          newCurrentConversation = (Messages.findOne({
            condoId: condoId,
            target: 'manager',
            'users.userId': Meteor.userId(),
            global: true
          },
          { sort: { lastMsg: -1 } }) || {})._id
        } else {
          newCurrentConversation = oldMsgId
        }
      } else {
        newCurrentConversation = (Messages.findOne({
          condoId: condoId,
          target: 'manager',
          'users.userId': Meteor.userId(),
          global: true
        },
        { sort: { lastMsg: -1 } }) || {})._id
      }
      // else view EmptyState
      if (!newCurrentConversation) {
        newCurrentConversation = 'MbEmptyMessage'
      }
    }

    if (newCurrentConversation !== null) {
      FlowRouter.setParams({ msgId: newCurrentConversation })
      Template.instance().currentConversation.set(newCurrentConversation)
    } else {
      FlowRouter.setParams({ msgId: undefined })
      Template.instance().currentConversation.set(null)
    }
  },
  currentConversation: () => Template.instance().currentConversation.get(),
  selectConversationCb: () => {
    let template = Template.instance()
    let setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    return (newCurrentConversation) => {
      if (!!setIsNewMessageAsFalse && typeof setIsNewMessageAsFalse === 'function') {
        setIsNewMessageAsFalse()
      }
      FlowRouter.setParams({ msgId: newCurrentConversation })
      template.currentConversation.set(newCurrentConversation)
    }
  },
  isSubscribeReady: () => Template.instance().subscriptionsReady(),
  isNewMessage: () => Template.currentData().isNewMessage,
  setIsNewMessageAsFalse: () => {
    let setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    if (!!setIsNewMessageAsFalse && typeof setIsNewMessageAsFalse === 'function') {
      return () => setIsNewMessageAsFalse()
    } else {
      return () => {}
    }
  }
})

Template.MessengerPersonnalManager.events({
})
