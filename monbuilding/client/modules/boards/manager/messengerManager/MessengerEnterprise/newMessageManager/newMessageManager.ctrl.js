import { FileManager } from '/client/components/fileManager/filemanager.js'
import { CommonTranslation } from '/common/lang/lang'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'

function checkInputs (condoId, destinataires, typeMessage, subject, message) {
  let isOk = true
  if (condoId === 'all') {
    $('.form-group').find('.buttonCondoView').addClass('errorModalInput')
    isOk = false
  } else {
    $('.form-group').find('.buttonCondoView').removeClass('errorModalInput')
  }
  if (!destinataires || destinataires.length === 0) {
    $('[name=destinataires]').next().addClass('errorModalInput')
    isOk = false
  } else {
    $('[name=destinataires]').next().removeClass('errorModalInput')
  }
  if (!typeMessage || typeMessage.length === 0) {
    $('[name=typeMessage]').next().addClass('errorModalInput')
    isOk = false
  } else {
    $('[name=typeMessage]').next().removeClass('errorModalInput')
  }
  if (!subject || subject.length === 0 || subject.length > 50) {
    $('[name=sujet]').addClass('errorModalInput')
    isOk = false
  } else {
    $('[name=sujet]').removeClass('errorModalInput')
  }
  if (!message || message.length === 0) {
    $('.info-msg-box').css('border', '1px solid red')
    isOk = false
  } else {
    $('.info-msg-box').css('border', '1px solid #ddd')
  }
  return isOk
}

Template.newMessageManager.onCreated(function () {
  this.directMessageId = new ReactiveVar(FlowRouter.getParam('wildcard'))
  this.fm = new FileManager(MessengerFiles)
  this.length = new ReactiveVar(0)
  this.condoId = new ReactiveVar('all')
  this.errorMessage = new ReactiveVar('')
  this.files = new ReactiveVar([])
  this.message = new ReactiveVar('')
})

Template.newMessageManager.onDestroyed(function() {
})

function formatState(state) {
  const splitted = state.text.split('_//_')
  if (!state.id || !splitted[1]) {
    return state.text
  }
  var $state = $(
    '<span>' + splitted[0] + '</span>'
  )
  return $state
}

function displaySelect (elem) {
  const splitted = elem.text.split('_//_')
  if (elem.disabled === true) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    return $('<span>' + splitted[0] + '<span class="infoSelect2"> (' + splitted[1] + ') (' + translate.commonTranslation.noAccessManagerMP + ')</span></span>')
  }
  var $state = $('<span>' + splitted[0] + '<span class="infoSelect2">(' + splitted[1] + ')</span></span>')
  return $state
}

Template.newMessageManager.onRendered(function() {
  const lang = FlowRouter.getParam('lang') || 'fr'
  const translate = new CommonTranslation(lang)
  $('#typeMessage').select2({
    minimumResultsForSearch: -1,
    placeholder: translate.commonTranslation.select_type_message,
  })
  $('#destinataires').select2({
    templateResult: displaySelect,
    templateSelection: formatState,
    placeholder: translate.commonTranslation.add_recipient,
    dropdownAutoWidth: 'true',
    width: 'resolve',
    escapeMarkup: function(markup) {
      return markup
    },
  })
  $('#residence').select2({
    minimumResultsForSearch: -1,
    placeholder: translate.commonTranslation.occupant,
  })
})

Template.newMessageManager.events({
  'show.bs.modal #modal_new_message' (event, template) {
    template.condoId.set(Session.get('currentCondo'))
    let modal = template.$(event.currentTarget)
    window.location.hash = '#modal_new_message'
    window.onhashchange = function() {
      if (location.hash !== '#modal_new_message'){
        modal.modal('hide')
      }
    }
    template.length.set(0)
  },
  'hidden.bs.modal #modal_new_message' (event, template) {
    history.replaceState('', document.title, window.location.pathname)
    template.condoId.set('all')
    $('.errorModalInput').removeClass('errorModalInput')
    $('.select2-selection__rendered').empty()
    template.length.set(0)
    template.message.set('')
    template.files.set([])
    emptyFormByParentId('NewMessageForm')
  },
  'input #sujet' (event, template) {
    if ($('#sujet').val().length > 50) {
      event.preventDefault()
      $('#sujet').val($('#sujet').val().substring(0, 50))
    }
    else
    template.length.set($('#sujet').val().length)
  },
  'click .dropdown-condo' (e, t) {
    t.fm.setCustomFields({ userId: Meteor.userId(), condoId: e.currentTarget.getAttribute('data-id'), tmp: true })
    t.condoId.set(e.currentTarget.getAttribute('data-id'))
  },
  'click #submitMessage' (event, template) {
    event.preventDefault()

    const self = $(event.currentTarget)
    self.button('loading')
    self.prop('disabled', true)

    const condoIdTemplate = template.condoId.get()
    let destinataires = $('[name=destinataires]').val()
    let typeMessage = $('[name=typeMessage]').val()
    let subject = $('[name=sujet]').val()
    const message = template.message.get()
    const _files = template.files.get()

    let lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new CommonTranslation(lang)

    if (checkInputs(condoIdTemplate, destinataires, typeMessage, subject, message)) {
      destinataires.push(Meteor.userId())

      template.fm.batchUpload(_files)
      .then(files => {
        let fileIds = files.map(f => f._id)
        let data = {
          destinataires: destinataires,
          typeMessage: typeMessage,
          message: message,
          files: fileIds,
          details: subject,
          condoId: condoIdTemplate
        }

        Meteor.call('addMessageAsGestionnaire', data, function (error) {
          self.button('reset')
          self.prop('disabled', true)
          if (!error) {
            sAlert.success(translation.commonTranslation['message_sent'])
            $('#modal_new_message').modal('hide')
          }
          else {
            sAlert.error(error)
          }
        })
      })
      .catch(err => {
        self.button('reset')
        self.prop('disabled', false)

        sAlert.error(err)
      })
    }
    else {
      self.button('reset')
      self.prop('disabled', true)
      template.errorMessage.set(translation.commonTranslation['fill_in_all_fields'])
    }
  }
})

Template.newMessageManager.helpers({
  errorMessage: () => {
    return Template.instance().errorMessage.get()
  },
  getResidents: () => {
    const condoId = Template.instance().condoId.get()
    if (condoId) {
      let userInCondo = UsersRights.find({ condoId: condoId, userId: { $ne: Meteor.userId() } }, { fields: { userId: true, defaultRoleId: true } }).fetch()
      const thisCondo = Condos.findOne({ _id: condoId }, { fields: { settings: true } })
      let availableRole = ['manager']
      if (thisCondo && thisCondo.settings && thisCondo.settings.options && thisCondo.settings.options.messenger && thisCondo.settings.options.messengerGestionnaire) {
        availableRole.push('occupant')
      }
      return _.without(userInCondo.map(user => {
        let userProfile = UsersProfile.findOne({ _id: user.userId, role: { $in: availableRole } }, { fields: { firstname: true, lastname: true, role: true } })
        if (!!userProfile) {
          let disabled = false
          if (userProfile.role === 'occupant') {
            disabled = !Meteor.userHasRight("messenger", "writeToManager", condoId, user.userId)
            const thisUser = GestionnaireUsers.findOne({ _id: user.userId, 'resident.condos.condoId': condoId }, { fields: { _id: true, resident: true } })
            if (!thisUser) {
              return null
            } else {
              const thisUserCondo = thisUser.resident[0].condos.find((condo) => condo.condoId === condoId)
              if (!thisUserCondo || !thisUserCondo.preferences || !thisUserCondo.preferences.messagerie) {
                return null
              }
            }
          }
          let role = DefaultRoles.findOne({ _id: user.defaultRoleId }, { fields: { name: true } })
          return {
            id: user.userId,
            text: `${userProfile.firstname} ${userProfile.lastname}${!!role ? '_//_' + role.name : ''}`,
            disabled
          }
        }
      }), null, undefined)
      // return (GestionnaireUsers.find({'resident.condos.condoId': Template.instance().condoId.get()}).fetch())
    }
  },
  nbChar: () => {
    return (50 - Template.instance().length.get())
  },
  getCondoId: () => {
    return Template.instance().condoId.get()
  },
  CDD_cursor: () => {
    let condoIds = []
    let condoIdSee = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
    condoIds = condoIdSee.filter((condoId) => { return Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, Meteor.userId()) })
    return condoIds
  },
  onCondoChange: () => {
    const templ = Template.instance()

    return (condoId) => {
      if (condoId) {
        templ.condoId.set(condoId)
      }
    }
  },
  MbTextArea: () => MbTextArea,
  onFileAdded: () => {
    const tpl = Template.instance()

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  previewFiles: () => {
    return Template.instance().files.get()
  },
  getMessageValue: () => Template.instance().message.get(),
  onMessageChange: () => {
    const t = Template.instance()

    return (message) => t.message.set(message)
  }
})
