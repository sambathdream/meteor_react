import { Template } from 'meteor/templating'
import { ModuleMessagerieIndex, CommonTranslation } from '/common/lang/lang.js'
let timeout = null;

Template.messageListManager.onCreated(function() {
  this.userSearch = new ReactiveVar('')
})

Template.messageListManager.onRendered(function() {
})

Template.messageListManager.onDestroyed(function() {
})

Template.messageListManager.events({
  'click .msgLine' (event, tempate) {
    let msgId = event.currentTarget.getAttribute('msgId')
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.messenger.detail', { lang, tab: 'enterprise', msgId })
  },
  'click .archiveButton' (event, template) {
    event.stopPropagation()
    let tooltipId = event.currentTarget.getAttribute('aria-describeby')
    $('.' + 'tooltip').remove()
    const conversationId = template.$(event.currentTarget).attr('idx')
    Meteor.call('archiveConversation', conversationId)
  },
  'input #searchUser' (event, template) {
    template.userSearch.set(event.currentTarget.value)
  },
  'scroll .messenger-manager-list' (event, template) {
    const currentScrol = template.$(event.currentTarget).scrollTop()
    const chatWidth = template.$(event.currentTarget).prop('scrollHeight') - template.$('.messenger-manager-list').height()
    const messageCountLimit = Template.currentData().messageCountLimit
    const setMessageCountLimit = Template.currentData().setMessageCountLimit

    if (currentScrol === chatWidth) {
      if (messageCountLimit < 200) {
        setMessageCountLimit(messageCountLimit + 25)
      } else if (messageCountLimit < 500) {
        setMessageCountLimit(messageCountLimit + 50)
      } else {
        setMessageCountLimit(messageCountLimit + 75)
      }
    }
  }
})

Template.messageListManager.helpers({
  onSearch: () => {
    const template = Template.instance()
    return (val) => template.userSearch.set(val)
  },
  isSearchActive: () => Template.instance().userSearch.get() !== '',
  getMessages: () => {
    const condoId = Template.currentData().condoId
    const search = Template.instance().userSearch.get()
    const messageCountLimit = Template.currentData().messageCountLimit
    const setMessageCountLimit = Template.currentData().setMessageCountLimit

    let condoIds = []

    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
    } else if (Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, Meteor.userId())) {
      condoIds = [condoId]
    } else {
      return []
    }

    let messages = Messages.find({
      $and: [
        { target: 'manager' },
        {
          $or: [
            { incidentId: { $exists: true } },
            {
              $and: [
                { type: { $exists: true } },
                { details: { $exists: true } },
              ]
            }
          ]
        },
        { condoId: { $in: condoIds } }
      ]
    }, { limit: messageCountLimit, sort: { lastMsg: -1 } }).fetch() || []

    if (!!messages === false) {
      return []
    }

    // console.log('=====================================')
    messages = messages.map(msg => {
      let userIds = _.unique(msg.users.map(u => u.userId))
      // console.log('-----------------------------------')
      // console.log(msg.isArchived, msg)
      return ({
        _id: msg._id,
        isArchived: !!msg.isArchived,
        isNew: !msg.managerReplied,
        isUrgent: _isUrgentMessage(msg.urgent),
        date: moment(msg.lastMsg).format('DD/MM/YYYY'),
        occupantUser: _getOccupant(userIds, msg.condoId),
        condoName: (Condos.findOne({ _id: msg.condoId }) || { getNameWithId: () => '-' }).getNameWithId(),
        type: msg.type,
        details: msg.details,
        managerUser: _getManager(userIds, msg.condoId)
      })
    })
    // console.log('=====================================')

    if (search === '') {
      return messages
    }

    let regexp = new RegExp(search, 'i')
    messages = messages.filter((msg) => {
      let founded = false
      for (const user of msg.occupantUser) {
        if (regexp.test(user)) {
          founded = true
        }
      }
      if (founded === false) {
        for (const user of msg.managerUser) {
          if (regexp.test(user.fullName)) {
            founded = true
          }
        }
      }
      return founded
    })
    if (messages.length < 20 && messageCountLimit < 50) {
      setMessageCountLimit(messageCountLimit + 10)
    }
    return messages
  },
  activeTooltip: () => {
    if (timeout != null) {
      clearTimeout(timeout)
    }
    timeout = setTimeout(function () {
      $('[data-toggle="tooltip"]').tooltip({
        template: '<div class="tooltip messenger_tooltip_manager"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: 'hover'
      })
    }, 50)
  },
  condoId: () => {
    return Template.currentData().condoId
  }
})

function _getOccupant (userIds, condoId) {
  const lang = FlowRouter.getParam('lang') || 'fr'
  let translation = new CommonTranslation(lang)
  if (!!userIds === true && Array.isArray(userIds) && userIds.length > 0) {
    let ret = _.without(userIds.map(userId => {
      const userProfile = UsersProfile.findOne({ _id: userId })
      if (!!userProfile === true) {
        const defaultRoleId = (UsersRights.findOne({ condoId: condoId, userId: userId }) || {}).defaultRoleId
        const isOccupant = DefaultRoles.findOne({ _id: defaultRoleId, for: 'occupant' })
        if (!!isOccupant === true) {
          return `${userProfile.firstname} ${userProfile.lastname}`
        } else {
          return null
        }
      } else {
        return translation.commonTranslation['account_deactivated']
      }
    }), null, undefined)
    if (ret.length > 2) {
      let length = ret.length - 2
      translation = new ModuleMessagerieIndex(lang)
      return [...ret.splice(0, 2), `${translation.moduleMessagerieIndex['AND_x_more']} ${length} ${translation.moduleMessagerieIndex['and_x_MORE']}`]
    } else {
      return ret
    }
  } else {
    return []
  }
}

function _getManager (userIds, condoId) {
  let lang = FlowRouter.getParam('lang') || 'fr'
  const translation = new CommonTranslation(lang)
  if (!!userIds === true && Array.isArray(userIds) && userIds.length > 0) {
    return _.without(userIds.map(userId => {
      const userProfile = UsersProfile.findOne({ _id: userId })
      if (!!userProfile === true) {
        const defaultRoleId = (UsersRights.findOne({ condoId: condoId, userId: userId }) || {}).defaultRoleId
        const isManager = DefaultRoles.findOne({ _id: defaultRoleId, for: 'manager' })
        if (!!isManager === true) {
          return {
            fullName: `${userProfile.firstname} ${userProfile.lastname}`,
            initial: `${userProfile.firstname[0]}${userProfile.lastname[0]}`,
            avatar: _avatarLink(userId)
          }
        } else {
          return null
        }
      }
    }), null, undefined)
  } else {
    return []
  }
}

function _isUrgentMessage (priorityId) {
  let priority = IncidentPriority.findOne({ _id: priorityId })
  return !!priority === true && !!priority.defaultPriority === true && priority.defaultPriority === 'urgent'
}

function _avatarLink (userId) {
  let file = Avatars.findOne({ userId: userId })
  if (file) {
    return file.avatar.avatar
  }
}
