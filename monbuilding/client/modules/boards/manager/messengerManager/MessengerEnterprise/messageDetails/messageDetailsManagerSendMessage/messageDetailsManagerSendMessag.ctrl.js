import { CommonTranslation, MessageInput, ModuleMessagerieIndex } from '/common/lang/lang.js'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'

let typingTimeout = null

Template.messageDetailsManagerSendMessage.onCreated(function () {
  this.messages = new ReactiveVar('')
  this.files = new ReactiveVar([])
  const conversationId = this.data.currentConversation
  const conversation = Messages.findOne({ _id: conversationId })
  this.fm = new FileManager(MessengerFiles, { userId: Meteor.userId(), condoId: conversation.condoId, msgId: conversationId })
})

Template.messageDetailsManagerSendMessage.onRendered(function () {
})

Template.messageDetailsManagerSendMessage.helpers({
  getCurrentConversation: () => Template.currentData().currentConversation,
  getMessageIdForEditing: () => Template.currentData().messageIdForEditing,
  MbTextArea: () => MbTextArea,
  getMessageValue: () => Template.instance().messages.get(),
  setMessageValue: () => {
    let template = Template.instance()
    return (value) => template.messages.set(value)
  },
  disabledButton: () => !((Template.instance().messages.get() && Template.instance().messages.get() !== '') || (Template.instance().files.get() && Template.instance().files.get().length > 0)),
  getFiles: () => {
    const files = Template.instance().files.get()
    if (!!files) {
      files.forEach((file, index) => {
        if (typeof file === 'string') {
          files[index] = MessengerFiles.findOne({ _id: file })
        }
      })
    }
    return files
  },
  onFileAdded: () => {
    const template = Template.instance()
    let oldFiles = template.files.get()
    return (newFiles) => {
      template.files.set([
        ...oldFiles,
        ...newFiles
      ])
    }
  },
  onFileRemoved: () => {
    const template = Template.instance()

    return (fileId) => {
      template.files.set(template.files.get().filter(f => f.fileId ? f.fileId !== fileId : (f._id ? f._id !== fileId : f.id !== fileId)))
    }
  },
  refreshTextAndFile: (messageId) => {
    let message = MessagesFeed.findOne({ _id: messageId })
    if (!!message === true) {
      Template.instance().messages.set(message.text)
      Template.instance().files.set(message.files)
    }
  },
  validMessage: (event) => {
    const template = Template.instance()
    const conversationId = Template.currentData().currentConversation

    const messageIdForEditing = Template.currentData().messageIdForEditing
    const setMessageIdForEditing = Template.currentData().setMessageIdForEditing

    const messages = Template.instance().messages.get().trim()
    const files = Template.instance().files.get()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translationModuleMessagerieIndex = new ModuleMessagerieIndex(lang)
    const translationCommonTranslation = new CommonTranslation(lang)
    return (event) => {
      Meteor.call('userIsTyping', conversationId)
      if (typingTimeout !== null) {
        clearTimeout(typingTimeout)
      }
      typingTimeout = setTimeout(function () {
        Meteor.call('userIsNotTyping', conversationId)
      }, 3000)
      if (event.key === 'Enter' && !event.shiftKey) {
        event.preventDefault()

        if (files && files.length && !template.fm.validateFiles(files)) {
          sAlert.error(translationCommonTranslation.commonTranslation['VIDEO_Files_not_alowed'])
          return
        }
        template.messages.set('')
        template.files.set([])


        // Editing a message :
        if (!!messageIdForEditing === true) {
          if ((!!messages === false || messages === '') && (!!files === false || files.length === 0)) {


            bootbox.confirm({
              size: 'medium',
              title: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message'],
              message: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message_explain'],
              buttons: {
                confirm: { label: translationCommonTranslation.commonTranslation['confirm'], className: 'btn-red-confirm' },
                cancel: { label: translationCommonTranslation.commonTranslation['cancel'], className: 'btn-outline-red-confirm' }
              },
              backdrop: true,
              callback: function (res) {
                if (res) {
                  let messageSave = MessagesFeed.findOne({ _id: messageIdForEditing })
                  Meteor.call('removeMessageFeed', messageIdForEditing, function (error, result) {
                    if (!error) {
                      let existMessageAfterDeletion = MessagesFeed.findOne()
                      if (!!existMessageAfterDeletion === false) {
                        Meteor.call('removeConversation', messageSave.messageId, function (error, result) {
                          if (!error) {
                            sAlert.success(translationModuleMessagerieIndex.moduleMessagerieIndex['conversation_deleted'])
                          }
                        })
                      }
                    }
                  })
                }
              }
            })
          } else {
            if (!!files) {
              template.fm.batchUpload(files)
                .then(results => {
                  let filesId = results.map(f => f._id)
                  Meteor.call('updateMessageFeed', messageIdForEditing, messages, filesId, function (error, result) {
                    if (!error) {
                      if (result === 0) {
                      } else {
                        template.messages.set(messages)
                      }
                    }
                  })
                })
                .catch(err => {

                })
            } else {
              Meteor.call('updateMessageFeed', messageIdForEditing, messages, [], function (error, result) {
                if (!error) {
                  if (result === 0) {
                  } else {
                    template.messages.set(messages)
                  }
                }
              })
            }
          }
          setMessageIdForEditing(null)
          return
        }

        if (!messages && (!files || files.length === 0)) return

        Meteor.call('userIsNotTyping', conversationId)

        // Send a message in existing conversation :
        template.fm.batchUpload(files)
        .then(results => {
          let filesId = results.map(f => f._id)
          let conversation = Messages.findOne(conversationId)
          if (!!conversation === false) {
            template.messages.set(messages)
            template.files.set(files)
            sAlert.error('Error: Conversation not found.')
          }
          if (!!conversation.incidentId === true) {
            Meteor.call('replyAtIncidentMessage', conversation.incidentId)
          }
          Meteor.call('addComment', conversationId, messages, filesId, false, function (error) {
            if (!error) {
            } else {
              template.messages.set(messages)
              template.files.set(files)
              sAlert.error(error)
            }
          })
        })
        .catch(err => {
          sAlert.error(translationCommonTranslation.commonTranslation[err.reason])
        })

        const chatBody = $('.conversation-body')
        if (chatBody && chatBody[0]) {
          chatBody.stop().animate({
            scrollTop: chatBody[0].scrollHeight
          }, 800)
        }
      }
    }
  }
})

Template.messageDetailsManagerSendMessage.events({
})
