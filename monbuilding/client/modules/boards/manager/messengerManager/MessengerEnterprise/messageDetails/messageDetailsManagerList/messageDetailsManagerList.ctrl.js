import { ModuleMessagerieIndex } from '/common/lang/lang.js'
import { ReactiveVar } from 'meteor/reactive-var'
import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
* Created by vmariot on 30/04/18.
*/

Template.messageDetailsManagerList.onCreated(function () {
  this.elemToDisplay = new ReactiveVar(20)
  this.conversationId = null
  this.conversation = []
})

Template.messageDetailsManagerList.onRendered(function () {
})

// Warning:
//   For Messenger, I use an array in 3D :
//     messengerArrray = [GroupOfDay][GroupOfUser][GroupOfMessage]

Template.messageDetailsManagerList.helpers({
  updateData: () => {
    Template.instance().conversationId = Template.currentData().currentConversation
    Template.instance().subscribe('messengerFeed', Template.instance().conversationId, Template.instance().elemToDisplay.get())
    Template.instance().subscribe('messagerieFiles', Template.instance().conversationId)
  },
  getUsersInformation: () => Template.currentData().usersInformation,
  getCurrentConversation: () => Template.currentData().currentConversation,
  isSubscribeReady: () => Template.instance().subscriptionsReady(),
  getMessageFeed: () => {
    let msgId = Template.instance().conversationId
    let limit = Template.instance().elemToDisplay.get()
    if (!!msgId === true) {
      let messages = MessagesFeed.find({
        messageId: msgId
      }, { limit: limit, sort: { date: -1 } }).fetch()
      let conversation = []
      conversation[0] = []
      conversation[0][0] = []
      let oldUserId = null
      let oldDate = null
      let indexOfDay = 0
      let indexOfUser = 0
      for (let index = 0; index < messages.length; index++) {
        const element = messages[index]
        let userId = element.userId
        if (!(element.isNewUserMessage && !UsersProfile.findOne({ _id: userId }))) {
          if (!!element && !!element.isNewUserMessage === true) {
            userId = 'newUser-' + userId
          }
          const date = element.date
          if (oldDate === null) oldDate = date
          if (moment(oldDate).format('DD/MM/YYYY') === moment(date).format('DD/MM/YYYY')) {
            if (oldUserId === null) {
              conversation[indexOfDay][indexOfUser] = [element]
              oldUserId = userId
            } else if (oldUserId === userId) {
              conversation[indexOfDay][indexOfUser].push(element)
              oldUserId = userId
            } else {
              indexOfUser++
              conversation[indexOfDay][indexOfUser] = [element]
              oldUserId = userId
            }
            oldDate = date
          } else if (oldDate !== date) {
            indexOfDay++
            indexOfUser = 0
            conversation[indexOfDay] = []
            conversation[indexOfDay][indexOfUser] = [element]
            oldUserId = userId
            oldDate = date
          }
        }
      }
      return conversation
    }
  },
  showDate: (elementsOfDay) => {
    if (!!elementsOfDay === true && Array.isArray(elementsOfDay) && !!elementsOfDay[0] === true && !!elementsOfDay[0][0] === true) {
      let date = elementsOfDay[0][0].date
      let lang = FlowRouter.getParam('lang') || 'fr'
      let translate = new ModuleMessagerieIndex(lang)
      if (moment(date).format('L') === moment().locale('fr').format('L')) {
        return translate.moduleMessagerieIndex['today']
      } else if (moment().diff(moment(date), 'days') === 1) {
        return translate.moduleMessagerieIndex['yesterday']
      } else {
        return moment(date).locale(lang).format('LL')
      }
    }
  },
  scrollBottom: () => {
    setTimeout(() => {
      const chatBody = $('.conversation-body')
      if (chatBody && chatBody[0]) {
        chatBody.stop().animate({
          scrollTop: chatBody[0].scrollHeight
        }, 800)
      }
    }, 2000)
  },
  getSetMessageIdForEditing: () => Template.currentData().setMessageIdForEditing,
  getUsersStatus: () => {
    const userConnect = Template.instance().userConnect
    const conversationId = Template.currentData().currentConversation
    if (!conversationId) return
    Meteor.call('getUserStatusInConversation', conversationId, (err, res) => {
      if (!err) {
        Session.set('usersStatus', res)
      }
    })
    return Session.get('usersStatus')
  },
})

Template.messageDetailsManagerList.events({
  'scroll .conversation-body': function (event, template) {
    if (template.$(event.currentTarget).scrollTop() === 0) {
      if (template.elemToDisplay.get() < 200) {
        template.elemToDisplay.set(template.elemToDisplay.get() + 25)
      } else if (template.elemToDisplay.get() < 500) {
        template.elemToDisplay.set(template.elemToDisplay.get() + 50)
      } else {
        template.elemToDisplay.set(template.elemToDisplay.get() + 75)
      }
    }
  }
})
