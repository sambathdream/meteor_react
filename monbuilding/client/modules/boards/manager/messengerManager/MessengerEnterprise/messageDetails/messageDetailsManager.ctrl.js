import { FileManager } from '/client/components/fileManager/filemanager.js'
import { ModuleMessagerieIndex, ForumLang, CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.messageDetailsManager.onCreated(function () {
  const msgId = FlowRouter.getParam('msgId')
  const conversation = Messages.findOne({ _id: msgId })
  this.elemToDisplay = new ReactiveVar(20)
  this.messageIdForEditing = new ReactiveVar(null)
  this.isOpen = new ReactiveVar(false)

  this.incidentId = conversation.incidentId
  this.conversationId = msgId
  this.conversation = new ReactiveVar(conversation)
  this.invitedUserIds = []

  this.fm = new FileManager(MessengerFiles, { msgId: msgId, userId: Meteor.userId(), condoId: conversation.condoId, tmp: true })

  Meteor.call('updateLastVisit', msgId)
  this.subscribe('messagerieGestionnaireFiles')
  this.subscribe('messengerFeed', msgId, this.elemToDisplay.get())
  this.subscribe('isTyping', msgId)
  if (this.incidentId) {
    this.subscribe('messengerManagerIncidentFiles', this.incidentId)
  }
})

Template.messageDetailsManager.onDestroyed(function () {
})

Template.messageDetailsManager.onRendered(function () {
  $('select[name="ajoutParticipants"]').select2()
})

Template.messageDetailsManager.events({
  'hidden.bs.modal #modal_add_participants': function (event, template) {
    template.isOpen.set(false);
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_add_participants': function (event, template) {
    template.isOpen.set(true);
    let modal = $(event.currentTarget);
    template.invitedUserIds = []
    window.location.hash = "#modal_add_participants";
    window.onhashchange = function () {
      if (location.hash != "#modal_add_participants") {
        modal.modal('hide');
      }
    }
  },
  'click #backbutton': function (event, template) {
    event.preventDefault()
    const lang = FlowRouter.getParam('lang') || 'fr'
    if (Session.get('lastRoute') && Session.get('lastRoute').name === 'app.gestionnaire.incidents.id' && template.incidentId) {
      FlowRouter.go('app.gestionnaire.incidents.id', { lang, incidentId: template.incidentId })
    } else {
      FlowRouter.go('app.gestionnaire.messenger', { lang, tab: 'enterprise' })
    }
  },
  'click #validInvite': function (event, template) {
    event.preventDefault()
    $('#validInvite').button('loading')
    const participants = template.invitedUserIds
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new CommonTranslation(lang)
    if (participants && participants.length > 0) {
      Meteor.call('inviteUserToConversation', template.conversationId, participants, function (error, result) {
        $('#validInvite').button('reset')
        if (!!error === false) {
          sAlert.success(translation.commonTranslation['invitation_success'])
          template.conversation.set(Messages.findOne({ _id: template.conversationId }))
          $('#modal_add_participants').modal('hide')
        }
      })
    }
  },
})

Template.messageDetailsManager.helpers({
  getConversationId: () => Template.instance().conversationId,
  getConversation: () => Template.instance().conversation.get(),
  getStatus: (id) => {
    const users = Session.get('usersStatus')
    if (!!users) {
      const user = users.find((u) => u._id === id)
      return !!user && user.status
    }
  },
  getUsersInConverrsation: () => {
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    const conversation = Template.instance().conversation.get()
    if (!!conversation === true) {
      const condoId = conversation.condoId
      const userOfConversation = conversation.users
      const userWithGoodFormat = []
      for (let index = 0; index < userOfConversation.length; index++) {
        const user = userOfConversation[index]
        if (!!userWithGoodFormat.find(u => u._id === user.userId) === false) {
          let profile = UsersProfile.findOne({ _id: user.userId })
          if (!!profile === true) {
            let profilePicture = Avatars.findOne({ _id: profile._id })
            let usersRights = UsersRights.findOne({ userId: profile._id, condoId: condoId })
            let disabled;
            if (profile.role === 'manager') {
              disabled = !Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, user.userId) &&
              !Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, user.userId)
            } else {
              disabled = !Meteor.userHasRight("messenger", "writeToManager", condoId, user.userId)
            }
            userWithGoodFormat.push({
              _id: user.userId,
              isCreator: !!user.creator,
              firstname: profile.firstname,
              lastname: profile.lastname,
              fullname: `${profile.firstname} ${profile.lastname}`,
              initial: `${profile.firstname[0]}${profile.lastname[0]}`,
              phone: profile.tel || profile.tel2 || ' - ',
              disabled: disabled,
              fullname_short: `${profile.firstname[0]}. ${profile.lastname}`,
              avatarURL: !!profilePicture === true ? profilePicture.avatar.avatar : null,
              roleId: profile.role === 'manager' ? 'manager' : !!usersRights === true ? usersRights.defaultRoleId : null
            })
          } else {
            let deactivatedAccount = translation.commonTranslation['deactivated_account']
            let firstname = deactivatedAccount.split(' ')[0]
            let lastname = deactivatedAccount.split(' ')[1]
            userWithGoodFormat.push({
              _id: user.userId,
              isCreator: null,
              firstname: firstname,
              lastname: lastname,
              fullname: `${firstname} ${lastname}`,
              initial: '?',
              phone: '-',
              fullname_short: `${firstname} ${lastname}`,
              avatarURL: null,
              roleId: null
            })
          }
        }
      }
      return _.unique(userWithGoodFormat)
    } else {
      return []
    }
  },
  getConversationInitial: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      if (conversation.picture) {
        let conversationFile = MessengerFiles.findOne({ _id: conversation.picture })
        if (!!conversationFile === true && conversationFile.isImage === true) {
          let file = conversationFile
          return { isPicture: !!file === true, src: file.link('avatar') }
        }
        return {}
      } else if (conversation.name && conversation.name !== '') {
        return { isPicture: false, initial: conversation.name[0] }
      } else {
        // conversationUsers = conversationUsers.filter(u => u._id !== Meteor.userId())
        let firstUser = conversationUsers.find(user => user._id === conversation.origin)
        if (!firstUser) {
          firstUser = conversationUsers[0]
        }
        if (!!firstUser === true) {
          return { isPicture: !!firstUser.avatarURL === true, src: firstUser.avatarURL, initial: firstUser.initial }
        }
      }
    }
    return { isPicture: false, initial: '?' }
  },
  getConversationName: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      if (conversation.name && conversation.name !== '') {
        return conversation.name
      } else {
        let firstUser = conversationUsers.find(user => user._id === conversation.origin)
        if (!firstUser) {
          firstUser = conversationUsers[0]
        }
        if (!!firstUser === true) {
          return firstUser.fullname
        } else {
          return '?'
        }
      }
    }
  },
  getConversationTel: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      let firstUser = conversationUsers.find(user => user._id === conversation.origin)
      if (!firstUser) {
        firstUser = conversationUsers[0]
      }
      if (!!firstUser === true) {
        return firstUser.phone
      } else {
        return '-'
      }
    }
  },
  getMessageIdForEditing: () => Template.instance().messageIdForEditing.get(),
  setMessageIdForEditing: () => {
    const messageIdForEditing = Template.instance().messageIdForEditing
    return (messageId) => {
      messageIdForEditing.set(messageId)
    }
  },
  invitUser: () => {
    const conversation = Template.instance().conversation.get()
    if (conversation) {
      const condoId = conversation.condoId
      const inConvUsers = _.uniq(_.pluck(conversation.users, 'userId'))
      let userInCondo = UsersRights.find({ condoId: condoId, userId: { $nin: inConvUsers } }, { fields: { userId: true, defaultRoleId: true } }).fetch()
      userInCondo = userInCondo.sort((a, b) => a.defaultRoleId.localeCompare(b.defaultRoleId))

      const thisCondo = Condos.findOne({ _id: condoId }, { fields: { settings: true } })
      let availableRole = ['manager']
      if (thisCondo && thisCondo.settings && thisCondo.settings.options && thisCondo.settings.options.messenger && thisCondo.settings.options.messengerGestionnaire) {
        availableRole.push('occupant')
      }

      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      return _.without(userInCondo.map(user => {
        let userProfile = UsersProfile.findOne({ _id: user.userId, role: { $in: availableRole } }, { fields: { firstname: true, lastname: true, role: true } })
        if (!!userProfile) {
          let disabled = false
          if (userProfile.role === 'occupant') {
            disabled = !Meteor.userHasRight("messenger", "writeToManager", condoId, user.userId)
            const thisUser = GestionnaireUsers.findOne({ _id: user.userId, 'resident.condos.condoId': condoId }, { fields: { _id: true, resident: true } })
            if (!thisUser) {
              return null
            } else {
              const thisUserCondo = thisUser.resident[0].condos.find((condo) => condo.condoId === condoId)
              if (!thisUserCondo || !thisUserCondo.preferences || !thisUserCondo.preferences.messagerie) {
                return null
              }
            }
          } else {
            disabled = !Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, user.userId)
          }
          let role = DefaultRoles.findOne({ _id: user.defaultRoleId }, { fields: { name: true } })
          return {
            id: user.userId,
            text: `${userProfile.firstname} ${userProfile.lastname}${!!role ? '_//_' + role.name : ''}${!!disabled ? '_//_' + translate.commonTranslation.noAccessManagerMP : ''}`,
            locked: disabled
          }
        }
      }), null, undefined)
    }
  },
  getUserDetails: (userId) => {
    const conversation = Template.instance().conversation.get()
    if (conversation) {
      let user = GestionnaireUsers.findOne({ _id: userId })
      if (user && user.profile.school) {
        return user.profile.school
      }
      const condoId = conversation.condoId
      let role = UsersRights.findOne({ userId: userId, condoId: condoId })
      if (role) {
        role = DefaultRoles.findOne({ _id: role.defaultRoleId })
        return role.name
      }
    }
  },
  renderIsTyping: () => {
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    const isTypingList = IsTyping && IsTyping.find({ 'userId': { $ne: Meteor.userId() } })
    const length = isTypingList.count()
    let div = '<div class="conversation-body-footer">'
    div += '<div class="is-typing">'
    div += '<div class="is-typing-wrap">'
    if (length > 0) {
      div += '<div class="is-typing-label">'
      isTypingList.forEach((isTyping, index) => {
        if (index === (length - 1)) {
          div += `<p class="is-typing-item">${isTyping.firstname}</p>`
        } else {
          div += `<p class="is-typing-item">${isTyping.firstname}</p>, `
        }
      })
      div += length === 1 ? translation.commonTranslation['is_typing'] : translation.commonTranslation['are_typing']
      div += '</div>'
    }
    div += '</div>'
    div += '</div>'
    div += '</div>'
    return div
  },
  onSelect: () => {
    const t = Template.instance()

    return (val) => {
      if (val && val.length > 0) {
        $('#validInvite').removeClass('disabled')
      } else {
        $('#validInvite').addClass('disabled')
      }
      t.invitedUserIds = val
    }
  },
  isOpen: () => {
    return Template.instance().isOpen.get()
  }
})
