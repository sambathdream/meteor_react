import { CommonTranslation, MessageInput, ModuleMessagerieIndex } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'

import { getMessageFiles } from '/client/sharedFunctions/messenger-files.js'

/**
* Created by vmariot on 30/04/18.
*/

const getFileUrlPath = (file, tag) => {
  return `${file._downloadRoute}/${file._collectionName}/${file._id}/${tag}/${file._id}.${file.extension}`
}

Template.messageDetailsManagerListItem.onCreated(function () {
  this.expandedAttachments = new ReactiveVar([])

  this.getFiles = (files) => {
    const images = _.map(files, f => {
      if (f && f.meta && f.meta.thumb) {
        return {
          lowRes: f.meta.thumb,
          preview: f.meta.thumb,
          large: f.meta.thumb,
          type: 'image',
          filename: f.name,
          downloadPath: `${f.meta.thumb}?download=true`,
          isVideo: true,
          videoPath: getFileUrlPath(f, 'original'),
          videoType: f.type
        }
      } else if (f.type.split("/")[0] === "image") {
        return {
          lowRes: getFileUrlPath(f, 'thumb100'),
          preview: getFileUrlPath(f, 'preview'),
          large: getFileUrlPath(f, 'large'),
          type: 'image',
          filename: f.name,
          downloadPath: `${getFileUrlPath(f, 'original')}?download=true`
        }
      } else {
        return {
          lowRes: `/img/icons/svg/${getImageByExtension(f.type)}`,
          preview: `/img/icons/svg/${getImageByExtension(f.type)}`,
          large: `/img/icons/svg/${getImageByExtension(f.type)}`,
          type: 'file',
          filename: f.name,
          downloadPath: `${getFileUrlPath(f, 'original')}?download=true`
        }
      }
    })

    return images
  }
})

Template.messageDetailsManagerListItem.onRendered(function () {
})

Template.messageDetailsManagerListItem.helpers({
  getCurrentMessage: () => Template.currentData().currentMessage,
  MbFilePreview: () => MbFilePreview,
  isNewUserMsg: () => {
    let msg = Template.currentData().currentMessage
    return msg && msg.length === 1 && msg[0] && msg[0].isNewUserMessage === true
  },
  getNewUserName: () => {
    let users = Template.currentData().usersInformation
    let msg = Template.currentData().currentMessage
    if (msg && msg.length === 1 && msg[0] && msg[0].isNewUserMessage === true) {
      if (msg[0].userId === Meteor.userId()) {
        return `${Meteor.user().profile.firstname} ${Meteor.user().profile.lastname}`
      }
      let user = users.find(u => u._id === msg[0].userId)
      if (!!user === true) {
        return user.fullname
      }
    }
  },
  getUserName: (userId) => {
    let users = Template.currentData().usersInformation
    let user = users.find(u => u._id === userId)
    if (!!user === true) {
      return user.fullname
    }
  },
  getMsgFiles: getMessageFiles(Template),
  messageFromMe: (userId) => Meteor.userId() === userId,
  isMyMessage: () => {
    let currentBlock = Template.currentData().currentMessage
    if (!!currentBlock === true && !!currentBlock[0] === true) {
      return currentBlock[0].userId === Meteor.userId()
    }
  },
  getUserPicture: (userId) => {
    if (!!userId === true) {
      let users = Template.currentData().usersInformation
      let user = users.find(user => (user || {})._id === userId)
      if (!user) {
        user = '?'
      } else {
        return { isPicture: !!user.avatarURL === true, src: user.avatarURL, initial: user.initial }
      }
    }
  },
  displayHour: (date) => {
    if (!!date === true) {
      return moment(date).local().format('HH:mm')
    }
  },
  isModified: (msg) => {
    return msg.lastUpdated ? true : false
  },
  isAttachmentExpanded: (msg) => {
    // console.log(Template.instance().expandedAttachments.get().indexOf(msg._id))
    return Template.instance().expandedAttachments.get().indexOf(msg._id) === -1 ? 4 : -1
  },
  onExpand: (msg) => {
    const t = Template.instance()

    const _onExpand = () => {
      t.expandedAttachments.set([
        ...t.expandedAttachments.get(),
        msg._id
      ])
    }

    return () => _onExpand
  },
  getRandomId: () => {
    return 'attachment-' + Math.floor(Math.random() * 10000)
  },
  initColorbox: (id) => {
    setTimeout(() => {
      $(`a.cb-${id}.is-image`)
        .colorbox({
          rel: 'msg',
          photo: true,
          scalePhotos: true,
          maxHeight:'75%',
          maxWidth:'90%',
          href: function() {
            const $img = $(this).find('img');

            return $img.attr('data-highres');
          },
          width: function() {
            const $img = $(this).find('img');

            return $img.attr('data-type') === 'file' ? 480 : false;
          },
          height: function() {
            const $img = $(this).find('img');

            return $img.attr('data-type') === 'file' ? 480 : false;
          },
          title: function() {
            const $img = $(this).find('img');

            if ($img.attr('data-type') === 'file') {
              const title = $img.attr('alt');
              const download = $img.attr('data-download');


              return `${title} <a href="${download}" target="_blank">Download</a>`;
            }

            return false;
          }
        });

      $(`a.cb-${id}.is-video`).each((index) => {
        const element = $(`a.cb-${id}.is-video`).eq(index)
        const video = $(element.attr('data-href'))
        element.colorbox({
          rel: 'msg',
          inline: true,
          width:"80%",
          height:"80%",
          href: video
        });
      });
    }, 100)
  },
  getUserStatus: (userId) => {
    const usersStatus = Template.currentData().usersStatus
    let currentUserStatus = usersStatus && usersStatus.find(user => user._id === userId)
    return currentUserStatus && currentUserStatus.status
  }
})

Template.messageDetailsManagerListItem.events({
  'click .msg-edit': function (event, template) {
    event.preventDefault()
    $('.displayBlock').toggleClass('displayBlock')
    let messageId = event.currentTarget.getAttribute("data-id")
    let setMessageIdForEditing = template.data.setMessageIdForEditing
    if (!!messageId === true && !!setMessageIdForEditing === true && typeof setMessageIdForEditing === 'function') {
      setMessageIdForEditing(messageId)

      const body = $('body');
      body.stop().animate({
        scrollTop: body[0].scrollHeight
      }, 800);
    }
  },
  'click .msg-delete': function (event, template) {
    event.preventDefault()
    $('.displayBlock').toggleClass('displayBlock')
    let messageId = event.currentTarget.getAttribute("data-id")
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translationModuleMessagerieIndex = new ModuleMessagerieIndex(lang)
    const translationCommonTranslation = new CommonTranslation(lang)

    bootbox.confirm({
      size: 'medium',
      title: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message'],
      message: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message_explain'],
      buttons: {
        confirm: { label: translationCommonTranslation.commonTranslation['confirm'], className: 'btn-red-confirm' },
        cancel: { label: translationCommonTranslation.commonTranslation['cancel'], className: 'btn-outline-red-confirm' }
      },
      backdrop: true,
      callback: function (res) {
        if (res) {
          let messageSave = MessagesFeed.findOne({ _id: messageId })
          Meteor.call('removeMessageFeed', messageId, function (error, result) {
            if (!error) {
              let existMessageAfterDeletion = MessagesFeed.findOne()
              if (!!existMessageAfterDeletion === false) {
                Meteor.call('removeConversation', messageSave.messageId, function (error, result) {
                  if (result) {
                    sAlert.success(translationModuleMessagerieIndex.moduleMessagerieIndex['conversation_deleted'])
                  }
                })
              }
            }
          })
        }
      }
    })
  },
  'click .item-pencil-dropdown': function (event, template) {
    let buttonItem = event.currentTarget
    let wrapperItem = buttonItem.parentElement
    let menuItem = wrapperItem.children[1]
    // grab the menu item's position relative to its positioned parent
    let menuItemPos = $(buttonItem).position()

    // // place the submenu in the correct position relevant to the menu item
    $(menuItem).css({
      top: menuItemPos.top - 30,
      left: menuItemPos.left + Math.round($(buttonItem).outerWidth() * 0.75) - (138 + 24)
    })
    $(menuItem).toggleClass('displayBlock')
  }
})
