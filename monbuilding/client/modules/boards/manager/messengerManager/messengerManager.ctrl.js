import { Template } from 'meteor/templating'
import { ModuleMessagerieIndex, CommonTranslation } from '/common/lang/lang.js'
let timeout = null;

Template.messengerManager.onCreated(function () {
  this.condoId = new ReactiveVar('all')
  this.userSearch = new ReactiveVar('')
  this.messageCountLimit = new ReactiveVar(20)
  let tabId = FlowRouter.getParam('tab')
  this.currentMessenger = new ReactiveVar(tabId !== 'global' ? 'enterprise' : 'global')
  let isNewMessage = FlowRouter.getRouteName() === 'app.gestionnaire.messenger.new'
  this.isNewMessage = new ReactiveVar(isNewMessage)
  Meteor.call('setNewAnalytics', { type: 'module', module: 'messenger', accessType: 'web', condoId: '' })
})

Template.messengerManager.onRendered(function () {
})

Template.messengerManager.onDestroyed(function () {
  Meteor.call('updateAnalytics', { type: 'module', module: 'messenger', accessType: 'web', condoId: '' })
})

Template.messengerManager.helpers({
  checkTabMessengerManager: () => {
    let tab = FlowRouter.getParam('tab')
    if (tab === 'enterprise') {
      FlowRouter.setParams({ tab: 'enterprise', msgId: undefined })
      Template.instance().messageCountLimit.set(20)
      Template.instance().currentMessenger.set('enterprise')
    } else if (tab === 'global') {
      FlowRouter.setParams({ tab: 'global' })
      Template.instance().currentMessenger.set('global')
    }
  },
  getCurrentTab: () => Template.instance().currentMessenger.get(),
  setCurrentTab: () => {
    let template = Template.instance()
    return (newTab) => {
      if (newTab === 'enterprise') {
        FlowRouter.setParams({ tab: 'enterprise', msgId: undefined })
        template.currentMessenger.set(newTab)
      } else {
        FlowRouter.setParams({ tab: 'global' })
        template.currentMessenger.set(newTab)
      }
    }
  },
  messengerEnterpriseIsSelected: () => Template.instance().currentMessenger.get() === 'enterprise',
  autoSelectCondoId: () => {
    let condoIds = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
    if (!!condoIds === true && Array.isArray(condoIds) && condoIds.length > 0) {
      Template.instance().condoId.set(condoIds[0])
      Session.set('currentCondo', condoIds[0])
    }
  },
  getCondo: () => Template.instance().condoId.get(),
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const t = Template.instance()
    if (condoId !== t.condoId.get()) {
      t.condoId.set(condoId)
      t.messageCountLimit.set(20)
    }
    return true
  },
  // setCondo: () => {
  //   let template = Template.instance()
  //   return (condoId) => {
  //     template.condoId.set(condoId)
  //     template.messageCountLimit.set(20)
  //   }
  // },
  getMessageCountLimit: () => Template.instance().messageCountLimit.get(),
  setMessageCountLimit: () => {
    let template = Template.instance()
    return (limit) => template.messageCountLimit.set(limit)
  },
  getTabForMainSelectorMessengerManager: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new CommonTranslation(lang)
    const condoId = Template.instance().condoId.get()
    let badgesEnterprise = 0
    let badgesPersonnal = 0
    if (condoId !== 'all') {
      const gestionnaireBadges = BadgesGestionnaire.findOne({ _id: condoId }) || {}
      badgesEnterprise = gestionnaireBadges.messenger_enterprise || 0
      badgesPersonnal = gestionnaireBadges.messenger_personnal || 0
    } else {
      BadgesGestionnaire.find().forEach(badge => {
        badgesEnterprise += badge.messenger_enterprise || 0
        badgesPersonnal += badge.messenger_personnal || 0
      })
    }
    return [
      {
        _id: 'enterprise',
        name: translation.commonTranslation['messenger_enterprise'],
        badges: badgesEnterprise
      },
      {
        _id: 'global',
        name: translation.commonTranslation['messenger_personnal'],
        badges: badgesPersonnal
      }
    ]
  },
  getIsNewMessage: () => Template.instance().isNewMessage.get(),
  setIsNewMessageAsFalse: () => {
    let template = Template.instance()
    return () => template.isNewMessage.set(false)
  },
  // CDD_cursor: () => {
  //   return Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
  // },
  canSendMessage: () => {
    const selectedCondo = localStorage.getItem('condoIdSelected')
    if  (selectedCondo !== 'all') {
      return Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', selectedCondo)
              && Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', selectedCondo)
    } else {
      let condoIds = []
      let condoIdSee = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
      condoIds = condoIdSee.filter((condoId) => { return Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, Meteor.userId()) })
      return condoIds && condoIds.length > 0
    }
  },
  isSubscribeReady: () => Template.instance().subscriptionsReady(),
  moduleMessengerManagerIsActive: () => {
    const selectedCondo = localStorage.getItem('condoIdSelected')
    if (selectedCondo != 'all') {
      return Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', selectedCondo)
    } else {
      let condoIds = []
      let condoIdSee = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise')
      condoIds = condoIdSee.filter(condoId => {
        return Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId)
      })
      return condoIds && condoIds.length > 0
    }
  }
})

Template.messengerManager.events({
  'click #addMessage' (event, template) {
    template.isNewMessage.set(true)
  }
})
