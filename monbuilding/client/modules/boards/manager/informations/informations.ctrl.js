import { Template } from "meteor/templating";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { DeletePost, ModuleActualityIndex, CommonTranslation } from "/common/lang/lang.js";
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';

function GestionnaireCondosInCharge(gestionnaire) {
  if (gestionnaire) {
    const enterprise = Enterprises.findOne({ _id: gestionnaire.identities.gestionnaireId }, { fields: { users: true } })
    const mapUser = enterprise.users
    const findUser = _.find(mapUser, function (elem) {
      return elem.userId == Meteor.userId();
    })
    const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);
    if (condoIds) {
      return condoIds;
    }
  }
}

Template.informationsGestionnaire.onCreated(function () {
  const utcOffset = new Date().getTimezoneOffset()
  this.title = new ReactiveVar(null);
  this.oldtitle = new ReactiveVar(null);
  this.oldInfo = new ReactiveVar(null);
  this.description = new ReactiveVar(null);
  this.olddesc = new ReactiveVar(null);
  this.condoId = new ReactiveVar("all");
  this.oldcondoId = new ReactiveVar("all");
  this.condoIdFilter = new ReactiveVar("all");
  this.infoId = new ReactiveVar(FlowRouter.getParam("infoId"));
  this.infosSearch = new ReactiveVar("");
  this.sortInfo = new ReactiveVar(false);
  this.editingPost = new ReactiveVar(null);
  this.locate = new ReactiveVar('');
  this.condoIds = new ReactiveVar([]);
  this.scheduledPost = new ReactiveVar(false);
  this.scheduledDate = new ReactiveVar(moment().toDate());
  this.scheduledTime = new ReactiveVar(moment().subtract(utcOffset, 'm').add(1, 'h').format('HH') + ':00')

  this.isActuError = new ReactiveVar(0b11);
  this.condoError = new ReactiveVar(false);
  this.fm = new FileManager(UserFiles);

  this.counter = 20;
  this.hasReachedEnd = false;
  const instance = Template.instance();
  const detailId = instance.infoId.get();
  if (detailId) {
    const post = ActuPosts.findOne(detailId)
    Meteor.call('module-actu-view-by-id', post && post.actualityId, detailId);
  }

  const self = this
  // this.updateActuSub = _.debounce((self, searchText) => {
  //   Meteor.subscribe("actus_gestionnaire", self.counter, searchText, () => {
  //     console.log(ActuPosts.find().fetch())
  //     console.log('yp')
  //   })
  // }, 500)
  this.autorun(() => {
    // console.log('this.infosSearch.get()', this.infosSearch.get())
    const searchText = self.infosSearch.get()
    this.subscribe("info_gestionnaire", self.counter, searchText, () => {
      // console.log(ActuPosts.find().fetch())
      // console.log('yp')
    })
    // this.updateActuSub(self, searchText)
  });

  Meteor.call('setNewAnalytics', { type: "module", module: "info", accessType: "web", condoId: '' });
  let condoIds = GestionnaireCondosInCharge(Meteor.user())
  let actuIds = []
  _.each(condoIds, function (cId) {
    let c = Condos.findOne(cId)
    if (c) {
      let actu = _.find(c.modules, function (mod) {
        return mod.name === "actuality"
      })
      if (actu) {
        actuIds.push(actu.data.actualityId)
      }
    }
  })

  this.files = new ReactiveVar([]);
  this.existingFiles = new ReactiveVar([]);
  this.subscribe('actuFiles', actuIds);
});

Template.informationsGestionnaire.onDestroyed(function () {
})

Template.informationsGestionnaire.onRendered(function () {
});

function putInSelectedCondo() {
  let selectedCondo = Session.get('selectedCondo')
  if (!selectedCondo || selectedCondo === 'all') {
    selectedCondo = Template.instance().condoId.curValue
  }
  const condo = Condos.findOne({ _id: { $eq: selectedCondo } });
  const actu = _.filter(_.map(condo, function (value, key) {
    return key == 'modules' ? _.find(value, function (elem) {
      return elem.name == 'actuality';
    }) : _.noop;
  }), function (elem) {
    return elem != undefined;
  })[0];
  return { actuId: actu.data.actualityId, condoId: condo._id };
}

function fillActuObject(createActu, template) {
  const lang = FlowRouter.getParam("lang") || "fr"
  const tr_common = new CommonTranslation(lang)
  const utcOffset = new Date().getTimezoneOffset()

  $(".submitActu").button('loading');

  let selectedCondo = Session.get('selectedCondo')
  if (!selectedCondo || selectedCondo === 'all') {
    selectedCondo = Template.instance().condoId.curValue
  }

  createActu = {
    title: $('.titleActu').val(),
    locate: $('.locateActu').val(),
    description: $('.mb__textarea > textarea').val(),
    startDate: '',
    startHour: '',
    endDate: '',
    endHour: '',
    // actualityId: putInSelectedCondo().actuId,
    userId: Meteor.user()._id,
    views: [],
    history: [],
    files: template.existingFiles.get(),
    condoId: selectedCondo,
    condoIds: template.condoIds.get(),
    scheduledPost: template.scheduledPost.get(),
    scheduledDate: template.scheduledDate.get(),
    scheduledTime: template.scheduledTime.get()
  };

  const __files = template.files.get();

  template.fm.batchUpload(__files)
    .then(files => {
      createActu.files = createActu.files.concat(_.map(files, f => f._id));

      if (template.editingPost.get()) {
        createActu.isUpdate = new Date();
        createActu.updatedAt = Date.now();

        Meteor.call('module-actu-edit-post', template.editingPost.get(), createActu, function (error) {
          template.editingPost.set(null);
          $('#modal_add_information').modal('hide');
          $(".submitActu").button('reset');
          $('#buttonCondoViewModal').val('');
          $('.titleActu').val("");
          $('.locateActu').val("");
          $('.mb__textarea > textarea').val('');
        });
      }
      else {
        createActu.createdAt = Date.now();
        createActu.updatedAt = Date.now();

        Meteor.call('module-actu-create-post', createActu, function () {
          $(".submitActu").button('reset');
          $('#modal_add_information').modal('hide');
          $('#buttonCondoViewModal').val('');
          $('.titleActu').val("");
          $('.locateActu').val("");
          $('.mb__textarea > textarea').val('');
        });
      }
    })
    .catch(err => {
      sAlert.error(tr_common.commonTranslation[err.reason])
      $(".submitActu").button('reset');
    })
}

function resetErrorHandler(template) {
  $('#buttonCondoViewModal').removeClass('errorModalInput');
  $('.titleActu').removeClass('errorModalInput');
  $('.locateActu').removeClass('errorModalInput');
  $($('.descrActu').children()[2]).removeClass('errorModalInput');
  template.isActuError.set(0b11);
  template.condoError.set(false);
}

Template.informationsGestionnaire.events({
  'click li .timeSlotStart': function(event, template) {
    template.scheduledTime.set(this.toString())
  },
  'click .info-msg-box': function () {
    $('.mb__textarea > textarea').focus();
  },
  'show.bs.modal #modal_add_information': function (event, template) {
    let modal = $(event.currentTarget);
    window.location.hash = "#modal_add_information";
    window.onhashchange = function () {
      if (location.hash != "#modal_add_information") {
        modal.modal('hide');
      }
    }

    if (!!template.editingPost.get()) {
      const oldInfo = ActuPosts.findOne(template.editingPost.get());
      if (!!oldInfo) {
        template.oldInfo.set(oldInfo)
        template.olddesc.set(oldInfo.description)
        template.oldtitle.set(oldInfo.title)
        template.title.set(oldInfo.title);
        template.description.set(oldInfo.description);
        template.condoId.set(oldInfo.condoId);
        template.existingFiles.set(oldInfo.files);
        Session.set('selectedCondo', oldInfo.condoId);
        setTimeout(() => { $('#modal_add_information .info-msg-box-row .mb__textarea textarea')[0].focus() }, 1000);
      }
    }
  },
  'hidden.bs.modal #modal_add_information': function (event, template) {
    emptyFormByParentId('modal_add_information');
    $('.errorModalInput').removeClass('errorModalInput');
    template.files.set([]);
    template.existingFiles.set([]);
    template.isActuError.set(0b11);
    template.condoError.set(false);
    $('.mb__textarea > textarea').val('');
    template.condoId.set('all')
    template.fm.clearFiles();
    if ($('#attachments li')[0]) {
      $('#attachments li')[0].remove();
    }
    template.title.set(null);
    template.description.set(null);
    template.editingPost.set(null);
    template.condoIds.set([]);
    history.replaceState('', document.title, window.location.pathname);
  },
  'keyup .locateActu': function (event, template) {
    template.locate.set(event.currentTarget.value)
  },
  'scroll .info-list': function (event, template) {
    if (template.hasReachedEnd == true)
      return
    if (event.currentTarget.offsetHeight + event.currentTarget.scrollTop == event.currentTarget.scrollHeight) {
      template.counter += 15;
      template.subscribe('info_gestionnaire', template.counter)
    }
  },
  'click .submitActu': function (event, template) {
    const descField = $('.mb__textarea > textarea');
    const condoPickerField = $('#mb-condo-picker').find('.mb-select');

    descField.parents('.info-msg-box').removeClass('mb-input-error');
    condoPickerField.removeClass('mb-select-error');

    template.isActuError.set(0b11);
    let createActu = {};
    if (Template.instance().condoId.get() == 'all' || !Template.instance().condoId.get()) {
      $('#mb-condo-picker').find('.mb-select').addClass('mb-select-error')
      template.condoError.set(true);
      $(".submitActu").button('reset');
    }
    else {
      $('#buttonCondoViewModal').removeClass('errorModalInput');
      template.condoError.set(false);
    }
    if (!$('.titleActu').val()) {
      $('.titleActu').addClass("mb-input-error");
      template.isActuError.set(template.isActuError.get() & 0b1);
      $(".submitActu").button('reset');
    }
    if (descField.val().trim() === '') {
      descField.parents('.info-msg-box').addClass('mb-input-error');
      template.isActuError.set(template.isActuError.get() & 0b10);
      $(".submitActu").button('reset');
    }

    if (template.isActuError.get() === 0b11) {
      $('.titleActu').removeClass('mb-input-error');
      descField.parents('.info-msg-box').removeClass('mb-input-error');
      fillActuObject(createActu, template, Template.currentData().type);
      // template.editingPost.set(null);
    }
    template.isActuError.get() === 0b10 ? $('.titleActu').removeClass('mb-input-error')
      : template.isActuError.get() === 0b1 ? descField.parents('.info-msg-box').removeClass('mb-input-error')
        : _.noop;
  },
  'click .post-edit': function (event, template) {
    const id = event.currentTarget.getAttribute('data-id');
    template.editingPost.set(id);
  },
  'click .post-delete': function (event, template) {
    const id = event.currentTarget.getAttribute('data-id');
    const post = ActuPosts.findOne(id);
    const lang = (FlowRouter.getParam("lang") || "fr")
    let translation = new DeletePost(lang);
    let tr_common = new CommonTranslation(lang)
    bootbox.confirm({
      title: tr_common.commonTranslation["title_delete_information"],
      message: post.userId == Meteor.userId() ? tr_common.commonTranslation["message_delete_information"] : tr_common.commonTranslation["message_delete_information_other"],
      buttons: {
        confirm: { label: translation.deletePost["yes"], className: "btn-red-confirm" },
        cancel: { label: translation.deletePost["no"], className: "btn-outline-red-confirm" }
      },
      callback: function (res) {
        if (res) {
          Meteor.call('module-actu-post-delete', id);
          sAlert.success(translation.deletePost["succes_delete_post"]);
        }
      }
    });
  },
  'click .seeMoreOrLess': function (event, template) {
    let contentObj = $(event.currentTarget.parentElement).find('.info-content');
    let translation = new ModuleActualityIndex((FlowRouter.getParam("lang") || "fr"));
    contentObj.find('.ellipsisedText').toggleClass('show');
    contentObj.find('.notEllipsisedText').toggleClass('show');
    contentObj.toggleClass('ellipsis');
    $(event.currentTarget).text(contentObj.hasClass('ellipsis') ? translation.moduleActualityIndex["see_more"] : translation.moduleActualityIndex["see_less"]);
  },
  'input .titleActu': function (event, template) {
    template.title.set(event.currentTarget.value)
  },
  'input .mb__textarea > textarea': function (event, template) {
    template.description.set(event.currentTarget.value)
  },
  'click #info-back-form': function (e, t) {
    let lang = FlowRouter.getParam("lang") || "fr";
    t.infoId.set('');
    FlowRouter.go(`/${lang}/gestionnaire/informations`);
  },
  'click .go-to-detail': function (e, t) {
    const id = e.currentTarget.getAttribute('data-id');
    const lang = FlowRouter.getParam('lang') || 'fr';
    t.infoId.set(id);
    Meteor.call('module-actu-view-by-id', null, id);
    FlowRouter.go('app.gestionnaire.informations.infoId', {
      lang,
      infoId: id
    });
  }
});


Template.informationsGestionnaire.helpers({
  getCondoIdFilter: () => {
    return Template.instance().condoIdFilter.get()
  },
  getCondo: (condoId) => {
    let condo = Condos.findOne(condoId);
    if (condo) {
      if (condo.info.id != -1)
        return condo.info.id + ' - ' + condo.getName();
      else
        return condo.getName();
    }
  },
  getActu: () => {
    const instance = Template.instance();
    const condoId = instance.condoIdFilter.get();

    const search = instance.infosSearch.get();
    // const regexp = new RegExp(search.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");
    // console.log('yo')
    // console.log(ActuPosts.find().fetch())
    const editRight = Meteor.listCondoUserHasRight("actuality", "write")
    const deleteRight = Meteor.listCondoUserHasRight("actuality", "delete")
    return _.map(ActuPosts.find({
      $and: [
        { condoId: { $in: condoId === 'all' ? Meteor.listCondoUserHasRight("actuality", "see") : [condoId] } },
        { startDate: { $eq: '' } }
      ]
    }, { sort: { createdAt: -1 } }).fetch(), (a) => {
      return {
        editRight: editRight.includes(a.condoId) && a.userId === Meteor.userId(),
        deleteRight: deleteRight.includes(a.condoId),
        ...a
      }
    });
  },
  // getFiles: () => {
  //   const post = ActuPosts.findOne(Template.instance().infoId.get());
  //   if (post) {
  //     return UserFiles.find({ _id: { $in: post.files } });
  //   }
  // },
  isActuError: () => {
    return Template.instance().isActuError.get() != 0b11;
  },
  isError: () => {
    return Template.instance().isActuError.get() !== 0b11 || Template.instance().condoError.get();
  },
  condoError: () => {
    return Template.instance().condoError.get();
  },
  ifEditPost: (input) => {
    if (Template.instance().editingPost.get()) {
      const post = ActuPosts.findOne(Template.instance().editingPost.get());
      if (post && !Template.instance().fm.getFileList().length) {
        if ($('#attachments li')[0]) {
          $('#attachments li').remove();
        }
        const files = UserFiles.find({ _id: { $in: post.files } });
        Template.instance().fm.addFiles(files.fetch());
        if (Template.instance().fm.getFileList().length) {
          const post = ActuPosts.findOne(Template.instance().editingPost.get());
          // var files = UserFiles.find({ _id: { $in: post.files } });
          for (let i = 0; i < files.fetch().length; i++) {
            let data = {};
            data.name = files.fetch()[i].name;
            data.size = (files.fetch()[i].size / 1000).toFixed(2) + ' kb';
            data.id = 'file-' + files.fetch()[i].size;

            var wrapper = $('#file-' + files.fetch()[i].size);
            wrapper.find('.meter').remove();
            wrapper.addClass('completed');
          }
        }
      }
      if (_.has(ActuPosts.findOne(Template.instance().editingPost.get()), input))
        return ActuPosts.findOne(Template.instance().editingPost.get())[input];
    }

    return Template.instance()[input] ? Template.instance()[input].get() : "";
  },
  isEdited: (postId) => {
    let info = ActuPosts.findOne(postId);
    if (info) {
      return (info.isUpdate);
    }
    return (false);
  },
  eachLoad: () => {
    if (!Template.instance().editingPost.get()) {
      resetErrorHandler(Template.instance());
      $('#buttonCondoViewModal').val('');
      $('.titleActu').val('');
      $('.locateActu').val('');
    }
    return Template.currentData().load;
  },
  editPost: () => {
    return Template.instance().editingPost.get();
  },
  CDD_cursor_modal: () => {
    let condoIds = [];
    let condoIdSee = Meteor.listCondoUserHasRight("actuality", "see");
    condoIds = condoIdSee.filter((condoId) => { return Meteor.userHasRight('actuality', 'write', condoId, Meteor.userId()) });
    return condoIds;
  },
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const templ = Template.instance()
    if (condoId !== templ.condoIdFilter.get()) {
      templ.condoIdFilter.set(condoId);
    }
    return true
  },
  modalCondoIdCb: () => {
    var templ = Template.instance();
    return (condoId) => {
      if (condoId)
        templ.condoId.set(condoId);
    }
  },
  addNewInformation: () => {
    return () => {
      $('#modal_add_information').modal('show');
    }
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  searchMode: () => {
    return Template.instance().infosSearch.get() !== ''
  },
  descriptionChange: () => {
    const t = Template.instance();

    return value => t.description.set(value);
  },
  MbTextArea: () => MbTextArea,
  setHeight: () => {
    setTimeout(() => {
      $(".mb-info-container").animate({ height: window.innerHeight - 140 }, 0);
    }, 10)
  },
  onSearch: () => {
    const template = Template.instance();
    return (val) => {
      template.infosSearch.set(val)
    }
  },
  isAllCondo: () => {
    return Template.instance().condoIdFilter.get() === 'all'
  },
  cantSendActu: () => {
    const instance = Template.instance()
    let title = instance.title.get()
    let description = instance.description.get()
    let oldDesc = instance.olddesc.get()
    let oldTitle = instance.oldtitle.get()
    let condoIds = instance.condoIds.get()
    let locate = instance.locate.get()
    let old = instance.oldInfo.get()
    let filesHasChange = true
    filesHasChange = filesHasChange && (instance.files.get().length)
    filesHasChange = filesHasChange && (instance.existingFiles.get().length === old.files)

    if (condoIds.length > 0 &&
    title &&
    title.trim() !== '' &&
    description &&
    description.trim() !== '' &&
    (title !== oldTitle ||
    description !== oldDesc ||
    condoId !== old.condoId ||
    locate !== old.locate ||
    filesHasChange
    ))
      return false
    return true
  },
  isParamExist: () => {
    return !!Template.instance().infoId.get();
  },
  detailId: () => {
    const instance = Template.instance();
    const detailId = instance.infoId.get();
    // if (!!detailId) {
    //   const post = ActuPosts.findOne(detailId)
    //   if (!!post) {
    //     Meteor.call('module-actu-view-by-id', post.actualityId, detailId);
    //   }
    // }
    return detailId;
  },
  MbFilePreview: () => MbFilePreview,
  getFiles: (fileIds) => {
    return UserFiles.find({_id: {$in: fileIds.filter(file => !file.fileId)}})
  },
  getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  viewFile: (file) => {
    let tag = 'large'
    return `${file._downloadRoute}/${file._collectionName}/${file._id}/${tag}/${file._id}.${file.extension}`
    // return FileServer.getFileUrl(file, 'thumbnail')
    // console.log('file', file)
    // return "HEY"
  },
  getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  getPreviewFiles: () => {
    let fileIds = Template.instance().existingFiles.get()
    const reactiveFiles = Template.instance().files.get()

    const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds } });

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onFileAdded: () => {
    const tpl = Template.instance();

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  getNewClass: (item) => {
    return !item.userUpdateView.find(view => view === Meteor.userId()) ? 'is-new': '';
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      tpl.existingFiles.set(_.filter(tpl.existingFiles.get(), f => f.fileId ? f.fileId !== fileId : f !== fileId))
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  toggleCheck: (key) => {
    const instance = Template.instance()

    return () => () => {
      value = instance[key].get()
      instance[key].set(!value)
    }
  },
  getSelectedCondos: () => {
    return Template.instance().condoIds.get()
  },
  multiCondoCb: () => {
    const instance = Template.instance()
    return (condos) => {
      if (_.isArray(condos)) {
        instance.condoIds.set(condos)
      }
    }
  },
  timeSlot: () => {
    // dropdown for the inputs startHour and endHour
    let slots = [];
    for (let i = 0; i < 24; i++) {
      let hour = (i < 10) ? "0" + i.toString() : i.toString();
      slots.push(hour + ":" + "00");
      slots.push(hour + ":" + "15");
      slots.push(hour + ":" + "30");
      slots.push(hour + ":" + "45");
    }
    return slots;
  },
  getScheduledDate: () => {
    return moment(Template.instance().scheduledDate.get()).locale(FlowRouter.getParam("lang") || "fr").format('dddd, D MMM YYYY')
  },
});
