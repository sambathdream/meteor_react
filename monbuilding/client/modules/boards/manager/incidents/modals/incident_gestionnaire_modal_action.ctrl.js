import moment from 'moment';
import {UsersManager} from '/client/components/UsersManager/UsersManager.js';
import {FileManager} from '/client/components/fileManager/filemanager.js';
import { Session } from 'meteor/session';
import { DeletePost, CommonTranslation } from '/common/lang/lang.js'
import MbTextArea from "../../../../../components/MbTextArea/MbTextArea";
import { clearLine } from 'readline';

function closeModal(modalId) {
	Meteor.setTimeout(function() {
		Session.set("modal", false);
	}, 500);
}


/*
** ACTION MODAL
** @Params :
** action {
**  title: String
**
: ["valid", "OS", "interv", "cancelInterv"]
**  remind: Boolean
**  cb: function (data, [notifHours], [intervData])
** }
*/

const batchUploadCustomFiles = (files, t) => {
  return new Promise((resolve, reject) => {
    t.fm.batchUpload(tf)
      .then(files => {
        let fileIds = files.map(f => f._id);
        resolve(fileIds);
      })
  })
}

/**
 *
 * @param data
 * @param t
 * @returns Promises
 */
const batchUploadFiles = (data, t, specialCase = null) => {
  if (specialCase) {
    return new Promise((resolve, reject) => {
      t.fm.batchUpload(data)
        .then(files => {
          let fileIds = files.map(f => f._id);

          resolve(fileIds);
        }).catch(err => {
          sAlert.error(bootConfirm.commonTranslation[err.reason])
          reject()
        })
    })
  }

  return _.map(t.tempFiles.get(), (tf, key) => {
    if (key === 'custom') {
      return new Promise((resolve, reject) => {
        const customFilePromises = _.map(tf, (customFiles, index) => {
          const customObj = t.customs.list()[index];

          if (customObj.userManager.getUsersList().length > 0 && $(`[name=checkcustom${index}]`).is(':checked')) {
            return batchUploadFiles(customFiles, t, true);
          }
        });

        Promise.all(_.compact(customFilePromises)).then(result => {
          _.map(result, (r, i) => {
            data.share[key][i].files = r;
          });

          resolve()
        }).catch(err => {
          // sAlert.error(bootConfirm.commonTranslation[err.reason])
          reject(err)
        })
      })
    } else {
      return new Promise((resolve, reject) => {
        t.fm.batchUpload(tf)
          .then(files => {
            let fileIds = files.map(f => f._id);

            data.share[key].files = fileIds

            resolve();
          }).catch(err => {
            // sAlert.error(bootConfirm.commonTranslation[err.reason])
            reject(err)
          })
      })
    }
  })
}

Template.incident_gestionnaire_modal_action.onCreated(function() {
  this.id = FlowRouter.getParam("incidentId");
  this.action = this.data.action;

  this.fm = new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true});

  this.errorMessage = new ReactiveVar("");
  this.undefinedHour = new ReactiveVar(false);
  this.remindSelector = new ReactiveVar("day");
  this.customs = new ReactiveArray([]);
  this.toValidate = new ReactiveVar(false);

  this.customs.push({
    userManager: new UsersManager(GestionnaireUsers),
    // userManager: (list),
    fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
    message: {value: ""}
  });

  this.tempFiles = new ReactiveVar({
    all: [],
    declarer: [],
    intern: [],
    custom: []
  });

  this.messages = new ReactiveVar({
    all: '',
    declarer: '',
    intern: '',
    custom: ''
  });

  this.shareData = {
    'all': {
      fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
      message: {value: ""}
    },
    'declarer': {
      fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
      message: {value: ""}
    },
    'intern': {
      fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
      message: {value: ""}
    }
  }
});

Template.incident_gestionnaire_modal_action.onRendered(function () {
  const lang = FlowRouter.getParam("lang") || "fr"
  $('#incident_action_modal').modal({
    show: true,
    keyboard: false,
    backdrop: 'static'
  });

  $('#datepicker').datepicker({
    format: "dd/mm/yyyy",
    weekStart: 1,
    maxViewMode: 0,
    language: lang,
    autoclose: true,
    todayHighlight: true,
    startDate: '0d',
    templates: {
      leftArrow: '<i class="fa fa-chevron-left"></i>',
      rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
  }).on('show', function(event){initMonth(event)}).on('changeMonth', function(event){changeMonth(event)});

  function changeMonth(event) {
    let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
    if (new Date(event.date).getMonth() == new Date().getMonth()) {
      $(prev).css('visibility', 'visible');
      $(prev).removeClass('prev');
      $(prev).html('');
    }
    else {
      $(prev).addClass('prev');
      $(prev).html('<i class="fa fa-chevron-left"></i>');
    }
  }

  function initMonth() {
    let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
    let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
    const months = {
      'janvier': 'JANVIER',
      'février': 'FÉVRIER',
      'mars': 'MARS',
      'avil': 'AVRIL',
      'mai': 'MAI',
      'juin': 'JUIN',
      'juillet': 'JUILLET',
      'août': 'AOÛT',
      'septembre': 'SEPTEMBRE',
      'octobre': 'OCTOBRE',
      'novembre': 'NOVEMBRE',
      'décembre': 'DÉCEMBRE'
    };
    for (var i = 0; i != Object.keys(months).length; i++) {
      if (Object.keys(months)[i] == date)
      break;
    }
    if (new Date().getMonth() == i) {
      $(prev).css('visibility', 'visible');
      $(prev).removeClass('prev');
      $(prev).html('');
    }
  }
});

Template.incident_gestionnaire_modal_action.events({
  'show.bs.modal #incident_action_modal': function(event, template) {
    let modal = $(event.currentTarget);
    window.location.hash = "#incident_action_modal";
    window.onhashchange = function() {
      if (location.hash != "#incident_action_modal"){
        modal.modal('hide');
      }
    }
  },
  'hidden.bs.modal #incident_action_modal': function(event, template) {
    emptyFormByParentId('incident_action_modal');
    history.replaceState('', document.title, window.location.pathname);
    Session.set("modal", false);
  },
  'click .up-btn': function(e, t) {
    var btn = $(this);
    if ($('.remind-nb').attr('max') == undefined || (parseInt($('.remind-nb').val()) < parseInt($('.remind-nb').attr('max')))) {
      $('.remind-nb').val(parseInt($('.remind-nb').val(), 10) + 1);
    } else {
      btn.next("disabled", true);
    }
  },
  'click .down-btn': function(e, t) {
    var btn = $(this);
    if ($('.remind-nb').attr('min') == undefined || (parseInt($('.remind-nb').val()) > parseInt($('.remind-nb').attr('min')))) {
      $('.remind-nb').val(parseInt($('.remind-nb').val(), 10) - 1);
    } else {
      btn.next("disabled", true);
    }
  },
  'input .remind-nb': function(e, t) {
    $('.remind-nb').keydown(function(e){
      /*Allow: delete, backspace, tab, escape, enter*/
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
      /*Allow: Ctrl/cmd+A*/
      (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      /*Allow: Ctrl/cmd+C*/
      (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) ||
      /*Allow: Ctrl/cmd+X*/
      (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) ||
      /*Allow: home, end, left, right*/
      (e.keyCode >= 35 && e.keyCode <= 39)) {
        return;
      }
      /*Ensure that it is a number and stop the keypress*/
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });
  },
  'focusout .remind-nb': function(e, t) {
    $('.remind-nb').val(parseInt(e.currentTarget.value));
  },
  'click .closeModal': function(e, t) {
    closeModal("#incident_action_modal");
  },
  'click #checkUndefined': function(e, t) {
    let val = $('[name=checkUndefined]').is(':checked');
    t.undefinedHour.set(val);
    $("#time-menu").css("background-color", "ececec")
    $("#time-menu").css("cursor", "auto")
    $("#time-menu").prop("disabled", val)
  },
  'click .addCustom': function(e, t) {
    t.customs.push({
      userManager: new UsersManager(GestionnaireUsers),
      fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
      message: {value: ""},
    });

    t.messages.set({
      ...t.messages.get(),
      custom: [...t.messages.get()['custom'], '']
    })

    t.tempFiles.set({
      ...t.tempFiles.get(),
      custom: [...t.tempFiles.get()['custom'], []]
    })
  },
  'input #sujet': function (event, template) {
		if ($('#sujet').val().length > 50) {
			event.preventDefault();
			$('#sujet').val($('#sujet').val().substring(0, 50));
		}
		else
			template.length.set($('#sujet').val().length);
	},
  'click #incident-submit': function(e, t) {
    const el = this;
    let action = t.action;
    let data = {
      share: {
        all: {
          state: $('[name=checkall]').is(':checked'),
          comment: ($('[name=checkall]').is(':checked') ? t.messages.get().all : ""),
          files: []
        },
        declarer: {
          state: $('[name=checkdeclarer]').is(':checked'),
          comment: ($('[name=checkdeclarer]').is(':checked') ? t.messages.get().declarer : ''),
          files: []
        },
        intern: {
          state: $('[name=checkintern]').is(':checked'),
          comment: ($('[name=checkintern]').is(':checked') ? t.messages.get().intern : ''),
          files: []
        },
        custom: []
      }
    }
    $(".roleCheckBox").each(function() {
      let thisId = $(this).attr("id");
      data.share[thisId] = {
        state: $('[name=check' + thisId + ']').is(':checked'),
        comment: ($('[name=check' + thisId + ']').is(':checked') ? t.shareData[thisId].message.value : ""),
        files: _.map(_.filter(t.shareData[thisId].fileManager.getFileList(), (e) => {return e.done === true}), (e) => {return e.file._id})
      }
    });

    /*NOTIF TIME ON MODAL ACTION*/
    let i = Incidents.findOne(t.id);
    let priority = IncidentPriority.findOne(i.info.priority);
    let notifHours = getReminderValue(i.condoId, i.info.priority);
    let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));

    if ($('#checkboxRed7').is(':checked')){
      notifHours = parseInt($('#remindNb').val());
      if ($('.select-min option:selected').val() == 'days')
      notifHours *= 24;
    }
    if (!notifHours || notifHours === 0 || notifHours < -1) {
      notifHours = 1;
    }
    let inc = 0;
    for (c of t.customs.list()) {
      let message = c.message.value;
      let list = c.userManager.getUsersList();
      if (list.length > 0 && $('[name=checkcustom'+inc+']').is(':checked')) {
        data.share.custom.push({
          "users": list,
          "comment": t.messages.get()['custom'][inc],
          "files": []
        });
      }
      inc++;
    }

    if (action.type === "valid") {
      data.title = $('[name=inputTitle]').val();
      data.priority = $('[name=selectPriority]').val();
      data.type = $('[name=selectType]').val();
    }
    if (action.type === "OS") {
      data.keeper_intervention = $('[name=checkKeeper]').is(':checked');
      data.extern_intervention = $('[name=checkExtern]').is(':checked');
      data.extern_details = $('[name=inputExtern]').val();
      if (data.keeper_intervention === false && data.extern_intervention === false) {
        $('.checkLabel').addClass('errorModalLabel');
        t.errorMessage.set(translation.commonTranslation['add_technician_error'])
        return ;
      }
    }

    /* GESTION ERREUR SUR DATE INTERVENTION*/
    let interv = undefined
    if (action.type === 'interv' || action.type === 'OS') {
      let undef = t.undefinedHour.get()
      let time = $('[name=time]').val().split(':')
      let arrdate = $('[name=datepicker]').val().split('/')
      let timeOfInterv = moment(`${$('[name=datepicker]').val()} ${$('[name=time]').val()}`, 'DD/MM/YYYY HH:mm')
      if ($('[name=datepicker]').val() !== '' && (arrdate.length !== 3 || timeOfInterv.isValid() !== true) && action.type === 'OS') {
        $('#datepicker').addClass('errorModalInput')
        t.errorMessage.set(translation.commonTranslation['intervention_date_invalid'])
        return
      }
      if (time.length == 2 && arrdate.length !== 3 && action.type === 'OS') {
        $('#datepicker').addClass('errorModalInput')
        t.errorMessage.set(translation.commonTranslation['choose_date_error'])
        return
      }
      else if ($('[name=time]').val() !== '' && time.length != 2 && arrdate.length == 3 && action.type === 'OS' && undef == false) {
        $('#time-menu').addClass('errorModalInput')
        t.errorMessage.set(translation.commonTranslation['intervention_time_invalid'])
        return
      }
      let date = timeOfInterv.isValid() ? timeOfInterv.toDate(): new Date(arrdate[1] + '/' + arrdate[0] + '/' + arrdate[2])
      /*let date = moment($('[name=datepicker]').val(), 'DD/MM/YYYY');*/
      if (!undef) {
        if (action.type !== 'OS') {
          date.setHours(parseInt(time[0]), parseInt(time[1]))
          if (moment(date).isSameOrAfter(moment()) === false) {
            $('#time-menu').addClass('errorModalInput')
            t.errorMessage.set(translation.commonTranslation['intervention_time_invalid'])
            return
          }
        }
      } else {
        if (!isNaN(date.getDate()) && moment(date).isSameOrAfter(moment().startOf('day')) === false) {
          $('#datepicker').addClass('errorModalInput')
          t.errorMessage.set(translation.commonTranslation['intervention_date_invalid'])
          return
        }
      }
      if (!isNaN(date.getDate())) {
        interv = { share: data.share, date: date, undefinedHour: undef }
      }
    }
    if (action.type==="cancelInterv") {
      let dateIndex = parseInt($('[name=radioCancel]:checked').val());
      if (isNaN(dateIndex)) {
        $('.radioLabel').addClass('errorModalLabel');
        t.errorMessage.set(translation.commonTranslation['cancel_date_error']);
        return ;
      }
      data.date = i.state.intervention[dateIndex].date;
      data.undefinedHour = i.state.intervention[dateIndex].undefinedHour;
    }

    const bootConfirm = new CommonTranslation(FlowRouter.getParam("lang") || "fr");

    if (_.find(data.share, function(elem, index) {  
      return (index == "custom" ? (elem.length > 0 ? true : false) : elem.state ) 
    }) == undefined) {
      $('[name=checkintern]').prop( "checked", true );
      data.share.intern.state = true;
      bootbox.confirm({
        size: "medium",
        title: translation.commonTranslation['confirmation'],
        message: translation.commonTranslation['no_communication'],
        buttons: {
          confirm: {label: bootConfirm.commonTranslation["confirm"], className: "btn-red-confirm"},
          cancel: {label: bootConfirm.commonTranslation["cancel"], className: "btn-outline-red-confirm"}
        },
        backdrop: false,
        callback: function(result) {
          if (result) {
            const filePromises = batchUploadFiles(data, t);

            Promise.all(filePromises).then(() => {
              action.cb(data, notifHours, interv);

              $('#incident_action_modal').modal('hide');
              closeModal("#incident_action_modal");
            }).catch(err => {
              sAlert.error(bootConfirm.commonTranslation[err.reason])
            });
          }
        }
      });
    }
    else {
      const filePromises = batchUploadFiles(data, t);

      Promise.all(filePromises).then(() => {
        action.cb(data, notifHours, interv);

        $('#incident_action_modal').modal('hide');
        closeModal("#incident_action_modal");
      }).catch(err => {
        sAlert.error(bootConfirm.commonTranslation[err.reason])
      });
    }
  },
  'keyup #inputExtern': function(e, t) {
    $('[name=checkExtern]').prop('checked', e.currentTarget.value !== "");
  },
  'click li .timeSlot': function(event, template) {
    $('[name=time]').val(this.toString());
  },
  'click #selectRemindDay': function(e, t) {
    t.remindSelector.set('day');
  },
  'click #selectRemindHours': function(e, t) {
    t.remindSelector.set('hours');
  },
  'keyup #inputNotif, keydown #inputNotif, click #checkNotif-label': function(e, t) {
    if (e.currentTarget.getAttribute('id') === "checkNotif-label")
    $('[name=checkNotif]').prop('checked', !$('[name=checkNotif]').is(':checked'));
    else
    $('[name=checkNotif]').prop('checked', e.currentTarget.value !== "");
  },
  'click .msg-trigger': function(e, t) {
    e.preventDefault();
    $(e.currentTarget).toggleClass('on');
    $(e.currentTarget).parent().parent().next('tr.msg-box').toggle();
    $(e.currentTarget).parent().parent().toggleClass('no-border');
  },
  'input .check-option': function (e,t) {
    t.toValidate.set(!t.toValidate.get());
  }
});

Template.incident_gestionnaire_modal_action.helpers({
  declarerIsNotGone: (incident) => {
    let declarerId = incident.declarer.userId;
    return (GestionnaireUsers.findOne({_id: declarerId})
    || Meteor.users.findOne({_id: declarerId}));
  },
  getExcept: (incident) => {
    let list = GestionnaireUsers.find({}).fetch()
    let usersWithoutRight = []
    list.forEach((user) => {
      let hasRight = Meteor.userHasRight('incident', 'see', incident.condoId, user._id)
      if (!hasRight) {
        usersWithoutRight.push(user._id)
      }
    })

    if (incident.declarer.autoDeclare) {
      return usersWithoutRight;
    }
    usersWithoutRight.push(incident.declarer.userId)
    return usersWithoutRight;
  },
  getGestionnaireRemind: (incident) => {
    let priority = IncidentPriority.findOne(incident.info.priority);
    return getReminderValue(incident.condoId, incident.info.priority);
  },
  getTextAreaParams: (field, customId) => {
    let shareAll = Template.instance().action.shareAll;
    if (field === 'custom') {
      field = field + customId;
      return {
        fileManager: Template.instance().customs.list()[customId].fileManager,
        message: Template.instance().customs.list()[customId].message,
        name: 'area' + field,
        activeCB: (b) => {
          $('[name=check'+field+']').prop('checked', b);
        }
      };
    }
    return {
      fileManager: Template.instance().shareData[field].fileManager,
      message: Template.instance().shareData[field].message,
      name: 'area' + field,
      activeCB: (b) => {
        if (field === 'all' && shareAll)
        return;
        $('[name=check'+field+']').prop('checked', b);
        $('[name=check'+field+']').prop('disabled', b);
        $('[name=check'+field+']').parent().find('label').prop('style', 'cursor: not-allowed;');
      }
    };
  },
  incident: () => {
    let i = Incidents.findOne(Template.instance().id)
    if (i) {
      return i
    }
  },
  action: () => {
    return Template.instance().action;
  },
  dateToday: () => {
    let date = new Date();
    return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
  },
  isUndefinedHour: () => {
    return Template.instance().undefinedHour.get();
  },
  timeSlot: () => {
    let slots = [];
    for (let i = 0; i < 24; i++) {
      let hour = (i < 10) ? "0" + i.toString() : i.toString();
      slots.push(hour + ":" + "00");
      slots.push(hour + ":" + "15");
      slots.push(hour + ":" + "30");
      slots.push(hour + ":" + "45");
    }
    return slots;
  },
  getRemindSelector: () => {
    return Template.instance().remindSelector.get();
  },
  getNotifDefault: (i) => {
    let h = getReminderValue(i.condoId, i.info.priority);

    if (Template.instance().remindSelector.get() === "day")
    return Math.round(h / 24);
    else
    return h;
  },
  errorMessage: () => {
    return Template.instance().errorMessage.get();
  },
  getUsersManager: (index) => {
    return Template.instance().customs.list()[index].userManager;
  },
  customs: () => {
    // console.log('Template.instance().customs.list()', Template.instance().customs.list())
    return Template.instance().customs.list();
  },
  isCustomChecked: (custom) => {
    return custom.userManager.getUsersList().length !== 0;
  },
  showInterv: (t) => {
    return (t === "interv" || t === "OS");
  },
  roleToList: (condoId) => {
    let rolesId = CondosModulesOptions.findOne({condoId: condoId}).incident.managerActionVisibleBy || [];
    // console.log('rolesId', rolesId)
    return DefaultRoles.find({ _id: { $in: rolesId } }).fetch();
  },
  createShareData: (roleId) => {
    if (!Template.instance().shareData[roleId]) {
      Template.instance().shareData[roleId] = {
        fileManager: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
        message: {value: ""}
      }
    }
  },
  getIntervName: (data) => {
    let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
    let ret = "";
    if (data.extern_details == undefined || data.extern_intervention == undefined || data.keeper_intervention == undefined)
    return "<i>undefined</i>";
    if (data.keeper_intervention == true) {
      ret = translation.commonTranslation['keeper'];
    }
    if (data.extern_intervention == true) {
      // console.log(data);
      if (ret != "")
      ret += ", ";
      ret += data.extern_details;
    }
    return ret;
  },
  getPreviewFiles: (key, index) => {
    const t = Template.instance();
    return () => {
      if (key === 'custom') {
        return t.tempFiles.get()[key][index];
      }

      return t.tempFiles.get()[key];
    }
  },
  onFileAdded: (key, index) => {
    const t = Template.instance();

    return () => (files) => {
      if (key === 'custom') {
        $(`[name="checkcustom${index}"]`).prop('checked', true);

        let customFiles =  [];

        if (t.tempFiles.get()[key].length === 0) {
          customFiles = [files];
        } else {
          customFiles = _.reduce(t.tempFiles.get()[key], (tmp, a, k) => {
            if (key === 'custom' && k === index) {
              tmp.push([...a, ...files]);
            } else {
              tmp.push(a);
            }

            return tmp;
          }, [])
        }

        t.tempFiles.set({
          ...t.tempFiles.get(),
          [key]: customFiles
        })
      } else {
        $(`[name="check${key}"]`).prop('checked', true);

        t.tempFiles.set({
          ...t.tempFiles.get(),
          [key]: [...t.tempFiles.get()[key], ...files]
        })
      }

    }
  },
  onFileRemoved: (key, index) => {
    const t = Template.instance()

    return () => (fileId) => {
      if (key === 'custom') {
        const filteredFiles = _.filter(t.tempFiles.get()[key][index], (f) => f.fileId ? f.fileId !== fileId : f.id !== fileId);

        t.tempFiles.set({
          ...t.tempFiles.get(),
          [key]: _.reduce(t.tempFiles.get()[key], (tmp, a, k) => {
            if (k === index) {
              tmp.push(filteredFiles);
            } else {
              tmp.push(a);
            }

            return tmp;
          }, [])
        })
      } else {
        t.tempFiles.set({
          ...t.tempFiles.get(),
          [key]: _.filter(t.tempFiles.get()[key], (f) => f.fileId ? f.fileId !== fileId : f.id !== fileId)
        })
      }
    }
  },
  getDescriptionValue: (key, index) => {
    const t = Template.instance();

    return () => {
      return key === 'custom' ? t.messages.get()[key][index] : t.messages.get()[key]
    }
  },
  descriptionChange: (key, index) => {
    const t = Template.instance();

    return () => (value) => {
      if (key === 'custom') {
        $(`[name="checkcustom${index}"]`).prop('checked', value !== '');

        const custom = t.messages.get()[key].length === 0 ? [value] : _.reduce(t.messages.get()[key], (tmp, a, k) => {
          if (k === index) {
            tmp.push(value);
          } else {
            tmp.push(a);
          }
          return tmp;
        }, [])

        t.messages.set({
          ...t.messages.get(),
          [key]: custom
        })
      } else {
        $(`[name="check${key}"]`).prop('checked', value !== '');

        t.messages.set({
          ...t.messages.get(),
          [key]: value
        })
      }
    }
  },
  doNotValidate: () =>{
    Template.instance().toValidate.get();
    let all = $('[name=checkall]').is(':checked');
    let declarer = $('[name=checkdeclarer]').is(':checked');
    let intern = $('[name=checkintern]').is(':checked');
    let others = false;
    let res = true;
    $(".check-option").each(function() {
      let thisId = $(this).attr("id");
      if(thisId.indexOf('checkboxRedC') >= 0){
        others = others || $(this).is(':checked');
      }
    });
    let action = Template.instance().action;
    if (action.type==="cancel") {
      res = all||declarer||intern||others
    }
    return res; 
  },
  showInputOther: (index) => {
    Template.instance().toValidate.get();
    let res =false;
    $(".check-option").each(function() {
      let thisId = $(this).attr("id");
      let actual = 'checkboxRedC'+index;
      if(thisId === actual ){
        res =$(this).is(':checked');
      }
    });
    return res;
  },
  MbTextArea: () => MbTextArea,
});
