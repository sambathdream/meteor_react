import moment from 'moment';
import {UsersManager} from '/client/components/UsersManager/UsersManager.js';
import {FileManager} from '/client/components/fileManager/filemanager.js';
import { Session } from 'meteor/session';

function closeModal(modalId) {
	Meteor.setTimeout(function() {
		Session.set("modal", false);
	}, 500);
}


/*
** EVAL MODAL
*/
Template.incident_gestionnaire_modal_eval.onCreated(function() {
	this.id = FlowRouter.getParam("incidentId");
});

Template.incident_gestionnaire_modal_eval.onRendered(function () {
	$('#incident_eval_modal').modal('show');
});

Template.incident_gestionnaire_modal_eval.events({
	'click .closeModal': function(e, t) {
		closeModal('#incident_eval_modal');
	}
});

Template.incident_gestionnaire_modal_eval.helpers({
	getIncidentEvals: () => {
		let i = Incidents.findOne(Template.instance().id);
		if (i)
			return i.eval;
		else
			console.log("MongoError: Can't find incident '"+Template.instance().id+"'");
	},
	getEvalUsername: (eval) => {
		let u = GestionnaireUsers.findOne(eval.userId);
		if (u)
			return u.profile.firstname + " " + u.profile.lastname;
		else
			console.log("MongoError: Can't find user '"+eval.userId+"' of eval");
	},
	evalValue: (eval, indexStar) => {
		return (indexStar <= eval.value);
	}
});

