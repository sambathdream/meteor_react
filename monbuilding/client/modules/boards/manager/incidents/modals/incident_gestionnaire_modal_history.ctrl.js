import moment from 'moment';
import {UsersManager} from '/client/components/UsersManager/UsersManager.js';
import {FileManager} from '/client/components/fileManager/filemanager.js';
import { Session } from 'meteor/session';
import { CommonTranslation } from "/common/lang/lang";

import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';

function closeModal(modalId) {
	Meteor.setTimeout(function() {
		Session.set("modal", false);
	}, 500);
}

Template.incident_gestionnaire_modal_history.onCreated(function () {
	this.id = FlowRouter.getParam("incidentId");
	this.item = this.data.item;
	this.autorun(function () {
		Meteor.subscribe("module_incidents_gestionnaire");
	});
});

Template.incident_gestionnaire_modal_history.onRendered(function () {
	$('#incident_history_modal').modal({
		show: true,
		keyboard: false,
		backdrop: 'static'
	});
});

Template.incident_gestionnaire_modal_history.events({
	'show.bs.modal #incident_history_modal': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#incident_history_modal";
		window.onhashchange = function() {
			if (location.hash != "#incident_history_modal"){
				modal.modal('hide');
			}
		}
	},
  'shown.bs.modal #incident_history_modal': function() {
    window.dispatchEvent(new Event('resizePhotosetGrid'));
  },
	'hidden.bs.modal #incident_history_modal': function(event, template) {
		history.replaceState('', document.title, window.location.pathname);
		Session.set("modal", false);
	},
	'click #goMessagerie': function(e, t) {
		$('#incident_history_modal').modal('hide');
		$('.modal-backdrop').remove();
		$('body').removeClass('modal-open');
    let msgId = e.currentTarget.getAttribute("msgId");
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.messenger.detail', { lang, tab: 'enterprise', msgId });
	}
});

Template.incident_gestionnaire_modal_history.helpers({
	checkTypeInterv: (type) => {
		if (type == 6 || type == 7 || type == 11)
			return true;
		return false;
	},
	getFilePath: (id) => {
		let file = IncidentFiles.findOne(id);
		if (!file)
			file = UserFiles.findOne(id);
		if (file)
			return file.link().replace("localhost", location.hostname);
	},
	getFileName: (id) => {
		let file = IncidentFiles.findOne(id);
		if (!file)
			file = UserFiles.findOne(id);
		if (file)
			return file.name;
	},
	getFiles: (ids, isMessenger) => {
    let files = null
    if (isMessenger === true) {
      files = MessengerFiles.find({ _id: { $in: ids } });
      if (!files.fetch().length) {
        files = UserFiles.find({ _id: { $in: ids } });
      }
    } else {
      files = IncidentFiles.find({ _id: { $in: ids }});
      if (!files.fetch().length)  {
        files = UserFiles.find({ _id: { $in: ids }});
      }
    }
		if (files)
			return files;
	},
	getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
	getMessengerFiles: (ids) => {
		let files = MessengerFiles.find({ _id: { $in: ids }});
		if (files)
			return files;
	},
	item: () => {
		return Template.instance().item;
	},
	modalShowShare: (share) => {
		if (share)
			return (share.comment !== "" || share.files.length);
	},
	getCustomShareRole: (share, roleId) => {
		if (share && share[roleId])
			return (share[roleId]);
	},
	getNbCustom: (type, i) => {
		if (type === "users")
			return _.reduce(i.share.custom, function (ret, e)
				{return ret + e.users.length}, 0);
		if (type === "messages")
			return _.reduce(i.share.custom, function (ret, e)
				{return ret + (e.comment !== "" ? e.users.length : 0)}, 0);
	},
	getUsername: (userId) => {
		let u = GestionnaireUsers.findOne(userId);
		return (u.profile.firstname[0] + ". " + u.profile.lastname);
	},
	displayDetails: (item) => {
    // let lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"))
    if (item.intervenantKeeper || item.intervenantOther) {
      let intervStr = translation.commonTranslation['technician'] + ' : '
      if (item.intervenantKeeper) {
        intervStr += translation.commonTranslation['keeper'] + ' ' + (item.intervenantOther ? ", " : "")
      }
      if (item.intervenantOther) {
        intervStr += (item.intervenantName !== "" ? "'" + item.intervenantName + "'" : translation.commonTranslation['other_technician'])
      }
      return intervStr

    } else {
      if (item.details && item.details != "") {
        if (item.detailsIsDate) {
          let dateDetail = (item.undefinedHour == true ? moment(item.detailsDate).format("DD/MM/YYYY") : moment(item.detailsDate).format("DD/MM/YYYY") + ' ' + translation.commonTranslation['at'] + ' ' + moment(item.detailsDate).format('HH:mm'));
          return translation.commonTranslation['intervention_scheduled'] + dateDetail + translation.commonTranslation['was_canceled']
        }
        return item.details
      } else if (item.date_interv && item.date_interv != "") {
        return translation.commonTranslation['programmed_the'] + ' ' + moment(item.date_interv).format("DD/MM/YYYY" + (item.undefinedHour ? "" : ' [' + translation.commonTranslation['at'] + '] ' +  moment(item.date_interv).format('HH:mm')))
      }
    }
	},
	// addModaleCb: () => {
	// 	let template = Template.instance();
	// 	return (condoId) => {

	// 	}
	// },
	getUserName: (userId) => {
		let ret = "-";
		let user = GestionnaireUsers.findOne(userId);
		if (user && user != undefined) {
			return user.profile.firstname + " " + user.profile.lastname;
		}
		user = Meteor.users.findOne(userId);
		if (!user || user == undefined)
			return ret;
		else {
			return user.profile.firstname + " " + user.profile.lastname;
		}
	},
  MbFilePreview: () => MbFilePreview,
});
