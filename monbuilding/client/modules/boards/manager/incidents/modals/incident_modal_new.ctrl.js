import moment from 'moment';
import {UsersManager} from '/client/components/UsersManager/UsersManager.js';
import {FileManager} from '/client/components/fileManager/filemanager.js';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import { Session } from 'meteor/session';

function closeModal(modalId) {
	Meteor.setTimeout(function() {
		Session.set("modal", false);
	}, 500);
}

/*
** NEW INCIDENT MODAL
*/
Template.incident_modal_new.onCreated(function() {
	this.errorMessage = new ReactiveVar("");
	this.condoId      = new ReactiveVar(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || null);
	this.condoSearch  = new ReactiveVar("");
	this.typeSelected = new ReactiveVar("");
	this.length       = new ReactiveVar(0);
	this.prioSelected = new ReactiveVar(-1);
	this.zoneSelected = new ReactiveVar(-1);
	this.textarea = {
		fm: new FileManager(IncidentFiles, {userId: Meteor.userId(), tmp: true}),
		msg: {value: ''}
	};
	this.board = this.data.board;

	this.description = new ReactiveVar('');
	this.files = new ReactiveVar([]);
	this.existingFiles = new ReactiveVar([]);
});

Template.incident_modal_new.onRendered(function() {
	/*$('#newIncident_modal').modal({*/
		/*  show: true,*/
		/*  keyboard: false,*/
		/*  backdrop: 'static'*/
		/*});*/
});

Template.incident_modal_new.events({
	'show.bs.modal #newIncident_modal': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#newIncident_modal";
		window.onhashchange = function() {
			if (location.hash != "#newIncident_modal"){
				modal.modal('hide');
			}
		}

		template.condoId.set(Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id") || null)
	},
	'hidden.bs.modal #newIncident_modal': function(event, template) {
		emptyFormByParentId('newIncident_modal');
		$('.confirmAction').button('reset')
		template.errorMessage.set('');
		$('.errorModalInput').removeClass('errorModalInput');
		template.length.set(0);
		template.description.set('');
		template.typeSelected.set(null)
		template.prioSelected.set(null)
		template.zoneSelected.set(null)
		if ($('#attachments li')[0]) {
			$('#attachments li').remove();
		}
		history.replaceState('', document.title, window.location.pathname);
		Session.set("modal", false);
	},
	'click .confirmAction': function(e, t) {
		$('.confirmAction').button('loading')
		let condoId = t.condoId.get();

		if (!condoId || condoId == '') {
			$('.confirmAction').button('reset')
			t.errorMessage.set('Merci de renseigner un immeuble.');
			return ;
		}
		if ($('[name=detailRadio]').is(':checked') == false) {
			$('.confirmAction').button('reset')
			t.errorMessage.set('Merci de renseigner un type.');
			return ;
		}
		if ($('[name=priorityRadio]').is(':checked') == false) {
			$('.confirmAction').button('reset')
			t.errorMessage.set('Merci de renseigner une priorité.');
			return ;
		}
		if ($('[name=zoneRadio]').is(':checked') == false) {
			$('.confirmAction').button('reset')
			t.errorMessage.set('Merci de renseigner une zone.');
			return ;
		}
		let incidentType = IncidentType.findOne({condoId: condoId, defaultType: "incident"});
		if (!incidentType)
			incidentType = IncidentType.findOne({condoId: "default", defaultType: "incident"});
		let incident = {
			"declarer": {
				"phone": "",
			},
			"info": {
				"priority": $('[name=priorityRadio]:checked').val(),
				"type": $('[name=detailRadio]:checked').val(),
				"zone": $('[name=zoneRadio]:checked').val(),
				"title": $('[name=title]').val(),
				"incidentTypeId": incidentType._id,
				"comment": t.description.get(),
			}
		};

    if (incident.info.title === "") {
      $('[name=title]').addClass('errorModalInput');
      $('.confirmAction').button('reset')
      t.errorMessage.set('Merci de renseigner un titre.');
      return ;
    }

    const __files = t.files.get();

    t.textarea.fm.batchUpload(__files)
      .then(results => {
        Meteor.call('publishIncident', condoId, incident, t.board, _.map(results, (f) => f._id), function(error, result) {
          $('.confirmAction').button('reset')
          if (!error) {
            t.files.set([])
            $('#newIncident_modal').modal('hide');
            closeModal('newIncident_modal');
          }
          else {
            sAlert.error(error);
          }
        });
      }).catch((err) => {
        t.errorMessage.set('Files upload error.');
        $('.confirmAction').button('reset');
      });
	},
	'click .cancelAction': function(e, t) {
		$('.confirmAction').button('reset')
		closeModal('newIncident_modal');
	},
	'click input[name=detailRadio]': function(e, t) {
		let value = e.currentTarget.getAttribute('value');
		t.typeSelected.set(value);
	},
	'click input[name=priorityRadio]': function(e, t) {
		let value = e.currentTarget.getAttribute('value');
		t.prioSelected.set(value);
	},
	'click input[name=zoneRadio]': function(e, t) {
		let value = e.currentTarget.getAttribute('value');
		t.zoneSelected.set(value);
	},
	'input #sujet': function (event, template) {
		if ($('#sujet').val().length > 50) {
			event.preventDefault();
			$('#sujet').val($('#sujet').val().substring(0, 50));
		}
		else
			template.length.set($('#sujet').val().length);
	},
	'input #answer': function (event, template) {
		template.description.set($('#answer').val());
	},
});

Template.incident_modal_new.helpers({
	getTextAreaParams: () => {
		return {
			fileManager: Template.instance().textarea.fm,
			message: Template.instance().textarea.msg,
			name: 'area_new',
			activeCB: (b) => {
				;
			}
		};
	},
	currentCondo: () => {
		let condoId = Template.instance().condoId.get();
		let condo = Condos.findOne(condoId);
		if (condo && condo.info.id != -1) {
			if (condo.name == condo.info.address)
				return condo.info.id + ' - ' + condo.info.address + ", " +  condo.info.code + " " + condo.info.city;
			else
				return condo.info.id + ' - ' + condo.name;
		}
		else if (condo) {
			return condo.getName();
		}
	},
	gestCondos: () => {
		let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
		return (Condos.find({$or : [{name: regexp}, {"info.id": regexp}, {"info.address": regexp}, {"info.code": regexp}, {"info.city": regexp}], "settings.options.incidents": true}).fetch());
	},
	Condos: () => {
		return Condos.find().fetch();
	},
	isActiveType: (type) => {
		return type === Template.instance().typeSelected.get();
	},
	isActivePrio: (prio) => {
		return prio === Template.instance().prioSelected.get();
	},
	isActiveZone: (zone) => {
		return zone === Template.instance().zoneSelected.get();
	},
	errorMessage: () => {
		return Template.instance().errorMessage.get();
	},
	board: () => {
		return Template.instance().board;
	},
	nbChar: () => {
		return (50 - Template.instance().length.get());
	},
	CDD_cursor: () => {
	  const canSee = Meteor.listCondoUserHasRight("incident", "see");
	  const canAdd = Meteor.listCondoUserHasRight("incident", "autodeclare");

		return _.filter(canSee, (c) => canAdd.includes(c));
	},
	addModaleCb: () => {
		let template = Template.instance();
		return (condoId) => {
			template.condoId.set(condoId);
		}
	},
	getSelectedCondo: () => {
		return Template.instance().condoId.get();
	},
	isCondoSelected: () => {
    return Template.instance().condoId.get() !== null && Template.instance().condoId.get() !== '' && Template.instance().condoId.get() !== 'all';
	},
	cantSendIncident: () => {
		let typeSelected = Template.instance().typeSelected.get()
		let prioSelected = Template.instance().prioSelected.get()
		let zoneSelected = Template.instance().zoneSelected.get()
		let sujetLength = Template.instance().length.get()
		let textarea = Template.instance().description.get()
		if (sujetLength && sujetLength > 0 && typeSelected && prioSelected && zoneSelected && textarea && textarea.trim() !== '')
			return false
		return true
	},
  MbTextArea: () => MbTextArea,
  getDescriptionValue: () => {
	  return Template.instance().description.get();
  },
  descriptionChange: () => {
	  const t = Template.instance();
	  return (value) => {
	    t.description.set(value);
    }
  },
  getPreviewFiles: () => {
    let fileIds = Template.instance().existingFiles.get()
		const reactiveFiles = Template.instance().files.get()

		const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onFileAdded: () => {
    const tpl = Template.instance();

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      tpl.existingFiles.set(_.filter(tpl.existingFiles.get(), f => f.fileId ? f.fileId !== fileId : f !== fileId))
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
});

