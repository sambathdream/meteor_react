import { Session } from 'meteor/session';
import { CommonTranslation } from "/common/lang/lang";

var bool_message = true;

Template.incident_gestionnaire_elem.onCreated(function() {
	let currentContext = FlowRouter.current();

	this.id = FlowRouter.getParam("incidentId");
	if (!this.id) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go("app.errorAccessDenied", { lang });
  }

	Meteor.call('updateLastViewIncident', this.id, true);

	Session.set('modal', false);
	this.modalData = {};
});

Template.incident_gestionnaire_elem.onRendered(function () {
	setTimeout(function () {
		recalculateHeighFirst();
	}, 500);
	$(window).on('resize', function(){
		recalculateHeigh();
	});
});

let maxHeight = function(elems) {
	return Math.max.apply(null, elems.map(function ()
	{
		return $(this).height();
	}).get());
};

function recalculateHeighFirst() {
	let elem = $(".equal-height-elem");
	let height = maxHeight(elem);

	elem.height(height);
}


function recalculateHeigh() {
	let elem = $(".equal-height-elem");
	elem.height("100%");
	let height = maxHeight(elem);

	elem.height(height);
}


Template.incident_gestionnaire_elem.events({
	'click #pjDispo' (e, t) {
		let i = Incidents.findOne(t.id);
		t.modalData = {"item": i.history[i.history.length - 1]};
		Session.set('modal', 'incident_history');
	},
	'click #backbutton' (e, t) {
		let current = FlowRouter.current();
		FlowRouter.go("app.gestionnaire.incidents", current.params);
	},
	'click .detailsEval' (e, t) {
		Session.set('modal', 'incident_eval');
	},
	'click .historyitem' (e, t) {
		let id = parseInt(e.currentTarget.getAttribute("index"));
		let i = Incidents.findOne(t.id);
		t.modalData = {"item": i.history[id], "condoId": i.condoId};
		Session.set('modal', 'incident_history');
	},
	'click #incidentReopen' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["reopen_incident"],
				type: "default",
				remind: true,
				shareall: true,
				cb: function(data, notifHours) {
					Meteor.call('reopenIncident', t.id, data, notifHours);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentValid' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["validate_incident"],
				type: "valid",
				remind: true,
				shareall: true,
				cb: function(data, notifHours = -1) {
					Meteor.call('validIncident', t.id, data, notifHours);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
  },
  'click .goToPrivateIncident': (e, t) => {
    $('#incident_action_modal').modal('hide')
    setTimeout(() => {
      let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
      t.modalData = {
        action: {
          title: translation.commonTranslation["not_validate_incident"],
          type: "cancel",
          remind: false,
          shareall: false,
          cb: function (data) {
            Meteor.call('cancelIncident', t.id, data);
            setTimeout(() => recalculateHeigh(), 500)
          }
        }
      };
      Session.set('modal', 'incident_action');
    }, 700);

  },
	'click #incidentCancel' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["not_validate_incident"],
				type: "cancel",
				remind: false,
				shareall: false,
				cb: function(data) {
					Meteor.call('cancelIncident', t.id, data);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentSolved' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["solved_incident"],
				type: "default",
				remind: false,
				shareall: true,
				cb: function(data) {
					Meteor.call('solvedIncident', t.id, data);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentComment' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["add_note"],
				type: "default",
				remind: false,
				shareall: false,
				cb: function(data, notifHours) {
					Meteor.call('commentIncident', t.id, data, notifHours);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentOS' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["add_intervention"],
				type: "OS",
				remind: true,
				shareall: true,
				cb: function(data, notifHours, interv) {
					Meteor.call('osIncident', t.id, data, notifHours, lang);
					if (interv) {
						interv.keeper_intervention = data.keeper_intervention,
						interv.extern_intervention = data.extern_intervention,
						interv.extern_details      = data.extern_details,
						Meteor.call('intervIncident', t.id, interv, notifHours);
						setTimeout(() => recalculateHeigh(), 500)
					}
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentInterv' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["add_intervention"],
				type: "interv",
				remind: true,
				shareall: true,
				cb: function(data, notifHours) {
					Meteor.call('intervIncident', t.id, data, notifHours);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentCancelInterv' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["cancel_intervention"],
				type: "cancelInterv",
				remind: true,
				shareall: true,
				cb: function(data, notifHours) {
					Meteor.call('cancelIntervIncident', t.id, data, notifHours, lang);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};
		Session.set('modal', 'incident_action');
	},
	'click #incidentDevis' (e, t) {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		t.modalData = {
			action: {
				title: translation.commonTranslation["add_quote"],
				type: "default",
				remind: true,
				shareall: false,
				cb: function(data, notifHours) {
					Meteor.call('devisIncident', t.id, data, notifHours);
					setTimeout(() => recalculateHeigh(), 500)
				}
			}
		};

		Session.set('modal', 'incident_action');
	}
});

Template.incident_gestionnaire_elem.helpers({
	checkStatusCancelOrResolved: (status, cancel, resolved) => {
		if (status == cancel || status == resolved)
			return true;
		return false;
	},
	checkHistoryFileDeclare: () => {
		let i = Incidents.findOne(Template.instance().id);
		if (i) {
			let firstHistory = i.history[i.history.length - 1];
			if (firstHistory.share.declarer.files.length > 0)
				return true;
			return false;
		}
		return false;
	},
	getIncidentEvals: () => {
		let i = Incidents.findOne(Template.instance().id);
		if (i)
			return i.eval;
	},
	
	avatarLink : (userId) => {
    let res = { 
      src:'',
      hasPicto: false,
      ini: '-',
      name: '-'
    };
    
		if (_.isObject(userId)){
      userId = userId.userId;
      let u = GestionnaireUsers.findOne(userId);
      if (u){
        res.name = u.profile.firstname + " " + u.profile.lastname;
        res.ini = u.profile.firstname[0] +""+ u.profile.lastname[0];
      }
		  else {
			  u = Meteor.users.findOne(userId);
			  if(u){
          res.name = u.profile.firstname + " " + u.profile.lastname;
          res.ini = u.profile.firstname[0] +""+ u.profile.lastname[0];
        } 
		  }
      let avatar = Avatars.findOne({userId: userId});

		  if (avatar && avatar != undefined) {
        res.src = avatar.avatar.original;
			  res.hasPicto = true;
	    }
    }
		return res;
	},
	getEvalBuilding: (eval) => {
		let res = '-';
		let u = GestionnaireUsers.findOne(eval.userId);
		if(u){
			let c = Condos.findOne({'_id':u.lastCondoId});
			res = c.name;
		}
		return res;
	},
	evalValue: (eval, indexStar) => {
		return (indexStar <= eval.value);
	},
	greaterThanZero: function(v) {
		return (v > 0);
	},
	incident: () => {
		return Incidents.findOne(Template.instance().id);
	},
	modalItem: () => {
		let id = Template.instance().selectedItem.get();
		if (id === -1)
			return {};
		let elem = Incidents.findOne(Template.instance().id);
		return elem.history[id];
	},
	getAddress: (i) => {
		if (!i)
			return ;
		let condo = Condos.findOne(i.condoId);
		if (condo && condo.info.id != -1)
			return condo.info.id + ' - ' + condo.getName();
		else
			return condo.getName();
	},
	getNbFiles: (i) => {
		let result = _.reduce(i.share, function (ret, e) {
						if (e.files)
							return ret + e.files.length;
						else
							return ret;
					}, 0);
		result += _.reduce(i.share.custom, function (ret, e) {
						return ret + e.files.length;
					}, 0);

		return result;
	},
	hasInterv: (i) => {
		for (d of i.state.intervention) {
			if (d.active)
				return true;
		}
		return false;
	},
	getDeclarerStatus: (i) => {
		let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
		if (!i)
			return ;
		if (i.declarer.autoDeclare)
			return translation.commonTranslation['manager'];
		// const rights = UsersRights.findOne({
    //   'condoId': i.condoId,
    //   'userId': i.declarer.userId
    // })
    // if (!!rights) {
    //   let roleOfUser = DefaultRoles.findOne(rights.defaultRoleId)
    //   if (!!roleOfUser)
    //     return roleOfUser.name
    // }
    //
    // return ''

		let user = GestionnaireUsers.findOne({_id : i.declarer.userId});
		if (user) {
			let condo = _.find(user.resident[0].condos, function(c) {return c.condoId === i.condoId});
			let status = "";
			for (b of condo.buildings) {
				status =  Meteor.getBuildingStatusValue(b.type, true, null);
			}
			return status;
		}
		else
			return  translation.commonTranslation['done_by'] ;
	},
	evalMoyenne: (i, indexStar) => {
		let acc = 0;
		let len = 0;
		for (e of i.eval) {
			if (e.value !== 0) {
				acc += e.value;
				len++;
			}
		}
		return (indexStar <= Math.round(acc/len));
	},
	evalNbVote: (i) => {
		return _.reduce(i.eval, function(acc, e){return acc + (e.value !== 0);}, 0);
	},
	hasModal: () => {
		return Session.get("modal") !== false;
	},
	getModalTemplate: () => {
		let modal = Session.get("modal");
		if (modal) {
			if (modal === "incident_history")
				return "incident_gestionnaire_modal_history";
			if (modal === "incident_eval")
				return "incident_gestionnaire_modal_eval";
			if (modal === "incident_action")
				return "incident_gestionnaire_modal_action";
		}
	},
	getModalData: () => {
		return Template.instance().modalData;
	},
	getNbCustom: (type, i) => {
		if (type === "users")
			return _.reduce(i.share.custom, function (ret, e)
				{return ret + e.users.length}, 0);
		if (type === "messages")
			return _.reduce(i.share.custom, function (ret, e)
				{return ret + (e.comment !== "" ? e.users.length : 0)}, 0);
	},
	isMessageItem: (title) => {
		return title == 12;
	},
	isReply: (reply) => {
		return !reply;
	},
	getDeclarerProfile: (declarer) => {
		if (!declarer || declarer == undefined)
			return;
		let ret = {firstname: "-", lastname: "-", phone: "-", email: "-"};
		let user = GestionnaireUsers.findOne(declarer.userId);
		if (user && user != undefined) {
			let avatar = Avatars.findOne({userId: user._id});
			ret.uid = user._id;
			ret.ini = user.profile.firstname.charAt(0) + user.profile.lastname.charAt(0);
			ret.hasPicture = false;
			ret.avatarURL = null;
			if(avatar && avatar !== undefined && avatar !== null){
				ret.avatarURL = avatar.avatar.original;
				ret.hasPicture = true;
			} 
			ret.firstname = user.profile.firstname;
			ret.lastname = user.profile.lastname;
			ret.email = user.emails[0].address;
			ret.landline = (user.profile.tel2 ? `+${user.profile.tel2Code} ${user.profile.tel2}` : (declarer.landline ? declarer.landline : ""));
			ret.phone = (user.profile.tel ? `+${user.profile.telCode} ${user.profile.tel}`: (declarer.phone ? declarer.phone : "-"));
			return ret;
		}
		user = Meteor.users.findOne(declarer.userId);
		if (!user || user == undefined)
			return ret;
		else {
			let avatar = Avatars.findOne({userId: user._id});
			if(avatar && avatar !== undefined && avatar !== null){
				ret.avatarURL = avatar.avatar.original;
				ret.hasPicture = true;
			} else {
				ret.avatarURL = null;
				ret.hasPicture = false;
			}
			ret.ini = user.profile.firstname.charAt(0) + user.profile.lastname.charAt(0);
			ret.firstname = user.profile.firstname;
			ret.lastname = user.profile.lastname;
			ret.email = user.emails[0].address;
      ret.landline = (user.profile.tel2 ? `+${user.profile.tel2Code} ${user.profile.tel2}` : (declarer.landline ? declarer.landline : ""));
			ret.phone = (user.profile.tel ? `+${user.profile.telCode} ${user.profile.tel}` : (declarer.phone ? declarer.phone : "-"));
			return ret;
		}
	},
	getUserName: (userId) => {
		let ret = "-";
		let user = GestionnaireUsers.findOne(userId);
		if (user && user != undefined) {
			return user.profile.firstname + " " + user.profile.lastname;
		}
		user = Meteor.users.findOne(userId);
		if (!user || user == undefined)
			return ret;
		else {
			return user.profile.firstname + " " + user.profile.lastname;
		}
	},
	getEvalDate: (eval) => {
		return eval.date !== null ? eval.date.toLocaleDateString() : void 0;
	},
	getUserInfo: (userId) => {
		let ret = { hasPicture: false};
		let user = Meteor.users.findOne(userId);
		if(!user && user === undefined){
			user = GestionnaireUsers.findOne(userId);
		}
		ret.src = null;
		if (user && user !== undefined) {
			if(user.avatar !== null){
				let avatar = Avatars.findOne({userId: user._id});
				if(avatar && avatar !== undefined && avatar !== null){
					ret.src = avatar.avatar.original;
					ret.hasPicture = true;
				} 
			} 
			ret.ini = user.profile.firstname.charAt(0) + user.profile.lastname.charAt(0);
		} 
		return ret;
	},
});
