import { Template } from 'meteor/templating'
import {Pagination} from '/client/components/pagination/pagination.js';

function activeCondo() {
	let incidentEnable = _.map(Condos.find({'settings.options.incidents': true}).fetch(), function(c) {return c._id});
  let incidentAvailable = Meteor.listCondoUserHasRight("incident", "see");
	return _.intersection(incidentEnable, incidentAvailable)
}

const regexpSearch = {
	'a': '[a|à|â|ä]',
	'e': '[e|é|è|ê|ë]',
	'i': '[i|î|ï|ì]',
	'o': '[o|ô|ö|ò]',
	'u': '[u|ù|ü|û]',
	'y': '[y|ý|ŷ|ÿ|ỳ]'
}
Template.incidents_select_tab.onCreated(function () {
  Session.set('modal', false);
  let currentContext = FlowRouter.current();
  const activeCondoList = activeCondo()
	this.tables = [
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 0, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 1, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 2, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 3, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
	];
	this.tables[0].pagination.setQuery(this.tables[0].query, {sort: {"lastUpdate": -1}});
	this.tables[1].pagination.setQuery(this.tables[1].query, {sort: {"lastUpdate": -1}});
	this.tables[2].pagination.setQuery(this.tables[2].query, {sort: {"lastUpdate": -1}});
	this.tables[3].pagination.setQuery(this.tables[3].query, {sort: {"lastUpdate": -1}});
	this.searchText = new ReactiveVar('')
	this.condoSelected = new ReactiveVar('all');
	this.selectedTab = new ReactiveVar('toValidate');
})

Template.incidents_select_tab.onRendered(function() {
	let template = Template.instance();
	/*setTimeout(function() {
	  let condoList = Meteor.listCondoUserHasRight("incident", "see");
	 	template.condoSelected.set(condoList.length == 1 ? Condos.findOne(condoList[0])._id : "all");
	 }, 200)*/
	setTimeout(function() {
    const activeCondoList = activeCondo()
		template.tables[0].query.condoId = {$in: activeCondoList};
		template.tables[1].query.condoId = {$in: activeCondoList};
		template.tables[2].query.condoId = {$in: activeCondoList};
		template.tables[3].query.condoId = {$in: activeCondoList};
	}, 400)
});

let maxHeight = function(elems) {
	return Math.max.apply(null, elems.map(function ()
	{
		return $(this).height();
	}).get());
};

function recalculateHeigh() {
	let elem = $(".equal-height");
	let height = maxHeight(elem);

	elem.height(height);
	$(".equal-height-scroll").height(height);
}

Template.incidents_select_tab.helpers({
	getCondoSelected: () => {
		return Template.instance().condoSelected.get()
	},
  isSelectedTab: (data) => {
		return Template.instance().selectedTab.get() === data;
	},
  getDefaultRoles: () => Template.currentData().getDefaultRole,
  getSelectedTab: () => Template.currentData().currentTab,

  refreshCondoId: () => {
		const condoId = Template.currentData().selectedCondoId
		const template = Template.instance()
    if (condoId !== template.condoSelected.get()) {
    	template.condoSelected.set(condoId);
    	let i = 0;
    	for (tab of template.tables) {
        let q = {
          "state.status": i,
          condoId: {$in: condoId === 'all' ? activeCondo() : [condoId]}
        }
        tab.pagination.setPage(1);
    		tab.pagination.setQuery(q, tab.pagination.option.get());
    		i++;
    	}
		}
    return true
	},


  isSortBy: (state, field) => {
		return Template.instance().tables[state].sort.get().field == field;
	},
	isSortUp: (state) => {
		return Template.instance().tables[state].sort.get().state == 1;
	},
	getPaginationByState: (state) => {
		return Template.instance().tables[state].pagination;
	},
	emptyIncidentByState: (state) => {
    let q = {"state.status": state};
		if (Template.instance().condoSelected.get() != "all") {
			q.condoId = Template.instance().condoSelected.get();
		}
		else {
			q.condoId = {$in: activeCondo()};
		}
		let incidents = Incidents.find(q).fetch();
		incidents = incidents.filter((i) => {
			return Meteor.userHasRight('incident', 'see', i.condoId, Meteor.userId());
		})
		return incidents.length == 0;
	},
	incidentByState: (state) => {
		return Template.instance().tables[state].pagination.getResults().fetch();
	},
	getAddress: (i) => {
		let condo = Condos.findOne(i.condoId);
		if (condo && condo.info.id != -1)
			return condo.info.id + ' - ' + condo.getName();
		else
			return condo.getName();
	},
	getItemClass: (i) => {
		if (!i.view.length || !i.view[0].lastView)
			return "new_item";
		if (i.view[0].lastView < i.lastUpdate)
			return "update_item";
		return "";
	},
	hasUnreadMessage: (incidentHistory) => {
		if (!incidentHistory)
			return false;
		return _.find(incidentHistory, function(elem) {
			if (elem.title == 12 && (elem.isReply == undefined || elem.isReply == ""))
				return true;
		})
	},
	canDeclare: () => {
    const selectedCondo = Template.instance().condoSelected.get()
    if (selectedCondo === 'all') {
      let condoIdCanDeclare = Meteor.listCondoUserHasRight("incident", "autodeclare");
      const condosId = condoIdCanDeclare.filter((condoId) => {
        return Meteor.userHasRight('incident', 'autodeclare', condoId, Meteor.userId());
      })
      return condosId && condosId.length > 0;
    } else {
      return Meteor.userHasRight('incident', 'autodeclare', selectedCondo, Meteor.userId());
    }
	},
	showModal: () => {
		return () => $('#newIncident_modal').modal('show');
	},
	searchCallback: () => {
		let template = Template.instance();
		const condoId = Template.currentData().selectedCondoId;
    return (value) => {
			let i = 0;
    	for (tab of template.tables) {
				let q ={};
				if(value && value.length > 0){
					q = {
						"state.status": i,
						condoId: {$in: condoId === 'all' ? activeCondo() : [condoId]},
						'info.title' : new RegExp(value, "i")
					}
				} else {
					q = {
						"state.status": i,
						condoId: {$in: condoId === 'all' ? activeCondo() : [condoId]}
					}
				}
    		template.tables[i].pagination.setQuery(q, {sort: {"lastUpdate": -1}});
    		i++;
    	}
    }
  }
})

Template.incidents_select_tab.events({
  'click .tabBarContainer > div': function (event, template) {
    let selectingTab = template.data.selectingTab
    let newTab = event.currentTarget.getAttribute('data-tab')
    if (selectingTab && typeof selectingTab === 'function') {
      selectingTab(newTab)
    }
  },
  'click .sortBy' (e, t) {
		const condoId = Template.currentData().selectedCondoId;
		const id = parseInt(e.currentTarget.getAttribute("state"));
		const sort = e.currentTarget.getAttribute("sort");
		if (t.tables[id].sort.get().field == sort) {
			let s = t.tables[id].sort.get().state;
			t.tables[id].sort.set({field: sort, state: s * -1});
		}
		else
			t.tables[id].sort.set({field: sort, state: -1});
		let sortObj = {sort: {}};
		if (sort == "date")
			sortObj.sort = {"lastUpdate": t.tables[id].sort.get().state};
		if (sort == "type")
			sortObj.sort = {"info.type": t.tables[id].sort.get().state};
		if (sort == "zone")
			sortObj.sort = {"info.zone": t.tables[id].sort.get().state};
		if (sort == "title")
			sortObj.sort = {"info.title": t.tables[id].sort.get().state,};
		let q = {
			"state.status": id,
			condoId: {$in: condoId === 'all' ? activeCondo() : [condoId]}
		}

		t.tables[id].pagination.setQuery(q, sortObj);

	},
	'click .elem': function(e, t) {
		let id = e.currentTarget.getAttribute("elemId");
		const lang = FlowRouter.getParam("lang") || "fr";
		FlowRouter.go("app.gestionnaire.incidents.id", { lang, incidentId: id });
		setTimeout(function () {
			recalculateHeigh();
		}, 50);
	},
})

