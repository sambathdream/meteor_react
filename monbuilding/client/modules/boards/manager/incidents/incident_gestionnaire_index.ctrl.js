Template.incident_gestionnaire_index.onCreated(function() {
  this.state = new ReactiveVar("list");
  let self = this;

  this.subsHandler = Meteor.subscribe("module_incidents_gestionnaire");
  this.subscribe('messagerieGestionnaireFiles');


  this.autorun(function () {
    FlowRouter.watchPathChange();
    let currentContext = FlowRouter.current();

    if (FlowRouter.getParam("incidentId"))
      self.state.set("elem");
    else
      self.state.set("list");
  });
  Meteor.call('setNewAnalytics', {type: "module", module: "incident", accessType: "web", condoId: ''});
});

Template.incident_gestionnaire_index.onDestroyed(function() {
  // Meteor.call('updateAnalytics', {type: "module", module: "incident", accessType: "web", condoId: ''});
})

Template.incident_gestionnaire_index.onRendered(function () {
    setTimeout(function () {
        recalculateHeigh();
    }, 500);
    $(window).on('resize', function(){
        recalculateHeigh();
    });
});

let maxHeight = function(elems) {
    return Math.max.apply(null, elems.map(function ()
    {
        return $(this).height();
    }).get());
};

function recalculateHeigh() {
    let elem = $(".equal-height");
    let height = maxHeight(elem);

    elem.height(height);
    $(".equal-height-scroll").height(height);
}

Template.incident_gestionnaire_index.events({

});

Template.incident_gestionnaire_index.helpers({
  'state': () => {
    return Template.instance().state.get();
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady() && Template.instance().subsHandler.ready();
  },
});