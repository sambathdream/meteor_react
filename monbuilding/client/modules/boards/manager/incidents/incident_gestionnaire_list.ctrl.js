import {Pagination} from '/client/components/pagination/pagination.js';

function activeCondo() {
	let incidentEnable = _.map(Condos.find({'settings.options.incidents': true}).fetch(), function(c) {return c._id});
	let incidentAvailable = Meteor.listCondoUserHasRight("incident", "see");
	return _.intersection(incidentEnable, incidentAvailable)
}

Template.incident_gestionnaire_list.onRendered(function() {
});

Template.incident_gestionnaire_list.onCreated(function() {
	Session.set('modal', false);
	let currentContext = FlowRouter.current();
  const activeCondoList = activeCondo()
	this.tables = [
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 0, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 1, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 2, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
		{pagination: new Pagination(Incidents, 15), query: {"state.status": 3, condoId: {$in: activeCondoList}}, sort: new ReactiveVar({field: "lastUpdate", state: -1})},
	];
	this.tables[0].pagination.setQuery(this.tables[0].query, {sort: {"lastUpdate": -1}});
	this.tables[1].pagination.setQuery(this.tables[1].query, {sort: {"lastUpdate": -1}});
	this.tables[2].pagination.setQuery(this.tables[2].query, {sort: {"lastUpdate": -1}});
	this.tables[3].pagination.setQuery(this.tables[3].query, {sort: {"lastUpdate": -1}});
	this.condoSearch = new ReactiveVar("");
	this.condoSelected = new ReactiveVar("all");

	this.selectedTab = new ReactiveVar('toValidate');
	let tabId = "toValidate"
	this.currentIncident = new ReactiveVar(tabId)
});

Template.incident_gestionnaire_list.events({
	'click .newIncident' (e, t) {
		Session.set('modal', true);
	},
});

Template.incident_gestionnaire_list.helpers({
	getCurrentTab: () => Template.instance().currentIncident.get(),
  setCurrentTab: () => {
    let template = Template.instance()
    return (newTab) => {
			template.currentIncident.set(newTab)
    }
	},
	getCurrentCondo: () => Template.currentData().selectedCondoId,
	Condos: () => {
		let find = {};
		if (Template.instance().condoSearch.get() != "") {
			return _.filter(Condos.find().fetch(), function(elem) {
				let regexp = new RegExp(Template.instance().condoSearch.get(), "i");
				return elem.name.match(regexp);
			});
		}
		return Condos.find({"settings.options.incidents": true}).fetch();
	},
	getCondoSelected: () => {
		return Template.instance().condoSelected.get()
	},
	refreshCondoId: () => {
		const condoId = Template.currentData().selectedCondoId
		const template = Template.instance()
    if (condoId !== template.condoSelected.get()) {
    	template.condoSelected.set(condoId);
    	let i = 0;
    	for (tab of template.tables) {
        let q = {
          "state.status": i,
          condoId: {$in: condoId === 'all' ? activeCondo() : [condoId]}
        }
        tab.pagination.setPage(1);
    		tab.pagination.setQuery(q, tab.pagination.option.get());
    		i++;
    	}
		}
    return true
	},
	canDeclare: () => {
	  const selectedCondo = Template.instance().condoSelected.get()
    if (selectedCondo === 'all') {
      let condoIdCanDeclare = Meteor.listCondoUserHasRight("incident", "autodeclare");
      const condosId = condoIdCanDeclare.filter((condoId) => {
        return Meteor.userHasRight('incident', 'autodeclare', condoId, Meteor.userId());
      })
      return condosId && condosId.length > 0;
    } else {
      return Meteor.userHasRight('incident', 'autodeclare', selectedCondo, Meteor.userId());
    }
	},
	showModal: () => {
		return () => $('#newIncident_modal').modal('show');
	}
});
