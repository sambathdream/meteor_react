Template.classifieds.onCreated(function () {
  this.condoId = new ReactiveVar(null)
  this.isAllSelected = new ReactiveVar(false)
  this.subscribe('classifiedsGestionnaireView')
})

Template.classifieds.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  notSameCondo: () => Template.instance().condoId.get() !== Template.currentData().selectedCondoId,
  refreshCondo: () => {
    const condoId = Template.currentData().selectedCondoId
    const lang = FlowRouter.getParam('lang') || 'fr'
    FlowRouter.go('app.gestionnaire.classifieds.condo', { lang, condo_id: condoId })
    Template.instance().condoId.set(condoId)
  },
  isAllSelected: () => Template.instance().isAllSelected.get(),
  getCondo: () => {
    const condoId = Template.instance().condoId.get()
    Template.instance().isAllSelected.set(condoId === 'all')
    let condo = Condos.findOne({ _id: condoId })
    if (condo) {
      let classifiedsId = _.find(condo.modules, function (elem) {
        return elem.slug === 'annonces'
      })
      let gestionnaireId = Meteor.user()
      let access = Enterprises.findOne(gestionnaireId.identities.gestionnaireId)
      if (access) {
        access = [1, 2]
        if (access.length) {
          return {
            classifiedsId: classifiedsId.data.classifiedsId,
            condoId: condoId,
            access: access,
            gestionnaire: true,
          }
        }
      }
    }
  }
})
