import { Template } from "meteor/templating";
import "./integrationCondo.html";

Template.integrationCondoM.onCreated(function() {
  this.subscribe('integrations');
	this.eventSearch = new ReactiveVar("");
});

Template.integrationCondoM.events({
  'click #backbutton' (e, t) {
    let current = FlowRouter.current();
    const lang = FlowRouter.getParam("lang") || "fr";
		FlowRouter.go("app.gestionnaire.integrations.tab", {lang, tab: 'condo'});
  },
  'click .integrationLine' (e, t) {
    const lang = FlowRouter.getParam("lang") || "fr";
    const integrationId = e.currentTarget.getAttribute("integrationId");
    FlowRouter.go("app.gestionnaire.integrationConfig", {lang, integrationId, condoId: FlowRouter.getParam("condoId")}, {from: 'condo'});
  },
});

Template.integrationCondoM.helpers({
  CDD_cursor: () => {
		return Condos.find().fetch().map(c => c._id);
  },
  condoId: () => {
    return FlowRouter.getParam("condoId")
  },
  onCondoSelect: () => (condoId) => {
    if (condoId !== this.id) {
      const lang = FlowRouter.getParam("lang") || "fr";
      FlowRouter.go("app.gestionnaire.integrationCondo", { lang, condoId });
    }
  },
  integrationBySection: function() {
    let integration = Integrations.find().fetch();
    const filter = Template.instance().eventSearch.get();
		if (filter !== '') {
			integration = _.filter(integration, i => (
				i.provider.indexOf(filter) >= 0 ||
				i.service.indexOf(filter) >= 0 ||
				i.company.indexOf(filter) >= 0
			))
		}
    return _
      .chain(integration)
        .groupBy('section')
        .map(group => ({
          section: group[0].section,
          apps: group
        }))
        .sortBy('section')
      .value()
  },
  getIntegrationConfig: function (integrationId) {
    const config = CondoIntegrationConfig.findOne({
      condoId: FlowRouter.getParam("condoId"),
      integrationId
    });
    return config || {}
  },
  formatDate: function (config) {
    if (config && config.activatedOn) {
      return moment(config.activatedOn).format("DD/MM/YYYY")
    }
    return '- - -'
  },
	onSearch: () => {
    const template = Template.instance();
    return (input) => {
      template.set('eventSearch', input);
    }
  }
});
