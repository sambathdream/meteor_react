import { handleActiveDirectoryModal } from '/client/sharedFunctions/active-directory'

Template.manager_occupantList.onCreated(function() {
  // this.condoId = new ReactiveVar(null)
  let tab = FlowRouter.getParam('tab')
  this.handler = Meteor.subscribe('usersProfile')
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: 'active'})
  }
})

Template.manager_occupantList.onDestroyed(function() {
})

Template.manager_occupantList.onRendered(() => {
})

Template.manager_occupantList.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab');
    FlowRouter.setParams({ tab: tabName })
  },
  'click .invitOccupant': (e, t) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.gestionnaire.occupantList.personalDetails', { lang })
  },
  'click .refresh-users-from-ad': () => {
    const condoId = Template.currentData().selectedCondoId
    handleActiveDirectoryModal(condoId)
  }
})

Template.manager_occupantList.helpers({
  haveActiveDirectory: () => {
    const condoId = Template.currentData().selectedCondoId
    if (condoId !== 'all') {
      return Condos.findOne({
        _id: condoId,
        integrations: { $exists: true },
        'integrations.activeDirectoryIntegration': true
      })
    }
    return false
  },
  condoIds: () => {
    // return Meteor.getManagerCondoIds()
    return Meteor.listCondoUserHasRight("trombi", "see")
  },
  getTemplateName: () => {
    let tab = FlowRouter.getParam('tab')
    if (tab === 'active') {
      return 'manager_activeUsers'
    } else {
      return 'manager_pendingUsers'
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('tab')
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: 'active' })
    }
    return tab
  },
  // condoIdCb: () => {
  //   let t = Template.instance()
  //   return (condoId) => {
  //     t.condoId.set(condoId)
  //   }
  // },
  userHasRightToAddOccupant: () => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight("trombi", "addOccupant")
    } else if (Meteor.userHasRight("trombi", "addOccupant", condoId)) {
      condoIds = [condoId]
    }
    return condoIds.length
  },
  shouldDisplayRestrictedAccess: () => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId == 'all') {
      condoIds = Meteor.listCondoUserHasRight("trombi", "see")
    } else if (Meteor.userHasRight("trombi", "see", condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
  getData: () => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId == 'all') {
      condoIds = Meteor.listCondoUserHasRight("trombi", "see")
    } else if (Meteor.userHasRight("trombi", "see", condoId)) {
      condoIds = [condoId]
    }
    return { condoId: condoIds }
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready()
  },
  getPendingCounter: () => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId == 'all') {
      condoIds = Meteor.listCondoUserHasRight("trombi", "see")
    } else if (Meteor.userHasRight("trombi", "see", condoId)) {
      condoIds = [condoId]
    }
    let counter = 0
    BadgesGestionnaire.find({ _id: { $in: condoIds } }).forEach(condoBadge => {
      counter += condoBadge.newUser
    });
    return counter
  }
})
