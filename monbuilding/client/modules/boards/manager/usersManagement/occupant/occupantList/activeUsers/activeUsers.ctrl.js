import { CommonTranslation } from "/common/lang/lang.js"
var PNF = require('google-libphonenumber').PhoneNumberFormat
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()

function condoDel (condos, userId, lang) {
  let btn = {}

  const translation = new CommonTranslation(lang)

  for (let index in condos) {
    const condo = Condos.findOne({ _id: condos[index].condoId })

    if (Meteor.userHasRight('trombi', 'delete', condos[index].condoId)) {
      let condoName = null

      if (condo) {
        if (condo.info.id != "-1")
          condoName = condo.info.id + ' - ' + condo.getName()
        else
          condoName = condo.getName();
      }

      btn['btn' + index] = {
        label: condoName, className: "btn-outline-red-confirm widthBtn", callback: function () {
          if (condo) {
            Meteor.call('removeResidentOfCondo', userId, condo._id, (err, res) => {
              if (err)
                sAlert.error(translation.commonTranslation["error_occured"])
              else {
                sAlert.success(translation.commonTranslation["validation_success"])
                if (condo.name === 'Les Estudines Paris Rosny') {
                  Meteor.call('sendSurvey', userId)
                }
              }
            })
          }
        }
      }
    }
  }
  btn['cancel'] = { label: translation.commonTranslation.cancel, className: "btn-red-confirm widthBtn" }

  return btn
}

Template.manager_activeUsers.onCreated(function() {
  // this.condoId = new ReactiveVar(null)
  this.searchText = new ReactiveVar('')
  this.filterSelected = new ReactiveVar('invited')
  this.filterSelectedReverse = new ReactiveVar(true)
})

Template.manager_activeUsers.onDestroyed(function() {
})

Template.manager_activeUsers.onRendered(() => {
})

Template.manager_activeUsers.events({
  'click .containerLine': function(e, t) {
    e.stopPropagation()
    if (this.canSeeDetail === true) {
      let userId = e.currentTarget.getAttribute("userid")
      const lang = FlowRouter.getParam("lang") || "fr"

      FlowRouter.go('app.gestionnaire.occupantList.occupantProfile', { lang, userId: userId, tab: 'documents' })
    }
  },
  'click .commonColumn.resendInvitation > button': function(event, template) {
    event.preventDefault()
    event.stopPropagation()
  },
  'click .commonColumn.trashColumn > div': function(event, template) {
    event.preventDefault()
    event.stopPropagation()
    const userId = this._id

    const lang = FlowRouter.getParam("lang") || "fr"
    const translate = new CommonTranslation(lang)

    let btn = condoDel(this.condos, this._id, lang)
    let size = Object.keys(btn).length

    let title = translate.commonTranslation.confirmation
    let message = translate.commonTranslation.select_building_to_remove + this.firstname
    $(event.currentTarget).css("max-width", "100%")
    bootbox.dialog({
      size: "medium",
      title: title,
      message: message,
      buttons: btn,
      onEscape: function () { },
      backdrop: true
    })
  },
  'click .filter-th': (e, t) => {
    const key = $(e.currentTarget).data('key')

    if (Template.instance().filterSelected.get() === key) {
      Template.instance().filterSelectedReverse.set(!Template.instance().filterSelectedReverse.get())
    } else {
      Template.instance().filterSelected.set(key)
    }
  },
})

Template.manager_activeUsers.helpers({
  updateCondo: () => {
    // Template.instance().condoId.set(Template.currentData().condoId)
    Template.currentData().condoId
  },
  getOccupants: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    // const condoIds = Template.instance().condoId.get()
    const condoIds = Template.currentData().condoId

    if (condoIds && condoIds.length) {
      let occupants = []
      let occupantsPending = []
      let users = []

      GestionnaireUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.condos.condoId': { $in: condoIds }
      }).forEach(user => {
        occupants.push(user.resident[0])
      })
      occupants.push({ isPendings: true })
      GestionnaireUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch:
            { 'condoId': { $in: condoIds },
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      }).forEach(user => {
        let l = user.resident[0]
        if (l) {
          l.isDeferred = !!user.deferredRegistrationDate && user.deferredRegistrationDate > Date.now()
          l.deferredRegistrationDate = user.deferredRegistrationDate
        }
        occupants.push(l)
      })
      let isPending = false
      occupants.forEach(user => {
        if (user.isPendings && user.isPendings === true) {
          isPending = true
          return
        }
        let thisUserCondo = []
        let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
        if (thisUserProfile) {
          // let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
          // let phoneMatch = false
          // try {
          //   number = phoneUtil.parse(number, "FR")
          //   if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
          //     phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
          //   }
          // } catch (e) { }

          const _userHasMatched =
            thisUserProfile.email.match(regexp) ||
            thisUserProfile.firstname.match(regexp) ||
            thisUserProfile.lastname.match(regexp) ||
            // thisUserProfile.tel.match(regexp) ||
            (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
            (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
            (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp)/*  ||
            phoneMatch */
          if (_userHasMatched) {
            let canSeeDetail = false
            if (!isPending) {
              thisUserCondo = _.filter(user.condos, condo => {
                if (_.contains(condoIds, condo.condoId)) {
                  if (Meteor.userHasRight('trombi', 'seeOccupantDetails', condo.condoId)) {
                    canSeeDetail = true
                  }
                  return true
                } else {
                  return false
                }
              })
            } else {
              thisUserCondo = _.filter(user.pendings, condo => {
                if (_.contains(condoIds, condo.condoId)) {
                  if (Meteor.userHasRight('trombi', 'seeOccupantDetails', condo.condoId)) {
                    canSeeDetail = true
                  }
                  return true
                } else {
                  return false
                }
              })
            }
            let avatar = Avatars.findOne({ _id: thisUserProfile._id })
            if (avatar && avatar.avatar.original) {
              thisUserProfile.avatar = avatar.avatar.original
            } else {
              thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
            }
            users.push({
              ...thisUserProfile,
              condos: thisUserCondo,
              invited: thisUserCondo[0].invited,
              isPending: isPending,
              isDeferred: user.isDeferred,
              deferredRegistrationDate: user.deferredRegistrationDate,
              canSeeDetail
            })
          }
        }
      })
      const filterSelectedReverse = Template.instance().filterSelectedReverse.get()
      const filterSelected = Template.instance().filterSelected.get()
      users.sort((a, b) => {
        if (filterSelected === 'firstname') {
          if (a.firstname.toLowerCase() < b.firstname.toLowerCase()) return filterSelectedReverse ? -1 : 1;
          if (a.firstname.toLowerCase() > b.firstname.toLowerCase()) return filterSelectedReverse ? 1 : -1;
          return 0;
        } else if(filterSelected === 'lastname') {
          if (a.lastname.toLowerCase() < b.lastname.toLowerCase()) return filterSelectedReverse ? -1 : 1;
          if (a.lastname.toLowerCase() > b.lastname.toLowerCase()) return filterSelectedReverse ? 1 : -1;
          return 0;
        } else if (filterSelected === 'email') {
          if (a.email.toLowerCase() < b.email.toLowerCase()) return filterSelectedReverse ? -1 : 1;
          if (a.email.toLowerCase() > b.email.toLowerCase()) return filterSelectedReverse ? 1 : -1;
          return 0;
        } else if (filterSelected === 'invited') {
          return filterSelectedReverse ? b.invited - a.invited : a.invited - b.invited
        }
      })

      return users
    } else {
      return []
    }
  },
  getTotal: () => {
    // const condoIds = Template.instance().condoId.get()
    const condoIds = Template.currentData().condoId

    let occupants = null
    let occupantsPending = null

    occupants = GestionnaireUsers.find({
      'resident.condos.condoId': { $in: condoIds }
    }, { fields: { _id: true } })
    occupantsPending = GestionnaireUsers.find({
      'resident.pendings': {
        $elemMatch:
          { 'condoId': { $in: condoIds },
            'invitByGestionnaire': { $exists: true },
            'invitByGestionnaire': true }
      }
    }, { fields: { _id: true } })
    const count = (occupants ? occupants.count() : 0) + (occupantsPending ? occupantsPending.count() : 0)
    return count
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitOccupant').click()
    }
  },
  isCondoAvailable: () => {
    // return !!Template.instance().condoId.get()
    return !!Template.currentData().condoId
  },
  getCondoName: (condoId) => {
    let condo = Condos.findOne(condoId)
    if (condo && condoId) {
      if (condo.info.id != '-1') {
        return condo.info.id + ' - ' + condo.getName()
      }
      else {
        return condo.getName()
      }
    }
  },
  userCanDeleteThisCondo: (condos) => {
    let newCondos = []
    for (const condo of condos) {
      if (Meteor.userHasRight('trombi', 'delete', condo.condoId)) {
        newCondos.push(condo)
      }
    }
    return newCondos
  },
  resendInvitation: (user) => {
    const t = Template.instance()

    return () => () => {
      const userId = user._id

      const lang = FlowRouter.getParam("lang") || "fr"
      const translate = new CommonTranslation(lang)

      let title = translate.commonTranslation.confirmation
      let message = translate.commonTranslation.confirm_sending_validation + user.firstname + ' ?'
      bootbox.confirm({
        size: "medium",
        title,
        message,
        buttons: {
          'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
          'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
        },
        backdrop: true,
        callback: function (result) {
          if (result) {
            Meteor.call('resendValidationToUser', userId, user.condos[0].condoId, (err, res) => {
              if (err) {
                sAlert.error(translate.commonTranslation.error_occured)
              } else {
                sAlert.success(translate.commonTranslation.invitation_send_success)
              }
            })
          }
        }
      })
    }
  },
  filterSelected: (key) => {
    return Template.instance().filterSelected.get() === key
  },
  filterSelectedReverse: () => {
    return Template.instance().filterSelectedReverse.get()
  }
})
