var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
import { CommonTranslation } from "/common/lang/lang.js";

Template.manager_pendingUsers.onCreated(function() {
  // this.condoId = new ReactiveVar(null)
  this.searchText = new ReactiveVar('')
})

Template.manager_pendingUsers.onDestroyed(function() {
})

Template.manager_pendingUsers.onRendered(() => {
})

Template.manager_pendingUsers.events({
  'click .containerLine': function(e, t) {
    e.stopPropagation()
    if (this.canSeeDetail === true) {
      let userId = $(e.currentTarget).attr('userId')
      const lang = FlowRouter.getParam("lang") || "fr";
      FlowRouter.go('app.gestionnaire.occupantList.occupantProfile', { lang, userId, tab: 'documents' });
    }
  },
  'click .download': function (e, t) {
    e.stopPropagation();
  },
  'click .justifFileDownload': function (event, template) {
    event.stopPropagation();
    event.preventDefault();
    let _event = event;
    $(_event.currentTarget).css("max-width", "100%");
    bootbox.alert({
      size: "medium",
      title: "Justificatifs",
      message: _event.currentTarget.outerHTML,
      backdrop: true
    });
  },
  'click .validOccupantColumn': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const userId = $(event.currentTarget).attr('userId')
    const condoId = $(event.currentTarget).attr('condoId')
    // const condoId = Template.currentData().condoId

    bootbox.confirm({
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.confirm_occupant,
      buttons: {
        cancel: { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        confirm: { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      callback: function (result) {
        if (result) {
          Meteor.call('newResidentUserConfirm', userId, condoId, (err, res) => {
            if (err) {
              sAlert.error(translate.commonTranslation.error_occured)
            }
            else
              sAlert.success(translate.commonTranslation.validation_success)
          });
        }
      }
    })
  },
  'click .denyOccupantColumn': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const userId = $(event.currentTarget).attr('userId')
    const condoId = $(event.currentTarget).attr('condoId')
    // const condoId = Template.currentData().condoId

    bootbox.confirm({
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.deny_occupant,
      buttons: {
        cancel: { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        confirm: { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      callback: function (result) {
        if (result) {
          Meteor.call('newResidentUserDeny', userId, condoId, (err, res) => {
            if (err)
              sAlert.error(translate.commonTranslation.error_occured)
            else
              sAlert.success(translate.commonTranslation.success_occupant_deny)
          })
        }
      }
    })
  }
})

Template.manager_pendingUsers.helpers({
  updateCondo: () => {
    // Template.instance().condoId.set(Template.currentData().condoId)
    Template.currentData().condoId
  },
  getOccupants: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().condoId

    let occupants = []
    let users = []

    occupants = GestionnaireUsers.find({
      '_id': { $ne: Meteor.userId() },
      'resident': {
        $elemMatch: {
          'pendings.condoId': { $in: condoId },
          'validation.type': 'gestionnaire'
        }
      }
    })
    occupants.forEach(user => {
      let thisUserProfile = UsersProfile.findOne({ _id: user._id })
      if (thisUserProfile) {

        let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
        let phoneMatch = false
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
          }
        } catch (e) { }

        const _userHasMatched =
          thisUserProfile.email.match(regexp) ||
          thisUserProfile.firstname.match(regexp) ||
          thisUserProfile.lastname.match(regexp) ||
          thisUserProfile.tel.match(regexp) ||
          (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          phoneMatch
        thisUserCondo = user.resident[0].pendings
        if (_userHasMatched) {
          let canSeeDetail = false
          thisUserCondo.forEach((condo) => {
            if (Meteor.userHasRight('trombi', 'seeOccupantDetails', condo.condoId)) {
              canSeeDetail = true
            }
          })
          let avatar = Avatars.findOne({ _id: thisUserProfile._id })
          if (avatar && avatar.avatar.original) {
            thisUserProfile.avatar = avatar.avatar.original
          } else {
            thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
          }
          users.push({
            ...thisUserProfile,
            condos: thisUserCondo,
            invited: thisUserCondo[0].invited,
            canSeeDetail
          })
        }
      }
    })
    users.sort((a, b) => { return b.invited - a.invited })

    return users
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: () => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().condoId

    let occupants = GestionnaireUsers.find({
      '_id': { $ne: Meteor.userId() },
      'resident': {
        $elemMatch: {
          'pendings.condoId': { $in: condoId },
          'validation.type': 'gestionnaire'
        }
      }
    })
    return occupants ? occupants.count() : 0
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitOccupant').click()
    }
  },
  isCondoAvailable: () => {
    // return !!Template.instance().condoId.get()
    return !!Template.currentData().condoId
  },
  getCondoName: (condoId) => {
    let condo = Condos.findOne(condoId)
    if (condo && condoId) {
      if (condo.info.id != '-1') {
        return condo.info.id + ' - ' + condo.getName()
      }
      else {
        return condo.getName()
      }
    }
  }
})
