import { CommonTranslation } from "/common/lang/lang.js";

var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.managerList.onCreated(function() {
  // this.condoId = new ReactiveVar('')
  this.searchText = new ReactiveVar('')
})

Template.managerList.onDestroyed(function() {
})

Template.managerList.onRendered(() => {
})

Template.managerList.events({
  'click .invitManagerButton': (event, template) => {
    // let condoId = Template.instance().condoId.get()
    let condoId = Template.currentData().selectedCondoId
    const lang = FlowRouter.getParam("lang") || "fr";
    if (condoId === 'all') {
      FlowRouter.go('app.gestionnaire.managerList.personalDetails', { lang })
    } else {
      FlowRouter.go('app.gestionnaire.managerList.personalDetails.condoId', { lang, condoId })
    }
  },
  'click .commonColumn.resendInvitation > button': function (event, template) {
    event.preventDefault()
    event.stopPropagation()
  },
  'click .containerLine': (event, template) => {
    let userId = $(event.currentTarget).attr('userId')
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.gestionnaire.managerList.managerProfile', { lang, userId: userId, tab: 'buildings' })
  },
  'click .commonColumn.trashColumn > div': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    let userId = $(event.currentTarget).attr('userId')
    const user = UsersProfile.findOne({ _id: userId })
    let condoIds = []

    const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));

    $('[name="condosColumn-' + userId + '"]').children().each((index, elem) => {
      condoIds.push({ _id: $(elem).attr('condoId'), name: $(elem).text() })
    })


    // let btn = condoDel(result, userId)
    // let size = Object.keys(btn).length
    // let message = ''
    let title = '<div style="border-bottom: 1px solid #e0e0e0; padding: 0 0 17px 15px; margin: 0 -15px 22px -15px !important;"> \''
					translation.commonTranslation["delete_building_from"] + ' ' + user.firstname + ' ' + user.lastname + '</div>'

    let message = translation.commonTranslation["select_building_delete_for"] + user.firstname + ' ' + user.lastname

    let btn = {}

    condoIds.forEach((condo, index) => {
      btn['btn' + index] = {
        label: condo.name,
        className: "btn-outline-red-confirm widthBtn", callback: function () {
          if (result) {
            // Meteor.call('removeResidentOfCondo', userId, condo._id, (err, res) => {
            //   if (err)
            //     sAlert.error("Une erreur est survenue.")
            //   else {
            //     sAlert.success("Validation réussie")
            //   }
            // })
          }
        }
      }
    })

    btn['cancel'] = { label: translation.commonTranslation["cancel"], className: "btn-red-confirm widthBtn" }

    // if (size > 1) {
    // } else {
    //   message = "Vous ne semblez pas avoir les droits pour supprimer cet/ces immeuble(s) !"
    // }

    bootbox.dialog({
      size: "medium",
      title: title,
      message: message,
      buttons: btn,
      onEscape: function () { },
      backdrop: true
    })
  }
})

Template.managerList.helpers({
  CDD_cursorMain: () => {
    // return Meteor.getManagerCondoIds()
    return Meteor.listCondoUserHasRight("managerList", "seeManager")
  },
  // mainCondoIdCb: () => {
  //   let template = Template.instance()
  //   return (condoId) => {
  //     template.condoId.set(condoId)
  //   }
  // },
  shouldNotDisplayRestrictedAccess: (allowedCondoIds) => {
    // const condoId = Template.instance().condoId.get()
    const condoId = Template.currentData().selectedCondoId
    let condoIds = []
    if (condoId === 'all') {
      condoIds = allowedCondoIds
    } else if (Meteor.userHasRight("managerList", "seeManager", condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
  getManagers: (allowedCondoIds) => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");
    let userIds = []
    let condosPerUser = []
    // const condoIds = (Template.instance().condoId.get() === 'all') ? allowedCondoIds : [Template.instance().condoId.get()]
    const condoIds = (Template.currentData().selectedCondoId === 'all') ? allowedCondoIds : [Template.currentData().selectedCondoId]
    let condos = {}
    Condos.find({ _id: { $in: allowedCondoIds } }).forEach(condo => {
      condos[condo._id] = condo
    })


    let users = []
    Enterprises.find({ condos: { $in: condoIds } }).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (!user.isAdmin && user.userId !== Meteor.userId()) {
          condosPerUser[user.userId] = []
          const condosInCharge = _.pluck(user.condosInCharge, 'condoId')
          let _userCondosHasMatched = false
          if (_.intersection(condosInCharge, condoIds).length > 0) {
            condosInCharge.forEach(condoId => {
              if (_.contains(allowedCondoIds, condoId)) {
                let thisCondo = condos[condoId]
                if (!_userCondosHasMatched) {
                  _userCondosHasMatched = thisCondo.name.match(regexp) || thisCondo.info.address.match(regexp) || thisCondo.info.city.match(regexp) || thisCondo.info.code.match(regexp)
                }
                let name = ''
                if (thisCondo.info.id != "-1")
                  name = thisCondo.info.id + ' - ' + thisCondo.getName()
                else
                  name = thisCondo.getName();

                condosPerUser[user.userId].push({
                  _id: condoId,
                  name: name
                })
              }
            })
            if (condosPerUser[user.userId].length > 0) {
              let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
              if (thisUserProfile) {

                let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
                let phoneMatch = false
                try {
                  number = phoneUtil.parse(number, "FR")
                  if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
                    phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
                  }
                } catch (e) {}

                const _userHasMatched =
                  thisUserProfile.email.match(regexp) ||
                  thisUserProfile.firstname.match(regexp) ||
                  thisUserProfile.lastname.match(regexp) ||
                  thisUserProfile.tel.match(regexp) ||
                  (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
                  (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
                  (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
                  phoneMatch
                if (_userHasMatched || _userCondosHasMatched) {
                  let avatar = Avatars.findOne({ _id: thisUserProfile._id })
                  if (avatar && avatar.avatar.original) {
                    thisUserProfile.avatar = avatar.avatar.original
                  } else {
                    thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
                  }
                  users.push({
                    ...thisUserProfile,
                    condos: condosPerUser[user.userId]
                  })
                }
              }
            }
          }
        }
      })
    })
    return users
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: (allowedCondoIds) => {
    let userIds = []
    let condosPerUser = []
    // const condoIds = (Template.instance().condoId.get() === 'all') ? allowedCondoIds : [Template.instance().condoId.get()]
    const condoIds = (Template.currentData().selectedCondoId === 'all') ? allowedCondoIds : [Template.currentData().selectedCondoId]

    let users = 0
    Enterprises.find({ condos: { $in: condoIds } }).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (!user.isAdmin && user.userId !== Meteor.userId()) {
          condosPerUser[user.userId] = 0
          user.condosInCharge.forEach(condoInCharge => {
            if (_.contains(condoIds, condoInCharge.condoId)) {
              condosPerUser[user.userId]++
            }
          })
          if (condosPerUser[user.userId] > 0) {
            let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
            if (thisUserProfile) {
              users++
            }
          }
        }
      })
    })
    return users
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitManagerButton').click()
    }
  },
  getCondoIdsRights: (allowedCondoIds) => {
    // return (Template.instance().condoId.get() === 'all') ? allowedCondoIds : [Template.instance().condoId.get()]
    return (Template.currentData().selectedCondoId === 'all') ? allowedCondoIds : [Template.currentData().selectedCondoId]
  },
  getSelectedCondo: () => {
    // return Template.condoId.get()
    return Template.currentData().selectedCondoId
  },
  resendInvitation: (user) => {
    const t = Template.instance()

    return () => () => {
      const userId = user._id

      const lang = FlowRouter.getParam("lang") || "fr"
      const translate = new CommonTranslation(lang)

      let title = translate.commonTranslation.confirmation
      let message = translate.commonTranslation.confirm_sending_validation + user.firstname + ' ?'
      bootbox.confirm({
        size: "medium",
        title,
        message,
        buttons: {
          'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
          'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
        },
        backdrop: true,
        callback: function (result) {
          if (result) {
            Meteor.call('resendValidationToUser', userId, user.condos[0]._id, (err, res) => {
              if (err) {
                sAlert.error(translate.commonTranslation.error_occured)
              } else {
                sAlert.success(translate.commonTranslation.invitation_send_success)
              }
            })
          }
        }
      })
    }
  }
})
