import { CommonTranslation } from "/common/lang/lang.js"


var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.manager_editNamesOccupant.onCreated(function () {
  this.isOpen = new ReactiveVar(false)

  this.firstname = new ReactiveVar(null)
  this.lastname = new ReactiveVar(null)

  this.canSubmit = new ReactiveVar(false)
})

Template.manager_editNamesOccupant.onDestroyed(() => {
})

Template.manager_editNamesOccupant.onRendered(() => {
})

Template.manager_editNamesOccupant.events({
  'hidden.bs.modal #modal_manager_edit_names': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_manager_edit_names': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_manager_edit_names"

    let thisUser = UsersProfile.findOne({ _id: FlowRouter.getParam('userId') })
    template.firstname.set(thisUser.firstname)
    template.lastname.set(thisUser.lastname)

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_manager_edit_names") {
        modal.modal('hide');
      }
    }
  },
  'click .saveContact': (e, t) => {
    if (t.canSubmit.get() === true) {
      const that = $(e.currentTarget)
      that.button('loading')
      const firstname = t.firstname.get()
      const lastname = t.lastname.get()

      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      const userId = FlowRouter.getParam('userId')
      Meteor.call('updateUserNames', firstname, lastname, userId, (error, result) => {
        that.button('reset')
        if (!error) {
          sAlert.success(translate.commonTranslation.changes_saved)
          $('#modal_manager_edit_names').modal('hide')
        } else {
          sAlert.error(error)
        }
      })
    }
  }
})

function updateSubmit(template) {
  let canSubmit = false
  const firstname = template.firstname.get()
  const lastname = template.lastname.get()

  canSubmit = !!firstname && !!lastname

  template.canSubmit.set(canSubmit)
}

Template.manager_editNamesOccupant.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  isFieldError: (key) => {
    const t = Template.instance();
    return !t[key].get()
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      t[key].set(value)
      updateSubmit(t)
    }
  },
  getFirstname: () => {
    const t = Template.instance()
    return () => t.firstname.get() || ''
  },
  getLastname: () => {
    const t = Template.instance()
    return () => t.lastname.get() || ''
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  }
})
