
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
import { CommonTranslation } from "/common/lang/lang.js"

Template.manager_editBuildingModal.onCreated(function () {
  this.subscribe('PhoneCode');
  this.subscribe('schools')

  this.condoId = new ReactiveVar(null)
  this.isOpen = new ReactiveVar(false)

  this.form = new ReactiveDict()
  this.form.setDefault({
    company: null,
    diploma: null,
    school: null,
    studies: null,
    door: null,
    floor: null,
    office: null,
    wifi: null
  })

})

Template.manager_editBuildingModal.onDestroyed(() => {
})

Template.manager_editBuildingModal.onRendered(() => {
})

Template.manager_editBuildingModal.events({
  'hidden.bs.modal #modal_manager_occupant_edit_building': (event, template) => {
    template.isOpen.set(false)
    template.condoId.set(null)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_manager_occupant_edit_building': (event, template) => {
    template.isOpen.set(false)
    template.condoId.set(null)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_manager_occupant_edit_building";

    template.isOpen.set(true)
    const condoId = Template.currentData().condoId
    Template.instance().condoId.set(condoId)
    window.onhashchange = function () {
      if (location.hash != "#modal_manager_occupant_edit_building") {
        modal.modal('hide');
      }
    }
  },
  'click .saveBuilding': (e, t) => {
    $(e.currentTarget).button('loading')
    const form = t.form.all()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang);
    if (form.company) {
      const name = new RegExp(`^${form.company}$`, 'ig')
      const thisCompany = CompanyName.findOne({ name: name })
      if (!thisCompany) {
        $(e.currentTarget).button('reset')
        return sAlert.error(translate.commonTranslation['wrong_company_name'])
      }
      form.company = thisCompany._id
    }
    const condoId = t.condoId.get()
    const userId = FlowRouter.getParam('userId')
    Meteor.call('saveOccupantBuildingInfo', condoId, form, userId, (error, result) => {
      $(e.currentTarget).button('reset')
      if (!error) {
        sAlert.success(translate.commonTranslation['building_edit_success'])
        $('#modal_manager_occupant_edit_building').modal('hide')
      } else {
        sAlert.error(error)
      }
    })

  }
})

Template.manager_editBuildingModal.helpers({
  isOpen: () => {
    return !!(Template.instance().condoId.get())
    // return Template.instance().isOpen.get() && Template.instance().condoId.get()
  },
  updateData: () => {
    const condoId = Template.currentData().condoId
    Template.instance().condoId.set(condoId)
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'company' || key === 'diploma' || key === 'door' || key === 'floor' || key === 'office' || key === 'wifi') {
        let thisVal = t.form.get(key)
        thisVal = !value ? null : value
        t.form.set(key, thisVal)
      }
    }
  },
  onSelectCb: (key) => {
    const t = Template.instance()

    return () => selected => {
      t.form.set(key, !selected ? null : selected)
    }

  },
  getForm: (key) => {
    const t = Template.instance()

    return () => t.form.get(key) || ''
  },
  getSchoolOptions: () => {
    let school = Schools.findOne();
    if (school) {
      return _.map(school.schools, (elem) => {
        return {
          text: elem.name,
          id: elem.name
        };
      });
    }
  },
  getStudiesOptions: () => {
    return schoolSubject
  },
  getStudiesGroup: () => {
    return [
      "Business & Management",
      "Finance",
      "Admin, RH & Juridique",
      "IT & Digital",
      "Sciences naturelles & Ingénierie",
      "Production & Logistique",
      "Humanités et Création",
      "Sciences Médicales",
      "Autres"
    ]
  },
  getCondo: () => {
    const condoId = Template.instance().condoId.get()
    if (condoId) {
      let occupant = GestionnaireUsers.findOne({ _id: FlowRouter.getParam('userId') })
      if (occupant) {
        occupant = occupant.resident[0]
      }

      let thisCondo = occupant.condos.find(condo => { return condo.condoId === condoId })
      if (!thisCondo) {
        thisCondo = occupant.pendings.find(condo => { return condo.condoId === condoId })
      }
      if (!thisCondo.userInfo)
        thisCondo.userInfo = {}
      const thisCompany = CompanyName.findOne({ _id: thisCondo.userInfo.company })
      Template.instance().form.set({
        company: thisCompany ? thisCompany.name : null,
        diploma: thisCondo.userInfo.diploma || null,
        school: thisCondo.userInfo.school || null,
        studies: thisCondo.userInfo.studies || null,
        door: thisCondo.userInfo.porte || null,
        floor: thisCondo.userInfo.etage || null,
        office: thisCondo.userInfo.office || null,
        wifi: thisCondo.userInfo.wifi || null
      })
      return condoId
    }
  }
})

let schoolSubject = [
  [
    "Achat",
    "Commercial & Business Development",
    "Communication, RP & Evénementiel",
    "Management, Conseil & Stratégie",
    "Marketing & Webmarketing",
    "Relations publiques, publicité",
    "Service & Relations Clients",
    "Tourisme, Restauration & Hotellerie"
  ],
  [
    "Actuariat",
    "Audit",
    "Contrôle de Gestion & Comptabilité",
    "Economie",
    "Finance d'entreprise",
    "Finance de marché",
    "Gestion d'actifs"
  ],
  [
    "Administratif",
    "Droit",
    "Ressources Humaines"
  ],
  [
    "Développement informatique",
    "Electronique & Traitement du signal",
    "Gestion de projet IT & Product",
    "Infra, Réseaux & Télécoms",
    "Intelligence artificielle",
    "Webdesign & Ergonomie"
  ],
  [
    "Agronomie",
    "Aéronautique",
    "Architecture & Urbanisme",
    "Biologie",
    "Chimie & Procédés",
    "Energie, Matériaux & Mécanique",
    "Environnement",
    "Génie Civil & Structures",
    "Génie Industriel & Conception",
    "Statistiques, Data & Math App"
  ],
  [
    "Logistique & Supply Chain",
    "Production & Exploitation",
    "Qualité & Maintenance",
    "Travaux & Chantier"
  ],
  [
    "Audiovisuel",
    "Création & Graphisme",
    "Journalisme & Edition",
    "Musique",
    "Philosophie et autres sciences"
  ],
  [
    "Dentisterie",
    "Infirmière",
    "Médecine",
    "Pharmacie",
    "Psychiatrie",
    "Santé publique",
    "Technologie Médicale",
    "Vétérinaire"
  ],
  [
    "Education & Formation",
    "Géographie & Géologie",
    "Langues",
    "Sciences Sociales",
    "Jeune travailleur",
    "Autres"
  ]
]
