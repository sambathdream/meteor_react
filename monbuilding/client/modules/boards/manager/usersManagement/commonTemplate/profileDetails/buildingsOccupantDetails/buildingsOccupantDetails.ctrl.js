
import { CommonTranslation } from '/common/lang/lang.js'

Template.buildingsOccupantDetails.onCreated(function () {
  this.userId = FlowRouter.getParam('userId')
  this.searchText = new ReactiveVar('')
  this.editingCondo = new ReactiveVar('')
})

Template.buildingsOccupantDetails.onDestroyed(function () {
})

Template.buildingsOccupantDetails.onRendered(() => {
})

Template.buildingsOccupantDetails.events({
  'click .editCondo': (e, t) => {
    t.editingCondo.set($(e.currentTarget).data('condoid'))
    $('#modal_manager_occupant_edit_building').modal('show')
  },
  'click .deleteCondo': (e, t) => {
    const condoId = $(e.currentTarget).data('condoid')
    const userId = t.userId
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new CommonTranslation(lang)
    const userProfile = UsersProfile.findOne({_id: userId})
    const userName = userProfile.firstname + ' ' + userProfile.lastname
    const condoName = $(e.currentTarget).data('condoname')
    bootbox.confirm({
      title: translation.commonTranslation.confirmation,
      message: translation.commonTranslation.are_you_sure_remove + userName + translation.commonTranslation.are_you_sure_remove2 + condoName + translation.commonTranslation.interrogation_point,
      buttons: {
        confirm: { label: translation.commonTranslation["delete"], className: "btn-red-confirm" },
        cancel: { label: translation.commonTranslation["cancel"], className: "btn-outline-red-confirm" }
      },
      show: true,
      callback: function (res) {
        if (res) {
          Meteor.call('removeResidentOfCondo', userId, condoId, (err, res) => {
            if (err)
              sAlert.error(translation.commonTranslation["error_occured"])
            else {
              sAlert.success(translation.commonTranslation["validation_success"])
              if (condoName === 'Les Estudines Paris Rosny') {
                Meteor.call('sendSurvey', userId)
              }
            }
          })
        }
      }
    })
  }
})

Template.buildingsOccupantDetails.helpers({
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  getOccupantCondos: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    let allowedCondoIds = Meteor.listCondoUserHasRight("trombi", "see")
    let userId = Template.instance().userId

    const occupant = GestionnaireUsers.findOne({ _id: Template.instance().userId })

    const translation = new CommonTranslation(FlowRouter.getParam("lang") || "fr")

    let listCondo = []
    const condos = [...occupant.resident[0].condos, ...occupant.resident[0].pendings]
    _.each(condos, (condoResident) => {
      if (_.contains(allowedCondoIds, condoResident.condoId) && Meteor.userHasRight('trombi', 'seeOccupantDetails', condoResident.condoId)) {
        const condo = Condos.findOne({ _id: condoResident.condoId })
        let condoName = !!condo.info.address ? condo.info.address : '-'
        if (condo.name && condo.name !== "" && (condo.info.address && condo.name !== condo.info.address)) {
          condoName = condo.name
          if (condo.info.id && condo.info.id !== '-1') {
            condoName = condo.info.id + ' - ' + condoName
          }
        }
        let adress = condo.info.address + ", " + condo.info.code + " " + condo.info.city
        let userRight = UsersRights.findOne({
          $and: [
            { "userId": userId },
            { "condoId": condo._id }
          ]
        })
        let defaultRoleName = translation.commonTranslation["undefined"]
        if (userRight && userRight.defaultRoleId) {
          let defaultRole = DefaultRoles.findOne({ _id: userRight.defaultRoleId })
          if (defaultRole && defaultRole.name) {
            defaultRoleName = defaultRole.name
          }
        }
        if (!condoResident.userInfo) {
          condoResident.userInfo = {}
        }
        condoResident.buildings.forEach(building => {
          let thisBuilding = Buildings.findOne({ _id: building.buildingId })
          if (thisBuilding) {
            building.name = thisBuilding.info.address + ", " + thisBuilding.info.code + " " + thisBuilding.info.city
            if (thisBuilding.name && thisBuilding.name !== "" && (thisBuilding.info.address && thisBuilding.name !== thisBuilding.info.address)) {
              building.name = thisBuilding.name + '<br>' + building.name
            }
          } else if (building.buildingId === '-1') {
            building.buildingId = Math.random()
            building.name = '-'
          }
          listCondo.push({
            _id: building.buildingId, // to make it faster, with a UNIQ id
            condoId: condoResident.condoId,
            invited: condoResident.invited,
            joined: condoResident.joined,
            name: condoName,
            adress: adress,
            role: defaultRoleName,
            company: condoResident.userInfo.company || '-',
            school: condoResident.userInfo.school || '-',
            studies: condoResident.userInfo.studies || '-',
            diploma: condoResident.userInfo.diploma || '-',
            office: condoResident.userInfo.office || '-',
            door: condoResident.userInfo.porte || '-',
            floor: condoResident.userInfo.etage || '-',
            wifi: condoResident.userInfo.wifi || '-',
            building
          })
        })
      }
    })
    return listCondo
  },
  getEditingCondo: () => {
    return Template.instance().editingCondo.get()
  },
  getTotalCondos: () => {
    return 1
  },
})
