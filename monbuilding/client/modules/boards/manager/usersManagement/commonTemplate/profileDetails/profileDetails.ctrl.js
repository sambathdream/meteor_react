Template.profileDetails.onCreated(function () {
  this.userId = FlowRouter.getParam('userId')
  this.subscribe('UserDocumentsPublishManagerSide', this.userId)

  let tab = FlowRouter.getParam('tab')
  if (FlowRouter.getRouteName() === 'app.gestionnaire.managerList.managerProfile') {
    if (!tab || (tab !== 'buildings' && tab !== 'rights')) {
      FlowRouter.setParams({ tab: 'buildings' })
    }
  } else {
    if (!tab || (tab !== 'buildingsOccupant' && tab !== 'rights' && tab !== 'documents'&& tab !== 'payments')) {
      FlowRouter.setParams({ tab: 'documents' })
    }
  }
})

Template.profileDetails.onDestroyed(function () {
})

Template.profileDetails.onRendered(() => {
})

Template.profileDetails.events({
  'click #backbutton': (event, template) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    if (FlowRouter.getRouteName() === 'app.gestionnaire.managerList.managerProfile') {
      FlowRouter.go('app.gestionnaire.managerList', { lang });
    } else {
      FlowRouter.go('app.gestionnaire.occupantList.tab', { lang, tab: 'active' })
    }
  },
  'click .tabBarContainer > div': (event, template) => {
    let tabName = $(event.currentTarget).data('tab');
    FlowRouter.setParams({tab: tabName})
  },
  'click .action-button#profile-edit-button': (event, t) => {
    // $('#modal_manager_edit_profile').modal()
    // console.log('MODAL EDIT USER TOGGLED')
  }
})

Template.profileDetails.helpers({
  getUserProfile: () => {
    let profile = UsersProfile.findOne({ _id: Template.instance().userId })
    const lang = FlowRouter.getParam("lang") || "fr";
    if (profile === undefined) {
      if (FlowRouter.getRouteName() === 'app.gestionnaire.managerList.managerProfile') {
        FlowRouter.go('app.gestionnaire.managerList', { lang })
      } else {
        FlowRouter.go('app.gestionnaire.occupantList.tab', { lang, tab: 'active' })
      }
    }
    return profile
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().userId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  selectedTab: () => {
    return FlowRouter.getParam('tab')
  },
  isManagerModule: () => {
    return FlowRouter.getRouteName() === 'app.gestionnaire.managerList.managerProfile'
  },
  getTabTemplate: () => {
    switch (FlowRouter.getParam('tab')) {
      case 'buildings':
        return 'buildingsDetails'
        break;
      case 'buildingsOccupant':
        return 'buildingsOccupantDetails'
        break;
      case 'rights':
        return 'rightsDetails'
        break;
      case 'documents':
        return 'occupantDocuments'
        break;
      case 'payments':
        return 'rightsDetails'
        break;
      default:
        break;
    }
  },
  documentsAvailable: () => {
    const occupant = GestionnaireUsers.findOne({ _id: Template.instance().userId })

    let condoIds = []
    if (occupant && occupant.resident[0]) {
      condoIds = _.map(occupant.resident[0].condos, function (elem) {
        if (Meteor.userHasRight('trombi', 'seeOccupantDetails', elem.condoId)) {
          return elem.condoId;
        }
      });
    }
    let options = CondosModulesOptions.find({ condoId: { $in: condoIds } }).fetch()
    let ret = false;
    _.each(options, function (option) {
      if (option.profile['documents'] == true)
        ret = true;
    });
    if (ret === false && FlowRouter.getParam('tab') === 'documents') {
      Meteor.setTimeout(function() {
        FlowRouter.setParams({ tab: 'buildingsOccupant' })
      }, 100);
    }
    return ret;
  }
})
