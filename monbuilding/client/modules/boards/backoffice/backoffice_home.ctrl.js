import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'

import './backoffice_home.view.html'

Template.backoffice_home.onCreated(function () {
  Meteor.isAdmin = true
  Meteor.isManager = false
  console.time('backoffice')
  this.handler = this.subscribe('backoffice')
  this.subscribe('avatarLinksGestionnaire')
  // this.subscribe('UserFiles')
  if (FlowRouter.current().path === '/backoffice') {
    FlowRouter.go('app.backoffice.managerList')
  }
})

Template.backoffice_home.onRendered(function () {
})

Template.backoffice_home.events({
  'click .display-modules': function (event, template) {
    $('[name=\'navBackoffice\']').removeClass('backMenuShow')
    $('[name=\'navBackoffice\']').addClass('backMenuHide')
  },
  'click #backHamburger': function (event, template) {
    if ($('.backMenuShow[name=\'navBackoffice\']').length) {
      $('[name=\'navBackoffice\']').removeClass('backMenuShow')
      $('[name=\'navBackoffice\']').addClass('backMenuHide')
    } else {
      $('[name=\'navBackoffice\']').addClass('backMenuShow')
      $('[name=\'navBackoffice\']').removeClass('backMenuHide')
    }
  },

  'click #goBoardGestionnaire': function (event, template) {
    template.handler.stop()
  },

  'click #users': function (event, template) {
    FlowRouter.go('app.backoffice.occupantList.tab', { tab: 'active' })
  },
  'click #buildings': function (event, template) {
    FlowRouter.go('app.backoffice.buildings')
  },
  'click #condos': function (event, template) {
    FlowRouter.go('app.backoffice.buildingsList.tab', { tab: 'list' })
  },
  'click #manager': function (event, template) {
    FlowRouter.go('app.backoffice.managerList')
  },
  'click #archives': function (event, template) {
    FlowRouter.go('app.backoffice.archives')
  },
  'click #concierge': function (event, template) {
    FlowRouter.go('app.backoffice.concierge')
  },
  // 'click #enterprise': function (event, template) {
  //   FlowRouter.go('app.backoffice.enterprise')
  // },
  'click #enterprise': function (event, template) {
    FlowRouter.go('app.backoffice.entityList')
  },
  'click #report': function (event, template) {
    FlowRouter.go('app.backoffice.report')
  },
  'click #stats': function (event, template) {
    FlowRouter.go('app.backoffice.globalStats')
  },
  'click #resource-type': function (event, template) {
    FlowRouter.go('app.backoffice.resourceType')
  },
  'click #resource-service': function (event, template) {
    FlowRouter.go('app.backoffice.resourceService')
  },
  'click #roles': function (event, template) {
    FlowRouter.go('app.backoffice.roles')
  },
  'click #coffee': function (event, template) {
    FlowRouter.go('app.backoffice.scripts')
  },
  'click #integrations': function (event, template) {
    FlowRouter.go('app.backoffice.integrations')
  }
})

Template.backoffice_home.helpers({
  isSubscribeReady: () => {
    // return true
    if (Template.instance().subscriptionsReady() === true) {
      console.timeEnd('backoffice')
    }
    return Template.instance().subscriptionsReady()
  },
})
