import { Template } from "meteor/templating";
import { Meteor } from 'meteor/meteor';

import "./scripts.view.html";
import { Session } from "meteor/session";

Template.scripts.onCreated(function() {
});

Template.scripts.onRendered(function() {
	bootbox.alert({
		size: "medium",
		title: "Disclaimer",
		message: "Attention, l'utilisation de certains scripts peut causer des dommages irréversibles.",
		backdrop: true
	});
});

Template.scripts.events({
	'click .runMethod': function(event, template) {
		$(".runMethod").button("loading");
		if ($(event.currentTarget).data("method") == "ServerScriptCreateCondosModulesOptions") {
			bootbox.confirm({
				size: "medium",
				title: "Confirmation",
				message: "Voulez vous effacer les précédentes options de chaques immeubles ?",
				buttons : {
					'cancel' : {label: "Non", className: "btn-red-confirm"},
					'confirm' : {label: "Oui", className: "btn-outline-red-confirm"}
				},
				backdrop: true,
				callback: function(result) {
					Meteor.call($(event.currentTarget).data("method"), result, function(error, result) {
						$(".runMethod").button("reset");
						if (error)
							sAlert.error(error);
						else
							sAlert.success("Done");
					});
				}
			});
		}
		else {
			Meteor.call($(event.currentTarget).data("method"), function(error, result) {
				$(".runMethod").button("reset");
				if (error)
					sAlert.error(error);
				else
					sAlert.success("Done");
			});
		}
	}
});

Template.scripts.helpers({
	getServerScripts: () => {
		let template = Template.instance();
		Meteor.call("getServerScripts", function (error, result) {
			if (result) {
				Session.set("ServerScripts", result);
			}
		});
		return Session.get("ServerScripts");
	}
});
