import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.admin_entityBuildings.onCreated(function () {
  this.searchText = new ReactiveVar('')
})

Template.admin_entityBuildings.onDestroyed(function () {

})

Template.admin_entityBuildings.onRendered(() => {
  $('.actionButton').on('click', function (e) {
    if ($(e.currentTarget).hasClass('addBuilding')) {
      e.stopImmediatePropagation()
      const entityId = FlowRouter.getParam('entityId')
      FlowRouter.go('app.backoffice.buildingsList.newBuildings.entityId', { entityId })
    }
  })
})

Template.admin_entityBuildings.events({
  'click .containerLine': (e, t) => {
    const condoId = $(e.currentTarget).attr('condoId')
    FlowRouter.go('app.backoffice.condos.condo_details', { id: condoId })
  },
  'click .trashColumn': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    bootbox.confirm({
      size: 'medium',
      title: 'Confirmation',
      message: 'Voulez-vous vraiment le supprimer ?',
      buttons: {
        'cancel': { label: 'Annuler', className: 'btn-outline-red-confirm' },
        'confirm': { label: 'Confirmer', className: 'btn-red-confirm' }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          const condoId = $(e.currentTarget).attr('condoId')
          if (condoId) {
            Meteor.call('removeCondo', condoId)
          }
        }
      }
    })
  }
})

Template.admin_entityBuildings.helpers({
  searchCallback: () => {
    const t = Template.instance()

    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      t.searchText.set(value)
    }
  },
  getBuildingsList: () => {
    const searchText = Template.instance().searchText.get()
    const regexp = new RegExp(searchText.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    let condoTypeRegexp = []

    if (('Monopropriété').match(regexp)) condoTypeRegexp.push('mono')
    if (('Copropropriété').match(regexp)) condoTypeRegexp.push('copro')
    if (('Résidence étudiante').match(regexp)) condoTypeRegexp.push('etudiante')
    if (('Bureau').match(regexp)) condoTypeRegexp.push('office')

    const entityId = FlowRouter.getParam('entityId')
    const entity = Enterprises.findOne({ _id: entityId })

    const allowedCondos = (entity || {}).condos

    return Condos.find({
      $or: [
        { name: regexp },
        { 'info.address': regexp },
        { 'info.city': regexp },
        { 'info.code': regexp },
        { 'settings.condoType': { $in: condoTypeRegexp } }
      ],
      _id: { $in: allowedCondos }
    }, { sort: { createdAt: -1 } })
  },
  getCondoAdress: (condo) => {
    return condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city
  },
  getTypoOfCondo: (condoType) => {
    if (condoType === 'mono') {
      return 'Monopropriété'
    } else if (condoType === 'copro') {
      return 'Copropropriété'
    } else if (condoType === 'etudiante') {
      return 'Résidence étudiante'
    } else if (condoType === 'office') {
      return 'Bureau'
    } else {
      return '-'
    }
  },
  emptySearch: () => {
    return !Template.instance().searchText.get()
  },
  getBuildingId: (buildingId) => {
    if (!buildingId || buildingId === '-1') {
      return '-'
    }
    return buildingId
  }

})
