import { Template } from "meteor/templating";

Template.admin_entityDetailController.onCreated(function () {
  this.entityId = FlowRouter.getParam('entityId')
  let tab = FlowRouter.getParam('tab')
  this.handler = Meteor.subscribe('admin_userFiles')

  if (!tab || (tab !== 'details' && tab !== 'buildings' && tab !== 'managers' && tab !== 'occupants')) {
    FlowRouter.setParams({ tab: 'details' })
  }
})

Template.admin_entityDetailController.onDestroyed(function () {
})

Template.admin_entityDetailController.onRendered(function () {

})

Template.admin_entityDetailController.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ tab: tabName })
  }
})

Template.admin_entityDetailController.helpers({
  getTemplateName: () => {
    let tab = FlowRouter.getParam('tab')
    if (tab === 'details') {
      return 'admin_entityDetails'
    } else if (tab === 'buildings') {
      return 'admin_entityBuildings'
    } else if (tab === 'managers') {
      return 'admin_entityManagers'
    } else if (tab === 'occupants') {
      return 'admin_entityOccupants'
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('tab')
    if (!tab || (tab !== 'details' && tab !== 'buildings' && tab !== 'managers' && tab !== 'occupants')) {
      FlowRouter.setParams({ tab: 'details' })
    }
    return tab
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready()
  }
})
