import { CommonTranslation } from "/common/lang/lang.js";

var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

function notCondoInCharge(userId, condoId) {
  let gestionnaire = Enterprises.findOne({ 'users.userId': userId });
  gestionnaire = _.map(_.find(gestionnaire.users, function (elem) {
    return elem.userId == userId;
  }).condosInCharge, function (elem) {
    return elem.condoId;
  });
  return _.find(gestionnaire, function (elem) {
    return elem == condoId;
  }) === undefined;
};

Template.admin_entityManagers.onCreated(function () {
  this.searchText = new ReactiveVar('')
  this.handler = Meteor.subscribe('usersProfile')
  const entityId = FlowRouter.getParam('entityId')
  const entity = Enterprises.findOne({ _id: entityId })
  this.entityCondoIds = (entity || {}).condos || []
})

Template.admin_entityManagers.onDestroyed(function () {
})

Template.admin_entityManagers.onRendered(() => {
  $('.actionButton').on('click', function (e) {
    if ($(e.currentTarget).hasClass('addManager')) {
      e.stopImmediatePropagation()
      const entityId = FlowRouter.getParam('entityId')
      FlowRouter.go('app.backoffice.managerList.personalDetails.entityId', { entityId })
    }
  })
})

Template.admin_entityManagers.events({
  'click .invitManagerButton': (event, template) => {
    const entityId = FlowRouter.getParam('entityId')
    FlowRouter.go('app.backoffice.managerList.personalDetails.entityId', { entityId })
  },
  'click .containerLine': (event, template) => {
    let userId = $(event.currentTarget).attr('userId')
    FlowRouter.go('app.backoffice.managerList.managerProfile', { userId: userId, tab: 'buildings' })
  },
  'click .commonColumn.trashColumn > div': function (e, t) {
    e.preventDefault()
    e.stopPropagation()
    const userId = this._id
    const userName = this.firstname + ' ' + this.lastname

    event.stopPropagation();
    let condoContacts = CondoContact.find({ "defaultContact.userId": userId }).fetch();

    if (condoContacts.length) {
      let users = _.filter(Enterprises.findOne({ 'users.userId': userId }).users, function (elem) {
        return elem.userId != userId;
      });
      var dropdown = '';
      _.each(users, function (user) {
        dropdown += `<option value='${user.userId}'>${user.firstname} ${user.lastname}</option>'`;
      });
      sAlert.warning('Ce gestionnaire est un contact par défaut');
      bootbox.confirm({
        size: 'medium',
        title: 'Définir un nouveau contact par défaut',
        message: `<select id='select' class='select-target form-control' style='width: 100%'>${dropdown}</select>`,
        backdrop: true,
        callback: function (result) {
          if (result) {
            var newDefault = $('#select').prop('value');
            bootbox.confirm({
              size: "medium",
              title: "Confirmation",
              message: "Voulez-vous vraiment le supprimer ?",
              buttons: {
                'cancel': { label: "Annuler", className: "btn-outline-red-confirm" },
                'confirm': { label: "Confirmer", className: "btn-red-confirm" }
              },
              backdrop: true,
              callback: function (result) {
                if (result) {
                  const user = Meteor.users.findOne(userId);
                  _.each(condoContacts, function (condoContact) {
                    Meteor.call('changeDefaultContact', condoContact._id, newDefault);
                    if (notCondoInCharge(newDefault, condoContact.condoId))
                      Meteor.call('addCondoInCharge', user.identities.gestionnaireId, newDefault, {
                        access: [1, 2],
                        condoId: condoContact.condoId,
                        notifications: {
                          actualites: true,
                          classifieds: true,
                          edl: true,
                          forum_forum: true,
                          forum_syndic: true,
                          incidents: true,
                          messenger: true,
                          new_user: true,
                        },
                      }, null)
                  });
                  Meteor.call('removeCondoContactReferences', user._id);
                  Meteor.call('removeManagerEnterprise', user.identities.gestionnaireId, user._id);
                }
              }
            });
          }
        }
      });
    }
    else {
      bootbox.confirm({
        size: "medium",
        title: "Confirmation",
        message: "Voulez-vous vraiment le supprimer ?",
        buttons: {
          'cancel': { label: "Annuler", className: "btn-outline-red-confirm" },
          'confirm': { label: "Confirmer", className: "btn-red-confirm" }
        },
        backdrop: true,
        callback: function (result) {
          if (result) {
            const user = Meteor.users.findOne(userId);
            if (user) {
              Meteor.call('removeCondoContactReferences', user._id);
            }
            Meteor.call('removeManagerEnterprise', user.identities.gestionnaireId, user._id);
          }
        }
      });
    }
  }
})

Template.admin_entityManagers.helpers({
  getManagers: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");
    let userIds = []
    const condoIds = Template.instance().entityCondoIds

    let users = []
    Enterprises.find({ condos: { $in: condoIds } }).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (!user.isAdmin && user.userId !== Meteor.userId()) {
          const condosInCharge = _.pluck(user.condosInCharge, 'condoId')
          let _userCondosHasMatched = false
          if (_.intersection(condosInCharge, condoIds).length > 0) {
            let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
            if (thisUserProfile) {
              const _userHasMatched =
                thisUserProfile.email.match(regexp) ||
                thisUserProfile.firstname.match(regexp) ||
                thisUserProfile.lastname.match(regexp) ||
                thisUserProfile.tel.match(regexp) ||
                thisUserProfile.tel2.match(regexp) ||
                (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
                (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
                (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
                enterprise.name.match(regexp)
              if (_userHasMatched) {
                let avatar = Avatars.findOne({ _id: thisUserProfile._id })
                if (avatar && avatar.avatar.original) {
                  thisUserProfile.avatar = avatar.avatar.original
                } else {
                  thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
                }
                users.push(thisUserProfile)
              }
            }
          }
        }
      })
    })
    return users
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: () => {
    let userIds = []
    const condoIds = Template.instance().entityCondoIds

    let users = 0
    Enterprises.find({ condos: { $in: condoIds } }).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (!user.isAdmin && user.userId !== Meteor.userId()) {
          const condosInCharge = _.pluck(user.condosInCharge, 'condoId')
          if (_.intersection(condosInCharge, condoIds).length > 0) {
            let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
            if (thisUserProfile) {
              users++
            }
          }
        }
      })
    })
    return users
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitManagerButton').click()
    }
  },
  getCondoIdsRights: (allowedCondoIds) => {
    return (Template.instance().condoId.get() === 'all') ? allowedCondoIds : [Template.instance().condoId.get()]
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready()
  }
})
