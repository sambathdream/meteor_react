import { FileManager } from '/client/components/fileManager/filemanager.js';
import mime from 'mime-types'

var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

const mimeType = (type) => {
  return type.split('/')[0]
}

const getImageByExtension = (ext) => {
  const basePath = '/img/icons/svg/'

  switch (ext) {
    case 'zip':
      return `${basePath}extensionzip.svg`
    case 'docx':
    case 'doc':
      return `${basePath}extensiondoc.svg`
    case 'pptx':
    case 'ppt':
      return `${basePath}extensionppt.svg`
    case 'xls':
    case 'xlsx':
      return `${basePath}extensionxls.svg`
    case 'pdf':
      return `${basePath}extensionpdf.svg`
    case 'txt':
      return `${basePath}extensiontxt.svg`
    default:
      return `${basePath}extensionunknown.svg`
  }
}

function saveFile(t, file, logoType) {
  file.id = `file-${getRandomString()}`
  if (file.type && mimeType(file.type) === 'image') {
    file.preview = {
      type: 'image',
      url: window.URL.createObjectURL(file)
    }
  } else {
    file.preview = {
      type: 'file'
    }
  }

  if (logoType === 'mobile') {
    t.logoMobile.set(file)
  } else {
    t.logoWeb.set(file)
  }


  let fileSrc = null
  const re = /gif|jpe?g|png|bmp/i
  if (re.test(mime.extension(file.type))) {
    fileSrc = file.preview.url
    var reader = new FileReader();
    reader.onload = function (e) {
      t.$('.attachmentDisplay-' + logoType)
        .attr('src', e.target.result)
        .css({ 'max-height': 'calc(100% - 32px)', 'height': '' })
    };
    reader.readAsDataURL(file);
    t.$('.attachmentName-' + logoType).text(file.name).parent().css('display', 'block')
  }
  else {
    sAlert.error('Selectionnez une image uniquement')
  }
}

Template.admin_entityDetails.onCreated(function () {
  this.fm = new FileManager(UserFiles)
  this.canSubmit = new ReactiveVar(true)

  const entityId = FlowRouter.getParam('entityId')
  const entity = Enterprises.findOne({ _id: entityId })
  this.formEntity = new ReactiveDict()
  this.formEntity.setDefault({
    entityName: entity.name || null,
    siretNumber: entity.info.siret || null,
    legalGuardian: entity.info.chef || null,
    email: entity.info.email || null,
    phoneCode: entity.info.telCode || '33',
    phone: entity.info.tel || null,
    domain: entity.settings.domain || '',
    validCopro: entity.settings.validUsers.copro || false,
    validMono: entity.settings.validUsers.mono || false,
    validOffice: entity.settings.validUsers.office || false,
    validStudent: entity.settings.validUsers.etudiante || false,
  })

  this.logoMobile = new ReactiveVar(entity.logo.fileMobileId || null)
  this.logoWeb = new ReactiveVar(entity.logo.fileId || null)
  this.goodPhone = false
  this.adressInfos = new ReactiveVar({
    address: entity.info.address + ', ' + entity.info.code + ' ' + entity.info.city + ', France',
    city: entity.info.city,
    code: entity.info.code,
    country: "France",
    id: "aaa",
    lat: 0,
    lng: 0,
    road: entity.info.address.split(' ').splice(1).join(' '),
    street_number: entity.info.address.split(' ')[0]
  })
  this.isAdressError = new ReactiveVar(false)
})

Template.admin_entityDetails.onDestroyed(function () {
})

Template.admin_entityDetails.onRendered(() => {
  let t = Template.instance()
  let previousFileName = null
  t.$('.attachmentContainer').on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    e.preventDefault()
    e.stopPropagation()
  })
  .on('dragover dragenter', function (e) {
    if (!$(e.currentTarget).children('.dragArea').hasClass('is-dragover')) {
      $(e.currentTarget).children('.dragArea').addClass('is-dragover')
    }
  })
  .on('dragleave dragend drop', function (e) {
    $(e.currentTarget).children('.dragArea').removeClass('is-dragover')
  })
  .on('drop', function (e) {
    e.stopImmediatePropagation()
    const type = $(e.currentTarget).children('.dragArea').data('type')
    saveFile(t, e.originalEvent.dataTransfer.files[0], type)
  })

  const entityId = FlowRouter.getParam('entityId')
  const entity = Enterprises.findOne({ _id: entityId })

  if (entity.logo.fileId) {
    const webFile = UserFiles.findOne({ _id: entity.logo.fileId })
    if (webFile) {
      t.$('.attachmentDisplay-web')
        .attr('src', webFile.link())
        .css({ 'max-height': 'calc(100% - 32px)', 'height': '' })
      t.$('.attachmentName-web').text(webFile.name).parent().css('display', 'block')
    }
  }
  if (entity.logo.fileMobileId) {
    const mobileFile = UserFiles.findOne({ _id: entity.logo.fileMobileId })
    if (mobileFile) {
      t.$('.attachmentDisplay-mobile')
        .attr('src', mobileFile.link())
        .css({ 'max-height': 'calc(100% - 32px)', 'height': '' })
      t.$('.attachmentName-mobile').text(mobileFile.name).parent().css('display', 'block')
    }
  }

  $('.actionButton').on('click', function (e) {
    if (!$(e.currentTarget).hasClass('saveChanges')) {
      return
    }
    e.stopImmediatePropagation()
    if (t.canSubmit.get() === true) {
      $(e.currentTarget).button('loading')
      const entityId = FlowRouter.getParam('entityId')
      let form = t.formEntity.all()
      form.logoWeb = t.logoWeb.get()
      form.logoMobile = t.logoMobile.get()
      form.adressInfos = t.adressInfos.get()

      const newLogoWeb = (typeof form.logoWeb === 'object') ? form.logoWeb : null
      const newLogoMobile = (typeof form.logoMobile === 'object') ? form.logoMobile : null

      const __files = _.without([newLogoWeb, newLogoMobile], null)
      const hasLogoWeb = !!newLogoWeb
      const hasLogoMobile = !!newLogoMobile
      t.fm.batchUpload(__files).then(files => {
        if (newLogoWeb) {
          form.logoWeb = hasLogoWeb ? files[0]._id : null
        }
        if (newLogoMobile) {
          form.logoMobile = hasLogoMobile ? (hasLogoWeb ? files[1]._id : files[0]._id) : null
        }
        Meteor.call('updateEntity', entityId, form, (error, result) => {
          $(e.currentTarget).button('reset')
          if (error) {
            sAlert.error(error)
          } else {
            sAlert.success('Modifications enregistrées')
          }
        })
      }).catch(err => {
        $(e.currentTarget).button('reset')
        sAlert.error('Une erreur est survenu pendant l\'upload des logos')
      })
    }
  })
})

Template.admin_entityDetails.events({
  'change .inputLogo': (e, t) => {
    const logoType = $(e.currentTarget).data('type')
    const file = e.currentTarget.files[0]
    saveFile(t, file, logoType)
    e.currentTarget.value = ''
  },
  'click .attachmentContainer': (e, t) => {
    t.$(e.currentTarget).next('.inputLogo').click()
  },
})

function updateSubmit(template) {
  let canSubmit = false
  const form = template.formEntity.all()
  const adressInfos = template.adressInfos.get()
  canSubmit =
    form.entityName !== null &&
    form.legalGuardian !== null &&
    adressInfos && adressInfos.street_number &&
    form.email !== null && (Isemail.validate(form.email) === true) &&
    (form.phone === null || form.phone === '' || template.goodPhone === true)
  if (canSubmit) {
    $('.saveChanges').removeClass('disabled')
  } else {
    $('.saveChanges').addClass('disabled')
  }
  template.canSubmit.set(canSubmit)
}



Template.admin_entityDetails.helpers({
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  getFormEntity: (key) => {
    const t = Template.instance()
    if (key === 'adress') {
      return () => t.adressInfos.get() || {}
    } else {
      return () => t.formEntity.get(key) || ''
    }
  },
  onCheckboxClick: (key) => {
    const t = Template.instance()
    return () => () => {
      const value = t.formEntity.get(key)
      t.formEntity.set(key, !value)
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()
    return () => (value) => {
      if (key === 'phone') {
        let number = '+' + t.formEntity.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.formEntity.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.formEntity.set(key, !value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.formEntity.set(key, !value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'adress') {
        if (value && value.error) {
          sAlert.error(value.error)
          t.isAdressError.set(true)
        } else {
          t.isAdressError.set(false)
          t.adressInfos.set(value)
        }
      } else {
        t.formEntity.set(key, value)
      }
      updateSubmit(t)
    }
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.formEntity.get('phone')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.formEntity.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          }
        } catch (e) {
          t.goodPhone = false
        }
      }
      t.formEntity.set(key, !selected ? null : selected)
      updateSubmit(t)
    }
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.formEntity.get(key)
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.formEntity.get(key)
        let number = '+' + t.formEntity.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  getInputGoogleAdress: () => {
    return Template.instance().adressInfos;
  },
  isAdressError: () => {
    return Template.instance().isAdressError.get()
  }
})
