Template.admin_entityController.onCreated(function () {
  let tab = FlowRouter.getParam('tab')
  this.handler = Meteor.subscribe('admin_entities')
  this.handler2 = Meteor.subscribe('PhoneCode')

})

Template.admin_entityController.onDestroyed(function () {

})

Template.admin_entityController.onRendered(() => {

})

Template.admin_entityController.events({
  'click .addEntity': (e, t) => {
    FlowRouter.go('app.backoffice.createEntity')
  },
  'click #backbutton': (e, t) => {
    FlowRouter.go('app.backoffice.entityList')
  }
})

Template.admin_entityController.helpers({
  getButtonClass: () => {
    const tab = FlowRouter.getParam('tab')
    switch (tab) {
      case 'details':
        return 'saveChanges'
      case 'buildings':
        return 'addBuilding'
      case 'managers':
        return 'addManager'
      case 'occupants':
        return 'addOccupant'

      default:
        break;
    }
  },
  setButtonName: () => {
    const tab = FlowRouter.getParam('tab')
    let value = ''
    switch (tab) {
      case 'details':
        value = 'Sauvegarder'
        break;
      case 'buildings':
        value = 'Ajouter un immeuble'
        break;
      case 'managers':
        value = 'Ajouter un gestionnaire'
        break;
      case 'occupants':
        value = 'Ajouter un occupant'
        break;

      default:
        break;
    }

    $('.actionButton').text(value)
    setTimeout(() => {
      $('.actionButton').text(value)
    }, 200);
  },
  isDetailPage: () => {
    return FlowRouter.getRouteName() === 'app.backoffice.entityList.entityId.tab'
  },
  getEntityName: () => {
    const entityId = FlowRouter.getParam('entityId')
    if (entityId) {
      const entity = Enterprises.findOne({ _id: entityId })
      if (entity) {
        return entity.name
      }
    }
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready() && Template.instance().handler2.ready()
  },
  getTemplateName: () => {
    let entityId = FlowRouter.getParam('entityId')
    if (!!entityId) {
      return 'admin_entityDetailController'
    } else {
      return 'admin_entityList'
    }
  },
})
