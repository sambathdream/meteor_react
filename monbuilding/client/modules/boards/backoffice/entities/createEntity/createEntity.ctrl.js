import { FileManager } from '/client/components/fileManager/filemanager.js';

Template.admin_createEntity.onCreated(function () {
  this.subscribe('PhoneCode');
  this.fm = new FileManager(UserFiles)

  this.currentStep = new ReactiveVar(1)
  this.stepState = new ReactiveDict()
  this.stepState.setDefault({
    1: false,
    2: false,
    3: false,
    4: false,
    5: false
  })
  this.entity = new ReactiveVar(null)
  this.buildings = new ReactiveVar(null)
  this.createdCondoIds = new ReactiveVar(null)
  this.createdEntityId = new ReactiveVar(null)
})

Template.admin_createEntity.onDestroyed(function () {
})

Template.admin_createEntity.onRendered(() => {
})

Template.admin_createEntity.events({
  'click #backButtonMaster': (e, t) => {
    FlowRouter.go('app.backoffice.enterprise')
  },
  'click .step': (e, t) => {
    const stepNumber = $(e.currentTarget).text()
    if (t.stepState.get(stepNumber) === true && (t.currentStep.get() <= 2 && stepNumber > 0) || (t.currentStep.get() >= 3 && stepNumber >= 3)) {
      t.currentStep.set(parseInt(stepNumber))
    }
  }
})

Template.admin_createEntity.helpers({
  canSubmit: () => {
    return Template.instance().stepState.get(Template.instance().currentStep.get())
  },
  currentStep: (stepNumber) => {
    return Template.instance().currentStep.get() === stepNumber
  },
  isStepEnded: (stepNumber) => {
    return Template.instance().stepState.get(stepNumber)
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  setStepStateCb: (stepNumber) => {
    const t = Template.instance()
    return () => (state) => {
      t.stepState.set(stepNumber, state)
    }
  },
  saveEntity: () => {
    const t = Template.instance()
    return () => (form) => {
      t.$('.saveEntity').button('loading')
      const __files = _.without([form.logoWeb, form.logoMobile], null)
      const hasLogoWeb = !!form.logoWeb
      const hasLogoMobile = !!form.logoMobile
      t.fm.batchUpload(__files).then(files => {
        form.logoWeb = hasLogoWeb ? files[0]._id : null
        form.logoMobile = hasLogoMobile ? (hasLogoWeb ? files[1]._id : files[0]._id) : null
        Meteor.call('createEntity', form, (error, result) => {
          t.$('.saveEntity').button('reset')
          if (error) {
            sAlert.error(error)
          } else {
            FlowRouter.go('app.backoffice.entityList')
          }
        })
      }).catch(err => {
        t.$('.saveEntity').button('reset')
        sAlert.error('Une erreur est survenu pendant l\'upload des logos')
      })
    }
  },
  goNextCb: (stepNumber) => {
    const t = Template.instance()
    return () => (form) => {
      if (t.stepState.get(t.currentStep.get()) && (stepNumber - 1) === t.currentStep.get()) {
        if (t.stepState.get(stepNumber - 1) === true) {
          if (t.currentStep.get() !== 2) {
            if (t.currentStep.get() === 1) {
              t.entity.set(form)
            }
            t.currentStep.set(stepNumber)
          } else {
            const entity = t.entity.get()
            t.buildings.set(form)
            bootbox.confirm({
              size: "medium",
              title: "Confirmation",
              message: "En validant, vous ne pourrez plus revenir en arrière. De plus l'entité et les immeubles seront créés et validé.",
              buttons: {
                'cancel': { label: "Annuler", className: "btn-outline-red-confirm" },
                'confirm': { label: "Confirmer", className: "btn-red-confirm" }
              },
              backdrop: true,
              callback: function (result) {
                if (result) {
                  const __files = _.without([entity.logoWeb, entity.logoMobile], null)
                  const hasLogoWeb = !!entity.logoWeb
                  const hasLogoMobile = !!entity.logoMobile
                  t.fm.batchUpload(__files).then(files => {
                    entity.logoWeb = hasLogoWeb ? files[0]._id : null
                    entity.logoMobile = hasLogoMobile ? (hasLogoWeb ? files[1]._id : files[0]._id) : null
                    Meteor.call('createEntity', entity, (error, entityId) => {
                      if (!error && entityId) {
                        Meteor.call('addBuildingsToEntity', entityId, form, (error, condoIds) => {
                          if (!error && condoIds) {
                            t.createdCondoIds.set(condoIds)
                            t.createdEntityId.set(entityId)
                            t.stepState.set({ 3: true, 4: false })
                            t.currentStep.set(stepNumber)
                          }
                        })
                      }
                    })
                  }).catch(err => {
                    sAlert.error('Une erreur est survenue pendant l\'upload des logos')
                  })
                }
              }
            })
          }
        }
      }
    }
  },
  getCurrentStep: () => {
    return Template.instance().currentStep.get()
  },
  getNewCondosIds: () => {
    return Template.instance().createdCondoIds.get()
  },
  getEntityId: () => {
    return Template.instance().createdEntityId.get()
  }
})
