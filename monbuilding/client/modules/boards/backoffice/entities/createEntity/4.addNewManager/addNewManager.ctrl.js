
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.admin_addNewManager.onCreated(function () {
  this.subscribe('PhoneCode');

  this.condoId = FlowRouter.getParam('condoId') || null
  this.searchText = new ReactiveVar('')
  this.canSubmit = new ReactiveVar(false)
  this.form = new ReactiveDict()
  this.allCondosSelected = false
  this.showCustimizeRights = new ReactiveVar(false)

  this.goodPhone = false
  this.goodLandline = false
  this.customizeRoleId = null
  this.customizeCondoId = null
  this.customizeIndex = null
  this.form.setDefault({
    entity: Template.currentData().entityId,
    firstname: null,
    lastname: null,
    email: null,
    phone: null,
    phoneCode: '33',
    phoneCodeLandline: '33',
    landline: null,
    condos: [null],
    roles: [null]
  })
  this.customRights = [null]
})

Template.admin_addNewManager.onDestroyed(function () {
})

Template.admin_addNewManager.onRendered(() => {
  let t = Template.instance()
  let previousFileName = null

  $('.nextStep').on('click', function (e) {
    if ($(e.currentTarget).hasClass('4')) {
      e.stopImmediatePropagation()
      t.data.goNextCb()
    }
  })
})

Template.admin_addNewManager.events({
  'hidden.bs.modal #modal_rights_manager': function (event, template) {
    template.showCustimizeRights.set(false)
    template.customizeRoleId = null
    template.customizeCondoId = null

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_rights_manager': function (event, template) {
    let modal = $(event.currentTarget);
    window.location.hash = "#modal_rights_manager";
    window.onhashchange = function () {
      if (location.hash != "#modal_rights_manager") {
        modal.modal('hide');
      }
    }
  },
  'click .addIcon': (event, template) => {
    let form = template.form.get('roles')
    let formCondo = template.form.get('condos')
    let isAllSelected = true
    formCondo.forEach((condo, index) => {
      if (condo === null || form[index] === null) {
        isAllSelected = false
      }
    })
    if (isAllSelected === true && template.allCondosSelected === false) {
      form.push(null)
      formCondo.push(null)
      template.form.set('roles', form)
      template.form.set('condos', formCondo)
      template.customRights.push(null)
      updateSubmit(template)
    }
  }
})

function updateSubmit(template) {
  let canSubmit = false
  const form = template.form.all()
  canSubmit =
    form.entity !== null &&
    form.firstname !== null &&
    form.lastname !== null &&
    form.email !== null && (Isemail.validate(form.email) === true) &&
    (form.phone === null || form.phone === '' || template.goodPhone === true) &&
    (form.landline === null || form.landline === '' || template.goodLandline === true) &&
    form.condos[0] !== null &&
    form.roles[0] !== null
  template.canSubmit.set(canSubmit)
}

Template.admin_addNewManager.helpers({
  updateNextStep: (count) => {
    Template.instance().data.setStepStateCb(!!Enterprises.findOne({ _id: Template.instance().form.get('entity') }).users.length)
  },
  canAddMoreCondo: () => {
    let template = Template.instance()
    let form = template.form.get('roles')
    let formCondo = template.form.get('condos')
    let isAllSelected = true
    formCondo.forEach((condo, index) => {
      if (condo === null || form[index] === null) {
        isAllSelected = false
      }
    })
    if (isAllSelected === true && template.allCondosSelected === false) {
      return true
    }
    return false
  },
  mainCondoIdCb: () => {
    let template = Template.instance()
    return (condoId) => {
      template.condoId.set(condoId)
    }
  },
  onSelectRole: (index) => {
    const t = Template.instance()

    return () => selected => {
      let form = t.form.get('roles')
      form[index] = !selected ? null : selected
      t.customRights[index] = null
      t.form.set('roles', form)
      updateSubmit(t)
    }

  },
  onSelectCondo: (index) => {
    const t = Template.instance()

    return () => selected => {
      let formCondo = t.form.get('condos')
      formCondo[index] = !selected ? null : selected
      t.customRights[index] = null
      t.form.set('condos', formCondo)
      updateSubmit(t)
    }
  },
  onSelectEntity: () => {
    const t = Template.instance()

    return () => selected => {
      t.form.clear()
      t.form.setDefault({
        entity: null,
        firstname: null,
        lastname: null,
        email: null,
        phone: null,
        phoneCode: '33',
        phoneCodeLandline: '33',
        landline: null,
        condos: [null],
        roles: [null]
      })
      t.form.set('entity', selected)
      updateSubmit(t)
    }
  },
  canCustomizeRights: (index) => {
    const t = Template.instance()

    let form = t.form.get('roles')
    let formCondo = t.form.get('condos')

    if (!!form[index] && !!formCondo[index]) {
      return true
    } else {
      return false
    }
  },
  customizeRightsOn: () => {
    return Template.instance().showCustimizeRights.get()
  },
  customizeRights: (index) => {
    const t = Template.instance()

    return () => e => {
      t.customizeIndex = index
      t.customizeRoleId = t.form.get('roles')[index]
      t.customizeCondoId = t.form.get('condos')[index]
      t.showCustimizeRights.set(true)
      Meteor.setTimeout(function () {
        $("#modal_rights_manager").modal('show')
      }, 500);
    }
  },
  getCustomRights: () => {
    let t = Template.instance()
    return t.customRights[t.customizeIndex]
  },
  saveModalRights: () => {
    let t = Template.instance()

    return (newRights) => {
      t.customRights[t.customizeIndex] = newRights
      t.customizeRoleId = null
      t.customizeCondoId = null
      $("#modal_rights_manager").modal('hide')
    }

  },
  getCustomizeRoleId: () => {
    return Template.instance().customizeRoleId
  },
  getCustomizeCondoId: () => {
    return Template.instance().customizeCondoId
  },
  selectedCondo: () => {
    return Template.instance().form.get('condos')
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.form.get('phone')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          }
        } catch (e) {
          t.goodPhone = false
        }
      } else if (key === 'phoneCodeLandline') {
        let number = '+' + selected + t.form.get('landline')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set('landline', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          }
        } catch (e) {
          t.goodLandline = false
        }
      }
      t.form.set(key, !selected ? null : selected)
      updateSubmit(t)
    }
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.form.get(key)
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.form.get(key)
        let number = '+' + t.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      case 'landline': {
        const value = t.form.get(key)
        let number = '+' + t.form.get('phoneCodeLandline') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodLandline = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodLandline = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'phone') {
        let number = '+' + t.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.form.set(key, !value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.form.set(key, !value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'landline') {
        let number = '+' + t.form.get('phoneCodeLandline') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.form.set(key, !value ? null : value)
            t.goodLandline = false
          }
        } catch (e) {
          t.form.set(key, !value ? null : value)
          t.goodLandline = false
        }
      } else {
        t.form.set(key, !value ? null : value)
      }
      updateSubmit(t)
    }
  },
  getEntity: () => {
    return Enterprises.find({ _id: Template.currentData().entityId }, { fields: { condos: 1 } })
  },
  getCiviliteOptions: () => {
    return [
      { text: 'Monsieur', id: 'm' },
      { text: 'Madame', id: 'mme' }
    ]
  },
  getEntityOptions: () => {
    let enterprises = []
    Enterprises.find({ _id: Template.currentData().entityId }).forEach(enterprise => {
      enterprises.push({
        text: enterprise.name,
        id: enterprise._id
      })
    })
    return enterprises
  },
  getForm: (key, index) => {
    const t = Template.instance()

    if (index === null) {
      return () => t.form.get(key) || ''
    } else {
      return () => t.form.get(key)[index] || ''
    }
  },
  isEntitySelected: () => {
    return !!Template.instance().form.get('entity')
  },
  getroleOptions: (condoId) => {
    const condo = Condos.findOne({ _id: condoId }, { fields: { 'settings': true } })
    const condoType = condo && (condo.settings.condoType === 'etudiante' ? 'student' : condo.settings.condoType)
    let listRoles = DefaultRoles.find({ for: 'manager', ['forCondoType.' + condoType]: true })
    let roles = []
    if (listRoles) {
      listRoles.forEach(role => {
        roles.push({
          text: role.name,
          id: role._id
        })
      })
    }
    return roles
  },
  getCondoOptions: (index) => {
    let enterprise = Template.instance().form.get('entity')
    let condos = []
    if (enterprise) {
      let condosId = Enterprises.findOne({ _id: enterprise }).condos
      let alreadySelectedCondo = Template.instance().form.get('condos')
      if (index === -1) {
        const result = (condosId.length - alreadySelectedCondo.length) > 0
        Template.instance().allCondosSelected = !(condosId.length - alreadySelectedCondo.length) > 0
        return result
      }
      alreadySelectedCondo.splice(index, 1)
      Condos.find({ _id: { $in: condosId } }).forEach(condo => {
        if (!_.contains(alreadySelectedCondo, condo._id)) {
          condos.push({
            text: condo.getName(),
            id: condo._id
          })
        }
      })
    }
    return condos
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady();
  },
  setOpacity: () => {
    setTimeout(() => {
      $('.displayCondoList').css('opacity', 1)
    }, 1);
  },
  getClasses: () => {
    let classes = 'danger saveAndAddNewManager'
    if (!Template.instance().canSubmit.get()) {
      classes += ' disabled'
    }
    return classes
  },
  saveAndAddManager: () => {
    const template = Template.instance()
    return () => {
      if (template.canSubmit.get() === true) {
        let form = template.form.all()
        let customRights = template.customRights
        Meteor.call('createNewManagerFromManagerSide', form, customRights, (error, result) => {
          if (!error && result) {
          } else {
            sAlert.error(error)
          }
        })
      }
    }
  },
  getSavedManagers: () => {
    return Enterprises.findOne({ _id: Template.instance().form.get('entity') }).users
  },
  getDisplayedValue: (value) => {
    return (value && value.length) ? value : '-'
  },
  getEmailAddr: (userId) => {
    return Meteor.users.findOne({ _id: userId }).emails[0].address
  },
  getBuildingName: (condoId) => {
    const condo = Condos.findOne({ _id: condoId })
    return condo ? condo.getName() : '-'
  },
  getUserRole: (userId, condoId) => {
    let userRight = UsersRights.findOne({
      $and: [
        { "userId": userId },
        { "condoId": condoId }
      ]
    })
    let defaultRoleName = 'non défini'
    if (userRight && userRight.defaultRoleId) {
      let defaultRole = DefaultRoles.findOne({ _id: userRight.defaultRoleId })
      if (defaultRole && defaultRole.name) {
        defaultRoleName = defaultRole.name
      }
    }
    return defaultRoleName
  }
})
