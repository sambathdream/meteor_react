import { FileManager } from '/client/components/fileManager/filemanager.js';

function removeBorder(detailId) {
	$("#spanPicto" + detailId).css("border", "");
	$("#inputFr" + detailId).css("border", "");
	$("#inputEn" + detailId).css("border", "");
}

Template.createEntity_messenger_manager_incidentArea_modal.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.fm = new FileManager(UserFiles, {condoId: this.condoId, messengerArea: true});
	this.isOpen = new ReactiveVar(false);
	this.isAddingElem = new ReactiveVar(false);
});

Template.createEntity_messenger_manager_incidentArea_modal.onRendered(function() {
});

Template.createEntity_messenger_manager_incidentArea_modal.events({
	'hidden.bs.modal #modal_messenger_manager_incidentArea': function(event, template) {
		template.isOpen.set(false);
		template.fm.clearFiles()
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_messenger_manager_incidentArea': function(event, template) {
		template.isOpen.set(true);
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_messenger_manager_incidentArea";
		window.onhashchange = function() {
			if (location.hash != "#modal_messenger_manager_incidentArea"){
				modal.modal('hide');
			}
		}
	},
	'click .createNewArea' (event, template) {
		template.isAddingElem.set(true);
		Meteor.call("createNewIncidentArea", template.condoId, function(error, result) {
			if (!error)
				template.isAddingElem.set(false);
		});
	},
	'click .removeArea' (event, template) {
		event.preventDefault();
		event.stopPropagation();
		let areaId = $(event.currentTarget).attr("areaId");
		$(".loader" + areaId).css("display", "");
		Meteor.call("deleteIncidentArea", template.condoId, areaId, function(error, result) {
		});
	},
	'click .typeManagerBackofficeModal' (event, template) {
		let areaId = $(event.currentTarget).attr("areaId");
		$('#inputPictoEdit' + areaId).click();
	},
	'click #save' (event, template) {
		$("#save").button("loading");
		let area = $(".allArea");
		$(".loader").css("display", "")
		$(".fail").css("display", "none");
		$(".done").css("display", "none");
		let condoId = template.condoId;
		area.each(function(index, elem) {
			let areaId = $(elem).attr("areaId");
			removeBorder(areaId);
			let pictureLink = $("#spanPicto" + areaId).attr('fileLink');
			let value_fr = $("#inputFr" + areaId).val();
			let value_en = $("#inputEn" + areaId).val();
			Meteor.call("saveIncidentAreaForCondo", condoId, {areaId, pictureLink, value_fr, value_en}, function(error, result) {
				$(".loader" + areaId).css("display", "none");
				if (error) {
					if (error.error == 531) {
						let errors = error.reason.split("|");
						_.each(errors, function(elem) {
							$("#" + elem +  areaId).css("border", "2px solid red");
						})
					}
					$(".fail" + areaId).css("display", "");
				}
				else
					$(".done" + areaId).css("display", "");
			})
		}).promise().done(function() { $("#save").button("reset"); })
	},
	'change [name="inputPicto"]': function(event, template) {
		if (event.currentTarget.files) {
			var elapsed = 0;
			let areaId = $(event.currentTarget).attr("areaId");
			$(".globalLoader" + areaId).css("display", "");

			let id = template.fm.insert(event.currentTarget.files[0]);

			var timer = setInterval(function() {
				if (template.fm.getFileList()[id].done) {
					clearInterval(timer);
					var waitForImage = setInterval(function() {
						if (UserFiles.findOne(template.fm.getFileList()[id].file._id)) {
							let thisFile = UserFiles.findOne(template.fm.getFileList()[id].file._id).link();
							$(".globalLoader" + areaId).addClass("p" + 100);
							$(".fileLoader" + areaId).text(100 + "%");


							if (thisFile) {
								let absoluteLinkRe = /.*(\/download.*)/ig;
								let reResult = absoluteLinkRe.exec(thisFile);
								if (reResult[1]) {
									let background = "url('" + reResult[1] + "') top left no-repeat #dffbff";
									$("#spanPicto" + areaId).css('background', background);
									$("#spanPicto" + areaId).attr('fileLink', reResult[1]);
									$("#spanPicto" + areaId).children().first().removeClass("show_camera_edit");
									setTimeout(function() {
										$(".globalLoader" + areaId).css("display", "none");
									}, 500);
								}
							}
							clearInterval(waitForImage);
						}
					}, 100);
				}
				if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
					$(".globalLoader" + areaId).removeClass("p" + elapsed);
					elapsed = template.fm.getFileList()[id].upload.progress.get();
					$(".globalLoader" + areaId).addClass("p" + elapsed);
					$(".fileLoader" + areaId).text(elapsed + "%");
				}
			}, 100);
		}
	},
});

Template.createEntity_messenger_manager_incidentArea_modal.helpers({
	isOpen: () => {
		return Template.instance().isOpen.get() && IncidentArea.findOne();
	},
	isAddingElement: () => {
		console.log(Template.instance().isAddingElem.get())
		return Template.instance().isAddingElem.get();
	},
	getCondoId: () => {
		return Template.instance().condoId;
	}
});
