Template.admin_buildingsSpecifications.onCreated(function () {
  this.id = new ReactiveVar(null)
  this.showBackButtom = new ReactiveVar(false)
})

Template.admin_buildingsSpecifications.onDestroyed(function () {
})

Template.admin_buildingsSpecifications.onRendered(() => {
  let t = Template.instance()
  let previousFileName = null

  $('.nextStep').on('click', function (e) {
    if ($(e.currentTarget).hasClass('3')) {
      e.stopImmediatePropagation()
      t.data.goNextCb()
    }
  })
})

Template.admin_buildingsSpecifications.events({
  'click .editThisModule': function (event, template) {
    FlowRouter.setParams({
      id: $(event.currentTarget).attr("condoId"),
      module: $(event.currentTarget).attr("moduleName"),
      moduleParam: $(event.currentTarget).attr("moduleParam")
    })
  },
  'click #backbutton': (e, t) => {
    FlowRouter.setParams({
      id: FlowRouter.getParam('id'),
      module: undefined,
      moduleParam: undefined,
    })
  },
})

Template.admin_buildingsSpecifications.helpers({
  getTemplateCharacteristic: () => {
    let moduleName = FlowRouter.getParam("module");
    let paramName = FlowRouter.getParam("moduleParam");
    if (moduleName && paramName) {
      Template.instance().showBackButtom.set(true)
      return 'createEntity_' + moduleName + "_" + paramName;
    }
    else
      Template.instance().showBackButtom.set(false)
      return "createEntity_characteristics"
  },
  getDatas: () => {
    let moduleName = FlowRouter.getParam("module");
    let paramName = FlowRouter.getParam("moduleParam");
    if (moduleName && paramName) {
    } else {
      return {
        selectedCondo: FlowRouter.getParam('id') || null
      }
    }
  },
  showBackButtom: () => {
    return Template.instance().showBackButtom.get()
  },
  condoIdCb: () => {
    const t = Template.instance()

    return (selected) => {
      FlowRouter.setParams({
        id: selected,
        module: FlowRouter.getParam('module'),
        moduleParam: FlowRouter.getParam('moduleParam'),
      })
    }
  },
  getCondoIds: () => {
    return Template.currentData().condoIds
  },
})
