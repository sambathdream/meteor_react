Template.createEntity_other_customEmail.onCreated(function () {
  this.condoId = FlowRouter.getParam("id")
  this.value = new ReactiveVar('')
});

Template.createEntity_other_customEmail.onRendered(function () {
});

Template.createEntity_other_customEmail.events({
  'click #saveCustomEmail': function(event, template) {
    let value = template.value.get()
    let condoId = template.condoId
    Meteor.call('saveCondoCustomEmail', condoId, value, function(error, result) {
      if (!error) {
        sAlert.success('Enregistré')
      } else {
        sAlert.error(error)
      }
    })
  }
});

Template.createEntity_other_customEmail.helpers({
  getValue: () => {
    let condo = Condos.findOne(Template.instance().condoId)
    if (condo && condo.settings && condo.settings.customEmail) {
      Template.instance().value.set(condo.settings.customEmail)
      return condo.settings.customEmail
    } else {
      Template.instance().value.set('')
      return ''
    }
  },
  onBlur: () => {
    const instance = Template.instance();
    return (val) => {
      const current = instance.value.get()
      if (val !== current) {
        instance.value.set(val)
      }
    }
  }
});
