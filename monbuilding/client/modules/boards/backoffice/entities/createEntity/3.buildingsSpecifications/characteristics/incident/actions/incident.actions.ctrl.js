let timers = [];

function initiateSaver(name) {
	if (!($("#noty_topRight_layout_container")[0])) {
		$("#noty_topRight_layout_container").remove();
		$(".display-modules").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">Sauvegarde de ' + name + '..</span></div></div></li></ul>');
		$("#noty-li").animate({"height": "34.4502px"}, "fast");
	}
};

function updateSaver(ret) {
	if (!ret) {
		$("#noty_topRight_layout_container").remove();
	}
	else {
		if (ret.error && ret.error == 403)
			$(".noty_text").html(ret.reason);
		else
			$(".noty_text").html(ret);
		$("#noty-li").css(
			{
				'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
				'border-radius': '5px',
				'border': '1px solid red',
				'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
				'color': 'red',
				'width': '310px',
				'cursor': 'pointer',
				'height': '34.4502px'
			}
		);
		setTimeout(function() {
			$("#noty_topRight_layout_container").remove();
		}, 3000)
	}
};

Template.createEntity_incident_actions.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
});

Template.createEntity_incident_actions.onRendered(function() {

});

Template.createEntity_incident_actions.events({
	'click [name="checkboxVisibleBy"]': (event, template) => {
		let roleId = $(event.currentTarget).attr("id");
		let status = $(event.currentTarget).is(':checked');
		let name = $(event.currentTarget).attr("roleName");
		initiateSaver(name);
		if (timers[roleId].timer != null) {
			clearTimeout(timers[roleId].timer);
			timers[roleId].timer = null;
		}
		let condoId = template.condoId;
		timers[roleId].timer = setTimeout(function() {
			Meteor.call("updateCondosModulesOptions", condoId, {module: "incident", param: "managerActionVisibleBy", toChange: roleId, newValue: status}, function(error, result) {
				updateSaver(error);
			});
		}, 500);
	},
});

Template.createEntity_incident_actions.helpers({
	getCondoId: () => {
		return Template.instance().condoId;
	},
	allReady: () => {
    if (!DefaultRoles.find({}, { fields: { _id: true } }).count() || !Condos.find({}, { fields: { _id: true } }).count() || !Condos.findOne(Template.instance().condoId) || !CondosModulesOptions.findOne({condoId: Template.instance().condoId}))
			return false;
		return true;
	},
	getRoleOccupant: () => {
		let condoType = Condos.findOne(Template.instance().condoId).settings.condoType;
		if (condoType == "etudiante")
			condoType = "student";
		var query = "forCondoType." + condoType;
		let roles = DefaultRoles.find({[query]: true, for: "occupant", name: {$ne: "Occupant"}}).fetch();
		return roles;
	},
	getRolesChecked: () => {
		return CondosModulesOptions.findOne({condoId: Template.instance().condoId}).incident.managerActionVisibleBy;
	},
	isChecked: (rolesChecked, roleId, roleName) => {
		if (!timers[roleId])
			timers[roleId] = {name: roleName, timer: null};
		return _.contains(rolesChecked, roleId);
	},
});
