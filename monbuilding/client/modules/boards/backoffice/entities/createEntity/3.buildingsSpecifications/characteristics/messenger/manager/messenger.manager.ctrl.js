Template.createEntity_messenger_manager.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.selectedType = new ReactiveVar("");
	this.selectedIncidentDetails = new ReactiveVar("");
});

Template.createEntity_messenger_manager.onRendered(function() {

});

Template.createEntity_messenger_manager.events({
	'change [name="typeRadio"]': function(event, template) {
		template.selectedType.set($(event.currentTarget).val());
	},
	'dblclick .details': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
	}
});

Template.createEntity_messenger_manager.helpers({
	getBrokenValue: (elem) => {
		let lang = FlowRouter.getParam("lang") || "fr";
		let ret = elem["value-" + lang];
		ret = ret.replace("/", "/<br>");
		return ret;
	},
	isTypeSelected: () => {
		if (Template.instance().selectedType.get() != "")
			return true;
		return false;
	},
	isIncidentSelected: () => {
		if (Template.instance().selectedType.get() != "") {
			let thisType = IncidentType.findOne(Template.instance().selectedType.get());
			if (thisType && thisType.defaultType == "incident")
				return true;
		}
		return false;
	},
	getDropdownElem: () => {
		let listDropdown = ContactManagement.findOne({condoId: Template.instance().condoId});
		if (!listDropdown) {
			let condoType = Condos.findOne(Template.instance().condoId).settings.condoType
			listDropdown = ContactManagement.findOne({forCondoType: condoType});
		}
		if (listDropdown) {
			let listOfIds = _.find(listDropdown.messengerSet, function(list) {
				return list.id == Template.instance().selectedType.get();
			})
			if (listOfIds)
				return listOfIds.declarationDetailIds;
		}
	},
	getCondoId: () => {
		return Template.instance().condoId;
	},
	allReady: () => {
		if (!IncidentType.find().fetch() || !ContactManagement.find().fetch())
			return false;
		return true;
	},
	getTypeId: () => {
		return Template.instance().selectedType.get();
	}
});
