import { FileManager } from '/client/components/fileManager/filemanager.js';

function removeBorder(detailId) {
	$("#spanPicto" + detailId).css("border", "");
	$("#inputFr" + detailId).css("border", "");
	$("#inputEn" + detailId).css("border", "");
}

Template.createEntity_messenger_manager_incidentDetails_modal.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.fm = new FileManager(UserFiles, {condoId: this.condoId, messengerDetails: true});
	this.isOpen = new ReactiveVar(false);
	this.isAddingElem = new ReactiveVar(false);
});

Template.createEntity_messenger_manager_incidentDetails_modal.onRendered(function() {
});

Template.createEntity_messenger_manager_incidentDetails_modal.events({
	'hidden.bs.modal #modal_messenger_manager_incidentDetails': function(event, template) {
		template.isOpen.set(false);
		template.fm.clearFiles()
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_messenger_manager_incidentDetails': function(event, template) {
		template.isOpen.set(true);
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_messenger_manager_incidentDetails";
		window.onhashchange = function() {
			if (location.hash != "#modal_messenger_manager_incidentDetails"){
				modal.modal('hide');
			}
		}
	},
	'click .createNewDetails' (event, template) {
		template.isAddingElem.set(true);
		Meteor.call("createNewIncidentDetails", template.condoId, function(error, result) {
			if (!error)
				template.isAddingElem.set(false);
		});
	},
	'click .removeDetails' (event, template) {
		event.preventDefault();
		event.stopPropagation();
		let detailsId = $(event.currentTarget).attr("detailsId");
		$(".loader" + detailsId).css("display", "");
		Meteor.call("deleteIncidentDetails", template.condoId, detailsId, function(error, result) {
		});
	},
	'click .typeManagerBackofficeModal' (event, template) {
		let detailsId = $(event.currentTarget).attr("detailsId");
		$('#inputPictoEdit' + detailsId).click();
	},
	'click #save' (event, template) {
		$("#save").button("loading");
		let details = $(".allDetails");
		$(".loader").css("display", "")
		$(".fail").css("display", "none");
		$(".done").css("display", "none");
		let condoId = template.condoId;
		details.each(function(index, elem) {
			let detailsId = $(elem).attr("detailsId");
			removeBorder(detailsId);
			let pictureLink = $("#spanPicto" + detailsId).attr('fileLink');
			let value_fr = $("#inputFr" + detailsId).val();
			let value_en = $("#inputEn" + detailsId).val();
			Meteor.call("saveIncidentDetailsForCondo", condoId, {detailsId, pictureLink, value_fr, value_en}, function(error, result) {
				$(".loader" + detailsId).css("display", "none");
				if (error) {
					if (error.error == 531) {
						let errors = error.reason.split("|");
						_.each(errors, function(elem) {
							$("#" + elem +  detailsId).css("border", "2px solid red");
						})
					}
					$(".fail" + detailsId).css("display", "");
				}
				else
					$(".done" + detailsId).css("display", "");
			})
		}).promise().done(function() { $("#save").button("reset"); })
	},
	'change [name="inputPicto"]': function(event, template) {
		if (event.currentTarget.files) {
			var elapsed = 0;
			let detailsId = $(event.currentTarget).attr("detailsId");
			$(".globalLoader" + detailsId).css("display", "");

			let id = template.fm.insert(event.currentTarget.files[0]);

			var timer = setInterval(function() {
				if (template.fm.getFileList()[id].done) {
					clearInterval(timer);
					var waitForImage = setInterval(function() {
						if (UserFiles.findOne(template.fm.getFileList()[id].file._id)) {
							let thisFile = UserFiles.findOne(template.fm.getFileList()[id].file._id).link();
							$(".globalLoader" + detailsId).addClass("p" + 100);
							$(".fileLoader" + detailsId).text(100 + "%");


							if (thisFile) {
								let absoluteLinkRe = /.*(\/download.*)/ig;
								let reResult = absoluteLinkRe.exec(thisFile);
								if (reResult[1]) {
									let background = "url('" + reResult[1] + "') top left no-repeat #dffbff";
									$("#spanPicto" + detailsId).css('background', background);
									$("#spanPicto" + detailsId).attr('fileLink', reResult[1]);
									$("#spanPicto" + detailsId).children().first().removeClass("show_camera_edit");
									setTimeout(function() {
										$(".globalLoader" + detailsId).css("display", "none");
									}, 500);
								}
							}
							clearInterval(waitForImage);
						}
					}, 100);
				}
				if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
					$(".globalLoader" + detailsId).removeClass("p" + elapsed);
					elapsed = template.fm.getFileList()[id].upload.progress.get();
					$(".globalLoader" + detailsId).addClass("p" + elapsed);
					$(".fileLoader" + detailsId).text(elapsed + "%");
				}
			}, 100);
		}
	},
});

Template.createEntity_messenger_manager_incidentDetails_modal.helpers({
	isOpen: () => {
		return Template.instance().isOpen.get() && IncidentDetails.findOne();
	},
	isAddingElement: () => {
		console.log(Template.instance().isAddingElem.get())
		return Template.instance().isAddingElem.get();
	},
	getCondoId: () => {
		return Template.instance().condoId;
	}
});
