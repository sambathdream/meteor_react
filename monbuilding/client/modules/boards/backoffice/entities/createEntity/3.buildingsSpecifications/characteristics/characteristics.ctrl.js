Template.createEntity_characteristics.onCreated(function() {
  this.selectedCondo = new ReactiveVar(Template.currentData().selectedCondo)
});

Template.createEntity_characteristics.onRendered(function() {

});

Template.createEntity_characteristics.events({
  'click #moduleOption': function (event, template) {
    const module = event.currentTarget.children[0].getAttribute('name');
    const value = $(`#moduleOption > [name=${module}]`).prop('checked');
    let option = {};
    option[module] = value;
    Meteor.call('saveCondoOptions', template.selectedCondo.get(), option);
  },
});

Template.createEntity_characteristics.helpers({
  updateCondoDatas: () => {
    Template.instance().selectedCondo.set(Template.currentData().selectedCondo)
  },
	options: (option) => {
		let condo = Condos.findOne({_id: Template.instance().selectedCondo.get()});
		if (condo && condo.settings.options[option]) {
			return condo.settings.options[option];
		}
	},
	getTypo: () => {
		const translateTab = {'etudiante': 'Résidence étudiante',
		'mono': 'Monopropriété',
		'copro': 'Copropriété',
		'office': 'Office'};
		let condo = Condos.findOne({_id: Template.instance().selectedCondo.get()});
		if (condo && condo.settings.condoType)
			return translateTab[condo.settings.condoType];
	},
	getCustomEmailSender: () => {
		let condo = Condos.findOne({ _id: Template.instance().selectedCondo.get() });
		if (condo && !!condo.settings.customEmail) {
			return condo.settings.customEmail
		}
		return false
  },
  condoSelected: () => {
    return Template.instance().selectedCondo.get()
  },
  isCondoSelected: () => {
    return !!Template.instance().selectedCondo.get()
  }
});
