import { FileManager } from '/client/components/fileManager/filemanager.js';

function removeBorder(detailId) {
	$("#inputFr" + detailId).css("border", "");
	$("#inputEn" + detailId).css("border", "");
}

Template.createEntity_messenger_manager_dropdownDetails_modal.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.isOpen = new ReactiveVar(false);
	this.isAddingElem = new ReactiveVar(false);
});

Template.createEntity_messenger_manager_dropdownDetails_modal.onRendered(function() {
});

Template.createEntity_messenger_manager_dropdownDetails_modal.events({
	'hidden.bs.modal #modal_messenger_manager_dropdownDetails': function(event, template) {
		template.isOpen.set(false);
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_messenger_manager_dropdownDetails': function(event, template) {
		template.isOpen.set(true);
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_messenger_manager_dropdownDetails";
		window.onhashchange = function() {
			if (location.hash != "#modal_messenger_manager_dropdownDetails"){
				modal.modal('hide');
			}
		}
	},
	'click .createNewDropdownDetail' (event, template) {
		template.isAddingElem.set(true);
		Meteor.call("createNewDropdownDetail", template.condoId, function(error, result) {
			if (!error)
				template.isAddingElem.set(false);
		});
	},
	'click .removeDropdownDetails' (event, template) {
		event.preventDefault();
		event.stopPropagation();
		let detailsId = $(event.currentTarget).attr("detailsId");
		$(".loader" + detailsId).css("display", "");
		Meteor.call("deleteDropdownDetail", template.condoId, detailsId, function(error, result) {
			$(".loader" + detailsId).css("display", "none");
			if (error) {
				if (error.error == 405) {
					sAlert.error("Vous devez avoir au moins un élément valide");
				}
				$(".fail" + detailsId).css("display", "");
			}
			else {
				$(".done" + detailsId).css("display", "");
			}
		});
	},
	'click #save' (event, template) {
		$("#save").button("loading");
		let details = $(".allDetails");
		$(".loader").css("display", "")
		$(".fail").css("display", "none");
		$(".done").css("display", "none");
		let condoId = template.condoId;
		details.each(function(index, elem) {
			let detailsId = $(elem).attr("detailsId");
			let typeId = template.data.typeId;
			removeBorder(detailsId);
			let value_fr = $("#inputFr" + detailsId).val();
			let value_en = $("#inputEn" + detailsId).val();
			Meteor.call("saveDropdownDetailForCondo", condoId, {typeId, detailsId, value_fr, value_en}, function(error, result) {
				$(".loader" + detailsId).css("display", "none");
				if (error) {
					if (error.error == 531) {
						let errors = error.reason.split("|");
						_.each(errors, function(elem) {
							$("#" + elem +  detailsId).css("border", "2px solid red");
						})
					}
					$(".fail" + detailsId).css("display", "");
				}
				else
					$(".done" + detailsId).css("display", "");
			})
		}).promise().done(function() { $("#save").button("reset"); })
	},
});

Template.createEntity_messenger_manager_dropdownDetails_modal.helpers({
	initSwitch: (typeId) => {
		setTimeout(function() {
			if (typeId == "sampleCheckbox")
				$("#" + typeId).bootstrapSwitch();
			else
				$("#isUsed" + typeId).bootstrapSwitch();
		}, 100);
	},
	isOpen: () => {
		return Template.instance().isOpen.get() && DeclarationDetails.findOne();
	},
	isAddingElement: () => {
		return Template.instance().isAddingElem.get();
	},
	getCondoId: () => {
		return Template.instance().condoId;
	},
	getDropdownDetail: () => {
		let listDropdown = ContactManagement.findOne({condoId: Template.instance().condoId});
		if (!listDropdown) {
			let condoType = Condos.findOne(Template.instance().condoId).settings.condoType
			listDropdown = ContactManagement.findOne({forCondoType: condoType});
		}
		if (listDropdown) {
			let listOfIds = _.find(listDropdown.messengerSet, function(list) {
				return list.id == Template.instance().data.typeId;
			})
			let temporaryDetails = DeclarationDetails.find({condoId: Template.instance().condoId, isNewEmpty: true}).fetch();
			if (listOfIds) {
				let ids = listOfIds.declarationDetailIds;
				_.each(temporaryDetails, function(elem) {
					ids.push(elem._id);
				})

				return listOfIds.declarationDetailIds;
			}
		}
	},
});
