Template.modal_new_subBuilding.onCreated(function () {
  this.subBuildingName = new ReactiveVar(null)
  this.adressInfos = new ReactiveVar({})

  this.isAdressError = new ReactiveVar(false)
  this.canSubmit = new ReactiveVar(false)
})

Template.modal_new_subBuilding.onDestroyed(function () {
})

Template.modal_new_subBuilding.onRendered(() => {
  let t = Template.instance()
})

Template.modal_new_subBuilding.events({
  'hidden.bs.modal #addSubBuilding': function (e, t) {
    t.subBuildingName.set(null)
    t.adressInfos.set({})
    t.isAdressError.set(false)
    t.canSubmit.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #addSubBuilding': function (e, t) {
    t.subBuildingName.set(null)
    t.adressInfos.set({})
    t.isAdressError.set(false)
    t.canSubmit.set(false)
    window.location.hash = "#addSubBuilding";
    window.onhashchange = function () {
      if (location.hash != "#addSubBuilding") {
        modal.modal('hide');
      }
    }
  },

})

function updateSubmit(template) {
  let canSubmit = false
  const subBuildingName = template.subBuildingName.get()
  const adressInfos = template.adressInfos.get()
  canSubmit = adressInfos && adressInfos.street_number && subBuildingName && subBuildingName.length
  template.canSubmit.set(canSubmit)
}

Template.modal_new_subBuilding.helpers({
  getFormSubBuilding: (key) => {
    const t = Template.instance()
    return () => {
      if (key === 'adress') {
          return t.adressInfos.get()
      } else if (key === 'buildingName') {
        return t.subBuildingName.get()
      }

      t.formBuilding.get(key) || ''
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()
    return () => (value) => {
      if (key === 'adress') {
        if (value && value.error) {
          sAlert.error(value.error)
          t.isAdressError.set(true)
        } else {
          t.isAdressError.set(false)
          t.adressInfos.set(value)
        }
      } else if (key === 'buildingName') {
        t.subBuildingName.set(value)
      }
      updateSubmit(t)
    }
  },
  getInputGoogleAdress: () => {
    return Template.instance().adressInfos;
  },
  isAdressError: () => {
    return Template.instance().isAdressError.get()
  },
  saveSubBuilding: () => {
    const t = Template.instance()
    return () => {
      if (t.canSubmit.get()) {
        if (typeof t.data.addSubBuildingCb === 'function') {
          const id = getRandomString()
          const subBuildingName = t.subBuildingName.get()
          const adressInfos = t.adressInfos.get()
          t.data.addSubBuildingCb({ id, subBuildingName, adressInfos })
          t.$('#addSubBuilding').modal('hide')

        }
      }
    }
  },
  getClasses: () => {
    let classes = 'btn btn-red-confirm'
    if (!Template.instance().canSubmit.get()) {
      classes += ' disabled'
    }
    return classes
  }
})
