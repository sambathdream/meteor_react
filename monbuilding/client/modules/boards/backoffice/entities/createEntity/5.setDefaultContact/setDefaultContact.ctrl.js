Template.admin_setNewEntityDefaultContact.onCreated(function () {
  this.canSubmit = new ReactiveVar(false)
})

Template.admin_setNewEntityDefaultContact.onDestroyed(function () {
})

Template.admin_setNewEntityDefaultContact.onRendered(() => {
  let t = Template.instance()
  let previousFileName = null

  $('.nextStep').on('click', function (e) {
    if ($(e.currentTarget).hasClass('5')) {
      e.stopImmediatePropagation()
      FlowRouter.go('app.backoffice.enterprise')
    }
  })
})

Template.admin_setNewEntityDefaultContact.events({
})


Template.admin_setNewEntityDefaultContact.helpers({
  updateSubmit: () => {
    const canSubmit = Template.instance().canSubmit.get()
    if (canSubmit) {
      Template.instance().data.setStepStateCb(canSubmit)
    }

  },
  getCondos: () => {
    const condoIds = Template.currentData().condoIds
    let condos = []
    if (condoIds && condoIds.length) {
      let hasAllSelected = true
      Condos.find({ _id: { $in: condoIds } }).forEach(condo => {
        const name = condo.getName()
        const condoId = condo._id
        const condoContact = CondoContact.findOne({ condoId: condoId })
        const userId = condoContact.defaultContact.userId
        const condoContactId = condoContact._id
        if (!userId) {
          hasAllSelected = false
        }
        condos.push({
          condoId,
          condoContactId,
          name,
          userId
        })
      })
      Template.instance().canSubmit.set(hasAllSelected)
    }
    return condos
  },
  getDefaultContactOptions: (condoId) => {
    let managers = []
    const enterprise = Enterprises.findOne({ condos: condoId })
    if (enterprise) {
      enterprise.users.forEach(user => {
        managers.push({
          text: user.firstname + ' ' + user.lastname,
          id: user.userId
        })
      })
    }
    return managers
  },
  onSelectDefaultContact: (condoContactId) => {
    const t = Template.instance()

    return () => (selected) => {
      if (!!selected) {
        Meteor.call('changeDefaultContact', condoContactId, selected, (error, result) => {
        })
      }
    }
  }
})
