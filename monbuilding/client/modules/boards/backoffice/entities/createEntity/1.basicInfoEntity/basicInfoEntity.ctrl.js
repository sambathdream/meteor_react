import mime from 'mime-types'

var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

const mimeType = (type) => {
  return type.split('/')[0]
}

const getImageByExtension = (ext) => {
  const basePath = '/img/icons/svg/'

  switch (ext) {
    case 'zip':
      return `${basePath}extensionzip.svg`
    case 'docx':
    case 'doc':
      return `${basePath}extensiondoc.svg`
    case 'pptx':
    case 'ppt':
      return `${basePath}extensionppt.svg`
    case 'xls':
    case 'xlsx':
      return `${basePath}extensionxls.svg`
    case 'pdf':
      return `${basePath}extensionpdf.svg`
    case 'txt':
      return `${basePath}extensiontxt.svg`
    default:
      return `${basePath}extensionunknown.svg`
  }
}

function saveFile(t, file, logoType) {
  file.id = `file-${getRandomString()}`
  if (file.type && mimeType(file.type) === 'image') {
    file.preview = {
      type: 'image',
      url: window.URL.createObjectURL(file)
    }
  } else {
    file.preview = {
      type: 'file'
    }
  }

  if (logoType === 'mobile') {
    t.logoMobile.set(file)
  } else {
    t.logoWeb.set(file)
  }


  let fileSrc = null
  const re = /gif|jpe?g|png|bmp/i
  if (re.test(mime.extension(file.type))) {
    fileSrc = file.preview.url
    var reader = new FileReader();
    reader.onload = function (e) {
      t.$('.attachmentDisplay-' + logoType)
      .attr('src', e.target.result)
      .css({'max-height': 'calc(100% - 32px)', 'height': ''})
    };
    reader.readAsDataURL(file);
    t.$('.attachmentName-' + logoType).text(file.name).parent().css('display', 'block')
  }
  else {
    sAlert.error('Selectionnez une image uniquement')
    // if (file.ext) {
    //   fileSrc = getImageByExtension(file.ext)
    // } else {
    //   fileSrc = getImageByExtension(mime.extension(file.type))
    // }
    // t.$('.attachmentDisplay-' + logoType).attr('src', fileSrc).css({'height': 'calc(100% - 32px)', 'max-height': ''})
  }
}

Template.admin_basicInfoEntity.onCreated(function () {
  this.formEntity = new ReactiveDict()
  this.formEntity.setDefault({
    entityName: null,
    siretNumber: null,
    legalGuardian: null,
    email: null,
    phoneCode: '33',
    phone: null,
    validCopro: true,
    validMono: true,
    validOffice: true,
    validStudent: true,
  })

  this.logoMobile = new ReactiveVar(null)
  this.logoWeb = new ReactiveVar(null)
  this.goodPhone = false
  this.adressInfos = new ReactiveVar({})
  this.isAdressError = new ReactiveVar(false)
})

Template.admin_basicInfoEntity.onDestroyed(function () {
})

Template.admin_basicInfoEntity.onRendered(() => {
  let t = Template.instance()
  let previousFileName = null
  t.$('.attachmentContainer').on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
    e.preventDefault()
    e.stopPropagation()
  })
  .on('dragover dragenter', function (e) {
    if (!$(e.currentTarget).children('.dragArea').hasClass('is-dragover')) {
      $(e.currentTarget).children('.dragArea').addClass('is-dragover')
    }
  })
  .on('dragleave dragend drop', function (e) {
    $(e.currentTarget).children('.dragArea').removeClass('is-dragover')
  })
  .on('drop', function (e) {
    e.stopImmediatePropagation()
    const type = $(e.currentTarget).children('.dragArea').data('type')
    saveFile(t, e.originalEvent.dataTransfer.files[0], type)
  })

  $('.nextStep').on('click', function(e) {
    if ($(e.currentTarget).hasClass('1')) {
      e.stopImmediatePropagation()
      let form = t.formEntity.all()
      form.logoWeb = t.logoWeb.get()
      form.logoMobile = t.logoMobile.get()
      form.adressInfos = t.adressInfos.get()
      t.data.goNextCb(form)
    }
  })
  $('.saveEntity').on('click', function(e) {
    e.stopImmediatePropagation()
    let form = t.formEntity.all()
    form.logoWeb = t.logoWeb.get()
    form.logoMobile = t.logoMobile.get()
    form.adressInfos = t.adressInfos.get()
    t.data.saveEntityCb(form)
  })
})

Template.admin_basicInfoEntity.events({
  'change .inputLogo': (e, t) => {
    const logoType = $(e.currentTarget).data('type')
    const file = e.currentTarget.files[0]
    saveFile(t, file, logoType)
    e.currentTarget.value = ''
  },
  'click .attachmentContainer': (e, t) => {
    t.$(e.currentTarget).next('.inputLogo').click()
  },
})

function updateSubmit(template) {
  let canSubmit = false
  const form = template.formEntity.all()
  const adressInfos = template.adressInfos.get()
  canSubmit =
    form.entityName !== null &&
    form.legalGuardian !== null &&
    adressInfos && adressInfos.street_number &&
    form.email !== null && (Isemail.validate(form.email) === true) &&
    (form.phone === null || form.phone === '' || template.goodPhone === true)
  template.data.setStepStateCb(canSubmit)
}



Template.admin_basicInfoEntity.helpers({
  getFormEntity: (key) => {
    const t = Template.instance()
    return () => t.formEntity.get(key) || ''
  },
  onCheckboxClick: (key) => {
    const t = Template.instance()
    return () => () => {
      const value = t.formEntity.get(key)
      t.formEntity.set(key, !value)
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()
    return () => (value) => {
      if (key === 'phone') {
        let number = '+' + t.formEntity.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.formEntity.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.formEntity.set(key, !value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.formEntity.set(key, !value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'adress') {
        if (value && value.error) {
          sAlert.error(value.error)
          t.isAdressError.set(true)
        } else {
          t.isAdressError.set(false)
          t.adressInfos.set(value)
        }
      } else {
        t.formEntity.set(key, value)
      }
      updateSubmit(t)
    }
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.formEntity.get('phone')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.formEntity.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          }
        } catch (e) {
          t.goodPhone = false
        }
      }
      t.formEntity.set(key, !selected ? null : selected)
      updateSubmit(t)
    }
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.formEntity.get(key)
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.formEntity.get(key)
        let number = '+' + t.formEntity.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  getInputGoogleAdress: () => {
    return Template.instance().adressInfos;
  },
  isAdressError: () => {
    return Template.instance().isAdressError.get()
  }
})
