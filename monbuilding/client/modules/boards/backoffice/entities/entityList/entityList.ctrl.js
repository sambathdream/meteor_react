import { Template } from "meteor/templating";

Template.admin_entityList.onCreated(function() {

})

Template.admin_entityList.onDestroyed(function() {

})

Template.admin_entityList.onRendered(function() {

})

Template.admin_entityList.events({
  'click .trashColumn': function (e, t) {
    e.preventDefault()
    e.stopPropagation()
    const entity = this
    if (entity && entity._id) {
      bootbox.confirm({
        size: "medium",
        title: "Confirmation",
        message: `Voulez-vous vraiment supprimer ${entity.name} ?`,
        buttons: {
          'cancel': { label: "Annuler", className: "btn-outline-red-confirm" },
          'confirm': { label: "Confirmer", className: "btn-red-confirm" }
        },
        backdrop: true,
        callback: function (result) {
          if (result) {
            if (entity._id)
              Meteor.call('removeEnterprise', entity._id, (error, result) => {
                if (!error) {
                  sAlert.success('Entité supprimé')
                } else {
                  sAlert.error(error)
                }
              })
          }
        }
      });
    }
  },
  'click .containerLine': function (e, t) {
    const entityId = this._id
    FlowRouter.go('app.backoffice.entityList.entityId.tab', { entityId, tab: 'details' })
  }

})

Template.admin_entityList.helpers({
  getEntitiesList: () => {
    return Enterprises.find()
  },
  addBuildings: function () {
    const entity = this
    return () => () => {
      FlowRouter.go('app.backoffice.buildingsList.newBuildings.entityId', { entityId: entity._id })
    }
  },
  inviteManager: function () {
    const entity = this
    return () => () => {
      if (entity.condos.length > 0) {
        FlowRouter.go('app.backoffice.managerList.personalDetails.entityId', { entityId: entity._id })
      } else {
        sAlert.error('Vous devez ajouter au moins un immeuble à cette entité')
      }
    }
  },
  inviteOccupant: function () {
    const entity = this
    return () => () => {
      if (entity.users.length > 0) {
        FlowRouter.go('app.backoffice.occupantList.personalDetails.entityId', { entityId: entity._id })
      } else {
        sAlert.error('Vous devez ajouter au moins un gestionnaire à cette entité')
      }
    }
  },
  getAddManagerClass: function () {
    const entity = this
    if (!entity.condos.length) {
      return 'disallowed'
    }
    return 'danger'
  },
  getAddOccupantClass: function () {
    const entity = this
    if (!entity.users.length) {
      return 'disallowed'
    }
    return 'danger'
  },
  getEntityAdress: function () {
    const entity = this
    return entity.info.address + ', ' + entity.info.code + ' ' + entity.info.city
  },

})
