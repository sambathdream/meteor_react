import { Template } from "meteor/templating";
import "./reportDetails.view.html";

Template.reportDetails.onCreated(function() {
	Meteor.subscribe('report');
	// Meteor.subscribe('backoffice');
});

Template.reportDetails.events({
	'click #goBack': function() {
		FlowRouter.go("app.backoffice.report");
	},
    'click .imgReport': function(event, template) {
        event.stopPropagation();
        event.preventDefault();
        let _event = event;
        $(_event.currentTarget).css("max-width", "100%");
        bootbox.alert({
            size: "large",
            title: "Report Image",
            message: _event.currentTarget.outerHTML,
            backdrop: true
        });
    }
});

Template.reportDetails.helpers({
	getReportId : () => {
		return FlowRouter.getParam('id');
	},
	getReport: () => {
		return Report.findOne(FlowRouter.getParam('id'));
	},
	getCondoName: (elemId) => {
		let condo = Condos.findOne(elemId);
		if (condo)
			return condo.getName();
	},
	getDeclarerName: (elemId) => {
		let user = Meteor.users.findOne(elemId);
		if (user)
			return user.profile.firstname + " " + user.profile.lastname;
	},
	getAuthorNames: (tabId) => {
		let retval = "";
		_.each(tabId, function(elemId) {
			let user = Meteor.users.findOne(elemId);
			if (user) {
				if (retval == "")
					retval = user.profile.firstname + " " + user.profile.lastname;
				else
					retval += "<br>" + user.profile.firstname + " " + user.profile.lastname;
			}
		});
		return retval;
	},
	imgLink : (imgId) => {
		return UserFiles.findOne(imgId).link();
    },

});
