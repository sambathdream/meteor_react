import { Template } from "meteor/templating";
import "./report.view.html";

Template.report.onCreated(function() {
	Meteor.subscribe('report');
	// Meteor.subscribe('backoffice');
});

Template.report.events({
	'click .reportLine': function(event, template) {
		let reportId = event.currentTarget.getAttribute("reportId");
		FlowRouter.go("app.backoffice.report.report_details", {id: reportId});
	}
});

Template.report.helpers({
	report: () => {
		return Report.find().fetch();
	},
	getCondoName: (elemId) => {
		let condo = Condos.findOne(elemId);
		if (condo)
			return condo.getName();
	},
	getDeclarerName: (elemId) => {
		let user = Meteor.users.findOne(elemId);
		if (user)
			return user.profile.firstname + " " + user.profile.lastname;
	},
	getAuthorNames: (tabId) => {
		let retval = "";
		_.each(tabId, function(elemId) {
			let user = Meteor.users.findOne(elemId);
			if (user) {
				if (retval == "")
					retval = user.profile.firstname + " " + user.profile.lastname;
				else
					retval += "<br>" + user.profile.firstname + " " + user.profile.lastname;
			}
		});
		return retval;
	}
});
