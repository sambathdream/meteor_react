import { Template } from "meteor/templating";
import "./integrationConfig.html";
import { WSAEINVALIDPROCTABLE } from "constants";

Template.integrationConfig.onCreated(function() {
  this.condoId = FlowRouter.getParam("condoId");
  this.integrationId = FlowRouter.getParam("integrationId");
  this.eventSearch = new ReactiveVar("");

  this.subscribe('integrations');
  this.subscribe('integrationConfig', this.condoId, this.integrationId)
});

Template.integrationConfig.events({
  'click #backbutton' (e, t) {
    const lang = FlowRouter.getParam("lang") || "fr";
    let current = FlowRouter.current();
    if (current.queryParams.from && current.queryParams.from === 'condo') {
      FlowRouter.go("app.backoffice.integrationCondo", { lang, condoId: Template.instance().condoId });
    } else {
      FlowRouter.go("app.backoffice.integrationDetail", { lang, integrationId: Template.instance().integrationId });
    }
  },
  'click .add-contact-icon' (e, t) {
    const template = Template.instance()
    if (template.contacts) {
      const contacts = template.contacts.get()
      contacts.push({
        name: '',
        role: '',
        email: '',
        phone: ''
      })
      template.set('contacts', contacts);
    }
  },
  'click #save' (e, t) {
    const template = Template.instance()
    const comments = template.comments
    const contacts = template.contacts && _.filter(template.contacts.get(), contact => (
      contact.name !== '' &&
      contact.role !== '' &&
      contact.email !== '' &&
      contact.phone !== ''
    ))
    $(e.currentTarget).button('loading')
    Meteor.call('integration-setup', template.condoId, template.integrationId, {comments, contacts}, (error, result) => {
      $(e.currentTarget).button('reset')
      sAlert.success('Sauvegarde effectuée')
    })
  },
  'change textarea' (e, t) {
    const template = Template.instance()
    template.comments = e.target.value;
  }
});

Template.integrationConfig.helpers({
  getIntegration: function () {
    const template = Template.instance()
    const integration = Integrations.findOne({_id: template.integrationId});
    return integration || {
      provider: ''
    }
  },
  getCondo: function () {
    const template = Template.instance()
    const condo = Condos.findOne({_id: template.condoId})
    return condo
  },
  onInputDetails: function (field, index) {
    const template = Template.instance()
    return () => function (value) {
      if (template.contacts) {
        const contacts = template.contacts.get()
        if (contacts[index]) {
          const val = value.trim();
          contacts[index][field] = val;
          template.set('contacts', contacts);
        }
      }
    }
  },
  getCondoConfig: function () {
    const config = CondoIntegrationConfig.findOne({
      condoId: Template.instance().condoId,
      integrationId: Template.instance().integrationId
    });
    return config
  },
  formatDate: function (config) {
    if (config && config.activatedOn) {
      return moment(config.activatedOn).format("DD/MM/YYYY")
    }
    return '- - -'
  },
  getTextAreaContent: function(config) {
    if (!config) {
      return ''
    } else {
      return config.comments
    }
  },
  getContacts: function (config) {
    if (!config) {
      return []
    }
    const template = Template.instance()
    if (!template.contacts) {
      let contacts = []
      if (config.contacts && config.contacts.length > 0) {
        contacts = config.contacts
      }
      if (contacts.length === 0) {
        contacts.push({
          name: '',
          role: '',
          email: '',
          phone: ''
        })
      }
      template.contacts = new ReactiveVar(contacts)
    }
    return template.contacts.get()
  },
  hasConfiguration: (integration) => {
    return integration.requiredFields && integration.requiredFields.length > 0
  },
  integrationConfig: () => {
    const t = Template.instance()
    return IntegrationConfig.findOne({ condoId: t.condoId, integrationId: t.integrationId })
  },
  getConfigValue: function (field) {
    return this[field] || '-'
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  getIntegrationId: () => Template.instance().integrationId,
  getCondoId: () => Template.instance().condoId
});
