import { CommonTranslation } from "/common/lang/lang.js"

Template.editIntegrationConfig.onCreated(function () {
  this.integrationId = Template.currentData().integrationId
  this.integrationConfigId = null
  this.condoId = Template.currentData().condoId
  this.datas = {}

  this.isOpen = new ReactiveVar(false)
})

Template.editIntegrationConfig.onDestroyed(() => {
})

Template.editIntegrationConfig.onRendered(() => {
})

Template.editIntegrationConfig.events({
  'hidden.bs.modal #modal_edit_integration_config': (event, template) => {
    template.isOpen.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_edit_integration_config': (event, template) => {
    template.isOpen.set(false)

    const integration = Integrations.findOne({ _id: template.integrationId })
    const integrationConfig = IntegrationConfig.findOne({ integrationId: template.integrationId, condoId: template.condoId })

    let datas = []
    if (integration && integration.requiredFields) {
      integration.requiredFields.forEach(field => {
        datas.push({
          key: field,
          value: integrationConfig[field] || null
        })
      })
    }
    template.datas = datas
    template.integrationConfigId = integrationConfig._id
    let modal = $(event.currentTarget);
    template.isOpen.set(true)
    window.location.hash = "#modal_edit_integration_config";

    window.onhashchange = function () {
      if (location.hash != "#modal_edit_integration_config") {
        modal.modal('hide');
      }
    }
  },
  'click .saveIntegrationConfig': (e, t) => {
    $(e.currentTarget).button('loading')
    const datas = t.datas
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const configId = t.integrationConfigId
    Meteor.call('updateIntegration-config', configId, datas, (error, result) => {
      $(e.currentTarget).button('reset')
      if (error) {
        sAlert.error(translate.commonTranslation.error_occured)
      } else {
        sAlert.success(translate.commonTranslation.modif_success)
        $('#modal_edit_integration_config').modal('hide')
      }
    })
  }
})

Template.editIntegrationConfig.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  fields: () => {
    const datas = Template.instance().datas
    return datas
  },
  onInputDetails: function (index) {
    const t = Template.instance()
    return () => (value) => {
      t.datas[index]['value'] = value
    }
  },
})
