import { Template } from "meteor/templating";
import "./integrationDetail.html";
import { isMoment } from "moment";

Template.integrationDetail.onCreated(function() {
  this.subscribe('integrations');
  this.id = FlowRouter.getParam("integrationId");
	this.eventSearch = new ReactiveVar("");
	this.selectedTab = new ReactiveVar("integration");
});

Template.integrationDetail.events({
  'click #backbutton' (e, t) {
		let current = FlowRouter.current();
		FlowRouter.go("app.backoffice.integrations", current.params);
  },
  'click .condoLine' (e, t) {
    const condoId = e.currentTarget.getAttribute("condoId");
    FlowRouter.go("app.backoffice.integrationConfig", {integrationId: Template.instance().id, condoId});
  },
  // app.backoffice.integrationCondo
  'click label' (e, t) {
    e.stopPropagation();
  },
  'click [type=checkbox]' (e, t) {
    e.stopPropagation();
  },
  'change [type=checkbox]' (e, t) {
    const condo = e.currentTarget.getAttribute('condoId');
    const enabled = e.currentTarget.checked;
    Meteor.call('integration-setup', condo, Template.instance().id, {active: enabled});
  }
});

Template.integrationDetail.helpers({
  onSearch: () => {
      const template = Template.instance();
      return (input) => {
    template.set('eventSearch', input);
      }
  },

  getIntegration: function () {
    const template = Template.instance()
    const integration = Integrations.findOne({_id: template.id});
    return integration || {
      provider: ''
    }
  },

  getCondos: function () {
    let regexp = new RegExp(Template.instance().eventSearch.get(), "i");
    const condos = (Condos.find({$or: [{"name": regexp}]}).fetch());
		return condos;
  },
  getCondoConfig: function (condoId) {
    const config = CondoIntegrationConfig.findOne({
      condoId,
      integrationId: Template.instance().id
    });
    return config
  },

  formatDate: function (config) {
    if (config && config.activatedOn) {
      return moment(config.activatedOn).format("DD/MM/YYYY")
    }
    return '- - -'
  }
});
