import { Template } from "meteor/templating";
import "./integrationCondo.html";

Template.integrationCondo.onCreated(function() {
  this.subscribe('integrations');
  this.id = FlowRouter.getParam("condoId");
	this.eventSearch = new ReactiveVar("");
});

Template.integrationCondo.events({
  'click #backbutton' (e, t) {
		let current = FlowRouter.current();
		FlowRouter.go("app.backoffice.integrations.tab", {tab: 'condo'});
  },
  'click .integrationLine' (e, t) {
    const integrationId = e.currentTarget.getAttribute("integrationId");
    FlowRouter.go("app.backoffice.integrationConfig", {integrationId, condoId: Template.instance().id}, {from: 'condo'});
  },
});

Template.integrationCondo.helpers({
  integrationBySection: function() {
    let integration = Integrations.find().fetch();
    const filter = Template.instance().eventSearch.get();
		if (filter !== '') {
			integration = _.filter(integration, i => (
				i.provider.indexOf(filter) >= 0 ||
				i.service.indexOf(filter) >= 0 ||
				i.company.indexOf(filter) >= 0
			))
		}
    return _
      .chain(integration)
        .groupBy('section')
        .map(group => ({
          section: group[0].section,
          apps: group
        }))
        .sortBy('section')
      .value()
  },
  getIntegrationConfig: function (integrationId) {
    const config = CondoIntegrationConfig.findOne({
      condoId: Template.instance().id,
      integrationId
    });
    return config || {}
  },
  formatDate: function (config) {
    if (config && config.activatedOn) {
      return moment(config.activatedOn).format("DD/MM/YYYY")
    }
    return '- - -'
  },
	onSearch: () => {
    const template = Template.instance();
    return (input) => {
      template.set('eventSearch', input);
    }
  }
});
