import { Template } from "meteor/templating";
import "./integrations.view.html";

Template.integrations.onCreated(function() {
	this.subscribe('integrations');
	this.eventSearch = new ReactiveVar("");
	this.selectedTab = new ReactiveVar(FlowRouter.getParam("tab") || "integration");
});

Template.integrations.events({
	'click .list-tab': function(event, template) {
		template.selectedTab.set(event.currentTarget.getAttribute('tab'));
		FlowRouter.go("app.backoffice.integrations.tab", {tab: template.selectedTab.get()});
  },
  'click .integrationLine': function(event, template) {
    const lang = FlowRouter.getParam("lang") || "fr";
    const integrationId = event.currentTarget.getAttribute("integrationid");
    if (integrationId) {
      FlowRouter.go("app.backoffice.integrationDetail", { lang, integrationId });
    } else {
      const condoId = event.currentTarget.getAttribute("condoid");
      FlowRouter.go("app.backoffice.integrationCondo", { lang, condoId });
    }
  }

});

Template.integrations.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
	getCondos: () => {
    let regexp = new RegExp(Template.instance().eventSearch.get(), "i");
    const condos = (Condos.find({$or: [{"name": regexp}]}).fetch());
		return condos;
	},
	getTypeCondo: (condoType) => {
		const typeTab = {
		  etudiante: 'Résidence étudiante',
		  mono: 'Monopropriété',
		  copro: 'Copropriété',
		  office: 'Office',
		};
		return typeTab[condoType];
	},
	selectedTab: (name) => {
		const currenttab = Template.instance().selectedTab.get();
		return  name === currenttab;
	},
	onSearch: () => {
      const template = Template.instance();
      return (input) => {
        template.set('eventSearch', input);
      }
  },
	getIntegrations: () => {
		const template = Template.instance();
		let integrations = Integrations.find().fetch();
		let other = CondoIntegrationConfig.find().fetch();
		const filter = template.eventSearch.get();
		if (filter !== '') {
			integrations = _.filter(integrations, i => (
				i.provider.indexOf(filter) >= 0 ||
				i.service.indexOf(filter) >= 0 ||
				i.company.indexOf(filter) >= 0
			))
    }
		return integrations;
  },
  getActiveBuildings: (integrationId) => {
    return CondoIntegrationConfig.find({ integrationId, active: true }, {fields: { _id: true } }).count()
  }
});
