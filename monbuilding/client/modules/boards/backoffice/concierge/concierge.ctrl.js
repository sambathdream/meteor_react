Template.concierge.onCreated(function() {
	this.listSelectedTab = new ReactiveVar("active");
});

Template.concierge.events({
	'click #attachments': function(event, template) {
		event.stopPropagation();
		event.preventDefault();
		let _event = event;
		$(_event.currentTarget).css("max-width", "100%");
		bootbox.alert({
			size: "large",
			title: "Justificatifs",
			message: _event.currentTarget.outerHTML,
			backdrop: true
		});
	},
	'mouseenter [name="infoBulle"]': function(event, template) {
		let bulleName = "bulle" + $(event.currentTarget).attr('idx');
		$("[name='" + bulleName + "']").css("display", 'block');
	},
	'mouseleave [name="infoBulle"]': function(event, template) {
		let bulleName = "bulle" + $(event.currentTarget).attr('idx');
		$("[name='" + bulleName + "']").css("display", 'none');
	},
	'click .copyToClipboard': function(event, template) {
		var $temp = $("<input>");
		$("body").append($temp);
		$temp.val($(event.currentTarget).attr('copyVal')).select();
		document.execCommand("copy");
		$temp.remove();
		$("#noty_topRight_layout_container").remove();
		$(".allListBack").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">Copié</span></div></div></li></ul>');
		$("#noty-li").animate({"height": "34.4502px"}, "fast");
		setTimeout(function() {
			$("#noty-li").animate({"height": "0px"}, "fast", function() {
				$("#noty_topRight_layout_container").remove();
			});
		}, 2000);
	},
	'click .remove': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		const conciergeId = $(event.currentTarget).attr('conciergeId');
		if (template.listSelectedTab.get() == "active")
			bootbox.dialog({
				size: "medium",
				title: "Confirmation",
				message: "Voulez-vous supprimer ou archiver cette demande de conciergerie ?",
				buttons: {
					cancel: {
						label: "Annuler",
						className: 'btn-outline-red-confirm'
					},
					archive: {
						label: "Archiver",
						className: 'btn-outline-orange-archive',
						callback: function(){
							Meteor.call('removeConcierge', conciergeId, "archive", function(error) {
								if (!error)
									return true;
								else
									return false;
							})
						}
					},
					confirm: {
						label: "Confirmer",
						className: 'btn-red-confirm',
						callback: function(){
							Meteor.call('removeConcierge', conciergeId, "delete", function(error) {
								if (!error)
									return true;
								else
									return false;
							})
						}
					}
				},
				backdrop: true,
			});
		else
			bootbox.dialog({
				title: "Confirmation",
				message: "Voulez-vous vraiment supprimer cette demande de conciergerie ?",
				buttons: {
					cancel: {
						label: "Annuler",
						className: 'btn-outline-red-confirm'
					},
					confirm: {
						label: "Confirmer",
						className: 'btn-red-confirm',
						callback: function(){
							Meteor.call('removeConcierge', conciergeId, "delete", function(error) {
								if (!error)
									return true;
								else
									return false;
							})
						}
					}
				},
				backdrop: true,
			});		

	},
	'click button[name="listTabSwitch"]': function (event, template) {
		template.listSelectedTab.set($(event.currentTarget).attr('id'));
	},
});

Template.concierge.helpers({
	concierges: () => {
		if (Template.instance().listSelectedTab.get() == "active")
			return ConciergeMessage.find({$or: [{isArchive: {$exists: false}}, {isArchive: false}]}, {sort: {date: -1}}).fetch();
		else if (Template.instance().listSelectedTab.get() == "archive")
			return ConciergeMessage.find({$or: [{isArchive: {$exists: true}}, {isArchive: true}]}, {sort: {date: -1}}).fetch();
	},
	getUserName: (userId) => {
		if (!userId)
			return '-';
		let user = Meteor.users.findOne(userId);
		if (!user || !user.profile)
			return '-';
		return user.profile.firstname + " " + user.profile.lastname;
	},
	getEmail: (userId) => {
		if (!userId)
			return '-';
		let user = Meteor.users.findOne(userId);
		if (!user || !user.emails)
			return '<a>-</a>';
		return "<a style='text-decoration: underline;' target='_blank' href='mailto:" + user.emails[0].address + "'>" + user.emails[0].address + "</a>";
	},
	getPhone: (userId) => {
		if (!userId)
			return '-';
		let user = Meteor.users.findOne(userId);
		if (!user || !user.profile || !user.profile.phone)
			return '<a>-</a>';
		return "<a>" + user.profile.phone + "</a>";
	},
	getEmailVal: (userId) => {
		if (!userId)
			return '-';
		let user = Meteor.users.findOne(userId);
		if (!user || !user.emails)
			return '-';
		return user.emails[0].address;
	},
	getPhoneVal: (userId) => {
		if (!userId)
			return '-';
		let user = Meteor.users.findOne(userId);
		if (!user || !user.profile || !user.profile.phone)
			return '-';
		return user.profile.phone;
	},
	getContent: (content) => {
		str = content[0].text.split("\n").join("<br>");
		return str;
	},
	getFilesId: (content) => {
		return content[0].files;
	},
	displayFile: (fileId) => {
		if (fileId) {
			let file = UserFiles.findOne({_id: fileId});
			if (file) {
				return file.link().replace("localhost", location.hostname);
			}
			else
				return "-";
		}
		return "-";
	},
	fileType: (fileId) => {
		if (fileId) {
			let file = UserFiles.findOne({_id: fileId});
			if (file) {
				return file.isImage;
			}
		}
	},
	getListActiveTab: () => {
		return Template.instance().listSelectedTab.get();
	}
});