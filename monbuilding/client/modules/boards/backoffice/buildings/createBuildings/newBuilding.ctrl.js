Template.admin_newBuildings.onCreated(function () {
  this.formBuilding = new ReactiveDict()
  this.formBuilding.setDefault({
    buildingName: '',
    buildingId: null,
    type: null,
    subBuildings: []
  })
  this.savedBuildings = new ReactiveArray(null)
  this.adressInfos = new ReactiveVar({})
  this.isAdressError = new ReactiveVar(false)
  this.canSave = new ReactiveVar(false)
  this.canSubmit = new ReactiveVar(false)
})

Template.admin_newBuildings.onDestroyed(function () {
})

Template.admin_newBuildings.onRendered(() => {
})

Template.admin_newBuildings.events({
  'click .saveNewBuildings': (e, t) => {
    if (t.canSubmit.get() === true) {
      e.stopImmediatePropagation()
      let form = t.savedBuildings.array()
      const entityId = FlowRouter.getParam('entityId') || null
      if (entityId) {
        Meteor.call('addBuildingsToEntity', entityId, form, (error, result) => {
          // console.log('result', result)
          FlowRouter.go('app.backoffice.buildingsList.tab', { tab: 'list' })
        })
      } else {
        Meteor.call('addBuildings', form, (error, result) => {
          // console.log('result', result)
          FlowRouter.go('app.backoffice.buildingsList.tab', { tab: 'list' })
        })
      }
    }
  },
  'click #backbutton': (e, t) => {
    if (FlowRouter.getRouteName() === 'app.backoffice.buildingsList.newBuildings.entityId') {
      FlowRouter.go('app.backoffice.entityList')
    } else {
      FlowRouter.go('app.backoffice.buildingsList.tab', { tab: 'list' })
    }
  },
  'click .mb-input-addIcon': (e, t) => {
    t.$('#addSubBuilding').modal('show')
  },
  'click .trash': (e, t) => {
    const buildingIndex = $(e.currentTarget).attr('buildingindex')
    t.savedBuildings.splice(buildingIndex, 1)
  },
  'click .edit': (e, t) => {
    const buildingIndex = $(e.currentTarget).attr('buildingindex')
    const savedValues = t.savedBuildings.array()[buildingIndex]
    t.formBuilding.set(savedValues.formBuilding)
    t.adressInfos.set(savedValues.adressInfos)
    t.isAdressError.set(false)
    t.canSave.set(true)
    t.savedBuildings.splice(buildingIndex, 1)
  }
})

function updateSubmit(t) {
  let canSubmit = false
  const form = t.formBuilding.all()
  const adressInfos = t.adressInfos.get()
  canSubmit =
    form.type !== null &&
    adressInfos && adressInfos.street_number
  t.canSave.set(canSubmit)
  // t.data.setStepStateCb(canSubmit)
}



Template.admin_newBuildings.helpers({
  getEntityName: () => {
    const entityId = FlowRouter.getParam('entityId')
    if (entityId) {
      const entity = Enterprises.findOne({ _id: entityId })
      if (entity) {
        return '(' + entity.name + ')'
      }
    }
  },
  updateNextStep: (count) => {
    Template.instance().canSubmit.set(!!count)
  },
  getFormBuilding: (key) => {
    const t = Template.instance()
    return () => t.formBuilding.get(key) || ''
  },
  getFormBuildingAdress: () => {
    const t = Template.instance()
    return () => t.adressInfos.get()
  },
  getFormBuildingType: () => {
    const t = Template.instance()
    return t.formBuilding.get('type') || '-1'
  },
  getFormBuildingTypeSelected: () => {
    const type = Template.instance().formBuilding.get('type')
    // console.log('type', type)
    if (type) {
      return type
    } else {
      return null
    }
  },
  getFormSubBuildingSelected: () => {
    const subBuildings = Template.instance().formBuilding.get('subBuildings')
    let subBuildingsSelected = []
    subBuildings.forEach((subBuilding) => {
      if (subBuilding && subBuilding.selected) {
        subBuildingsSelected.push(subBuilding.id)
      }
    })
    return subBuildingsSelected
  },
  getFormSubBuildingOptions: () => {
    const subBuildings = Template.instance().formBuilding.get('subBuildings')
    let subBuildingsOptions = []
    subBuildings.forEach((subBuilding) => {
      if (subBuilding) {
        subBuildingsOptions.push({ id: subBuilding.id, text: subBuilding.name })
      }
    })
    return subBuildingsOptions
  },
  onSelectSubBuilding: () => {
    const t = Template.instance()

    return (val) => {
      const subBuildings = t.formBuilding.get('subBuildings')
      let newSubBuildings = subBuildings
      let hasChanged = false
      subBuildings.forEach((subBuilding, index) => {
        if (_.contains(val, subBuilding.id) && subBuilding.selected === false) {
          hasChanged = true
          newSubBuildings[index].selected = true
        } else if (!_.contains(val, subBuilding.id) && subBuilding.selected === true) {
          hasChanged = true
          newSubBuildings[index].selected = false
        }
      })
      if (hasChanged === true) {
        t.formBuilding.set('subBuildings', newSubBuildings)
      }
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()
    return () => (value) => {
      if (key === 'adress') {
        if (value && value.error) {
          sAlert.error(value.error)
          t.isAdressError.set(true)
        } else {
          t.isAdressError.set(false)
          const regexpAddress = new RegExp(value.street_number + " " + value.road, "i");
          const regexpCode = new RegExp(value.code, "i");
          const regexpcity = new RegExp(value.city, "i");

          const existAdress = Condos.findOne({
            'info.address': regexpAddress,
            'info.city': regexpcity,
            'info.code': regexpCode,
          }, {fields: { _id: 1 } })
          if (!existAdress) {
            t.isAdressError.set(false)
            t.adressInfos.set(value)
          } else {
            t.isAdressError.set(true)
            sAlert.error('Cette adresse est deja utilisé')
          }
        }
      } else {
        t.formBuilding.set(key, value)
      }
      updateSubmit(t)
    }
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      // console.log('selected', selected)
      t.formBuilding.set(key, !selected ? null : selected)
      updateSubmit(t)
    }
  },
  getBuildingTypeOptions: () => {
    return [
      { id: 'copro', text: 'Copropriété' },
      { id: 'mono', text: 'Monopropriété' },
      { id: 'etudiante', text: 'Résidence étudiante' },
      { id: 'office', text: 'Bureau' },
    ]
  },
  isAdressError: () => {
    return Template.instance().isAdressError.get()
  },
  getSavedBuildings: () => {
    return Template.instance().savedBuildings.list()
  },
  getClasses: () => {
    let classes = 'danger'
    if (!Template.instance().canSave.get()) {
      classes += ' disabled'
    }
    return classes
  },
  addSubBuildingCb: () => {
    const t = Template.instance()

    return (form) => {
      const actualSubBuilding = t.formBuilding.get('subBuildings')
      actualSubBuilding.push({
        id: form.id,
        name: form.subBuildingName,
        adressInfos: form.adressInfos,
        selected: true
      })
      t.formBuilding.set('subBuildings', actualSubBuilding)
    }
  },
  saveAndAddBuilding: () => {
    const t = Template.instance()
    return () => {
      if (t.canSave.get()) {
        const formBuilding = t.formBuilding.all()
        const adressInfos = t.adressInfos.get()

        if (formBuilding.subBuildings.length === 0) {
          formBuilding.subBuildings = []
        }
        t.savedBuildings.push({
          formBuilding,
          adressInfos
        })

        t.formBuilding.clear()
        t.formBuilding.setDefault({
          buildingName: '',
          buildingId: null,
          type: null,
          subBuildings: []
        })
        t.adressInfos.set({})

      }
    }
  },
  getDisplayedValue: (value) => {
    return (value && value.length) ? value : '-'
  },
  getEntityOptions: () => {
    return Enterprises.find().map(entity => {
      return {
        id: entity._id,
        text: entity.name
      }
    })
  },
  getTypeValue: (key) => {
    const translateTab = {
      'etudiante': 'Résidence étudiante',
      'mono': 'Monopropriété',
      'copro': 'Copropriété',
      'office': 'Bureau'
    };
    return translateTab[key]
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  }
})
