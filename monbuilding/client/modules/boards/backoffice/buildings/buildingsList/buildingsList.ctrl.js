import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { handleActiveDirectoryModal } from '/client/sharedFunctions/active-directory'

Template.admin_buildingsList.onCreated(function () {
  this.searchText = new ReactiveVar('')
})

Template.admin_buildingsList.onDestroyed(function () {

})

Template.admin_buildingsList.onRendered(() => {

})

Template.admin_buildingsList.events({
  'click .containerLine': (e, t) => {
    const condoId = $(e.currentTarget).attr('condoId')
    FlowRouter.go('app.backoffice.condos.condo_details', { id: condoId })
  },
  'click .active-directory': function (e) {
    e.stopPropagation()
    const condoId = this.building._id
    handleActiveDirectoryModal(condoId)
  },
  'click .trashColumn': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    bootbox.confirm({
      size: 'medium',
      title: 'Confirmation',
      message: 'Voulez-vous vraiment le supprimer ?',
      buttons: {
        'cancel': { label: 'Annuler', className: 'btn-outline-red-confirm' },
        'confirm': { label: 'Confirmer', className: 'btn-red-confirm' }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          const condoId = $(e.currentTarget).attr('condoId')
          if (condoId) {
            Meteor.call('removeCondo', condoId)
          }
        }
      }
    })
  }
})

Template.admin_buildingsList.helpers({
  inviteManager: (condoId) => {
    return () => () => {
      FlowRouter.go('app.backoffice.managerList.personalDetails.condoId', { condoId })
    }
  },
  inviteOccupant: (condoId) => {
    return () => () => {
      FlowRouter.go('app.backoffice.occupantList.personalDetails.condoId', { condoId })
    }
  },
  searchCallback: () => {
    const t = Template.instance()

    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      t.searchText.set(value)
    }
  },
  getBuildingsList: () => {
    const searchText = Template.instance().searchText.get()
    const regexp = new RegExp(searchText.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    let condoTypeRegexp = []

    if (('Monopropriété').match(regexp)) condoTypeRegexp.push('mono')
    if (('Copropropriété').match(regexp)) condoTypeRegexp.push('copro')
    if (('Résidence étudiante').match(regexp)) condoTypeRegexp.push('etudiante')
    if (('Bureau').match(regexp)) condoTypeRegexp.push('office')

    return Condos.find({
      $or: [
        { name: regexp },
        { 'info.address': regexp },
        { 'info.city': regexp },
        { 'info.code': regexp },
        { 'settings.condoType': { $in: condoTypeRegexp } }
      ]
    }, {sort: { createdAt: -1 }})
  },
  getEntityName: (condoId) => {
    const entity = Enterprises.findOne({ condos: condoId })

    if (entity) {
      return entity.name
    } else {
      return '-'
    }
  },
  getCondoAdress: (condo) => {
    return condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city
  },
  getTypoOfCondo: (condoType) => {
    if (condoType === 'mono') {
      return 'Monopropriété'
    } else if (condoType === 'copro') {
      return 'Copropropriété'
    } else if (condoType === 'etudiante') {
      return 'Résidence étudiante'
    } else if (condoType === 'office') {
      return 'Bureau'
    } else {
      return '-'
    }
  }

})
