Template.admin_buildingsController.onCreated(function () {
  let tab = FlowRouter.getParam('tab')
  this.handler = Meteor.subscribe('admin_buildings')
  if (!tab || (tab !== 'list' && tab !== 'default')) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: 'list' })
  }

})

Template.admin_buildingsController.onDestroyed(function () {

})

Template.admin_buildingsController.onRendered(() => {

})

Template.admin_buildingsController.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ tab: tabName })
  },
  'click .addBuildings': (e, t) => {
    FlowRouter.go('app.backoffice.buildingsList.newBuildings')
  }
})

Template.admin_buildingsController.helpers({
  isSubscribeReady: () => {
    return Template.instance().handler.ready()
  },
  getTemplateName: () => {
    let tab = FlowRouter.getParam('tab')
    if (tab === 'list') {
      return 'admin_buildingsList'
    } else {
      return 'admin_buildingDefault'
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('tab')
    if (!tab || (tab !== 'list' && tab !== 'default')) {
      FlowRouter.setParams({ tab: 'list' })
    }
    return tab
  },

})
