import { Template } from "meteor/templating"
import "./condoDetails.view.html";

const regexpSearch = {
	'a': '[a|à|â|ä]',
	'e': '[e|é|è|ê|ë]',
	'i': '[i|î|ï|ì]',
	'o': '[o|ô|ö|ò]',
	'u': '[u|ù|ü|û]',
	'y': '[y|ý|ŷ|ÿ|ỳ]'
};

Template.condoDetails.onCreated(function() {
	if (!FlowRouter.getParam("tab"))
	FlowRouter.go('app.backoffice.condos.condo_details_tabs', {id: FlowRouter.getParam("id"), tab: 'stat'});
	this.condoId = new ReactiveVar(FlowRouter.current().params.id);
	// this.managerSearch = new ReactiveVar('');
	this.typo = new ReactiveVar('');
	this.isError = new ReactiveVar(false);
	this.residentSearch = new ReactiveVar('');
	Meteor.subscribe('customBuildings');
  Meteor.subscribe('admin_condosCharacteristics');

	this.selectedTab = new ReactiveVar((FlowRouter.getParam("tab") ? FlowRouter.getParam("tab") :'stat'));

});

Template.condoDetails.onRendered(function() {
});

Template.condoDetails.events({

	'click #goBoardGestionnaire': function(event, template) {
		$(event.currentTarget).button("loading");
		let id = Template.instance().condoId.get();
		if (!Enterprises.findOne({"condos": id}))
			bootbox.alert({
				size: "medium",
				title: "Erreur",
				message: "Cette immeuble n'est pas rattaché à un gestionnaire",
				backdrop: true,
				callback: function() { $(event.currentTarget).button("reset"); }
			});
		else {
			let enterpriseId = Enterprises.findOne({"condos": id})._id;
			let condoId = [id];

			event.preventDefault();
			Meteor.call('updateBoardManagerForAdmin', enterpriseId, condoId, function(error) {
				if (!error)
					window.location.href = '/fr/gestionnaire/';
			})
		}
	},
	'click #goBoardOccupant': function(event, template) {
		event.preventDefault();
		$(event.currentTarget).button("loading");
		let id = Template.instance().condoId.get();
		let condoId = [id];
		let condo = Condos.findOne({_id: {$in: condoId}});
		let keeper, cs = [];
		Meteor.call('getKeeperOfCondo', condo._id, function(error, result) {
			keeper = result;
			Meteor.call('getCSOfCondo', condo._id, function(error, result) {
				cs = result;
				var dialog = bootbox.dialog({
					title: 'Selectionnez le type de compte',
					message: "<p>Selectionnez le type de compte avec le quel vous voulez avoir accès au board occupant</p>",
					onEscape: function() { $(event.currentTarget).button("reset"); },
					buttons: {
						cancel: {
							label: "Normal",
							className: 'btn-danger',
							callback: function(){
								Meteor.call('updateBoardOccupantForAdmin', condoId, "normal", function(error) {
									console.log(error);
									if (!error) {}
										// window.location.href = '/fr/resident/';
								})
								return false;
							}
						},
						noclose: {
							label: "Gardien",
							className: keeper.length ?  'btn-warning' : 'btn-warning display-none',
							callback: function(){
								Meteor.call('updateBoardOccupantForAdmin', condoId, "keeper", function(error) {
									console.log(error);
									if (!error) {}
										// window.location.href = '/fr/resident/';
								})
								return false;
							}
						},
						ok: {
							label: "Conseil syndical",
							className: cs.length ?  'btn-info' : 'btn-info display-none',
							callback: function(){
								Meteor.call('updateBoardOccupantForAdmin', condoId, "cs", function(error) {
									console.log(error);
									if (!error) {}
										// window.location.href = '/fr/resident/';
								})
								return false;
							}
						}
					},
				});
			});
		});
	},
	'click #goBack': function(event, template) {
    FlowRouter.go('app.backoffice.buildingsList.tab', { tab: 'list' })
  },
	'click #seeOnly': function(event, template) {
		const index = event.currentTarget.children[0].getAttribute('name');
		$($(event.currentTarget).children()[0])[0].checked = !($($(event.currentTarget).children()[0])[0].checked);
		$(`#seeWrite input[name='${index}']`)[0].checked = false;
		$(`#seeDelete input[name='${index}']`)[0].checked = false;
		Meteor.call('saveAccessEdit', {
			type: 0,
			condo: Template.instance().condoId.get(),
			user: event.currentTarget.children[0].getAttribute('user'),
		});
	},
	'click #seeWrite, click #seeDelete': function(event, template) {
		const index = event.currentTarget.children[0].getAttribute('name');
		$($(event.currentTarget).children()[0])[0].checked = !($($(event.currentTarget).children()[0])[0].checked);
		$(`#seeOnly input[name='${index}']`)[0].checked = false;
		Meteor.call('saveAccessEdit', {
			type: $(event.currentTarget).attr('id') == 'seeWrite' ? 1 : 2,
			condo: Template.instance().condoId.get(),
			user: event.currentTarget.children[0].getAttribute('user'),
		});
	},
	'click #moduleOption': function(event, template) {
		const module = event.currentTarget.children[0].getAttribute('name');
		const value = $(`#moduleOption > [name=${module}]`).prop('checked');
		let option = {};
    option[module] = value;
    console.log('option', option)
		Meteor.call('saveCondoOptions', template.condoId.get(), option);
	},
	'click .tabCondos': function(event, template) {
		FlowRouter.go('app.backoffice.condos.condo_details_tabs', {id: Template.instance().condoId.get(), tab: event.currentTarget.getAttribute('tab')});
		template.selectedTab.set(event.currentTarget.getAttribute('tab'));
	},
	'click #saveCondoOption': function(event, template) {
		template.isError.set(false);
		for (let i = 0; i != $('input.condoOption').length; i++) {
			if (!$($('input.condoOption')[i]).val()) {
				template.isError.set(true);
				$($('input.condoOption')[i]).addClass('errorModalInput');
			}
			else {
				$($('input.condoOption')[i]).removeClass('errorModalInput');
			}
		}
		if (!template.isError.get()) {
			Meteor.call('editCondoInfo', template.condoId.get(), $("input.condoOption[placeholder='Nom']").val(), {
				address: $("input.condoOption[placeholder='Adresse']").val(),
				city: $("input.condoOption[placeholder='Ville']").val(),
				code: $("input.condoOption[placeholder='Code postal']").val(),
				id: $("input.condoOption[placeholder='Identifiant']").val(),
			});
			sAlert.success('Les modifications ont été apporté avec succès');
		}
	},
	'click .remove': function(event, template) {
		event.stopPropagation();
		bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: "Voulez-vous vraiment retirer ce gestionnaire ?",
			buttons : {
				'cancel' : {label: "Annuler", className: "btn-outline-red-confirm"},
				'confirm' : {label: "Confirmer", className: "btn-red-confirm"}
			},
			backdrop: true,
			callback: function(result) {
				if (result) {
					const managerId = event.currentTarget.getAttribute('managerid');
					const gestionnaire = Enterprises.findOne({'users.userId': managerId});
					if (managerId) {
						Meteor.call('removeCondoInCharge', gestionnaire._id, managerId, FlowRouter.getParam('id'));
						sAlert.success('Le gestionnaire a été retiré avec succès');
					}
				}
			}
		});
	},
	'click .removeBuilding': function(event, template) {
		event.stopPropagation();
		bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: "Voulez-vous vraiment supprimer ce bâtiment ?",
			buttons : {
				'cancel' : {label: "Annuler", className: "btn-outline-red-confirm"},
				'confirm' : {label: "Confirmer", className: "btn-red-confirm"}
			},
			backdrop: true,
			callback: function(result) {
				if (result) {
					const buildingId = event.currentTarget.getAttribute('buildingId');
					if (buildingId) {
						Meteor.call('removeBuilding', buildingId, function(error, result) {
							if (!error)
								sAlert.success("Le bâtiment a été supprimé avec succès");
							else
								sAlert.error(error);
						});
					}
				}
			}
		});
	},
	'click .removeResident': function(event, template) {
		event.stopPropagation();
		bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: "Voulez-vous vraiment retirer cet occupant ?",
			buttons : {
				'cancel' : {label: "Annuler", className: "btn-outline-red-confirm"},
				'confirm' : {label: "Confirmer", className: "btn-red-confirm"}
			},
			backdrop: true,
			callback: function(result) {
				if (result) {
					const residentId = event.currentTarget.getAttribute('residentid');
					if (residentId) {
						Meteor.call('removeResidentOfCondo', residentId, FlowRouter.getParam('id'));
						sAlert.success("L'occupant a été retiré avec succès");
					}
				}
			}
		});
	},
	'input #residentSearch' (event, template) {
		let input = event.currentTarget.value;
		for (let i = 0; i != Object.keys(regexpSearch).length; i++) {
		  input = input.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]]);
		}
		template.residentSearch.set(input);
	  },
});

Template.condoDetails.helpers({
	selectedTab: (name) => {
		return Template.instance().selectedTab.get() == name;
	},
	selectTabList: (list) => {
		list = list.split(';');
		return _.contains(list, Template.instance().selectedTab.get());
	},
	condo: () => {
		let condo = Condos.findOne({_id: Template.instance().condoId.get()});
		if (condo)
			return condo;
	},
	options: (option) => {
		let condo = Condos.findOne({_id: Template.instance().condoId.get()});
		if (condo && condo.settings.options[option])
			return condo.settings.options[option];
	},
	getManagerInCharge: () => {
		const condoId = Condos.findOne({_id: Template.instance().condoId.get()});
		if (condoId) {
			let users = Meteor.users.find({$and : [{'identities.gestionnaireId': {$exists: true}}, {'identities.adminId': {$exists: false}}]}).fetch();
			const manager = Enterprises.findOne({condos: condoId._id});
			if (users && manager) {
				_.each(_.filter(users, function(elem) {
					return elem.identities.gestionnaireId == manager._id;
				}), function(user) {
					const gestUser = _.find(manager.users, function(elem) {
						return elem.userId == user._id;
					});
					user.access = _.find(gestUser.condosInCharge, function(condo) {
						return condo.condoId == condoId._id;
					});
					if (user.access !== undefined)
						user.access = user.access.access;
					user.profile.email = user.emails[0].address;
				});
				users = _.filter(users, function(elem) {
					return elem.access !== undefined;
				});
				return Template.instance().residentSearch.get() ? _.filter(users, function(elem) {
					let regexp = new RegExp(Template.instance().residentSearch.get(), "i");
					return elem.profile.firstname.match(regexp) || elem.profile.lastname.match(regexp)
					|| elem.profile.email.match(regexp) || elem.profile.tel.match(regexp);
				}) : users;
			}
		}
	},
	accessRight: (type, access) => {
		return _.find(access, function(elem) {
			return elem == type;
		}) !== undefined;
	},
	getResidentInCharge: () => {
		const condoId = Condos.findOne({_id: Template.instance().condoId.get()});
		if (condoId) {
			const residents = Residents.find({$or: [
				{'condos.condoId': condoId._id},
				{'pendings.condoId': condoId._id},
				]}).fetch();
			let users = _.filter(Meteor.users.find({$and: [{'identities.residentId': {$exists: true} }, {'identities.adminId': {$exists: false} }]}).fetch(), function(user) {
				if (residents)
					return _.find(residents, function(elem) {
						return elem.userId == user._id;
					}) !== undefined;
			});
			_.each(residents, function(resident) {
				const building = _.filter(_.map(resident, function(value, key) {
					if (key == 'condos' || key == 'userId' ||key == 'pendings')
						return value;
				}), function(elem) {
					return elem !== undefined;
				});
				let user = _.find(users, function(elem) {
					return elem._id == building[0];
				});
				if (user) {
					user.building = building[1];
					user.pending = building[2];
					user.profile.email = user.emails[0].address;
				}
			});
			return Template.instance().residentSearch.get() ? _.filter(users, function(elem) {
				let regexp = new RegExp(Template.instance().residentSearch.get(), "i");
				return elem.profile.firstname.match(regexp) || elem.profile.lastname.match(regexp)
				|| elem.profile.email.match(regexp) || elem.profile.tel.match(regexp);
			}) : users;
		}
	},
	getBuilding: (condoId, building) => {
		if (condoId == Template.instance().condoId.get()) {
			if (building.type == 'Invitation')
				return "Non confirmé<br>";
			const build = Buildings.findOne(building.buildingId);
			if (build && build != undefined && build.name)
				return build.name + "<br>";
			else
				"-" + "<br>";
		}
	},
	getStatut: (condoId, building, residentId) => {
		if (condoId == Template.instance().condoId.get()) {
			if (building.type == 'Invitation')
				return "en attente...<br>";
			return Meteor.getBuildingStatusValue(building.type, true, null) + '<br>';
		}
	},
	getChoosenTab: () => {
		return Template.instance().chooseTab.get();
	},
	isError: () => {
		return Template.instance().isError.get();
	},
	getEnterprise: (userId) => {
		const gestionnaire = Enterprises.findOne({'users.userId': userId});
		if (gestionnaire) {
			return gestionnaire.name;
		}
	},
	getBuildings: () => {
		let condoId = Template.instance().condoId.get();
		if (Condos.findOne(condoId) != undefined) {
			let buildingsId = Condos.findOne(condoId).buildings;
			if (buildingsId && buildingsId != [] && buildingsId != undefined && buildingsId != "") {
				let buildings = Buildings.find({"_id": {$in: buildingsId}}).fetch();
				return _.map(buildings, function(building) {
					return {"_id": building._id, "name": (building.name == building.info.address ? "-" : building.name), "address": building.info.address + ", " + building.info.city + " " + building.info.code};
				})
			}
		}
	},
	getEntrepriseBuilding: () => {
		const gestionnaire = Enterprises.findOne({'condos': Template.instance().condoId.get()});
		if (gestionnaire) {
			return gestionnaire.name;
		}
	},
	getTemplateCharacteristic: () => {
		let moduleName = FlowRouter.getParam("module");
		let paramName = FlowRouter.getParam("moduleParam");
		if (moduleName && paramName) {
			return moduleName + "_" + paramName;
		}
		else
			return "characteristics"
	},
	getTitleBack: () => {
		if (FlowRouter.getParam("moduleParam"))
			return "Retour aux caracteristiques"
		return "Retour à la liste";
	},
	getRoleName: (userId) => {
		let condoId = Template.instance().condoId.get();
		let userRight = UsersRights.findOne({userId: userId, condoId: condoId});
		if (userRight)
			return DefaultRoles.findOne({_id: userRight.defaultRoleId}).name;
		else
			return "Not validated";
	},

});
