Template.override_userright_modale.onCreated(function(){
});

Template.override_userright_modale.onRendered(function(){
	$("#usersSelected").select2();
});

Template.override_userright_modale.events({
	'click #valid': function(event, template) {
		let userIds = $("#usersSelected").val();
		_.each(userIds, function(userId){
			Meteor.call('removeUserRight', userId, FlowRouter.getParam("id"), template.data.module, template.data.right);
		});
		$('#override_userright_modale').modal('hide');
	},
});

Template.override_userright_modale.helpers({
});