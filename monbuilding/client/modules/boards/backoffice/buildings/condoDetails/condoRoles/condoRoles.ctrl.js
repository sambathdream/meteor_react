import { Template } from "meteor/templating";

Template.condoRoles.onCreated(function(){
	this.findUserForCondoAndRoleResult = new ReactiveVar([]);
	this.findUserForCondoAndRoleModule = new ReactiveVar();
	this.findUserForCondoAndRoleRight = new ReactiveVar();
	Meteor.subscribe('getCondoRole', FlowRouter.getParam('id'));
	this.condoOptionsRelation = [
		{
			condoOption: "forum",
			userRight: "forum"
		},
		{
			condoOption: "classifieds",
			userRight: "ads"
		},
		{
			condoOption: "reservations",
			userRight: "reservation"
		},
		{
			condoOption: "map",
			userRight: "map"
		},
		{
			condoOption: "manual",
			userRight: "manual"
		},
		{
			condoOption: "trombi",
			userRight: "trombi"
		},
		{
			condoOption: "incidents",
			userRight: "incident"
		},
		{
			condoOption: "informations",
			userRight: "actuality"
		},
		{
			condoOption: "informations",
			userRight: "actuality"
		},
	];
});

Template.condoRoles.onRendered(function(){
});

Template.condoRoles.events({
	'click .labelForCheckbox': function(event, template) {
		let currentDefaultRole = Session.get("currentDefaultRole");
		let edition = $(event.currentTarget).attr("for").split('.');
		edition[2] = edition[2] == "true" ? true : edition[2] == "false" ? false : null;

		Meteor.call('updateCondoRole', FlowRouter.getParam('id'), currentDefaultRole.for, currentDefaultRole.name,  edition[0], edition[1], edition[2]);

		Meteor.call('findUserForCondoAndRole', FlowRouter.getParam('id'), currentDefaultRole._id, edition[0], edition[1], function(error, result){
			if (error) {
				sAlert.error(error);
				return false;
			}
			if (!_.isEmpty(result)) {
				template.findUserForCondoAndRoleResult.set(result);
				template.findUserForCondoAndRoleModule.set(edition[0]);
				template.findUserForCondoAndRoleRight.set(edition[1]);
				$('#override_userright_modale').modal('show');
			}
		});
	}
});

Template.condoRoles.helpers({
	getFindUserForCondoAndRoleResult: () => {
		return Template.instance().findUserForCondoAndRoleResult.get();
	},
	getFindUserForCondoAndRoleModule: () => {
		return Template.instance().findUserForCondoAndRoleModule.get();
	},
	getFindUserForCondoAndRoleRight: () => {
		return Template.instance().findUserForCondoAndRoleRight.get();
	},
	getCondoRoles: () => {
		let currentDefaultRole = DefaultRoles.findOne(Session.get("currentDefaultRole")._id);
		if (!currentDefaultRole)
			return undefined;

		let currentCondoRole = CondoRole.findOne({
			condoId: FlowRouter.getParam('id'),
			for: currentDefaultRole.for,
			name: currentDefaultRole.name
		});

		let mergedRoles = Meteor.mergeRoles(currentDefaultRole, currentCondoRole);
		mergedRoles.rights = _.sortBy(mergedRoles.rights, function(right){
      return right.displayOrder;
		});

		let condo = Condos.findOne(FlowRouter.getParam('id'));
		let toRemove = [];
		_.each(mergedRoles.rights, function(val, key){
			let relation = findRelation(val.module, Template.instance().condoOptionsRelation);
			if (relation !== false) {
				if (!condo.settings.options[relation]) {
					mergedRoles.rights[key].disabled = true;
					mergedRoles.rights.push(mergedRoles.rights[key]);
					toRemove.push(key);
				}
			}
		});
		let i = toRemove.length - 1;
		while (i >= 0) {
			mergedRoles.rights.splice(toRemove[i], 1);
			i--;
		}
		return mergedRoles;
	},
	iscurrentDisplayingModule: (moduleName) => {
    if (Template.instance().currentDisplayingModule == moduleName && moduleName !== 'view')
			return true;
		Template.instance().currentDisplayingModule = moduleName;
		return false;
	},
	condoOptions: (option) => {
		let condo = Condos.findOne(FlowRouter.getParam('id'));
		if (condo) {
			return condo.settings.options[option];
		}
	},
  viewName: (moduleName, displayOrder) => {
    return moduleName + displayOrder
  }
});

Meteor.containsRight = function (rights, module, right) {
	let i = 0;
	while (i < rights.length) {
		if (rights[i].module == module && rights[i].right == right)
			return rights[i];
		i++;
	}
	return false;
}

Meteor.mergeRoles = function (defaultRole, condoRole) {
	let merged = {
		for: defaultRole.for,
		name: defaultRole.name,
		rights: []
	};

	_.each(defaultRole.rights, function(defaultRoleRight, key){
		if (defaultRoleRight.default == null)
			return;
		let containsRightRet = condoRole ? Meteor.containsRight(condoRole.rights, defaultRoleRight.module, defaultRoleRight.right) : false;
		if (containsRightRet != false) {
      containsRightRet.displayOrder = defaultRoleRight.displayOrder
			containsRightRet['value-fr'] = defaultRoleRight['value-fr']
			containsRightRet['value-en'] = defaultRoleRight['value-en'];
			merged.rights.push(containsRightRet);
		}
		else
			merged.rights.push(defaultRoleRight);
	});
	return merged;
}

function findRelation(module, condoOptionsRelation) {
	let i = 0;
	while (i < condoOptionsRelation.length) {
		if (condoOptionsRelation[i].userRight == module)
			return condoOptionsRelation[i].condoOption;
		i++;
	}
	return false;
}