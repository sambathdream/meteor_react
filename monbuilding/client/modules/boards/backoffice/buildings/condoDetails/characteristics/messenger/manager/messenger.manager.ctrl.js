Template.messenger_manager.onCreated(function() {
  this.condoId = FlowRouter.getParam('id')
  this.handler = Meteor.subscribe('admin_userFiles_messenger_condo_caracteristic')
  this.selectedType = new ReactiveVar('')
  this.selectedIncidentDetails = new ReactiveVar('')
  this.incidentMessageFr = new ReactiveVar('')
  this.incidentMessageEn = new ReactiveVar('')
})

Template.messenger_manager.onRendered(function () {
})

Template.messenger_manager.events({
  'change [name="typeRadio"]': function (event, template) {
    template.selectedType.set($(event.currentTarget).val())
  },
  'click #saveCustomIncidentMessage': function (event, template) {
    const condoId = Template.instance().condoId
    const incidentMessageFr = template.incidentMessageFr.get()
    if (!incidentMessageFr) {
      sAlert.error('Merci de remplir tous les champs obligatoires')
      return false
    }
    const incidentMessageEn = template.incidentMessageEn.get()
    if (!incidentMessageEn) {
      sAlert.error('Merci de remplir tous les champs obligatoires')
      return false
    }
    Meteor.call('updateCondosModulesOptions', condoId, { module: 'incident', param: 'customMessage-fr', toChange: null, newValue: incidentMessageFr }, function (error, result) {
      if (!error) {
        sAlert.success('Les modifications ont été apporté avec succès - FR')
      }
    })
    Meteor.call('updateCondosModulesOptions', condoId, { module: 'incident', param: 'customMessage-en', toChange: null, newValue: incidentMessageEn }, function (error, result) {
      if (!error) {
        sAlert.success('Les modifications ont été apporté avec succès - EN')
      }
    })
  },
  'dblclick .details': function (event, template) {
    event.preventDefault()
    event.stopPropagation()
  }
})

Template.messenger_manager.helpers({
  getBrokenValue: (elem) => {
    let lang = FlowRouter.getParam('lang') || 'fr'
    let ret = elem['value-' + lang]
    ret = ret.replace('/', '/<br>')
    return ret
  },
  isTypeSelected: () => {
    if (Template.instance().selectedType.get() !== '')
      return true
    return false
  },
  isIncidentSelected: () => {
    if (Template.instance().selectedType.get() !== '') {
      let thisType = IncidentType.findOne(Template.instance().selectedType.get())
      if (thisType && thisType.defaultType === 'incident')
        return true
    }
    return false
  },
  getDropdownElem: () => {
    let listDropdown = ContactManagement.findOne({condoId: Template.instance().condoId})
    if (!listDropdown) {
      let condoType = Condos.findOne(Template.instance().condoId).settings.condoType
      listDropdown = ContactManagement.findOne({forCondoType: condoType})
    }
    if (listDropdown) {
      let listOfIds = _.find(listDropdown.messengerSet, function(list) {
        return list.id === Template.instance().selectedType.get()
      })
      if (listOfIds)
        return listOfIds.declarationDetailIds
    }
  },
  getCondoId: () => {
    return Template.instance().condoId
  },
  allReady: () => {
    if (!Template.instance().handler.ready() || !IncidentType.find().count() || !ContactManagement.find().count())
      return false
    return true
  },
  getTypeId: () => {
    return Template.instance().selectedType.get()
  },
  getValue_fr: () => {
    const condoId = Template.instance().condoId
    const condoModulesOptions = CondosModulesOptions.findOne({ condoId: condoId })
    if (condoModulesOptions) {
      return condoModulesOptions.incident['customMessage-fr']
    }
  },
  onBlur_fr: () => {
    const template = Template.instance()
    return value => template.incidentMessageFr.set(value)
  },
  getValue_en: () => {
    const condoId = Template.instance().condoId
    const condoModulesOptions = CondosModulesOptions.findOne({ condoId: condoId })
    if (condoModulesOptions) {
      return condoModulesOptions.incident['customMessage-en']
    }
  },
  onBlur_en: () => {
    const template = Template.instance()
    return value => template.incidentMessageEn.set(value)
  },
})
