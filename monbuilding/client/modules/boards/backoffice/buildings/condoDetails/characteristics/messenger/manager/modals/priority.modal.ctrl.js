import { FileManager } from '/client/components/fileManager/filemanager.js';

function removeBorder(priorityId) {
	$("#spanPicto" + priorityId).css("border", "");
	$("#inputFr" + priorityId).css("border", "");
	$("#inputEn" + priorityId).css("border", "");
}

Template.messenger_manager_priority_modal.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.fm = new FileManager(UserFiles, {condoId: this.condoId, messengerType: true});
	this.isOpen = new ReactiveVar(false);
	this.isAddingElem = new ReactiveVar(false);
});

Template.messenger_manager_priority_modal.onRendered(function() {
});

Template.messenger_manager_priority_modal.events({
	'hidden.bs.modal #modal_messenger_manager_priority': function(event, template) {
		template.isOpen.set(false);
		template.fm.clearFiles()
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_messenger_manager_priority': function(event, template) {
		template.isOpen.set(true);
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_messenger_manager_priority";
		window.onhashchange = function() {
			if (location.hash != "#modal_messenger_manager_priority"){
				modal.modal('hide');
			}
		}
	},
	'click .createNewType' (event, template) {
		template.isAddingElem.set(true);
		Meteor.call("createNewIncidentPriority", template.condoId, function(error, result) {
			if (!error)
				template.isAddingElem.set(false);
		});
	},
	'click .removeType' (event, template) {
		let priorityId = $(event.currentTarget).attr("priorityId");
		$(".loader" + priorityId).css("display", "");
		Meteor.call("deleteIncidentPriority", template.condoId, priorityId, function(error, result) {
		});
	},
	'click .typeManagerBackofficeModal' (event, template) {
		let priorityId = $(event.currentTarget).attr("priorityId");
		$('#inputPictoEdit' + priorityId).click();
	},
	'click #save' (event, template) {
		$("#save").button("loading");
		let type = $(".allType");
		$(".loader").css("display", "")
		$(".fail").css("display", "none");
		$(".done").css("display", "none");
		let condoId = template.condoId;
		type.each(function(index, elem) {
			let priorityId = $(elem).attr("priorityId");
			removeBorder(priorityId);
			let pictureLink = $("#spanPicto" + priorityId).attr('fileLink');
			let value_fr = $("#inputFr" + priorityId).val();
			let value_en = $("#inputEn" + priorityId).val();
			let showUrgent =  $("#showUrgent" + priorityId).is(':checked');
			Meteor.call("saveIncidentPriorityForCondo", condoId, {priorityId, pictureLink, value_fr, value_en, showUrgent}, function(error, result) {
				$(".loader" + priorityId).css("display", "none");
				if (error) {
					console.log(error);
					if (error.error == 531) {
						let errors = error.reason.split("|");
						_.each(errors, function(elem) {
							$("#" + elem +  priorityId).css("border", "2px solid red");
						})
					}
					$(".fail" + priorityId).css("display", "");
				}
				else
					$(".done" + priorityId).css("display", "");
			})
		}).promise().done(function() { $("#save").button("reset"); })
	},
	'change [name="inputPicto"]': function(event, template) {
		if (event.currentTarget.files) {
			var elapsed = 0;
			let priorityId = $(event.currentTarget).attr("priorityId");
			$(".globalLoader" + priorityId).css("display", "");

			let id = template.fm.insert(event.currentTarget.files[0]);

			var timer = setInterval(function() {
				if (template.fm.getFileList()[id].done) {
					clearInterval(timer);
					var waitForImage = setInterval(function() {
						if (UserFiles.findOne(template.fm.getFileList()[id].file._id)) {
							let thisFile = UserFiles.findOne(template.fm.getFileList()[id].file._id).link();
							$(".globalLoader" + priorityId).addClass("p" + 100);
							$(".fileLoader" + priorityId).text(100 + "%");


							if (thisFile) {
								let absoluteLinkRe = /.*(\/download.*)/ig;
								let reResult = absoluteLinkRe.exec(thisFile);
								if (reResult[1]) {
									let background = "url('" + reResult[1] + "') top left no-repeat #dffbff";
									$("#spanPicto" + priorityId).css('background', background);
									$("#spanPicto" + priorityId).attr('fileLink', reResult[1]);
									$("#spanPicto" + priorityId).children().first().removeClass("show_camera_edit");
									setTimeout(function() {
										$(".globalLoader" + priorityId).css("display", "none");
									}, 500);
								}
							}
							clearInterval(waitForImage);
						}
					}, 100);
				}
				if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
					$(".globalLoader" + priorityId).removeClass("p" + elapsed);
					elapsed = template.fm.getFileList()[id].upload.progress.get();
					$(".globalLoader" + priorityId).addClass("p" + elapsed);
					$(".fileLoader" + priorityId).text(elapsed + "%");
				}
			}, 100);
		}
	},
});

Template.messenger_manager_priority_modal.helpers({
	initSwitch: (priorityId) => {
		setTimeout(function() {
			if (priorityId == "sampleCheckbox")
				$("#" + priorityId).bootstrapSwitch();
			else
				$("#showUrgent" + priorityId).bootstrapSwitch();
		}, 100);
	},
	isOpen: () => {
		return Template.instance().isOpen.get() && IncidentPriority.findOne();
	},
	isAddingElement: () => {
		return Template.instance().isAddingElem.get();
	},
	getCondoId: () => {
		return Template.instance().condoId;
	}
});
