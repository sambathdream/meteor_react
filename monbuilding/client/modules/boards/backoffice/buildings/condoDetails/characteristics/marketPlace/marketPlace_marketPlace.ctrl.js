Template.marketPlace_marketPlace.onCreated(function () {
  this.condoId = FlowRouter.getParam('id')
  this.handler = Meteor.subscribe('admin_userFiles_messenger_condo_caracteristic')
})

Template.marketPlace_marketPlace.onRendered(function () {
})

Template.marketPlace_marketPlace.events({
})

Template.marketPlace_marketPlace.helpers({
  getCondoSettings: () => {
    const condoId = Template.instance().condoId
    return Condos.findOne({ _id: condoId }).settings
  },
  getIsActive: (settings, name, isBuildingOption) => {
    const t = Template.instance()
    return settings && settings.accounting && settings.accounting[name] && (isBuildingOption ? settings.accounting[name].isActive : true)
  },
  getForm: (settings, name) => {
    const t = Template.instance()
    return settings && settings.accounting && settings.accounting[name] && (settings.accounting[name].value || '<i>unknown</i>')
  },
  onCheckboxClick: (settings, name, isBuildingOption) => {
    const t = Template.instance()
    const condoId = t.condoId
    return () => () => {
      const value = !(settings && settings.accounting && settings.accounting[name] && (isBuildingOption ? settings.accounting[name].isActive : settings.accounting[name]))
      Meteor.call('saveCondoAccountingOptions', condoId, name, value, isBuildingOption, (error, result) => {
        console.log('error', error)
      })
    }
  }
})
