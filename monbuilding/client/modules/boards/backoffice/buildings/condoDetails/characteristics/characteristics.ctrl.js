Template.characteristics.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
});

Template.characteristics.onRendered(function() {

});

Template.characteristics.events({
	'click .editThisModule': function (event, template) {
		let condoId = FlowRouter.getParam("id");
		let tab = FlowRouter.getParam("tab");
		let module = $(event.currentTarget).attr("moduleName")
		let moduleParam = $(event.currentTarget).attr("moduleParam");
		FlowRouter.go("app.backoffice.condos.condo_details_tabs.moduleParam", {id: condoId, tab: tab, module: module, moduleParam: moduleParam})
	},
	'click #dropdownType': function(event, template) {
		const type = event.currentTarget.getAttribute('type');
		Meteor.call('changeCondoTypeOption', type, FlowRouter.getParam("id"), function(error) {
			if (error)
				sAlert.error(error);
			else
				sAlert.success("Typologie de l'immeuble modifié")
		});
	},
});

Template.characteristics.helpers({
	options: (option) => {
		let condo = Condos.findOne({_id: Template.instance().id.get()});
		if (condo && condo.settings.options[option]) {
			return condo.settings.options[option];
		}
	},
	getTypo: () => {
		const translateTab = {'etudiante': 'Résidence étudiante',
		'mono': 'Monopropriété',
		'copro': 'Copropriété',
		'office': 'Office'};
		let condo = Condos.findOne({_id: Template.instance().id.get()});
		if (condo && condo.settings.condoType)
			return translateTab[condo.settings.condoType];
	},
	getCustomEmailSender: () => {
		let condo = Condos.findOne({ _id: Template.instance().id.get() });
		if (condo && !!condo.settings.customEmail) {
			return condo.settings.customEmail
		}
		return false
	}
});
