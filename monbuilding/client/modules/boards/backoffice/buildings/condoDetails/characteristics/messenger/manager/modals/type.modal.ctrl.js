import { FileManager } from '/client/components/fileManager/filemanager.js';

function removeBorder(typeId) {
	$("#spanPicto" + typeId).css("border", "");
	$("#inputFr" + typeId).css("border", "");
	$("#inputEn" + typeId).css("border", "");
}

Template.messenger_manager_type_modal.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.fm = new FileManager(UserFiles, {condoId: this.condoId, messengerType: true});
	this.isOpen = new ReactiveVar(false);
	this.isAddingElem = new ReactiveVar(false);
});

Template.messenger_manager_type_modal.onRendered(function() {
});

Template.messenger_manager_type_modal.events({
	'hidden.bs.modal #modal_messenger_manager_type': function(event, template) {
		template.isOpen.set(false);
		template.fm.clearFiles()
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_messenger_manager_type': function(event, template) {
		template.isOpen.set(true);
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_messenger_manager_type";
		window.onhashchange = function() {
			if (location.hash != "#modal_messenger_manager_type"){
				modal.modal('hide');
			}
		}
	},
	'click .createNewType' (event, template) {
		template.isAddingElem.set(true);
		Meteor.call("createNewIncidentType", template.condoId, function(error, result) {
			if (!error)
				template.isAddingElem.set(false);
		});
	},
	'click .removeType' (event, template) {
		let typeId = $(event.currentTarget).attr("typeId");
		$(".loader" + typeId).css("display", "");
		Meteor.call("deleteIncidentType", template.condoId, typeId, function(error, result) {
		});
	},
	'click .typeManagerBackofficeModal' (event, template) {
		let typeId = $(event.currentTarget).attr("typeId");
		$('#inputPictoEdit' + typeId).click();
	},
	'click #save' (event, template) {
		$("#save").button("loading");
		let type = $(".allType");
		$(".loader").css("display", "")
		$(".fail").css("display", "none");
		$(".done").css("display", "none");
		let condoId = template.condoId;
		type.each(function(index, elem) {
			let typeId = $(elem).attr("typeId");
			removeBorder(typeId)
			let pictureLink = $("#spanPicto" + typeId).attr('fileLink');
			let value_fr = $("#inputFr" + typeId).val();
			let value_en = $("#inputEn" + typeId).val();
			let isIncident =  $("#isIncident" + typeId).is(':checked');
			Meteor.call("saveIncidentTypeForCondo", condoId, {typeId, pictureLink, value_fr, value_en, isIncident}, function(error, result) {
				$(".loader" + typeId).css("display", "none");
				if (error) {
					if (error.error == 531) {
						let errors = error.reason.split("|");
						_.each(errors, function(elem) {
							$("#" + elem +  typeId).css("border", "2px solid red");
						})
					}
					$(".fail" + typeId).css("display", "");
				}
				else
					$(".done" + typeId).css("display", "");
			})
		}).promise().done(function() { $("#save").button("reset"); })
	},
	'change [name="inputPicto"]': function(event, template) {
		if (event.currentTarget.files) {
			var elapsed = 0;
			let typeId = $(event.currentTarget).attr("typeId");
			$(".globalLoader" + typeId).css("display", "");

			let id = template.fm.insert(event.currentTarget.files[0]);

			var timer = setInterval(function() {
				if (template.fm.getFileList()[id].done) {
					clearInterval(timer);
					var waitForImage = setInterval(function() {
						if (UserFiles.findOne(template.fm.getFileList()[id].file._id)) {
							let thisFile = UserFiles.findOne(template.fm.getFileList()[id].file._id).link();
							$(".globalLoader" + typeId).addClass("p" + 100);
							$(".fileLoader" + typeId).text(100 + "%");


							if (thisFile) {
								let absoluteLinkRe = /.*(\/download.*)/ig;
								let reResult = absoluteLinkRe.exec(thisFile);
								if (reResult[1]) {
									let background = "url('" + reResult[1] + "') top left no-repeat #dffbff";
									$("#spanPicto" + typeId).css('background', background);
									$("#spanPicto" + typeId).attr('fileLink', reResult[1]);
									$("#spanPicto" + typeId).children().first().removeClass("show_camera_edit");
									setTimeout(function() {
										$(".globalLoader" + typeId).css("display", "none");
									}, 500);
								}
							}
							clearInterval(waitForImage);
						}
					}, 100);
				}
				if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
					$(".globalLoader" + typeId).removeClass("p" + elapsed);
					elapsed = template.fm.getFileList()[id].upload.progress.get();
					$(".globalLoader" + typeId).addClass("p" + elapsed);
					$(".fileLoader" + typeId).text(elapsed + "%");
				}
			}, 100);
		}
	},
});

Template.messenger_manager_type_modal.helpers({
	initSwitch: (typeId) => {
		setTimeout(function() {
			if (typeId == "sampleCheckbox")
				$("#" + typeId).bootstrapSwitch();
			else
				$("#isIncident" + typeId).bootstrapSwitch();
		}, 100);
	},
	isOpen: () => {
		return Template.instance().isOpen.get() && IncidentType.findOne();
	},
	isAddingElement: () => {
		return Template.instance().isAddingElem.get();
	},
	getCondoId: () => {
		return Template.instance().condoId;
	}
});
