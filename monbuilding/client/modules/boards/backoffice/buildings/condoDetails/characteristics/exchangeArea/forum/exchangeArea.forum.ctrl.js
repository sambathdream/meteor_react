let timer = null

function initiateSaver(name) {
  if (!($("#noty_topRight_layout_container")[0])) {
    $("#noty_topRight_layout_container").remove();
    $(".display-modules").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">Sauvegarde de ' + name + '..</span></div></div></li></ul>');
    $("#noty-li").animate({"height": "34.4502px"}, "fast");
  }
};

function updateSaver(ret) {
  if (!ret) {
    $("#noty_topRight_layout_container").remove();
  }
  else {
    if (ret.error && ret.error == 403)
      $(".noty_text").html(ret.reason);
    else
      $(".noty_text").html(ret);
    $("#noty-li").css(
      {
        'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
        'border-radius': '5px',
        'border': '1px solid red',
        'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
        'color': 'red',
        'width': '310px',
        'cursor': 'pointer',
        'height': '34.4502px'
      }
    );
    setTimeout(function() {
      $("#noty_topRight_layout_container").remove();
    }, 3000)
  }
};

Template.exchangeArea_forum.onCreated(function() {
  this.condoId = FlowRouter.getParam("id");
});

Template.exchangeArea_forum.onRendered(function() {

});

Template.exchangeArea_forum.events({
  'click [name="checkboxVisibleBy"]': (event, template) => {
    let status = $(event.currentTarget).is(':checked');
    let name = $(event.currentTarget).attr("typeName");
    initiateSaver(name);
    if (timer != null) {
      clearTimeout(timer);
      timer = null;
    }
    let condoId = template.condoId;
    const val = status === true ? name : 'normal'
    timer = setTimeout(function() {
      Meteor.call("updateCondosModulesOptions", condoId, {module: "forum", param: "type", toChange: null, newValue: val}, function(error, result) {
        updateSaver(error);
      });
    }, 500);
  },
});

Template.exchangeArea_forum.helpers({
  getCondoId: () => {
    return Template.instance().condoId;
  },
  allReady: () => {
    if (!Condos.find().fetch() || !Condos.findOne(Template.instance().condoId) || !CondosModulesOptions.findOne({condoId: Template.instance().condoId}))
      return false;
    return true;
  },
  getForumType: () => {
    return CondosModulesOptions.findOne({condoId: Template.instance().condoId}).forum.type;
  }
});
