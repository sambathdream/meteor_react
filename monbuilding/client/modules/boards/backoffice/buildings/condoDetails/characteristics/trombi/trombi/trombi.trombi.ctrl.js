Template.trombi_trombi.onCreated(function() {
	this.condoId = FlowRouter.getParam("id");
	this.id = new ReactiveVar(FlowRouter.current().params.id);
});

Template.trombi_trombi.onRendered(function() {

});

Template.trombi_trombi.events({
	'click #moduleOption': function(event, template) {
		const module = event.currentTarget.children[0].getAttribute('name');
		const value = $(`#moduleOption > [name=${module}]`).prop('checked');
		let option = {};
		option[module] = value;
		Meteor.call('saveCondoOptions', template.condoId, option);
	}
});

Template.trombi_trombi.helpers({
	getCondoId: () => {
		return Template.instance().condoId;
	},
	allReady: () => {
		if (!IncidentType.find().fetch() || !ContactManagement.find().fetch())
			return false;
		return true;
	},
	options: (option) => {
		let condo = Condos.findOne({_id: Template.instance().id.get()});
		if (condo && condo.settings.options[option])
			return condo.settings.options[option];
	},
});
