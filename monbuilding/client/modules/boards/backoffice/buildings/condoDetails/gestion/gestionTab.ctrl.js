import { Template } from "meteor/templating"
import "./gestionTab.view.html";

Template.condoDetailsGestion.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
	this.subscribe('fetchForumPosts', [this.id.get()])
	this.subscribe('forumFiles', [this.id.get()])
	this.moduleName = new ReactiveVar('stat');
	this.selectedStats = new ReactiveVar('stat');
	this.incidentFilter = new ReactiveVar("thisMonth");
	this.startDate = new ReactiveVar();
	this.endDate = new ReactiveVar();
});

Template.condoDetailsGestion.onRendered(function() {
});

Template.condoDetailsGestion.events({
	'click .tabModule': function(event, template) {
		template.moduleName.set(event.currentTarget.getAttribute('tab'));
		template.selectedStats.set(event.currentTarget.getAttribute('tab'));
	},
	'click .incidentFilter': function(event, template) {
		template.incidentFilter.set(event.currentTarget.getAttribute('filter'));
	}
});

Template.condoDetailsGestion.helpers({
	selectedStats: (name) => {
		return Template.instance().selectedStats.get() == name;
	},
	incidentFilter : () => {
		switch (Template.instance().incidentFilter.get()) {
			case "today":
				return "Aujourd'hui";
				break;
			case "yesterday":
				return "Hier";
				break;
			case "thisMonth":
				return "Ce mois";
				break;
			case "lastMonth":
				return "Mois dernier";
				break;
			case "thisYear":
				return "Cette année";
				break;
			case "lastYear":
				return "L'année dernière";
				break;
			case "calendar":
				return "Calendrier";
				break;
		}
	},
	filters: () => {
		let filters = [
			{name: "today", displayName: "Aujourd'hui"},
			{name: "yesterday", displayName: "Hier"},
			{name: "thisMonth", displayName: "Ce mois"},
			{name: "lastMonth", displayName: "Mois dernier"},
			{name: "thisYear", displayName: "Cette année"},
			{name: "lastYear", displayName: "L'année dernière"},
			{name: "calendar", displayName: "Calendrier"}
		]
		return filters;
	},
	formatHtml: (title, arg1, arg2, arg3, arg4) => {
		let html = "<tbody><tr><td>";
		html += title;
		html += "</td><td><div>";
		if (arg1 == '-')
			html += '-';
		else
			html += getNbrOfAction(arg1, arg2);
		html += "</div></td><td><div>";
		if (arg3 == '-')
			html += '-';
		else
			html += getNbrOfFollow(arg3, arg4);
		html += "</div></td></tr></tbody>";
		return html;
	},
	formatHeaderHtml: (title, arg1, arg2, arg3, arg4) => {
		let header = "<thead><tr><th class='sub-title'>";
		header += title;
		header += "</th><th class='sub-title'>";
		if (arg1 == '-')
			header += '-';
		else
			header += getNbrOfAction(arg1, arg2);
		header += "</th><th class='sub-title'>";
		if (arg3 == '-')
			header += '-';
		else
			header += getNbrOfFollow(arg3, arg4);
		header += "</th></tr></thead>";
		return header;
	},
});

function getNbrOfAction(moduleName, options) {
	let condoId = Template.instance().id.get()
	let condo = Condos.findOne({_id: condoId});

	if (condo) {
		const module = _.find(condo.modules, (elem) => {return elem.name == moduleName});
		let dateRef;
		let format;

		switch (Template.instance().incidentFilter.get()) {
			case "today":
				format = 'L';
				dateRef = moment(new Date()).format('L');
				break;
			case "yesterday":
				format = 'L';
				dateRef = moment(new Date()).add(-1, 'days').format('L');
				break;
			case "thisMonth":
				format = 'MM/YYYY';
				dateRef = moment(new Date()).format('MM/YYYY');
				break;
			case "lastMonth":
				format = 'MM/YYYY';
				dateRef = moment(new Date()).add(-1, 'month').format('MM/YYYY');
				break;
			case "thisYear":
				format = 'YYYY';
				dateRef = moment(new Date()).format('YYYY');
				break;
			case "lastYear":
				format = 'YYYY';
				dateRef = moment(new Date()).add(-1, 'year').format('YYYY');
				break;
			case "Calendar":
				format = 'L';
				dateRef = "custom";
		}

		switch (moduleName) {

			case 'incident':
				if (!condo.settings.options.incidents)
					return '-';

				const incidents = _.filter(Incidents.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
					const tmp = moment(elem.createdAt).format(format);

					if (dateRef == "custom" &&
							tmp.isAfter(Template.instance().startDate.get()) &&
							tmp.isBefore(Template.instance().endDate.get())) {
							return true;
					} else {
						return dateRef == tmp;
					}
				});

				if (options == 'message') {
					let sum = 0;
					_.each(incidents, (elem) => {
						sum += elem.history.length;
					});
					return sum;
				} else if (options == 'toValid') {
					let sum = 0;
					_.each(incidents, (elem) => {
						if (elem.state.status == 0)
							sum++;
					});
					return sum;
				} else if (options == 'open') {
					let sum = 0;
					_.each(incidents, (elem) => {
						if (elem.state.status == 1)
							sum++;
					});
					return sum;
				} else if (options == 'solve') {
					let sum = 0;
					_.each(incidents, (elem) => {
						if (elem.state.status == 2)
							sum++;
					});
					return sum;
				} else if (options == 'close') {
					let sum = 0;
					_.each(incidents, (elem) => {
						if (elem.state.status == 3)
						sum++;
					});
					return sum;
				} else if (options == 'vote') {
					let sum = 0;
					_.each(incidents, (elem) => {
						_.each(elem.eval, (e) => {
							if (e.value > 0)
								sum++;
						})
					});
					return sum;
				} else {
					return incidents.length;
				}
				break;

			case 'forum':
				if (!condo.settings.options.forum)
					return '-';

				const posts = _.filter(ForumPosts.find({}).fetch(), (elem) => {
					const tmp = moment(elem.date).format(format);
					if (dateRef == "custom" &&
							tmp.isAfter(Template.instance().startDate.get()) &&
							tmp.isBefore(Template.instance().endDate.get())) {
							return true;
					} else {
						return dateRef == tmp;
					}
				});

				if (options == 'reponse') {
					let sum = 0;
					_.each(posts, (elem) => {
						let res = ReactiveMethod.call("getForumCommentCount", condoId, elem._id);
						if (!!res)
							sum += res;
					});
					return sum;
				} else if (options == 'message') {
					return posts.length;
				} else {
					let sum = 0;
					_.each(posts, (elem) => {
						sum += ForumCommentPosts.find({ postId: elem._id }).count();
					});
					return sum + posts.length;
				}
				break;

			case 'classifieds':
				return (condo.settings.options.classifieds) ?
					_.filter(ClassifiedsAds.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
						const tmp = moment(elem.createdAt).format(format);
						if (dateRef == "custom" &&
								tmp.isAfter(Template.instance().startDate.get()) &&
								tmp.isBefore(Template.instance().endDate.get())) {
								return true;
						} else {
							return dateRef == tmp;
						}
					}).length
					: '-';
				break;

			case 'actuality':
				return (condo.settings.options.informations) ?
					_.filter(ActuPosts.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
						const tmp = moment(elem.createdAt).format(format);
						if (dateRef == "custom" &&
								tmp.isAfter(Template.instance().startDate.get()) &&
								tmp.isBefore(Template.instance().endDate.get())) {
								return true;
						} else {
							return dateRef == tmp;
						}
					}).length
					: '-';
				break;

			case 'conversation':
				const conversation = _.filter(Messages.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
					const tmp = moment(elem.date).format(format);
					if (dateRef == "custom" &&
							tmp.isAfter(Template.instance().startDate.get()) &&
							tmp.isBefore(Template.instance().endDate.get())) {
							return true;
					} else {
						return dateRef == tmp;
					}
				});

				if (options == 'conversation')
        return conversation.length;
				else if (options == 'Gardien' || options == 'Occupant' || options == 'Conseil Syndical' || options == 'Incident') {
          let sum = 0;
          if (options !== 'Incident') {
            let defaultRoles = DefaultRoles.findOne({ name: options }) || {}
            _.each(conversation, (elem) => {
              if (elem.target == defaultRoles._id)
                sum ++;
            });
          } else {
            _.each(conversation, (elem) => {
              if (elem.incidentId !== null)
                sum++;
            });
          }
					return sum;
				} else if (options == 'message') {
					let sum = 0;
					_.each(conversation, (elem) => {
						if (!!elem && !!elem.feed)
							sum += elem.feed.length;
					});
					return sum;
				} else {
					let sum = 0;
					_.each(conversation, (elem) => {
						if (!!elem && !!elem.feed)
							sum += elem.feed.length + 1;
					});
					return sum;
				}
				break;

			case 'message':
				const messages = _.filter(Messages.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
					const tmp = moment(elem.date).format(format);
					if (dateRef == "custom" &&
							tmp.isAfter(Template.instance().startDate.get()) &&
							tmp.isBefore(Template.instance().endDate.get())) {
							return true;
					} else {
						return dateRef == tmp;
					}
        });

				if (options == 'message') {
					let sum = 0;
					_.each(messages, (elem) => {
						if (!!elem && !!elem.feed)
							sum += elem.feed.length;
					});
					return sum;
				} else if (options == 'Gardien' || options == 'Occupant' || options == 'Conseil Syndical' || options == 'Incident') {
          let sum = 0;

          const conversation = _.filter(Messages.find({ condoId: Template.instance().id.get() }).fetch(), (elem) => {
            const tmp = moment(elem.date).format(format);
            if (dateRef == "custom" &&
              tmp.isAfter(Template.instance().startDate.get()) &&
              tmp.isBefore(Template.instance().endDate.get())) {
              return true;
            } else {
              return dateRef == tmp;
            }
          });

          if (options !== 'Incident') {
            let defaultRoles = DefaultRoles.findOne({ name: options }) || {}
            _.each(conversation, (elem) => {
              if (elem.target == defaultRoles._id) {
                sum += MessagesFeed.find({ messageId: elem._id }).count()
                console.log('sum', sum);
              }
            });
          } else {
            _.each(conversation, (elem) => {
              if (elem.incidentId !== null)
                sum += MessagesFeed.find({ messageId: elem._id }).count()
            });
          }
					_.each(messages, (elem) => {
						if (!!elem && !!elem.feed && elem.target == options)
							sum += elem.feed.length;
					});
					return sum;
				} else {
					let sum = 0;
					_.each(messages, (elem) => {
						if (!!elem && !!elem.feed)
							sum += elem.feed.length + 1;
					});
					return sum;
				}
				break;

			case 'conciergerie':
				if (condo.settings.options.conciergerie)
					return _.filter(ConciergeMessage.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
						const tmp = moment(elem.date).format(format);
						if (dateRef == "custom" &&
								tmp.isAfter(Template.instance().startDate.get()) &&
								tmp.isBefore(Template.instance().endDate.get())) {
								return true;
						} else {
							return dateRef == tmp;
						}
					}).length;
				return '-';
				break;

			default:
				return '-';
		}
	}
	return 'N/A';
};
function getNbrOfFollow(moduleName, options) {
	const resident = Residents.find({'condos.condoId': Template.instance().id.get()}).fetch();
	const condo = Condos.findOne({_id: Template.instance().id.get()});

	if (condo) {
		const module = _.find(condo.modules, (elem) => {return elem.name == moduleName});
		const reSize = _.size(resident);

		switch (moduleName) {
			case 'forum':

				let sum = reSize;
				if (options == 'reponse') {

					const posts = _.filter(ForumPosts.find({}).fetch(), (elem) => {
						const tmp = moment(elem.date).format(format);
						if (dateRef == "custom" &&
						tmp.isAfter(Template.instance().startDate.get()) &&
						tmp.isBefore(Template.instance().endDate.get())) {
							return true;
						} else {
							return dateRef == tmp;
						}
					});

					_.each(posts, (elem) => {
						let unfollowList = UnFollowForumPosts.findOne({postId: elem._id})
						if (!!unfollowList)
						sum += reSize - _.size(unfollowList.unfollowList);
					});
					return sum;
				}
				let unfollowList = UnFollowForumPosts.find({ condoId: Template.instance().id.get() }).fetch()
				if (!!unfollowList) {
					_.each(unfollowList, (list) => {
						sum += reSize - _.size(list.unfollowList);
					})
				}
				return sum
				break;

			case 'actuality':
				const actu = Actus.find({_id: module.data.actualityId})
				return  _.size(resident) - _.size(actu.unfollowers);
				break;

			case 'classifieds':
				const ads = Classifieds.find({_id: module.data.classifiedsId})
				return  _.size(resident) - _.size(ads.unfollowers);
				break;

			case 'incident':
				let incident = 0;
				_.each(resident, (elem) => {
					_.each(elem.condos, (e) => {
						if (e.condoId == Template.instance().id.get() && e.notifications.incident)
							incident++;
					});
				});
				return incident;
				break;

			default:
				return '-';
		}
	}
	return 'N/A';
};
