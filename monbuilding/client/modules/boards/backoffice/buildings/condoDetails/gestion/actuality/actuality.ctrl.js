import { Template } from "meteor/templating"
import "./actuality.view.html";

Template.condoDetailsGestionActuality.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
});

Template.condoDetailsGestionActuality.onRendered(function() {
});

Template.condoDetailsGestionActuality.events({
});

Template.condoDetailsGestionActuality.helpers({
  displayTab: () => {
    const condo = Condos.findOne({_id: Template.instance().id.get()})
    let display = "<thead><tr>";
    if (condo) {
      const module = _.find(condo.modules, (elem) => {return elem.name == 'actuality'});
			display += "<th>Date de création</th>";
      display += "<th>Titre</th>";
      display += "<th>Auteur</th>";
			display += "<th>Date de début</th>";
			display += "<th>Date de fin</th>";
			display += "<th>Localisation</th>";
      display += "<th>Description</th>";

      _.each(ActuPosts.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
        const user = Meteor.users.findOne({_id: elem.userId});
        display += "<tbody><tr>";
				display += "<td><div>" + moment(new Date(elem.createdAt)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
        display += "<td>" + elem.title + "</td>";
        if (user && user.profile)
          display += "<td><div>" + user.profile.firstname + ' ' + user.profile.lastname + "</div></td>";
        else
          display += "<td><div> - </div></td>";
				display += "<td><div>" + ((elem.startDate != '') ? elem.startDate + ' à ' + elem.startHour : '-') + "</div></td> : ";
				display += "<td><div>" + ((elem.endDate != '') ? elem.endDate + ' à ' + elem.endHour : '-') + "</div></td> : ";
        display += "<td><div>" + elem.locate + "</div></td>";
				display += "<td><div>" + elem.description + "</div></td>";
        display +="</tr></tbody>";
      });
    }
    display +=	"</tr></thead>";
    return display;
  }
});
