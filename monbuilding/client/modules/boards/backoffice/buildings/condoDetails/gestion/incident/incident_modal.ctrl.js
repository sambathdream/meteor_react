import { Template } from "meteor/templating"
import "./incident_modal.view.html";

Template.condoDetailsGestionIncidentModal.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
});

Template.condoDetailsGestionIncidentModal.onRendered(function() {
});

Template.condoDetailsGestionIncidentModal.events({
});

Template.condoDetailsGestionIncidentModal.helpers({
	displayMessages : () => {
		const condo = Condos.findOne({_id: Template.instance().id.get()})
    let display = "<thead><tr>";

    if (condo) {
      const module = _.find(condo.modules, (elem) => {return elem.name == 'incident'});
			const id = Template.instance().data.id;

			display += "<th>Date de création</th>";
			display += "<th>Auteur</th>";
			display += "<th>Pièce(s) Jointe(s)</th>";
      display += "<th>Description</th>";
			if (module) {
				const incidents = _.find(Incidents.find({incident: module.data.incidentId}).fetch(), (elem) => {return elem._id == id});
				if (incidents) {
					_.each(incidents, (elem) => {
						_.each(elem.history, (hist) => {
							display +="<tr><tbody>";
							display += "<td><div>" + moment(new Date(hist.date)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
			        if (hist.name)
			          display += "<td><div>" + hist.name + "</div></td>";
			        else
			          display += "<td><div> - </div></td>";
							display += "<td><div>";
							if (hist.share.all > 0) {
								for (let i = 0; i < elem.files.length; i++) {
									let file = UserFiles.findOne({_id: elem.files[i]});

									if (!!file && file.isImage) {
										const justifLink = file.link().replace("localhost", location.hostname);
										display += "<a id='justifFileMessage' src='" + justifLink + "'> Piece jointe " + (i + 1) + " </a> <br />";
									}
								}
							} else {
								display += "-";
							}
							display += "</td></div>"
			        display += "<td><div>" + elem.detail + "</div></td>";
			        display +="</tr></tbody>";
						});
					});
				}
			}
		}
		display +=	"</tr></thead>";
		return display;
	}
});
