import { Template } from "meteor/templating"
import "./ads.view.html";

Template.condoDetailsGestionAds.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
});

Template.condoDetailsGestionAds.onRendered(function() {
});

Template.condoDetailsGestionAds.events({
});

Template.condoDetailsGestionAds.helpers({
  displayTab: () => {
    const condo = Condos.findOne({_id: Template.instance().id.get()})
    let display = "<thead><tr>";
    if (condo) {
      const module = _.find(condo.modules, (elem) => {return elem.name == 'classifieds'});

			display += "<th>Date de création</th>";
      display += "<th>Titre</th>";
      display += "<th>Auteur</th>";
      display += "<th>Description</th>";
      display += "<th>Prix</th>";
      _.each(ClassifiedsAds.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
        const user = Meteor.users.findOne({_id: elem.userId});
        display += "<tbody><tr>";
				display += "<td><div>" + moment(new Date(elem.createdAt)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
        display += "<td>" + elem.title + "</td>";
        if (user && user.profile)
          display += "<td><div>" + user.profile.firstname + ' ' + user.profile.lastname + "</div></td>";
        else
          display += "<td><div> - </div></td>";
        display += "<td><div>" + elem.description + "</div></td>";
        display += "<td><div>" + elem.price + "</div></td>";
        display +="</tr></tbody>";
      });
    }
    display +=	"</tr></thead>";
    return display;
  }
});
