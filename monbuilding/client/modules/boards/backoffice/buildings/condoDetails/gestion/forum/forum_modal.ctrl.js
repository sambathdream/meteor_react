import { Template } from "meteor/templating"
import "./forum_modal.view.html";

Template.condoDetailsGestionForumModal.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
	this.autorun(() => {
		if (!!Session.get('isPostSelected')) {
			let postId = Session.get('isPostSelected')
			console.log('postId', postId);
			if (!!postId) {
				this.subscribe('forumCommentPosts', this.id.get(), postId, () => {
					console.log('ForumCommentPosts.find().fetch()', ForumCommentPosts.find().fetch());
				});
			}
		}
	})
});

Template.condoDetailsGestionForumModal.onRendered(function() {
});

Template.condoDetailsGestionForumModal.events({
	'click #justifFileMessage': function(event, template) {
		event.stopPropagation();
		event.preventDefault();
		let _event = event;
		$(_event.currentTarget).css("max-width", "100%");
		bootbox.alert({
			size: "large",
			title: "piece jointe",
			message: "<img style='width: 100%' src='" + _event.currentTarget.getAttribute("src") + "'>",
			backdrop: true
		});
	},
});

Template.condoDetailsGestionForumModal.helpers({
	getMode: () => {
		return Template.instance().data.mode;
	},
	displayMessages : () => {
		const condoId = Template.instance().id.get()
		const postId = Template.instance().data.id

		let display = "<thead><tr>";

		if (!!condoId && !!postId && !!Template.instance().subscriptionsReady()) {
			let comments = Template.instance().data.mode == 'cs' ? null : ForumCommentPosts.find({}).fetch()

			display += "<th>Date de création</th>";
			display += "<th>Auteur</th>";
			display += "<th>Pièce(s) Jointe(s)</th>";
			display += "<th>Description</th>";
			if (comments) {
				_.each(comments, (elem) => {
					const user = Meteor.users.findOne({_id: elem.userId});

					display +="<tr><tbody>";
					display += "<td><div>" + moment(new Date(elem.commentDate)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
					if (user && user.profile)
						display += "<td><div>" + user.profile.firstname + ' ' + user.profile.lastname + "</div></td>";
					else
						display += "<td><div> - </div></td>";
					display += "<td><div>";
					if (!!elem.filesId && elem.filesId.length > 0) {
						for (let i = 0; i < elem.filesId.length; i++) {
							let file = ForumFiles.findOne({_id: elem.filesId[i]});

							if (!!file && file.isImage) {
								const justifLink = file.link().replace("localhost", location.hostname);
								display += "<a id='justifFileMessage' src='" + justifLink + "'> Piece jointe " + (i + 1) + " </a> <br />";
							}
						}
					} else {
						display += "-";
					}
					display += "</td></div>"
					display += "<td><div>" + elem.comment + "</div></td>";
					display +="</tr></tbody>";
				});
			}
		}
		display +=	"</tr></thead>";
		return display;
	},
	isPostSelected: () => {
		let postId = Template.instance().data.id
		Session.set('isPostSelected', !!postId && postId)
	}
});
