import { Template } from "meteor/templating"
import "./incident.view.html";

Template.condoDetailsGestionIncident.onCreated(function() {
	this.id = new ReactiveVar(FlowRouter.current().params.id);
	this.selectedIncident = new ReactiveVar();
});

Template.condoDetailsGestionIncident.onRendered(function() {
});

Template.condoDetailsGestionIncident.events({
	'click [id=tab]': function(event, template) {
		template.selectedIncident.set($(event.currentTarget).attr('value'));
	},
});

Template.condoDetailsGestionIncident.helpers({
	selectedIncident: (id) => {
		return Template.instance().selectedIncident.get() == id;
	},
  displayTab: () => {
    const condo = Condos.findOne({_id: Template.instance().id.get()})
    let display = "<thead><tr>";
    if (condo) {
      const module = _.find(condo.modules, (elem) => {return elem.name == 'incident'});

			display += "<th>Date de création</th>";
      display += "<th>Titre</th>";
      display += "<th>Auteur</th>";
			display += "<th>Priorité</th>";
			display += "<th>Type</th>";
			display += "<th>Zone</th>";
			display += "<th>Description</th>";
      _.each(Incidents.find({condoId: Template.instance().id.get()}).fetch(), (elem) => {
        display += "<tbody><tr id='tab' data-toggle='modal' data-target='#modal_historic' value='" + elem._id + "'>";
				display += "<td><div>" + moment(new Date(elem.createdAt)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
        display += "<td>" + elem.info.title + "</td>";
				if (elem.declarer) {
					let user = Meteor.users.findOne(elem.declarer.userId);
					if (!user || user == undefined)
						user.profile = {firstname: "User", lastname: "Not Found"};
					display += "<td><div>" + user.profile.firstname + ' ' + user.profile.lastname + "</div></td>";
				} else {
					display += "<td><div>" + '-' + "</div></td>";
				}
				if (elem.info) {
					display += "<td><div>" + elem.info.priority + "</div></td>";
					display += "<td><div>" + elem.info.type + "</div></td>";
					display += "<td><div>" + elem.info.zone + "</div></td>";
	        display += "<td><div>" + elem.info.comment + "</div></td>";
				} else {
					display += "<td><div>-</div></td>";
					display += "<td><div>-</div></td>";
					display += "<td><div>-</div></td>";
					display += "<td><div>-</div></td>";
				}
        display +="</tr></tbody>";
      });
    }
    display +=	"</tr></thead>";
    return display;
  }
});
