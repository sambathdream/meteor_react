import { Template } from "meteor/templating"
import "./forum.view.html";

Template.condoDetailsGestionForum.onCreated(function() {
	Meteor.subscribe('UserFiles');
	this.id = new ReactiveVar(FlowRouter.current().params.id);
	this.selectedPost = new ReactiveVar();
});

Template.condoDetailsGestionForum.onRendered(function() {
});

Template.condoDetailsGestionForum.events({
	'click [id=tab]': function(event, template) {
		template.selectedPost.set($(event.currentTarget).attr('value'));
	},
	'click #forumFileDownload': function (event, template) {
		event.stopPropagation();
		event.preventDefault();
		let _event = event;
		$(_event.currentTarget).css("max-width", "100%");
		bootbox.alert({
			size: "large",
			title: "piece jointe",
			message: "<img style='width: 100%' src='" + _event.currentTarget.getAttribute("src") + "'>",
			backdrop: true
		});
	},
});

Template.condoDetailsGestionForum.helpers({
	selectedPost: (id) => {
		return Template.instance().selectedPost.get() == id;
	},
	getSelectedId: () => {
		return Template.instance().selectedPost.get();
	},
  displayTab: () => {
		const condoId = Template.instance().id.get()
    let display = "<thead><tr>";

		if (condoId && !!Template.instance().subscriptionsReady()) {
			let posts = Template.instance().data.mode == 'cs' ? null : ForumPosts.find({}, {sort: {updatedAt: -1}}).fetch()

			display += "<th>Date de création</th>";
			display += "<th>Contenu</th>";
      display += "<th>Auteur</th>";
			display += "<th>Pièce(s) Jointe(s)</th>";
			display += "</tr></thead>";
      _.each(posts, (elem) => {
        const user = Meteor.users.findOne({_id: elem.userId});
        display += "<tbody><tr id='tab' data-toggle='modal' data-target='#modal_list_message' value='" + elem._id + "'>";
				display += "<td><div>" + moment(new Date(elem.date)).format("DD/MM/YYYY[" + ' à ' + "]HH:mm") + "</div></td>";
        display += "<td><div>" + ((elem.subject.length > 161) ? (elem.subject.substr(0, 160) + '[...]') : elem.subject) + "</div></td>";
        if (user && user.profile)
          display += "<td><div>" + user.profile.firstname + ' ' + user.profile.lastname + "</div></td>";
        else
          display += "<td><div> - </div></td>";
				display += "<td><div>";
				if (!!elem.filesId && elem.filesId.length > 0) {
					for (let i = 0; i < elem.filesId.length; i++) {
						let file = ForumFiles.findOne({_id: elem.filesId[i]});

						if (!!file && file.isImage) {
							const forumFileLink = file.link().replace("localhost", location.hostname);

							display += "<a id='forumFileDownload' src='" + forumFileLink + "'> Piece jointe " + (i + 1) + " </a> <br />";
						}
					}
				} else {
					display += "-";
				}
				display += "</td></div>"
        display +="</tr></tbody>";
      });
    }
    return display;
  },
});
