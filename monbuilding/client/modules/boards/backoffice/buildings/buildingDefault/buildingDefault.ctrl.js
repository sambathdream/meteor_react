import { Template } from 'meteor/templating'
import * as _ from 'underscore'

const tabDefaultOption = {
  'Information': { mono: true, copro: true, office: true, student: true },
  'Manuel': { mono: true, copro: true, office: true, student: true },
  'Incidents': { mono: false, copro: false, office: false, student: false },
  'Trombinoscope (Tous les occupants)': { mono: true, copro: true, office: true, student: true },
  'Trombinoscope (CS uniquement)': { mono: false, copro: false, office: false, student: false },
  'Trombinoscope (Gardien uniquement)': { mono: false, copro: false, office: false, student: false },
  'Plan': { mono: true, copro: true, office: true, student: true },
  'Forum': { mono: true, copro: true, office: true, student: true },
  'Forum conseil syndical': { mono: false, copro: true, office: false, student: false },
  'Petites annonces': { mono: true, copro: true, office: true, student: true },
  'Conciergerie': { mono: true, copro: true, office: true, student: true },
  'Réservation': { mono: false, copro: false, office: false, student: false },
  'Couronne': { mono: false, copro: true, office: false, student: false },
  'Messagerie (occupant)': { mono: true, copro: true, office: true, student: true },
  'Messagerie (conseil syndical)': { mono: false, copro: true, office: false, student: false },
  'Messagerie (gardien)': { mono: true, copro: true, office: true, student: true },
  'Messagerie (gestionnaire)': { mono: false, copro: false, office: false, student: false },
  'Messagerie (dropdown incident copro)': { mono: false, copro: true, office: false, student: false },
  'Messagerie (dropdown incident mono)': { mono: true, copro: false, office: true, student: true },
  'Profil (defaut)': { mono: true, copro: true, office: true, student: true },
  'Profil (etudiant)': { mono: false, copro: false, office: false, student: true },
  'Profil (entreprise)': { mono: false, copro: false, office: true, student: false }
}

Template.admin_buildingDefault.onCreated(function () {

})

Template.admin_buildingDefault.onDestroyed(function () {

})

Template.admin_buildingDefault.onRendered(() => {

})

Template.admin_buildingDefault.events({
})

Template.admin_buildingDefault.helpers({
  modules: () => {
    let ret = []
    _.each(tabDefaultOption, function (elem, index) {
      ret.push(index)
    })
    return ret
  },
  getName: (elem) => {
    return elem
  },
  getMonoOption: (elem) => {
    return tabDefaultOption[elem].mono
  },
  getCoproOption: (elem) => {
    return tabDefaultOption[elem].copro
  },
  getOfficeOption: (elem) => {
    return tabDefaultOption[elem].office
  },
  getStudentOption: (elem) => {
    return tabDefaultOption[elem].student
  }
})
