Template.roles.onCreated(function(){
});

Template.roles.onRendered(function(){
});

Template.roles.events({
	'click .labelForCheckbox': function(event, template) {
		let currentDefaultRole = Session.get("currentDefaultRole");
		let edition = $(event.currentTarget).attr("for").split('.');
		edition[2] = edition[2] == "true" ? true : edition[2] == "false" ? false : null;

		Meteor.call('updateDefaultRole', currentDefaultRole._id, edition[0], edition[1], edition[2]);
	},
	'click .deleteRight': function (event, template) {
		let currentDefaultRole = Session.get("currentDefaultRole");
		let right = $(event.currentTarget).data("right").split('.');
		bootbox.confirm({
			size: "medium",
			title: "Confirmation",
			message: "Êtes-vous sûr de vouloir supprimer le droit " + right[1] + " du module " + right[0] + " ?",
			buttons : {
				'cancel' : {label: "Annuler", className: "btn-outline-red-confirm"},
				'confirm' : {label: "Confirmer", className: "btn-red-confirm"}
			},
			backdrop: true,
			callback: function(result) {
				if (result) {
					Meteor.call('deleteDefaultRole', currentDefaultRole._id, right[0], right[1]);
				}
			}
		});
  },
  'click .createNewRoles': (e, t) => {
    FlowRouter.go('app.backoffice.newRoles')
  }
});

Template.roles.helpers({
  getRoleTypo: function (typo) {
    return this.currentDefaultRole && this.currentDefaultRole.forCondoType[typo]
  },
  onCheckboxClick: function (key) {
    const t = Template.instance()
    const currentDefaultRole = this.currentDefaultRole
    return () => () => {
      Meteor.call('changeRoleCondoTypeOption', currentDefaultRole._id, key, !currentDefaultRole.forCondoType[key], (error, result) => {
        if (error) console.log(error)
      })
    }
  },
	isRoleSelected: () => {
		return !_.isEmpty(Session.get("currentDefaultRole"));
	},
	getCurrentDefaultRole: () => {
		let currentDefaultRole = DefaultRoles.findOne(Session.get("currentDefaultRole")._id);
		if (!currentDefaultRole)
			return undefined;
		currentDefaultRole.rights = _.sortBy(currentDefaultRole.rights, function(right){
			return right.displayOrder;
		});
		return currentDefaultRole;
	},
	iscurrentDisplayingModule: (moduleName) => {
    if (Template.instance().currentDisplayingModule == moduleName && moduleName !== 'view')
			return true;
		Template.instance().currentDisplayingModule = moduleName;
		return false;
	},
  viewName: (moduleName, displayOrder) => {
    return moduleName + displayOrder
  }
});
