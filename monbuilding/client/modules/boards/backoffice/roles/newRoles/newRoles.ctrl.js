function getMergedRights(type) {
  let currentDefaultRole = DefaultRoles.findOne({ name: type })
  if (!currentDefaultRole)
    return undefined

  currentDefaultRole.rights = _.sortBy(currentDefaultRole.rights, function (right) {
    return right.displayOrder
  })
  currentDefaultRole.name = ''
  delete currentDefaultRole._id
  return currentDefaultRole
}

function updateSubmit (t) {
  const rights = t.defaultRight.get()
  const name = rights.name
  const typos = rights.forCondoType

  t.canSubmit.set(!!name && !!typos && (typos.copro || typos.mono || typos.office || typos.student))
}
Template.admin_newRoles.onCreated(function () {
  this.userId = Meteor.userId()
  this.currentDisplayingModule = null
  this.subscribe('getDefaultRoles')
  this.canSubmit = new ReactiveVar(false)

  this.typeSelected = new ReactiveVar('Occupant')

  // this.roleName = ''

  this.defaultRight = new ReactiveVar(null)
})

Template.admin_newRoles.onDestroyed(function () {
})

Template.admin_newRoles.onRendered(() => {
})

Template.admin_newRoles.events({
  'click .labelForCheckbox': function (event, template) {
    const userId = template.userId
    const roleId = template.roleId

    const edition = $(event.currentTarget).attr('for').split('.')
    edition[2] = edition[2] == "true" ? true : (edition[2] == "false" || edition[2] === '') ? false : null

    const rightName = $(event.currentTarget).data('display')
    let index = $(event.currentTarget).data('index')

    let defaultRight = template.defaultRight.get()

    defaultRight.rights[index].default = !edition[2]
    template.defaultRight.set(defaultRight)
  },
})

Template.admin_newRoles.helpers({
  isSubscribeReady: () => {
    if (Template.instance().subscriptionsReady()) {
      let currentDefaultRole = getMergedRights('Occupant')
      Template.instance().defaultRight.set(currentDefaultRole)
    }
    return Template.instance().subscriptionsReady()
  },
  getRoleTypo: function (typo) {
    return this.UserRights && this.UserRights.forCondoType[typo]
  },
  onRoleTypoClick: (key) => {
    const t = Template.instance()
    return () => () => {
      const tempDefault = t.defaultRight.get()
      tempDefault.forCondoType[key] = !tempDefault.forCondoType[key]
      t.defaultRight.set(tempDefault)
      updateSubmit(t)
    }
  },
  onNameChange: () => {
    const t = Template.instance()
    return (value) => {
      const tempDefault = t.defaultRight.get()
      tempDefault.name = value
      t.defaultRight.set(tempDefault)
      updateSubmit(t)
    }
  },
  getTypeSelected: (key) => {
    const t = Template.instance()
    return () => t.typeSelected.get() === key
  },
  onCheckboxClick: (key) => {
    const t = Template.instance()
    return () => () => {
      let currentDefaultRole = getMergedRights(key)
      t.defaultRight.set(currentDefaultRole)

      t.typeSelected.set(key)
    }
  },
  iscurrentDisplayingModule: (moduleName) => {
    if (Template.instance().currentDisplayingModule == moduleName && moduleName !== 'view')
      return true
    Template.instance().currentDisplayingModule = moduleName
    return false
  },
  getUserRights: () => {
    return Template.instance().defaultRight.get()
  },
  viewName: (moduleName, displayOrder) => {
    return moduleName + displayOrder
  },
  saveRights: () => {
    let t = Template.instance()

    return () => {
      if (t.canSubmit.get()) {
        t.canSubmit.set(false)
        const rights = t.defaultRight.get()

        Meteor.call('createNewDefaultRole', rights, (error, result) => {
          if (error) {
            sAlert.error(error)
          } else {
            FlowRouter.go('app.backoffice.roles')
          }
        })
      }
    }
  },
  getCondoId: () => {
    return Template.instance().condoId
  },
  getModalTitle: () => {
    return Template.instance().modalTitle
  },
  getButtonClasses: () => {
    if (Template.instance().canSubmit.get()) {
      return 'btn btn-red-confirm'
    } else {
      return 'btn btn-red-confirm disabled'
    }
  }
})
