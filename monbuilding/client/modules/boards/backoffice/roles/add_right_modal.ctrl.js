Template.add_right_modal.onCreated(function(){
});

Template.add_right_modal.onRendered(function(){
});

Template.add_right_modal.events({
	'click #addDefaultRight': function(event, template) {
		let form = {
			defaultRightNameCanonical : $('[name="defaultRightNameCanonical"]').val(),
			defaultRightNameDisplay : $('[name="defaultRightNameDisplay"]').val(),
			defaultRightModule : Session.get("dropdownSelectedModule"),
		}
		Meteor.call('addRightToDefaultRole', Session.get("currentDefaultRole")._id, form.defaultRightModule, form.defaultRightNameCanonical, form.defaultRightNameDisplay, function (err, res){
			if (!err) {
				$('#add_right_modal').modal("hide");
				$('[name="defaultRightNameCanonical"]').val('');
				$('[name="defaultRightNameDisplay"]').val('');
				sAlert.success("Le nouveau droit à bien été ajouté");
			} else {
				sAlert.error(err);
			}
		});
	},
});

Template.add_right_modal.helpers({
	getModulesList: () => {
		if (!_.isEmpty(Session.get("currentDefaultRole"))) {
			let currentDefaultRole = DefaultRoles.findOne(Session.get("currentDefaultRole")._id);
			let result = _.map(currentDefaultRole.rights, function(right){
				return {
					id: right.module,
					name: right.module
				}
			});
			return uniqModule(result);
		}
	},
});

function uniqModule(array) {
	let resArr = [];
	array.filter(function(item){
		let i = resArr.findIndex(x => x.name == item.name);
		if(i <= -1){
			resArr.push({id: item.id, name: item.name});
		}
		return null;
	});
	_.each(resArr, function(elem, key){
		resArr[key].name = resArr[key].name.charAt(0).toUpperCase() + resArr[key].name.slice(1);
	});
	return resArr;
}