function set_options_of_chart(option) {
	 
	let options = option;
	
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-percentage_download').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-percentage_download').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-percentage_download').css("opacity", 1);
		}, 5);
	}
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-percentage_download');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';

		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let totalUser = 0;
	let totalDownload = 0

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let playStoreDownloadLength = oldUser.playStoreNumberDownload;
	let appleStoreDownloadLength = oldUser.appleStoreNumberDownload;

	let data = [];

	duration = end.diff(start, 'years');
	let activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{ "condos.joined": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "condos.joined": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('YYYY') }
	});

	let withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('YYYY') };
	});

	let pendingUser = _.map(Residents.find({
		$and: [
			{ "pendings.0": { $exists: true } },
			{ "pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "pendings.invited": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'pendings.condoId': { $in: condoIds } },
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }, ,
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('YYYY') };
	});

	let downloadOnPlay = _.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('YYYY'), y: item.count };
	});

	let downloadOnApple = _.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('YYYY'), y: item.count };
	});

	let year = start;
	for (var i = 0; i <= duration; i++) {

		label.push(year.format("YYYY"));

		activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0;
		pendingUserLength += _.filter(pendingUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0;
		withoutPasswordUserLength += _.filter(withoutPasswordUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0;

		if (_.find(downloadOnPlay, function (res) { return res.x == year.format('YYYY') })) {
			playStoreDownloadLength = _.find(downloadOnPlay, function (res) { return res.x == year.format('YYYY') }).y;
		} else {
			playStoreDownloadLength = playStoreDownloadLength;
		}
		if (_.find(downloadOnApple, function (res) { return res.x == year.format('YYYY') })) {
			appleStoreDownloadLength = _.find(downloadOnApple, function (res) { return res.x == year.format('YYYY') }).y;
		} else {
			appleStoreDownloadLength = appleStoreDownloadLength;
		}

		totalUser = activeUserLength + pendingUserLength + withoutPasswordUserLength;

		totalDownload = playStoreDownloadLength + appleStoreDownloadLength;

		data.push(totalUser == 0 ? totalDownload : parseInt(((totalDownload * 100) / totalUser)));

		year.add(1, "y");
	}

	return {
		labels: label,
		percentageOfDownload: data,
	};
}
function get_data_of_month(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let totalUser = 0;
	let totalDownload = 0

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let playStoreDownloadLength = oldUser.playStoreNumberDownload;
	let appleStoreDownloadLength = oldUser.appleStoreNumberDownload;

	let data = [];

	duration = end.diff(start, 'months');
	activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{ "condos.joined": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "condos.joined": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('MM[/]YYYY') }
	});


	withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('MM[/]YYYY') };
	});

	pendingUser = _.map(Residents.find({
		$and: [
			{ "pendings.0": { $exists: true } },
			{ "pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "pendings.invited": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'pendings.condoId': { $in: condoIds } },
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }, ,
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('MM[/]YYYY') };
	});

	let downloadOnPlay = _.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('MM[/]YYYY'), y: item.count };
	});

	let downloadOnApple = _.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('MM[/]YYYY'), y: item.count };
	});

	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));

		if (month.diff(moment().endOf("months"), 'days') < -32) {

			activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0;
			pendingUserLength += _.filter(pendingUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0;
			withoutPasswordUserLength += _.filter(withoutPasswordUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0;

			if (_.find(downloadOnPlay, function (res) { return res.x == month.format('MM[/]YYYY') })) {
				playStoreDownloadLength = _.find(downloadOnPlay, function (res) { return res.x == month.format('MM[/]YYYY') }).y;
			} else {
				playStoreDownloadLength = playStoreDownloadLength;
			}
			if (_.find(downloadOnApple, function (res) { return res.x == month.format('MM[/]YYYY') })) {
				appleStoreDownloadLength = _.find(downloadOnApple, function (res) { return res.x == month.format('MM[/]YYYY') }).y;
			} else {
				appleStoreDownloadLength = appleStoreDownloadLength;
			}

			totalUser = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			totalDownload = playStoreDownloadLength + appleStoreDownloadLength;

			data.push(totalUser == 0 ? totalDownload : parseInt(((totalDownload * 100) / totalUser)));
		} else {
			data.push(0);			
		}

		month.add(1, "M");
	}

	return {
		labels: label,
		percentageOfDownload: data,
	};
}
function get_data_of_day(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let totalUser = 0;
	let totalDownload = 0

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let playStoreDownloadLength = oldUser.playStoreNumberDownload;
	let appleStoreDownloadLength = oldUser.appleStoreNumberDownload;

	let data = [];

	duration = end.diff(start, 'days')
	activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{ "condos.joined": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "condos.joined": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('DD[/]MM[/]YY') }
	});


	withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('DD[/]MM[/]YY') };
	});

	pendingUser = _.map(Residents.find({
		$and: [
			{ "pendings.0": { $exists: true } },
			{ "pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "pendings.invited": { $lte: new Date(parseInt(end.format('x'))) } },
			{ 'pendings.condoId': { $in: condoIds } },
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }, ,
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('DD[/]MM[/]YY') };
	});

	let downloadOnPlay = _.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return {x: moment(item.date).format('DD[/]MM[/]YY'), y: item.count};
	});

	let downloadOnApple = _.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('DD[/]MM[/]YY'), y: item.count };
	});

	let day = start;
	for (var i = 0; i < duration; i++) {

		label.push(day.format("ddd DD MMM"));

		if (day.diff(moment().endOf("days"), 'hours') < 0) {

			activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == day.format('DD[/]MM[/]YY') }).length || 0;
			pendingUserLength += _.filter(pendingUser, function (aUser) { return aUser.date == day.format('DD[/]MM[/]YY') }).length || 0;
			withoutPasswordUserLength += _.filter(withoutPasswordUser, function (aUser) { return aUser.date == day.format('DD[/]MM[/]YY') }).length || 0;

			if (_.find(downloadOnPlay, function (res) { return res.x == day.format('DD[/]MM[/]YY') })) {
				playStoreDownloadLength = _.find(downloadOnPlay, function (res) { return res.x == day.format('DD[/]MM[/]YY') }).y;
			} else {
				playStoreDownloadLength = playStoreDownloadLength;
			}
			if (_.find(downloadOnApple, function (res) { return res.x == day.format('DD[/]MM[/]YY') })) {
				appleStoreDownloadLength = _.find(downloadOnApple, function (res) { return res.x == day.format('DD[/]MM[/]YY') }).y;
			} else {
				appleStoreDownloadLength = appleStoreDownloadLength;
			}

			totalUser = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			totalDownload = playStoreDownloadLength + appleStoreDownloadLength;

			data.push(totalUser == 0 ? totalDownload : parseInt(((totalDownload * 100) / totalUser)));

		} else {
			data.push(0);
		}


		day.add(1, "d");
	}

	return {
		labels: label,
		percentageOfDownload: data,
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	let activeUser = _.map(Residents.find({
		"condos.0": { $exists: true },
		"condos.joined": { $lte: new Date(parseInt(start.format('x'))) },
		'condos.condoId': { $in: condoIds }
	}).fetch(), function (resident) {
		return resident._id
	});

	let withoutPasswordUser = _.map(Meteor.users.find({
		"createdAt": { $lte: new Date(parseInt(start.format('x'))) },
		"services.password": { $exists: false },
		"_id": { $nin: activeUser }
	}).fetch(), function (user) {
		return user._id
	});

	let pendingUser = _.map(Residents.find({
		"pendings.0": { $exists: true },
		"pendings.invited": { $lte: new Date(parseInt(start.format('x'))) },
		'pendings.condoId': { $in: condoIds },
		"_id": { $nin: activeUser },
		"userId": { $nin: withoutPasswordUser },
	}).fetch(), function (resident) {
		return resident._id;
	});

	let _numberDownloadPlay = _.flatten([_.map(Analytics.find({
		"type": "download",
		"date": { $lte: parseInt(start.format('x')) },
		"store": "PlayStore",
	}, { sort: { date: -1 } }).fetch(), function (item) {
		return item.count;
	})]);

	let _numberDownloadApple = _.flatten([_.map(Analytics.find({
		"type": "download",
		"date": { $lte: parseInt(start.format('x')) },
		"store": "AppStore",
	}, { sort: { date: -1 } }).fetch(), function (item) {
		return item.count;
	})]);

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds, { activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser, playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds, { activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser, playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}
	else {
		data = get_data_of_day(start, end, condoIds, { activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser, playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}

	dataset.push({
		label: '% téléchargements vs. active users',
		data: data.percentageOfDownload,
		backgroundColor: "#a3b4bc",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		},
	});

	dataset.push({
		label: '',
		data: data.percentageOfDownload,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end',
			formatter: function (value, context) {
				return value + '%';
			}
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_percentage_download(start, end, option) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)
	$('#bar_percentage_download').remove();
	$('#bar_percentage_download-container').append('<canvas id="bar_percentage_download"><canvas>');
	let ctx = $('#bar_percentage_download');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-percentage_download-legend").html(myChart.generateLegend());
}



Template.bar_percentage_download.onCreated(function () {
});

let myInterval;

Template.bar_percentage_download.onRendered(function () {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function () {

		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || (!render && condoIds != Session.get('condoIds').length) || (!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_percentage_download(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
	$(window).resize(function () {
		start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
		end = moment(Session.get('endDate'), "DD[/]MM[/]YY");
		render_bar_percentage_download(start.clone(), end.clone(), template.data.option);
	});
});

Template.bar_percentage_download.onDestroyed(function () {
	clearInterval(myInterval);
})

Template.bar_percentage_download.events({
});

Template.bar_percentage_download.helpers({
});