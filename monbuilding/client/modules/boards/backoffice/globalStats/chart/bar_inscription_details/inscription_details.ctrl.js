function set_options_of_chart(option) {
	 
	let options = option;
	
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-inscription_details').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-inscription_details').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-inscription_details').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;

	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-inscription_details');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds) {
	let label = [];
	let data_total = [];
	let data_without_invit = [];
	let data_with_invit_back = [];
	let data_with_invit_gest = [];
	
	let bar_inscription_with_invit_back = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_with_invit_gest = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByGestionnaire": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByGestionnaire": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_without_invit = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": {$ne: true}}, 
					{"condos.invitByGestionnaire": {$ne: true}}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": {$ne: true}}, 
					{"pendings.invitByGestionnaire": {$ne: true}},
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_total = _.flatten([_.map(Residents.find({
		$or: [
			{"condos.condoId": {$in: condoIds}},
			{"pendings.condoId": {$in: condoIds}},
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	bar_inscription_total = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_total}}).fetch(), function(user) {return moment(user.createdAt).format('YYYY');})))
	bar_inscription_without_invit = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_without_invit}}).fetch(), function(user) {return moment(user.createdAt).format('YYYY');})))
	bar_inscription_with_invit_back = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_back}}).fetch(), function(user) {return moment(user.createdAt).format('YYYY');})))
	bar_inscription_with_invit_gest = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_gest}}).fetch(), function(user) {return moment(user.createdAt).format('YYYY');})))
	
	let duration = end.diff(start, 'years');
	let year = start;
	for (var i = 0; i <= duration; i++) {
		data_total.push(bar_inscription_total[year.format('YYYY')] || 0);
		data_without_invit.push(bar_inscription_without_invit[year.format('YYYY')] || 0);
		data_with_invit_back.push(bar_inscription_with_invit_back[year.format('YYYY')] || 0);
		data_with_invit_gest.push(bar_inscription_with_invit_gest[year.format('YYYY')] || 0);
		label.push(year.format('YYYY'))
		year.add(1, "y");
	}
	
	return {
		labels: label,
		data_total: data_total,
		data_without_invit: data_without_invit,
		data_with_invit_back: data_with_invit_back,
		data_with_invit_gest: data_with_invit_gest,
	};
}
function get_data_of_month(start, end, condoIds) {
	let label = [];
	let data_total = [];
	let data_without_invit = [];
	let data_with_invit_back = [];
	let data_with_invit_gest = [];
	
	let bar_inscription_with_invit_back = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_with_invit_gest = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByGestionnaire": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByGestionnaire": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_without_invit = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": {$ne: true}}, 
					{"condos.invitByGestionnaire": {$ne: true}}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": {$ne: true}}, 
					{"pendings.invitByGestionnaire": {$ne: true}},
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_total = _.flatten([_.map(Residents.find({
		$or: [
			{"condos.condoId": {$in: condoIds}},
			{"pendings.condoId": {$in: condoIds}},
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	bar_inscription_total = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_total}}).fetch(), function(user) {return moment(user.createdAt).format('MM[/]YYYY');})))
	bar_inscription_without_invit = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_without_invit}}).fetch(), function(user) {return moment(user.createdAt).format('MM[/]YYYY');})))
	bar_inscription_with_invit_back = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_back}}).fetch(), function(user) {return moment(user.createdAt).format('MM[/]YYYY');})))
	bar_inscription_with_invit_gest = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_gest}}).fetch(), function(user) {return moment(user.createdAt).format('MM[/]YYYY');})))
	
	let duration = end.diff(start, 'months');
	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		data_total.push(bar_inscription_total[month.format('MM[/]YYYY')] || 0);
		data_without_invit.push(bar_inscription_without_invit[month.format('MM[/]YYYY')] || 0);
		data_with_invit_back.push(bar_inscription_with_invit_back[month.format('MM[/]YYYY')] || 0);
		data_with_invit_gest.push(bar_inscription_with_invit_gest[month.format('MM[/]YYYY')] || 0);
		label.push(month.format('MMMM'))
		month.add(1, "M");
	}
	
	return {
		labels: label,
		data_total: data_total,
		data_without_invit: data_without_invit,
		data_with_invit_back: data_with_invit_back,
		data_with_invit_gest: data_with_invit_gest,
	};
}
function get_data_of_day(start, end, condoIds) {let label = [];
	let data_total = [];
	let data_without_invit = [];
	let data_with_invit_back = [];
	let data_with_invit_gest = [];
	
	let bar_inscription_with_invit_back = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_with_invit_gest = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByGestionnaire": true,}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByGestionnaire": true,}, 
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_without_invit = _.flatten([_.map(Residents.find({
		$or: [
			{
				$and: [
					{"condos.invitByAdmin": {$ne: true}}, 
					{"condos.invitByGestionnaire": {$ne: true}}, 
					{"condos.condoId": {$in: condoIds}},
				]
			},
			{
				$and: [
					{"pendings.invitByAdmin": {$ne: true}}, 
					{"pendings.invitByGestionnaire": {$ne: true}},
					{"pendings.condoId": {$in: condoIds}},
				]
			}
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	let bar_inscription_total = _.flatten([_.map(Residents.find({
		$or: [
			{"condos.condoId": {$in: condoIds}},
			{"pendings.condoId": {$in: condoIds}},
		]
	}).fetch(), function(user) {
		return user.userId;
	})]);
	
	bar_inscription_total = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_total}}).fetch(), function(user) {return moment(user.createdAt).format('DD[/]MM[/]YY');})))
	bar_inscription_without_invit = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_without_invit}}).fetch(), function(user) {return moment(user.createdAt).format('DD[/]MM[/]YY');})))
	bar_inscription_with_invit_back = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_back}}).fetch(), function(user) {return moment(user.createdAt).format('DD[/]MM[/]YY');})))
	bar_inscription_with_invit_gest = _.countBy(_.flatten(_.map(Meteor.users.find({"_id": {$in: bar_inscription_with_invit_gest}}).fetch(), function(user) {return moment(user.createdAt).format('DD[/]MM[/]YY');})))
	
	let duration = end.diff(start, 'days');
	let day = start;
	for (var i = 0; i < duration; i++) {
		data_total.push(bar_inscription_total[day.format('DD[/]MM[/]YY')] || 0);
		data_without_invit.push(bar_inscription_without_invit[day.format('DD[/]MM[/]YY')] || 0);
		data_with_invit_back.push(bar_inscription_with_invit_back[day.format('DD[/]MM[/]YY')] || 0);
		data_with_invit_gest.push(bar_inscription_with_invit_gest[day.format('DD[/]MM[/]YY')] || 0);
		label.push(day.format("ddd DD MMM"))
		day.add(1, "d");
	}
	
	return {
		labels: label,
		data_total: data_total,
		data_without_invit: data_without_invit,
		data_with_invit_back: data_with_invit_back,
		data_with_invit_gest: data_with_invit_gest,
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];
	
	let bar_duration_connexion = [];
	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}
	
	dataset.push({
		label: '# invitation backoffice',
		data:  data.data_with_invit_back,
		fill: true,
		backgroundColor: "#69D2E7",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});
	dataset.push({
		label: '# invitation gestionnaire',
		data:  data.data_with_invit_gest,
		fill: true,
		backgroundColor: "#6a6a6c",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});
	dataset.push({
		label: '# sans invitation',
		data:  data.data_without_invit,
		fill: true,
		backgroundColor: "#fdb812",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});
	dataset.push({
		label: '',
		data: data.data_total,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_inscription_details(start, end, option) {
	
	let options = set_options_of_chart(option)
	
	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condo) {return condo._id});
	}
	
	let data = build_datasets_of_chart(start, end, condoIds)
	
	$('#bar_inscription_details').remove();
	$('#bar_inscription_details-container').append('<canvas id="bar_inscription_details"><canvas>');
	let ctx = $("#bar_inscription_details");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-inscription_details-legend").html(myChart.generateLegend());
}

Template.bar_inscription_details.onCreated(function() {
});

let myInterval;

Template.bar_inscription_details.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
		(!render && condoIds != Session.get('condoIds').length) ||
		(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_inscription_details(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_inscription_details.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_inscription_details.events({
});

Template.bar_inscription_details.helpers({
});