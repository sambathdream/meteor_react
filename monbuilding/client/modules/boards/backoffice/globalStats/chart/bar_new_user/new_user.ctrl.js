function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-new_user').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-new_user').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-new_user').css("opacity", 1);
		}, 5);
	}
	options.scales.yAxes = [{
		id: 'A',
		type: 'linear',
		position: 'right',
		display: false,
		ticks: {
			padding: 10,
			beginAtZero: true
		},
		stacked: true,
	}, {
		id: 'B',
		type: 'linear',
		position: 'left',
		gridLines: {
			display: false,
			color: "#88898e",
			lineWidth: 2,
			zeroLineWidth: 2,
			zeroLineColor: "#88898e",
			drawTicks: true,
			tickMarkLength: 3
		},
		ticks: {
			callback: function (value) {
				if (value < 0)
					return '(' + value * -1 + ')' + '%';
				else
					return value + '%';
			},
			padding: 10,
			stepSize: 50,
			min: -5,
			max: 5,
		},
		stacked: true,
	}, {
		id: 'littlebar',
		display: false,
		stacked: true,
		ticks: {
			beginAtZero: true,
			max: 50,
			autoSkip: false,
		},
		type: 'linear',
		position: 'right',
	}];
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-new_user');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let activeUserArray = [];
	let pendingUserArray = [];
	let withoutPasswordUserArray = [];

	let delta = [];

	duration = end.diff(start, 'years');
	activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{
				$or: [
					{"condos.joined": { $gte: new Date(parseInt(start.format('x'))) }},
					{"condos.joined": { $gte: parseInt(start.format('x')) }},
				]
			},
			{
				$or: [
					{"condos.joined": { $lte: new Date(parseInt(end.format('x'))) }},
					{"condos.joined": { $lte: parseInt(end.format('x')) }},
				]
			},
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('YYYY') }
	});

	withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('YYYY') };
	});

	pendingUser = _.map(Residents.find({
		$and: [
			{
				$or: [
					{$and: [
						{"pendings.0": { $exists: true }},
						{"pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"pendings.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'pendings.condoId': { $in: condoIds }}
					]},
					{$and: [
						{"condos.0": { $exists: true }},
						{"condos.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"condos.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'condos.condoId': { $in: condoIds }}
					]},
				],
			},
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }
		]
	}).fetch(), function (resident) {
		if (resident.pendings[0])
			return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('YYYY') };
		else if (resident.condos[0])
			return { _id: resident.userId, date: moment(resident.condos[0].invited).format('YYYY') };
	});

	let year = start;
	for (var i = 0; i <= duration; i++) {

		label.push(year.format("YYYY"));

		totalBefore = activeUserLength + pendingUserLength + withoutPasswordUserLength;

		withoutPasswordToSubtract = 0;
		pendingToSubtract = 0;
		activeUserData = _.filter(activeUser, function (aUser) { return aUser.date == year.format('YYYY'); });
		if (activeUserData.length > 0) {
			let OldPendingUser = oldUser.pendingUser;
			let OldWithoutPasswordUser = oldUser.withoutPasswordUser;

			_.each(activeUserData, function(data) {
				if (_.contains(OldWithoutPasswordUser, data._id))
					withoutPasswordToSubtract += 1;
				if (_.contains(OldPendingUser, data._id))
					pendingToSubtract += 1;
			})
		}

		activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0;
		pendingUserLength += (_.filter(pendingUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0) - pendingToSubtract;
		withoutPasswordUserLength += (_.filter(withoutPasswordUser, function (aUser) { return aUser.date == year.format('YYYY') }).length || 0) - withoutPasswordToSubtract;

		total = activeUserLength + pendingUserLength + withoutPasswordUserLength;

		activeUserArray.push(activeUserLength);
		pendingUserArray.push(pendingUserLength);
		withoutPasswordUserArray.push(withoutPasswordUserLength);

		delta.push(totalBefore != 0 ? ((total / totalBefore) - 1) * 100 : 0);

		year.add(1, "y");
	}

	return {
		labels: label,
		activeUserArray: activeUserArray,
		pendingUserArray: pendingUserArray,
		withoutPasswordUserArray: withoutPasswordUserArray,
		delta: delta
	};
}
function get_data_of_month(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let activeUserArray = [];
	let pendingUserArray = [];
	let withoutPasswordUserArray = [];

	let delta = [];


	duration = end.diff(start, 'months');
	activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{
				$or: [
					{"condos.joined": { $gte: new Date(parseInt(start.format('x'))) }},
					{"condos.joined": { $gte: parseInt(start.format('x')) }},
				]
			},
			{
				$or: [
					{"condos.joined": { $lte: new Date(parseInt(end.format('x'))) }},
					{"condos.joined": { $lte: parseInt(end.format('x')) }},
				]
			},
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('MM[/]YYYY') }
	});


	withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('MM[/]YYYY') };
	});

	pendingUser = _.map(Residents.find({
		$and: [
			{
				$or: [
					{$and: [
						{"pendings.0": { $exists: true }},
						{"pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"pendings.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'pendings.condoId': { $in: condoIds }}
					]},
					{$and: [
						{"condos.0": { $exists: true }},
						{"condos.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"condos.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'condos.condoId': { $in: condoIds }}
					]},
				],
			},
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }
		]
	}).fetch(), function (resident) {
		if (resident.pendings[0])
			return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('MM[/]YYYY') };
		else if (resident.condos[0])
			return { _id: resident.userId, date: moment(resident.condos[0].invited).format('MM[/]YYYY') };
	});

	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		if (month.diff(moment().endOf("months"), 'days') < -32) {
			totalBefore = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			withoutPasswordToSubtract = 0;
			pendingToSubtract = 0;
			activeUserData = _.filter(activeUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY'); });
			if (activeUserData.length > 0) {
				let OldPendingUser = oldUser.pendingUser;
				let OldWithoutPasswordUser = oldUser.withoutPasswordUser;

				_.each(activeUserData, function(data) {
					if (_.contains(OldWithoutPasswordUser, data._id))
						withoutPasswordToSubtract += 1;
					if (_.contains(OldPendingUser, data._id))
						pendingToSubtract += 1;
				})
			}

			activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0;
			pendingUserLength += (_.filter(pendingUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0) - pendingToSubtract;
			withoutPasswordUserLength += (_.filter(withoutPasswordUser, function (aUser) { return aUser.date == month.format('MM[/]YYYY') }).length || 0) - withoutPasswordToSubtract;

			total = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			activeUserArray.push(activeUserLength);
			pendingUserArray.push(pendingUserLength);
			withoutPasswordUserArray.push(withoutPasswordUserLength);

			delta.push(totalBefore != 0 ? ((total / totalBefore) - 1) * 100 : 0);
		} else {
			activeUserArray.push(0);
			pendingUserArray.push(0);
			withoutPasswordUserArray.push(0);

			delta.push(0);
		}
		month.add(1, "M");
	}

	return {
		labels: label,
		activeUserArray: activeUserArray,
		pendingUserArray: pendingUserArray,
		withoutPasswordUserArray: withoutPasswordUserArray,
		delta: delta
	};
}
function get_data_of_day(start, end, condoIds, oldUser) {
	let duration = 0;

	let label = [];

	let activeUserLength = oldUser.activeUser.length;
	let pendingUserLength = oldUser.pendingUser.length;
	let withoutPasswordUserLength = oldUser.withoutPasswordUser.length;

	let withoutPasswordToSubtract = 0;
	let pendingToSubtract = 0;

	let activeUserArray = [];
	let pendingUserArray = [];
	let withoutPasswordUserArray = [];

	let delta = [];

	duration = end.diff(start, 'days')
	activeUser = _.map(Residents.find({
		$and: [
			{ "condos.0": { $exists: true } },
			{
				$or: [
					{"condos.joined": { $gte: new Date(parseInt(start.format('x'))) }},
					{"condos.joined": { $gte: parseInt(start.format('x')) }},
				]
			},
			{
				$or: [
					{"condos.joined": { $lte: new Date(parseInt(end.format('x'))) }},
					{"condos.joined": { $lte: parseInt(end.format('x')) }},
				]
			},
			{ 'condos.condoId': { $in: condoIds } }
		]
	}).fetch(), function (resident) {
		return { _id: resident.userId, date: moment(resident.condos[0].joined).format('DD[/]MM[/]YY') }
	});


	withoutPasswordUser = _.map(Meteor.users.find({
		$and: [
			{ "createdAt": { $gte: new Date(parseInt(start.format('x'))) } },
			{ "createdAt": { $lte: new Date(parseInt(end.format('x'))) } },
			{ "services.password": { $exists: false } },
			{ "_id": { $nin: _.map(activeUser, function (res) { return res._id }) } },
		]
	}).fetch(), function (user) {
		return { _id: user._id, date: moment(user.createdAt).format('DD[/]MM[/]YY') };
	});

	pendingUser = _.map(Residents.find({
		$and: [
			{
				$or: [
					{$and: [
						{"pendings.0": { $exists: true }},
						{"pendings.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"pendings.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'pendings.condoId': { $in: condoIds }}
					]},
					{$and: [
						{"condos.0": { $exists: true }},
						{"condos.invited": { $gte: new Date(parseInt(start.format('x'))) } },
						{"condos.invited": { $lte: new Date(parseInt(end.format('x'))) }},
						{'condos.condoId': { $in: condoIds }}
					]},
				],
			},
			{ "userId": { $nin: _.map(activeUser, function (res) { return res._id }) } },
			{ "userId": { $nin: _.map(withoutPasswordUser, function (res) { return res._id }) } }
		]
	}).fetch(), function (resident) {
		if (resident.pendings[0])
			return { _id: resident.userId, date: moment(resident.pendings[0].invited).format('DD[/]MM[/]YY') };
		else if (resident.condos[0])
			return { _id: resident.userId, date: moment(resident.condos[0].invited).format('DD[/]MM[/]YY') };
	});

	let day = start;
	for (var i = 0; i < duration; i++) {

		label.push(day.format("ddd DD MMM"));

		if (day.diff(moment().endOf("days"), 'hours') < 0) {
			totalBefore = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			withoutPasswordToSubtract = 0;
			pendingToSubtract = 0;
			activeUserData = _.filter(activeUser, function (aUser) { return aUser.date == day.format('DD[/]MM[/]YY'); });
			if (activeUserData.length > 0) {
				let OldPendingUser = oldUser.pendingUser;
				let OldWithoutPasswordUser = oldUser.withoutPasswordUser;

				_.each(activeUserData, function(data) {
					if (_.contains(OldWithoutPasswordUser, data._id))
						withoutPasswordToSubtract += 1;
					if (_.contains(OldPendingUser, data._id))
						pendingToSubtract += 1;
				})
			}

			activeUserLength += _.filter(activeUser, function (aUser) { return aUser.date == day.format('DD[/]MM[/]YY'); }).length || 0;
			pendingUserLength += (_.filter(pendingUser, function (pUser) { return pUser.date == day.format('DD[/]MM[/]YY'); }).length || 0) - pendingToSubtract;
			withoutPasswordUserLength += (_.filter(withoutPasswordUser, function (wUser) { return wUser.date == day.format('DD[/]MM[/]YY'); }).length || 0) - withoutPasswordToSubtract;

			total = activeUserLength + pendingUserLength + withoutPasswordUserLength;

			activeUserArray.push(activeUserLength);
			pendingUserArray.push(pendingUserLength);
			withoutPasswordUserArray.push(withoutPasswordUserLength);

			delta.push(totalBefore != 0 ? ((total / totalBefore) - 1) * 100 : 0);
		} else {
			activeUserArray.push(0);
			pendingUserArray.push(0);
			withoutPasswordUserArray.push(0);

			delta.push(0);
		}
		day.add(1, "d");
	}

	return {
		labels: label,
		activeUserArray: activeUserArray,
		pendingUserArray: pendingUserArray,
		withoutPasswordUserArray: withoutPasswordUserArray,
		delta: delta
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	let activeUser = _.map(Residents.find({
		"condos.0": { $exists: true },
		$and: [
			{
				$or: [
					{"condos.joined": { $lte: new Date(parseInt(start.format('x'))) }},
					{"condos.joined": { $lte: parseInt(start.format('x')) }},
				]
			},
			{'condos.condoId': { $in: condoIds }}
		]
	}).fetch(), function (resident) {
		return resident._id
	});

	let withoutPasswordUser = _.map(Meteor.users.find({
		"createdAt": { $lte: new Date(parseInt(start.format('x'))) },
		"services.password": { $exists: false },
		"identities.residentId": { $nin: activeUser }
	}).fetch(), function (user) {
		return user._id
	});

	let pendingUser = _.map(Residents.find({
		$or: [
			{$and: [
				{"pendings.0": { $exists: true }},
				{"pendings.invited": { $lte: new Date(parseInt(start.format('x'))) }},
				{'pendings.condoId': { $in: condoIds }}
			]},
			{$and: [
				{"condos.0": { $exists: true }},
				{"condos.invited": { $lte: new Date(parseInt(start.format('x'))) }},
				{'condos.condoId': { $in: condoIds }}
			]},
		],
		"_id": { $nin: activeUser },
		"userId": { $nin: withoutPasswordUser },
	}).fetch(), function (resident) {
		return resident.userId;
	});

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds, {activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser});
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds, {activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser});
	}
	else {
		data = get_data_of_day(start, end, condoIds, {activeUser: activeUser, withoutPasswordUser: withoutPasswordUser, pendingUser: pendingUser});
	}

	dataset.push({
		label: 'Growth',
		yAxisID: 'B',
		data: _.map(data.delta, function (d) { return d.toFixed(1); }),
		datalabels: {
			display: false,
		},
		fill: false,
		backgroundColor: "#8b999f",
		borderColor: "#8b999f",
		pointHitRadius: 20,
		type: 'line',
	});

	dataset.push({
		label: 'Active users',
		yAxisID: 'A',
		data: data.activeUserArray,
		backgroundColor: "#fdb812",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		},
	});

	dataset.push({
		label: 'Inactive invited users',
		yAxisID: 'A',
		data: data.withoutPasswordUserArray,
		backgroundColor: "#a3b4bc",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		},
	});

	dataset.push({
		label: 'Users to be validated',
		yAxisID: 'A',
		data: data.pendingUserArray,
		backgroundColor: "#69d2e8",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		},
	});

	dataset.push({
		label: '',
		data: _.map(data.labels, function (DONT_USE, index) { return data.activeUserArray[index] + data.pendingUserArray[index] + data.withoutPasswordUserArray[index] }),
		yAxisID: 'A',
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_new_user(start, end, option) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	let max = 5;

	_.each(data.datasets[0].data, function (d) {
		if (max < d) max = parseInt(d);
	})
	options.scales.yAxes[1].ticks.max = parseInt(Math.round(max / 10) * 10);
	options.scales.yAxes[1].ticks.min = parseInt(Math.round(max / 10) * 10 * -1);

	$('#bar_new_user').remove();
	$('#bar_new_user-container').append('<canvas id="bar_new_user"><canvas>');
	let ctx = $('#bar_new_user');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-new-user-legend").html(myChart.generateLegend());
}



Template.bar_new_user.onCreated(function() {
});

let myInterval;

Template.bar_new_user.onRendered(function() {
	let start       = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end			= moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds	= Session.get('condoIds').length;
	let condoType	= Session.get('condoType').length;
	let render		= true;
	let template	= Template.instance();
	myInterval		= setInterval(function() {

		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || (!render && condoIds != Session.get('condoIds').length) || (!render && condoType != Session.get('condoType').length))) {
			start		= moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end			= moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds	= Session.get('condoIds').length;
			condoType	= Session.get('condoType').length;
			render		= true;
		}
		if (render) {
			render_bar_new_user(start.clone(), end.clone(), template.data.option);
			render		= false;
		}
	}, 1000)
	$( window ).resize(function() {
		start		= moment(Session.get('startDate'), "DD[/]MM[/]YY");
		end			= moment(Session.get('endDate'), "DD[/]MM[/]YY");
		render_bar_new_user(start.clone(), end.clone(), template.data.option);
	});
});

Template.bar_new_user.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_new_user.events({
});

Template.bar_new_user.helpers({
});