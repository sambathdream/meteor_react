function set_options_of_chart(option) {

	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-number_conversation').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_conversation').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_conversation').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_conversation');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_admin = [];
	let data_union_council = [];
	let data_building_supervisor = [];
	let data_occupant = [];

	let bar_number_conversation = [];
  duration = end.diff(start, 'years');
  bar_number_conversation_admin = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": "manager"
  }).fetch(), function (messages) {
    return moment(messages.date).format('YYYY')
  })]));

  bar_number_conversation_union_council = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleUnionCouncil(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('YYYY')
  })]));

  bar_number_conversation_building_supervisor = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleSupervisor(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('YYYY')
  })]));

  bar_number_conversation_occupant = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleOccupant(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('YYYY')
  })]));

	let year = start;
	for (var i = 0; i <= duration; i++) {
		label.push(year.format("YYYY"));
		data_admin.push(bar_number_conversation_admin[year.format('YYYY')] || 0);
		data_union_council.push(bar_number_conversation_union_council[year.format('YYYY')] || 0);
		data_building_supervisor.push(bar_number_conversation_building_supervisor[year.format('YYYY')] || 0);
		data_occupant.push(bar_number_conversation_occupant[year.format('YYYY')] || 0);
		year.add(1, "y");
	}

	return {
		labels: label,
		admin: data_admin,
		union_council: data_union_council,
		building_supervisor: data_building_supervisor,
		occupant: data_occupant,
	};
}
function get_data_of_month(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_admin = [];
	let data_union_council = [];
	let data_building_supervisor = [];
	let data_occupant = [];

  duration = end.diff(start, 'months');
  bar_number_conversation_admin = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": "manager"
  }).fetch(), function (messages) {
    return moment(messages.date).format('MM[/]YYYY')
  })]));

  bar_number_conversation_union_council = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleUnionCouncil(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('MM[/]YYYY')
  })]));

  bar_number_conversation_building_supervisor = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleSupervisor(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('MM[/]YYYY')
  })]));

  bar_number_conversation_occupant = _.countBy(_.flatten([_.map(Messages.find({
    "condoId": { $in: condoIds },
    "target": getDefaultRoleOccupant(),
  }).fetch(), function (messages) {
    return moment(messages.date).format('MM[/]YYYY')
  })]));

	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_admin.push(bar_number_conversation_admin[month.format('MM[/]YYYY')] || 0);
		data_union_council.push(bar_number_conversation_union_council[month.format('MM[/]YYYY')] || 0);
		data_building_supervisor.push(bar_number_conversation_building_supervisor[month.format('MM[/]YYYY')] || 0);
		data_occupant.push(bar_number_conversation_occupant[month.format('MM[/]YYYY')] || 0);
		month.add(1, "M");
	}

	return {
		labels: label,
		admin: data_admin,
		union_council: data_union_council,
		building_supervisor: data_building_supervisor,
		occupant: data_occupant,
	};
}
function get_data_of_day(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_admin = [];
	let data_union_council = [];
	let data_building_supervisor = [];
	let data_occupant = [];

	duration = end.diff(start, 'days');
	bar_number_conversation_admin = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
		"target": "manager"
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_conversation_union_council = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
    "target": getDefaultRoleUnionCouncil(),
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_conversation_building_supervisor = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
    "target": getDefaultRoleSupervisor(),
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_conversation_occupant = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
    "target": getDefaultRoleOccupant(),
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY')
	})]));

	duration = end.diff(start, 'days');
	let day = start;
	for (var i = 0; i < duration; i++) {
		label.push(day.format("ddd DD MMM"));
		data_admin.push(bar_number_conversation_admin[day.format('DD[/]MM[/]YY')] || 0);
		data_union_council.push(bar_number_conversation_union_council[day.format('DD[/]MM[/]YY')] || 0);
		data_building_supervisor.push(bar_number_conversation_building_supervisor[day.format('DD[/]MM[/]YY')] || 0);
		data_occupant.push(bar_number_conversation_occupant[day.format('DD[/]MM[/]YY')] || 0);
		day.add(1, "d");
	}

	return {
		labels: label,
		admin: data_admin,
		union_council: data_union_council,
		building_supervisor: data_building_supervisor,
		occupant: data_occupant,
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: '# Gestionnaire',
		data: data.admin,
		fill: true,
		lineTension: 0.6,
		backgroundColor: "#fdb812",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '# Gardien',
		data: data.building_supervisor,
		fill: true,
		lineTension: 0.6,
		backgroundColor: "#e4666b",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '# Conseil syndical',
		data: data.union_council,
		fill: true,
		lineTension: 0.6,
		backgroundColor: "#8b999f",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '# Occupants',
		data: data.occupant,
		fill: true,
		lineTension: 0.6,
		backgroundColor: "#69d2e7",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '',
		data: _.map(data.labels, function (DONT_USE, index) { return data.admin[index] + data.building_supervisor[index] + data.union_council[index] + data.occupant[index] }),
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_number_conversation(start, end, option) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#bar_number_conversation').remove();
	$('#bar_number_conversation-container').append('<canvas id="bar_number_conversation"><canvas>');
	let ctx = $('#bar_number_conversation');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_conversation-legend").html(myChart.generateLegend());
}


Template.bar_number_conversation.onCreated(function() {
});

let myInterval;

Template.bar_number_conversation.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_number_conversation(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_number_conversation.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_number_conversation.events({
});

Template.bar_number_conversation.helpers({
});

function getDefaultRoleOccupant() {
  let defaultRoles = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })
  if (!!defaultRoles === true) {
    return defaultRoles._id
  }
}
function getDefaultRoleUnionCouncil() {
  let defaultRoles = DefaultRoles.findOne({ name: 'Conseil Syndical' }, { fields: { _id: true } })
  if (!!defaultRoles === true) {
    return defaultRoles._id
  }
}

function getDefaultRoleSupervisor() {
  let defaultRoles = DefaultRoles.findOne({ name: 'Gardien' }, { fields: { _id: true } })
  if (!!defaultRoles === true) {
    return defaultRoles._id
  }
}