function set_options_of_chart(option) {
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-total_users_modules').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-total_users_modules').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-total_users_modules').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-total_users_modules');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_users_by_year(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('YYYY')} && moment n: ${moment(u.loginDate).format('YYYY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('YYYY') == moment(u.endDate).format('YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_year(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
  let data_feed = []
  let data_forum = []
  let data_incidents = []
  let data_ads = []
  let data_informations = []
  let data_reservations = []
  let data_concierge = []
  let data_messenger = []
  let data_trombi = []

  let plateforms = [ "web", "ios", "android" ]
  let accessType = ""
  switch (select) {
    case 0:
      accessType = { $in: plateforms }
      break;
    case 1:
      accessType = "web"
      break;
    case 2:
      accessType = "ios"
      break;
    case 3:
      accessType = "android"
      break;
    default:
      break;
  }

	duration = end.diff(start, 'years');
  let bar_total_users_feed = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "feed",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_forum = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "forum",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_incidents = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "incident",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_ads = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "ads",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_info = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "info",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_resa = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "resa",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_concierge = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "concierge",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_messenger = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "messenger",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

  let bar_total_users_trombi = _.countBy(_.flatten([_.map(get_users_by_year(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "trombi",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("YYYY")
  })]));

	let year;

  for (var i = 0; i <= duration; i++) {
		year = start;
		label.push(year.format("YYYY"));
		data_feed.push(bar_total_users_feed[day.format('YYYY')] || 0);
		data_forum.push(bar_total_users_forum[day.format('YYYY')] || 0);
		data_incidents.push(bar_total_users_incidents[day.format('YYYY')] || 0);
		data_ads.push(bar_total_users_ads[day.format('YYYY')] || 0);
		data_informations.push(bar_total_users_info[day.format('YYYY')] || 0);
		data_reservations.push(bar_total_users_resa[day.format('YYYY')] || 0);
		data_concierge.push(bar_total_users_concierge[day.format('YYYY')] || 0);
		data_messenger.push(bar_total_users_messenger[day.format('YYYY')] || 0);
		data_trombi.push(bar_total_users_trombi[day.format('YYYY')] || 0);
		year.add(1, "y");
	}

	return {
		labels: label,
		feed: data_feed,
		forum: data_forum,
    incidents: data_incidents,
    ads: data_ads,
    informations: data_informations,
    reservations: data_reservations,
    concierge: data_concierge,
    messenger: data_messenger,
    trombi: data_trombi
	};
}

function get_users_by_month(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('MM[/]YY')} && moment n: ${moment(u.loginDate).format('MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('MM[/]YY') == moment(u.endDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_month(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
  let data_feed = []
  let data_forum = []
  let data_incidents = []
  let data_ads = []
  let data_informations = []
  let data_reservations = []
  let data_concierge = []
  let data_messenger = []
  let data_trombi = []

  let plateforms = [ "web", "ios", "android" ]
  let accessType = ""
  switch (select) {
    case 0:
      accessType = { $in: plateforms }
      break;
    case 1:
      accessType = "web"
      break;
    case 2:
      accessType = "ios"
      break;
    case 3:
      accessType = "android"
      break;
    default:
      break;
  }

  duration = end.diff(start, 'months');
  let bar_total_users_feed = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "feed",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_forum = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "forum",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_incidents = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "incident",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_ads = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "ads",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_info = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "info",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_resa = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "resa",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_concierge = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "concierge",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_messenger = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "messenger",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_trombi = _.countBy(_.flatten([_.map(get_users_by_month(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "trombi",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("MM[/]YYYY")
  })]));

	let month = start.startOf('month')

  for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_feed.push(bar_total_users_feed[day.format('MM[/]YYYY')] || 0);
		data_forum.push(bar_total_users_forum[day.format('MM[/]YYYY')] || 0);
		data_incidents.push(bar_total_users_incidents[day.format('MM[/]YYYY')] || 0);
		data_ads.push(bar_total_users_ads[day.format('MM[/]YYYY')] || 0);
		data_informations.push(bar_total_users_info[day.format('MM[/]YYYY')] || 0);
		data_reservations.push(bar_total_users_resa[day.format('MM[/]YYYY')] || 0);
		data_concierge.push(bar_total_users_concierge[day.format('MM[/]YYYY')] || 0);
		data_messenger.push(bar_total_users_messenger[day.format('MM[/]YYYY')] || 0);
		data_trombi.push(bar_total_users_trombi[day.format('MM[/]YYYY')] || 0);
		month.add(1, "M");
	}

	return {
		labels: label,
		feed: data_feed,
		forum: data_forum,
    incidents: data_incidents,
    ads: data_ads,
    informations: data_informations,
    reservations: data_reservations,
    concierge: data_concierge,
    messenger: data_messenger,
    trombi: data_trombi
	};
}

function get_users_by_day(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      if (w.userId == u.userId && moment(w.endDate).format('DD[/]MM[/]YY') == moment(u.endDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_day(start, end, condoIds, select) {
	let duration = 0
  let label = []
  let data_feed = []
  let data_forum = []
  let data_incidents = []
  let data_ads = []
  let data_informations = []
  let data_reservations = []
  let data_concierge = []
  let data_messenger = []
  let data_trombi = []

  // console.log("analytics: ", Analytics.find().fetch())
  let plateforms = [ "web", "ios", "android" ]
  let accessType = ""
  switch (select) {
    case 0:
      accessType = { $in: plateforms }
      break;
    case 1:
      accessType = "web"
      break;
    case 2:
      accessType = "ios"
      break;
    case 3:
      accessType = "android"
      break;
    default:
      break;
  }

  duration = end.diff(start, 'days');
  let bar_total_users_feed = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "feed",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_forum = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "forum",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_incidents = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "incident",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_ads = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "ads",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_info = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "info",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_resa = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "resa",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_concierge = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "concierge",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_messenger = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "messenger",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_trombi = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "module",
    accessType: accessType,
    module: "trombi",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.endDate).format("DD[/]MM[/]YY")
  })]));

  // console.log("bar_total_users_web: ", bar_total_users_web)
  // console.log("bar_total_users_mobile: ", bar_total_users_mobile)

  let day = start;

  for (var i = 0; i < duration; i++) {
    label.push(day.format("ddd DD MMM"));
		data_feed.push(bar_total_users_feed[day.format('DD[/]MM[/]YY')] || 0);
		data_forum.push(bar_total_users_forum[day.format('DD[/]MM[/]YY')] || 0);
		data_incidents.push(bar_total_users_incidents[day.format('DD[/]MM[/]YY')] || 0);
		data_ads.push(bar_total_users_ads[day.format('DD[/]MM[/]YY')] || 0);
		data_informations.push(bar_total_users_info[day.format('DD[/]MM[/]YY')] || 0);
		data_reservations.push(bar_total_users_resa[day.format('DD[/]MM[/]YY')] || 0);
		data_concierge.push(bar_total_users_concierge[day.format('DD[/]MM[/]YY')] || 0);
		data_messenger.push(bar_total_users_messenger[day.format('DD[/]MM[/]YY')] || 0);
		data_trombi.push(bar_total_users_trombi[day.format('DD[/]MM[/]YY')] || 0);
    day.add(1, "d");
	}

	return {
		labels: label,
		feed: data_feed,
		forum: data_forum,
    incidents: data_incidents,
    ads: data_ads,
    informations: data_informations,
    reservations: data_reservations,
    concierge: data_concierge,
    messenger: data_messenger,
    trombi: data_trombi
	};
}

function build_datasets_of_chart(start, end, condoIds, select) {
	let data = []
	let dataset = []

	if ((end.diff(start, 'years')) >= 1) {
    // console.log("====== in years ======")
		data = get_data_of_year(start, end, condoIds, select)
	}
	else if ((end.diff(start, 'months')) >= 3) {
    // console.log("====== in months ======")
		data = get_data_of_month(start, end, condoIds, select)
	}
	else {
    // console.log("====== in days ======")
		data = get_data_of_day(start, end, condoIds, select)
  }

  // console.log("data: ", data)
  dataset.push({
		label: '# utilisateurs du fil d\'actualité',
		data: data.feed,
		fill: true,
		backgroundColor: "#674172",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# utilisateurs du forum',
		data: data.forum,
		fill: true,
		backgroundColor: "#fe0900",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

	dataset.push({
		label: '# utilisateurs des incidents',
		data: data.incidents,
		fill: true,
		backgroundColor: "#5d5e62",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# utilisateurs des petites annonces',
		data: data.ads,
		fill: true,
		backgroundColor: "#fdb813",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
  });

  dataset.push({
		label: '# utilisateurs des informations',
		data: data.informations,
		fill: true,
		backgroundColor: "#ffe199",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# utilisateurs des réservations',
		data: data.reservations,
		fill: true,
		backgroundColor: "#69d2e7",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# utilisateurs de la conciergerie',
		data: data.concierge,
		fill: true,
		backgroundColor: "#8b999f",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
  });

  dataset.push({
		label: '# utilisateurs de la messagerie',
		data: data.messenger,
		fill: true,
		backgroundColor: "#e0e0e0",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# utilisateurs du trombinoscope',
		data: data.trombi,
		fill: true,
		backgroundColor: "#AEE4EF",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

	dataset.push({
		label: '',
		data: data.forum,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_total_users_modules(start, end, option, select) {
	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds, select)

	$('#bar_total_users_modules').remove();
	$('#bar_total_users_modules-container').append('<canvas id="bar_total_users_modules"><canvas>');
	let ctx = $("#bar_total_users_modules");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-total_users_modules-legend").html(myChart.generateLegend());
}



// Templates methods
Template.bar_total_users_modules.onCreated(function() {
  this.select = new ReactiveVar(0)
});

let myInterval;

Template.bar_total_users_modules.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");

  let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;

  let render = true;
	let template = Template.instance();

  myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) ||
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {

      start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");

      condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;

      render = true;
		}
		if (render) {
			render_bar_total_users_modules(start.clone(), end.clone(), template.data.option, template.select.get());
			render = false;
		}
	}, 1000)
});


Template.bar_total_users_modules.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_total_users_modules.events({
	'change #usersPlatform': (event, template) => {
		template.select.set(parseInt(event.currentTarget.value));
		let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
		let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
		render_bar_total_users_modules(start.clone(), end.clone(), template.data.option, template.select.get());
	}
});

Template.bar_total_users_modules.helpers({
});