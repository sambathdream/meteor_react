import { CommonTranslation } from "/common/lang/lang.js"

function DayUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'days');
	for (var i = 0; i < duration; i++) {
		tmpCol = {
			types: [],
			name: start.format('ddd DD MMM'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let actusOfDay = _.filter(collection.actus, (a) => {
			return moment(a.createdAt).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let resourcesOfDay = _.filter(collection.resources, (a) => {
			if (a != undefined)
				return moment(a.start).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		let messengerOfDay = _.filter(collection.messenger, (a) => {
			return moment(a.date).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
		});
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "actus"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "resources"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "concierge"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		start.add(1, 'd');
	}
	return {cols: cols, total: total};
}

function WeekUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

  const lang = FlowRouter.getParam("lang") || "fr"
  const tr_common = new CommonTranslation(lang)

  duration = end.diff(start, 'weeks');
	for (var i = 0; i < duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		tmpCol = {
			types: [],
			name: tr_common.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('W YYYY') == start.format('W YYYY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('W YYYY') == start.format('W YYYY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
			return moment(i.start).format('W YYYY') == start.format('W YYYY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('W YYYY') == start.format('W YYYY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('W YYYY') == start.format('W YYYY');
		});
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "actus"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "resources"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "concierge"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		start.add(1, 'weeks');
	}
	return {cols: cols, total: total};
}

function MonthUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'month');
	for (var i = 0; i <= duration; i++) {
		tmpCol = {
			types: [],
			name: !(start.format('YYYY') == moment().format('YYYY')) ? start.format('MMMM YYYY') :  start.format('MMMM'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
			return moment(i.start).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('MM[/]YY') == start.format('MM[/]YY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('MM[/]YY') == start.format('MM[/]YY');
		});
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "actus"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "resources"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "concierge"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		start.add(1, 'M');
	}
	return {cols: cols, total: total};
}

function YearUtilisations(start, end, collection) {
	let cols = [];
	let total = 0;
	let duration = 0;
	let tmpCol = [];

	duration = end.diff(start, 'year');
	for (var i = 0; i <= duration; i++) {
		tmpCol = {
			types: [],
			name: start.format('YYYY'),
			total: 0
		};
		let forumOfDay = _.filter(collection.forum, (a) => {
			return moment(a.date).format('YYYY') == start.format('YYYY');
		});
		let incidentsOfDay = _.filter(collection.incidents, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let classifiedsOfDay = _.filter(collection.ads, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let actusOfDay = _.filter(collection.actus, (i) => {
			return moment(i.createdAt).format('YYYY') == start.format('YYYY');
		});
		let resourcesOfDay = _.filter(collection.resources, (i) => {
			return moment(i.start).format('YYYY') == start.format('YYYY');
		});
		let conciergeOfDay = _.filter(collection.concierge, (i) => {
			return moment(i.date).format('YYYY') == start.format('YYYY');
		});
		let messengerOfDay = _.filter(collection.messenger, (i) => {
			return moment(i.date).format('YYYY') == start.format('YYYY');
		});
		tmpCol.types.push({value: forumOfDay.length, category: "forum"});
		tmpCol.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpCol.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpCol.types.push({value: actusOfDay.length, category: "actus"});
		tmpCol.types.push({value: resourcesOfDay.length, category: "resources"});
		tmpCol.types.push({value: conciergeOfDay.length, category: "concierge"});
		tmpCol.types.push({value: messengerOfDay.length, category: "messenger"});
		tmpCol.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		cols.push(tmpCol);
		total += (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
		start.add(1, 'y');
	}
	return {cols: cols, total: total};
}

function getUtilisation(start, end, condoId) {
	let forum = {};
	let actus = (condoId == "all" ? ActuPosts.find().fetch() : ActuPosts.find({condoId: {$in: condoId}}).fetch());
	let classifieds = (condoId == "all" ? ClassifiedsAds.find().fetch() : ClassifiedsAds.find({condoId: {$in: condoId}}).fetch());
	let incidents = (condoId == "all" ? Incidents.find().fetch() : Incidents.find({condoId: {$in: condoId}}).fetch());
	let resources = (condoId == "all" ? _.flatten(_.map(Resources.find({type: {$ne: "Etat des lieux"}}).fetch(), function(r){ return r.events})) : _.flatten(_.map(Resources.find({type: {$ne: "Etat des lieux"}, condoId: condoId}).fetch(), function(r){ return r.events})));
	let concierge = (condoId == "all" ? ConciergeMessage.find().fetch() : ConciergeMessage.find({condoId: {$in: condoId}}).fetch());
	if (condoId == "all") {
		forum = ForumPosts.find().fetch();
	}
	else {
		forum = ForumPosts.find({condoId: {$in: condoId}}).fetch();
	}
	let conversationIds = (condoId == "all" ? _.map(Messages.find().fetch(), function(conv) {return conv._id}) : _.map(Messages.find({condoId: {$in: condoId}}).fetch(), function(conv) {return conv._id}))
	let messenger = MessagesFeed.find({ messageId: { $in: conversationIds } }).fetch()
	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		})
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		})
	}
	// else if ((duration = end.diff(start, 'days')) > 31) {
	// 	res = WeekUtilisations(start, end, {
	// 		"forum": forum,
	// 		"incidents": incidents,
	// 		"ads": classifieds,
	// 		"actus": actus,
	// 		"resources": resources,
	// 		"concierge": concierge,
	// 		"messenger": messenger
	// 	})
	// }
	else {
		res = DayUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		})
	}
	let data = {
		cols: res.cols,
		total: res.total,
		categories: [
			{
				name: "Posts forum",
				slug: "forum",
				color: "#fdb813"
			},
			{
				name: "Incidents déclarés",
				slug: "incidents",
				color: "#e4666b"
			},
			{
				name: "Petites annonces",
				slug: "classifieds",
				color: "#dc8086"
			},
			{
				name: "Informations",
				slug: "informations",
				color: "#d3a6ad"
			},
			{
				name: "Réservations",
				slug: "reservations",
				color: "#a3b4bb"
			},
			{
				name: "Demandes conciergerie",
				slug: "conciergerie",
				color: "#8e989f"
			},
			{
				name: "Messages envoyés",
				slug: "messenger",
				color: "#b8d2da"
			}
		]
	};
	return data;
}

function buildDatasetUtilisation(start, end, condoId) {
	const data = getUtilisation(start, end, condoId);

	let label = [];
	let dataset = [];

	_.each(data.cols, (val) => {
		label.push(val.name);
	});

	let totalDs = {
		label: '',
		backgroundColor: 'transparent',
		borderColor: 'transparent',
		data: [],
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	};

	if (condoId == 'all') {
		condoId = _.map(Condos.find().fetch(), function(condo) { return condo._id });
	}

	let modules = _.map(Condos.find({"_id": {$in: condoId}}).fetch(), function(condo) {
		return condo.settings.options;
	});

	let isInCondo = false;

	_.each(data.categories, (val, key) => {
		isInCondo = false;
		let ds = {
			label: val.name,
			backgroundColor: val.color,
			borderColor: 'white',
			borderWidth: 1,
			data: [],
			datalabels: {
				display: false,
			}
		};

		_.each(modules, function(module) {
			if (module[val.slug] == true)
				isInCondo = true;
		})
		_.each(data.cols, (v, index) => {
			if (isInCondo) {
				if (v.types.length > 0) {
					const dsData = v.types[key];
					if (dsData !== undefined) {
						if (totalDs.data[index] == undefined) {
							totalDs.data[index] = 0;
						}
						totalDs.data[index] += dsData.value;
						ds.data.push(dsData.value);
					} else {
						ds.data.push(0);
					}
				} else {
					ds.data.push(0);
				}
			}
		});
		if (isInCondo) {
			dataset.push(ds);
		}
	});
	dataset.sort((a, b) => b.label.localeCompare(a.label));
	dataset.push(totalDs);

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(label, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: label,
		datasets: dataset
	}
}

function calculate_bar_number_module(startDate, endDate, option, condoId) {
	let options = option;
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-number_module').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_module').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_module').css("opacity", 1);
		}, 5);
	};
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_module');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = 'all';
	}

	let data = buildDatasetUtilisation(startDate, endDate, condoIds);

	$('#bar_number_module').remove();
	$('#bar_number_module-container').append('<canvas id="bar_number_module"><canvas>');
	let ctx = $("#bar_number_module");
	let myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_module-legend").html(myChart.generateLegend());
}


Template.bar_number_module.onCreated(function() {
});

let myInterval;

Template.bar_number_module.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) ||
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			calculate_bar_number_module(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_number_module.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_number_module.events({
});

Template.bar_number_module.helpers({
});