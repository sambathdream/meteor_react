function set_options_of_chart(option) {

	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-number_message').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_message').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_message').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;
	options.scales.yAxes = [{
		type: 'linear',
		position: 'left',
		gridLines: {
			display: false,
			color: "#88898e",
			lineWidth: 2,
			zeroLineWidth: 2,
			zeroLineColor: "#88898e",
			drawTicks: true,
			tickMarkLength: 3
		},
		ticks: {
			callback: function (value) {
				if (value >= 1) {
					return (value).toFixed(0);
				}
			},
			padding: 10,
			beginAtZero: true
		},

	}, {
		id: 'littlebar',
		display: false,
		stacked: true,
		ticks: {
			beginAtZero: true,
			max: 50,
			autoSkip: false,
		},
		type: 'linear',
		position: 'right',
	}];
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_message');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_bar_conv = [];
	let data_line_msg = [];
	let line_number_message = [];

	duration = end.diff(start, 'years');
	bar_number_message_total = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
		"incidentId": {$exists: false}
	}).fetch(), function (messages) {
		return moment(messages.date).format('YYYY');
	})]));

	line_number_message = _.countBy(_.flatten([_.map(MessagesFeed.find({
		"messageId": {
			$in: _.map(Messages.find({
				"condoId": { $in: condoIds },
				"incidentId": {$exists: false}
			}).fetch(), function (msg) {
				return msg._id
			})
		}
	}).fetch(), function (messages) {
		return moment(messages.date).format('YYYY');
	})]));


	let year;
	for (var i = 0; i <= duration; i++) {
		year = start;
		label.push(year.format("YYYY"));
		data_bar_conv.push(bar_number_message_total[year.format('YYYY')] || 0);
		data_line_msg.push(line_number_message[year.format('YYYY')] || 0);
		year.add(1, "y");
	}

	return {
		labels: label,
		bar_conv: data_bar_conv,
		line_msg: data_line_msg
	};
}
function get_data_of_month(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_bar_conv = [];
	let data_line_msg = [];
	let line_number_message = [];

	duration = end.diff(start, 'months');
	bar_number_message_total = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
		"incidentId": {$exists: false}
	}).fetch(), function (messages) {
		return moment(messages.date).format('MM[/]YYYY');
	})]));

	line_number_message = _.countBy(_.flatten([_.map(MessagesFeed.find({
		"messageId": {
			$in: _.map(Messages.find({
				"condoId": { $in: condoIds },
				"incidentId": {$exists: false}
			}).fetch(), function (msg) {
				return msg._id
			})
		}
	}).fetch(), function (messages) {
		return moment(messages.date).format('MM[/]YYYY');
	})]));

	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_bar_conv.push(bar_number_message_total[month.format('MM[/]YYYY')] || 0);
		data_line_msg.push(line_number_message[month.format('MM[/]YYYY')] || 0);
		month.add(1, "M");
	}

	return {
		labels: label,
		bar_conv: data_bar_conv,
		line_msg: data_line_msg
	};
}
function get_data_of_day(start, end, condoIds, oldUser) {

	let duration = 0;
	let label = [];
	let data_bar_conv = [];
	let data_line_msg = [];
	let line_number_message = [];

	duration = end.diff(start, 'days');
	bar_number_message_total = _.countBy(_.flatten([_.map(Messages.find({
		"condoId": { $in: condoIds },
		"incidentId": {$exists: false}
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY')
	})]));

	line_number_message = _.countBy(_.flatten([_.map(MessagesFeed.find({
		"messageId": {
			$in: _.map(Messages.find({
				"condoId": { $in: condoIds },
				"incidentId": {$exists: false}
			}).fetch(), function (msg) {
				return msg._id
			})
		}
	}).fetch(), function (messages) {
		return moment(messages.date).format('DD[/]MM[/]YY');
	})]));

	let day = start;
	for (var i = 0; i < duration; i++) {
		label.push(day.format("ddd DD MMM"));
		data_bar_conv.push(bar_number_message_total[day.format('DD[/]MM[/]YY')] || 0);
		data_line_msg.push(line_number_message[day.format('DD[/]MM[/]YY')] || 0);
		day.add(1, "d");
	}

	return {
		labels: label,
		bar_conv: data_bar_conv,
		line_msg: data_line_msg
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: '# messages',
		data: data.line_msg,
		lineTension: 0.25,
		fill: false,
		backgroundColor: "rgb(105, 210, 231)",
		borderColor: "rgb(105, 210, 231)",
		pointBorderColor: "rgb(105, 210, 231)",
		pointBackgroundColor: "rgb(105, 210, 231)",
		pointBorderWidth: 1,
		type: 'line',
		datalabels: {
			display: false
		}
	});

	dataset.push({
		label: '# conversations',
		data: data.bar_conv,
		fill: true,
		backgroundColor: "rgba(255, 71, 82, 1)",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

	dataset.push({
		label: '',
		data: data.bar_conv,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_line_number_message(start, end, option) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#line_number_message').remove();
	$('#line_number_message-container').append('<canvas id="line_number_message"><canvas>');
	let ctx = $("#line_number_message");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_message-legend").html(myChart.generateLegend());
}


Template.line_number_message.onCreated(function() {
});

let myInterval;

Template.line_number_message.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_line_number_message(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.line_number_message.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.line_number_message.events({
});

Template.line_number_message.helpers({
});