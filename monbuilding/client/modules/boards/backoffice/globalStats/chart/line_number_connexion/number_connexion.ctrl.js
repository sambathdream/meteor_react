function calculate_line_number_connexion(startDate, endDate, option) {

	let progress = document.getElementById('line_number_connexion-loading');
	let options = option;
	options.animation = {
		onProgress: function(animation) {
			progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
		},
		onComplete: function(animation) {
			window.setTimeout(function() {
				$('#line_number_connexion-loading').fadeOut(200);
			}, 100);
		}
	};
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-number_connexion').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_connexion').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_connexion').css("opacity", 1);
		}, 5);
	}
	options.scales.yAxes = [{
		id: 'A',
		type: 'linear',
		position: 'right',
		display: false,
		gridLines: {
			drawBorder: false,
			display : false
		},
		ticks: {
			beginAtZero:true
		}
	}, {
		id: 'B',
		type: 'linear',
		position: 'left',
		gridLines: {
			drawBorder: false,
			display : false
		},
		ticks: {
			custom: function(value) {
				return value + '%';
			},
			min: 0
		}
	}];

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condo) {return condo._id});
	}

	let duration = 0;
	let label = [];
	let data = [];
	let data_max = Residents.find({"condos.condoId": {$in: condoIds}}).fetch().length;

	let line_number_connexion = [];
	if ((duration = endDate.diff(startDate, 'years')) > 1) {
		line_number_connexion = _.countBy(_.flatten([ _.map(Analytics.find({
			'type': 'login',
			"condoId": {$in: condoIds},
		}).fetch(), function(loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == loginAnalytics.condoId; }))
				return moment(loginAnalytics.loginDate).format('YYYY');
		})]));

		let year; 
		for (var i = 0; i <= duration; i++) {
			year = startDate;
			value = line_number_connexion[year.format('YYYY')];
			label.push(year.format("YYYY"));
			data.push(value || 0);
			year.add(1, "y");
		}
	}

	else if ((duration = endDate.diff(startDate, 'months')) >= 3) {
		line_number_connexion = _.countBy(_.flatten([ _.map(Analytics.find({
			'type': 'login',
			"condoId": {$in: condoIds},
		}).fetch(), function(loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == loginAnalytics.condoId; }))
				return moment(loginAnalytics.loginDate).format('MM[/]YYYY');
		})]));

		let month; 
		for (var i = 0; i <= duration; i++) {
			month = startDate;
			value = line_number_connexion[month.format('MM[/]YYYY')];
			label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMM") : month.format("MMM YYYY"));
			data.push(value || 0);
			month.add(1, "M");
		}
	}

	else {
		line_number_connexion = _.countBy(_.flatten([ _.map(Analytics.find({
			'type': 'login',
			"condoId": {$in: condoIds},
		}).fetch(), function(loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == loginAnalytics.condoId; }))
				return moment(loginAnalytics.loginDate).format('DD[/]MM[/]YY')
		})]));
		duration = endDate.diff(startDate, 'days');
		let day;
		for (var i = 0; i <= duration; i++) {
			day = startDate;
			value = line_number_connexion[day.format('DD[/]MM[/]YY')];
			label.push(day.format("ddd DD MMM"));
			data.push(Math.trunc(value) || 0);
			day.add(1, "d");
		}
	}


	$('#line_number_connexion').remove();
	$('#line_number_connexion-container').append('<canvas id="line_number_connexion"><canvas>');
	let ctx = $("#line_number_connexion");
	let myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: label,
			datasets: [{
				label: '',
				data: data,
				lineTension: 0,
				backgroundColor: "transparent",
				borderColor: "transparent",
				pointBorderColor: "transparent",
				pointBackgroundColor: "transparent",
				pointHoverBackgroundColor: "transparent",
				pointHoverBorderColor: "transparent",
				type: 'line',
				datalabels: {
					color: '#000000',
					align: 'end'
				}
			},{
				label: '# de utilisateurs connectés',
				yAxisID: 'A',
				data:  data,
				datalabels: {
					display: false,
				},
				fill: true,
				backgroundColor: "#69d2e7",
				borderColor: "#69d2e7",
			}, {
				label: '% de utilisateurs connectés vs. active users',
				yAxisID: 'B',
				data: _.map(data, function(data){ return (data / data_max) * 100}),
				fill: false,
				lineTension: 0.4,
				backgroundColor: "#8b999f",
				borderColor: "#8b999f",
				borderCapStyle: 'square',
				borderDashOffset: 0.0,
				borderJoinStyle: 'miter',
				pointRadius: 4,
				pointBorderColor: "#8b999f",
				pointBackgroundColor: "#8b999f",
				pointBorderWidth: 1,
				pointHoverRadius: 4.20,
				pointHoverBackgroundColor: "#8b999f",
				pointHoverBorderColor: "#8b999f",
				pointHoverBorderWidth: 2,
				pointHitRadius: 10,
				datalabels: {
					display: false,
				},
				// notice the gap in the data and the spanGaps: true
				spanGaps: true,
				type: 'line',
			}]
		},
		options: options,
	});
	$("#chartjs-number_connexion-legend").html(myChart.generateLegend());
}


Template.line_number_connexion.onCreated(function() {
});

let myInterval;

Template.line_number_connexion.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
		end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
		condoIds = Session.get('condoIds').length;
		condoType = Session.get('condoType').length;
		render = true;
	}
	if (render) {
		calculate_line_number_connexion(start.clone(), end.clone(), template.data.option);
		render = false;
	}
}, 1000)
});


Template.line_number_connexion.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.line_number_connexion.events({
});

Template.line_number_connexion.helpers({
});