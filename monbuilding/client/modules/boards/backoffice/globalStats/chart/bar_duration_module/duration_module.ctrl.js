function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-duration_module').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-duration_module').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-duration_module').css("opacity", 1);
		}, 5);
	};
	options.scales.yAxes[0].ticks = {
		padding: 10,
		beginAtZero: true,
		fontColor: "#88898e",
		callback: function (value, index, values) {
			if (value >= 1440) {
				if (value % 1440 == 0)
					return parseInt(value / 1440) + 'j';
				else {
					if (value % 60 == 0) {
						return parseInt(value / 1440) + 'j';
					}
					return parseInt(value / 1440) + 'j ' + parseInt(value % 1440 / 60) + 'h';
				}
			}
			else if (value >= 60) {
				if (value % 60 == 0)
					return parseInt(value / 60) + 'h';
				else
					return parseInt(value / 60) + 'h' + parseInt(value % 60);
			}
			else if (value >= 1)
				return parseInt(value) + ' min';
		}
	};
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-duration_module');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(': ')[1]) {
						let value = parseInt(body[0].split(': ')[1]);
						if (parseInt(value) >= 1440) {
							if (parseInt(value) % 1440 == 0)
								value = parseInt(parseInt(value) / 1440) + 'j';
							else {
								if (parseInt(value) % 60 == 0) {
									value = parseInt(parseInt(value) / 1440) + 'j';
								}
								value = parseInt(parseInt(value) / 1440) + 'j ' + parseInt(parseInt(value) % 1440 / 60) + 'h';
							}
						}
						else if (parseInt(value) >= 60) {
							if (parseInt(value) % 60 == 0)
								value = parseInt(parseInt(value) / 60) + 'h';
							else
								value = parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
						}
						else if (parseInt(value) >= 1)
							value = parseInt(value) + ' min';
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, module) {
	let data = [];
	let analytics = _.flatten([ _.map(Analytics.find({
		'type': 'module',
		'startDate': {$gt: parseInt(start.format('x'))},
		'endDate': {$lte: parseInt(end.format('x'))},
		'condoId': {$in: condoIds},
	}).fetch(), function(moduleAnalytics) {
		return {
			module: moduleAnalytics.module, 
			submodule: moduleAnalytics.submodule, 
			date: moment(moduleAnalytics.startDate).format('YYYY'), 
			on: moduleAnalytics.loggedTime
		};
	})]);

	let label = [];
	let duration = end.diff(start, 'years');
	let year = start.clone();	
	for (var i = 0; i < duration; i++) {
		_.each(module, function (mod) {
			mod.content.push({ date: year.format('YYYY'), on: 0 });
		})
		year.add(1, "years");
	}
	year = start.clone();
	for (var i = 0; i <= duration; i++) {
		let analytics_of_year = _.filter(analytics, function(analytic) {
			return analytic.date == year.format('YYYY');
		})
		_.each(analytics_of_year, function(aOd) {
			if (aOd.module != 'messenger') {
				let tester = _.find(module[aOd.module].content, function(elem) { return elem.date == year.format('YYYY')});
				if (!tester)
					module[aOd.module].content.push({date: year.format('YYYY'), on: aOd.on});
				else {
					module[aOd.module].content[module[aOd.module].content.indexOf((tester))].on += aOd.on || 0;					
				}
			} else {
        let index = '';
        let role = DefaultRoles.findOne({ _id: aOd.submodule })
        if (role && role.name === 'Occupant') {
          index = "messengerO"
        } else if (role && role.name === 'Gardien') {
          index = "messengerG"
        } else if (role && role.name === 'Conseil Syndical') {
          index = "messengerCS"
        } else if (aOd.submodule == 'manager') {
          index = "messengerGe"
        }

				let tester = _.find(module[index].content, function(elem) { return elem.date == year.format('YYYY')});
				if (!tester)
					module[index].content.push({date: year.format('YYYY'), on: aOd.on});
				else {
					module[index].content[module[index].content.indexOf((tester))].on += aOd.on || 0;					
				}
			}
		});
		label.push(year.format('YYYY'));
		year.add(1, "years");
	}
	return {
		labels: label,
		data: module,
	};
}
function get_data_of_month(start, end, condoIds, module) {
	let data = [];
	let analytics = _.flatten([ _.map(Analytics.find({
		'type': 'module',
		'startDate': {$gt: parseInt(start.format('x'))},
		'endDate': {$lte: parseInt(end.format('x'))},
		'condoId': {$in: condoIds},
	}).fetch(), function(moduleAnalytics) {
		return {
			module: moduleAnalytics.module, 
			submodule: moduleAnalytics.submodule, 
			date: moment(moduleAnalytics.startDate).format('MM[/]YYYY'), 
			on: moduleAnalytics.loggedTime
		};
	})]);

	let label = [];
	let duration = end.diff(start, 'months');
	let month = start.clone();
	for (var i = 0; i < duration; i++) {
		_.each(module, function (mod) {
			mod.content.push({ date: month.format('MM[/]YYYY'), on: 0 });
		})
		month.add(1, 'months');
	}
	month = start.clone();
	for (var i = 0; i <= duration; i++) {
		let analytics_of_month = _.filter(analytics, function (analytic) {
			return analytic.date == month.format('MM[/]YYYY');
		})
		_.each(analytics_of_month, function (aOd) {
			let tester = [];
			if (aOd.module != 'messenger' && module[aOd.module] != undefined) {
				tester = _.find(module[aOd.module].content, function (elem) { return elem.date == month.format('MM[/]YYYY') });
				module[aOd.module].content[module[aOd.module].content.indexOf((tester))].on += (!!aOd && aOd.on) || 0;
			} else {
        let index = '';
        let role = DefaultRoles.findOne({ _id: aOd.submodule })
        if (role && role.name === 'Occupant') {
          index = "messengerO"
        } else if (role && role.name === 'Gardien') {
          index = "messengerG"
        } else if (role && role.name === 'Conseil Syndical') {
          index = "messengerCS"
        } else if (aOd.submodule == 'manager') {
          index = "messengerGe"
        }
				if (index != '' && module[index] != undefined) {
					tester = _.find(module[index].content, function (elem) { return elem.date == month.format('MM[/]YYYY') });
					module[index].content[module[index].content.indexOf((tester))].on += aOd.on || 0;
				}
			}
		});
		label.push(month.format('MMMM'));
		month.add(1, "months");
	}
	return {
		labels: label,
		data: module,
	};
}
function get_data_of_day(start, end, condoIds, module) {
	let data = [];
	let analytics = _.flatten([ _.map(Analytics.find({
		'type': 'module',
		'startDate': {$gt: parseInt(start.format('x'))},
		'endDate': {$lte: parseInt(end.format('x'))},
		'condoId': {$in: condoIds},
	}).fetch(), function(moduleAnalytics) {
		return {
			module: moduleAnalytics.module, 
			submodule: moduleAnalytics.submodule, 
			date: moment(moduleAnalytics.startDate).format('DD[/]MM[/]YYYY'), 
			on: moduleAnalytics.loggedTime
		};
	})]);

	let label = [];
	let duration = end.diff(start, 'days');
	let day = start.clone();
	for (var i = 0; i < duration; i++) {
		_.each(module, function (mod) {
			mod.content.push({date: day.format('DD[/]MM[/]YYYY'), on: 0});
		})
		day.add(1, 'days');
	}
	day = start.clone();
	for (var i = 0; i < duration; i++) {
		let analytics_of_day = _.filter(analytics, function(analytic) {
			return analytic.date == day.format('DD[/]MM[/]YYYY');
		})
		_.each(analytics_of_day, function(aOd) {
			let tester = [];
			if (aOd.module != 'messenger' && module[aOd.module] != undefined) {
				tester = _.find(module[aOd.module].content, function (elem) { return elem.date == day.format('DD[/]MM[/]YYYY') });
				module[aOd.module].content[module[aOd.module].content.indexOf((tester))].on += aOd.on || 0;					
			} else {
        let index = '';
        let role = DefaultRoles.findOne({ _id: aOd.submodule })
        if (role && role.name === 'Occupant') {
          index = "messengerO"
        } else if (role && role.name === 'Gardien') {
          index = "messengerG"
        } else if (role && role.name === 'Conseil Syndical') {
          index = "messengerCS"
        } else if (aOd.submodule == 'manager') {
          index = "messengerGe"
        }
				if (index != '' && module[index] != undefined) {
					tester = _.find(module[index].content, function (elem) { return elem.date == day.format('DD[/]MM[/]YYYY') });
					module[index].content[module[index].content.indexOf((tester))].on += aOd.on || 0;
				}
			}
		});
		label.push(day.format('ddd DD MMM'));
		day.add(1, "days");
	}

	return {
		labels: label,
		data: module,
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let duration = 0;
	let data = [];
	let dataset = [];
	let condoModule = {};

	let condos = Condos.find({'_id': {$in: condoIds}}).fetch();
	_.each(condos, function(condo) {
		if (!_.find(condoModule, function(cM) { return cM.module == "ads"})									&& condo.settings.options.classifieds == true)					condoModule.ads				= { displayName: 'Petites Annonces',			content: [], color: "#dc8086" };
		if (!_.find(condoModule, function(cM) { return cM.module == "concierge"})							&& condo.settings.options.conciergerie == true)					condoModule.concierge		= { displayName: 'Conciergerie', 				content: [], color: "#8e989f" };
		if (!_.find(condoModule, function(cM) { return cM.module == "forum"})								&& condo.settings.options.forum == true)						condoModule.forum			= {	displayName: 'Forum', 						content: [], color: "#fdb813" };
		if (!_.find(condoModule, function(cM) { return cM.module == "incident"})							&& condo.settings.options.incidents == true)					condoModule.incident		= {	displayName: 'Incidents', 					content: [], color: "#e4666b" };
		if (!_.find(condoModule, function(cM) { return cM.module == "info"})								&& condo.settings.options.informations == true)					condoModule.info			= { displayName: 'Information', 				content: [], color: "#d3a6ad" };
		if (!_.find(condoModule, function(cM) { return cM.module == "manual"})								&& condo.settings.options.manual == true)						condoModule.manual			= { displayName: 'Manuel', 						content: [], color: "#cb9ed6" };
		if (!_.find(condoModule, function(cM) { return cM.module == "map"})									&& condo.settings.options.map == true)							condoModule.map				= { displayName: 'Plan', 						content: [], color: "#cac0e2" };
		if (!_.find(condoModule, function(cM) { return cM.module == "messengerG"})							&& condo.settings.options.messengerGardien == true)				condoModule.messengerG		= {	displayName: 'Messagerie gardien', 			content: [], color: "#b8d2da" };
		if (!_.find(condoModule, function(cM) { return cM.module == "messengerCS"})							&& condo.settings.options.messengerSyndic == true)				condoModule.messengerCS		= { displayName: 'Messagerie conseil syndical', content: [], color: "#94bfcc" };
		if (!_.find(condoModule, function(cM) { return cM.module == "messengerGe"})							&& condo.settings.options.messengerGestionnaire == true)		condoModule.messengerGe		= { displayName: 'Messagerie gestionnaire', 	content: [], color: "#60a0b3" };
		if (!_.find(condoModule, function(cM) { return cM.module == "messengerO"})							&& condo.settings.options.messengerResident == true)			condoModule.messengerO		= {	displayName: 'Messagerie occupant', 		content: [], color: "#3994af" };
		if (!_.find(condoModule, function(cM) { return cM.module == "resa"})								&& condo.settings.options.reservations == true)					condoModule.resa			= { displayName: 'Reservation', 				content: [], color: "#a3b4bb" };
		if (!_.find(condoModule, function(cM) { return cM.module == "trombi"})								&& condo.settings.options.trombi == true)						condoModule.trombi			= { displayName: 'Trombinoscope', 				content: [], color: "#aee4ef" };
	});

	let bar_duration_module = [];
	if ((duration = end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds, condoModule);
	}
	else if ((duration = end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds, condoModule);
	}
	else {
		data = get_data_of_day(start, end, condoIds, condoModule);
	}

	_.each(data.data, function(test) {
		dataset.push({
			label: test.displayName,
			data: _.map(test.content, function(d) { return (d.on / 1000 / 60).toFixed(1); }),
			fill: true,
			backgroundColor: test.color,
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});
	});
	dataset.sort((a, b) => b.label.localeCompare(a.label));


	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	dataset.push({
		label: '',
		data: data.data,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end',
			formatter: function (value, context) {
				if (value >= 1440) {
					if (value % 1440 == 0)
						return parseInt(value / 1440) + 'j';
					else {
						if (value % 60 == 0) {
							return parseInt(value / 1440) + 'j';
						}
						return parseInt(value / 1440) + 'j ' + parseInt(value % 1440 / 60) + 'h';
					}
				}
				else if (value >= 60) {
					if (value % 60 == 0)
						return parseInt(value / 60) + 'h';
					else
						return parseInt(value / 60) + 'h' + parseInt(value % 60);
				}
				else if (value >= 1)
					return parseInt(value) + ' min';
			}
		}
	});
	
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_duration_module(start, end, option) {

	let options = set_options_of_chart(option);

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condoId) { return condoId._id});
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#bar_duration_module').remove();
	$('#bar_duration_module-container').append('<canvas id="bar_duration_module"><canvas>');
	let ctx = $("#bar_duration_module");
	let myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-duration_module-legend").html(myChart.generateLegend());
}

Template.bar_duration_module.onCreated(function() {
});

let myInterval;

Template.bar_duration_module.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {

			start     = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end       = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds  = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render    = true;
		}
		if (render) {
			render_bar_duration_module(start.clone(), end.clone(), template.data.option);
			render    = false;
		}
	}, 1000)
});


Template.bar_duration_module.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_duration_module.events({
});

Template.bar_duration_module.helpers({
});