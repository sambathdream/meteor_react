function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-number_connexion').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_connexion').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_connexion').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 10;
	options.scales.yAxes = [{
		id: 'A',
		type: 'linear',
		position: 'right',
		display: false,
		gridLines: {
			drawBorder: false,
			display: false
		},
		ticks: {
			beginAtZero: true
		}
	}, {
		id: 'B',
		type: 'linear',
		position: 'left',
		gridLines: {
			display: false,
			color: "#88898e",
			lineWidth: 2,
			zeroLineWidth: 2,
			zeroLineColor: "#88898e",
			drawTicks: true,
			tickMarkLength: 3
		},
		ticks: {
			callback: function (value) {
				if (value < 0) {
					return '(' + value * -1 + ')' + '%';
				}
				else if (value < 1) {
					return;
				}
				else {
					return value + '%';
				}
			},
			padding: 10,
			beginAtZero: true,
			min: 0,
			max: 100
		}
	}, {
		id: 'littlebar',
		display: false,
		stacked: true,
		ticks: {
			beginAtZero: true,
			max: 50,
			autoSkip: false,
		},
		type: 'linear',
		position: 'right',
		}];
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_connexion');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
	let data = [];
	let bending = [];

	let bar_number_connexion = [];
	duration = end.diff(start, 'years');


	_.each(
		_.map(Analytics.find({'type': 'login', "condoId": { $in: condoIds }}).fetch(), function (loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function (condoId) { return condoId == loginAnalytics.condoId; }))
				return {userId: loginAnalytics.userId, date: moment(loginAnalytics.loginDate).format('YYYY')};
			}
		)
	, function(elem) {
		if (_.find(temp, (item) => { return (item.userId == elem.userId && item.date == elem.date)}) == undefined)
			temp.push(elem)
	})
	dataAnalytics = _.pluck(temp, 'date')
	bar_number_connexion = _.countBy(_.flatten([dataAnalytics]));


	let year = start;
	let activeUserYear = moment(start).add(1, 'y');

	for (var i = 0; i <= duration; i++) {
		let activeUser = Residents.find({
			"condos.0": { $exists: true },
			$and: [
				{
					$or: [
						{"condos.joined": { $lt: new Date(parseInt(activeUserYear.format('x'))) }},
						{"condos.joined": { $lt: parseInt(activeUserYear.format('x')) }},
					]
				},
				{'condos.condoId': { $in: condoIds }}
			]
		}).fetch().length;

		value = bar_number_connexion[year.format('YYYY')];
		label.push(year.format("YYYY"));
		data.push(value || 0);
		bending.push( (( ( (value || 0) * 100) / (activeUser || 0) ) ).toFixed(1))
		year.add(1, "y");
		activeUserYear.add(1, "y");
	}
	
	return {
		labels: label,
		data: data,
		bending: bending
	};
}
function get_data_of_month(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
	let data = [];
	let bending = [];


	duration = end.diff(start, 'months');

	let temp = [];

	_.each(
		_.map(Analytics.find({'type': 'login', "condoId": { $in: condoIds }}).fetch(), function (loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function (condoId) { return condoId == loginAnalytics.condoId; }))
				return {userId: loginAnalytics.userId, date: moment(loginAnalytics.loginDate).format('MM[/]YYYY')};
			}
		)
	, function(elem) {
		if (_.find(temp, (item) => { return (item.userId == elem.userId && item.date == elem.date)}) == undefined)
			temp.push(elem)
	})
	dataAnalytics = _.pluck(temp, 'date')



	bar_number_connexion = _.countBy(_.flatten([dataAnalytics]));
	
	let month = start.startOf('month')
	let activeUserMonth = moment(start.startOf('month')).add(1, 'M');

	for (var i = 0; i <= duration; i++) {

		let activeUser = Residents.find({
			"condos.0": { $exists: true },
			$and: [
				{
					$or: [
						{"condos.joined": { $lt: new Date(parseInt(activeUserMonth.format('x'))) }},
						{"condos.joined": { $lt: parseInt(activeUserMonth.format('x')) }},
					]
				},
				{'condos.condoId': { $in: condoIds }}
			]
		}).fetch().length;




		value = bar_number_connexion[month.format('MM[/]YYYY')];

		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data.push(value || 0);
		bending.push( (( ( (value || 0) * 100) / (activeUser || 0) ) ).toFixed(1))
		month.add(1, "M");
		activeUserMonth.add(1, "M");

	}
	
	return {
		labels: label,
		data: data,
		bending: bending
	};
}
function get_data_of_day(start, end, condoIds, oldUser) {
	
	let duration = 0;
	let label = [];
	let data = [];
	let bending = [];
	
	
	let temp = [];

	_.each(
		_.map(Analytics.find({'type': 'login', "condoId": { $in: condoIds }}).fetch(), function (loginAnalytics) {
			if (condoIds.length == 0 || _.find(condoIds, function (condoId) { return condoId == loginAnalytics.condoId; }))
				return {userId: loginAnalytics.userId, date: moment(loginAnalytics.loginDate).format('DD[/]MM[/]YY')};
			}
		)
	, function(elem) {
		if (_.find(temp, (item) => { return (item.userId == elem.userId && item.date == elem.date)}) == undefined)
			temp.push(elem)
	})
	dataAnalytics = _.pluck(temp, 'date')
	bar_number_connexion = _.countBy(_.flatten([dataAnalytics]));

	duration = end.diff(start, 'days');
	let day = start;
	let activeUserDay = moment(start).add(1, 'day');
	for (var i = 0; i < duration; i++) {
		let activeUser = Residents.find({
			"condos.0": { $exists: true },
			$and: [
				{
					$or: [
						{"condos.joined": { $lt: new Date(parseInt(activeUserDay.format('x'))) }},
						{"condos.joined": { $lt: parseInt(activeUserDay.format('x')) }},
					]
				},
				{'condos.condoId': { $in: condoIds }}
			]
		}).fetch().length;

		value = bar_number_connexion[day.format('DD[/]MM[/]YY')];


		label.push(day.format("ddd DD MMM"));
		data.push(Math.trunc(value) || 0);

		let tempBending = (( ( (value || 0) * 100) / (activeUser || 0) ) ).toFixed(1);
		bending.push(tempBending)

		day.add(1, "d");
		activeUserDay.add(1, "d");
	}
	
	return {
		labels: label,
		data: data,
		bending: bending
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: '% utilisateurs connectés vs. active users',
		yAxisID: 'B',
		data: data.bending,
		fill: false,
		lineTension: 0.4,
		backgroundColor: "#8b999f",
		borderColor: "#8b999f",
		borderCapStyle: 'square',
		borderDashOffset: 0.0,
		borderJoinStyle: 'miter',
		pointRadius: 4,
		pointBorderColor: "#8b999f",
		pointBackgroundColor: "#8b999f",
		datalabels: {
			display: false,
		},
		type: 'line',
	});

	dataset.push({
		label: '# utilisateurs connectés',
		yAxisID: 'A',
		data: data.data,
		datalabels: {
			display: false,
		},
		fill: true,
		backgroundColor: "#69d2e7",
		borderColor: "white",
	});

	dataset.push({
		label: '',
		data: data.data,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});
	
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_number_connexion(start, end, option) {
	
	let options = set_options_of_chart(option)
	
	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}
	
	let data = build_datasets_of_chart(start, end, condoIds)
	
	$('#bar_number_connexion').remove();
	$('#bar_number_connexion-container').append('<canvas id="bar_number_connexion"><canvas>');
	let ctx = $('#bar_number_connexion');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_connexion-legend").html(myChart.generateLegend());
}



Template.bar_number_connexion.onCreated(function() {
});

let myInterval;

Template.bar_number_connexion.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
		(!render && condoIds != Session.get('condoIds').length) ||
		(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_number_connexion(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_number_connexion.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_number_connexion.events({
});

Template.bar_number_connexion.helpers({
});