function calculate_pie_state_user(startDate, endDate, option) {
	 
	let options = {};

	options.plugins = {
		datalabels: {
			display: function(context) {
				return context.dataset.data[context.dataIndex] > 0;
			},
			color: "white",
			font: {
				weight: 'bold'
			},
			// display: false
		}
	};

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condo) {return condo._id});
	}

	let residentIdInCondo = _.map(Residents.find({
		'condos.condoId': {$in: condoIds}
	}).fetch(), function(elem) {return elem.userId});
	let residentIdInPending = _.map(Residents.find({
		'pendings.condoId': {$in: condoIds}
	}).fetch(), function(elem) {return elem.userId});
	let nbrUserWithoutPassword = _.map(Meteor.users.find({
		"services.password": {$exists: false},
		$or: [
			{"_id": {$in: residentIdInCondo}},
			{"_id": {$in: residentIdInPending}},
		],
	}).fetch(), function(user) {return user._id});
	let nbrUserJoined = Residents.find({
		"condos.0": {$exists: true},
		$and: [
			{"userId": {$nin: nbrUserWithoutPassword}},
			{"userId": {$in: residentIdInCondo}},
		]
	}).fetch();
	let nbrUserWithoutCondo = Residents.find({
		"validation.isValidate": false,
		$and: [
			{"userId": {$nin: nbrUserWithoutPassword}},
			{"userId": {$in: residentIdInPending}},
		]
	}).fetch();

	$('#pie_state_user').remove();
	$('#pie_state_user-container').append('<canvas id="pie_state_user"><canvas>');
	let ctx = $("#pie_state_user");
	let myChart = new Chart(ctx, {
		type: 'doughnut',
		data: {
			datasets: [{
				data: [
					Math.trunc(nbrUserJoined.length),
					Math.trunc(nbrUserWithoutPassword.length),
					Math.trunc(nbrUserWithoutCondo.length)
				],
				backgroundColor: [
					"rgba(105, 210, 239, 0.8)",
					"rgba(253, 184,  29, 0.8)",
					"rgba(255, 69, 92, 0.8)",
				],
				hoverBackgroundColor: [
					"rgba(105, 210, 239, 1)",
					"rgba(253, 184,  29, 1)",
					"rgba(255, 69, 92, 1)",
				],
				hoverBorderColor: [
					"rgba(105, 210, 239, 0.8)",
					"rgba(253, 184,  29, 0.8)",
					"rgba(255, 69, 92, 0.8)",
				]
			}],
			labels: [
				'Active users',
				'Users w/o password',
				'Users to be validated'
			],
		},
		options: options
	});
	$("#chartjs-state_user-legend").html(myChart.generateLegend());
}


Template.pie_state_user.onCreated(function() {
});

let myInterval;

Template.pie_state_user.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			calculate_pie_state_user(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.pie_state_user.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.pie_state_user.events({
});

Template.pie_state_user.helpers({
});