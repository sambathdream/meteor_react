function set_options_of_chart(option) {
	 
	let options = option;
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-duration_creation_validation').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-duration_creation_validation').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-duration_creation_validation').css("opacity", 1);
		}, 5);
	};
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-duration_creation_validation');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(': ')[1]) {
						let value = parseInt(body[0].split(': ')[1]);
						if (parseInt(value) >= 1440) {
							if (parseInt(value) % 1440 == 0)
								value = parseInt(parseInt(value) / 1440) + 'j';
							else {
								if (parseInt(value) % 60 == 0) {
									value = parseInt(parseInt(value) / 1440) + 'j';
								}
								value = parseInt(parseInt(value) / 1440) + 'j ' + parseInt(parseInt(value) % 1440 / 60) + 'h';
							}
						}
						else if (parseInt(value) >= 60) {
							if (parseInt(value) % 60 == 0)
								value = parseInt(parseInt(value) / 60) + 'h';
							else
								value = parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
						}
						else if (parseInt(value) >= 1)
							value = parseInt(value) + ' min';
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds) {
	let bar_duration_creation_validation = _.flatten([_.map(Residents.find({
		"condos.0": {$exists: true},
		"condos.condoId": {$in: condoIds},
	}).fetch(), function(user) {
		return _.map(user.condos, function(condo) {
			return {month: moment(condo.joined).format('YYYY'), time: moment(condo.joined).diff(condo.invited).valueOf()};
		})
	})]);

	let data = [];
	let label = [];

	let duration = end.diff(start, 'years')
	let year = start; 
	for (var i = 0; i <= duration; i++) {
		let total = 0;
		let count = 0;
		_.each(_.filter(bar_duration_creation_validation, function(elem) {return elem.year == year.format('YYYY')}), function(elem) {
			total += elem.time;
			count++;
		});
		data.push(((total / count) / 1000 / 60 / 60 / 24).toFixed(1) || 0);
		label.push(year.format("YYYY"));
		year.add(1, "y");
	}
	return {
		labels: label,
		data: data,
	};
}
function get_data_of_month(start, end, condoIds) {
	let bar_duration_creation_validation = _.flatten([_.map(Residents.find({
		"condos.0": {$exists: true},
		"condos.condoId": {$in: condoIds},
	}).fetch(), function(user) {
		return _.map(user.condos, function(condo) {
			return {month: moment(condo.joined).format('MM[/]YYYY'), time: moment(condo.joined).diff(condo.invited).valueOf()};
		})
	})]);

	let data = [];
	let label = [];

	let duration = end.diff(start, 'months');
	let month = start;
	for (var i = 0; i <= duration; i++) {
		let total = 0;
		let count = 0;
		_.each(_.filter(bar_duration_creation_validation, function(elem) {return elem.month == month.format('MM[/]YYYY')}), function(elem) {
			total += elem.time;
			count++;
		});
		data.push(((total / count) / 1000 / 60 / 60 / 24).toFixed(1) || 0);
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		month.add(1, "M");
	}
	return {
		labels: label,
		data: data,
	};
}
function get_data_of_day(start, end, condoIds) {
	let bar_duration_creation_validation = _.flatten([_.map(Residents.find({
		"condos.0": {$exists: true},
		"condos.condoId": {$in: condoIds},
	}).fetch(), function(user) {
		return _.map(user.condos, function(condo) {
			return {day: moment(condo.joined).format('DD[/]MM[/]YY'), time: moment(condo.joined).diff(condo.invited).valueOf()};
		})
	})]);

	let data = [];
	let label = [];

	let duration = end.diff(start, 'days');
	let day = start;
	for (var i = 0; i < duration; i++) {
		let total = 0;
		let count = 0;
		_.each(_.filter(bar_duration_creation_validation, function(elem) {return elem.day == day.format('DD[/]MM[/]YY')}), function(elem) {
			total += elem.time;
			count++;
		});
		data.push((((total / count) / 1000 / 60 / 60 / 24).toFixed(1)) || 0);
		label.push(day.format("ddd DD MMM"));
		day.add(1, "d");
	}
	return {
		labels: label,
		data: data,
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let duration = 0;
	let data = [];
	let dataset = [];

	let bar_duration_connexion = [];
	if ((duration = end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((duration = end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: 'Durée moyenne entre création et validation',
		data: data.data,
		fill: true,
		backgroundColor: "#32a565",
		borderColor: "#32a565",
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '',
		data: data.data,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end',
			formatter: function (value, context) {
				if (value >= 1440) {
					if (value % 1440 == 0)
						return parseInt(value / 1440) + 'j';
					else {
						if (value % 60 == 0) {
							return parseInt(value / 1440) + 'j';
						}
						return parseInt(value / 1440) + 'j ' + parseInt(value % 1440 / 60) + 'h';
					}
				}
				else if (value >= 60) {
					if (value % 60 == 0)
						return parseInt(value / 60) + 'h';
					else
						return parseInt(value / 60) + 'h' + parseInt(value % 60);
				}
				else if (value >= 1)
					return parseInt(value) + ' min';
			}
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_duration_creation_validation(start, end, option) {

	let options = set_options_of_chart(option);

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condoId) {return condoId._id});
	}

	let data = build_datasets_of_chart(start, end, condoIds);

	$('#bar_duration_creation_validation').remove();
	$('#bar_duration_creation_validation-container').append('<canvas id="bar_duration_creation_validation"><canvas>');
	let ctx = $("#bar_duration_creation_validation");
	let myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-duration_creation_validation-legend").html(myChart.generateLegend());
}


Template.bar_duration_creation_validation.onCreated(function() {
});

let myInterval;

Template.bar_duration_creation_validation.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_duration_creation_validation(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_duration_creation_validation.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_duration_creation_validation.events({
});

Template.bar_duration_creation_validation.helpers({
});