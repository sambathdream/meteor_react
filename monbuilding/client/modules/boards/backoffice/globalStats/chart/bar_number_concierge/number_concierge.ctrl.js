function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-number_concierge').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-number_concierge').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_concierge').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_concierge');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds) {
	let duration = 0;
	let label = [];
	let data_urgent = [];
	let data_non_urgent = [];
	let data_devis = [];
	let data_services = [];
	let data_param1 = [];
	let data_param2 = [];


	duration = end.diff(start, 'years');
	bar_number_concierge_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "service récurrent",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY');
	})]));

	bar_number_concierge_non_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "demande de devis",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY');
	})]));

	bar_number_concierge_devis = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention non urgente",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY');
	})]));

	bar_number_concierge_services = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention en urgence",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY');
	})]));

	bar_number_concierge_param1 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Travaux",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY')
	})]));

	bar_number_concierge_param2 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Services à domicile",
	}).fetch(), function (item) {
		return moment(item.date).format('YYYY')
	})]));

	let year = start;
	for (var i = 0; i <= duration; i++) {
		label.push(year.format("YYYY"));
		data_urgent.push(bar_number_concierge_urgent[year.format('YYYY')] || 0);
		data_non_urgent.push(bar_number_concierge_non_urgent[year.format('YYYY')] || 0);
		data_devis.push(bar_number_concierge_devis[year.format('YYYY')] || 0);
		data_services.push(bar_number_concierge_services[year.format('YYYY')] || 0);
		data_param1.push(bar_number_concierge_param1[year.format('YYYY')] || 0);
		data_param2.push(bar_number_concierge_param2[year.format('YYYY')] || 0);
		year.add(1, "y");
	}

	return {
		labels: label,
		urgent: data_urgent,
		non_urgent: data_non_urgent,
		devis: data_devis,
		services: data_services,
		param1: data_param1,
		param2: data_param2
	};
}
function get_data_of_month(start, end, condoIds) {

	let duration = 0;
	let label = [];
	let data_urgent = [];
	let data_non_urgent = [];
	let data_devis = [];
	let data_services = [];
	let data_param1 = [];
	let data_param2 = [];


	duration = end.diff(start, 'months');
	bar_number_concierge_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention en urgence",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY');
	})]));

	bar_number_concierge_non_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention non urgente",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY');
	})]));

	bar_number_concierge_devis = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "demande de devis",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY');
	})]));

	bar_number_concierge_services = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "service récurrent",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY');
	})]));

	bar_number_concierge_param1 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Travaux",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY')
	})]));

	bar_number_concierge_param2 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Services à domicile",
	}).fetch(), function (item) {
		return moment(item.date).format('MM[/]YYYY')
	})]));

	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_urgent.push(bar_number_concierge_urgent[month.format('MM[/]YYYY')] || 0);
		data_non_urgent.push(bar_number_concierge_non_urgent[month.format('MM[/]YYYY')] || 0);
		data_devis.push(bar_number_concierge_devis[month.format('MM[/]YYYY')] || 0);
		data_services.push(bar_number_concierge_services[month.format('MM[/]YYYY')] || 0);
		data_param1.push(bar_number_concierge_param1[month.format('MM[/]YYYY')] || 0);
		data_param2.push(bar_number_concierge_param2[month.format('MM[/]YYYY')] || 0);
		month.add(1, "M");
	}

	return {
		labels: label,
		urgent: data_urgent,
		non_urgent: data_non_urgent,
		devis: data_devis,
		services: data_services,
		param1: data_param1,
		param2: data_param2
	};
}
function get_data_of_day(start, end, condoIds) {

	let duration = 0;
	let label = [];
	let data_urgent = [];
	let data_non_urgent = [];
	let data_devis = [];
	let data_services = [];
	let data_param1 = [];
	let data_param2 = [];


	bar_number_concierge_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention en urgence",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_concierge_non_urgent = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "intervention non urgente",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_concierge_devis = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "demande de devis",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_concierge_services = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"category": "service récurrent",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_concierge_param1 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Travaux",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	bar_number_concierge_param2 = _.countBy(_.flatten([_.map(ConciergeMessage.find({
		"date": { $gte: start.valueOf() },
		"date": { $lt: end.valueOf() },
		"condoId": { $in: condoIds },
		"param1": "Services à domicile",
	}).fetch(), function (item) {
		return moment(item.date).format('DD[/]MM[/]YY')
	})]));

	duration = end.diff(start, 'days');
	let day = start;
	for (var i = 0; i < duration; i++) {
		label.push(day.format("ddd DD MMM"));
		data_urgent.push(bar_number_concierge_urgent[day.format('DD[/]MM[/]YY')] || 0);
		data_non_urgent.push(bar_number_concierge_non_urgent[day.format('DD[/]MM[/]YY')] || 0);
		data_devis.push(bar_number_concierge_devis[day.format('DD[/]MM[/]YY')] || 0);
		data_services.push(bar_number_concierge_services[day.format('DD[/]MM[/]YY')] || 0);
		data_param1.push(bar_number_concierge_param1[day.format('DD[/]MM[/]YY')] || 0);
		data_param2.push(bar_number_concierge_param2[day.format('DD[/]MM[/]YY')] || 0);
		day.add(1, "d");
	}

	return {
		labels: label,
		urgent: data_urgent,
		non_urgent: data_non_urgent,
		devis: data_devis,
		services: data_services,
		param1: data_param1,
		param2: data_param2
	};
}

function build_datasets_of_chart(start, end, condoIds, select) {
	let data = [];
	let dataset = [];

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	if (select) {
		dataset.push({
			label: 'Urgente',
			data: data.urgent,
			fill: true,
			backgroundColor: "#3894af",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: 'Non Urgente',
			data: data.non_urgent,
			fill: true,
			backgroundColor: "#e4666b",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: 'Devis',
			data: data.devis,
			fill: true,
			backgroundColor: "#fdb812",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: 'Service récurrent',
			data: data.services,
			fill: true,
			backgroundColor: "#7abe7c",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: '',
			data: _.map(data.labels, function (elem, index) { return data.urgent[index] + data.non_urgent[index] + data.devis[index] + data.services[index] }),
			lineTension: 0,
			backgroundColor: "transparent",
			borderColor: "transparent",
			pointBorderColor: "transparent",
			pointBackgroundColor: "transparent",
			pointHoverBackgroundColor: "transparent",
			pointHoverBorderColor: "transparent",
			type: 'line',
			datalabels: {
				color: '#000000',
				align: 'end'
			}
		});
	}
	else {
		dataset.push({
			label: 'Travaux',
			data: data.param1,
			fill: true,
			backgroundColor: "#5a0c51",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: 'Services à domicile',
			data: data.param2,
			fill: true,
			backgroundColor: "#a24d9a",
			borderColor: "white",
			borderWidth: 1,
			datalabels: {
				display: false,
			}
		});

		dataset.push({
			label: '',
			data: _.map(data.labels, function (elem, index) { return data.param1[index] + data.param2[index] }),
			lineTension: 0,
			backgroundColor: "transparent",
			borderColor: "transparent",
			pointBorderColor: "transparent",
			pointBackgroundColor: "transparent",
			pointHoverBackgroundColor: "transparent",
			pointHoverBorderColor: "transparent",
			type: 'line',
			datalabels: {
				color: '#000000',
				align: 'end'
			}
		});
	}

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_number_concierge(start, end, option, select) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds, select)

	$('#bar_number_concierge').remove();
	$('#bar_number_concierge-container').append('<canvas id="bar_number_concierge"><canvas>');
	let ctx = $("#bar_number_concierge");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_concierge-legend").html(myChart.generateLegend());
}


Template.bar_number_concierge.onCreated(function() {
	this.select = new ReactiveVar(1);
});

let myInterval;

Template.bar_number_concierge.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_number_concierge(start.clone(), end.clone(), template.data.option, template.select.get());
			render = false;
		}
	}, 1000)
});


Template.bar_number_concierge.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_number_concierge.events({
	'change #typeConcierge': (event, template) => {
		template.select.set(parseInt(event.currentTarget.value));
		let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
		let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
		render_bar_number_concierge(start.clone(), end.clone(), template.data.option, template.select.get());
	}
});

Template.bar_number_concierge.helpers({
});