function set_options_of_chart(option) {
	 	
	let options = option;
	
	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-join_and_invit_user').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-join_and_invit_user').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-join_and_invit_user').css("opacity", 1);
		}, 5);
	};
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-join_and_invit_user');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds) {
	let bar_join_user = _.countBy(_.flatten([ _.map(Residents.find({
		$and: [
			{"condos.0": {$exists: true}},
			{"condos.joined": {$gte: new Date(start)}},
			{"condos.joined": {$lt: new Date(end)}},
			{"condos.condoId": {$in: condoIds}},
			{"validation.isValidate": {$exists: true}},
		]
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.joined).format('YYYY');
		});
	})]));
	
	let bar_invit_user = _.countBy(_.flatten([ _.map(Residents.find({
		$and: [{
			$or: [
				{"condos.invitByAdmin": true},
				{"condos.invitByGestionnaire": true},
				{"pendings.invitByAdmin": true},
				{"pendings.invitByGestionnaire": true},
			]
		},
		{
			"condos.condoId": {$in: condoIds}
		}]
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.invited).format('YYYY');
		});
	})]));
	
	let label = [];
	let value_join = null;
	let value_invit = null;
	let data_join = [];
	let data_invit = [];
	let duration = end.diff(start, 'years');
	let year = start;
	for (var i = 0; i <= duration; i++) {
		label.push(year.format("YYYY"));
		value_join = bar_join_user[year.format('YYYY')];
		value_invit = bar_invit_user[year.format('YYYY')];
		data_join.push(value_join || 0);
		data_invit.push(value_join || 0);
		year.add(1, "y");
	}
	
	return {
		labels: label,
		data_join: data_join,
		data_invit: data_invit
	};
}
function get_data_of_month(start, end, condoIds) {
	let bar_join_user = _.countBy(_.flatten([ _.map(Residents.find({
		"condos.0": {$exists: true},
		"condos.joined": {$gte: new Date(start)},
		"condos.joined": {$lt: new Date(end)},
		"condos.condoId": {$in: condoIds},
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.joined).format('MM[/]YYYY');
		});
	})]));
	
	let bar_invit_user = _.countBy(_.flatten([ _.map(Residents.find({
		$and: [{
			$or: [
				{"condos.invitByAdmin": true},
				{"condos.invitByGestionnaire": true},
				{"pendings.invitByAdmin": true},
				{"pendings.invitByGestionnaire": true},
			]
		},{
			$or: [
				{"condos.condoId": {$in: condoIds}},
				{"pendings.condoId": {$in: condoIds}},
			]
		}]
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.invited).format('MM[/]YYYY');
		});
	})]));
	
	let label = [];
	let value_join = null;
	let value_invit = null;
	let data_join = [];
	let data_invit = [];
	let duration = end.diff(start, 'months');
	let month = start.startOf('month') 
	for (var i = 0; i <= duration; i++) {
		value_join = bar_join_user[month.format('MM[/]YYYY')];
		value_invit = bar_invit_user[month.format('MM[/]YYYY')];
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_join.push(value_join || 0);
		data_invit.push(value_join || 0);
		month.add(1, "M");
	}
	
	return {
		labels: label,
		data_join: data_join,
		data_invit: data_invit
	};
}
function get_data_of_day(start, end, condoIds) {
	let bar_join_user = _.countBy(_.flatten([ _.map(Residents.find({
		"condos.0": {$exists: true},
		"condos.joined": {$gte: new Date(start)},
		"condos.joined": {$lt: new Date(end)},
		"condos.condoId": {$in: condoIds},
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.joined).format('DD[/]MM[/]YY')
		});
	})]));
	
	let bar_invit_user = _.countBy(_.flatten([ _.map(Residents.find({
		$and: [{
			$or: [
				{"condos.invitByAdmin": true},
				{"condos.invitByGestionnaire": true},
				{"pendings.invitByAdmin": true},
				{"pendings.invitByGestionnaire": true},
			]
		},{
			$or: [
				{"condos.condoId": {$in: condoIds}},
				{"pendings.condoId": {$in: condoIds}},
			]
		}]
	}).fetch(), function(_user) {
		return _.map(_user.condos, function(cActive) {
			if (condoIds.length == 0 || _.find(condoIds, function(condoId) { return condoId == cActive.condoId; }))
			return moment(cActive.invited).format('DD[/]MM[/]YY')
		});
	})]));
	
	let label = [];
	let value_join = null;
	let value_invit = null;
	let data_join = [];
	let data_invit = [];
	let duration = end.diff(start, 'days');
	let day;
	for (var i = 0; i < duration; i++) {
		day = start;
		value_join = bar_join_user[day.format('DD[/]MM[/]YY')];
		value_invit = bar_invit_user[day.format('DD[/]MM[/]YY')];
		label.push(day.format("ddd DD MMM"));
		data_join.push(Math.trunc(value_join) || 0);
		data_invit.push(Math.trunc(value_invit) || 0);
		day.add(1, "d");
	}
	
	return {
		labels: label,
		data_join: data_join,
		data_invit: data_invit
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	let bar_duration_connexion = [];
	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: 'Nombre d\'utilisateur validé',
		data:  data.data_join,
		backgroundColor: "rgba(139, 153, 159, 0.7)",
		borderColor: "white",
		datalabels: {
			display: false,
		}
	});
	dataset.push({
		label: 'Nombre d\'utilisateur invité',
		data:  data.data_invit,
		backgroundColor: "rgba(255, 71, 82, 0.7)",
		borderColor: "white",
		datalabels: {
			display: false,
		}
	});
	dataset.push({
		label: '',
		data: _.map(data.labels, function(DONT_USE, index) {return data.data_join[index] + data.data_invit[index]}),
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_join_and_invit_user(start, end, option) {
		
	let options = set_options_of_chart(option)
	
	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condo) {return condo._id});
	}
	
	let data = build_datasets_of_chart(start, end, condoIds)
	
	$('#bar_join_and_invit_user').remove();
	$('#bar_join_and_invit_user-container').append('<canvas id="bar_join_and_invit_user"><canvas>');
	let ctx = $("#bar_join_and_invit_user");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-join_and_invit_user-legend").html(myChart.generateLegend());
}


Template.bar_join_and_invit_user.onCreated(function() {
});

let myInterval;

Template.bar_join_and_invit_user.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
				(!render && condoIds != Session.get('condoIds').length) ||
				(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_join_and_invit_user(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_join_and_invit_user.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_join_and_invit_user.events({
});

Template.bar_join_and_invit_user.helpers({
});