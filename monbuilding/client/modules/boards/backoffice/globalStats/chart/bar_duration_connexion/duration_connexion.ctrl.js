function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function() {
			$('#x-axis-arrow-duration_connexion').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-duration_connexion').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-duration_connexion').css("opacity", 1);
		}, 5);
	}
	options.scales.yAxes[0].ticks = {
		padding: 10,
		beginAtZero: true,
		fontColor: "#88898e",
		callback:function(value, index, values) {
			if (value >= 1440) {
				if (value % 1440 == 0)
					return parseInt(value / 1440) + 'j';
				else {
					if (value % 60 == 0) {
						return parseInt(value / 1440) + 'j';
					}
					return parseInt(value / 1440) + 'j ' + parseInt(value % 1440 / 60) + 'h';
				}
			}
			else if (value >= 60) {
				if (value % 60 == 0)
					return parseInt(value / 60) + 'h';
				else
					return parseInt(value / 60) + 'h' + parseInt(value % 60);
			}
			else if (value >= 1)
				return parseInt(value) + ' min';
		}
	};	
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-duration_connexion');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(': ')[1]) {
						let value = parseInt(body[0].split(': ')[1]);
						if (parseInt(value) >= 1440) {
							if (parseInt(value) % 1440 == 0)
								value = parseInt(parseInt(value) / 1440) + 'j';
							else {
								if (parseInt(value) % 60 == 0) {
									value = parseInt(parseInt(value) / 1440) + 'j';
								}
								value = parseInt(parseInt(value) / 1440) + 'j ' + parseInt(parseInt(value) % 1440 / 60) + 'h';
							}
						}
						else if (parseInt(value) >= 60) {
							if (parseInt(value) % 60 == 0)
								value = parseInt(parseInt(value) / 60) + 'h';
							else
								value = parseInt(parseInt(value) / 60) + 'h' + parseInt(value) % 60;
						}
						else if (parseInt(value) >= 1)
							value = parseInt(value) + ' min';
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(': ')[0] + '<br>' + value + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds) {
	let data = [];
	let bar_duration_connexion = _.flatten([ _.map(Analytics.find({
		'type': 'login',
		'condoId': {$in: condoIds},
	}).fetch(), function(loginAnalytics) {
		return {date: moment(loginAnalytics.loginDate).format('YYYY'), on: (((loginAnalytics.logStatus.loggedTime) / 1000) / 60)};
	})]);

	bar_duration_connexion = _.map(_.chain(bar_duration_connexion).groupBy('date')._wrapped, function(nbr) {
		let moyenne_on = 0;
		_.each(nbr, function(e) {
			moyenne_on += e.on;
		});
		moyenne_on = moyenne_on / nbr.length
		return {
			date: nbr[0].date, 
			on: moyenne_on,
		}
	});

	let duration = end.diff(start, 'years')
	let year = start; 
	for (var i = 0; i <= duration; i++) {
		value = _.find(bar_duration_connexion, function(res) { return res.date == year.format('YYYY') });
		data.push({date: year.format('YYYY'), on: value ? (value.on).toFixed(1) : 0});
		year.add(1, "y");
	}

	return {
		labels: _.map(data, function(d) { return d.date; }),
		data: _.map(data, function(d) { return d.on; }),
	};
}
function get_data_of_month(start, end, condoIds) {
	let data = [];
	let bar_duration_connexion = _.flatten([ _.map(Analytics.find({
		'type': 'login',
		'condoId': {$in: condoIds},
	}).fetch(), function(loginAnalytics) {
		return {date: moment(loginAnalytics.loginDate).format('MM[/]YYYY'), on: (((loginAnalytics.logStatus.loggedTime) / 1000) / 60)};
	})]);

	bar_duration_connexion = _.map(_.chain(bar_duration_connexion).groupBy('date')._wrapped, function(nbr) {
		let moyenne_on = 0;
		_.each(nbr, function(e) {
			moyenne_on += e.on;
		});
		moyenne_on = moyenne_on / nbr.length
		return {
			date: nbr[0].date, 
			on: moyenne_on,
		}
	});

	let duration = end.diff(start, 'months')
	let month = start.startOf('month')
	for (var i = 0; i <= duration; i++) {
		value = _.find(bar_duration_connexion, function(res) { return res.date == month.format('MM[/]YYYY') });
		data.push({date: (moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY")), on: value ? (value.on).toFixed(1) : 0});
		month.add(1, "M");
	}

	return {
		labels: _.map(data, function(d) { return d.date; }),
		data: _.map(data, function(d) { return d.on; }),
	};
}
function get_data_of_day(start, end, condoIds) {
	let data = [];
	let bar_duration_connexion = _.flatten([ _.map(Analytics.find({
		'type': 'login',
		'condoId': {$in: condoIds},
	}).fetch(), function(loginAnalytics) {
		return {date: moment(loginAnalytics.loginDate).format('DD[/]MM[/]YY'), on: (((loginAnalytics.logStatus.loggedTime) / 1000) / 60)};
	})]);

	bar_duration_connexion = _.map(_.chain(bar_duration_connexion).groupBy('date')._wrapped, function(nbr) {
		let moyenne_on = 0;
		_.each(nbr, function(e) {
			moyenne_on += e.on;
		});
		moyenne_on = moyenne_on / nbr.length
		return {
			date: nbr[0].date, 
			on: moyenne_on,
		}
	});

	duration = end.diff(start, 'days');
	let day = start;
	for (var i = 0; i < duration; i++) {
		value = _.find(bar_duration_connexion, function(res) { return res.date == day.format('DD[/]MM[/]YY') });
		data.push({date: day.format("ddd DD MMM"), on: value ? (value.on).toFixed(1) : 0});
		day.add(1, "d");
	}
	return {
		labels: _.map(data, function(d) { return d.date; }),
		data: _.map(data, function(d) { return d.on; }),
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let duration = 0;
	let data = [];
	let dataset = [];

	let bar_duration_connexion = [];
	if ((duration = end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds);
	}
	else if ((duration = end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds);
	}
	else {
		data = get_data_of_day(start, end, condoIds);
	}

	dataset.push({
		label: 'Durée moyenne de connexion',
		data: data.data,
		fill: true,
		backgroundColor: "#aee4ef",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '',
		data: data.data,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end',
			formatter: function (value, context) {
				if (value >= 1440) {
					if (value % 1440 == 0)
						return parseInt(value / 1440) + 'j';
					else {
						if (value % 60 == 0) {
							return parseInt(value / 1440) + 'j';
						}
						return parseInt(value / 1440) + 'j ' + parseInt(value % 1440 / 60) + 'h';
					}
				}
				else if (value >= 60) {
					if (value % 60 == 0)
						return parseInt(value / 60) + 'h';
					else
						return parseInt(value / 60) + 'h' + parseInt(value % 60);
				}
				else if (value >= 1)
					return parseInt(value) + ' min';
				else 
					return '< 1 min';
			}
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function(col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});
	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_duration_connexion(start, end, option) {

	 

	let options = set_options_of_chart(option);

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function(condo) {return condo._id});
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#bar_duration_connexion').remove();
	$('#bar_duration_connexion-container').append('<canvas id="bar_duration_connexion"><canvas>');
	let ctx = $("#bar_duration_connexion");
	let myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-duration_connexion-legend").html(myChart.generateLegend());
}

Template.bar_duration_connexion.onCreated(function() {
});

let myInterval;

Template.bar_duration_connexion.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_duration_connexion(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_duration_connexion.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_duration_connexion.events({
});

Template.bar_duration_connexion.helpers({
});