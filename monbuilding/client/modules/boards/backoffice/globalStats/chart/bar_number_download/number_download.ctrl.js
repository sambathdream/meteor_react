function set_options_of_chart(option) {
	 
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-number_download').css("top", (axis.top + 108) + "px");
			$('#x-axis-arrow-number_download').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-number_download').css("opacity", 1);
		}, 5);
	};
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-number_download');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 99) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_data_of_year(start, end, condoIds, oldData) {

	let duration = 0;
	let label = [];

	let data_apple = [];
	let data_play_store = [];

	let bar_number_download_apple = [];
	let bar_number_download_play_store = [];

	let playStoreDownloadLength = oldData.playStoreNumberDownload;
	let appleStoreDownloadLength = oldData.appleStoreNumberDownload;

	duration = end.diff(start, 'years')

	bar_number_download_apple = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('YYYY'), y: item.count };
	})]);

	bar_number_download_play_store = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('YYYY'), y: item.count };
	})]);

	let year = start;
	for (var i = 0; i <= duration; i++) {
		label.push(year.format("YYYY"));

		if (day.diff(moment().endOf("year"), 'hours') < 0) {
			if (_.find(bar_number_download_play_store, function (res) { return res.x == year.format("YYYY") })) {
				playStoreDownloadLength = _.find(bar_number_download_play_store, function (res) { return res.x == year.format("YYYY") }).y;
			} else {
				playStoreDownloadLength = playStoreDownloadLength;
			}
			if (_.find(bar_number_download_apple, function (res) { return res.x == year.format("YYYY") })) {
				appleStoreDownloadLength = _.find(bar_number_download_apple, function (res) { return res.x == year.format("YYYY") }).y;
			} else {
				appleStoreDownloadLength = appleStoreDownloadLength;
			}
			data_apple.push(appleStoreDownloadLength);
			data_play_store.push(playStoreDownloadLength);

		} else {
			data_apple.push(0);
			data_play_store.push(0);
		}
		year.add(1, "y");
	}

	return {
		labels: label,
		apple: data_apple,
		play_store: data_play_store
	};
}
function get_data_of_month(start, end, condoIds, oldData) {

	let duration = 0;
	let label = [];

	let data_apple = [];
	let data_play_store = [];

	let bar_number_download_apple = [];
	let bar_number_download_play_store = [];

	let playStoreDownloadLength = oldData.playStoreNumberDownload;
	let appleStoreDownloadLength = oldData.appleStoreNumberDownload;

	duration = end.diff(start, 'months');

	bar_number_download_apple = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('MM[/]YYYY'), y: item.count };
	})]);

	bar_number_download_play_store = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).format('MM[/]YYYY'), y: item.count };
	})]);

	let month = start.startOf('month');
	for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));

		if (month.diff(moment().endOf("months"), 'days') < -32) {

			if (_.find(bar_number_download_play_store, function (res) { return res.x == month.format("MM[/]YYYY") })) {
				playStoreDownloadLength = _.find(bar_number_download_play_store, function (res) { return res.x == month.format("MM[/]YYYY") }).y;
			} else {
				playStoreDownloadLength = playStoreDownloadLength;
			}
			if (_.find(bar_number_download_apple, function (res) { return res.x == month.format("MM[/]YYYY") })) {
				appleStoreDownloadLength = _.find(bar_number_download_apple, function (res) { return res.x == month.format("MM[/]YYYY") }).y;
			} else {
				appleStoreDownloadLength = appleStoreDownloadLength;
			}
			data_apple.push(appleStoreDownloadLength);
			data_play_store.push(playStoreDownloadLength);
		
		} else {
		
			data_apple.push(0);
			data_play_store.push(0);
		
		}
		month.add(1, "M");
	}

	return {
		labels: label,
		apple: data_apple,
		play_store: data_play_store
	};
}
function get_data_of_day(start, end, condoIds, oldData) {

	let duration = 0;
	let label = [];

	let data_apple = [];
	let data_play_store = [];

	let bar_number_download_apple = [];
	let bar_number_download_play_store = [];

	let playStoreDownloadLength = oldData.playStoreNumberDownload;
	let appleStoreDownloadLength = oldData.appleStoreNumberDownload;

	duration = end.diff(start, 'days');

	bar_number_download_apple = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "AppStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).add(2, 'h').format("DD[/]MM[/]YY"), y: item.count };
	})]);

	bar_number_download_play_store = _.flatten([_.map(Analytics.find({
		"type": "download",
		"store": "PlayStore",
	}).fetch(), function (item) {
		return { x: moment(item.date).add(2, 'h').format("DD[/]MM[/]YY"), y: item.count };
	})]);

	let day;
	day = start;
	for (var i = 0; i < duration; i++) {
		label[i] = (day.format("ddd DD MMM"));
		if (day.diff(moment().endOf("days"), 'hours') < 0) {
			if (_.find(bar_number_download_play_store, function (res) { return res.x == day.format("DD[/]MM[/]YY") })) {
				playStoreDownloadLength = _.find(bar_number_download_play_store, function (res) { return res.x == day.format("DD[/]MM[/]YY") }).y;
			} else {
				playStoreDownloadLength = playStoreDownloadLength;
			}
			if (_.find(bar_number_download_apple, function (res) { return res.x == day.format("DD[/]MM[/]YY") })) {
				appleStoreDownloadLength = _.find(bar_number_download_apple, function (res) { return res.x == day.format("DD[/]MM[/]YY") }).y;
			} else {
				appleStoreDownloadLength = appleStoreDownloadLength;
			}
			data_apple.push(appleStoreDownloadLength);
			data_play_store.push(playStoreDownloadLength);
		} else {
			data_apple.push(0);
			data_play_store.push(0);
		}
		day.add(1, "d");
	}

	return {
		labels: label,
		apple: data_apple,
		play_store: data_play_store
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = [];
	let dataset = [];

	let _numberDownloadPlay = _.flatten([_.map(Analytics.find({
		"type": "download",
		"date": { $lte: parseInt(start.format('x')) },
		"store": "PlayStore",
	}, { sort: { date: -1 } }).fetch(), function (item) {
		return item.count;
	})]);

	let _numberDownloadApple = _.flatten([_.map(Analytics.find({
		"type": "download",
		"date": { $lte: parseInt(start.format('x')) },
		"store": "AppStore",
	}, { sort: { date: -1 } }).fetch(), function (item) {
		return item.count;
	})]);

	if ((end.diff(start, 'years')) > 1) {
		data = get_data_of_year(start, end, condoIds, { playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}
	else if ((end.diff(start, 'months')) >= 3) {
		data = get_data_of_month(start, end, condoIds, { playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}
	else {
		data = get_data_of_day(start, end, condoIds, { playStoreNumberDownload: _numberDownloadPlay[0], appleStoreNumberDownload: _numberDownloadApple[0] });
	}

	dataset.push({
		label: '# AppStore',
		data: data.apple,
		backgroundColor: "#69d2e7",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '# PlayStore',
		data: data.play_store,
		backgroundColor: "#8b999f",
		borderColor: "white",
		borderWidth: 1,
		datalabels: {
			display: false,
		}
	});

	dataset.push({
		label: '',
		data: _.map(data.labels, function (DONT_USE, index) { return data.apple[index] + data.play_store[index]}),
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_number_download(start, end, option) {

	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#bar_number_download').remove();
	$('#bar_number_download-container').append('<canvas id="bar_number_download"><canvas>');
	let ctx = $('#bar_number_download');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-number_download-legend").html(myChart.generateLegend());
}


import { MonthOfYear } from "/common/lang/lang.js";

Template.bar_number_download.onCreated(function() {
	this.monthofyear = new ReactiveVar(new MonthOfYear(this.lang).month);
});

let myInterval;

Template.bar_number_download.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) || 
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_number_download(start.clone(), end.clone(), template.data.option);
			render = false;
		}

		$('#dateDownload').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			endDate: '0d',
			language: 'fr',
			todayHighlight: true,
			maxViewMode: 1,
			templates: {
				leftArrow: '<i class="fa fa-chevron-left"></i>',
				rightArrow: '<i class="fa fa-chevron-right"></i>'
			},

		}).on('changeMonth', function(event) {

			let next = $(".datepicker-dropdown table thead tr:nth-child(2) th:last-child")[0];
			
			if (new Date(event.date).getMonth() == new Date().getMonth()) {
				$(next).css('visibility', 'visible');
				$(next).removeClass('next');
				$(next).html('');
			}
			else {
				$(next).addClass('next');
				$(next).html('<i class="fa fa-chevron-right"></i>');
			}
		});
	}, 1000);
});


Template.bar_number_download.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_number_download.events({
	'click .bar_number_download-reload': function(event, template) {
		let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
		let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
		render_bar_number_download(start.clone(), end.clone(), template.data.option);
	},
	'click #addDownload': function() {
		let template = Template.instance();
		let date = $('#dateDownload').val();
		let number = $('#numberDownload').val();
		let type = $('#typeDownload').val();
		let ana;
		if (date != undefined && number != undefined && type != undefined) {
			if (ana = Analytics.findOne({
				'store': type,
				'date': moment(date, "DD-MM-YYYY").valueOf()
			})) {
				bootbox.confirm({
					title: "Confirmation",
					message: "Il existe deja une valeur pour ce store a cette date, voulez vous vraiment l'ecraser ? (valeur : " + ana.count + ")",
					backdrop: true,
					callback: function(result) {
						if (result) {
							Meteor.call('setNewAnalytics', {
								type: 'download',
								date: date,
								count: number,
								store: type,
							}, function(error) {
								if (!error) {
									let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
									let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
									render_bar_number_download(start.clone(), end.clone(), template.data.option);
									sAlert.success('Add download');
									$('#dateDownload').val('');
									$('#numberDownload').val('');
									$('#typeDownload')[0].selectedIndex = 0;
								}
							})
						}
					}
				});
			} else {
				Meteor.call('setNewAnalytics', {
					type: 'download',
					date: date,
					count: number,
					store: type,
				}, function(error) {
					if (!error) {
						let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
						let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
						render_bar_number_download(start.clone(), end.clone(), template.data.option);
						sAlert.success('Add download');
						$('#dateDownload').val('');
						$('#numberDownload').val('');
						$('#typeDownload').val('')
					}
				})
			}
		}
	},
	'click .datepicker': function(event, template) {
		let next = $(".datepicker-dropdown table thead tr:nth-child(2) th:last-child")[0];
		let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
		for (var i = 0; i != template.monthofyear.get().long.length; i++) {
			if (template.monthofyear.get().long[i] == date)
				break;
		}
		if (new Date().getMonth() == i) {
			$(next).css('visibility', 'visible');
			$(next).removeClass('next');
			$(next).html('');
		}
		else {
			$(next).addClass('next');
			$(next).html('<i class="fa fa-chevron-right"></i>');
		}
	},
});

Template.bar_number_download.helpers({
	dateToday: () => {
		let date = new Date(Date('now'));
		return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
	}
});