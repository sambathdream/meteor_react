function set_options_of_chart(option) {
	let options = option;

	options.scales.xAxes[0].afterSetDimensions = function (axis) {
		setTimeout(function () {
			$('#x-axis-arrow-total_users_access_type').css("top", (axis.top + 68) + "px");
			$('#x-axis-arrow-total_users_access_type').css("left", (axis.right + 15) + "px");
			$('#x-axis-arrow-total_users_access_type').css("opacity", 1);
		}, 5);
	}
	options.layout.padding.left = 40;
	options.tooltips = {
		enabled: false,
		custom: function (tooltipModel) {
			if (((tooltipModel && tooltipModel.body && tooltipModel.body[0].lines[0].split(": ")[1] == undefined) == true))
				return;
			var tooltipEl = document.getElementById('chartjs-chart-tooltips-total_users_access_type');

			if (!tooltipEl) {
				tooltipEl = document.createElement('div');
				tooltipEl.id = 'chartjs-tooltip';
				tooltipEl.innerHTML = "<table></table>"
				document.body.appendChild(tooltipEl);
			}

			if (tooltipModel.opacity === 0) {
				tooltipEl.style.opacity = 0;
				return;
			}

			tooltipEl.classList.remove('above', 'below', 'no-transform');
			if (tooltipModel.yAlign) {
				tooltipEl.classList.add(tooltipModel.yAlign);
			} else {
				tooltipEl.classList.add('no-transform');
			}

			function getBody(bodyItem) {
				return bodyItem.lines;
			}

			if (tooltipModel.body) {
				var titleLines = tooltipModel.title || [];
				var bodyLines = tooltipModel.body.map(getBody);

				var innerHtml = '<thead>';

				titleLines.forEach(function (title) {
					innerHtml += '<tr><th></th></tr>';
				});
				innerHtml += '</thead><tbody>';

				bodyLines.forEach(function (body, i) {
					if (body[0].split(':')[1]) {
						var colors = tooltipModel.labelColors[i];
						var style = 'background:' + colors.backgroundColor;
						style += '; border-color:' + colors.borderColor;
						style += '; border-width: 2px';
						var div = '<div class="chartjs-tooltip-key" style="' + style + '"></div>';
						innerHtml += '<tr><td>' + div + body[0].split(':')[0] + '<br>' + body[0].split(':')[1] + '</td></tr>';
					}
				});
				innerHtml += '</tbody>';

				var tableRoot = tooltipEl.querySelector('table');
				tableRoot.innerHTML = innerHtml;
			}

			var position = this._chart.canvas.getBoundingClientRect();

			tooltipEl.style.opacity = 1;
			tooltipEl.style.left = (tooltipModel.caretX + 30) + 'px';
			tooltipEl.style.top = (tooltipModel.caretY + 59) + 'px';
			tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
		}
	};
	return options;
}

function get_users_by_year(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('YYYY')} && moment n: ${moment(u.loginDate).format('YYYY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('YYYY') == moment(u.loginDate).format('YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_year(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
	let data_web = [];
	let data_ios = [];
	let data_android = [];
	// let data_others = [];

	duration = end.diff(start, 'years');
  let bar_total_users_web = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "web",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("YYYY")
  })]));

  let bar_total_users_ios = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "ios",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("YYYY")
  })]));

  let bar_total_users_android = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "android",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("YYYY")
  })]));

  // let bar_total_users_others = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
  //   type: "login",
  //   accessType: undefined,
	// 	"condoId": { $in: condoIds }
	// }).fetch()), function (users) {
	// 	return moment(users.loginDate).format("YYYY")
  // })]));

	let year;

  for (var i = 0; i <= duration; i++) {
		year = start;
		label.push(year.format("YYYY"));
		data_web.push(bar_total_users_web[year.format('YYYY')] || 0);
		data_ios.push(bar_total_users_ios[year.format('YYYY')] || 0);
		data_android.push(bar_total_users_android[year.format('YYYY')] || 0);
		// data_others.push(bar_total_users_others[year.format('YYYY')] || 0);
		year.add(1, "y");
	}

	return {
		labels: label,
		web: data_web,
		ios: data_ios,
		android: data_android,
		// others: data_others,
	};
}

function get_users_by_month(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('MM[/]YY')} && moment n: ${moment(u.loginDate).format('MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('MM[/]YY') == moment(u.loginDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_month(start, end, condoIds, oldUser) {
	let duration = 0;
	let label = [];
	let data_web = [];
	let data_ios = [];
	let data_android = [];
	// let data_others = [];

  duration = end.diff(start, 'months');
  let bar_total_users_web = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "web",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_ios = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "ios",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("MM[/]YYYY")
  })]));

  let bar_total_users_android = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "android",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("MM[/]YYYY")
  })]));

  // let bar_total_users_others = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
  //   type: "login",
  //   accessType: undefined,
	// 	"condoId": { $in: condoIds }
	// }).fetch()), function (users) {
	// 	return moment(users.loginDate).format("MM[/]YYYY")
  // })]));

	let month = start.startOf('month')

  for (var i = 0; i <= duration; i++) {
		label.push(moment().format("YYYY") == month.format("YYYY") ? month.format("MMMM") : month.format("MMM YYYY"));
		data_web.push(bar_total_users_web[month.format('MM[/]YYYY')] || 0);
		data_ios.push(bar_total_users_ios[month.format('MM[/]YYYY')] || 0);
		data_android.push(bar_total_users_android[month.format('MM[/]YYYY')] || 0);
		// data_mobile.push(bar_total_users_mobile[month.format('MM[/]YYYY')] || 0);
		month.add(1, "M");
	}

	return {
		labels: label,
		web: data_web,
		ios: data_ios,
		android: data_android,
		// mobile: data_mobile
	};
}

function get_users_by_day(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      if (w.userId == u.userId && moment(w.loginDate).format('DD[/]MM[/]YY') == moment(u.loginDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function get_data_of_day(start, end, condoIds, oldUser) {
	let duration = 0
  let label = []
  let data_web = []
  let data_ios = []
  let data_android = []
  // let data_mobile = []

  duration = end.diff(start, 'days');

  let bar_total_users_web = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "web",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_ios = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "ios",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("DD[/]MM[/]YY")
  })]));

  let bar_total_users_android = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
    type: "login",
    accessType: "android",
		"condoId": { $in: condoIds }
	}).fetch()), function (users) {
		return moment(users.loginDate).format("DD[/]MM[/]YY")
  })]));

  // let bar_total_users_others = _.countBy(_.flatten([_.map(get_users_by_day(Analytics.find({
  //   type: "login",
  //   accessType: undefined,
	// 	"condoId": { $in: condoIds }
	// }).fetch()), function (users) {
	// 	return moment(users.loginDate).format("DD[/]MM[/]YY")
  // })]));

  // console.log("bar_total_users_web: ", bar_total_users_web)
  // console.log("bar_total_users_mobile: ", bar_total_users_mobile)

  let day = start;

  for (var i = 0; i < duration; i++) {
    label.push(day.format("ddd DD MMM"));
		data_web.push(bar_total_users_web[day.format('DD[/]MM[/]YY')] || 0);
		data_ios.push(bar_total_users_ios[day.format('DD[/]MM[/]YY')] || 0);
		data_android.push(bar_total_users_android[day.format('DD[/]MM[/]YY')] || 0);
		// data_others.push(bar_total_users_others[day.format('DD[/]MM[/]YY')] || 0);
    day.add(1, "d");
	}

	return {
		labels: label,
		web: data_web,
		ios: data_ios,
		android: data_android,
		// others: data_others
	};
}

function build_datasets_of_chart(start, end, condoIds) {
	let data = []
	let dataset = []

	if ((end.diff(start, 'years')) >= 1) {
    // console.log("====== in years ======")
		data = get_data_of_year(start, end, condoIds)
	}
	else if ((end.diff(start, 'months')) >= 3) {
    // console.log("====== in months ======")
		data = get_data_of_month(start, end, condoIds)
	}
	else {
    // console.log("====== in days ======")
		data = get_data_of_day(start, end, condoIds)
  }

  // console.log("data: ", data)
  // console.log("dataset: ", dataset)

	dataset.push({
		label: '# d\'utilisateurs connectés via Web',
		data: data.web,
		fill: false,
		backgroundColor: "#BDC3C7",
		borderColor: "rgb(105, 210, 231)",
		datalabels: {
			display: false
		}
  });

	dataset.push({
		label: '# d\'utilisateurs connectés via IOS',
		data: data.ios,
		fill: true,
		backgroundColor: "#4183D7",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  dataset.push({
		label: '# d\'utilisateurs connectés via Android',
		data: data.android,
		fill: true,
		backgroundColor: "#1BBC9B",
		borderColor: "rgba(255, 71, 82, 1)",
		datalabels: {
			display: false
		}
	});

  // dataset.push({
	// 	label: '# d\'utilisateurs connectés via d'autres plateformes',
	// 	data: data.others,
	// 	fill: true,
	// 	backgroundColor: "#F5AB35",
	// 	borderColor: "rgba(255, 71, 82, 1)",
	// 	datalabels: {
	// 		display: false
	// 	}
	// });

	dataset.push({
		label: '',
		data: data.web,
		lineTension: 0,
		backgroundColor: "transparent",
		borderColor: "transparent",
		pointBorderColor: "transparent",
		pointBackgroundColor: "transparent",
		pointHoverBackgroundColor: "transparent",
		pointHoverBorderColor: "transparent",
		type: 'line',
		datalabels: {
			color: '#000000',
			align: 'end'
		}
	});

	dataset.push({
		label: '',
		backgroundColor: '#88898e',
		borderColor: 'white',
		borderWidth: 1,
		data: _.map(data.labels, function (col) { return 1 }),
		yAxisID: 'littlebar',
		type: 'bar',
		datalabels: {
			display: false
		}
	});

	return {
		labels: data.labels,
		datasets: dataset
	}
}

function render_bar_total_users_access_type(start, end, option) {
	let options = set_options_of_chart(option)

	let condoIds = Session.get('condoIds');
	if (Session.get('condoType').length == 0 && condoIds.length == 0) {
		condoIds = _.map(Condos.find().fetch(), function (condo) { return condo._id });
	}

	let data = build_datasets_of_chart(start, end, condoIds)

	$('#bar_total_users_access_type').remove();
	$('#bar_total_users_access_type-container').append('<canvas id="bar_total_users_access_type"><canvas>');
	let ctx = $("#bar_total_users_access_type");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: data,
		options: options,
	});
	$("#chartjs-total_users_access_type-legend").html(myChart.generateLegend());
}



// Templates methods
Template.bar_total_users_access_type.onCreated(function() {
});

let myInterval;

Template.bar_total_users_access_type.onRendered(function() {
	let start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
	let end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
	let condoIds = Session.get('condoIds').length;
	let condoType = Session.get('condoType').length;
	let render = true;
	let template = Template.instance();
	myInterval = setInterval(function() {
		if ((!render && (start.format('DD[/]MM[/]YY') != Session.get('startDate') || end.format('DD[/]MM[/]YY') != Session.get('endDate')) ||
			(!render && condoIds != Session.get('condoIds').length) ||
			(!render && condoType != Session.get('condoType').length))) {
			start = moment(Session.get('startDate'), "DD[/]MM[/]YY");
			end   = moment(Session.get('endDate'), "DD[/]MM[/]YY");
			condoIds = Session.get('condoIds').length;
			condoType = Session.get('condoType').length;
			render = true;
		}
		if (render) {
			render_bar_total_users_access_type(start.clone(), end.clone(), template.data.option);
			render = false;
		}
	}, 1000)
});


Template.bar_total_users_access_type.onDestroyed(function() {
	clearInterval(myInterval);
})

Template.bar_total_users_access_type.events({
});

Template.bar_total_users_access_type.helpers({
});