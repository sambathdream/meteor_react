import { Template } from "meteor/templating";
import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { Meteor } from 'meteor/meteor';
import { Chart } from 'chart.js';
// import moment from 'moment';

import "./globalStats.view.html"

Template.globalStats.onCreated(function() {
	this.subscribe('fetchForumPostsAdmin')
	Session.set('startDate', moment().startOf('weeks').format('DD[/]MM[/]YY'));
	Session.set('endDate', moment().endOf('weeks').add(1, 'days').format('DD[/]MM[/]YY'));
	Session.set('condoIds', []);
	Session.set('condoType', []);
});

Template.globalStats.onRendered(function() {
	setTimeout(function() {
		$(".select-condos").select2({
			placeholder: "Tous les immeubles"
		});
		$(".select-type-condos").select2({
			placeholder: "Tous les types d'immeubles"
		});
	}, 100);

	setTimeout(function() {
		$('#datepicker-range').daterangepicker({
			"ranges": {
				'Aujourd\'hui': [
					moment(),
					moment().endOf('days')
				],
				'Hier': [
					moment().subtract(1, 'days'),
					moment().subtract(1, 'days')
				],
				'Cette semaine': [
					moment().startOf('weeks'),
					moment().endOf('weeks'),
				],
				'Semaine dernière': [
					moment().startOf('weeks').subtract(1, "week"),
					moment().endOf('weeks').subtract(1, "week")
				],
				'Ce mois': [
					moment().startOf('month'),
					moment().endOf('month'),
				],
				'Mois dernier': [
					moment().subtract(1, "month").startOf('month'),
					moment().subtract(1, "month").endOf('month')
				],
				'Cette année': [
					moment().startOf('year'),
					moment().endOf('year').subtract(1, 'days')
				],
				'Année dernière': [
					moment().startOf('year').subtract(1, 'year'),
					moment().startOf('year').subtract(1, 'year').add(11, 'month')
				]
			},
			"alwaysShowCalendars": true,
			"startDate": Session.get('startDate'),
			"endDate": moment(Session.get('endDate'), 'DD[/]MM[/]YY').subtract(1, 'days'),
			"opens": "left",
			"showCustomRangeLabel": false,
			"autoApply": true,
		}, function(start, end, label) {
			let endDate = end.clone();

			let start_month = start.clone().startOf('month');
			let end_month = end.clone().endOf('month');

			if (label != null) {
				setTimeout(function() {
					$('#datepicker-range').val(label);
				}, 10);
			} else {
				setTimeout(function() {

					if (start.format('DD MM YY') == end.format('DD MM YY')) {
						if (start.format('YYYY') == moment().format('YYYY')) {
							$('#datepicker-range').val(start.format('DD MMM'));
						}
						else {
							$('#datepicker-range').val(start.format('DD MMM YYYY'));
						}
					}

					else if (start.format('MM YY') == end.format('MM YY')) {
						if (start.format('DD MM YY') == start_month.startOf('month').format('DD MM YY') && end.format('DD MM YY') == end_month.endOf('month').format('DD MM YY')) {

							if (start.format('YYYY') == moment().format('YYYY')) {
								$('#datepicker-range').val(start.format('MMMM'));
							}

							else {
								$('#datepicker-range').val(start.format('MMMM YYYY'));
							}
						}

						else {

							if (start.format('YYYY') == moment().format('YYYY')) {
								$('#datepicker-range').val(start.format('DD') + ' - ' + end.format('DD MMM'));
							}
							else {
								$('#datepicker-range').val(start.format('DD') + ' - ' + end.format('DD MMM YYYY'));
							}
						}
					}
					else {
						let ret = "";
						if (start.format('YYYY') == moment().format('YYYY')) {
							ret = (start.format('DD MMM') + ' - ');
						}
						else {
							ret = (start.format('DD MMM YYYY') + ' - ');
						}
						if (end.format('YYYY') == moment().format('YYYY')) {
							ret += (end.format('DD MMM'));
						}
						else {
							ret += (end.format('DD MMM YYYY'));
						}
						$('#datepicker-range').val(ret);
					}
				}, 10)
			}
			Session.set('startDate', start.format('DD[/]MM[/]YY'));
			Session.set('endDate', endDate.add(1, 'days').format('DD[/]MM[/]YY'));
		});
		$('#datepicker-range').val('Cette Semaine');
	}, 500);
});


Template.globalStats.onDestroyed(function() {
	Session.set('startDate', undefined);
	Session.set('endDate', undefined);
	Session.set('condoIds', undefined);
	Session.set('condoType', undefined);
})

Template.globalStats.events({
	'select2:selecting .select-condos': function(event, template) {
		let condoIds = [];
		let condoIdsSession = Session.get('condoIds') || [];
		if (condoIdsSession.length != 0) {
			condoIds = condoIdsSession;
		}
		condoIds.push(event.params.args.data.id);
		Session.set('condoIds', condoIds);
		$('.w-no-condo').remove();
	},
	'select2:unselecting .select-condos': function(event, template) {
		let condoType = Session.get('condoType') || [];
		let condoIds = Session.get('condoIds') || [];
		if (condoIds.length != 0) {
			let index = condoIds.indexOf(event.params.args.data.id);
			condoIds.splice(index, 1);
			Session.set('condoIds', condoIds);
		}
		if (condoIds.length == 0 && condoType.length != 0 && $('.w-no-condo').length == 0) 
			$('#globalStat-header').append('<div class="col-md-2 pull-left w-no-condo">⚠ Aucun immeuble selectionné</div>')
		else 
			$('.w-no-condo').remove();
	},
	'select2:selecting .select-type-condos': function(event, template) {
		let condoType = Session.get('condoType') || [];
		// set session variable "CondoType";
		condoType.push(event.params.args.data.id);
		Session.set('condoType', condoType);
		// find condo; 
		let condos = Condos.find({
			"settings.condoType": {$in: condoType}
		}).fetch();
		// set session variable "CondoIds";
		let condoIds = _.map(condos, function(elem) { return elem._id; });
		Session.set('condoIds', condoIds);

		if (condoIds.length == 0 && condoType.length != 0 && $('.w-no-condo').length == 0) 
			$('#globalStat-header').append('<div class="col-md-2 pull-left w-no-condo">⚠ Aucun immeuble selectionné</div>')
		else 
			$('.w-no-condo').remove();
		// fill $(.select-condos) (select2);
		let condoSelected = [];
		_.each(condos, function(condo) {
			condoSelected.push(condo._id);
		})
		$(".select-condos").val(condoSelected).trigger('change');
	},
	'select2:unselecting .select-type-condos': function(event, template) {
		let condoType = Session.get('condoType') || [];
		// delete item in session variable "CondoType";
		let index = condoType.indexOf(event.params.args.data.id);
		condoType.splice(index, 1);
		Session.set('condoType', condoType);
		// find condo;
		let condos = Condos.find({
			"settings.condoType": {$in: condoType}
		}).fetch();
		// re-set session variable "CondoIds";
		let condoIds = _.map(condos, function(elem) { return elem._id; })
		Session.set('condoIds', condoIds);
		$('.w-no-condo').remove();
		// fill $(.select-condos) (select2);
		let condoSelected = [];
		_.each(condos, function(condo) {
			condoSelected.push(condo._id);
		})
		$(".select-condos").val(condoSelected).trigger('change');
	},
});

Template.globalStats.helpers({
	getcondos: () => {
		return Condos.find().fetch();
	},
	getOption: () => {
		return {
			scales: {
				xAxes: [{
					maxBarThickness: 50,
					gridLines: {
						display: false,
						color: "#88898e",
						lineWidth: 2,
						zeroLineWidth: 2,
						zeroLineColor: "#88898e",
						drawTicks: true,
						tickMarkLength: 3
					},
					ticks: {
						padding: 10,
						autoSkip: false,
						maxRotation: 0,
						callback: function(value, index, values) {
							let val = value.split(' ');
							if (values.length > 10) {
								if (index % (parseInt(values.length / 10) + 1) == 0) {
									if (val.length == 6) {
										return [
										val[0] + ' ' + val[1],
										val[2] + ' ' + val[3],
										val[4] + ' ' + val[5]
										];
									}
									if (val.length == 4) {
										return [
										val[0] + ' ' + val[1],
										val[2] + '-' + val[3]
										];
									}
									if (val.length == 3) {
										return [
											val[0].substring(0, val[0].length - 1),
											val[1] + ' ' + val[2].substring(0, val[0].length - 1)
										];
									}
									else if (val.length == 2) {
										return [
										val[0],
										val[1]
										]
									}
									else if (val.length == 1) {
										return val[0];
									}
								}
							}
							else {
								if (val.length == 6) {
									return [
									val[0] + ' ' + val[1],
									val[2] + ' ' + val[3],
									val[4] + ' ' + val[5]
									];
								}
								else if (val.length == 4) {
									return [
									val[0] + ' ' + val[1],
									val[2] + '-' + val[3]
									];
								}
								else if (val.length == 3) {
									return [
									val[0].substring(0, val[0].length - 1),
									val[1] + ' ' + val[2].substring(0, val[0].length - 1)
									];
								}
								else if (val.length == 2) {
									return [
									val[0],
									val[1]
									]
								}
								else if (val.length == 1) {
									return val[0];
								}
							}
						}
					},
					position: 'bottom',
					stacked: true,
				}],
				yAxes: [{
					gridLines: {
						display: false,
						color: "#88898e",
						lineWidth: 2,
						zeroLineWidth: 2,
						zeroLineColor: "#88898e",
						drawTicks: true,
						tickMarkLength: 3
					},
					ticks: {
						callback: function (value) {
							if (value < 1) {
								return;
							}
						},
						display: false,
						padding: 10,
						beginAtZero: true
					},
					stacked: true,
				}, {
					id: 'littlebar',
					display: false,
					ticks: {
						beginAtZero: true,
						min: 0,
						max: 50,
						autoSkip: false,
					},
					type: 'linear',
					position: 'right',
				}]
			},
			gridLines: {
				drawBorder: false
			},
			plugins: {
				datalabels: {
					display: false,
				}
			},
			layout: {
				padding: {
					left: 0,
					right: 300,
					top: 50,
					bottom: 50
				}
			},
			legend: {
				display: false,
			},
			legendCallback: function(chart) {
				var text = [];
				text.push('<ul class="chart-legend">');
				let datasets = chart.config.data.datasets;
				datasets = _.filter(datasets, function (data) {
					return data.label != '';
				})
				if (datasets.length >= 2) {
					for (var i = datasets.length - 1; i >= 0; i--) {
						if (!((datasets[i].label == ""))) {
							text.push('<li class="inline"><div class="inline" style="width: 14px; height: 14px; border-radius: 0; background-color:' + datasets[i].backgroundColor + '"> </div>');
							if (datasets[i].label) {
								text.push('<span class="chart-legend-item">'+datasets[i].label+'</div>');
							}
							text.push('</li>');
						}
					}
				}
				text.push('</ul>');
				return text.join("");
			},
			plugins: {
				datalabels: {
					enabled: true,
					display: function(context) {
						return context.dataset.data[context.dataIndex] > 0;
					}
				},
			},
			hover: {
				mode: null
			},
		}
	},
	ready: () => {
		if (Template.instance().isReady)
			return Template.instance().isReady.get();
	},
});
