import { FlowRouter } from 'meteor/kadira:flow-router'
import { BlazeLayout } from 'meteor/kadira:blaze-layout'
import { Meteor } from 'meteor/meteor'

var backofficeRoutes = FlowRouter.group({
  prefix: '/backoffice',
  name: 'app.backoffice',
  triggersEnter: [
    function (context, redirect) {
      Meteor.call('updateSEO', { routeName: context.route.name, lang: context.params.lang || 'fr' })
      if (context.path !== '/backoffice/login') {
        Meteor.call('user_allow_backoffice', function (err) {
          if (err) {
            if (err.error === 300) { /* unauthenticated user */
              FlowRouter.go('app.backoffice.login')
            } else { /* Other errors go to access denied */
              const lang = FlowRouter.getParam('lang') || 'fr'
              FlowRouter.go('app.errorAccessDenied', { lang })
              console.log('err', err)
              // FlowRouter.go('app.errorAccessDenied')
            }
          }
        })
      } else if (context.path === '/backoffice') {
        FlowRouter.go('app.backoffice.manager')
      }
    }
  ]
})

backofficeRoutes.route('/', {
  name: 'app.backoffice.home',
  action () {
    BlazeLayout.render('backoffice_home')
  }
})

backofficeRoutes.route('/globalStats', {
  name: 'app.backoffice.globalStats',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'globalStats' })
  }
})

backofficeRoutes.route('/resourceType', {
  name: 'app.backoffice.resourceType',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'ResourceType' })
  }
})

backofficeRoutes.route('/resourceService', {
  name: 'app.backoffice.resourceService',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'ResourceService' })
  }
})

// backofficeRoutes.route('/users', {
//   name: 'app.backoffice.users',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'listUsers' })
//   }
// })

// backofficeRoutes.route('/users/user_details/:id', {
//   name: 'app.backoffice.users.user_details',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'userDetails' })
//   }
// })

// backofficeRoutes.route('/users/user_details/:id/:tab', {
//   name: 'app.backoffice.users.user_details_tabs',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'userDetails' })
//   }
// })

// backofficeRoutes.route('/users/user_rights/:userId/:condoId', {
//   name: 'app.backoffice.users.user_rights',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'userRights' })
//   }
// })

// backofficeRoutes.route('/condos', {
//   name: 'app.backoffice.condos',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'listCondos' })
//   }
// })

// backofficeRoutes.route('/condos/tab/:tab', {
//   name: 'app.backoffice.condos.tab',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'listCondos' })
//   }
// })

backofficeRoutes.route('/condos/condo_details/:id', {
  name: 'app.backoffice.condos.condo_details',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'condoDetails' })
  }
})

backofficeRoutes.route('/condos/condo_details/:id/:tab', {
  name: 'app.backoffice.condos.condo_details_tabs',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'condoDetails' })
  }
})

backofficeRoutes.route('/condos/condo_details/:id/:tab/:module/:moduleParam', {
  name: 'app.backoffice.condos.condo_details_tabs.moduleParam',
  action (params, queryParams) {
    BlazeLayout.render('backoffice_home', { template: 'condoDetails' })
  }
})






backofficeRoutes.route('/entityList', {
  name: 'app.backoffice.entityList',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_entityController' })
  }
})

backofficeRoutes.route('/entityList/:entityId/:tab?', {
  name: 'app.backoffice.entityList.entityId.tab',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_entityController' })
  }
})

backofficeRoutes.route('/createEntity/:id?/:module?/:moduleParam?', {
  name: 'app.backoffice.createEntity',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_createEntity' })
  }
})

backofficeRoutes.route('/buildingsList/createBuildings', {
  name: 'app.backoffice.buildingsList.newBuildings',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_newBuildings' })
  }
})

backofficeRoutes.route('/buildingsList/createBuildings/:entityId', {
  name: 'app.backoffice.buildingsList.newBuildings.entityId',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_newBuildings' })
  }
})

backofficeRoutes.route('/buildingsList/:tab?', {
  name: 'app.backoffice.buildingsList.tab',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_buildingsController' })
  }
})

backofficeRoutes.route('/managerList', {
  name: 'app.backoffice.managerList',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_managerList' })
  }
})

backofficeRoutes.route('/managerList/createManager/personalDetails', {
  name: 'app.backoffice.managerList.personalDetails',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetails' })
  }
})

backofficeRoutes.route('/managerList/createManager/personalDetails/:condoId', {
  name: 'app.backoffice.managerList.personalDetails.condoId',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetails' })
  }
})

backofficeRoutes.route('/managerList/createManager/personalDetails/entity/:entityId', {
  name: 'app.backoffice.managerList.personalDetails.entityId',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetails' })
  }
})

backofficeRoutes.route('/managerList/:userId/managerProfile/:tab', {
  name: 'app.backoffice.managerList.managerProfile',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_profileDetails' })
  }
})

backofficeRoutes.route('/occupantList', {
  name: 'app.backoffice.occupantList',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_occupantList' })
  }
})

backofficeRoutes.route('/occupantList/:tab', {
  name: 'app.backoffice.occupantList.tab',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_occupantList' })
  }
})

backofficeRoutes.route('/occupantList/:userId/occupantProfile/:tab', {
  name: 'app.backoffice.occupantList.occupantProfile',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_profileDetails' })
  }
})

backofficeRoutes.route('/occupantList/createOccupant/personalDetails', {
  name: 'app.backoffice.occupantList.personalDetails',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetailsOccupant' })
  }
})

backofficeRoutes.route('/occupantList/createOccupant/personalDetails/:condoId', {
  name: 'app.backoffice.occupantList.personalDetails.condoId',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetailsOccupant' })
  }
})

backofficeRoutes.route('/occupantList/createOccupant/personalDetails/entity/:entityId', {
  name: 'app.backoffice.occupantList.personalDetails.entityId',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_personalDetailsOccupant' })
  }
})







// backofficeRoutes.route('/manager', {
//   name: 'app.backoffice.manager',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'listManager' })
//   }
// })

// backofficeRoutes.route('/manager/manager_details/:id', {
//   name: 'app.backoffice.manager.manager_details',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'managerDetails' })
//   }
// })

// backofficeRoutes.route('/manager/manager_details/:id/:tab', {
//   name: 'app.backoffice.manager.manager_details_tab',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'managerDetails' })
//   }
// })

// backofficeRoutes.route('/manager/manager_rights/:userId/:condoId', {
//   name: 'app.backoffice.manager.manager_rights',
//   action () {
//     BlazeLayout.render('backoffice_home', { template: 'managerRights' })
//   }
// })

backofficeRoutes.route('/archives', {
  name: 'app.backoffice.archives',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'archives' })
  }
})

backofficeRoutes.route('/concierge', {
  name: 'app.backoffice.concierge',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'concierge' })
  }
})

backofficeRoutes.route('/enterprise', {
  name: 'app.backoffice.enterprise',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'listEnterprise' })
  }
})

backofficeRoutes.route('/enterprise/enterprise_details/:id', {
  name: 'app.backoffice.enterprise.enterprise_details',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'enterpriseDetails' })
  }
})

backofficeRoutes.route('/enterprise/enterprise_details/:id/add_manager', {
  name: 'app.backoffice.enterprise.enterprise_details.add_manager',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'enterpriseDetails_modal_manager' })
  }
})

backofficeRoutes.route('/enterprise/enterprise_details/:id/add_condo', {
  name: 'app.backoffice.enterprise.enterprise_details.add_condo',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'enterpriseDetails_modal_condo' })
  }
})

backofficeRoutes.route('/report', {
  name: 'app.backoffice.report',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'report' })
  }
})

backofficeRoutes.route('/report/report_details/:id', {
  name: 'app.backoffice.report.report_details',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'reportDetails' })
  }
})

backofficeRoutes.route('/coffee', {
  name: 'app.backoffice.scripts',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'scripts' })
  }
})

backofficeRoutes.route('/integrations', {
  name: 'app.backoffice.integrations',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'integrations' })
  }
})

backofficeRoutes.route('/integrations/tab/:tab', {
  name: 'app.backoffice.integrations.tab',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'integrations' })
  }
})

backofficeRoutes.route('/integrations/detail/:integrationId', {
  name: 'app.backoffice.integrationDetail',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'integrationDetail' })
  }
})

backofficeRoutes.route('/integrations/condo/:condoId', {
  name: 'app.backoffice.integrationCondo',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'integrationCondo' })
  }
})

backofficeRoutes.route('/integrations/config/:condoId/:integrationId', {
  name: 'app.backoffice.integrationConfig',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'integrationConfig' })
  }
})

backofficeRoutes.route('/roles', {
  name: 'app.backoffice.roles',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'roles' })
  }
})

backofficeRoutes.route('/roles/newRoles', {
  name: 'app.backoffice.newRoles',
  action () {
    BlazeLayout.render('backoffice_home', { template: 'admin_newRoles' })
  }
})
