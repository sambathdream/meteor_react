import { Template } from "meteor/templating"

Template.modal_add_condo_manager.onCreated(function () {
  this.userId = new ReactiveVar(FlowRouter.getParam('userId'));
  this.isOpen = new ReactiveVar(false);

  this.condosToAdd = new ReactiveArray([])
  this.condosToAdd.splice(0, 1, {
    condoName: "",
    condoId: "",
    roleId: "",
    roleName: "",
  });
});

Template.modal_add_condo_manager.onRendered(function () {

});

Template.modal_add_condo_manager.events({
  'hidden.bs.modal #add_condo_manager_modal': function (event, template) {
    template.isOpen.set(false);
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #add_condo_manager_modal': function (event, template) {
    template.isOpen.set(true);
    let modal = $(event.currentTarget);
    window.location.hash = "#add_condo_manager_modal";
    window.onhashchange = function () {
      if (location.hash != "#add_condo_manager_modal") {
        modal.modal('hide');
      }
    }
  },
  'click [name="condoName"]': function (event, template) {
    let idxParent = $(event.currentTarget).attr("idxparent");
    $("[name='condo" + idxParent + "'").removeClass("errorModalInput");
    let idx = $(event.currentTarget).attr("idx");
    let condoId = $(event.currentTarget).attr("condoId");
    let name = $(event.currentTarget).attr("condoName");
    template.condosToAdd.splice(idxParent, 1, {
      condoName: name,
      condoId: condoId,
      roleId: "",
      roleName: "",
    });
  },
  'click [name="condoRole"]': function (event, template) {
    let idxParent = $(event.currentTarget).attr("idxparent");
    $("[name='role" + idxParent + "'").removeClass("errorModalInput");
    let roleId = $(event.currentTarget).attr("roleId");
    let name = $(event.currentTarget).attr("roleName");
    let thisElement = template.condosToAdd.list()[idxParent];
    thisElement.roleName = name;
    thisElement.roleId = roleId;
    template.condosToAdd.splice(idxParent, 1, thisElement);
  },
  'click [name="addCondo"]': function (event, template) {
    template.condosToAdd.push({
      condoName: "",
      condoId: "",
      roleId: "",
      roleName: "",
      statusId: "",
      statusName: "",
      buildingId: "",
      buildingName: "",
      door: "",
      floor: ""
    });
  },
  'click [name="clearSelect"]': function (event, template) {
    let selectIdx = $(event.currentTarget).attr("idx");
    template.condosToAdd.splice(selectIdx, 1);
  },
  'click #addCondoToManager': function (event, template) {
    event.preventDefault();
    event.stopPropagation();

    let newCondo = template.condosToAdd.list();
    let userId = Template.instance().userId.get();
    let hasError = false;

    _.each(newCondo, function (elem, index) {
      $("#addCondoToManager").button("loading");
      hasError = false;
      if (elem.condoId == undefined || elem.condoId == "") {
        $("[name='condo" + index + "'").addClass("errorModalInput");
        hasError = true;
      }
      if (elem.roleId == undefined || elem.roleId == "") {
        $("[name='role" + index + "'").addClass("errorModalInput");
        hasError = true;
      }

      if (hasError == false) {
        const user = Meteor.users.findOne({ _id: userId })
        Meteor.call('addCondoInCharge', user.identities.gestionnaireId, userId, {
          access: [1, 2],
          condoId: elem.condoId,
          notifications: { actualites: true, classifieds: true, edl: true, forum_forum: true, incidents: true, messenger: true, forum_syndic: true },
        }, elem.roleId, function (error, result) {
          $("#addCondoToManager").button("reset");
          if (!error) {
            sAlert.success("Résidence " + elem.condoName + " ajouté avec succès");
            $('#add_condo_manager_modal').modal('hide')
          }
          else
            sAlert.error(error);
        })
      }
    })

  },
});

Template.modal_add_condo_manager.helpers({
  getUserName: () => {
    let user = Meteor.users.findOne({ "_id": Template.instance().userId.get() });
    return user.profile.firstname + " " + user.profile.lastname;
  },
  getCondosToAdd: () => {
    return Template.instance().condosToAdd.list();
  },
  getCondoListToAdd: (idxParent) => {
    const enterprise = Enterprises.findOne({ "users.userId": Template.instance().userId.get() })
    const manager = enterprise.users.find((user) => {
      return user.userId === Template.instance().userId.get()
    })
    if (manager) {
      condosIds = manager.condosInCharge.map((condoInCharge) => {
        return condoInCharge.condoId
      })
      const allowedCondoIds = _.difference(enterprise.condos, condosIds)
      let condos = Condos.find({ _id: { $in: allowedCondoIds } }).fetch()
      if (condos) {
        return _.without(_.map(condos, function (condo) {
          isSelected = false;
          _.each(Template.instance().condosToAdd.list(), function (condoToAdd, index) {
            if (index != idxParent && condoToAdd.condoId == condo._id)
              isSelected = true;
          })
          if (isSelected == false) {
            let name = condo.getNameWithAddress();
            return { _id: condo._id, name: name, idxParent: idxParent };
          }
        }), undefined, null);
      }
    }
  },
  dropdownCondo: (idx) => {
    let thisElement = Template.instance().condosToAdd.list()[idx];
    if (thisElement)
      return thisElement.condoName;
  },
  dropdownRole: (idx) => {
    return Template.instance().condosToAdd.list()[idx].roleName;
  },
  dropdownStatus: (idx) => {
    return Template.instance().condosToAdd.list()[idx].statusName;
  },
  dropdownBuilding: (idx) => {
    return Template.instance().condosToAdd.list()[idx].buildingName;
  },
  isOpen: () => {
    return Template.instance().isOpen.get();
  },
  isSelected: (idx) => {
    if (Template.instance().condosToAdd.list()[idx].condoId != "")
      return true;
    return false;
  },
  getRoleForCondo: (idxParent) => {
    const condoId = Template.instance().condosToAdd.list()[idxParent].condoId
    if (condoId != "") {
      const condo = Condos.findOne({ _id: condoId }, { fields: { 'settings': true } })
      const condoType = condo && (condo.settings.condoType === 'etudiante' ? 'student' : condo.settings.condoType)

      let roles = DefaultRoles.find({ for: "manager", ['forCondoType.' + condoType]: true }).fetch()
      _.each(roles, function (role, index) {
        roles[index].idxParent = idxParent;
      })
      return roles;
    }
  },
  initiateData: () => {
    Template.instance().condosToAdd.clear();
    Template.instance().condosToAdd.splice(0, 1, {
      condoName: "",
      condoId: "",
      roleId: "",
      roleName: "",
      statusId: "",
      statusName: "",
      buildingId: "",
      buildingName: "",
      door: "",
      floor: ""
    });
  },
  getStatusList: (idxParent) => {
    let status = BuildingStatus.find().fetch();
    _.each(status, function (elem, index) {
      status[index].idxParent = idxParent;
    })
    return status
  },
  getBuildingList: (idxParent) => {
    let thisElement = Template.instance().condosToAdd.list()[idxParent];
    condoId = thisElement.condoId;
    let buildings = Buildings.find({ condoId: condoId }).fetch();
    _.each(buildings, function (building, index) {
      buildings[index].idxParent = idxParent;
    })
    return buildings;
  },


});
