import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.admin_occupantList.onCreated(function () {
  this.condoId = new ReactiveVar('all')
  let tab = FlowRouter.getParam('tab')
  this.handler = Meteor.subscribe('usersProfile')
  this.handler2 = Meteor.subscribe('admin_UsersJustifFiles')
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: 'active' })
  }
})

Template.admin_occupantList.onDestroyed(function () {
})

Template.admin_occupantList.onRendered(() => {
})

Template.admin_occupantList.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab');
    FlowRouter.setParams({ tab: tabName })
  },
  'click .invitOccupant': (e, t) => {
    let condoId = Template.instance().condoId.get()
    const lang = FlowRouter.getParam('lang') || 'fr'
    if (condoId === 'all') {
      FlowRouter.go('app.backoffice.occupantList.personalDetails', { lang })
    } else {
      FlowRouter.go('app.backoffice.occupantList.personalDetails.condoId', { lang, condoId })
    }
  }
})

Template.admin_occupantList.helpers({
  getTemplateName: () => {
    let tab = FlowRouter.getParam('tab')
    if (tab === 'active') {
      return 'admin_activeUsers'
    } else {
      return 'admin_pendingUsers'
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('tab')
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: 'active' })
    }
    return tab
  },
  condoIdCb: () => {
    let t = Template.instance()
    return (condoId) => {
      t.condoId.set(condoId)
    }
  },
  getData: () => {
    const condoId = Template.instance().condoId.get()
    let condoIds = []
    if (condoId == 'all') {
      condoIds = _.pluck(Condos.find({}, {fields: { _id: true }}).fetch(), '_id')
    } else {
      condoIds = [condoId]
    }
    return { condoId: condoIds }
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready() && Template.instance().handler2.ready()
  },
  getPendingCounter: () => {
    const condoId = Template.instance().condoId.get()
    let condoIds = []
    if (condoId == 'all') {
      condoIds = _.pluck(Condos.find({}, { fields: { _id: true } }).fetch(), '_id')
    } else {
      condoIds = [condoId]
    }
    let counter = 0
    let occupants = Residents.find({
      '_id': { $ne: Meteor.userId() },
      'pendings.condoId': { $in: condoIds },
      'validation.type': { $in: ['gestionnaire', 'backoffice'] }
    })
    return occupants ? occupants.count() : 0
  }
})
