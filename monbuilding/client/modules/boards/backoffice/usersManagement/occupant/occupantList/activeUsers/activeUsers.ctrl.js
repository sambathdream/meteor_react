var PNF = require('google-libphonenumber').PhoneNumberFormat
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()
import { CommonTranslation } from "/common/lang/lang.js"

function condoDel(condos, userId, lang) {
  let btn = {}

  const translation = new CommonTranslation(lang)

  for (index in condos) {
    const condo = Condos.findOne({ _id: condos[index].condoId })
    let condoName = null

    if (condo) {
      if (condo.info.id != "-1")
        condoName = condo.info.id + ' - ' + condo.getName()
      else
        condoName = condo.getName();
    }

    btn['btn' + index] = {
      label: condoName, className: "btn-outline-red-confirm widthBtn", callback: function () {
        if (condo) {
          Meteor.call('removeResidentOfCondo', userId, condo._id, (err, res) => {
            if (err)
              sAlert.error(translation.commonTranslation["error_occured"])
            else {
              sAlert.success(translation.commonTranslation["validation_success"])
              if (condo.name === 'Les Estudines Paris Rosny') {
                Meteor.call('sendSurvey', userId)
              }
            }
          })
        }
      }
    }
  }

  btn['cancel'] = { label: "Annuler", className: "btn-red-confirm widthBtn" }

  return btn
}


Template.admin_activeUsers.onCreated(function() {
  this.condoId = new ReactiveVar(null)
  this.searchText = new ReactiveVar('')
})

Template.admin_activeUsers.onDestroyed(function() {
})

Template.admin_activeUsers.onRendered(() => {
})

Template.admin_activeUsers.events({
  'click .containerLine': (event, template) => {
    let userId = $(event.currentTarget).attr('userId')
    const lang = FlowRouter.getParam("lang") || "fr"
    FlowRouter.go('app.backoffice.occupantList.occupantProfile', { lang, userId, tab: 'documents' })
  },
  'click .commonColumn.trashColumn > div': function(event, template) {
    event.preventDefault()
    event.stopPropagation()
    const userId = this._id

    const lang = 'fr'
    const translate = new CommonTranslation(lang)

    let btn = condoDel(this.condos, this._id, lang)
    let size = Object.keys(btn).length

    let title = translate.commonTranslation.confirmation
    let message = translate.commonTranslation.select_building_to_remove + this.firstname

    $(event.currentTarget).css("max-width", "100%")
    bootbox.dialog({
      size: "medium",
      title: title,
      message: message,
      buttons: btn,
      onEscape: function () { },
      backdrop: true
    })
  }
})

Template.admin_activeUsers.helpers({
  updateCondo: () => {
    Template.instance().condoId.set(Template.currentData().condoId)
  },
  getOccupants: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    const condoId = Template.instance().condoId.get()

    if (condoId && condoId.length) {
      let occupants = []
      let occupantsPending = []
      let users = []

      occupants = Residents.find({
        '_id': { $ne: Meteor.userId() },
        'condos.condoId': { $in: condoId }
      })
      occupantsPending = Residents.find({
        '_id': { $ne: Meteor.userId() },
        'pendings': {
          $elemMatch:
            { 'condoId': { $in: condoId },
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      })
      occupants = [...occupants, { isPendings: true }, ...occupantsPending]
      let isPending = false
      occupants.forEach(user => {
        if (user.isPendings && user.isPendings === true) {
          isPending = true
          return
        }
        let thisUserProfile = UsersProfile.findOne({ _id: user.userId })
        if (thisUserProfile) {

          let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
          let phoneMatch = false
          try {
            number = phoneUtil.parse(number, "FR")
            if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
              phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
            }
          } catch (e) { }

          const _userHasMatched =
            thisUserProfile.email.match(regexp) ||
            thisUserProfile.firstname.match(regexp) ||
            thisUserProfile.lastname.match(regexp) ||
            thisUserProfile.tel.match(regexp) ||
            (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
            (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
            (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
            phoneMatch
          if (!isPending) {
            thisUserCondo = user.condos
          } else {
            thisUserCondo = user.pendings
          }
          if (_userHasMatched) {
            let avatar = Avatars.findOne({ _id: thisUserProfile._id })
            if (avatar && avatar.avatar.original) {
              thisUserProfile.avatar = avatar.avatar.original
            } else {
              thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
            }
            users.push({
              ...thisUserProfile,
              condos: thisUserCondo,
              invited: thisUserCondo[0].invited,
              isPending: isPending
            })
          }
        }
      })
      users.sort((a, b) => { return b.invited - a.invited })

      return users
    } else {
      return []
    }
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: () => {
    const condoId = Template.instance().condoId.get()

    let occupants = null
    let occupantsPending = null

    occupants = Residents.find({
      '_id': { $ne: Meteor.userId() },
      'condos.condoId': { $in: condoId }
    }, { fields: { _id: true } })
    occupantsPending = Residents.find({
      '_id': { $ne: Meteor.userId() },
      'pendings': {
        $elemMatch:
          { 'condoId': { $in: condoId },
            'invitByGestionnaire': { $exists: true },
            'invitByGestionnaire': true },
      }
    }, { fields: { _id: true } })
    const count = (occupants ? occupants.count() : 0) + (occupantsPending ? occupantsPending.count() : 0)
    return count
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitOccupant').click()
    }
  },
  isCondoAvailable: () => {
    return !!Template.instance().condoId.get()
  },
  getCondoName: (condoId) => {
    let condo = Condos.findOne(condoId)
    if (condo && condoId) {
      if (condo.info.id != '-1') {
        return condo.info.id + ' - ' + condo.getName()
      }
      else {
        return condo.getName()
      }
    }
  }

})
