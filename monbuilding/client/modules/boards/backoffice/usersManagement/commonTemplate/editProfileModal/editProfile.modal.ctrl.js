import { CommonTranslation } from "/common/lang/lang.js"


var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.admin_editProfileOccupant.onCreated(function () {
  this.subscribe('PhoneCode');

  let userProfile = Meteor.users.findOne(FlowRouter.getParam('userId'))

  this.email = new ReactiveVar(null)
  this.firstname = new ReactiveVar(null)
  this.lastname = new ReactiveVar(null)
  this.phoneCode = new ReactiveVar(null)
  this.phone = new ReactiveVar(null)
  this.landlineCode = new ReactiveVar(null)
  this.landline = new ReactiveVar(null)
  this.isOpen = new ReactiveVar(false)

  this.goodPhone = false
  this.goodLandline = false
  this.canSubmit = new ReactiveVar(false)
})

Template.admin_editProfileOccupant.onDestroyed(() => {
})

Template.admin_editProfileOccupant.onRendered(() => {
})

Template.admin_editProfileOccupant.events({
  'hidden.bs.modal #modal_occupant_edit_profile': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_occupant_edit_profile': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_occupant_edit_profile";

    let thisUser = Meteor.users.findOne(FlowRouter.getParam('userId'))
    template.email.set(thisUser.emails[0].address)
    template.firstname.set(thisUser.profile.firstname)
    template.lastname.set(thisUser.profile.lastname)
    template.phoneCode.set(thisUser.profile.telCode || '33')
    template.phone.set(thisUser.profile.tel || null)
    template.landlineCode.set(thisUser.profile.tel2Code || '33')
    template.landline.set(thisUser.profile.tel2 || null)
    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_occupant_edit_profile") {
        modal.modal('hide');
      }
    }
  },
  'click .saveContact': (e, t) => {
    if (t.canSubmit.get() === true) {
      $(e.currentTarget).button('loading')
      const email = t.email.get()
      const firstname = t.firstname.get()
      const lastname = t.lastname.get()
      const phone = t.phone.get()
      const landline = t.landline.get()
      const phoneCode = t.phoneCode.get()
      const landlineCode = t.landlineCode.get()
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      const userId = FlowRouter.getParam('userId')
      Meteor.call('updateUserProfile', email, phone, phoneCode, landline, landlineCode, userId, firstname, lastname, (error, result) => {
        $(e.currentTarget).button('reset')
        if (!error) {
          sAlert.success(translate.commonTranslation.changes_saved)
          $('#modal_occupant_edit_profile').modal('hide')
        } else {
          sAlert.error(error)
        }
      })
    }
  }
})

function updateSubmit(template) {
  let canSubmit = false
  const email = template.email.get()
  const firstname = template.firstname.get()
  const lastname = template.lastname.get()
  const phone = template.phone.get()
  const landline = template.landline.get()
  canSubmit =
    email !== null && (Isemail.validate(email) === true) &&
    !!firstname && !!lastname &&
    (phone === null || phone === '' || template.goodPhone === true) &&
    (landline === null || landline === '' || template.goodLandline === true)
  template.canSubmit.set(canSubmit)
}

Template.admin_editProfileOccupant.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.phone.get()
        t.phoneCode.set(selected)
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.phone.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.goodPhone = false
          }
        } catch (e) {
          t.goodPhone = false
        }
      } else if (key === 'phoneCodeLandline') {
        let number = '+' + selected + t.landline.get()
        t.landlineCode.set(selected)
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.landline.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.goodLandline = false
          }
        } catch (e) {
          t.goodLandline = false
        }
      }
      // t.form.set(!selected ? null : selected)
      updateSubmit(t)
    }
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.email.get()
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.phone.get()
        let number = '+' + t.phoneCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      case 'landline': {
        const value = t.landline.get()
        let number = '+' + t.landlineCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodLandline = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodLandline = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'phone') {
        let number = '+' + t.phoneCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.phone.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.phone.set(!value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.phone.set(!value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'landline') {
        let number = '+' + t.landlineCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.landline.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.landline.set(!value ? null : value)
            t.goodLandline = false
          }
        } catch (e) {
          t.landline.set(!value ? null : value)
          t.goodLandline = false
        }
      } else if (key === 'email') {
        t.email.set(value)
      } else if (key === 'firstname') {
        t.firstname.set(value)
      } else if (key === 'lastname') {
        t.lastname.set(value)
      }
      updateSubmit(t)
    }
  },
  getPhoneCode: () => {
    const t = Template.instance()
    return () => t.phoneCode.get() || ''
  },
  getEmail: () => {
    const t = Template.instance()
    return () => t.email.get() || ''
  },
  getFirstName: () => {
    const t = Template.instance()
    return () => t.firstname.get() || ''
  },
  getLastName: () => {
    const t = Template.instance()
    return () => t.lastname.get() || ''
  },
  getPhone: () => {
    const t = Template.instance()
    return () => t.phone.get() || ''
  },
  getLandlineCode: () => {
    const t = Template.instance()
    return () => t.landlineCode.get() || ''
  },
  getLandline: () => {
    const t = Template.instance()
    return () => t.landline.get() || ''
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  }
})
