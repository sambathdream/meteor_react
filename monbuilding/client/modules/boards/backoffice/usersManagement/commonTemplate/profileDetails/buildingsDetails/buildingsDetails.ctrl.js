import { CommonTranslation } from "/common/lang/lang.js";

Template.admin_buildingsDetails.onCreated(function () {
  this.userId = FlowRouter.getParam('userId')
  this.searchText = new ReactiveVar('')
})

Template.admin_buildingsDetails.onDestroyed(function () {
})

Template.admin_buildingsDetails.onRendered(() => {
})

Template.admin_buildingsDetails.events({
  'click #defaultContact': function (event, template) {
    event.preventDefault();
    event.stopPropagation();
    const condoId = this._id
    const condo = Condos.findOne(condoId);
    var userDefault = '';
    var self = template;
    let condoContact = CondoContact.findOne({ condoId: condoId })
    if (condoContact) {
      if (condoContact.defaultContact.userId != template.userId) {
        userDefault = Meteor.users.findOne(condoContact.defaultContact.userId);
        bootbox.confirm({
          size: "medium",
          title: "Confirmation",
          message: `${userDefault ? `${userDefault.profile.firstname} ${userDefault.profile.lastname} est déja le contact par défaut de cette résidence. ` : ''}Voulez-vous vraiment que ce gestionnaire devienne le contact par défaut de la résidence ${condo.name} ?`,
          buttons: {
            'cancel': { label: "Non", className: "btn-outline-red-confirm" },
            'confirm': { label: "Oui", className: "btn-red-confirm" }
          },
          backdrop: true,
          callback: function (result) {
            if (result) {
              Meteor.call('changeDefaultContact', condoContact._id, self.userId, (error, result) => {
                if (!error) {
                  Meteor.call('removeCondoContactReferences', self.userId, (error, result) => {
                    if (!error) {
                      sAlert.success('Le gestionnaire par défaut a été changé.');
                    }
                  })
                }
              })
            }
          }
        });
      }
    }
  },
  'click .commonColumn.trashColumn > div': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    const condoId = $(e.currentTarget).data('condoid')

    let isDefaultContact = !!CondoContact.findOne({ condoId: condoId, 'defaultContact.userId': t.userId })
    let condoName = $(e.currentTarget).data('condoname')

    const translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));

    if (isDefaultContact) {
      bootbox.alert({
        size: "medium",
        title: translation.commonTranslation["action_impossible"],
        message: translation.commonTranslation["action_impossible_message_part_one"] + condoName + translation.commonTranslation["action_impossible_message_part_two"],
        backdrop: true
      })
    } else {
      if (Meteor.userHasRight('managerList', 'deleteCondoManager', condoId)) {
        bootbox.confirm({
          size: "medium",
          title: translation.commonTranslation["confirmation"],
          message: translation.commonTranslation["remove_manager_from_building"] + condoName + "</i> ?",
          buttons: {
            'cancel': { label: translation.commonTranslation["cancel"], className: "btn-outline-red-confirm" },
            'confirm': { label: translation.commonTranslation["confirm"], className: "btn-red-confirm" }
          },
          backdrop: true,
          callback: function (result) {
            if (result) {
              Meteor.call('removeManagerFromCondo', t.userId, condoId, (error, result) => {
                if (!error) {
                  sAlert.success(translation.commonTranslation["manager_as_been_removed"]);
                } else {
                  sAlert.error(translation.commonTranslation["alert_error"])
                }

              })
            }
          }
        })
      } else {
        sAlert.warning("Don't take me for a fool, you have no rights to do this")
      }
    }
  }
})

Template.admin_buildingsDetails.helpers({
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  getManagerCondos: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    let userId = Template.instance().userId

    let thisUserCondos = []
    Enterprises.find({'users.userId': userId}).forEach(enterprise => {
      enterprise.users.forEach(user => {
        if (user.userId === userId) {
          thisUserCondos = _.pluck(user.condosInCharge, 'condoId')
        }
      })
    })

    let listCondoId = thisUserCondos

    let listCondos = []

    Condos.find({ _id: { $in: listCondoId } }).forEach(condo => {
      let condoName = !!condo.info.address ? condo.info.address : '-'
      if (condo.name && condo.name !== "" && (condo.info.address && condo.name !== condo.info.address)) {
        condoName = condo.name
        if (condo.info.id && condo.info.id !== '-1') {
          condoName = condo.info.id + ' - ' + condoName
        }
      }
      let adress = condo.info.address + ", " + condo.info.code + " " + condo.info.city
      let userRight = UsersRights.findOne({
        $and: [
          { "userId": userId },
          { "condoId": condo._id }
        ]
      })
      let defaultRoleName = 'non défini'
      if (userRight && userRight.defaultRoleId) {
        let defaultRole = DefaultRoles.findOne({ _id: userRight.defaultRoleId })
        if (defaultRole && defaultRole.name) {
          defaultRoleName = defaultRole.name
        }
      }
      if (condoName.match(regexp) || adress.match(regexp) || defaultRoleName.match(regexp)) {
        listCondos.push({
          _id: condo._id,
          name: condoName,
          adress: adress,
          role: defaultRoleName
        })
      }
    })
    return listCondos
  },
  getTotalCondos: () => {
    return 1
  },
  defaultContact: (condoId) => {
    let condoContact = CondoContact.findOne({ condoId: condoId });
    if (!condoContact)
      return false;
    return condoContact.defaultContact.userId == Template.instance().userId
  }
})
