import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import * as _ from 'underscore'

Template.admin_profileDetails.onCreated(function () {
  this.userId = FlowRouter.getParam('userId')
  this.handler = Meteor.subscribe('UserDocumentsPublishManagerSide', this.userId)
  this.handler2 = Meteor.subscribe('usersProfile')
  this.handler3 = Meteor.subscribe('admin_residents')
  this.handler4 = Meteor.subscribe('admin_condosModulesOptions')
  this.handler5 = Meteor.subscribe('getDefaultRoles')

  let tab = FlowRouter.getParam('tab')
  this.fm = new FileManager(UserFiles, { userId: this.userId, publicPicture: true });

  if (FlowRouter.getRouteName() === 'app.backoffice.managerList.managerProfile') {
    if (!tab || (tab !== 'buildings' && tab !== 'rights' && tab !== 'password')) {
      FlowRouter.setParams({ tab: 'buildings' })
    }
  } else {
    if (!tab || (tab !== 'buildingsOccupant' && tab !== 'rights' && tab !== 'documents' && tab !== 'payments' && tab !== 'password')) {
      FlowRouter.setParams({ tab: 'documents' })
    }
  }
})

Template.admin_profileDetails.onDestroyed(function () {
})

Template.admin_profileDetails.onRendered(() => {
})

Template.admin_profileDetails.events({
  'click .editProfilePicture, click .cancelDropdown'(event, template) {
    $('.dropdownProfilePicture').toggleClass('displayNone')
  },
  'click .uploadPhoto'(event, template) {
    $('#inputAvatar').click();
  },
  'click .removePhoto'(event, template) {
    Meteor.call('setAvatarId', null, template.userId)
  },
  'change #inputAvatar'(event, template) {
    if (event.currentTarget.files && event.currentTarget.files.length === 1) {
      template.fm.insert(event.currentTarget.files[0], function (err, file) {
        event.currentTarget.value = ''
        if (!err && file) {
          template.fm.clearFiles()
          Meteor.call('setAvatarId', file._id, template.userId);
        }
      });
    }
  },
  'click .addBuildingOccupant': (e, t) => {
    $('#add_condo_occupant_modal').modal('show')
  },
  'click .addBuildingManager': (e, t) => {
    $('#add_condo_manager_modal').modal('show')
  },
  'click #backbutton': (event, template) => {
    if (Session.get('lastRoute') && Session.get('lastRoute').name !== FlowRouter.getRouteName()) { // C'EST ULTRA BANCALE !!!!
      const lastRoute = Session.get('lastRoute')
      FlowRouter.go(lastRoute.name, lastRoute.params)
    } else if (FlowRouter.getRouteName() === 'app.backoffice.managerList.managerProfile') {
      FlowRouter.go('app.backoffice.managerList')
    } else {
      FlowRouter.go('app.backoffice.occupantList.tab', { tab: 'active' })
    }
  },
  'click .tabBarContainer > div': (event, template) => {
    let tabName = $(event.currentTarget).data('tab')
    FlowRouter.setParams({tab: tabName})
  }
})

Template.admin_profileDetails.helpers({
  getUserProfile: () => {
    let profile = UsersProfile.findOne({ _id: Template.instance().userId })
    const lang = FlowRouter.getParam('lang') || 'fr'
    if (profile === undefined) {
      if (FlowRouter.getRouteName() === 'app.backoffice.managerList.managerProfile') {
        FlowRouter.go('app.backoffice.managerList', { lang })
      } else {
        FlowRouter.go('app.backoffice.occupantList.tab', { lang, tab: 'active' })
      }
    }
    return profile
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().userId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  selectedTab: () => {
    return FlowRouter.getParam('tab')
  },
  isManagerModule: () => {
    return FlowRouter.getRouteName() === 'app.backoffice.managerList.managerProfile'
  },
  getTabTemplate: () => {
    switch (FlowRouter.getParam('tab')) {
      case 'buildings':
        return 'admin_buildingsDetails'
      case 'buildingsOccupant':
        return 'admin_buildingsOccupantDetails'
      case 'rights':
        return 'admin_rightsDetails'
      case 'documents':
        return 'occupantDocuments'
      case 'payments':
        return 'admin_rightsDetails'
      case 'password':
        return 'admin_changePassword'
      default:
        break
    }
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready() &&
            Template.instance().handler2.ready() &&
            Template.instance().handler3.ready() &&
            Template.instance().handler4.ready() &&
            Template.instance().handler5.ready()
  },
  documentsAvailable: () => {
    const occupant = Residents.findOne({ userId: Template.instance().userId })
    let condoIds = []

    if (occupant) {
      condoIds = _.map(occupant.condos, function (elem) {
        return elem.condoId
      })
    }
    let options = CondosModulesOptions.find({ condoId: { $in: condoIds } }).fetch()
    let ret = false
    _.each(options, function (option) {
      if (option.profile['documents'] === true) {
        ret = true
      }
    })
    if (ret === false && FlowRouter.getParam('tab') === 'documents') {
      Meteor.setTimeout(function () {
        FlowRouter.setParams({ tab: 'buildingsOccupant' })
      }, 100);
    }
    return ret
  }

})
