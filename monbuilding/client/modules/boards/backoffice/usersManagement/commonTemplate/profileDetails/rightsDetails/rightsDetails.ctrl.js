function getMergedRights(roleId, condoId) {
  let currentDefaultRole = DefaultRoles.findOne(roleId);
  if (!currentDefaultRole)
    return undefined;

  let currentCondoRole = CondoRole.findOne({
    condoId,
    for: currentDefaultRole.for,
    name: currentDefaultRole.name
  });

  let mergedRoles = Meteor.mergeRoles(currentDefaultRole, currentCondoRole);
  mergedRoles.rights = _.sortBy(mergedRoles.rights, function (right) {
    return right.displayOrder;
  });
  return mergedRoles;
}

function initiateSaver(name) {
  if (!($("#noty_topRight_layout_container")[0])) {
    $("#noty_topRight_layout_container").remove();
    $(".display-modules").after('<ul id="noty_topRight_layout_container" class="i-am-new" style="top: 20px; right: 20px; position: fixed; width: 310px; height: auto; margin: 0px; padding: 0px; list-style-type: none; z-index: 10000000;"><li id="noty-li" style="overflow: hidden; background: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=&quot;) left top repeat-x scroll lightgreen; border-radius: 5px; border: 1px solid rgb(80, 194, 78); box-shadow: rgba(0, 0, 0, 0.1) 0px 2px 4px; color: darkgreen; width: 310px; cursor: pointer; height: 0px;" class="i-am-closing-now"><div class="noty_bar noty_type_success" id="noty_1199789782275857400"><div class="noty_message" style="font-size: 13px; line-height: 16px; text-align: left; padding: 8px 10px 9px; width: auto; position: relative;"><span class="noty_text">Sauvegarde ' + name + '..</span></div></div></li></ul>');
    $("#noty-li").animate({ "height": "34.4502px" }, "fast");
  }
};

function updateSaver(ret) {
  if (!ret) {
    $("#noty_topRight_layout_container").remove();
  }
  else {
    if (ret.error && ret.error == 403)
      $(".noty_text").html(ret.reason);
    else
      $(".noty_text").html(ret);
    $("#noty-li").css(
      {
        'background': 'url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAoCAQAAAClM0ndAAAAhklEQVR4AdXO0QrCMBBE0bttkk38/w8WRERpdyjzVOc+HxhIHqJGMQcFFkpYRQotLLSw0IJ5aBdovruMYDA/kT8plF9ZKLFQcgF18hDj1SbQOMlCA4kao0iiXmah7qBWPdxpohsgVZyj7e5I9KcID+EhiDI5gxBYKLBQYKHAQoGFAoEks/YEGHYKB7hFxf0AAAAASUVORK5CYII=") left top repeat-x scroll #ff7474',
        'border-radius': '5px',
        'border': '1px solid red',
        'box-shadow': 'rgba(0, 0, 0, 0.1) 0px 2px 4px',
        'color': 'red',
        'width': '310px',
        'cursor': 'pointer',
        'height': '34.4502px'
      }
    );
    setTimeout(function () {
      $("#noty_topRight_layout_container").remove();
    }, 3000)
  }
};

Template.admin_rightsDetails.onCreated(function () {
  this.userId = FlowRouter.getParam('userId')
  this.profile = UsersProfile.findOne({ _id: this.userId })
  this.currentDisplayingModule = null
  this.condoId = new ReactiveVar(FlowRouter.getParam('condoId') || '')
  this.roleId = new ReactiveVar(null)
  this.hasNoCondo = new ReactiveVar(false)
  this.hasNoRight = new ReactiveVar(false)

  this.board = FlowRouter.getRouteName() === 'app.backoffice.managerList.managerProfile' ? 'manager' : 'occupant'
})

Template.admin_rightsDetails.onDestroyed(function () {
})

Template.admin_rightsDetails.onRendered(() => {
})

Template.admin_rightsDetails.events({
  'click .labelForCheckbox': function (event, template) {
    const userId = template.userId
    const condoId = template.condoId.get()
    const roleId = template.roleId.get()

    const resourceId = event.currentTarget.getAttribute('data-id');
    const edition = $(event.currentTarget).attr('for').split('.');
    edition[2] = edition[2] == "true" ? true : edition[2] == "false" ? false : null;

    const rightName = $(event.currentTarget).data('display')

    initiateSaver('du droit: ' + rightName.slice(0, 20))

    console.log('condoId', condoId)
    Meteor.call('modifyOneRight', userId, condoId, roleId, edition[0], edition[1], !edition[2], (error, result) => {
      if (!error && result !== false) {
        Meteor.setTimeout(function () {
          updateSaver(error);
        }, 300);
      }
    })
  },
})

Template.admin_rightsDetails.helpers({
  getUserCondos: () => {
    let userCondosIds = []
    let template = Template.instance()
    if (template.board === 'manager') {
      Enterprises.find({ 'users.userId': template.userId }).forEach(enterprise => {
        userCondosIds.push(..._.pluck(enterprise.users.find(user => user.userId === template.userId).condosInCharge, 'condoId'))
      })
    } else {
      const user = Residents.findOne({ userId: template.userId })
      const condos = [...user.condos, ...user.pendings]
      userCondosIds = _.pluck(condos, 'condoId')
    }
    return userCondosIds
  },
  getManagerName: () => {
    return Template.instance().profile.firstname + ' ' + Template.instance().profile.lastname
  },
  condoIdCb: () => {
    let template = Template.instance()
    return (condoId) => {
      if (condoId === 'all') {
        template.hasNoCondo.set(true)
      } else {
        let userRight = UsersRights.findOne({
          $and: [
            { "userId": template.userId },
            { "condoId": condoId }
          ]
        });
        if (userRight && (template.roleId.get() !== userRight.defaultRoleId || template.condoId.get() !== condoId)) {
          template.roleId.set(null)
          Meteor.defer(() => {
            template.hasNoCondo.set(false)
            template.hasNoRight.set(false)
            template.roleId.set(userRight.defaultRoleId)
            template.condoId.set(condoId)
          })
        } else {
          template.condoId.set(condoId)
          template.hasNoRight.set(true)
        }
      }
    }
  },
  iscurrentDisplayingModule: (moduleName) => {
    if (Template.instance().currentDisplayingModule == moduleName && moduleName !== 'view')
      return true;
    Template.instance().currentDisplayingModule = moduleName;
    return false;
  },
  getUserRights: () => {
    let userRight = UsersRights.findOne({
      $and: [
        { "userId": Template.instance().userId },
        { "condoId": Template.instance().condoId.get() }
      ]
    });

    Template.instance().roleId.set(userRight.defaultRoleId)
    let condoRights = getMergedRights(userRight.defaultRoleId, Template.instance().condoId.get());
    let merged = condoRights;
    if (!merged) {
      return undefined
    }

    const rightNameToModuleName = {
      emergencyContact: 'emergencyContact',
      wallet: 'digitalWallet',
      marketPlace: 'marketPlace',
      incident: 'incidents',
      actuality: 'informations',
      manual: 'manual',
      map: 'map',
      trombi: 'trombi',
      messenger: 'messenger',
      reservation: 'reservations',
      forum: 'forum',
      ads: 'classifieds',
      print: 'print'
    }
    const condo = Condos.findOne({ _id: Template.instance().condoId.get() })

    _.each(merged.rights, function (right, key) {
      const moduleName = rightNameToModuleName[right.module] || null
      let isActive = true
      if (condo) {
        if (moduleName && condo.settings && condo.settings.options) {
          isActive = condo.settings.options[moduleName];
        }
      }
      if (!isActive) {
        merged.rights[key] = null
      } else {
        // if (right.module === 'view' && right.right === 'concierge') {
        //   merged.rights[key] = null
        //   return
        // }
        if (userRight && userRight.module && userRight.module[right.module] && _.isBoolean(userRight.module[right.module][right.right])) {
          merged.rights[key].default = userRight.module[right.module][right.right];
        }
      }
    });
    merged.rights = _.without(merged.rights, null)
    console.log('merged', merged);
    return merged;
  },
  onSelectRole: () => {
    const t = Template.instance()

    return () => roleId => {
      const userId = t.userId
      const condoId = t.condoId.get()

      if (!!roleId && t.roleId.get() !== roleId) {
        initiateSaver('du rôle')
        Meteor.call('modifyDefaultRoleIdForUser', userId, condoId, roleId, (error, result) => {
          if (!error) {
            t.roleId.set(null)
            Meteor.defer(() => {
              t.roleId.set(roleId)
            })
          }
          Meteor.setTimeout(function () {
            updateSaver(error);
          }, 300);
        })
      }
    }
  },
  getroleOptions: () => {
    const condoId = Template.instance().condoId.get()
    const condo = Condos.findOne({ _id: condoId }, { fields: { 'settings': true } })
    const condoType = condo && (condo.settings.condoType === 'etudiante' ? 'student' : condo.settings.condoType)
    let listRoles = DefaultRoles.find({ for: Template.instance().board, ['forCondoType.' + condoType]: true })
    let roles = []
    if (listRoles) {
      listRoles.forEach(role => {
        roles.push({
          text: role.name,
          id: role._id
        })
      })
    }
    return roles
  },
  getRole: () => {
    return Template.instance().roleId.get()
  },
  isRoleSelected: () => {
    return !!Template.instance().roleId.get()
  },
  hasNoCondo: () => {
    return !!Template.instance().hasNoCondo.get()
  },
  hasNoRight: () => {
    return !!Template.instance().hasNoRight.get()
  },
  getCondoId: () => {
    return Template.instance().condoId.get()
  },
  viewName: (moduleName, displayOrder) => {
    return moduleName + displayOrder
  },
  getBoard: () => {
    return Template.instance().board
  },
})
