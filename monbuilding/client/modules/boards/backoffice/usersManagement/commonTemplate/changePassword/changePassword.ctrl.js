import { CommonTranslation } from "/common/lang/lang.js"

Template.admin_changePassword.onCreated(function () {
  this.newPassword = new ReactiveVar(null)
  this.newPasswordCheck = new ReactiveVar(null)
  this.canSubmit = new ReactiveVar(false)
})

Template.admin_changePassword.onDestroyed(() => {
})

Template.admin_changePassword.onRendered(() => {
})

Template.admin_changePassword.events({
  'click .saveContact': (e, t) => {
    const canSubmit = t.canSubmit.get()

    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (canSubmit) {
      const newPassword = t.newPassword.get()
      const userId = FlowRouter.getParam('userId')
      Meteor.call('changeUsersPassword', userId, newPassword, (error, result) => {
        if (error) {
          sAlert.error(error.reason);
        }
        else {
          sAlert.success(tr_common.commonTranslation["modif_success"]);
        }
      });
    }

  }
})

function updateSubmit(template) {
  let canSubmit = false
  const newPassword = template.newPassword.get()
  const newPasswordCheck = template.newPasswordCheck.get()
  canSubmit =
    !!newPassword &&
    !!newPasswordCheck &&
    newPassword === newPasswordCheck
  template.canSubmit.set(canSubmit)
}

Template.admin_changePassword.helpers({
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'newPassword': {
        const newPassword = t.newPassword.get()
        if (newPassword === null || !!newPassword) {
          return false
        } else {
          return true
        }
      }
      case 'newPasswordCheck': {
        const newPassword = t.newPassword.get()
        const newPasswordCheck = t.newPasswordCheck.get()
        if (newPasswordCheck === null || !!newPasswordCheck) {
          if (newPassword !== newPasswordCheck) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'newPassword') {
        t.newPassword.set(value)
      } else if (key === 'newPasswordCheck') {
        t.newPasswordCheck.set(value)
      }
      updateSubmit(t)
    }
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  }
})
