import { Template } from "meteor/templating";
import { MarketBasket } from '/common/collections/marketPlace'
import { ModuleManager} from "./moduleManager.js";
import "./board.css";
import "./board.view.html";
import { Session } from 'meteor/session';
import Forum from '/client/modules/boards/common/forum/component/contextProvider';
import DetailsForumPost from '/client/modules/boards/common/forum/component/detail_forum_post';
import { ForumLang } from '/common/lang/lang'

const _passwordMessageClosed = 'passwordMessageClosed'

Template.board.onRendered(function() {
	var template = Template.instance();

	Meteor.call('isAdmin', Meteor.userId(), function(error, result) {
		template.isResidentAsAdmin.set(result);
	});
});

let PreviousCondo = null;

Template.board.onCreated(function() {
  Meteor.isAdmin = false
	Meteor.isManager = false;
	this.manager = new ModuleManager("resident");
	this.condos = new ReactiveVar([]);
	this.specificModule = new ReactiveVar("");
	this.condoId = new ReactiveVar("all");
	this.condoSearch = new ReactiveVar("");
	this.condoLength = new ReactiveVar(1);
	this.isResidentAsAdmin = new ReactiveVar(false);

	this.passwordMessageClosed = localStorage.getItem(_passwordMessageClosed) || false

	let condoId = FlowRouter.getParam('condo_id');

  this.subscribe("old-data", () => {
    this.subscribe('board_resident', () => {
      const resident = Residents.findOne({userId: Meteor.userId()});
      const condos = resident.getCondos();
      this.condos.set(condos);
      this.autorun(onPathChange.bind(this));
    });
  });
	this.subscribe("avatarLinks", condoId);
	this.subscribe('getDefaultRolesFor', "occupant");
  this.subscribe('getCondoRole', condoId);
	// this.subscribe('userWithJoinedDate', condoId);
	this.subscribe('userStatus');
	this.subscribe('usersProfile', [condoId]);
	// @KUYA HERE YOU GO
  Meteor.subscribe('market_place_basket', condoId)
	Meteor.subscribe('badges', condoId, () => {
	// 	// console.log('new Date()2', moment(Date.now()).format("ssSSSS"))
	});

  this.autorun(() => {
		const currentUser = Meteor.user()

		if (
			currentUser &&
			currentUser.services &&
			currentUser.services.password &&
			!currentUser.services.password.reset
		) {
			this.passwordMessageClosed = true
		}
    // set color palette based on condo settings
    const id = Session.get('selectedCondo') || FlowRouter.getParam('condoId') || FlowRouter.getParam("id") || FlowRouter.getParam("condo_id") || FlowRouter.getQueryParam("id")

    let palette = Session.get('defaultColorPalette');
    if (!!id && id !== 'all') {
      const condo = Condos.findOne(id);
      if (!!condo && !!condo.settings && !!condo.settings.colorPalette) {
        palette = condo.settings.colorPalette
      }
    }

    // document.body.style.setProperty("--column-background-color", palette.columnBackground);
    // document.body.style.setProperty("--menu-selected-item-color", palette.menuSelectedItem);
    // document.body.style.setProperty("--accent-color", palette.accent);
    // document.body.style.setProperty("--button-color", palette.button);
    // document.body.style.setProperty("--tab-color", palette.tab);
    // document.body.style.setProperty("--notification-color", palette.notification);

    document.body.style.setProperty("--primary-color", palette.primary);
    document.body.style.setProperty("--secondary-color", palette.secondary);
    document.body.style.setProperty("--third-color", Session.get('defaultColorPalette').third || '#fdb813');
  })
});

function onPathChange() {
	FlowRouter.watchPathChange();
	var currentContext = FlowRouter.current();
	if (currentContext && currentContext.params) {
		const condo_id = currentContext.params.condo_id;
		let module_slug = currentContext.params.module_slug;
		const building_id = currentContext.params.building_id;
		const incidentId = currentContext.params.incidentId;

		/*If condo change in url*/
		/*load the condo, + load the modules*/
		$("#menu-content li").removeClass("highlight");
    $("#menu-content #feed").removeClass("highlight");

    // $('.resident-dropdown a.btn').removeClass('sel')
    // if (module_slug === 'messenger') {
    //   $('.resident-dropdown a.btn').addClass('sel')
    // }

    if (module_slug === undefined) {
      module_slug = FlowRouter.current().path.split('/')[4]
    }
    if (module_slug === 'detailUser' || module_slug === 'createOccupant') {
      $(`#menu-content li a[slug='occupantList']`).parent().addClass('highlight');
    } else if (module_slug) {
      $(`#menu-content li a[slug='${module_slug}']`).parent().addClass('highlight');
    } else {
      $("#menu-content #feed").addClass("highlight");
    }
		const condos = this.condos.get();
		if (condo_id) {
			this.manager.changeCondo(condo_id);
			if (building_id)
				this.manager.changeBuilding(building_id);
		} else if (condos.length === 1 && Meteor.loggingIn()) {
			const lang = (FlowRouter.getParam("lang") || "fr");
			let params = null;
			if (incidentId) {
				params = {
					lang,
					condo_id: condos[0]._id,
					incidentId: incidentId
				};
			}
			else {
				params = {
					lang,
					condo_id: condos[0]._id,
				};
			}
			if (params) {
        FlowRouter.go('app.board.resident.condo', params);
      }
			return true;
		}

		/*If a module is specified in the url*/
		/*Load it as 'currentModule' reactiveVar*/
		if (module_slug) {
			if (currentContext.route.name === "app.board.resident.condo.module") {
        // @All I HATE THE MAN WHO DID THIS SHITTY ROUTE STRUCTURE, HE SHOULD BURN IN HELL !!!
        this.specificModule.set(false);

        // console.log(this.manager.currentModule.get())

        this.manager.changeModule(module_slug, 'condo');
        // console.log(this.manager.currentModule.get())
      }
			else
				this.manager.changeModule(module_slug, 'building');
		}
		else {
			this.manager.changeModule(null);
		}
	}
};


Template.board.events({
	'click .password-message__redirect': (e, t) => {
		const lang = (FlowRouter.getParam("lang") || "fr")
    t.specificModule.set("occupantProfile")
		FlowRouter.go('app.board.resident.condo.module.profile.tab', {lang, condo_id: t.manager.currentCondo.get()._id, tab: 'password'})
	},
	'click .password-message__close': (e, t) => {
		localStorage.setItem(_passwordMessageClosed, true)
		$('.password-message').hide()
	},
	'mouseenter .dropdown-item': function (event, template) {
		let menu = $(event.currentTarget).children('.badge');
		if (!menu.hasClass('unicBadgeReverse'))
			menu.toggleClass('unicBadge unicBadgeReverse');
	},

	'mouseleave .dropdown-item': function (event, template) {
		let menu = $(event.currentTarget).children('.badge');
		if (menu.hasClass('unicBadgeReverse'))
			menu.toggleClass('unicBadge unicBadgeReverse');
	},
	'click': function(event, template){
		var $wrapper =  $('.helpContent.helpButton');
		var $target  =  $(event.currentTarget);
		var $wrapper2 =  $('.helpContent.couronneButton');
		if ($(event.currentTarget).hasClass("menu-two")) {
			let lang = FlowRouter.getParam('lang');
			if ( lang == 'fr'){
				$("#langFr").css({background: "#FFFFFF"});
				$("#langEn").css({background: "#FFFFFF"});
			}

			else {
				$("#langEn").css({background: "#FFFFFF"});
				$("#langFr").css({background: "#FFFFFF"});
			}
			if ($(".helpContent.helpButton").css("display") == "none") {
				$(".helpContent.couronneButton").css({display: "none"});
				$(".helpContent.helpButton").css({display: "flex"});
			}
			else
				$(".helpContent.helpButton").css({display: "none"});
		}
		else if ($(event.currentTarget).hasClass("menu-three")) {
			if ($(".helpContent.couronneButton").css("display") == "none") {
				$(".helpContent.helpButton").css({display: "none"});
				$(".helpContent.couronneButton").css({display: "flex"});
			}
			else
				$(".helpContent.couronneButton").css({display: "none"});
		}
		else if ($(event.currentTarget).hasClass("helpButton") == true && !$.contains($wrapper[0], $target[0]) ) {
			$(".helpContent.helpButton").css({display: "none"});
		}
		else if ($(event.currentTarget).hasClass("couronneButton") == true && !$.contains($wrapper2[0], $target[0])){
			$(".helpContent.couronneButton").css({display: "none"});
		}
  },

  'click .walletContainer': (e, t) => {
    t.specificModule.set(false);
    const lang = (FlowRouter.getParam("lang") || "fr");
    const params = {
      lang,
      condo_id: t.manager.currentCondo.get()._id,
      module_slug: "wallet",
      wildcard: 'stats'
    };
    FlowRouter.go('app.board.resident.condo.module', params);

  },

  'click .marketContainer': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const params = {
      lang,
      condo_id: t.manager.currentCondo.get()._id,
    }
    FlowRouter.go('app.board.resident.condo.module.marketBasket', params);

  },

  'click .printContainer': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const params = {
      lang,
      condo_id: t.manager.currentCondo.get()._id,
      tab: '1'
    }
    FlowRouter.go('app.board.resident.condo.module.print', params);

  },

	'click .menu-close-cross, click .btn-close-menu': function(event, template) {
		$(".helpContent.helpButton").css({display: "none"});
		$(".helpContent.couronneButton").css({display: "none"});
	},

	// 'click .messengertab': function (e, t) {
	// 	t.specificModule.set(false);
	// 	const lang = (FlowRouter.getParam("lang") || "fr");
	// 	const params = {
	// 		lang,
	// 		condo_id: t.manager.currentCondo.get()._id,
	// 		module_slug: "messagerie"
	// 	};
	// 	Session.set("messagerie", e.currentTarget.getAttribute("tab"));
	// 	/*Session.set("createMessage", undefined);*/
	// 	FlowRouter.go('app.board.resident.condo.module', params);
	// },

	'click .messengertab': function (e, t) {
		t.specificModule.set(false);
		const lang = (FlowRouter.getParam("lang") || "fr");
		const params = {
			lang,
			condo_id: t.manager.currentCondo.get()._id,
			module_slug: "messenger",
			wildcard: e.currentTarget.getAttribute("tab")
		};
		FlowRouter.go('app.board.resident.condo.module', params);
	},
	'click .toggle': function(event, template) {
		var Event = event;
		setTimeout(function() {
			if ($(Event.target.parentElement).hasClass('collapsed') || $(Event.target.parentElement.parentElement).hasClass('collapsed')) {
				$($(Event.target).find('.arrow-down')[0]).addClass('arrow-right');
				$($(Event.target).find('.arrow-right')[0]).removeClass('arrow-down');
				if ($(Event.target).hasClass('arrow')){
					$(Event.target).addClass('arrow-right');
					$(Event.target).removeClass('arrow-down');
				}
			}
			else {
				$($(Event.target).find('.arrow-right')[0]).addClass('arrow-down');
				$($(Event.target).find('.arrow-down')[0]).removeClass('arrow-right');
				if ($(Event.target).hasClass('arrow')){
					$(Event.target).addClass('arrow-down');
					$(Event.target).removeClass('arrow-right');
				}
			}
		}, 100);
	},
	'click li .pull-left': function(event, template) {
		$("#nav_side_menu").toggleClass("appear");
		$("#mainContent").toggleClass("move");
	},
	'click #langFr': function(event, template) {
		Meteor.users.update(Meteor.userId(), {$set: {"profile.lang": "fr"}});
		FlowRouter.setParams({lang: 'fr'});
		$("#langFr").css({background: "#FFFFFF"});
		$("#langEn").css({background: "#FFFFFF"});
	},
	'click #langEn': function(event, template) {
		Meteor.users.update(Meteor.userId(), {$set: {"profile.lang": "en"}});
		FlowRouter.setParams({lang: 'en'});
		$("#langFr").css({background: "#FFFFFF"});
		$("#langEn").css({background: "#FFFFFF"});
	},
	'click .closeNav': function(event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		template.specificModule.set(false);
		var modules = null;
		for (var i = 0; i !== template.manager.currentCondo.get().modules.length; i++) {
			if ((template.manager.currentCondo.get().modules[i]).slug === event.currentTarget.getAttribute("slug")) {
				modules = template.manager.currentCondo.get().modules[i];
				break;
			}
		}
		if ($(window).innerWidth() < 1025) {
			$("#nav_side_menu").toggleClass("appear");
			$("#mainContent").toggleClass("move");
		}

		if (modules) {
			FlowRouter.go('app.board.resident.condo.module', {
				lang: lang,
				condo_id: template.manager.currentCondo.get()._id,
				module_slug: modules.slug,
			});
			return ;
		}
		else {
			if (event.currentTarget.getAttribute("slug") === "fil") {
				FlowRouter.go('app.board.resident.condo', {
					lang: lang,
					condo_id: template.manager.currentCondo.get()._id
				});
				return ;
			}
			// God forbid who ever construct this routing system in the first place :|
      // AGREED!!
			else if (event.currentTarget.getAttribute("slug") === "manual"
        || event.currentTarget.getAttribute("slug") === "map"
        || event.currentTarget.getAttribute("slug") === "concierge"
        || event.currentTarget.getAttribute("slug") === "forum"
        || event.currentTarget.getAttribute("slug") === "manual_plan"
        || event.currentTarget.getAttribute("slug") === "emergency"
      ) {
				template.specificModule.set("module_" + event.currentTarget.getAttribute("slug"));
        if (FlowRouter.getParam('wildcard')) {
          FlowRouter.go('app.board.resident.condo.module.' + event.currentTarget.getAttribute("slug"), {
            lang: lang,
            condo_id: template.manager.currentCondo.get()._id,
            wildcard: FlowRouter.getParam('wildcard')
          });
        } else {
          FlowRouter.go('app.board.resident.condo.module.' + event.currentTarget.getAttribute("slug"), {
            lang: lang,
            condo_id: template.manager.currentCondo.get()._id
          });
        }
				return ;
      } else if (event.currentTarget.getAttribute("slug") === "occupantList") {
        template.specificModule.set(event.currentTarget.getAttribute("slug") + 'ForOccupant');
        FlowRouter.go('app.board.resident.condo.module', {
          lang: lang,
          condo_id: template.manager.currentCondo.get()._id,
          module_slug: event.currentTarget.getAttribute("slug"),
          wildcard: 'active'
        });
        return;
      } else if (event.currentTarget.getAttribute("slug") === "marketPlace") {
        template.specificModule.set('occupant_marketPlace');
        FlowRouter.go('app.board.resident.condo.module', {
          lang: lang,
          condo_id: template.manager.currentCondo.get()._id,
          module_slug: event.currentTarget.getAttribute("slug"),
          wildcard: 'services'
        });
        return;
      } else if (event.currentTarget.getAttribute("slug") === "laundryRoom") {
        template.specificModule.set('laundryRoom');
        FlowRouter.go('app.board.resident.condo.module', {
          lang: lang,
          condo_id: template.manager.currentCondo.get()._id,
          module_slug: event.currentTarget.getAttribute("slug"),
          wildcard: ''
        });
        return;
      }
		}
	},
	'click #disconnectButton': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Meteor.call('isAdmin', Meteor.userId(), function(error, result) {
			if (result == true) {
				FlowRouter.go('app.backoffice.manager');
			}
			else {
        const lang = (FlowRouter.getParam("lang") || "fr");
        $('.disconnecting').css('display', 'block')
				Session.set('target_url', null);
				Meteor.logout(() => {
          $('.disconnecting').css('display', 'none')
					FlowRouter.go('app.home', {lang});
				});
			}
		})

    const palette = Session.get('defaultColorPalette');
    // document.body.style.setProperty("--column-background-color", palette.columnBackground);
    // document.body.style.setProperty("--menu-selected-item-color", palette.menuSelectedItem);
    // document.body.style.setProperty("--accent-color", palette.accent);
    // document.body.style.setProperty("--button-color", palette.button);
    // document.body.style.setProperty("--tab-color", palette.tab);
    // document.body.style.setProperty("--notification-color", palette.notification);

    document.body.style.setProperty("--primary-color", palette.primary);
    document.body.style.setProperty("--secondary-color", palette.secondary);
    document.body.style.setProperty("--third-color", palette.third || '#fdb813');
	},
	'click #goCgu': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Session.set('backFromCgu', FlowRouter.current().path);
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},
	'click #goProfile': function(e, t) {
		const lang = (FlowRouter.getParam("lang") || "fr");
    t.specificModule.set("occupantProfile");
		FlowRouter.go('app.board.resident.condo.module.profile.tab', {lang, condo_id: t.manager.currentCondo.get()._id, tab: 'documents'});
	},
	'click .selectCondo': function(event, template) {
		const condo = {
			lang: (FlowRouter.getParam("lang") || "fr"),
			condo_id: event.currentTarget.getAttribute('index'),
		};
		if (FlowRouter.getParam('condo_id') != condo.condo_id) {
			PreviousCondo = FlowRouter.getParam('condo_id');
			Meteor.call('updateLastKnownCondo', condo.condo_id, function(error, result) {
				if (!error) {
					FlowRouter.go('app.board.resident.condo', condo);
					location.reload();
				}
			})
		}
	},
	'click .logoGoHome': function(event, template) {
		FlowRouter.go('app.board.resident.condo', {lang: FlowRouter.getParam("lang") || "fr", "condo_id": FlowRouter.getParam("condo_id")});
	},
});

const badgesModulesToCondoSettings = {
  ads: 'classifieds',
  myads: 'classifieds',
  forum: 'forum',
  myforum: 'forum',
  incidentResident: 'incidents',
  information: 'informations',
  manual: 'manual',
  map: 'map',
  newUser: 'no_module',
  messagerieGardien: 'messengerGardien',
  messagerieGestionnaire: 'messengerGestionnaire',
  messagerieResident: 'messengerResident',
  messagerieConseilSyndical: 'messengerSyndic',
  reservation: 'reservations',
  reservation_reject: 'reservations'
}


Template.board.helpers({
	closedMessage: () => {
		return !Template.instance().passwordMessageClosed
	},
	currentCondo: () => {
		return Template.instance().manager.currentCondo.get();
	},
	currentModule: () => {
		// console.log(Template.instance().manager.currentModule.get())
		return Template.instance().manager.currentModule.get();
	},
	user: () => {
		return (Meteor.user() && Meteor.user().profile) ? Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname : "";
	},
	getBadge: (module) => {
    // TODO: @Kevin logic is bad and is not needed, as the badge is not even requested if module is not available
    // const currentCondoSettings = (Condos.findOne({ _id: FlowRouter.getParam('condo_id') }, { fields: { settings: true } }) || {}).settings
    // if (badgesModulesToCondoSettings[module] === 'no_module' || (currentCondoSettings && currentCondoSettings.options && currentCondoSettings.options[badgesModulesToCondoSettings[module]] === true) ) {
      let badges = BadgesResident.findOne({ '_id': FlowRouter.getParam('condo_id') });

      if (badges) {
        if (module === "actuality") {
          // debugger
          let event = badges['event'] ? +badges['event'] : 0
          let information = badges['information'] ? +badges['information'] : 0
          return (event + information) > 0 ? event + information : ''
        } else if (module === "forum") {
          let forum = badges['forum'] ? +badges['forum'] : 0
          let myforum = badges['myforum'] ? +badges['myforum'] : 0
          return (forum + myforum) > 0 ? forum + myforum : ''
        } else if (module === "classifieds") {
          let ads = badges['ads'] ? +badges['ads'] : 0
          let myads = badges['myads'] ? +badges['myads'] : 0
          return (ads + myads) > 0 ? ads + myads : ''
        } else if (module === "reservation") {
          let reservation = badges['reservation'] ? +badges['reservation'] : 0
          let reservation_reject = badges['reservation_reject'] ? +badges['reservation_reject'] : 0
          return (reservation + reservation_reject) > 0 ? reservation + reservation_reject : ''
        } else if (module === 'manual') {
          const manual = badges['manual'] || 0;
          const map = badges['map'] || 0;
          const total = manual + map;
          return (total > 0 ? total : "");
        } else {
          return (badges[module] && badges[module] > 0 ? badges[module] : "");
        }
      }
		// } else {
    //   return ''
    // }
	},
	specificModule: () => {
		var currentContext = FlowRouter.current();
		Meteor.defer(function() {
			if (currentContext && currentContext.params) {
				const condo_id = currentContext.params.condo_id;
				const module_slug = currentContext.params.module_slug;
				const building_id = currentContext.params.building_id;
				const incidentId = currentContext.params.incidentId;

				/*If condo change in url*/
				/*load the condo, + load the modules*/
				$("#menu-content li").removeClass("highlight");
				$("#menu-content #feed").removeClass("highlight");
        // $('.resident-dropdown a.btn').removeClass('sel')
        // if (module_slug === 'messenger') {
        //   $('.resident-dropdown a.btn').addClass('sel')
        // }
        if (module_slug === 'detailUser' || module_slug === 'createOccupant') {
          $(`#menu-content li a[slug='occupantList']`).parent().addClass('highlight');
        } else if (module_slug) {
          $(`#menu-content li a[slug='${module_slug}']`).parent().addClass('highlight');
        } else {
          $("#menu-content #feed").addClass("highlight");
        }
			}
		})
		return Template.instance().specificModule.get();
	},
	getCondo: () => {
		return Condos.findOne();
	},
	condoOptions: (option) => {
		let manager = Template.instance().manager;
		if (manager) {
			let currentCondo = manager.currentCondo.get();
			if (currentCondo) {
				let condo = Condos.findOne(currentCondo._id);
				if (condo) {
					return condo.settings.options[option];
				}
			}
		}
	},
	condoOptionsTrombi: () => {
		let manager = Template.instance().manager;
		if (manager) {
			let currentCondo = manager.currentCondo.get();
			if (currentCondo) {
				let condo = Condos.findOne(currentCondo._id);
				if (condo) {
					let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
					let supervisorRole = DefaultRoles.findOne({for: "occupant", "name": "Gardien"});
					if (condo.settings.options["trombiAll"])
						return true;
					else if ((condo.settings.options["trombiKeeper"] && supervisorRole.isUserMember(Meteor.userId(), currentCondo._id)) ||
						(condo.settings.options["trombiCS"] && syndicalRole.isUserMember(Meteor.userId(), currentCondo._id)))
						return true;
					return false;
				}
			}
		}
	},
	existCondoOptions: (options) => {
		let manager = Template.instance().manager;
		if (manager) {
			let currentCondo = manager.currentCondo.get();
			if (currentCondo) {
				let condo = Condos.findOne(currentCondo._id);
				if (condo) {
					for (option of options) {
						if (condo.settings.options[option]) {
							return true;
						}
					}
				}
			}
		}
		return false;
	},
	existOneSyndic: () => {
		let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
		if (syndicalRole)
			return syndicalRole.existOneUserInCondo(FlowRouter.getParam("condo_id"));
		return false;
	},
	existOneKeeper: () => {
		let supervisorRole = DefaultRoles.findOne({for: "occupant", "name": "Gardien"});
		if (supervisorRole)
			return supervisorRole.existOneUserInCondo(FlowRouter.getParam("condo_id"));
		return false;
	},
	addBadge: (gestionnaire, resident, conseilSyndical, gardien) => {
		return parseInt(gestionnaire || 0) + parseInt(resident || 0) + parseInt(conseilSyndical || 0) + parseInt(gardien || 0);
	},
	getResidences: () => {
		let resident = Residents.findOne({userId: Meteor.userId()});
		if (resident) {
			resident = _.map(resident.condos, function(elem) {
				return elem.condoId;
			});
			if (resident) {
				let condo = Condos.find({_id: {$in: resident}}).fetch();
				Template.instance().condoLength.set(condo.length)
				return (condo);
			}
		}
	},
	getResidencesLength: () => {
		return Template.instance().condoLength.get();
	},
	getResidencesName: (condoId) => {
		let condo = Condos.findOne(condoId);
		if ((condo && condo.info && condo.name && condo.info.address && condo.name && condo.info.address == condo.name))
			return condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city;
		else if (condo && condo.name)
			return condo.name;
	},
	getUserId: () => {
		return Meteor.userId();
	},
  showLogo: () => {
    const condoId = FlowRouter.getParam('condo_id')
    if (!Enterprises.findOne({ condos: condoId }, { fields: { _id: true } })) {
      return '/logo/enterpriseLarge.png'
    }

    const condoLogos = CondoLogos.findOne({ condoId: condoId })

    if (condoLogos && condoLogos.avatar && condoLogos.avatar.original) {
      return condoLogos.avatar.original
    }
    return '/logo/enterpriseLarge.png'
	},
	isAdminConnected: () => {
		return Template.instance().isResidentAsAdmin.get();
	},
	isSubscribeReady: () => {
		let isReady = Template.instance().subscriptionsReady()
		if (isReady === true) {
			if (Condos.findOne(FlowRouter.getParam('condo_id')) === undefined) {
				let newCondoId = Condos.findOne()
				if (newCondoId) {
					newCondoId = newCondoId._id
					Meteor.call('updateLastKnownCondo', newCondoId, function (error, result) {
						if (!error) {
							FlowRouter.go('app.board.resident.condo', newCondoId);
							location.reload();
						}
					})
				}
			}
		}
		return isReady
	},
	reactComponent: () => {
		return Forum;
	},
	getDicoLang: (moduleName) => {
		if (moduleName == 'module_forum')
			return new ForumLang(FlowRouter.getParam('lang') || 'fr')
	},
	getSrcFR: () => {
		// const lang = FlowRouter.getParam('lang') || 'fr'
		// if (lang === 'fr') {
		// 	return '/img/icons/svg/flag-fr-sel.svg'
		// } else {
			return '/img/icons/svg/flag-fr-unsel.svg'
		// }
	},
	getSrcEN: () => {
		// const lang = FlowRouter.getParam('lang') || 'fr'
		// if (lang === 'en') {
		// 	return '/img/icons/svg/flag-en-sel.svg'
		// } else {
			return '/img/icons/svg/flag-en-unsel.svg'
		// }
  },
  getDefaultRoleOccupant: () => {
    let defaultRoles = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })
    if (!!defaultRoles === true) {
      return defaultRoles._id
    }
  },
  getDefaultRoleUnionCouncil: () => {
    let defaultRoles = DefaultRoles.findOne({ name: 'Conseil Syndical' }, { fields: { _id: true } })
    if (!!defaultRoles === true) {
      return defaultRoles._id
    }
  },
  getDefaultRoleSupervisor: () => {
    let defaultRoles = DefaultRoles.findOne({ name: 'Gardien' }, { fields: { _id: true } })
    if (!!defaultRoles === true) {
      return defaultRoles._id
    }
  },
  langSelected: (lang) => {
    return lang === (FlowRouter.getParam('lang') || 'fr')
  },
  subTemplate: () => {
    return !!Template.currentData().sub_template
  },
  getSubTemplate: () => {
    const module_slug = FlowRouter.current().path.split('/')[4]
    if (module_slug) {
      Meteor.defer(() => {

        $("#menu-content li").removeClass("highlight");
        $("#menu-content #feed").removeClass("highlight");

        // $('.resident-dropdown a.btn').removeClass('sel')
        // if (module_slug === 'messenger') {
        //   $('.resident-dropdown a.btn').addClass('sel')
        // }

        if (module_slug === 'detailUser' || module_slug === 'createOccupant') {
          $(`#menu-content li a[slug='occupantList']`).parent().addClass('highlight');
        } else if (module_slug) {
          $(`#menu-content li a[slug='${module_slug}']`).parent().addClass('highlight');
        } else {
          $("#menu-content #feed").addClass("highlight");
        }
      })
    }
    return Template.currentData().sub_template
  },
  getMarketPlaceInfo: () => {
    let total = 0
    let badge = 0
    const basket = MarketBasket.findOne({ condoId: FlowRouter.getParam('condo_id'), status: 'in_progress' })

    const hasBook = basket && basket.services && basket.services.length > 0 && !!basket.services.find(s => (s.pay_outside_platform === true))
    const isBook = hasBook && !!basket.services.find(s => (s.pay_outside_platform === true && s.validatedAt === null))

    if (basket && basket.services) {
      basket.services.forEach(s => {
        // if (isBook && !s.pay_outside_platform) return
        if (!isBook && s.pay_outside_platform) return

        badge++
        total += parseFloat(s.price) * s.quantity
      })
    }
    return {
      badge: badge || null,
      total: parseFloat(total).toFixed(2).toString() + ' €'
    }
  },
  isModuleSelected: (moduleName) => {
    const routeName = FlowRouter.getRouteName()
    if (moduleName === 'marketPlace' && routeName === 'app.board.resident.condo.module.marketBasket') {
      return true
    } else if (FlowRouter.getParam('module_slug') === moduleName) {
      return true
    }
    return false
  }
});
