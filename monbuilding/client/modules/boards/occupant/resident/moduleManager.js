import { BlazeLayout } from 'meteor/kadira:blaze-layout'
import { ReactiveVar } from 'meteor/reactive-var'

export class ModuleManager {
  constructor(board) {
    this.currentBuilding = new ReactiveVar(null)
    this.currentCondo = new ReactiveVar(null)
    this.currentModule = new ReactiveVar(null)

    this.condoModules = new ReactiveVar([])
    this.buildingModules = new ReactiveVar([])

    this.last_condo_id = null
    this.last_module_slug = null

    this.board = board
  }

  changeCondo (condo_id) {
    if (condo_id === this.last_condo_id) {
      return true
    }

    const condo = Condos.findOne(condo_id)

    this.loadCondoModules(condo)

    this.currentCondo.set(condo)
    this.last_condo_id = condo_id
  }

  changeBuilding (building_id) {
    if (building_id === this.last_building_id)
      return true

    const building = Buildings.findOne(building_id)

    this.loadBuildingModules(building, this.last_condo_id)

    this.currentBuilding.set(building)
    this.last_building_id = building_id
  }

  changeModule (module_slug, type) {
    if (module_slug === null) {
      this.currentModule.set(null)
      return
    }
    if (module_slug === 'profile') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'profile',
        slug: 'profile',
        template: 'occupantProfile',
        title: 'Profile',
        type: 'condo',
      })
      return
    } else if (module_slug === 'map') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'map',
        slug: 'map',
        template: 'module_map',
        title: 'Map',
        type: 'condo',
      })
      return
    } else if (module_slug === 'manual') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'manual',
        slug: 'manual',
        template: 'module_manual',
        title: 'Manual',
        type: 'condo',
      })
      return
    }
    // @help @kevin not sure what this statement block, can you help me fix this out ?
    else if (module_slug === 'manual_plan') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'manual',
        slug: 'manual',
        template: 'module_manual_plan',
        title: 'Manual',
        type: 'condo',
      })
      return
    } else if (module_slug === 'occupantList') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'occupantList',
        slug: 'occupantList',
        template: 'occupantListForOccupant',
        title: 'occupantList',
        type: 'condo',
      })
      return
    } else if (module_slug === 'marketPlace') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'marketPlace',
        slug: 'marketPlace',
        template: 'occupant_marketPlace',
        title: 'marketPlace',
        type: 'condo',
      })
      return
    } else if (module_slug === 'laundryRoom') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'laundryRoom',
        slug: 'laundryRoom',
        template: 'laundryRoom',
        title: 'laundryRoom',
        type: 'condo',
      })
      return
    } else if (module_slug === 'detailUser') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'detailUser',
        slug: 'detailUser',
        template: 'detailUser',
        title: 'detailUser',
        type: 'condo',
      })
      return
    } else if (module_slug === 'createOccupant') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'createOccupant',
        slug: 'createOccupant',
        template: 'personalDetailsOccupantFromOccupant',
        title: 'createOccupant',
        type: 'condo',
      })
      return
    } else if (module_slug === 'messenger') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'messenger',
        slug: 'messenger',
        template: 'messenger',
        title: 'messenger',
        type: 'condo',
      })
      return
    } else if (module_slug === 'concierge') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'concierge',
        slug: 'concierge',
        template: 'module_concierge',
        title: 'Concierge',
        type: 'condo',
      })
      return
    } else if (module_slug === 'forum') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident', 'gestionnaire'],
        defaultDiv: false,
        name: 'forum',
        slug: 'forum',
        template: 'module_forum',
        title: 'Forum',
        type: 'condo',
      })
      return
    } else if (module_slug === 'wallet') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'wallet',
        slug: 'wallet',
        template: 'module_walletController',
        title: 'Wallet',
        type: 'condo',
      })
      return
    } else if (module_slug === 'emergency') {
      this.currentModule.set({
        board: 'resident',
        boards: ['resident'],
        defaultDiv: false,
        name: 'emergency',
        slug: 'emergency',
        template: 'module_emergency',
        title: 'Emergency Contact',
        type: 'condo',
      })
      return
    }

    let modules = null
    // Check if the module is for condo or building
    if (type === 'condo')
      modules = this.condoModules.get()
    else
      modules = this.buildingModules.get()

    // Search a module that match the name in the url
    const currentModule = modules.find((module) => {
      return module.slug === module_slug
    })

    //TODO: IF MODULE NOT FOUND, 404 ?
    this.currentModule.set(currentModule)
  }

  loadModules (building, condo) {
    const buildingModules = building.modules.map((module) => {
      return {
        ...module,
        building,
        condo,
        type: 'building',
        template: `module_${module.name}_index`,
        board: this.board
      }
    })

    const condoModules = condo.modules.map((module) => {
      return {
        ...module,
        building,
        condo,
        type: 'condo',
        template: `module_${module.name}_index`,
        board: this.board
      }
    })

    let self = this
    this.buildingModules.set(_.filter(buildingModules, function (m) { return m.boards.indexOf(self.board) != -1 }))
    this.condoModules.set(_.filter(condoModules, function (m) { return m.boards.indexOf(self.board) != -1 }))
  }

  loadCondoModules (condo) {
    if (!condo)
      return
    const condoModules = condo.modules.map((module) => {
      return {
        ...module,
        condo,
        type: 'condo',
        template: `module_${module.name}_index`,
        board: this.board
      }
    })
    let self = this
    this.condoModules.set(_.filter(condoModules, function (m) { return m.boards.indexOf(self.board) != -1 }))
  }

  loadBuildingModules (building, condo) {
    const buildingModules = building.modules.map((module) => {
      return {
        ...module,
        building,
        condo,
        type: 'building',
        template: `module_${module.name}_index`,
        board: this.board
      }
    })
    let self = this
    this.buildingModules.set(_.filter(buildingModules, function (m) { return m.boards.indexOf(self.board) != -1 }))
  }
}
