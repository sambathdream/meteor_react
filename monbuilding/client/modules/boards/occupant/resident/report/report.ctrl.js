import { Template } from 'meteor/templating';
import { FileManager } from "/client/components/fileManager/filemanager.js";
// import { Home } from "/common/lang/lang.js";

import './report.view.html';

function emptyModal(data) {
	_.each(data, function(type, index) {
		switch (index) {
			case "input":
				_.each(type, function(elem) {
					$(elem).val("");
				})
				break;
			case "select":
				_.each(type, function(elem) {
					$(elem).prop('selectedIndex', 0);
				})
				break;
			case "select2":
				_.each(type, function(elem) {
					$(elem).val(null).trigger("change");
				})
				break;
			default:
				_.each(type, function(elem) {
					$(elem).val("");
				})
				break;
		}
	});
}

Template.reportModal.onCreated(function() {
	this.fm = new FileManager(UserFiles);
});

Template.reportModal.onRendered(function() {
    setTimeout(function() {
        $(".select-contacts").select2();
        $(".select-contacts").select2("focus");
    }, 50);
});


Template.reportModal.events({
    'hidden.bs.modal #modal_report' (e, t) {
        emptyFormByParentId('modal_report');
    },
});

Template.reportModal.helpers({
    isDirectMessage : function(userId) {
        return FlowRouter.getParam("wildcard") === userId;
    },
	getResidents: function() {
		let condoId = FlowRouter.getParam("condo_id");
		let residents = ResidentUsers.find({_id: {$not: Meteor.userId()}}).fetch();
		residents = _.filter(residents, (r) => {
			let condo = _.find(r.resident.condos, (c) => {return c.condoId === condoId});
			return (condo && condo.preferences && condo.preferences.messagerie === true)
		});
		return residents;
	},
	getFileManagerReport: function() {
		return Template.instance().fm;
	},
	newReportCb: function() {
		return (message, files) => {
			const data = {
					condoId   : FlowRouter.getParam("condo_id"),
					from      : Meteor.userId(),
					category  : $("#reportCategory").val(),
					author    : $("select[name=reportDestinataires]").val(),
					message   : message,
					files	  : files
			};
			Meteor.call("newReportFromResident", data, function(error) {
				$('#sendAnswer').button('reset');
				$('#sendAnswer').prop('disabled', true)
				if (!error) {
          $('#modal_report').modal('toggle');
          $(".helpContent.helpButton").css({display: "none"});
        }
				else {
					sAlert.error(error);
				}
			});
		};
	},
	checkModule: function(moduleName) {
    let condo = Condos.findOne(FlowRouter.getParam('condo_id'));
		if (condo && condo.settings && condo.settings.options)
			return condo.settings.options[moduleName];
		else
			return false;
	}
});
