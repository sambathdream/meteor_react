import { FlowRouter } from 'meteor/kadira:flow-router';
import { BlazeLayout } from 'meteor/kadira:blaze-layout';

import PageForumContainer from '/client/modules/boards/common/forum/component/forum';
import DetailsForumPost from '/client/modules/boards/common/forum/component/detail_forum_post';

import ForumLang from '/common/lang/lang'


let residentRoutes = FlowRouter.group({
	prefix: '/:lang/resident',
	name: 'app.board.resident',
	triggersEnter: [
	function (context, redirect) {
    SEO.set(domainConfig.SEOconfig)
		Session.set("target_url", context.path);
		if (!Meteor.user() && !Meteor.loggingIn()) {
			FlowRouter.go("app.login.login", {lang: 'fr'});
		}
		else {
			Meteor.call('user_isResident', function (err) {
				if (err) {
					Session.set("target_url", null);
          const lang = FlowRouter.getParam('lang') || 'fr'
					if (err.error === 300)
						FlowRouter.go("app.errorNotAuthenticated"), { lang };
					else {
            FlowRouter.go("app.errorAccessDenied", { lang });
          }
				}
			});
		}
	}
	]
});

residentRoutes.route('/', {
	name: 'app.board.resident.home',
	action(params) {
    BlazeLayout.render('main_body', {template: 'loader'});
    // console.log('YO')
		let timer = Meteor.setInterval(function (){
      let resident = Residents.findOne({"userId": Meteor.userId()});
      // console.log('resident', resident)
			if (resident) {
				Meteor.clearInterval(timer);
        const lang = (FlowRouter.getParam("lang") || "fr");
        // console.log("error account not verified")
				if (params.condo_id) {
					Meteor.call('updateLastKnownCondo', params.condo_id, function(error, result) {
						if (!error) {
							FlowRouter.go('app.board.resident.condo', {lang, "condo_id": params.condo_id});
						}
					});
				}
				else if (resident.condos[0]) {
					Meteor.call('updateLastKnownCondo', resident.condos[0].condoId, function(error, result) {
						if (!error) {
							FlowRouter.go('app.board.resident.condo', {lang, "condo_id": resident.condos[0].condoId});
						}
					})
				}
				else if (resident.pendings[0]) {
					Meteor.call("userSetPending", function(error, result) {
						if (!error && result == true) {
							resident = Residents.findOne({"userId": Meteor.userId()});
							FlowRouter.go('app.board.resident.condo', {lang, "condo_id": resident.condos[0].condoId});
							// location.reload();
						}
						else {
              console.log("error account not verified route 1")
              console.log("lang : ", lang)
							Session.set("target_url", null);
							FlowRouter.go('app.errorAccountNotVerified', {lang});
						}
					})
				}
				else {
          console.log("error account not verified route 2")
					Session.set("target_url", null);
					FlowRouter.go('app.errorAccountNotVerified', {lang});
				}
			}
		}, 100);
	}
});

residentRoutes.route('/:condo_id', {
	name: 'app.board.resident.condo',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'board'});
	},
});

residentRoutes.route('/:condo_id/unfollow/:post_id', {
	name: 'app.board.resident.condo.module.forum.unfollow',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'forum_unfollow'});
	}
});

residentRoutes.route('/:condo_id/marketPlace/serviceDetails/:serviceId/:reservationId?', {
	name: 'app.board.resident.condo.module.marketPlace.serviceDetails.serviceId',
	action(params, query) {
		const lang = (FlowRouter.getParam('lang') || 'fr')
    BlazeLayout.render('main_body', { lang, template: 'board', sub_template: 'occupant_marketPlace_servicesDetails' })
	}
});

residentRoutes.route('/:condo_id/marketBasket', {
	name: 'app.board.resident.condo.module.marketBasket',
	action(params, query) {
		const lang = (FlowRouter.getParam('lang') || 'fr')
    BlazeLayout.render('main_body', { lang, template: 'board', sub_template: 'occupant_marketBasket' })
	}
});

residentRoutes.route('/:condo_id/print/:tab', {
	name: 'app.board.resident.condo.module.print',
	action(params, query) {
		const lang = (FlowRouter.getParam('lang') || 'fr')
    BlazeLayout.render('main_body', { lang, template: 'board', sub_template: 'occupant_print' })
	}
});

residentRoutes.route('/:condo_id/:module_slug/:wildcard(.*)?', {
	name: 'app.board.resident.condo.module',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'board'});
	},
});

residentRoutes.route('/:condo_id/forum', {
	name: 'app.board.resident.condo.module.forum',
	reactComponent: function() { return PageForumContainer; },
	action: function(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {ForumLang, lang, template: 'board' });
	}
});

residentRoutes.route('/:condo_id/forum/:postId', {
	name: 'app.board.resident.condo.module.forum.post',
	reactComponent: function() { return DetailsForumPost; },
	action: function(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {ForumLang, lang, template: 'board' });
	}
});

residentRoutes.route('/:condo_id/incident/:incidentId', {
	name: 'app.board.resident.condo.module.incident',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'board'});
	},
});

/*residentRoutes.route('/:condo_id/:building_id/:module_slug/:wildcard(.*)?', {
  name: 'app.board.resident.building.module',
  action(params, query) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    BlazeLayout.render('main_body', {lang, template: 'board'});
  },
});*/

residentRoutes.route('/:condo_id/messenger', {
	name: 'app.board.resident.condo.module.messenger',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', { lang, template: 'messenger' });
	}
});

residentRoutes.route('/:condo_id/profile/:tab?', {
	name: 'app.board.resident.condo.module.profile.tab',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', { lang, template: 'occupantProfile', tab: 'documents' });
	}
});

residentRoutes.route('/:condo_id/occupantList/:tab', {
  name: 'app.board.resident.condo.module.occupantList',
  action(params, query) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    BlazeLayout.render('main_body', { lang, template: 'occupantListForOccupant', tab: 'documents' });
  }
});

residentRoutes.route('/:condo_id/emergency', {
  name: 'app.board.resident.condo.module.emergency',
  action(params, query) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    BlazeLayout.render('main_body', {lang, template: 'module_emergency'});
  }
});

residentRoutes.route('/:condo_id/detailUser/:userId', {
	name: 'app.board.resident.condo.module.detailUser',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
    BlazeLayout.render('main_body', { lang, template: 'detailUser' });
	}
});

residentRoutes.route('/:condo_id/annonces?ad=:ad_id&back=:isBack', {
	name: 'app.board.resident.condo.module.classifieds.ad',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'module_classifieds_view_ad'});
	}
});

residentRoutes.route('/:condo_id/map', {
	name: 'app.board.resident.condo.module.map',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'module_map'});
	}
});

residentRoutes.route('/:condo_id/concierge', {
	name: 'app.board.resident.condo.module.concierge',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'module_concierge'});
	}
});

residentRoutes.route('/:condo_id/manual_plan', {
  name: 'app.board.resident.condo.module.manual_plan',
  action(params, query) {
    const lang = (FlowRouter.getParam("lang") || "fr");
    BlazeLayout.render('main_body', {lang, template: 'module_manual_plan'});
  }
});

residentRoutes.route('/:condo_id/manual', {
	name: 'app.board.resident.condo.module.manual',
	action(params, query) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		BlazeLayout.render('main_body', {lang, template: 'module_manual'});
	}
});
