/**
* Created by kuyarawa on 05/02/18.
*/
import { ModuleReservationIndex } from "/common/lang/lang.js";

export const ORDER = {
  'meeting' : 1,
  'coworking' : 2,
  'equipment' :3,
  'edl' :4,
  'restaurant' :5,
  'other' :6
};

export const RESA_PER_LOAD = 500;

Template.module_reservation_index.onCreated(function() {
  this.isManager = Meteor.user() && Meteor.user().identities && Meteor.user().identities.gestionnaireId;
  this.condoSelected = new ReactiveVar(FlowRouter.getParam('condo_id'))
  Session.set('showReservationForm', false);
  Session.set('showReservationCalendar', false);
  Session.set('showReservationNew', false);
  Session.set('reservationValidateId', false);
  Session.set('res.reservationSuccessId', null);
  Session.set('reservationForm', {});
  this.activeTab = new ReactiveVar(!!FlowRouter.current().queryParams.pending ? 'pending':'confirmed');
  this.calendarState = new ReactiveVar(true);
  this.resources = new ReactiveVar([]);
  this.eventDates = new ReactiveVar([]);
  this.form = new ReactiveDict();
  this.limit = new ReactiveVar(RESA_PER_LOAD);
  this.filterYear = new ReactiveVar(new Date().getFullYear());
  this.filterMonth = new ReactiveVar(new Date().getMonth());
  this.filterDay = new ReactiveVar(new Date().getDate());
  this.selectedResaId = new ReactiveVar(null);
  this.canSeeResources = new ReactiveVar(false);

  const translation = new ModuleReservationIndex(FlowRouter.getParam("lang") || "fr");
  let eventId = FlowRouter.current().queryParams.removee;
  let resourceId = FlowRouter.current().queryParams.remover;
  if (eventId !== undefined && resourceId !== undefined) {
    eventId = new Meteor.Collection.ObjectID(eventId);

    bootbox.confirm({
      title: "Confirmation",
      message: translation.moduleReservationIndex['cancel_reservation'] + " ?",
      callback: function(response) {
        if (response) {
          Meteor.call('removeEvent', FlowRouter.getParam('condo_id'), eventId, (err, res) => {
            if (res > 0)
              sAlert.success(translation.moduleReservationIndex["canceled_reservation"]);
            else
              sAlert.error(translation.moduleReservationIndex["problem_reservation"]);
          });
        }
      }
    });
  }

  let edlId = FlowRouter.current().queryParams.edl;
  let answer = FlowRouter.current().queryParams.answer;
  setTimeout(function(){
    if (edlId !== undefined && answer !== undefined) {
      let edl = Reservations.findOne({edlId});
      if (!edl)
        return sAlert.error("Etat des lieux inconnu.");
      let user = UsersProfile.findOne(edl.origin);
      if (!user)
        return sAlert.error("Utilisateur inconnu.");
      if (answer === "1") {
        bootbox.confirm({
          title: "Confirmation",
          message: "Etes-vous sûr de vouloir confirmer la sortie de " +
          user.firstname + " " + user.lastname + " ?",
          callback: function(res) {
            if (res) {
              Meteor.call("removeResidentOfCondo", user._id, edl.condoId, function(error, result) {
                if (error)
                  sAlert.error(error);
              });
            }
          }
        });
      }
      else if (answer === "0") {
        bootbox.alert({
          title: "Attention",
          message: "La sortie de " + user.firstname + " " + user.lastname + " n’étant pas confirmée, les accès lui sont maintenus."
        });
      }
    }
  }, 1000);

  this.autorun(() => {
    const condoId = !!this.condoSelected.get() && this.condoSelected.get() !== 'all' ? this.condoSelected.get() : null
    this.subscribe("resources", condoId);
    this.subscribe("get-terms-of-services", condoId);
    this.subscribe("reservations", condoId, this.limit.get(), this.activeTab.get());
    this.subscribe("specificReservations", condoId, Session.get('reservationSuccessId'));
    if (!!Session.get('reservationSuccessId')) {
      const newResa = Reservations.findOne(Session.get('reservationSuccessId'));
      if (!!newResa) {
        this.activeTab.set(!!newResa.pending ? 'pending':'confirmed');
      }
      this.selectedResaId.set(Session.get('reservationSuccessId'));
    }
    this.canSeeResources.set(this.condoSelected.get() === 'all' ? Meteor.listCondoUserHasRight("reservation", "seeResources").length > 0 : Meteor.userHasRight('reservation', 'seeResources', this.condoSelected.get()))
  });
  Meteor.call('setNewAnalytics', {type: "module", module: "resa", accessType: "web", condoId: this.condoSelected.get()})
});

Template.module_reservation_index.onDestroyed(function() {
  // Meteor.call('updateAnalytics', {type: "module", module: "resa", accessType: "web", condoId: Template.instance().condoSelected.get()});
});

Template.module_reservation_index.events({
  'click #reservation-page .list-tab' (e, t) {
    const element = $(e.currentTarget);
    if (!element.hasClass('selectedTabBar')) {
      $('#reservation-page').find('.selectedTabBar').removeClass('selectedTabBar');
      element.addClass('selectedTabBar');
      t.activeTab.set(element.data('type'));
      t.limit.set(RESA_PER_LOAD);
    }
  },
  'click #reserver' (e, t) {
    Session.set('showReservationCalendar', true);
  },
  'click #resources-btn' (e, t) {
    FlowRouter.go("app.gestionnaire.ressources", { lang: FlowRouter.getParam("lang") || "fr", condo_id: t.condoSelected.get() });
  },
  'click #reservation-form-btn' (e, t) {
    Session.set('reservationForm', {});
    t.form.set('resourceType', null);
    t.form.set('method', null);
    setTimeout(() => {
      Session.set('showReservationForm', true);
      Session.set('showReservationCalendar', false);
    }, 50)
  },
  'click #close-form' (e, t) {
    if (Session.equals('showReservationCalendarForm', true)) {
      Session.set('showReservationCalendarForm', false);
    } else {
      Session.set('showReservationForm', false);
    }
  },
});

const initDatePicker = (template, lang, eventDates) => {
  const self = template;
  const selector = $("#mini-calendar");
  selector.datepicker("destroy");
  selector.datepicker({
    maxViewMode: 0,
    language: lang,
    weekStart: 1,
    autoclose: true,
    todayHighlight: true,
    defaultDate: '0d',
    beforeShowDay: function(date) {
      const formattedDate = moment.utc(date).add(12, 'h').startOf('day').format('x');
      if (_.isArray(eventDates) && eventDates.includes(formattedDate)) {
        return {
          classes: 'booked-date'
        };
      }
    },
    templates: {
      leftArrow: '<i class="fa fa-chevron-left"></i>',
      rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
  })
  .on('changeMonth', function(event) {
    self.filterYear.set(new Date(event.date).getFullYear());
    self.filterMonth.set(new Date(event.date).getMonth());
    self.filterDay.set(new Date(event.date).getDate());
  })
  .on("changeDate", function(event) {
    self.filterDay.set(new Date(event.date).getDate());
    self.filterMonth.set(new Date(event.date).getMonth());
  });
};

Template.module_reservation_index.rendered = function () {
  setTimeout(() => {
    $("#mini-calendar").datepicker("setDate", new Date());
  }, 500)
};

Template.module_reservation_index.helpers({
  refreshCondoId: () => {
    const condoId = Template.currentData().selectedCondoId
    const t = Template.instance()
    if (Template.instance().isManager && condoId && condoId !== t.condoSelected.get()) {
      const lang = FlowRouter.getParam('lang') || 'fr';
      FlowRouter.go('app.gestionnaire.planningid', { lang, condo_id: condoId });
      t.condoSelected.set(condoId)
    }
    return true
  },
  showForm: () => {
    return Session.get('showReservationForm');
  },
  showCalendar: () => {
    return Session.get('showReservationCalendar');
  },
  showNew: () => {
    return Session.get('showReservationNew');
  },
  getActiveTab: () => {
    return Template.instance().activeTab.get();
  },
  isManager: () => {
    return Template.instance().isManager;
  },
  isNotManager: () => {
    return !Template.instance().isManager;
  },
  resourcesReady: () => {
    return Template.instance().subscriptionsReady()
  },
  getActiveTabClass: (tab) => {
    return Template.instance().activeTab.get() === tab ? 'selectedTabBar' : ''
  },
  getSelectedCondo: () => {
    return Template.instance().condoSelected.get();
  },
  calendarReady: () => {
    return Template.instance().calendarState.get()
  },
  initDatePicker: () => {
    const instance = Template.instance();
    const lang = (FlowRouter.getParam("lang") || "fr");
    const eventDates = Template.instance().eventDates.get()
    setTimeout(() => {
      initDatePicker(instance, lang, eventDates);
    }, 10);
  },
  needValidation: () => {
    let needValidation = false;
    const resources = Resources.find(!!Template.instance().condoSelected.get() && Template.instance().condoSelected.get() !== 'all' ? {condoId: Template.instance().condoSelected.get()}: {}).fetch();
    _.each(resources, (r) => {
      if (r.needValidation && r.needValidation.status) {
        needValidation = true
      }
    })

    return needValidation;
  },
  haveResources: () => {
    return Template.instance().resources.get().length > 0
  },
  getCalendarEvent: () => {
    const instance = Template.instance();
    return (dates) => {
      instance.eventDates.set(dates)
    }
  },
  getEventDates: () => {
    return Template.instance().eventDates.get()
  },
  setResources: () => {
    const condoId = Template.instance().condoSelected.get();
    const resources = attachResourceType(Resources.find(!!condoId && condoId !== 'all' ? {condoId: condoId} : {}).fetch());
    let expandedResources = [];
    _.each(resources, (r) => {
      if (!isInt(r.number)) {
        expandedResources.push({
          ...r,
          realId: r._id
        })
      } else {
        for (let i = 1; i <= r.number; i++) {
          expandedResources.push({
            ...r,
            number: i,
            _id: r._id + i,
            realId: r._id
          })
        }
      }
    })
    Template.instance().resources.set(expandedResources);
  },
  goToNewForm: () => {
    const instance = Template.instance();
    return (resource, time, allDay) => {
      let res = resource
      let week = false
      let resources = []

      if (resource === null) {
        week = true;
        resources = instance.resources.get();
      } else {
        if (!resource.realId) {
          res = _.find(instance.resources.get(), (r) => {
            return r._id === resource.id
          })
        }
      }

      instance.form.set('resources', resources);
      instance.form.set('week', week);
      instance.form.set('method', 'calendar');
      instance.form.set('selectedResource', res);
      instance.form.set('resourceType', res ? res.type : null);
      instance.form.set('resourceId', res ? res.realId : null );
      instance.form.set('startHour', time.start);
      instance.form.set('endHour', time.end);
      instance.form.set('isAllDay', allDay);
      instance.form.set('number', res && isInt(res.number) ? res.number : null);
      setTimeout(() => {
        Session.set('showReservationCalendar', false);
        Session.set('reservationFromIndexCalendar', true);
        Session.set('showReservationForm', true);
        setTimeout(() => {
          Session.set('showReservationNew', true);
        }, 200);
      }, 200);
      $('body, html').animate({scrollTop: 0});
    }
  },
  getFormData: () => {
    const instance = Template.instance();

    return {
      selectedResource: instance.form.get('selectedResource'),
      resourceType: instance.form.get('resourceType'),
      method: instance.form.get('method'),
      resourceId: instance.form.get('resourceId'),
      startHour: instance.form.get('startHour'),
      endHour: instance.form.get('endHour'),
      isAllDay: instance.form.get('isAllDay'),
      week: instance.form.get('week'),
      resources: instance.form.get('resources'),
      number: instance.form.get('number')
    }
  },
  goToValidate: () => {
    return (id) => {
      Session.set('reservationValidateId', id);
    }
  },
  showValidate: () => {
    return !!Session.get('reservationValidateId');
  },
  getLimit: () => {
    return Template.instance().limit.get()
  },
  loadMore: () => {
    const instance = Template.instance();
    return () => {
      instance.limit.set(instance.limit.get() + RESA_PER_LOAD)
    }
  },
  getBadge: (module) => {
    let condoId = FlowRouter.getParam('condo_id')
    let badges
    if (condoId && condoId !== 'all') {
      badges = BadgesGestionnaire.find({ _id: condoId }).fetch();
    } else {
      badges = BadgesGestionnaire.find().fetch();
    }
    if (!badges || badges.length === 0) {
      badges = BadgesResident.find({_id: condoId}).fetch();
    } if (!badges) {
      return "";
    }
    let nbBadges = 0;
    _.each(badges, function (badge) {
      nbBadges += badge[module] || 0;
    });
    return (nbBadges > 0) ? nbBadges : "";
  },
  getSpecificDate: () => {
    const instance = Template.instance();
    const day = instance.filterDay.get();
    const month = instance.filterMonth.get();
    const year = instance.filterYear.get();

    const start = moment().set({year: year, month: month, date: day}).startOf('day');
    const end = moment().set({year: year, month: month, date: day}).endOf('day');

    return {start, end}
  },
  getSelectedResa: () => {
    return Session.get('reservationSuccessId') || Template.instance().selectedResaId.get()
  },
  onSelectResa: () => {
    const instance = Template.instance();
    return (id) => {
      instance.selectedResaId.set(id)
    }
  },
  canSeeResources: () => {
    return Template.instance().canSeeResources.get()
  },
  canSeeReservationHelper: () => {
    const condoId = Template.instance().condoSelected.get()
    let right = false
    if (condoId === 'all') {
      Condos.find({}, {fields: { settings: true } }).forEach(condo => {
        if (condo && condo.settings && condo.settings.options.reservations === true && Meteor.userHasRight('reservation', 'see', condo._id)) {
          right = true
        }
      });
    } else {
      let condo = Condos.findOne({ _id: condoId })
      right = condo && condo.settings && condo.settings.options.reservations && Meteor.userHasRight('reservation', 'see', condoId)
    }
    return right
  }
});
