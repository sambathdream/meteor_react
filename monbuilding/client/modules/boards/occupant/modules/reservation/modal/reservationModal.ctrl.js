/**
 * Created by kuyarawa on 30/04/18.
 */
import { ModuleReservationIndex, CommonTranslation, Email } from '/common/lang/lang.js';
// import moment from 'moment'

Template.ReservationModal.onCreated(function () {});

Template.ReservationModal.events({
  'hide.bs.modal #reservation-detail': (e, t) => {
    Session.set('reservationSuccessId', false);
  },
  'show.bs.modal #reservation-detail': (e, t) => {
    setTimeout(() => {
      if (t.data.selectedResaId) {
        Meteor.call('setValidateReservationHasView', t.data.selectedResaId);
      }
    }, 500)
  },
  'click #book-edit-btn': (e, t) => {
    $("#reservation-detail").modal("hide");
    setTimeout(() => {
      Session.set('reservationEditId', t.data.selectedResaId);
      Session.set('showReservationForm', true);
      Session.set('showReservationNew', true);
    }, 200);
  },
  'click #book-delete-btn': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    const bookId = t.data.selectedResaId;

    bootbox.confirm({
      title: translate.moduleReservationIndex["confirmation"],
      message: translate.moduleReservationIndex["delete_reservation"],
      buttons: {
        confirm: {label: translate.moduleReservationIndex["yes"], className: "btn-red-confirm"},
        cancel: {label: translate.moduleReservationIndex["no"], className: "btn-outline-red-confirm"}
      },
      callback: function(res) {
        if (res) {
          const book = Reservations.findOne(bookId);
          if (book) {
            Meteor.call('removeEvent', book.condoId, book.eventId, (err, res) => {
              if (err) {
                sAlert.error(err.reason);
              }
              else if (res) {
                sAlert.success(translate.moduleReservationIndex["delete_success"]);
                $("#reservation-detail").modal("hide");
              }
            });
          }
        }
      }
    });
  },
  'click .add-to-calendar': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translationEmail = new Email(lang);

    const element = $(e.currentTarget);
    const type = element.data('type');
    const data = Reservations.findOne(t.data.selectedResaId);
    if (data) {
      const condo = Condos.findOne(data.condoId);
      const resource = Resources.findOne(data.resourceId);

      let start = moment(data.start);
      let end = moment(data.end);

      // create a new moment based on the original one
      let startFix = start.clone();
      let endFix = end.clone();

      // change the time zone of the new moment
      startFix.tz('Europe/Paris'); // or whatever time zone you desire
      endFix.tz('Europe/Paris'); // or whatever time zone you desire

      // shift the moment by the difference in offsets
      startFix.add(start.utcOffset() - startFix.utcOffset(), 'minutes');
      endFix.add(end.utcOffset() - endFix.utcOffset(), 'minutes');

      const resourceType = getResourceTypeByKey(resource.type);
      const eventName = translationEmail.email['reservation'] + (resource.type !== 'edl' ? resourceType  + ' - ' + resource.name + (isInt(data.number) ? ' (' + data.number + ')' : '') : resourceType);
      const common = {
        location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code,
        start: startFix.toDate(),
        end: endFix.toDate()
      };

      if (type === 'gcal') {
        const generateUrl = require('generate-google-calendar-url');
        const googleLink = generateUrl({
          title: eventName,
          details: '',
          ...common
        });
        window.open(googleLink, '_blank');
      } else {
        const cal = ics();
        cal.addEvent(eventName, '', common.location, common.start, common.end);
        cal.download();
      }
    }
  }
});

Template.ReservationModal.helpers({
  getReceipeDate: (book) => {
    return moment(!!book.createdAt? book.createdAt: new Date()).format('DD/MM/YYYY')
  },
  getDetail: () => {
    let resa = {};
    let data = {};
    const instance = Template.instance()
    if (!!instance.data.selectedResaId) {
      data = Reservations.findOne(Template.instance().data.selectedResaId);
    } else if (!!instance.data.bookData) {
      data = instance.data.bookData
    }
    if (data) {
      const condo = Condos.findOne(data.condoId);
      const resource = Resources.findOne(data.resourceId);

      resa = {
        book: data,
        condo,
        resource
      }
    }

    return resa;
  },
  editable: (book) => {
    return (!!book && (+moment().format('x') < book.start) && (!!book.pending || !book.rejected))
  },
  isMine: (book) => {
    return book && book.origin === Meteor.user()._id
  },
  notEdl: (type) => {
    return type !== 'edl';
  },
  setReservationHasView: (book) => {
    Meteor.call('setValidateReservationHasView', book._id);
  },
  timeString: (reservation) => {
    let timeString = '';

    if (reservation) {
      const start = moment(reservation.start)
      const end = moment(reservation.end)
      if (start.format('YYMMDD') === end.format('YYMMDD')) {
        timeString = `${start.format('dddd D MMM, YYYY, HH:mm')}-${end.format('HH:mm')}`
      } else {
        timeString = `${start.format('D MMM YYYY, HH:mm')}-${end.format('D MMM YYYY, HH:mm')}`
      }

      let recuring = '';
      if (reservation.isRecurring) {
        recuring = ` (${reservation.recurringType})`
      }

      return timeString + recuring
    }
  },
  getResourceName: (resource) => {
    return resource && resource.type === 'edl' && resource.name === 'edl' ? getResourceTypeByKey('edl') : resource && resource.name
  },
  isNeedValidationExcludeMe: (resource, book) => {
    return resource && resource.needValidation && resource.needValidation.status && !!book.pending && !resource.needValidation.managerIds.includes(Meteor.user()._id);
  },
  resNum: (res, book) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    return book.number && res.number > 1 ? (
        res.type === 'coworking' ? translate.moduleReservationIndex['place'] : translate.moduleReservationIndex['number']) + ': ' + book.number
      : false;
  },
  getBooker: (event) => {
    if (event) {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ModuleReservationIndex(lang);
      const user = UsersProfile.findOne(event.origin);
      return event.origin === Meteor.user()._id ? capitalize(translate.moduleReservationIndex['you']) : (user ? `${user.firstname} ${user.lastname}` : '');
    }
  },
  notEdlAndEquipment: (type, book) => {
    return type !== 'edl' && type !== 'equipment' && book && book.participantNumber > 0;
  },
  haveParticipants: (book) => {
    return book && ((!!book.participants && book.participants.length > 0) || (!!book.externalParticipants && book.externalParticipants.length > 0))
  },
  getParticipantNumber: (book) => {
    return book && book.participantNumber ? book.participantNumber : '-';
  },
  showSuccessModal: () => {
    if (!!Session.get('reservationSuccessId')) {
      setTimeout(() => {
        $("#reservation-detail").modal("show");
      }, 400);
    }
  },
  getParticipants: (participants) => {
    const users = UsersProfile.find({_id: {$in: participants}}).fetch();
    return !!users ? users : [];
  },
  isServiceNotEmpty: (book) => {
    return book && !_.isEmpty(book.services)
  },
  getFloor: (floor) => {
    return translateEtage(floor);
  },
  getServiceName: (service) => {
    const serviceData = ResourceServices.findOne({key: service});
    return serviceData ? getTranslatedValue(serviceData) : '';
  },
  getServiceChild: (service) => {
    const serviceData = ResourceServices.findOne({key: service.key});

    const data = serviceData.children.filter((c) => {
      return service.data.includes(c.key)
    }).map((d) => {
      return getTranslatedValue(d)
    });
    const custom = service.custom.map((c) => {
      return c
    });
    return _.sortBy([...data, ...custom])
  },
  rejected: (event) => {
    return event && !!event.rejected;
  },
  doneButton: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    return {
      label: translate.moduleReservationIndex["done"],
      class: 'red',
      onClick: () => {
        $("#reservation-detail").modal("hide");
      }
    }
  },
  getServices: (services) => {
    return _.map(services, (s, i) => {
      return {
        ...s,
        key: i
      }
    })
  },
  getEdlDirections: (book) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    return !!book.edlDirection ? translate.moduleReservationIndex[book.edlDirection] : translate.moduleReservationIndex["out"]
  },
  isPaymentConfirmation: () => {
    return !!Template.instance().data.paymentConfirmationCb
  },
  paymentConfirmButton: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new CommonTranslation(lang);
    const instance = Template.instance()
    return {
      label: translate.commonTranslation["confirm_paid"],
      class: 'red',
      onClick: () => {
        if (typeof instance.data.paymentConfirmationCb === 'function') {
          instance.data.paymentConfirmationCb();
        }
      }
    }
  }
});
