import { ModuleReservationIndex, ModuleReservationError } from "/common/lang/lang.js";
import { ORDER } from '../reservation.ctrl';

let $checkableTree, $categories, $sub, generateNavigatorLoaded, $event = [];
let $nodes = [];

let treeviewOptions = {
  showIcon: false,
  showCheckbox: true,
  expandIcon: "glyphicon glyphicon-chevron-right",
  collapseIcon: "glyphicon glyphicon-chevron-down",
  onNodeChecked: function(event, node) {
    $checkableTree.treeview('expandNode', node.nodeId);
    toggleChild(node, 'checkNode');
    $('#calendar').fullCalendar('rerenderEvents');
    // toggleRows($("#calendar"), function (row, resourceId) {
    //     // do some checking magic here
    //     return (resourceId == node["data-id"]);
    // }, true);
  },
  onNodeUnchecked: function (event, node) {
    $checkableTree.treeview('collapseNode', node.nodeId);
    toggleChild(node, 'uncheckNode');
    $('#calendar').fullCalendar('rerenderEvents');
    // toggleRows($("#calendar"), function (row, resourceId) {
    //     // do some checking magic here
    //     return (resourceId == node["data-id"]);
    // }, false);
  }
};

let translation;

Template.ReservationCalendar.onCreated(function() {
  this.lang = new ReactiveVar(FlowRouter.getParam("lang") || "fr");
  translation = new ModuleReservationIndex(this.lang);

  this.condoId = new ReactiveVar(FlowRouter.current().params.condo_id);
  this.selectedDate = new ReactiveVar(!!Session.get('resaSelectedDate') ? Session.get('resaSelectedDate'): moment().toDate());
  this.selectedMonth = new ReactiveVar(!!Session.get('resaSelectedDate') ? moment(Session.get('resaSelectedDate')).startOf('month').toDate(): moment().startOf('month').toDate());
  this.selectedType = new ReactiveVar(!!this.data.resourceType ? getResourceTypeByKey(this.data.resourceType, this.lang): '-');
  this.categories = new ReactiveVar();
  this.reservationSub = new ReactiveVar(false);
  this.currentMonth = ''
  this.currentCondoId = ''

  const instance = this

  instance.autorun(function () {
    instance.reservationSub.set(false)

    instance.subscription = instance.subscribe("allReservationsByMonth", instance.condoId.get() !== 'all' ? instance.condoId.get() : null, instance.selectedMonth.get());

    if (instance.subscription.ready()) {
      instance.reservationSub.set(true);
    } else {
      console.log("> Subscription is not ready yet.");
    }

    if (instance.currentCondoId !== instance.condoId.get()) {
      instance.currentCondoId = instance.condoId.get()
      instance.subscribe("resources", instance.condoId.get() !== 'all' ? instance.condoId.get() : null);
    }
  });

  let condo = Condos.findOne(this.condoId.get());
  if (condo && condo.settings.options.reservations === false) {
    // FlowRouter.go('/fr/resident');
  }
});

Template.ReservationCalendar.helpers({
  checkedCoworkingEmail: () => {
    return Template.instance().coworkingEmail.get();
  },
  dateToday: () => {
    // helper to display the today's date with the wanted format
    let date = new Date(Date('now'));
    return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
  },
  modalTab: () => {
    return Template.instance().modalTab.get();
  },
  currentCondo: () => {
    return Template.instance().condoId.get();
  },
  errModalCoworking: () => {
    return Template.instance().shortError.get();
  },
  reserveType: () => {
    return Template.instance().reserveType.get();
  },
  choisir: () => {
    return Template.instance().choisir.get();
  },
  options: () => {
    translation = new ModuleReservationIndex(Template.instance().lang.get());
    let instance = Template.instance();
    return {
      id: 'calendar',
      schedulerLicenseKey: "0752529861-fcs-1493371835",
      now: instance.selectedDate.get(),
      locale: instance.lang.get(),
      lang: instance.lang.get(),
      timeFormat: 'HH:mm',
      editable: false, // enable draggable events
      selectable: true,
      timezone: false,
      aspectRatio: 1.8,
      contentHeight: 730,
      scrollTime: "08:00", // undo default 6am scrollTime
      header: {
        left: "today",
        center: "prev title next",
        right: "timelineDay,agendaWeek,month"
      },
      height: 785,
      slotLabelFormat: "HH:mm",
      // slotDuration: (!!instance.data.resourceType && instance.data.resourceType === 'edl' ? "00:15:00" : "00:30:00"),
      slotDuration: "00:15:00",
      resourceColumns: [],
      defaultView: "timelineDay",
      views: {
        month : {
          titleFormat: 'MMMM YYYY'
        },
        timelineThreeDays: {
          type: "timeline",
          duration: { days: 3 }
        }
      },
      selectOverlap: true,
      selectConstraint: "businessHours",
      titleFormat: "dddd D MMMM YYYY",
      axisFormat: "HH:mm",
      displayEventEnd: true,
      eventOverlap: false, // will cause the event to take up entire resource height
      refetchResourcesOnNavigate: true,
      resources: function(callback) {
        let resources;
        if (instance.data.resourceType) {
          resources = Resources.find({
            type: instance.data.resourceType,
            ...(instance.condoId.get() !== 'all' ? {condoId: instance.condoId.get()}: {})
          }).fetch()
        } else {
          resources = _.sortBy(Resources.find(instance.condoId.get() !== 'all' ? {condoId: instance.condoId.get()}: {}).fetch(), (e) => {return ORDER[e.type]})
        }
        let colors = ["rgb(105, 210, 231)", "rgb(255, 165, 160)", "rgb(131, 137, 142)", "rgb(85, 142, 213)",
          "rgb(253, 184, 20)", "rgb(146, 208, 80)", "rgb(255, 70, 80)", "rgb(153, 84, 204)", "rgb(247, 150, 70)",
          "rgb(54, 147, 172)", "rgb(255, 255, 0)"];
        let i = 0;

        let data = [];
        _.each(resources, (resource) => {
          let businessHours = [];

          _.each(resource.horaires, (h) => {
            if (h.open) {
              if (h.openHours && h.openHours.length > 1) {
                _.each(h.openHours, (hs) => {
                  businessHours.push({
                    dow: [h.dow[0]],
                    start: hs.start,
                    end: hs.end,
                  });
                });
              } else {
                businessHours.push({
                  dow: [h.dow[0]],
                  start: h.start,
                  end: h.end,
                });
              }
            } else {
              businessHours.push({
                dow: [h.dow[0]],
                start: '24:00',
                end: '24:00',
              });
            }
          });

          const type = ResourceType.findOne({key: resource.type});

          if (isInt(resource.number)) {
            const onlyOne = resource.number === 1
            for (let n = 1; n <= resource.number; n++) {
              let res = {
                id: resource._id + n,
                resourceId: resource._id,
                title: (resource.type === "meeting" ? resource.name :
                  (resource.type === "coworking" && !onlyOne ? "Place " + n :
                    (resource.type === "equipment" && !onlyOne ? resource.name + ' ' + n :
                      (resource.type === "edl" ? (resource.name === 'edl' ? getTranslatedValue(type) : resource.name) + (!onlyOne ? ' ' + n : '') :
                        resource.name)))),
                category: getTranslatedValue(type),
                type: resource.type,
                subcategory: (resource.type === "meeting" || resource.type === "other" ? translateEtage(resource.etage) : (resource.type === 'edl' ? getTranslatedValue(type) : resource.name)),
                eventColor: colors[i],
                persons: resource.persons ? resource.persons : 0,
                seats: resource.seats ? resource.seats : 0,
                horaires: resource.horaires,
                edlDuration: resource.edlDuration || 45,
                number: n,
                businessHours: businessHours,
              };

              i = (i < colors.length - 1) ? (i + 1) : 0;
              data.push(res);
            }
          } else {
            let res = {
              id: resource._id,
              resourceId: resource._id,
              title: (resource.type === "meeting" ? resource.name :
                (resource.type === "coworking" ? "Place " + resource.number :
                  (resource.type === "equipment" ? resource.name + ' ' + resource.number :
                    (resource.type === "edl" ? (resource.name === 'edl' ? getTranslatedValue(type) : resource.name) :
                      resource.name)))),
              category: getTranslatedValue(type),
              type: resource.type,
              subcategory: (resource.type === "meeting" || resource.type === "other" ? translateEtage(resource.etage) : (resource.type === 'edl' ? getTranslatedValue(type) : resource.name)),
              eventColor: colors[i],
              persons: resource.persons ? resource.persons : 0,
              seats: resource.seats ? resource.seats : 0,
              horaires: resource.horaires,
              edlDuration: resource.edlDuration || 45,
              number: resource.number,
              businessHours: businessHours,
            };

            i = (i < colors.length - 1) ? (i + 1) : 0;
            data.push(res);
          }
        });
        callback(data);
      },
      events: function(start, end, timezone, callback) {
        const canSeeAll = Meteor.userHasRight('reservation', 'seeAll', (instance.condoId.get() !== 'all'? instance.condoId.get(): null));
        let startDate = new Date(start);
        let endDate = new Date(end);
        let resources = Resources.find(instance.condoId.get() !== 'all' ? {condoId: instance.condoId.get()}: {}).fetch();
        let events = _.reduce(resources, (memo, resource) => {
          const reservations = Reservations.find(
            {
              ...(instance.condoId.get() !== 'all' ? {condoId: instance.condoId.get()}: {}),
              resourceId: resource._id,
              rejected: {$ne : true},
              $and: [
                // {
                //   $or: [
                //     {pending: { $exists: false }},
                //     {pending: false}
                //   ]
                // },
                {
                  $or: [
                    {"start": {$gte: startDate.valueOf(), $lt: endDate.valueOf()}},
                    {"end": {$gte: startDate.valueOf(), $lt: endDate.valueOf()}},
                    {"start": {$lt: startDate.valueOf()}, "end": {$gte: endDate.valueOf()}}
                  ]
                }
              ]
            }).fetch();
          return memo.concat(_.map(reservations, (event) => {
            const user = UsersProfile.findOne(event.origin);
            const booker = !!user ? user.firstname + " " + user.lastname : '';
            event.resourceId = resource._id + (isInt(event.number) ? event.number: '');
            event.start = new Date(event.start);
            event.end = new Date(event.end);
            event.booker = event.title = (event.origin === Meteor.userId() ? booker : (canSeeAll && resource.type !== 'edl' ? booker : translation.moduleReservationIndex['booked']));
            return (event);
          }));
        }, []);
        callback(events);
      },
      eventRender: function(event, element){
        if (checkByEvent(event)) {
          if (!event.pending) {
            element.attr('title', event.booker);
            element.find(".fc-title").html(event.booker);
          } else {
            element.addClass('event-pending');
            element.attr('title', '');
            element.find(".fc-title").html('');
          }
        } else {
          return false;
        }
      },
      eventAfterAllRender: function(view){
        generateNavigator(instance.lang.get(), instance);
        showEventLine();

        $("#calendar .fc-timelineDay-view .fc-body .fc-scroller").on("scroll",function() {
          showTimeLineEvent();
        });
        $(".fc-nonbusiness").text(translation.moduleReservationIndex['close']);
      },
      eventClick: function(calEvent, jsEvent, view) {
      },
      select: function( start, end, jsEvent, view, resource ) {

        const check = +start.format('YYYYMMDDHHmm');

        const dateObj = new Date();
        const today = +`${dateObj.getFullYear()}${('0' + (dateObj.getMonth() + 1)).slice(-2)}${('0' + dateObj.getDate()).slice(-2)}${('0' + dateObj.getHours()).slice(-2)}${('0' + dateObj.getMinutes()).slice(-2)}`;
        if (instance.condoId.get() === 'all') {
          const translation = new ModuleReservationError(FlowRouter.getParam('lang') || 'fr');
          sAlert.error(translation.moduleReservationError["select_condo_warning"]);
        } else {
          if (check < today)
          {
            const translation = new ModuleReservationError(FlowRouter.getParam('lang') || 'fr');
            // Previous Day. show message if you want otherwise do nothing.
            // So it will be unselectable
            sAlert.error(translation.moduleReservationError["invalid_start_time"]);
          } else {
            if (typeof instance.data.onSelect === 'function') {
              const time = {
                start: start.toDate(),
                end: end.toDate()
              };
              if (view.type === "timelineDay") {
                instance.data.onSelect(resource, time, false)
              }
              if (view.type === "agendaWeek") {
                instance.data.onSelect(!!instance.data.selectedResource ? instance.data.selectedResource : null, time, false, true)
              }
              if (view.type === "month") {
                $('#calendar').fullCalendar('changeView', 'timelineDay', start.toDate())
              }

            }
          }
        }
      },
      viewRender: function(view, element) {
        if (view.type === "agendaWeek" ) {
          $("#calendar").find('.fc-toolbar > div > h2').empty().append(
            view.start.format('D - ')+view.end.format('D MMMM YYYY')
          );
        } else if ( view.type === "month") {
          $("#calendar").find('.fc-toolbar > div > h2').empty().append(
            view.intervalStart.format('MMMM YYYY')
          );
        }
        const currentdate = view.intervalStart;
        $("#minicalendar").datepicker('setDate', new Date(currentdate));
        instance.selectedDate.set(new Date(currentdate));
        Session.set('resaSelectedDate', new Date(currentdate));
      },
      eventDrop: function(event, delta, revertFunc) {
      },
      eventResize: function(event, delta, revertFunc) {
      }
    };
  },
  resourcesReady: () => {
    return Template.instance().reservationSub.get()// && Template.instance().reservationSub.ready();
  },
  // Choix de "Categorie".
  specificTypes: () => {
    let reserveType = Template.instance().reserveType.get();
    if (Template.instance().modalTab.get() === "eMenager")
      reserveType = "equipment";
    let resources = Resources.find({
      type: reserveType,
      ...(Template.instance().condoId.get() !== 'all' ? {condoId: Template.instance().condoId.get()}: {})
    }).fetch();
    let espaces = _.uniq(_.map(resources, (r) => {
      if (reserveType === "meeting")
        return r.etage;
      return r.name;
    }));
    if (espaces.length > 0)
      Template.instance().specificTypes.set(espaces[0]);
    return (espaces);
  },
  showForm: () => {
    return Session.get('showReservationForm');
  },
  onSelectType: () => {
    const instance = Template.instance();
    const calendar = $("#calendar");
    const resources = calendar.fullCalendar( "getResources" );
    return (val) => {
      instance.selectedType.set(val);
      $nodes = getNodes(resources, val, instance.lang.get(), instance);
      treeviewOptions.data = $nodes;
      $checkableTree = $("#navigator-container").treeview(treeviewOptions);

      $checkableTree.treeview('expandAll', { silent: true });
      calendar.fullCalendar('rerenderEvents');
    }
  },
  getTypeOptions: () => {
    const instance = Template.instance();
    translation = new ModuleReservationIndex(FlowRouter.getParam("lang") || "fr");

    const options = [{
      id: '-',
      text: translation.moduleReservationIndex['all_resources']
    }]

    return !!instance.categories.get() ? [...options, ..._.map(instance.categories.get(), (value) => {
      return {
        id: value.title,
        text: value.title
      }
    })] : options
  },
  getSelectedType: () => {
    return Template.instance().selectedType.get()
  }
});

Template.ReservationCalendar.rendered = function() {
  $checkableTree = false;
  let template = Template.instance();

  generateNavigatorLoaded = false;

  const selector = $("#minicalendar");
  selector.datepicker({
    language: Template.instance().lang.get(),
    maxViewMode: 0,
    autoclose: true,
    todayHighlight: true,
    minDate: new Date(),
    beforeShowDay: function(date) {
      const formattedDate = moment.utc(date).add(12, 'h').startOf('day').format('x');
      if (_.isArray(template.data.eventDates) && template.data.eventDates.includes(formattedDate)) {
        return {
          classes: 'booked-date'
        };
      }
    },
    templates: {
      leftArrow: '<i class="fa fa-chevron-left"></i>',
      rightArrow: '<i class="fa fa-chevron-right"></i>'
    }
  })
    .on("changeDate", function(e) {
      // `e` here contains the extra attributess
      let m = moment.utc(e.date).add(12, 'h'); // add 12 h for timezone workaround
      $("#calendar").fullCalendar("gotoDate", m);
      template.selectedDate.set(m.toDate());
      Session.set('resaSelectedDate', m.toDate());
      template.selectedMonth.set(m.startOf('month').toDate());
    }).on('changeMonth', function(event) {
      let m = moment.utc(event.date).add(12, 'h'); // add 12 h for timezone workaround
      template.selectedDate.set(m.startOf('month').toDate());
      template.selectedMonth.set(m.startOf('month').toDate());
  });

  selector.datepicker("setDate", template.selectedDate.get());
};

Template.ReservationCalendar.events({
  'click #backbutton': function (e, t) {
    Session.set('showReservationCalendar', false);
  },
  'click #langFr, click #langEn': function(e, t) {
    // generateNavigator();
    showEventLine();
  }
});

function generateModalCategorySelect() {
  translation = new ModuleReservationIndex(FlowRouter.getParam("lang") || "fr");
  const selectCategoryFilter = $('#cat-filter');
  if ($categories.length > 1) {
    selectCategoryFilter
      .append($('<option>', { value : '-' })
        .text(translation.moduleReservationIndex['all_resources']));
  }
  $.each($categories, function(key, value) {
    selectCategoryFilter
      .append($('<option>', { value : value.title })
        .text(value.title));
  });
}

function generateModalResourceSelect() {
  let calendar = $("#calendar");
  let resources = calendar.fullCalendar( "getResources" );
  let selectResource = $('#frm-resource');
  let cat = $('#frm-cat').val();
  selectResource.html("");
  $.each(resources, function(key, value) {
    if (value.category !== cat)
      return;
    let sub = _.find($sub, function(o) { return o.title === value.subcategory; });

    // let color = '';
    // if (!!value.eventColor) {
    //     color = ' style="color:'+value.eventColor+';"';
    // }

    // let text = "<i "+color+" class='fa fa-circle' ></i>&nbsp" + (!!sub.title ? sub.title + ' - ' : '') + value.title;
    let text = (!!sub.title ? sub.title + ' - ' : '') + value.title;

    selectResource
      .append($('<option>', { value : value.id })
        .text(text));
  });
}

function generateNavigator(lang, instance) {
  // get categories
  translation = new ModuleReservationIndex(lang);
  let calendar = $("#calendar");
  let resources = calendar.fullCalendar( "getResources" );

  $categories = _.uniq(_.map(resources, (r) => {
    return {title: r.category};
  }), (item, key, a) => {
    return item.title;
  });
  instance.categories.set($categories);
  // get sub categories
  $sub = _.uniq(_.map(resources, (r) => {
    return {title: r.subcategory, category: r.category};
  }), (item, key, a) => {
    return item.title;
  });

  loopResourcesAppend(resources, lang);

  if (!generateNavigatorLoaded) {
    generateModalCategorySelect();
    generateModalResourceSelect();

    $nodes = getNodes(resources, '-', lang, instance);

    treeviewOptions.data = $nodes;
    $checkableTree = $("#navigator-container").treeview(treeviewOptions);

    $checkableTree.treeview('expandAll', { silent: true });

    generateNavigatorLoaded = true;
  }
  toggleByNodes(resources);
}

function checkByEvent(event) {
  if ($checkableTree) {
    let checked = $checkableTree.treeview('getChecked');
    let res = _.find(checked, function (o) {
      return ((o['data-id'] === event.resourceId) && o['data-resource']);
    });

    return !!res;
  }
  return true;
}

// for persistent views on timeline
function toggleByNodes(resources) {
  let calendar = $("#calendar")
  let type = calendar.fullCalendar('getCalendar').getView().type;

  if (type === 'timelineDay') {
    let view = calendar.data("fullCalendar").view;
    let resourceRows = view.resourceRowHash;
    view.batchRows();

    for(let resourceId in resourceRows) {
      let row = resourceRows[resourceId];
      row.hide();
    }
    let checked = $checkableTree.treeview('getChecked');
    let res = _.filter(checked, function(o) { return o['data-resource']; });
    $.each( res, function( key, value ) {
      let row = resourceRows[value['data-id']];
      row.show();
    });

    view.unbatchRows();
  }
}

function toggleChild(node, action) {
  if (!!node.nodes) {
    $.each( node.nodes, function( key, value ) {
      $checkableTree.treeview(action, [ value.nodeId, { silent: false } ]);

      if (!!value.nodes) {
        toggleChild(value, action)
      }
    });
  }
}

function getNodes(resources, filter, lang, instance) {
  translation = new ModuleReservationIndex(lang);
  let nodes = [];
  let categories = $categories;
  if (filter !== '-') {
    categories = _.filter($categories, function(o) { return o.title === filter; });
  }

  const selectedResource = instance.data.selectedResource;
  let selectedId = false;
  if (!!selectedResource) {
    if (selectedResource.type === 'edl' && !!selectedResource.total) {
      selectedId = []
      for (let i = 1; i <= selectedResource.total; i++) {
        selectedId.push(selectedResource.realId + i)
      }
    } else {
      selectedId = [selectedResource._id]
    }
  }

  if (!!categories) {
    $.each(categories, function (key, value) {
      let sub = _.filter($sub, function (o) {
        return o.category === value.title;
      });
      let child = [];

      $.each(sub, function (k, v) {
        let res = _.filter(resources, function (o) {
          return o.subcategory === v.title;
        });
        let childTwo = [];

        $.each(res, function (r, i) {
          let color = '';

          if (typeof i.eventColor !== "undefined") {
            color = '<span style="color:' + i.eventColor + ';" class="fa fa-circle"></span>&nbsp';
          }

          let text = color + i.title;

          let nodeThree = {
            text: text,
            "data-id": i.id,
            "data-resource": true,
            selectable: false,
            state: {
              checked: !selectedId ? true : selectedId.includes(i.id)
            },
            name: i.title
          };

          if (i.type === 'edl') {
            child.push(nodeThree);
          } else {
            childTwo.push(nodeThree);
          }
        });
        let nodeTwo = {
          text: v.title,
          "data-id": v.title,
          nodes: childTwo,
          selectable: false,
          state: {
            checked: true
          }
        };
        if (v.category !== v.title) {
          child.push(nodeTwo);
        }
      });

      if (filter !== '-') {
        nodes = child;
      } else {
        let node = {
          text: value.title,
          "data-id": value.title,
          nodes: child,
          selectable: false,
          state: {
            checked: true
          }
        };

        nodes.push(node);
      }
    });
  }

  return nodes;
}

// Append resources header on timeline
function loopResourcesAppend(resources, lang) {
  translation = new ModuleReservationIndex(lang);
  $.each( resources, function( key, value ) {
    let color = '';
    if (!!value.eventColor) {
      color = ' style="color:'+value.eventColor+';"';
    }
    let cat = _.find($categories, function(o) { return o.title === value.category; });
    let sub = _.find($sub, function(o) { return o.title === value.subcategory; });

    let parent = (!!cat.title ? cat.title + ' > ' : '') + (value.type !== 'edl' && !!sub.title ? sub.title + ' > ' : '');

    let seat = (!!value.seats ? '&nbsp;<img class="seat-icon" src=/img/icons/chair-tiny.svg></img>&nbsp;'+value.seats+' pers.' : '');
    let person = (!!value.persons ? '&nbsp;<i class="fa fa-male"></i>&nbsp;'+value.persons+' pers.' : '');

    let prepend = "<p class='event-header'><i "+color+" class='fa fa-circle' ></i>&nbsp";
    prepend += parent + value.title;
    if (value.type === "meeting")
      prepend += '<small>'+seat+person+"</small></p>";
    else
      prepend += "</p>";
    $("tr[data-resource-id='"+value.id+"'] div.fc-event-container").prepend(prepend);

    if (value.children.length > 0) {
      loopResourcesAppend(value.children, lang)
    }
  });

  showTimeLineEvent();
}

function showEventLine() {
  let type = $('#calendar').fullCalendar('getCalendar').getView().type;

  if (type === "timelineDay") {

    let calendarDate = $('#calendar').fullCalendar('getDate');
    setTimeDayLineBar(calendarDate.format('YYYY-M-D'));

    showTimeLineEvent();
  }

  if (type === "agendaWeek") {
    setTimeWeekLineBar()
  }
}

function setTimeWeekLineBar() {

  $(".fc-body .fc-scroller-canvas .fc-weeklinebar").remove();

  const dateObj = new Date();
  const today = `${dateObj.getFullYear()}-${('0' + (dateObj.getMonth() + 1)).slice(-2)}-${('0' + dateObj.getDate()).slice(-2)}`;
  let h = dateObj.getHours();
  let m = dateObj.getMinutes();
  let timeband = (h*60+m) /15 * 22 - 3;
  let barleft = 0;
  $('#calendar .fc-agendaWeek-view .fc-head .fc-day-header').each(function(){
    if($(this).attr('data-date') == today){
      barleft = $(this).position().left;
    }
  });
  if(barleft !== 0){
    $("#calendar .fc-body .fc-scroller").append("<div class='fc-weeklinebar'></div>");
    $("#calendar .fc-weeklinebar").css("width", $('#calendar th.fc-day-header').width());
    $("#calendar .fc-weeklinebar").css('left', barleft);
    $("#calendar .fc-weeklinebar").css("transform", "translateY("+timeband+"px)");
    $('#calendar .fc-slats table tr').each(function(){
      if(!$(this).hasClass('fc-minor')){
        let times = $(this).attr("data-time").split(":");
        if(parseInt(times[0]) === parseInt(h))
          $(this).find('span').css("color","rgb(54, 147, 172)");
      }
    });
  }
}

function setTimeDayLineBar(showingdate) {

  // let today = moment.tz(moment(), "Europe/Paris").format('YYYY-MM-DD');
  const dateObj = new Date();
  let today = `${dateObj.getFullYear()}-${dateObj.getMonth() + 1}-${dateObj.getDate()}`

  if (today === showingdate) {
    // let d = moment.tz(moment(), "Europe/Paris");
    // let h = d.hour();
    // let m = d.minute();

    let h = dateObj.getHours()
    let m = dateObj.getMinutes()

    let width = $("#calendar > div.fc-view-container > div > table > thead > tr > td.fc-time-area.fc-widget-header > div > div > div > div.fc-content > table > tbody > tr > th:nth-child(1)").outerWidth();

    let timeband = h*width + (m / 60 * width);

    $("#calendar .fc-body .fc-scroller-canvas").append("<div class='fc-daylinebar'></div>");
    $("#calendar .fc-daylinebar").css("transform", "translateX(" + timeband + "px)");
    $('#calendar > div.fc-view-container > div > table > thead > tr > td.fc-time-area.fc-widget-header > div > div > div > div.fc-content > table > tbody > tr > th.fc-widget-header').each(function(){
      let times = $(this).attr("data-date").split("T");
      let hour = times[1].split(":");
      if(parseInt(hour[0]) === h)
        $(this).addClass("now-hour");
    });
  } else {
    // view.unrenderNowIndicator();
    $(".fc-body .fc-scroller-canvas .fc-daylinebar").remove();
  }
}

function showTimeLineEvent() {
  let scrollLeft = $('#calendar > div.fc-view-container > div > table > tbody > tr > td.fc-time-area.fc-widget-content.fc-unselectable > div > div.fc-scroller').scrollLeft();
  $('#calendar p.event-header').css('left', scrollLeft);
}
