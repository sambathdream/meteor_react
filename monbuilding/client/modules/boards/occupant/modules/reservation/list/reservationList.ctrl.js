/**
 * Created by kuyarawa on 05/02/18.
 */
import { ModuleReservationIndex, Email, CommonTranslation } from '/common/lang/lang.js';

const insertDateHeader = (array) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    let currentDate = '';
    return array.map((m) => {
        const newDay = moment(m.day).format('DDMMYY') === moment().format('DDMMYY') ? translate.moduleReservationIndex['today_book'] : moment(m.day).format('dddd, D MMM YYYY');

        let timeString = '';
        let isAfter = false;
        const start = moment(m.event.start).utc();
        const end = moment(m.event.end).utc();
        if (start.format('YYMMDD') === end.format('YYMMDD')) {
            timeString = `${start.format('HH:mm')} - ${end.format('HH:mm')}`
        } else {
            const day = moment(m.day);
            if (start.format('YYMMDD') === day.format('YYMMDD')) {
                timeString = `${start.format('HH:mm')} - ${end.format('D MMM YYYY, HH:mm')}`
            } else {
                isAfter = true;
                timeString = `${start.format('D MMM YYYY, HH:mm')} - ${end.format('D MMM YYYY, HH:mm')}`
            }
        }

        const newData = {
            ...m,
            string: timeString,
            isAfter
        };

        if (currentDate !== newDay) {
            currentDate = newDay
            return {...newData, label: newDay}
        } else {
            return {...newData}
        }
    })
}

const date = new Date()
const realOffset = date.getTimezoneOffset();

const mapReservationData = (instance, data, userId, managerId, status) => {
    let events = [];

    let regexp = new RegExp(instance.resaSearch.get(), "i");
    instance.resaLength.set(0);
    instance.resaData.set(events);
    const lang = (FlowRouter.getParam("lang") || "fr");
    moment.locale(lang);
    const offset = moment().utcOffset();
    const isManager = Meteor.isManager;

    const translate = new ModuleReservationIndex(lang);

    if (data.length > 0) {
      _.each(data, (y, index) => {

        let canSeeReservation = false
        if (isManager && instance.data.condoId === 'all') {
          const see = Meteor.listCondoUserHasRight('reservation', 'see')
          if (Array.isArray(see) === true) {
            canSeeReservation = !!see.find(seeAllCondoId => seeAllCondoId === y.condoId)
          }
        } else {
          if (isManager) {
            canSeeReservation = true
          } else {
            canSeeReservation = ((y.origin === Meteor.user()._id ||
            (Array.isArray(y.participants) && y.participants.includes(Meteor.user()._id))))
          }
        }

        if (canSeeReservation) {
          const x = Resources.findOne(y.resourceId);
          const startHour = moment(y.start).subtract(offset, 'm');
          const endHour = moment(y.end).subtract(offset, 'm');
          let specStatus = true;
          const user = UsersProfile.findOne(y.origin);
          const booker = y.origin === Meteor.user()._id ? translate.moduleReservationIndex['you'] : (user ? `${user.firstname} ${user.lastname}` : '')
          if (!!instance.data.specificDate) {
            specStatus = false;
            const specificStart = instance.data.specificDate.start;
            const specificEnd = instance.data.specificDate.end;

            if (((specificStart.isSameOrAfter(startHour) && specificStart.isBefore(endHour)) || (specificEnd.isAfter(startHour) && specificEnd.isSameOrBefore(endHour))) ||
              ((startHour.isSameOrAfter(specificStart) && startHour.isBefore(specificEnd)) || (endHour.isAfter(specificStart) && endHour.isSameOrBefore(specificEnd)))) {
              specStatus = true;
            }
          }

          if (x && specStatus) {
            const start = moment(y.start).subtract(offset, 'm');
            const end = moment(y.end).subtract(offset, 'm').add(-1, 'm');
            const diff = end.diff(start, 'days');
            let name = x.name;
            if (x.type === 'edl' && name === 'edl') {
              const type = ResourceType.findOne({key: 'edl'});
              name = type ? getTranslatedValue(type) : x.name;
            }

            for (let i = 0; i <= diff; i++) {
              const event = {
                ...y,
                startHour: startHour.toDate(),
                endHour: endHour.toDate()
              };
              let res = {
                resource_id: x._id,
                name: name,
                type: x.type,
                total: (x.number ? x.number : false),
                number: (y.number ? y.number : false),
                validatedByMe: x.needValidation && x.needValidation.status && x.needValidation.managerIds.includes(Meteor.user()._id) && !!y.pending,
                day: +start.add(i === 0 ? 0 : 1, 'd').format('x'),
                booker: booker,
                bookerName: (user ? `${user.firstname} ${user.lastname}` : '')
              };

              if (
                (status === 'history' && (y.start < +moment().subtract(realOffset, 'm').format('x') || !!y.rejected)) ||
                (status === 'confirmed' && moment().subtract(realOffset, 'm').isBefore(end)) ||
                (status === 'pending' && moment().subtract(realOffset, 'm').isBefore(end))
              ) {
                events.push({_id: y._id + i, start: event.start, creation: event.creation, event: event, ...res})
              }
            }
          }
        }

        if (typeof instance.data.getCalendarEvent === 'function') {
          const dates = _.map(_.groupBy(events, (e) => {
            return e.day
          }), (grouped) => {
            return moment(grouped[0].day).startOf('day').format('x')
          })

          instance.data.getCalendarEvent(dates);
        }

        if (index + 1 === data.length) {
          events = _.filter(events, (e) => {
            return regexp.test(e.name) || regexp.test(e.bookerName) || regexp.test(e.event.title)
          })
          instance.resaLength.set(events.length);
          instance.dataReady.set(true)
          if (status === 'history') {
            instance.resaData.set(insertDateHeader(_.chain(events).sortBy('creation').sortBy('start').sortBy('day').reverse().value()));
          } else {
            instance.resaData.set(insertDateHeader(_.chain(events).sortBy('creation').sortBy('start').sortBy('day').value()));
          }
        }
      });
    } else {
      instance.dataReady.set(true)
      if (typeof instance.data.getCalendarEvent === 'function') {
        instance.data.getCalendarEvent([]);
      }
    }
};

const getProps = (props) => {
    return Template.instance().data[props] ? Template.instance().data[props] : false;
};

Template.ReservationList.onCreated(function () {
    this.resaLength = new ReactiveVar(0);
    this.currentLength = new ReactiveVar(0);
    this.resaData = new ReactiveVar([]);
    this.dateChanged = new ReactiveVar(false)
    this.currentStatus = new ReactiveVar('')
    this.resaSearch = new ReactiveVar('')
    this.startFilter = new ReactiveVar(moment().startOf('year'))
    this.endFilter = new ReactiveVar(moment().endOf('year'))
    this.dataReady = new ReactiveVar(false)

    this.initDatePicker = (translation, lang, instance) => {
      setTimeout(function() {
        $('#daterange-stats').daterangepicker({
          "ranges": {
            [translation.commonTranslation["today"]]: [
              moment(),
              moment().locale(lang).endOf('days')
            ],
            [translation.commonTranslation["yesterday"]]: [
              moment().locale(lang).subtract(1, 'days'),
              moment().locale(lang).subtract(1, 'days')
            ],
            [translation.commonTranslation["this_week"]]: [
              moment().locale('fr').startOf('weeks'),
              moment().locale('fr').endOf('weeks'),
            ],
            [translation.commonTranslation["last_week"]]: [
              moment().locale('fr').startOf('weeks').subtract(1, "week"),
              moment().locale('fr').endOf('weeks').subtract(1, "week")
            ],
            [translation.commonTranslation["this_month"]]: [
              moment().locale(lang).startOf('month'),
              moment().locale(lang).endOf('month'),
            ],
            [translation.commonTranslation["last_month"]]: [
              moment().locale(lang).subtract(1, "month").startOf('month'),
              moment().locale(lang).subtract(1, "month").endOf('month')
            ],
            [translation.commonTranslation["this_year"]]: [
              moment().locale(lang).startOf('year'),
              moment().locale(lang).endOf('year').subtract(1, 'days')
            ],
            [translation.commonTranslation["last_year"]]: [
              moment().locale(lang).startOf('year').subtract(1, 'year'),
              moment().locale(lang).startOf('year').subtract(1, 'year').add(11, 'month')
            ]
          },
          "alwaysShowCalendars": true,
          "startDate": instance.startFilter.get(),
          "endDate": instance.endFilter.get(),
          "opens": "left",
          "showCustomRangeLabel": false,
          "autoApply": true,
          "locale" : $.fn.datepicker.dates[lang]
        }, function(start, end, label) {
          let endDate = end.clone();

          let start_month = start.clone().startOf('month');
          let end_month = end.clone().endOf('month');
          if (label != null) {
            setTimeout(function () {
              $('#daterange-stats').val(label);
            }, 10);
          } else {
            setTimeout(function () {

              if (start.format('DD MM YY') == end.format('DD MM YY')) {
                if (start.format('YYYY') == moment().format('YYYY')) {
                  $('#daterange-stats').val(start.locale(lang).format('DD MMM'));
                }
                else {
                  $('#daterange-stats').val(start.locale(lang).format('DD MMM YYYY'));
                }
              }

              else if (start.format('MM YY') == end.format('MM YY')) {
                if (start.format('DD MM YY') == start_month.startOf('month').format('DD MM YY') && end.format('DD MM YY') == end_month.endOf('month').format('DD MM YY')) {

                  if (start.format('YYYY') == moment().format('YYYY')) {
                    $('#daterange-stats').val(start.locale(lang).format('MMMM'));
                  }

                  else {
                    $('#daterange-stats').val(start.locale(lang).format('MMMM YYYY'));
                  }
                }

                else {

                  if (start.format('YYYY') == moment().format('YYYY')) {
                    $('#daterange-stats').val(start.locale(lang).format('DD') + ' - ' + end.locale(lang).format('DD MMM'));
                  }
                  else {
                    $('#daterange-stats').val(start.locale(lang).format('DD') + ' - ' + end.locale(lang).format('DD MMM YYYY'));
                  }
                }
              }
              else {
                let ret = "";
                if (start.format('YYYY') == moment().format('YYYY')) {
                  ret = (start.locale(lang).format('DD MMM') + ' - ');
                }
                else {
                  ret = (start.locale(lang).format('DD MMM YYYY') + ' - ');
                }
                if (end.format('YYYY') == moment().format('YYYY')) {
                  ret += (end.locale(lang).format('DD MMM'));
                }
                else {
                  ret += (end.locale(lang).format('DD MMM YYYY'));
                }
                $('#daterange-stats').val(ret);
              }
            }, 10)
          }
          instance.startFilter.set(start.locale(lang).startOf('d'));
          instance.endFilter.set(endDate.locale(lang).endOf('d'));
          instance.dateChanged.set(true)
        });
        $('#daterange-stats').val(translation.commonTranslation["this_year"]);
      }, 500);
    }
});

Template.ReservationList.events({
    'click .see-detail': (e, t) => {
        const element = $(e.currentTarget);
        const id = element.data('id');
        t.data.onSelectResa(id);

        const book = Reservations.findOne(id);
        const resource = Resources.findOne(!!book ? book.resourceId : null);

        let showDetailModal = true;

        if (!!book && !book.rejected && !!resource) {
            const isManager = Meteor.isManager;
            const date = new Date()
            const realOffset = date.getTimezoneOffset();
            if (isManager && (resource.needValidation && resource.needValidation.status && resource.needValidation.managerIds.includes(Meteor.user()._id)) && book.origin !== Meteor.user()._id && book.start > +moment().subtract(realOffset, 'm').format('x')) {
                showDetailModal = false;
                if (typeof t.data.goToValidate === 'function') {
                    t.data.goToValidate(id)
                }
            }
        }

        if (showDetailModal) {
            $("#reservation-detail").modal("show");
        }
    },
  'scroll .scrollable-area': function (event, template) {
    if (event.currentTarget.offsetHeight + event.currentTarget.scrollTop == event.currentTarget.scrollHeight) {
      const length = template.resaLength.get()
      if (length < 1000 && length !== template.currentLength.get()) {
        template.currentLength.set(length)
        template.data.onLoadMore()
      }
    }
  }
});

Template.ReservationList.helpers({
    getBooking: () => {
        const instance = Template.instance();
        const status = getProps('status');
        if (status !== instance.currentStatus.get()) {
          instance.currentStatus.set(status);
          instance.currentLength.set(0);
          instance.resaLength.set(0);
        }
        const startFilter = +instance.startFilter.get().format('x')
        const endFilter = +instance.endFilter.get().format('x')
        const isManager = Meteor.isManager;
        const where = !!Template.instance().data.condoId && Template.instance().data.condoId !== 'all' ? {condoId : Template.instance().data.condoId} : {};
        const data = Reservations.find({
          ...where,
          $or: [
            {$and: [
              {start: {$gte: startFilter}},
              {start: {$lt: endFilter}}
            ]},
            {$and: [
              {end: {$gt: startFilter}},
              {end: {$lte: endFilter}}
            ]},
            {$and: [
              {start: {$lte: startFilter}},
              {end: {$gte: endFilter}}
            ]}
          ]
        }).fetch()

        mapReservationData(instance, data, (isManager ? null : Meteor.user()._id), Meteor.user()._id, status);
    },
    getDataLength: () => {
        return !!Template.instance().resaLength.get();
    },
    getData: () => {
      const instance = Template.instance()
      return !instance.data.specificDate ? instance.resaData.get() : instance.resaData.get().filter((d) => {
        return d.day <= +instance.data.specificDate.end.format('x') && d.day >= +instance.data.specificDate.start.format('x')
      });
    },
    getName: (item) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return `${item.name}${(!!item.number && item.total > 1 && item.type !== 'edl' ? ' ' + translate.moduleReservationIndex[item.type === 'coworking' ? 'seat' : 'number'] + ' ' + item.number : '')}`
    },
    getBooker: (event) => {
        if (event) {
            const lang = (FlowRouter.getParam("lang") || "fr");
            const translate = new ModuleReservationIndex(lang);
            const user = UsersProfile.findOne(event.origin);
            return event.origin === Meteor.user()._id ? translate.moduleReservationIndex['you'] : (user ? `${user.firstname} ${user.lastname}` : '');
        }
    },
    getFloor: (floor) => {
        return translateEtage(floor);
    },
    getEmptyProps: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const isManager = Meteor.isManager;
        const instance = Template.instance();
        const tab = instance.data.status
        if (instance.data.condoId === 'all') {
          return {
            title: translate.moduleReservationIndex[tab === 'pending'? "no_pending_booking": "no_booking"],
            subTitle: translate.moduleReservationIndex["empty_content_all"]
          }
        } else {
          if (Template.instance().data.haveResources) {
            return {
              title: translate.moduleReservationIndex[tab === 'pending'? "no_pending_booking": "no_booking"],
              subTitle: translate.moduleReservationIndex["empty_content"],
              buttonLabel: translate.moduleReservationIndex["empty_button"],
              onButtonClick: () => {
                Session.set('showReservationForm', true);
              }
            }
          } else if (isManager) {
            const lang = FlowRouter.getParam("lang") || "fr";
            const canAddResources = (instance.data.condoId === null ? Meteor.listCondoUserHasRight("reservation", "addResources").length > 0 : Meteor.userHasRight('reservation', 'addResources', instance.data.condoId));
            let extraProps = {};
            if (canAddResources) {
              extraProps = {
                buttonLabel: translate.moduleReservationIndex["empty_button_label"],
                onButtonClick: () => {
                  Session.set('selectedCondo', instance.data.condoId);
                  FlowRouter.go("app.gestionnaire.ressources.form", { lang, condo_id:  instance.data.condoId, resourceId: 'new' });
                }
              }
            }
            return {
              title: translate.moduleReservationIndex["empty_title_no_ressource"],
              subTitle: translate.moduleReservationIndex["empty_subtitle"],
              ...extraProps
            }
          }
          else {
            return {
              title: translate.moduleReservationIndex["empty_title_no_ressource"],
              subTitle: translate.moduleReservationIndex["empty_sub_occupant"],
              // buttonLabel: translate.moduleReservationIndex["empty_button_label"],
              // onButtonClick: () => {
              //     FlowRouter.go("app.gestionnaire.ressources.form",  {resourceId: "new"});
              // }
            }
          }
        }
    },
    rejected: (event) => {
        return event && !!event.rejected;
    },
    getBookLength: () => {
        return Counts.get('book-length');
    },
    isNotLoadedAll: () => {
        return Counts.get('book-length') > Template.instance().data.limit
    },
    loadButton: () => {
        const instance = Template.instance();
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return {
            label: translate.moduleReservationIndex["load_more"],
            class: 'danger',
            onClick: () => {
                instance.data.onLoadMore();
            }
        }
    },
    selectedCondo: () => {
        return Template.instance().data.condoId;
    },
    condoName: (id) => {
        let condo = Condos.findOne({_id: id});
        if (condo) {
            if (condo.info.id !== -1)
                return condo.info.id + ' - ' + condo.getName();
            else
                return condo.getName();
        }
    },
    isMini: () => {
      return !!Template.instance().data.mini
    },
    isNew: (book) => {
      const status = getProps('status');
      const isManager = Meteor.isManager;

      let isNew = false

      if (isManager) {
        if (status === 'pending') {
          isNew = !!book.validatedByMe
        } else if (status === 'confirmed') {
          isNew = !!book && !!book.event && book.event.pending === false && book.event.rejected === false && !book.event.userViews.includes(Meteor.user()._id)
        }
      }
      if (!isManager && !!book && !!book.event && book.event.userViews && book.event.start >= +moment().subtract(realOffset, 'm')) {
        isNew = !book.event.userViews.includes(Meteor.user()._id)
      }
      return isNew
    },
    selectedDate: () => {
      const instance = Template.instance();
      if (!!instance.data.specificDate) {
        let lang = (FlowRouter.getParam("lang") || "fr")
        moment.locale(lang);
        // return the selected date
        return instance.data.specificDate.start.format('D MMMM Y');
      }
    },
    onSearch: () => {
      const template = Template.instance();
      return (val) => {
        template.resaSearch.set(val)
      }
    },
    isSearchActive: () => {
      return !!Template.instance().resaSearch.get() || Template.instance().dateChanged.get()
    },
    isDataReady: () => {
      return !!Template.instance().dataReady.get()
    },
    renderDatePicker: () => {
      let lang = FlowRouter.getParam("lang") || "fr"
      const translation = new CommonTranslation(lang)
      const instance = Template.instance();
      setTimeout(() => {
        instance.initDatePicker(translation, lang, instance);
      }, 500);
    },
});
