/**
 * Created by kuyarawa on 22/02/18.
 */
import { ModuleReservationIndex } from '/common/lang/lang.js';

Template.ReservationTimeTab.onCreated(function () {
    this.form = new ReactiveDict();

    const sessionData = Session.get('reservationForm');
    this.form.setDefault({
        allDay: sessionData && sessionData.isAllDay !== undefined ? sessionData.isAllDay : false,
        start: sessionData && sessionData.startHour !== undefined ? sessionData.startHour :null,
        end: sessionData && sessionData.endHour !== undefined ? sessionData.endHour :null
    })
});

Template.ReservationTimeTab.rendered = function () {

};

const initDatePicker = (lang, instance) => {
    const startDateElement = $('#start-date');
    const endDateElement = $('#end-date');
    const d = new Date()
    const offset = d.getTimezoneOffset();

    const format = !instance.form.get('allDay') ? 'dddd, D MMM YYYY, HH:mm': 'dddd, D MMM YYYY';
    const defaultOptions = {
        format: format,
        locale: lang,
        allowInputToggle: true,
        stepping: 15,
        icons: {
            date: 'custom-icon calendar-icon',
            up: 'custom-icon chevron-up',
            down: 'custom-icon chevron-down',
            // previous: 'custom-icon chevron-left',
            // next: 'custom-icon chevron-right',
        }
    }

    startDateElement.datetimepicker({
        ...defaultOptions,
        defaultDate: instance.form.get('start'),
        // minDate: new Date()
        minDate: moment().add(-1, 'd').subtract(offset, 'm').toDate()
    });

    startDateElement.data("DateTimePicker").options({format});

    endDateElement.datetimepicker({
        ...defaultOptions,
        defaultDate: instance.form.get('end'),
        useCurrent: false //Important! See issue #1075
    });
    startDateElement.on("dp.hide", function (e) {
        if (endDateElement.data("DateTimePicker")) {
          endDateElement.data("DateTimePicker").minDate(e.date);
        }
        instance.form.set('start', e.date.toDate());
        if (instance.form.equals('allDay', true)) {
            instance.form.set('end', e.date.toDate());
        }
        if (e.date.isAfter(moment(instance.form.get('end')))) {
          const newEnd = e.date.add(15, 'm')
          instance.form.set('end', newEnd.toDate())
          endDateElement.data("DateTimePicker").date(newEnd)
        }
    });

    startDateElement.on("dp.show", function (e) {
      $('.day:not(".disabled")').on('click', function(){
        if (!instance.form.get('allDay')) {
          $("a[data-action='togglePicker']").trigger('click');
        }
      });
    });

    startDateElement.on("dp.change", function (e) {
      $('.day:not(".disabled")').on('click', function(){
        if (!instance.form.get('allDay')) {
          $("a[data-action='togglePicker']").trigger('click');
        }
      });
    });

    endDateElement.on("dp.hide", function (e) {
        // startDateElement.data("DateTimePicker").maxDate(e.date);
        instance.form.set('end', e.date.toDate());
    });

    endDateElement.on("dp.show", function (e) {
      $('.day:not(".disabled")').on('click', function(){
        $("a[data-action='togglePicker']").trigger('click');
      });
    });

    endDateElement.on("dp.change", function (e) {
      $('.day:not(".disabled")').on('click', function(){
        $("a[data-action='togglePicker']").trigger('click');
      });
    });

    if (!!instance.form.get('allDay')) {
        $('#end-date input').attr('readonly', 'readonly');
    }
};

Template.ReservationTimeTab.helpers({
    isEdl: () => {
      return Template.instance().data.resourceType === 'edl'
    },
    allDayArgs: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const instance = Template.instance();
        return {
            checked : instance.form.get('allDay'),
            label: translate.moduleReservationIndex['all_day'],
            onClick: () => {
                const status = instance.form.get('allDay');
                instance.form.set('allDay', !status);
                if (!status) {
                    instance.form.set('end', instance.form.get('start'));
                    $('#end-date input').attr('readonly', 'readonly');
                    // $('#start-date').data("DateTimePicker").maxDate(false);
                } else {
                    $('#end-date input').removeAttr('readonly');
                    const ed = $('#end-date').data("DateTimePicker").date()
                    if (!!ed) {
                      instance.form.set('end', ed.toDate());
                    }
                }
              initDatePicker(lang, instance);
            }
        };
    },
    initDatePicker: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const instance = Template.instance();
        setTimeout(() => {
            initDatePicker(lang, instance);
        }, 10);
    },
    getParams: () => {
        const instance = Template.instance();

        return {
            resourceType: instance.data.resourceType,
            resources: instance.data.resources,
            condoId: instance.data.condoId,
            allDay: instance.form.get('allDay'),
            start: instance.form.get('start'),
            end: instance.form.get('end'),
            onClick: (resource, time) => {
                instance.data.onClick(resource, time, instance.form.get('allDay'))
            }
        }
    }
});
