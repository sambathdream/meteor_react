/**
 * Created by kuyarawa on 21/02/18.
 */
import { ModuleReservationIndex } from '/common/lang/lang.js';

Template.ReservationResourceTab.onCreated(function () {
    this.resourceSearch = new ReactiveVar('');
    this.resources = new ReactiveVar([]);
});

Template.ReservationResourceTab.events({
    'input #searchRes': (e, t) => {
        e.preventDefault();
        t.resourceSearch.set(e.currentTarget.value);
    },
    'click .resource-list-item': (e, t) => {
        if (typeof t.data.onClick === 'function') {
            const element = $(e.currentTarget);
            const id = element.data('id');
            const resource = _.find(t.resources.get(), (r) => r._id === id);
            Session.set('resaSelectedDate', new Date())
            t.data.onClick(resource)
        }
    }
});

Template.ReservationResourceTab.helpers({
    setResources: () => {
        const instance = Template.instance()
        instance.resources.set(instance.data.resourceType !== 'edl' ? instance.data.resources : _.uniq(instance.data.resources, (u) => {return u.realId}));
    },
    getResources: () => {
        const t = Template.instance();
        const searchRegex = new RegExp(t.resourceSearch.get() ? t.resourceSearch.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&') : '', 'i')
        return _.filter(t.resources.get(), (resource) => {
            return resource.name.match(searchRegex) || resource.typeName.match(searchRegex)
        });
    },
    resNum: (res) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return res.number && res.total > 1 ? (
                res.type === 'coworking' ? translate.moduleReservationIndex['place'] : translate.moduleReservationIndex['number']) + ': ' + res.number
            : false;
    },
    resPersons: (res) => {
        return res.persons ? res.persons : false;
    },
    resSeats: (res) => {
        return res.seats ? res.seats : false;
    },
    getFloorString: (floor) => {
        return translateEtage(floor);
    },
    isEdl: (res) => {
        return res.type === 'edl'
    },
    isSearchActive: () => Template.instance().resourceSearch.get() !== ""
});
