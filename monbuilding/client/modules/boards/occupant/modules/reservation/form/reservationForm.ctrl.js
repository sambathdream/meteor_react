/**
 * Created by kuyarawa on 20/02/18.
 */
import { ORDER } from '../reservation.ctrl';

Template.ReservationForm.onCreated(function () {
    Session.set('showReservationCalendarForm', false);
    Session.set('showReservationNew', !!Session.get('reservationEditId'));
    this.resources = new ReactiveVar([]);
    this.form = new ReactiveDict();

    const sessionData = Session.get('reservationForm');
    if (_.isEmpty(sessionData)) {
        this.form.setDefault({
            selectedResource: this.data.formData.selectedResource || null,
            resourceType: this.data.formData.resourceType || null,
            method: !!this.data.formData.method ? this.data.formData.method : 'time',
            resourceId: this.data.formData.resourceId || null,
            startHour: this.data.formData.startHour || null,
            number: this.data.formData.number || null,
            endHour: this.data.formData.endHour || null,
            isAllDay: this.data.formData.isAllDay || false,
            week: this.data.formData.week || false,
            resources: this.data.formData.resources || [],

            // not implemented yet!
            isRecurring: undefined,
            recurringType: undefined,
            recurringEnd: undefined
        })
    } else {
        this.form.setDefault(sessionData);
    }
});

Template.ReservationForm.rendered = function () {

}

Template.ReservationForm.events({
    'click #reservation-form-wrapper .method-tab' (e, t) {
        const element = $(e.currentTarget);
        if (!element.hasClass('selectedTabBar')) {
            $('#reservation-page').find('.selectedTabBar').removeClass('selectedTabBar');
            element.addClass('selectedTabBar');
            t.form.set('method', element.data('type'));
        }
    },
});

Template.ReservationForm.helpers({
    getResourceTypes: () => {
        const condoId = Template.instance().data.condoId;
        const resources = attachResourceType(Resources.find({condoId: condoId}).fetch());
        let expandedResources = [];
        _.each(resources, (r) => {
            if (!isInt(r.number)) {
                expandedResources.push({
                    ...r,
                    realId: r._id
                })
            } else {
                for (let i = 1; i <= r.number; i++) {
                    expandedResources.push({
                        ...r,
                        number: i,
                        _id: r._id + i,
                        total: r.number,
                        realId: r._id
                    })
                }
            }
        })
        Template.instance().resources.set(expandedResources);

        return _.sortBy(_.map(_.groupBy(resources, (doc) => {
            return doc.type
        }), (grouped) => {
            return {
                value: grouped[0].type,
                label: grouped[0].typeName,
                image: '/img/reservation/'+grouped[0].type+'.png'
            }
        }), (e) => {
            return ORDER[e.value]
        })
    },
    setForm: (key, scroll) => {
        const t = Template.instance();
        return () => value => {
            t.form.set(key, value);
            // if (key === 'resourceType' && value === 'edl') {
            //     Session.set('showReservationCalendarForm', true);
            // }

            if (scroll) {
                setTimeout(() => {
                    const $id = $("#" + scroll);
                    if ($id.length !== 0) {
                        const pos = $id.offset().top - 68;
                        $('body, html').animate({scrollTop: pos});
                    }
                }, 200)
            }
        }
    },
    isTypeActive: (value) => {
        return Template.instance().form.equals('resourceType', value)
    },
    getActiveMethodTabClass: (tab) => {
        return Template.instance().form.equals('method', tab) ? 'selectedTabBar' : ''
    },
    isResourceTab: () => {
        return Template.instance().form.equals('method', 'resource')
    },
    getSelectedResource: () => {
        return Template.instance().form.get('selectedResource')
    },
    getResources: () => {
        const t = Template.instance();
        return _.filter(t.resources.get(), (r) => { return t.form.equals('resourceType', r.type) })
    },
    haveResources: () => {
        const t = Template.instance();
        const condoId = Template.instance().data.condoId;
        return Resources.find({condoId: condoId}).fetch().length > 0
    },
    getSelectedType: () => {
        return Template.instance().form.get('resourceType')
    },
    getCondoId: () => {
        return Template.instance().data.condoId;
    },
    resourceTab: () => {
        const instance = Template.instance();
        return (resource) => {
            $('body, html').animate({scrollTop: 0});
            instance.form.set('selectedResource', resource);
            Session.set('reservationForm', instance.form.get());
            Session.set('showReservationCalendarForm', true);
        }
    },

    goToNewForm: () => {
        const instance = Template.instance();
        return (resource, time, allDay, isWeek) => {
            let res = resource
            let week = false
            let resources = []

            if (isWeek) {
              week = true
              resources = _.filter(instance.resources.get(), (r) => {
                return r.type === instance.form.get('resourceType')
              })
            }
            if (resource !== null) {
              if (!resource.realId || isWeek) {
                res = _.find(instance.resources.get(), (r) => {
                  return r._id === resource.id || r._id === resource._id
                })
              }
            }

            instance.form.set('resources', resources);
            instance.form.set('week', week);
            instance.form.set('selectedResource', res);
            instance.form.set('resourceId', res ? (isInt(res.number) && isWeek ? res._id: res.realId) : null );
            instance.form.set('startHour', time.start);
            instance.form.set('endHour', time.end);
            instance.form.set('isAllDay', allDay);
            instance.form.set('number', res && isInt(res.number) ? res.number : null);

            Session.set('showReservationCalendarForm', false);
            Session.set('showReservationNew', true);
            $('body, html').animate({scrollTop: 0});
        }
    },
    showCalendar: () => {
        return Session.get('showReservationCalendarForm');
    },
    showNew: () => {
        return Session.get('showReservationNew');
    },
    initState: () => {
        return !Session.get('showReservationNew') && !Session.get('showReservationCalendarForm');
    },
    isEdit: () => {
        return !!Session.get('reservationEditId') || !!Session.get('reservationFromIndexCalendar')
    },
    getFormArgs: () => {
        const instance = Template.instance();
        const editId = Session.get('reservationEditId');
        let data;

        if (!editId) {
            data = {
                condoId: instance.data.condoId,
                selectedResource: instance.form.get('selectedResource'),
                resourceType: instance.form.get('resourceType'),
                method: instance.form.get('method'),
                resourceId: instance.form.get('resourceId'),
                startHour: instance.form.get('startHour'),
                endHour: instance.form.get('endHour'),
                isAllDay: instance.form.get('isAllDay'),
                week: instance.form.get('week'),
                resources: instance.form.get('resources'),
                isRecurring: instance.form.get('isRecurring'),
                recurringType: instance.form.get('recurringType'),
                number: instance.form.get('number'),
                recurringEnd: instance.form.get('recurringEnd')
            }
        } else {
            const book = Reservations.findOne(editId);
            if (!!book) {
              let selectedResource = attachResourceType([Resources.findOne(book.resourceId)])[0]
              selectedResource.realId = selectedResource._id;
              if (isInt(book.number)) {
                selectedResource.number = book.number
                selectedResource._id = selectedResource._id + book.number
              }

              data = {
                eventId: book.eventId,
                bookId: editId,
                condoId: instance.data.condoId,
                selectedResource: selectedResource,
                resourceType: selectedResource.type,
                method: 'edit',
                resourceId: selectedResource.realId,
                startHour: moment(book.start).toDate(),
                endHour: moment(book.end).toDate(),
                isAllDay: book.isAllDay,
                week: false,
                resources: [],
                number: selectedResource.number || null,
                isRecurring: book.isRecurring,
                recurringType: book.recurringType,
                recurringEnd: book.recurringEnd
              }
            }
        }

        if (!!data) {
            Session.set('reservationForm', data);
        }

        return data
    },
    getEventDates: () => {
        return Template.instance().data.eventDates
    }
});
