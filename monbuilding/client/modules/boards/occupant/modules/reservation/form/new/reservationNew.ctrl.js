/**
 * Created by kuyarawa on 24/02/18.
 */
import { ModuleReservationIndex } from '/common/lang/lang.js';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import safeGet from 'safe-get'
// import moment from 'moment'

const initDatePicker = (lang, instance) => {
  const startDateElement = $('#start-date');
  const endDateElement = $('#end-date');
  const d = new Date()
  // const offset = d.getTimezoneOffset();

  const defaultOptions = {
    format: 'dddd, D MMM YYYY, HH:mm',
    locale: lang,
    allowInputToggle: true,
    stepping: 15,
    icons: {
      date: 'custom-icon calendar-icon',
      up: 'custom-icon chevron-up',
      down: 'custom-icon chevron-down'
    }
  }

  startDateElement.datetimepicker({
    ...defaultOptions,
    defaultDate: instance.form.get('startHour'),
    // maxDate: instance.form.get('endHour')
  });

  endDateElement.datetimepicker({
    ...defaultOptions,
    defaultDate: instance.form.get('endHour'),
    minDate: instance.form.get('startHour'),
    useCurrent: false //Important! See issue #1075
  });
  startDateElement.on("dp.hide", function (e) {
    if (endDateElement.data("DateTimePicker")) {
      endDateElement.data("DateTimePicker").minDate(e.date);
    }
    instance.form.set('startHour', e.date.toDate());
    if (e.date.isAfter(moment(instance.form.get('endHour')))) {
      const newEnd = e.date.add(15, 'm')
      instance.form.set('endHour', newEnd.toDate())
      endDateElement.data("DateTimePicker").date(newEnd)
    }
  });

  startDateElement.on("dp.change", function (e) {
    $('.day:not(".disabled")').on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });

  endDateElement.on("dp.show", function (e) {
    $('.day:not(".disabled")').on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });

  endDateElement.on("dp.hide", function (e) {
    // startDateElement.data("DateTimePicker").maxDate(e.date);
    instance.form.set('endHour', e.date.toDate());
  });

  endDateElement.on("dp.show", function (e) {
    $('.day:not(".disabled")').on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  })
  endDateElement.on("dp.change", function (e) {
    $('.day:not(".disabled")').on('click', function(){
      $("a[data-action='togglePicker']").trigger('click');
    });
  });
};

Template.ReservationNew.onCreated(function () {
  Session.set('reservationEditId', null);
  this.fromCalendar = Session.get('reservationFromIndexCalendar');
  Session.set('reservationFromIndexCalendar', false);

  const currentUser = UsersProfile.findOne(Meteor.user()._id);
  const defaultUser = {
    id: currentUser._id,
    text: `${currentUser.firstname} ${currentUser.lastname}`
  }

  this.form = new ReactiveDict();
  this.valid = new ReactiveVar(false);
  this.errorMessage = new ReactiveVar('');
  this.extraValid = new ReactiveVar(true);
  this.condoUsers = new ReactiveVar([defaultUser]);
  this.userListReady = new ReactiveVar(false);

  this.canInvite = new ReactiveVar(false);
  this.canReserveOther = new ReactiveVar(false);

  let bookData = {
    services: {},
    participantNumber: null,
    participants: [],
    externalParticipants: [],
    title: null,
    message: null,
    edlDirection: null,
    paid: !!this.data.selectedResource && this.data.selectedResource.paid,
    price: !!this.data.selectedResource && this.data.selectedResource.price,
    vat: !!this.data.selectedResource && this.data.selectedResource.vat,
    paidDuration: !!this.data.selectedResource && this.data.selectedResource.paidDuration,
    paidAmount: 0,
    qty: 0
  };

  let endHour = this.data.endHour
  if (this.data.resourceType === 'edl') {
    const duration = safeGet(this.data, 'selectedResource.edlDuration') || 45
    endHour = moment(this.data.startHour).add(duration, 'm').toDate()
  }

  const defaultData = {
    eventId: this.data.eventId,
    bookId: this.data.bookId,
    selectedResource: this.data.selectedResource,
    resourceType: this.data.resourceType,
    method: this.data.method,
    condoId: this.data.condoId,
    resourceId: this.data.resourceId,
    startHour: this.data.startHour,
    endHour: endHour,
    isAllDay: this.data.isAllDay,
    origin: Meteor.user()._id,
    bookedBy: Meteor.user()._id,
    number: this.data.number,
    isManager: Meteor.user() && Meteor.user().identities && Meteor.user().identities.gestionnaireId,
    sendEmail: false,

    // not implemented yet!
    isRecurring: this.data.isRecurring,
    recurringType: this.data.recurringType,
    recurringEnd: this.data.recurringEnd
  }

  if (!!this.data.bookId) {
    const book = Reservations.findOne(this.data.bookId);
    if (!!book) {
      bookData = {
        services: book.services,
        participantNumber: book.participantNumber,
        participants: book.participants,
        title: book.title,
        message: book.message,
        externalParticipants: book.externalParticipants,
        paid: book.paid,
        price: book.price,
        vat: book.vat,
        paidDuration: book.paidDuration,
        paidAmount: book.paidAmount,
        qty: book.qty
      }

      this.form.setDefault({
        ...bookData,
        ...defaultData
      });
    } else {
      this.form.setDefault({
        ...bookData,
        ...defaultData
      });
    }
  } else {
    this.form.setDefault({
      ...bookData,
      ...defaultData
    });
  }

  this.emptyExternalParticipant = {
    email: '',
    firstName: '',
    lastName: ''
  };

  this.updateValue = (t, key, value) => {
    if (key === 'resourceId') {
      let resource = _.find(t.data.resources, (r) => { return r._id === value });
      if (!!resource) {
        t.form.set('selectedResource', resource);
        t.form.set('resourceType', resource.type);
        t.form.set('resourceId', resource.realId);
      }
    } else {
      t.form.set(key, value);
    }

    t.validateFields(t)
  };

  this.validateFields = (t) => {
    let requireFields = ['condoId', 'resourceId'];
    if (t.form.equals('resourceType', 'meeting')) {
      requireFields.push('title');
    }
    if (t.form.equals('resourceType', 'edl')) {
      requireFields.push('edlDirection');
    }

    const error = _.filter(requireFields, field => !t.form.get(field) || t.form.get(field) === '')

    if (t.form.get('participantNumber') && t.form.get('participantNumber') !== '' && !isInt(t.form.get('participantNumber'))) {
      error.push('participantNumber')
    }

    t.valid.set(error.length === 0 && t.extraValid.get());
  };

  this.validateEp = (t) => {
    const instance = t;
    const externalParticipants = instance.form.get('externalParticipants');
    instance.extraValid.set(true);
    const keys = ['email', 'firstName', 'lastName']

    if (!!externalParticipants && _.isArray(externalParticipants) && externalParticipants.length > 0) {
      externalParticipants.forEach((ep) => {
        if (!!ep.email || !!ep.firstName || !!ep.lastName) {
          keys.forEach((key) => {
            if (key === 'email' ? !isValidEmail(ep.email): !ep[key]) {
              instance.extraValid.set(false);
            }
          })
        }
      })
    }
  };

  this.getChild = (service) => {
    let child = [];
    const serviceData = ResourceServices.findOne({key: service.id});
    if (serviceData) {
      const data = serviceData.children.filter((c) => {
        return service.data.includes(c.key)
      }).map((d) => {
        return {
          type: 'data',
          value: getTranslatedValue(d),
          key: d.key
        }
      });

      const custom = service.custom.map((c) => {
        return {
          type: 'custom',
          value: c,
          key: c
        }
      });

      child = [...data, ...custom]
    }

    return child;
  }

  this.setUser = (ids) => {
    const users = _.uniq([...this.condoUsers.get(), ..._.map(UsersProfile.find({_id: {$in: ids}}).fetch(), (u) => {
      return {
        id: u._id,
        text: `${u.firstname} ${u.lastname}`
      }
    })], (u) => {return u.id});

    this.condoUsers.set(users);
    this.userListReady.set(true);
  }

  this.autorun(() => {
    this.canInvite.set(Meteor.userHasRight('reservation', 'invite', this.form.get('condoId')) || Meteor.userHasRight('reservation', 'invit', this.form.get('condoId')))
    this.canReserveOther.set(Meteor.userHasRight('reservation', 'reserveOther', this.form.get('condoId')))
  });
});

Template.module_reservation_index.rendered = function () {

};

Template.ReservationNew.events({
  'click .confirm-resa-form-btn': (e, t) => {
    t.valid.set(false);

    let params = {};
    let endpoint = '';
    const common = {
      resourceId: t.form.get('resourceId'),
      condoId: t.form.get('condoId'),
      origin: t.form.get('origin'),
      bookedBy: t.form.get('bookedBy'),
      startDate: moment(t.form.get('startHour')).format('DD/MM/YY'),
      startTime: moment(t.form.get('startHour')).format('HH:mm'),
      title: t.form.get('title'),
      number: t.form.get('number'),
      message: t.form.get('message'),
      paid: t.form.get('paid'),
      price: t.form.get('price'),
      vat: t.form.get('vat'),
      paidDuration: t.form.get('paidDuration'),
      paidAmount: t.form.get('paidAmount'),
      qty: t.form.get('qty')
    };
    if (t.form.equals('resourceType', 'edl')) {
      params = {
        ...common,
        edlDirection: t.form.get('edlDirection')
      };
      endpoint = 'addEDLToResource';
    } else {
      endpoint = 'addEventToResource';
      params = {
        ...common,
        endDate: moment(t.form.get('endHour')).format('DD/MM/YY'),
        endTime: moment(t.form.get('endHour')).format('HH:mm'),
        emailEvent: true,
        participants: t.form.get('participants'),
        externalParticipants: _.compact(t.form.get('externalParticipants').map((p) => {
          return !!p && !!p.email ? p : undefined
        })),
        participantNumber: t.form.get('participantNumber'),
        isAllDay: t.form.get('isAllDay'),
        isRecurring: t.form.get('isRecurring'),
        recurringType: t.form.get('recurringType'),
        recurringEnd: t.form.get('recurringEnd'),
        services: t.form.get('services')
      }
    }

    if (!!t.form.get('eventId')) {
      params.eventId = t.form.get('eventId');
      Meteor.call('editEvent', params, (err, res) => {
        t.valid.set(true);
        if (err) {
          t.errorMessage.set(err.reason);
          $("#error-modal").modal("show");
        } else {
          if (!!t.form.get('paid')) {
            $("#reservation-detail").modal("show");
          } else {
            Session.set('showReservationForm', false);
            Session.set('showReservationCalendar', false);
            Session.set('showReservationNew', false);
            Session.set('reservationSuccessId', t.form.get('bookId'));
          }
        }
      });
    } else {
      const lang = (FlowRouter.getParam("lang") || "fr")
      Meteor.call(endpoint, params, lang, (err, res) => {
        t.valid.set(true);
        if (err) {
          t.errorMessage.set(err.reason);
          $("#error-modal").modal("show");
        } else {
          if (!!t.form.get('paid')) {
            $("#reservation-detail").modal("show");
            t.form.set('bookId', res.id)
            t.form.set('eventId', res.eventId)
          } else {
            Session.set('showReservationForm', false);
            Session.set('showReservationCalendar', false);
            Session.set('showReservationNew', false);
            Session.set('reservationSuccessId', res.id);
          }
        }
      });
    }
  },
  'click #close-form-2': (e, t) => {
    Session.set('showReservationNew', false);
    const sessionData = Session.get('reservationForm');
    Session.set('reservationForm', {});

    if (!!t.form.get('eventId')) {
      Session.set('showReservationForm', false);
      Session.set('showReservationCalendar', false);
      Session.set('reservationSuccessId', t.form.get('bookId'));
    } else {
      if (!!sessionData && !!sessionData.method && sessionData.method === 'resource') {
        setTimeout(() => {
          Session.set('showReservationForm', true);
          setTimeout(() => {
            Session.set('showReservationCalendarForm', true);
          }, 100);
        }, 100);
      } else {
        if (!t.form.get('eventId')) {
          setTimeout(() => {
            Session.set('showReservationForm', true);
          }, 200);
        }

        if (t.fromCalendar) {
          setTimeout(() => {
            Session.set('showReservationForm', false);
            Session.set('showReservationCalendar', true);
          }, 200);
        }
      }
    }
  },
  'click #add-external': (e, t) => {
    t.form.set('externalParticipants', [
      ...t.form.get('externalParticipants'),
      t.emptyExternalParticipant
    ]);
  },
  'click .remove-external': (e, t) => {
    const index = parseInt(e.currentTarget.getAttribute("data-index"));
    let prevData = t.form.get('externalParticipants');
    prevData.splice(index, 1);
    t.form.set('externalParticipants', prevData);
  },
  'click button.service-parent': (e, t) => {
    $(e.currentTarget).parent().toggleClass('open');
  },
  'click': (e, t) => {
    const element = $('div.dropdown.service-dropdown');
    if (!element.is(e.target)
      && element.has(e.target).length === 0
      && $('.open').has(e.target).length === 0
    ) {
      element.removeClass('open');
    }
  },
  'click .service-child': (e, t) => {
    const element = $(e.currentTarget);
    const parent = element.data('parent');
    const key = element.data('key');
    const type = element.data('type');

    const services = t.form.get('services');
    const child = _.find(services, (s, i) => i === parent) || {'data': [], 'custom': []};
    const childExist = child[type] && child[type].indexOf(key) !== -1;

    t.form.set('services', {
      ...services,
      [parent]: {
        ...child,
        [type]: childExist
          ? _.filter(child[type], d => d !== key)
          : child[type].concat(key)
      }
    })
  }
});

Template.ReservationNew.helpers({
  getResource: () => {
    const instance = Template.instance();
    return {
      resource: instance.form.get('selectedResource')
    }
  },
  getFloor: (floor) => {
    return translateEtage(floor);
  },
  resNum: (res) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    return res.number && res.number > 1 ? (
        res.type === 'coworking' ? translate.moduleReservationIndex['place'] : translate.moduleReservationIndex['number']) + ': ' + res.number
      : false;
  },
  isManager: () => {
    return !!(Meteor.user() && Meteor.user().identities && Meteor.user().identities.gestionnaireId);
  },
  isFieldError: (key) => {
    const instance = Template.instance();
    switch (key) {
      case 'participantNumber': {
        const value = instance.form.get('participantNumber')
        return value !== null && isNaN(value)
      }
      default:
        return false
    }
  },
  getErrorMessage: () => {
    return Template.instance().errorMessage.get()
  },
  isTimePath: () => {
    return Template.instance().form.equals('method', 'time');
  },
  getFormData: (key) => {
    return Template.instance().form.get(key) || ''
  },
  setFormData (key) {
    const instance = Template.instance()

    return () => value => {
      instance.updateValue(instance, key, value)
    }
  },
  getExternalParticipants: () => {
    const instance = Template.instance();
    const externalParticipants = instance.form.get('externalParticipants');

    if (!(_.isArray(externalParticipants) && externalParticipants.length > 0)) {
      instance.form.set('externalParticipants', [this.emptyExternalParticipant])
    }

    return instance.form.get('externalParticipants');
  },
  isEpError: (type, index) => {
    const instance = Template.instance();
    const externalParticipants = instance.form.get('externalParticipants');

    if (!!externalParticipants[index]) {
      if (!!externalParticipants[index].email || !!externalParticipants[index].firstName || !!externalParticipants[index].lastName) {
        return type === 'email' ? !isValidEmail(externalParticipants[index].email) : !externalParticipants[index][type];
      } else {
        return false
      }
    } else {
      return false
    }
  },
  getEpData: (field, index) => {
    const instance = Template.instance();
    const externalParticipants = instance.form.get('externalParticipants');

    return !!externalParticipants[index] ? externalParticipants[index][field]: '';
  },
  setEpData: (field, index) => {
    const instance = Template.instance()

    return () => value => {
      const prevData = instance.form.get('externalParticipants');
      instance.form.set('externalParticipants', prevData.map((d, i) => {
        return i !== index ? d : {
          ...d,
          [field]: value
        }
      }));
      instance.validateEp(instance)
      instance.validateFields(instance)
    }
  },
  canRemoveExternal: () => {
    return Template.instance().form.get('externalParticipants').length > 1
  },
  setUsersList: () => {
    const instance = Template.instance();
    Meteor.call('getResidentsOfCondoWithRight', instance.form.get('condoId'), 'reservation', 'see', (err, res) => {
      if (!err) {
        if (instance.form.get('isManager')) {
          Meteor.call('getGestionnaireOfCondoInChargeWithRight', instance.form.get('condoId'), 'reservation', 'see', (err2, res2) => {
            if (!err2) {
              instance.setUser([...res, ...res2])
            }
          })
        } else {
          instance.setUser(res)
        }
      }
    });
  },
  getUsersList: () => {
    return Template.instance().condoUsers.get()
  },
  getFormattedDate: (date) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    return moment(date).locale(lang).format('dddd, D MMM YYYY, HH:mm');
  },
  getEmailArgs: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);
    const instance = Template.instance();
    return {
      checked : instance.form.get('sendEmail'),
      label: translate.moduleReservationIndex['receive_copyof_email'],
      onClick: () => {
        const status = instance.form.get('sendEmail');
        instance.form.set('sendEmail', !status);
      }
    };
  },
  MbTextArea: () => MbTextArea,
  isNeedValidation: (resource) => {
    return resource && resource.needValidation && resource.needValidation.status;
  },
  isNeedValidationExcludeMe: (resource) => {
    return resource && resource.needValidation && resource.needValidation.status && !resource.needValidation.managerIds.includes(Meteor.user()._id);
  },
  getServices: () => {
    const instance = Template.instance();
    const service = instance.form.get('selectedResource') && instance.form.get('selectedResource').availableServices;
    return _.map(service, (s, i) => {
      return {
        ...s,
        id: i
      }
    })
  },
  haveService: () => {
    const instance = Template.instance();
    const service = instance.form.get('selectedResource') && instance.form.get('selectedResource').availableServices;
    let status = false
    if (!!service) {
      _.each(service, (s, i) => {
        const exist = instance.getChild({
          ...s,
          id: i
        })

        if (exist.length > 0) {
          status = true
        }
      })
    }
    return status
  },
  userListReady: () => {
    return Template.instance().userListReady.get()
  },
  getServiceName: (key) => {
    const service = ResourceServices.findOne({key: key});
    return !!service ? getTranslatedValue(service) : '';
  },
  isServiceActive: (key) => {
    const service = Template.instance().form.get('services');
    return !!service[key] && (service[key].data.length > 0 || service[key].custom.length > 0) ? 'service-active' : '';
  },
  getChildActiveState: (parentKey, childKey, type) => {
    const services = Template.instance().form.get('services');
    return services[parentKey] && services[parentKey][type] && services[parentKey][type].indexOf(childKey) !== -1
      ? 'checked-icon'
      : 'unchecked-icon';
  },
  childExist: (service) => {
    return Template.instance().getChild(service).length > 0
  },
  mergeChild: (service) => {
    return Template.instance().getChild(service)
  },
  formNotValid: () => {
    return !Template.instance().valid.get();
  },
  isWeek: () => {
    return Template.instance().data.week
  },
  getResourcesOption: () => {
    return _.map(Template.instance().data.resources, (r) => {
      return {
        id: r._id,
        text: `${r.name} ${isInt(r.number) ? ' - ' + r.number : ''}`
      }
    })
  },
  isMeeting: () => {
    const instance = Template.instance();
    return instance.form.equals('resourceType', 'meeting')
  },
  showNonEdlAndEquipment: () => {
    const instance = Template.instance();
    return !instance.form.equals('resourceType', 'edl') && !instance.form.equals('resourceType', 'equipment')
  },
  showNonEdl: () => {
    const instance = Template.instance();
    return !instance.form.equals('resourceType', 'edl')
  },
  showBookAs: () => {
    const isManager = (!!(Meteor.user() && Meteor.user().identities && Meteor.user().identities.gestionnaireId)) && (!Template.instance().form.get('eventId'))
    const canReserveOther = Meteor.userHasRight('reservation', 'reserveOther');
    return isManager || canReserveOther;
  },
  firstValidation: () => {
    const instance = Template.instance();
    instance.validateFields(instance);
  },
  canInvite: () => {
    return !!Template.instance().canInvite.get()
  },
  canReserveOther: () => {
    return !!Template.instance().canReserveOther.get()
  },
  initDatePicker: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const instance = Template.instance();
    setTimeout(() => {
      initDatePicker(lang, instance);
    }, 10);
  },
  edlDirectionOptions: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleReservationIndex(lang);

    return [
      {id: 'in', text: translate.moduleReservationIndex['in']},
      {id: 'out', text: translate.moduleReservationIndex['out']}
    ]
  },
  isEdit: () => {
    return !!Template.instance().form.get('eventId')
  },
  getCalculatePrice: (resource) => {
    let result = {
      value: 0,
      multiplier: 0
    };

    if (!!resource && !!resource.paid && !!resource.paidDuration && !!resource.price) {
      const instance = Template.instance()
      const start = moment(instance.form.get('startHour'));
      const end = moment(instance.form.get('endHour'))
      const duration = end.diff(start, 'minute')
      const paidPerMinute = parseFloat(resource.price) / parseInt(resource.paidDuration)
      const paidAmount = duration * paidPerMinute;
      const qty = (duration / parseInt(resource.paidDuration)).toFixed(2)
      instance.form.set('paidAmount', paidAmount)
      instance.form.set('qty', qty)

      return {
        value: paidAmount,
        multiplier: qty
      }
    }

    return result
  },
  getButtonClass: (disabled) => {
    return disabled ? 'disabled sm confirm-resa-form-btn': 'sm confirm-resa-form-btn'
  },
  confirmPayment: () => {
    return () => () => {
      // $("#reservation-detail").modal("show");
      console.log('confirm')
    }
  },
  onPaymentFinish: () => {
    const instance = Template.instance()
    return () => {
      setTimeout(() => {
        console.log('finish')
        Session.set('showReservationForm', false);
        Session.set('showReservationCalendar', false);
        Session.set('showReservationNew', false);
        Session.set('reservationSuccessId', instance.form.get('bookId'));
      }, 300);
    }
  },
  getTotalPrice: () => {
    const t = Template.instance()

    return parseInt(t.form.get('paidAmount') * 100)
  },
  getSummaryCutomData: () => {
    const t = Template.instance()

    const form = t.form.all()
    return {
      title: form.title,
      description: '',
      quantity: '1',
      price: form.paidAmount,
      vat: form.vat,
      pay_outside_platform: false,
      validatedAt: Date.now()
    }
  },
  getBookData: () => {
    const t = Template.instance()
    const form = t.form.all();

    return {
      ...form,
      start: +moment(t.form.get('startHour')).format('x'),
      end: +moment(t.form.get('endHour')).format('x'),
      emailEvent: true,
      externalParticipants: _.compact(t.form.get('externalParticipants').map((p) => {
        return !!p && !!p.email ? p : undefined
      }))
    };
  },
  getReservationId: () => Template.instance().form.get('bookId'),
  onConfirm: () => {
    return () => {
      $("#reservation-detail").modal("hide");
      setTimeout(() => {
        $('#modal_market_basket_reservation').modal('show');
      }, 100)
    }
  }
});
