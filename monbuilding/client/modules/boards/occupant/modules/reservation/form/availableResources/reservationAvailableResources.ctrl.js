/**
 * Created by kuyarawa on 23/02/18.
 */
import { ModuleReservationIndex } from '/common/lang/lang.js';

Template.ReservationAvailableResources.onCreated(function () {
    this.availableResources = new ReactiveVar([]);
    this.suggestion = new ReactiveVar([]);
    this.busy = new ReactiveVar(false);
    this.selected = new ReactiveVar(null);
    this.allDayTime = ReactiveVar({});

    this.getSuggestion = (params, instance) => {
        Meteor.call('getSuggestResources', params, (err, result) => {
            if (err) {
                console.log('Error!', err);
                instance.busy.set(false);
            } else {
                const suggestion = _.map(result, (r) => {
                  return {
                    ...r,
                    realId: r._id,
                    _id: r.key
                  }
                })
                instance.availableResources.set([]);
                instance.suggestion.set(instance.data.resourceType === 'edl' ? _.uniq(suggestion, (u) => {
                  return [u.realId, u.start].join()
                }) : suggestion);
                instance.busy.set(false);
            }
        });
    };
});

Template.ReservationAvailableResources.events({
    'click .resource-list-item.available': (e, t) => {
        if (typeof t.data.onClick === 'function') {
            const element = $(e.currentTarget);
            const id = element.data('id');
            const resource = _.find(t.availableResources.get(), (r) => r._id === id);
            let time = {
                start: t.data.start,
                end: t.data.end
            };

            if (t.data.allDay) {
              const dow = moment(t.data.start).day();
              const horaire = _.find(resource.horaires, (h) => { return h.dow[0] === dow });
              time = {
                start: moment(t.data.start).hour(parseInt(horaire.start.split(':')[0], 10)).minute(parseInt(horaire.start.split(':')[1], 10)).toDate(),
                end: moment(t.data.start).hour(parseInt(horaire.end.split(':')[0], 10)).minute(parseInt(horaire.end.split(':')[1], 10)).toDate()
              }
            }

            t.data.onClick(resource, time)
            if (!!t.data.checkBox) {
              t.selected.set(resource._id)
            }
        }
    },
    'click .resource-list-item.suggest': (e, t) => {
        if (typeof t.data.onClick === 'function') {
            const element = $(e.currentTarget);
            const id = element.data('id');
            const start = element.data('start');
            const end = element.data('end');
            const resource = _.find(t.suggestion.get(), (r) => r._id === id);
            const time = {
                start: moment(start).utc().toDate(),
                end: moment(end).utc().toDate()
            };

            t.data.onClick(resource, time)
        }
    },
});

Template.ReservationAvailableResources.helpers({
    callPromise: () => {
        const instance = Template.instance();
        const data = instance.data.resources;

        if ((instance.data.start && instance.data.end) || (instance.data.start && instance.data.resourceType === 'edl')) {
            let startTime = false;
            let endTime = false;
            const isEdl = instance.data.resourceType === 'edl'
            let duration = 0

            if (isEdl) {
              const edl = Resources.findOne({condoId: instance.data.condoId, type: 'edl'})
              if (!!edl) {
                duration = edl.edlDuration || 45
              }
            }

            if (instance.data.allDay) {
                // let minHour = 0;
                // let maxHour = 24;
                // const dow = moment(instance.data.start).day();
                //
                // _.each(data, (x) => {
                //     const horaire = _.find(x.horaires, (h) => { return h.dow[0] === dow });
                //     const startHour = parseInt(horaire.start.split(':')[0], 10);
                //     const endHour = parseInt(horaire.end.split(':')[0], 10);
                //
                //     if ((minHour < startHour) && (startHour > 0)) {
                //         minHour = startHour;
                //         startTime = horaire.start;
                //     }
                //     if ((maxHour > endHour) && (endHour > 0)) {
                //         maxHour = endHour;
                //         endTime = horaire.end;
                //     }
                // })
                //
                // instance.allDayTime.set({
                //     min: startTime,
                //     max: endTime
                // })

                startTime = '08:00'
                endTime = '20:00'
            }

            const params = {
                condoId: instance.data.condoId,
                type: instance.data.resourceType,
                startDate: moment(instance.data.start).format('DD-MM-YYYY'),
                startTime: startTime || moment(instance.data.start).format('HH:mm'),
                endDate: (isEdl ? moment(instance.data.start).add(duration, 'minutes').format('DD-MM-YYYY') : moment(instance.data.end).format('DD-MM-YYYY')),
                endTime: endTime || (isEdl ? moment(instance.data.start).add(duration, 'minutes').format('HH:mm') : moment(instance.data.end).format('HH:mm')),
                eventId: false,
                isAllDay: instance.data.allDay
            };

            instance.busy.set(true);
            Meteor.call('getFreeResources', params, (err, result) => {
                if (err && !instance.data.skipSuggestion) {
                    instance.getSuggestion(params, instance)
                } else {
                    if (result.length > 0) {
                        const availableResources = _.filter(data, c => isInt(c.number) ? result.find((r) => r.id === c.realId && r.number.indexOf(c.number) !== -1 ) : result.indexOf(c.realId) !== -1);
                        instance.availableResources.set(instance.data.resourceType === 'edl' ? _.uniq(availableResources, (u) => {return u.realId}) : availableResources);
                        instance.suggestion.set([]);
                        instance.busy.set(false);
                    } else {
                      if (!instance.data.skipSuggestion) {
                        instance.getSuggestion(params, instance)
                      }
                    }

                  if (instance.data.skipSuggestion && typeof instance.data.onEmpty === 'function' && instance.availableResources.get().length === 0) {
                    instance.busy.set(false);
                    instance.data.onEmpty();
                  }
                }
            });
        }
    },
    resourcesAvailable: () => {
        const instance = Template.instance();
        return instance.availableResources.get().length > 0 && ((instance.data.start && instance.data.end) || (instance.data.start && instance.data.resourceType === 'edl'))
    },
    suggestionAvailable: () => {
        const instance = Template.instance();
        return instance.suggestion.get().length > 0 && ((instance.data.start && instance.data.end) || (instance.data.start && instance.data.resourceType === 'edl'))
    },
    isBusy: () => {
        return Template.instance().busy.get()
    },
    getAvailableResources: () => {
        return Template.instance().availableResources.get()
    },
    getAvailableSuggestion: () => {
        return Template.instance().suggestion.get()
    },
    resNum: (res) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return res.number && res.total > 1 && res.type !== 'edl' ? (
                res.type === 'coworking' ? translate.moduleReservationIndex['place'] : translate.moduleReservationIndex['number']) + ': ' + res.number
            : false;
    },
    resPersons: (res) => {
        return res.persons ? res.persons : false;
    },
    resSeats: (res) => {
        return res.seats ? res.seats : false;
    },
    getFloorString: (floor) => {
        return translateEtage(floor);
    },
    getTime: (item) => {
        const start = moment(item.start).utc();
        const end = moment(item.end).utc();
        return `${start.format('dddd, D MMM YYYY, HH:mm')}-${start.format('DDMMYY') === end.format('DDMMYY') ? end.format('HH:mm'): end.format('dddd, D MMM, HH:mm')}`
    },
    checkArgs: (id) => {
      const instance = Template.instance();
      return {
        checked : instance.selected.get() === id,
        label: '',
        onClick: () => {
          const resource = _.find(instance.availableResources.get(), (r) => r._id === id);
          let time = {
            start: instance.data.start,
            end: instance.data.end
          };
          instance.selected.set(id)
          instance.data.onClick(resource, time)
        }
      }
    }
});
