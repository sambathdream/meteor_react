import { ModuleReservationIndex, ModuleReservationError, ModuleActualityIndex } from '/common/lang/lang.js';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';

Template.ReservationValidate.onCreated(function () {
    this.bookId = Session.get('reservationValidateId');
    this.actionSelected = new ReactiveVar('');
    this.rejectAction = new ReactiveVar(false);
    this.showSuggest = new ReactiveVar(false);
    this.taken = new ReactiveVar(false);
    this.isMoveBook = new ReactiveVar(false);
    this.rejectMessage = new ReactiveVar('');
    this.selectedResource = new ReactiveVar(false);
    this.emptySuggestion = new ReactiveVar(false);

    this.rejectBook = (instance) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const data = Reservations.findOne(instance.bookId);
        const reject = instance.actionSelected.get() === 'reject';
        let params = {
            eventId: data.eventId,
            condoId: data.condoId,
            rejected: reject,
            ...(isInt(data.number) ? {number: data.number}: {}),
            resourceId: data.resourceId,
            startDate: moment(data.start).format('DD/MM/YY'),
            startTime: moment(data.start).format('HH:mm'),
            endDate: moment(data.end).format('DD/MM/YY'),
            endTime: moment(data.end).format('HH:mm')
        };

        if (!!instance.rejectAction.get()) {
            params.rejectionMessage = instance.rejectMessage.get()
        }

        Meteor.call('rejectBooking', params, (err, res) => {
            if (err) {
                if (err.error === 412) {
                    instance.actionSelected.set('reject');
                    instance.rejectAction.set(true);
                    instance.taken.set(true);
                } else {
                    sAlert.error(err.reason);
                }
            }
            else if (res) {
                if (reject) {
                    if (!!data.pending) {
                      sAlert.success(translate.moduleReservationIndex["reject_reservation"]);
                    } else {
                      sAlert.success(translate.moduleReservationIndex["canceled_reservation"]);
                    }
                } else {
                    sAlert.success(translate.moduleReservationIndex["validate_reservation"]);
                }
                $("#confirmation-modal").modal("hide");
                setTimeout(() => {
                    Session.set('reservationValidateId', false);
                }, 500);
            }
        });
    }

    this.moveBook = (instance) => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ModuleReservationIndex(lang);
      const resource = instance.selectedResource.get()
      const book = Reservations.findOne(instance.bookId);
      let params = {
        ...book,
        resourceId: resource.realId,
        ...(isInt(resource.number) ? {number: resource.number}: {}),
        startDate: moment(book.start).format('DD/MM/YY'),
        startTime: moment(book.start).format('HH:mm'),
        endDate: moment(book.end).format('DD/MM/YY'),
        endTime: moment(book.end).format('HH:mm'),
        rejected: false,
        fromManagerSuggestion: true
      }

      if (!!instance.rejectAction.get()) {
        params.rejectionMessage = instance.rejectMessage.get()
      }

      Meteor.call('rejectBooking', params, (err, res) => {
        if (err) {
          if (err.error === 412) {
            instance.actionSelected.set('reject');
            instance.rejectAction.set(true);
            instance.taken.set(true);
          } else {
            sAlert.error(err.reason);
          }
        } else if (res) {
          sAlert.success(translate.moduleReservationIndex["validate_reservation"]);
          $("#confirmation-modal").modal("hide");
          setTimeout(() => {
            Session.set('reservationValidateId', false);
          }, 500);
        }
      });
    }
});

Template.ReservationValidate.events({
    'click #close-validate': (e, t) => {
        Session.set('reservationValidateId', false);
    },
    'click #reservation-not-validate-btn': (e, t) => {
        t.actionSelected.set('reject');
        t.rejectAction.set(true);
        t.rejectMessage.set('');
        t.showSuggest.set(false);
        t.isMoveBook.set(false);
        t.emptySuggestion.set(false);
        $("#confirmation-modal").modal("show");
    },
    'click #reservation-validate-btn': (e, t) => {
        t.actionSelected.set('approve');
        t.rejectAction.set(false);
        t.showSuggest.set(false);
        t.isMoveBook.set(false);
        t.emptySuggestion.set(false);
        $("#confirmation-modal").modal("show");
    },
});

Template.ReservationValidate.helpers({
    getData: () => {
        let resa = {};
        const data = Reservations.findOne(Template.instance().bookId);
        if (data) {
            const condo = Condos.findOne(data.condoId);
            const resource = Resources.findOne(data.resourceId);

            resa = {
                book: data,
                condo,
                resource
            }
        }

        return resa;
    },
    timeString: (reservation) => {
        let timeString = '';

        if (reservation) {
            const start = moment(reservation.start)
            const end = moment(reservation.end)
            if (start.format('YYMMDD') === end.format('YYMMDD')) {
                timeString = `${start.format('dddd D MMM, YYYY, HH:mm')}-${end.format('HH:mm')}`
            } else {
                timeString = `${start.format('D MMM YYYY, HH:mm')}-${end.format('D MMM YYYY, HH:mm')}`
            }

            let recuring = '';
            if (reservation.isRecurring) {
                recuring = ` (${reservation.recurringType})`
            }

            return timeString + recuring
        }
    },
    getResourceName: (resource) => {
        return resource && resource.type === 'edl' && resource.name === 'edl' ? getResourceTypeByKey('edl') : resource && resource.name
    },
    isNeedValidationExcludeMe: (resource, book) => {
      return resource && resource.needValidation && resource.needValidation.status && !!book.pending;
    },
    getFloor: (floor) => {
        return translateEtage(floor);
    },
    getBooker: (event) => {
        if (event) {
            const lang = (FlowRouter.getParam("lang") || "fr");
            const translate = new ModuleReservationIndex(lang);
            const user = UsersProfile.findOne(event.origin);
            return event.origin === Meteor.user()._id ? translate.moduleReservationIndex['you'].toUpperCase() : (user ? `${user.firstname} ${user.lastname}` : '');
        }
    },
    haveParticipants: (book) => {
      return book && ((!!book.participants && book.participants.length > 0) || (!!book.externalParticipants && book.externalParticipants.length > 0))
    },
    notEdlAndEquipment: (type, book) => {
        return type !== 'edl' && type !== 'equipment' && book && book.participantNumber > 0;
    },
    notEdl: (type) => {
        return type !== 'edl';
    },
    getParticipantNumber: (book) => {
        return book && book.participantNumber ? book.participantNumber : '-';
    },
    isServiceNotEmpty: (book) => {
        return book && !_.isEmpty(book.services)
    },
    getServices: (services) => {
        return _.map(services, (s, i) => {
            return {
                ...s,
                key: i
            }
        })
    },
    getServiceName: (service) => {
        const serviceData = ResourceServices.findOne({key: service});
        return serviceData ? getTranslatedValue(serviceData) : '';
    },
    getServiceChild: (service) => {
        const serviceData = ResourceServices.findOne({key: service.key});

        const data = serviceData.children.filter((c) => {
            return service.data.includes(c.key)
        }).map((d) => {
            return getTranslatedValue(d)
        });
        const custom = service.custom.map((c) => {
            return c
        });
        return _.sortBy([...data, ...custom])
    },
    getParticipants: (participants) => {
        const users = UsersProfile.find({_id: {$in: participants}}).fetch();
        return !!users ? users : [];
    },
    reject: () => {
        return Template.instance().rejectAction.get()
    },
    getConfirmMessage: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);

        return translate.moduleReservationIndex[Template.instance().actionSelected.get() === 'reject' ? 'reject_book' : 'approve_book' ]
    },
    getRejectMessage: (pending) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationError(lang);
        const translateResa = new ModuleReservationIndex(lang);

        return Template.instance().taken.get() ? translate.moduleReservationError['already_taken_on_validate'] : translateResa.moduleReservationIndex[!pending ? 'cancel_book' : 'reject_book' ]
    },
    sendButton: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const instance = Template.instance();
        return {
            label: translate.moduleReservationIndex[!!instance.showSuggest.get() && !!instance.selectedResource.get() ? "send_and_book_label": "send_label"],
            class: (!!instance.showSuggest.get() &&
              ((!instance.emptySuggestion.get() && (!instance.selectedResource.get() || !instance.rejectMessage.get())) || (instance.emptySuggestion.get() && !instance.rejectMessage.get()))
            ) ||
            (!instance.showSuggest.get() && !instance.rejectMessage.get()) ? 'disabled' : 'red',
            onClick: () => {
                if (!!instance.showSuggest.get() && (!!instance.selectedResource.get())) {
                  instance.isMoveBook.set(true);
                } else {
                  instance.rejectBook(instance);
                }
            }
        }
    },
    yesButton: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const instance = Template.instance();
        return {
            label: translate.moduleReservationIndex["yes"],
            class: 'red',
            onClick: () => {
                if (instance.actionSelected.get() === 'reject') {
                    instance.rejectAction.set(true);
                } else {
                    instance.rejectBook(instance);
                }
            }
        }
    },
    noButton: () => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return {
            label: translate.moduleReservationIndex["no"],
            class: 'danger',
            onClick: () => {
                $("#confirmation-modal").modal("hide");
            }
        }
    },
    confirmButton: () => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ModuleReservationIndex(lang);
      const instance = Template.instance();
      return {
        label: translate.moduleReservationIndex["confirm"],
        class: 'red',
        onClick: () => {
          instance.moveBook(instance);
        }
      }
    },
    previousButton: () => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ModuleActualityIndex(lang);
      const instance = Template.instance();
      return {
        label: translate.moduleActualityIndex["previous"],
        class: 'danger',
        onClick: () => {
          instance.isMoveBook.set(false);
        }
      }
    },
    rejectAction: () => {
      return Template.instance().actionSelected.get() === 'reject'
    },
    MbTextArea: () => MbTextArea,
    getMessage: () => {
        return Template.instance().rejectMessage.get()
    },
    setMessage: () => {
        const instance = Template.instance()

        return () => value => {
            instance.rejectMessage.set(value)
        }
    },
    getEdlDirections: (book) => {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        return !!book.edlDirection ? translate.moduleReservationIndex[book.edlDirection] : translate.moduleReservationIndex["out"]
    },
    getParams: (resource, book) => {
      if (!!resource && !!book) {
        const instance = Template.instance();
        const resources = attachResourceType(Resources.find({condoId: book.condoId, type: resource.type}).fetch())
        let expandedResources = [];
        _.each(resources, (r) => {
          if (!isInt(r.number) && r._id !== resource._id) {
            expandedResources.push({
              ...r,
              realId: r._id
            })
          } else if (isInt(resource.number)) {
            for (let i = 1; i <= r.number; i++) {
              if (r._id !== resource._id || (r._id === resource._id && resource.number !== i)) {
                expandedResources.push({
                  ...r,
                  number: i,
                  _id: r._id + i,
                  total: r.number,
                  realId: r._id
                })
              }
            }
          }
        })

        return {
          resourceType: resource.type,
          resources: expandedResources,
          condoId: book.condoId,
          allDay: book.isAllDay,
          start: book.start,
          end: book.end,
          skipSuggestion: true,
          checkBox: true,
          onClick: (resource, time) => {
            instance.selectedResource.set(resource)
          },
          onEmpty: () => {
            instance.emptySuggestion.set(true)
          }
        }
      } else {
        return {}
      }
    },
    suggestButton: () => {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const translate = new ModuleReservationIndex(lang);
      const instance = Template.instance()
      return {
        label: translate.moduleReservationIndex["show_other_resources"],
        class: 'danger',
        onClick: () => {
          instance.showSuggest.set(true)
        }
      }
    },
    showSuggestion: () => {
      return !!Template.instance().showSuggest.get()
    },
    getSelectedResource: () => {
      return Template.instance().selectedResource.get()
    },
    moveBook: () => {
      return !!Template.instance().isMoveBook.get()
    },
    resNum: (resource, book) => {
      const lang = FlowRouter.getParam('lang') || 'fr'
      const tr_common = new ModuleReservationIndex(lang)
      return book.number && resource.number > 1 ? (
        resource.type === 'coworking' ? tr_common.moduleReservationIndex['place'] : tr_common.moduleReservationIndex['number']
      ) + ': ' + book.number : false
    }
});
