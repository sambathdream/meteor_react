
import { CommonTranslation } from "/common/lang/lang.js"
import { MarketBasket, TermsOfServices, MarketReservations } from '/common/collections/marketPlace'
import { UserTransactions } from '/common/collections/Payments'
// import { MarketReservations } from '/common/collections/marketPlace'
// import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import PayzenWrapper from '/client/components/PayZen/PayzenWrapper'
import setTotalValues from '../basket.functions'

Template.marketBasketReservationModal.onCreated(function () {
  Meteor.subscribe('getUserTransactions', FlowRouter.getParam('condo_id'))
  this.isOpen = new ReactiveVar(false)
  this.hasReadTerms = new ReactiveVar(false)

  this.amount = new ReactiveVar(0)
  this.paymentWaiting = new ReactiveVar(true)
  this.transactionId = new ReactiveVar(null)
  this.billingAddress = new ReactiveVar(null)
  this.paymentError = new ReactiveVar(null)
})

Template.marketBasketReservationModal.onDestroyed(() => {
})

Template.marketBasketReservationModal.onRendered(() => {
})

Template.marketBasketReservationModal.events({
  'hidden.bs.modal #modal_market_basket_reservation': (event, template) => {
    template.isOpen.set(false)
    template.hasReadTerms.set(false)
    template.transactionId.set(null)
    template.paymentWaiting.set(true)
    template.hasReadTerms.set(false)

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_market_basket_reservation': (event, template) => {
    template.isOpen.set(false)
    template.hasReadTerms.set(false)
    template.transactionId.set(null)
    template.paymentWaiting.set(true)
    template.hasReadTerms.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_market_basket_reservation";

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_market_basket_reservation") {
        modal.modal('hide');
      }
    }
  },
  'click .read-terms': (e, t) => {
    $(e.currentTarget).button('loading')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoId = FlowRouter.getParam('condo_id')
    const isBook = Template.instance().data.isBook
    const basketId = Template.instance().data.basketId

    if (basketId) {
      if (isBook) {
        Meteor.call('validateBookBakset', condoId, (error, result) => {
          $(e.currentTarget).button('reset')
          if (!error) {
            t.hasReadTerms.set(true)
          } else {
            sAlert.error(error.reason ? translate.commonTranslation[error.reason] : error)
          }
        })
      } else {
        let amount = Template.currentData().total
        amount = parseInt(amount.replace(/\./, ''))
        Meteor.call('checkPaidBasketQuantity', condoId, (error, result) => {
          if (!error && !result) {
            Meteor.call('checkTimeSlotUnavailable', condoId, (error, result) => {
              if (!error && !result) {
                t.amount.set(amount)
                t.paymentWaiting.set(false)
              } else {
                $(e.currentTarget).button('reset')
                sAlert.error(translate.commonTranslation.rserved_slots_unavailable)
              }
            })
          } else {
            $(e.currentTarget).button('reset')
            sAlert.error(translate.commonTranslation.not_enough_item)
          }
        })
      }
    } else {
      let amount = Template.currentData().total

      t.amount.set(amount)
      t.paymentWaiting.set(false)
    }
  },
  'click .__confirmBtn': (e, t) => {
    t.paymentError.set(null)
  },
  'click .button-ok': (e, t) => {
    const basket = MarketBasket.findOne({ condoId: FlowRouter.getParam('condo_id'), status: 'in_progress' })

    if (basket && basket.services) {
      $('#modal_market_basket_reservation').modal('hide')
    } else {
      $('#modal_market_basket_reservation').modal('hide')
      if (typeof Template.instance().data.onFinish === 'function') {
        Template.instance().data.onFinish()
      } else {
        setTimeout(() => {
          const params = {
            lang: FlowRouter.getParam('lang') || 'fr',
            condo_id: FlowRouter.getParam('condo_id'),
            module_slug: 'marketPlace',
            wildcard: 'upcoming'
          }

          FlowRouter.go('app.board.resident.condo.module', params)
        }, 500)
      }
    }
  }
})

Template.marketBasketReservationModal.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  getModalTitle: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const hasReadTerms = Template.instance().hasReadTerms.get()
    const paymentWaiting = Template.instance().paymentWaiting.get()
    const transactionId = Template.instance().transactionId.get()

    if (!paymentWaiting) {
      return translate.commonTranslation.select_payment_method
    }

    if (hasReadTerms) {
      if (transactionId) {
        return translate.commonTranslation.thank_purchase
      } else {
        return translate.commonTranslation.thank_booking
      }
    } else {
      return translate.commonTranslation.terms_and_conditions_of_service
    }
  },
  hasReadTerms: () => {
    return Template.instance().hasReadTerms.get()
  },
  getTerms: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const terms = TermsOfServices.findOne({ condoId })
    return terms ? terms.value : ''
  },
  getUserName: () => {
    const user = Meteor.user()
    return user.profile.firstname + ' ' + user.profile.lastname
  },
  getBuildingAddress: () => {
    const billingAddress = Template.instance().billingAddress.get()
    if (billingAddress) {
      return billingAddress.address + ', ' + billingAddress.zipCode + ' ' + billingAddress.city + ', ' + billingAddress.country
    }

    const condoId = FlowRouter.getParam('condo_id')
    const thisCondo = Condos.findOne({ _id: condoId })

    if (thisCondo) {
      return thisCondo.getAddress()
    } else {
      return '-'
    }
  },
  getBasketId: () => Template.instance().data.basketId,
  getReservationId: () => !!Template.instance().data.reservationId ? Template.instance().data.reservationId : null,
  getBasket: () => {
    const basketId = Template.instance().data.basketId
    const isBook = Template.currentData().isBook
    const basket = MarketBasket.findOne({ _id: basketId }) || {}
    let retBasket = {
      hasPaid: false,
      subtotal: 0,
      isBook,
      differentVat: [],
      total: 0,
      services: []
    }

    if (!basket.services) {
      basket.services = [Template.currentData().summaryCustom]
    }

    retBasket.services = !!basket.services && basket.services.filter(s => {
      if ((isBook && !s.pay_outside_platform) || (!isBook && s.pay_outside_platform)) return false
      return true
    })
    setTotalValues(retBasket)
    return retBasket
  },
  getPayzenWrapper: () => PayzenWrapper,
  getAmount: () => Template.instance().amount.get(),
  saveTransactionId: () => {
    const t = Template.instance()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    return (transactionId, result, billingAddress) => {
      t.billingAddress.set(billingAddress || null)
      if (result !== 'REFUSED') {
        t.transactionId.set(transactionId)
        t.paymentError.set(null)
        t.paymentWaiting.set(true)
        t.hasReadTerms.set(true)
      } else {
        // t.paymentWaiting.set(true)
        // t.hasReadTerms.set(true)
        t.paymentError.set(result)
        console.log('result', result)
        // sAlert.error(translate.commonTranslation.error_occured)
      }
    }
  },
  paymentWaiting: () => Template.instance().paymentWaiting.get(),
  getTransactionId: () => {
    const transaction = UserTransactions.findOne({ _id: Template.instance().transactionId.get() })
    const response = (transaction && transaction.threeDsResponse) ? transaction.threeDsResponse.createPaymentResult : (transaction && transaction.createPayment && transaction.createPayment.result && transaction.createPayment.result.createPaymentResult) || null
    return (response && response.paymentResponse && response.paymentResponse.transactionId) || null
  },
  getPaymentError: () => Template.instance().paymentError.get()
})
