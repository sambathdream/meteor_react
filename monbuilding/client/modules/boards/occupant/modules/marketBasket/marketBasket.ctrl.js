import { MarketBasket, MarketServices } from '/common/collections/marketPlace'
import setTotalValues from './basket.functions'

Template.occupant_marketBasket.onCreated(function() {
  this.condoId = FlowRouter.getParam('condo_id')
  this.isBook = new ReactiveVar(true)
  this.basketId = new ReactiveVar(null)

  this.outOfStock = new ReactiveVar(false)
  this.timeSlotUnavailable = new ReactiveVar(false)

  this.handler = Meteor.subscribe('market_place_basket_with_reservations', this.condoId)
  Meteor.call('checkPaidBasketQuantity', this.condoId, (error, result) => {
    if (!error && result) {
      this.outOfStock.set(result)
    }
  })
  Meteor.call('checkTimeSlotUnavailable', this.condoId, (error, result) => {
    if (!error && result) {
      this.timeSlotUnavailable.set(result)
    }
  })
  Meteor.subscribe('get-terms-of-services', this.condoId)
})

Template.occupant_marketBasket.onRendered(function() {

})

Template.occupant_marketBasket.events({
  'click .button-valid-book': (e, t) => {
    t.isBook.set(true)
    $('#modal_market_basket_reservation').modal('show')
  },
  'click .button-valid-paid': (e, t) => {
    t.isBook.set(false)
    $('#modal_market_basket_reservation').modal('show')
  },
  'click .edit-container': function(e, t) {
    let reservationId = $(e.currentTarget).attr('reservationid')
    let serviceId = $(e.currentTarget).attr('serviceid')
    const lang = FlowRouter.getParam('lang') ||'fr'
    const condoId = t.condoId
    const params = {
      lang,
      condo_id: condoId,
      module_slug: 'marketPlace',
      serviceId,
      reservationId
    }
    FlowRouter.go('app.board.resident.condo.module.marketPlace.serviceDetails.serviceId', params)
  },
  'click .delete-container': (e, t) => {
    let reservationId = $(e.currentTarget).attr('reservationid')
    Meteor.call('deleteBasketItem', reservationId)
  }
})

Template.occupant_marketBasket.helpers({
  getCondoName: () => {
    const condoId = FlowRouter.getParam('condo_id')
    return Condos.findOne({ _id: condoId }).getName()
  },
  getBasket: () => {
    const basket = MarketBasket.findOne({ condoId: Template.instance().condoId, status: 'in_progress' })
    let retBasket = {
      hasBook: false,
      hasPaid: false,
      isBook: false,
      subtotal: 0,
      differentVat: [],
      total: 0,
      services: []
    }

    if (basket && basket.services) {
      const t_basketId = Template.instance().basketId.get()
      if (!t_basketId && t_basketId !== basket._id) {
        Template.instance().basketId.set(basket._id)
      }
      retBasket.hasBook = basket && basket.services && basket.services.length > 0 && !!basket.services.find(s => (s.pay_outside_platform === true))
      retBasket.hasPaid = retBasket.hasBook && basket.services && basket.services.length > 0 && !!basket.services.find(s => (s.pay_outside_platform !== true))
      retBasket.isBook = retBasket.hasBook && !!basket.services.find(s => (s.pay_outside_platform === true && s.validatedAt === null))
      retBasket.services = basket.services.filter(s => {
        if (retBasket.isBook && !s.pay_outside_platform) return false
        if (retBasket.isBook && s.validatedAt !== null) return false
        if (!retBasket.isBook && s.pay_outside_platform) return false
        return true
      })
      setTotalValues(retBasket)
    }
    return retBasket
  },
  isOutOfStock: (serviceId) => {
    const t = Template.instance()
    const outOfStock = t.outOfStock.get()
    return !!outOfStock && !!outOfStock[serviceId]
  },
  getOutOfStock: (serviceId) => {
    const t = Template.instance()
    const outOfStock = t.outOfStock.get()
    return !!outOfStock && outOfStock[serviceId]
  },
  isTimeSlotUnavailable: (marketReservationsId) => {
    const t = Template.instance()
    const timeSlotUnavailable = t.timeSlotUnavailable.get()
    return !!timeSlotUnavailable && !!timeSlotUnavailable.includes(marketReservationsId)
  },
  getIcon: (serviceId) => {
    const service = MarketServices.findOne({ _id: serviceId })
    if (service && service.files) {
      return service.files[0]
    }
    return null
  },
  isSubscribeReady: () => {
    return Template.instance().handler.ready()
  },
  getDataModal: (basket) => {
    return {
      isBook: Template.instance().isBook.get(),
      basketId: Template.instance().basketId.get(),
      total: basket.total
    }
  }
})
