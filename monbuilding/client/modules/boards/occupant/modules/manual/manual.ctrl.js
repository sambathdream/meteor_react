import {FileManager} from '/client/components/fileManager/filemanager.js'
import { DeletePost, ModuleManual, CommonTranslation } from '/common/lang/lang.js'

function constructAttachmentElement (data)  {
  var progress = $('<span></span>')
    .addClass('progress')
    .css('width', '0%')

  var meter = $('<div></div>')
    .addClass('meter')
    .append(progress)

  var iconFile = $('<span></span>')
    .addClass('icon-file')
    .append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'))

  var checkmark = $('<span class="checkmark"></span>')
    .append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'))

  var fileSize = $('<span class="file-size"></span>')
    .html(data.size)
    .append(checkmark)

  var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name)

  var fileRemove = $(`<span class="remove-item">`)
    .append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'))

  var file = $('<div></div>')
    .addClass('file')
    .append(iconFile)
    .append(fileName)
    .append(fileSize)
    .append(fileRemove)
    .append(meter)

  var list = $('<li></li>')
    .attr('id', data.id)
    .addClass('attachment-item')
    .append(file)

  return list
}

function createPdfViewer (condoId) {
  let condo = Condos.findOne({_id: condoId})
  if (condo && condo.manual) {
    let file = CondoDocuments.findOne({_id: condo.manual.data})
    let url = file.link().replace('localhost', location.hostname)
    PDFJS.workerSrc = '/packages/pascoual_pdfjs/build/pdf.worker.js'
    PDFJS.getDocument(url).then(function renderPages(pdf) {
      for (let currentPage = 1; currentPage <= pdf.numPages; currentPage++) {
        pdf.getPage(currentPage).then(function renderPage(page){
          var scale = 1
          var viewport = page.getViewport(scale)
          var canvas = document.createElement('canvas')
          var context = canvas.getContext('2d')
          canvas.style.border = '1px solid #000'
          canvas.style.margin = '5px auto'
          canvas.width = viewport.width
          canvas.height = viewport.height
          page.render({canvasContext: context, viewport: viewport})
          document.getElementById('pdfCanvas').appendChild(canvas)
        })
      }
    })
  }
}

Template.module_manual.onCreated(function (){
  Meteor.subscribe('condodocuments')
  const condoId = FlowRouter.getParam('condo_id')
  let condo = Condos.findOne(condoId)
  if (condo && condo.settings.options.manual === false) {
    // FlowRouter.go('/fr/resident')
  }
  this.fm = new FileManager(CondoDocuments, { condoId: condoId })
  this.msg = {value: ''}
  this.errorMessage = new ReactiveVar('')
  this.radio = new ReactiveVar(condo.manual ? condo.manual.type : 'file')
  this.isEditing = new ReactiveVar(false)
  Meteor.call('setNewAnalytics', { type: 'module', module: 'manual', accessType: 'web', condoId: condoId })
  Meteor.call('setDocumentHasView', condoId, 'manual')
})

Template.module_manual.onDestroyed(function () {
  // Meteor.call('updateAnalytics', {type: 'module', module: 'manual', accessType: 'web', condoId: FlowRouter.current().params.condo_id})
})

Template.module_manual.events({
  'show.bs.modal #modalManual': function (event, template) {
    let modal = $(event.currentTarget)
    window.location.hash = '#modalManual'
    window.onhashchange = function () {
      if (location.hash != '#modalManual') {
        modal.modal('hide')
      }
    }
  },
  'hidden.bs.modal #modalManual': function (event, template) {
    template.errorMessage.set('')
    $('.errorModalInput').removeClass('errorModalInput')
    if ($('#attachments li')[0]) {
      $('#attachments li').remove()
    }
    template.fm.clearFiles()
    $('.attachment-item').remove()
    $('#validManual').button('reset')
    history.replaceState('', document.title, window.location.pathname)
  },
  'change #fileUploadInputMan': function (e, t) {
    let lang = (FlowRouter.getParam('lang') || 'fr')
    let translation = new ModuleManual(lang)
    let tr_common = new CommonTranslation(lang)

    $('#changePdf').button(translation.moduleManual['loading'])

    if (e.currentTarget.files && e.currentTarget.files.length == 1) {
      if (e.currentTarget.files[0].type != 'application/pdf') {
        sAlert.error(tr_common.commonTranslation['pdf_only'])
        $('#changePdf').button('reset')
      } else {
        let hasThrownErr = false
        let id = t.fm.insert(e.currentTarget.files[0], function (err, file) {
          if(err) {
            sAlert.error(tr_common.commonTranslation[err.reason])
            $('#changePdf').button('reset')
            hasThrownErr = true
          }
        })

        if (!hasThrownErr) {
          var data = {}
          data.name = e.currentTarget.files[0].name
          data.size = (e.currentTarget.files[0].size / 1000).toFixed(2) + ' kb'
          data.id = 'file-' + id
          $('#documentValid').append(constructAttachmentElement(data))
          var progress = $('#documentValid').find('.progress')
          var elapsed = 0
          var wrapper = $('.attachment-item')

          var timer = setInterval(function () {
            if (elapsed == 100 || t.fm.getFileList()[id].done) {
              wrapper.find('.meter').remove()
              wrapper.addClass('completed')
              $('#changePdf').hide()
              $('#changePdf').button('reset')
              $('#documentValid').append('<button type="button" id="savePdf" class="btn red_btn pull-right">' + translation.moduleManual['save'] + '</button>')
              clearInterval(timer)
            }
            if (wrapper.find('.meter').length && elapsed < t.fm.getFileList()[0].upload.progress.get()) {
              elapsed = t.fm.getFileList()[id].upload.progress.get()
              progress.animate({
                width: elapsed + '%'
              }, 200)
            }
          }, 100)
        }
      }
    } else {
      $('#changePdf').button('reset')
    }
  },
  'click .remove-item': function (event, template) {
    $(event.currentTarget.parentElement.parentElement).remove()
    template.fm.clearFiles()
    $('#fileUploadInputMan').val('')
    $('#savePdf').remove()
    $('#changePdf').show()
  },
  'click #savePdf': function (event, template) {
    if (template.fm.getFileList()[0].done == true) {
      let file = template.fm.getFileList()[0].file
      Meteor.call('setManual', FlowRouter.getParam('condo_id'), file._id, 'file', function (error) {
        // $('#drop').button('reset')
        $('#modalManual').modal('hide')
        $('#changePdf').show()
        $('.attachment-item').remove()
        $('#savePdf').remove()
        template.radio.set('file')
      })
    }
  },
  'click #changePdf': function (e, t) {
    $('#fileUploadInputMan').click()
  },
  'click #deleteManual': function (e, t) {
    const bootConfirm = new DeletePost(FlowRouter.getParam('lang') || 'fr')
    let translation = new ModuleManual((FlowRouter.getParam('lang') || 'fr'))

    bootbox.confirm({
      title: translation.moduleManual['del_manual'],
      message: translation.moduleManual['sure_del_manual'],
      buttons: {
        confirm: {label: bootConfirm.deletePost['yes'], className: 'btn-red-confirm'},
        cancel: {label: bootConfirm.deletePost['no'], className: 'btn-outline-red-confirm'}
      },
      callback: function (result) {
        if (result)
        Meteor.call('deleteManual', FlowRouter.getParam('condo_id'))
      }
    })
  },
  'change input[name="selectRadio"]': function (e, t) {
    console.log('value', e.currentTarget.value);
    t.radio.set(e.currentTarget.value)
  },
})

Template.module_manual.helpers({
  isSmallerDevice: () => {
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      return true
    } else {
      return false
    }
  },
  manualData: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let condo = Condos.findOne(condoId)
    if (condo && condo.manual) {
      let data = condo.manual.data
      if (data) {
        if (condo.manual.type === 'file') {
          let file = CondoDocuments.findOne({_id: data})
          if (file) {
            const link = `/download/CondoDocuments/${file._id}/original/${file.name || file._id + '.pdf'}`
            return link
          }
        }
        else  {
          return data
        }
      }
    }
  },
  manualFile: (id) => {
    let condoId = FlowRouter.getParam('condo_id')
    if (condoId != '') {
      createPdfViewer(condoId)
      return true
    }
    return false
  },
  manualType: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let condo = Condos.findOne({_id: condoId})
    if (condo && condo.manual && condo.manual.type) {
      return condo.manual.type
    }
  },
  hasManual: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let condo = Condos.findOne(condoId)
    if (condo && condo.manual) {
      let data = condo.manual
      if (data) {
        return true
      }
      return false
    }
    return false
  },
  previousManual: () => {
    let condoId = FlowRouter.getParam('condo_id')
    if (condoId != '') {
      let condo = Condos.findOne({ _id: condoId })
      if (condo) {
        if (condo.manual && condo.manual.type === "text" && condo.manual.data) {
          return condo.manual.data
        }
      }
    }
  },
  lastUpdate: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let condo = Condos.findOne(condoId)
    if (condo) {
      return (condo.manual.manualUpdate)
    }
  },
  getFm: () => {
    return Template.instance().fm
  },
  getCb: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let isEditing = Template.instance().isEditing
    let radio = Template.instance().radio
    let translation = new ModuleManual((FlowRouter.getParam('lang') || 'fr'))
    return (message, files) => {
      if (message != '<p><br></p>') {
        Meteor.call('setManual', condoId, message, 'text')
        $('#modalManual').modal('hide')
        sAlert.success(translation.moduleManual['edit_success'])
        isEditing.set(false)
        radio.set('text')
      }
      else {
        sAlert.error(translation.moduleManual['empty_text'])
      }
    }
  },
  getCancelCb: () => {
    let isEditing = Template.instance().isEditing
    return () => {
      isEditing.set(false)
    }
  },
  isMessageError: () => {
    return Template.instance().errorMessage.get()
  },
  checkDocManual: () => Template.instance().radio.get() === 'file',
  checkTextManual: () => Template.instance().radio.get() === 'text',
  setDocumentRadio: () => {
    let radio = Template.instance().radio
    return () => {
      radio.set('file')
    }
  },
  setTextRadio: () => {
    let radio = Template.instance().radio
    return () => {
      console.log();
      radio.set('text')
    }
  }
})
