import { CommonTranslation, MessageInput, ModuleMessagerieIndex } from '/common/lang/lang.js'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { ReactiveVar } from 'meteor/reactive-var'
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'

let timeout = null
let typingTimeout = null

Template.conversation_footer.onCreated(function () {
  this.messageId = Template.currentData().currentConversation
  this.messages = new ReactiveVar('')
  this.amIBlocked = new ReactiveVar(false)
  this.files = new ReactiveVar([])
  this.sendNotif = new ReactiveVar(true)
  this.fm = new FileManager(MessengerFiles, { userId: Meteor.userId(), condoId: FlowRouter.getParam('condo_id'), msgId: Template.currentData().currentConversation })
  this.fmIncident = new FileManager(IncidentFiles, { userId: Meteor.userId(), condoId: FlowRouter.getParam('condo_id') })

  this.busy = new ReactiveVar(false);
})

Template.conversation_footer.onRendered(function () {

})

Template.conversation_footer.helpers({
  isSameMessage: () => Template.instance().messageId === Template.currentData().currentConversation,
  setMessageIdAndResetMessageInput: () => {
    Template.instance().messageId = Template.currentData().currentConversation
    Template.instance().messages.set('')
    Template.instance().files.set([])
  },
  getUsersInformation: () => Template.currentData().usersInformation,
  getSelectedTab: () => Template.currentData().currentTab,
  getCurrentConversation: () => Template.currentData().currentConversation,
  getMessageIdForEditing: () => Template.currentData().messageIdForEditing,
  MbTextArea: () => MbTextArea,
  getMessageValue: () => Template.instance().messages.get(),
  setMessageValue: () => {
    let template = Template.instance()
    return (value) => template.messages.set(value)
  },
  disabledButton: () => !((Template.instance().messages.get() && Template.instance().messages.get() !== '') || (Template.instance().files.get() && Template.instance().files.get().length > 0)),
  getFiles: () => {
    const files = Template.instance().files.get()
    if (!!files) {
      files.forEach((file, index) => {
        if (typeof file === 'string') {
          files[index] = MessengerFiles.findOne({ _id: file })
          if (!files[index]) {
            files[index] = IncidentFiles.findOne({ _id: file })
          }
        }
      })
    }
    return files
  },
  onFileAdded: () => {
    const template = Template.instance()
    let oldFiles = template.files.get()
    return (newFiles) => {
      template.files.set([
        ...oldFiles,
        ...newFiles
      ])
    }
  },
  onFileRemoved: () => {
    const template = Template.instance()
    return (fileId) => {
      template.files.set(template.files.get().filter(f => f.fileId ? f.fileId !== fileId : (f._id ? f._id !== fileId : f.id !== fileId)))
    }
  },
  refreshTextAndFile: (messageId) => {
    let message = MessagesFeed.findOne({ _id: messageId })
    if (!!message === true) {
      Template.instance().messages.set(message.text)
      Template.instance().files.set(message.files || message.incidentFiles)
    }
  },
  checkResidentRoomExist: (event) => {
    const isNewMessage = Template.currentData().isNewMessage
    const userForNewConversation = Template.currentData().userForNewConversation
    const condoId = FlowRouter.getParam('condo_id')
    const selectConversationCb = Template.currentData().selectConversationCb
    const setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    const onSelectUser = Template.currentData().onSelectUser
    const currentTab = Template.currentData().currentTab
    const conversationId = Template.currentData().currentConversation

    return (event) => {
      let isOccupantRoles = DefaultRoles.findOne({ _id: currentTab, name: 'Occupant'})
      if (!!isNewMessage === true && !!isOccupantRoles === true) {
        Meteor.call('checkResidentRoomExist', userForNewConversation, condoId, function (error, result) {
          if (!error && result) {
            selectConversationCb(result)
            setIsNewMessageAsFalse()
            onSelectUser([])
          }
        })
      }
      if (conversationId) {
        Meteor.call('updateLastVisit', conversationId)
      }
    }
  },
  validMessage: (event) => {
    const template = Template.instance()
    const conversationId = Template.currentData().currentConversation

    const messageIdForEditing = Template.currentData().messageIdForEditing
    const setMessageIdForEditing = Template.currentData().setMessageIdForEditing

    const isNewMessage = Template.currentData().isNewMessage
    const informationForNewIncident = Template.currentData().getInformationForNewIncident
    const setIsNewMessageAsFalse = Template.currentData().setIsNewMessageAsFalse
    const userForNewConversation = Template.currentData().userForNewConversation
    const onSelectUser = Template.currentData().onSelectUser

    const selectConversationCb = Template.currentData().selectConversationCb

    const messages = Template.instance().messages.get().trim()
    const files = Template.instance().files.get()
    const sendNotif = Template.instance().sendNotif.get()

    return (event) => {
      Meteor.call('userIsTyping', conversationId)
      if (typingTimeout !== null) {
        clearTimeout(typingTimeout)
      }
      typingTimeout = setTimeout(function () {
        Meteor.call('userIsNotTyping', conversationId)
      }, 3000)
      if (event.key === 'Enter' && !event.shiftKey) {
        event.preventDefault()

        const lang = FlowRouter.getParam('lang') || 'fr'
        const translationModuleMessagerieIndex = new ModuleMessagerieIndex(lang)
        const translationCommonTranslation = new CommonTranslation(lang)

        if (files && files.length && !template.fm.validateFiles(files)) {
          sAlert.error(translationCommonTranslation.commonTranslation['VIDEO_Files_not_alowed'])
          return
        }

        // Another check if previous process is still exist
        // prevent multiple message submission on lagging device such as Arthur's
        // case: template.files.set([]) already set to empty array but in his device it take coupld milis to update
        if (!!template.busy.get()) {
          return;
        }
        template.messages.set('')
        template.files.set([])


        template.busy.set(true)

        // Editing a message :
        if (!!messageIdForEditing === true) {
          if ((!!messages === false || messages === '') && (!!files === false || files.length === 0)) {

            bootbox.confirm({
              size: 'medium',
              title: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message'],
              message: translationModuleMessagerieIndex.moduleMessagerieIndex['delete_message_explain'],
              buttons: {
                confirm: { label: translationCommonTranslation.commonTranslation['confirm'], className: 'btn-red-confirm' },
                cancel: { label: translationCommonTranslation.commonTranslation['cancel'], className: 'btn-outline-red-confirm' }
              },
              backdrop: true,
              callback: function (res) {
                if (res) {
                  let messageSave = MessagesFeed.findOne({ _id: messageIdForEditing })
                  Meteor.call('removeMessageFeed', messageIdForEditing, function (error, result) {
                    if (!error) {
                      let existMessageAfterDeletion = MessagesFeed.findOne()
                      if (!!existMessageAfterDeletion === false) {
                        Meteor.call('removeConversation', messageSave.messageId, function (error, result) {
                          if (!error) {
                            sAlert.success(translationModuleMessagerieIndex.moduleMessagerieIndex['conversation_deleted'])
                          }
                        })
                      }
                    }

                    template.busy.set(false)
                  })
                }
              }
            })
          } else {
            if (!!files) {
              template.fm.batchUpload(files)
                .then(results => {
                  let filesId = results.map(f => f._id)
                  Meteor.call('updateMessageFeed', messageIdForEditing, messages, filesId, function (error, result) {
                    if (!error) {
                      if (result === 0) {
                      } else {
                        template.messages.set(messages)
                      }

                      template.busy.set(false)
                    }
                  })
                })
            } else {
              Meteor.call('updateMessageFeed', messageIdForEditing, messages, [], function (error, result) {
                if (!error) {
                  if (result === 0) {
                  } else {
                    template.messages.set(messages)
                  }

                  template.busy.set(false)
                }
              })
            }
          }
          setMessageIdForEditing(null)
          return
        }

        if (!messages && (!files || files.length === 0)) return template.busy.set(false)

        Meteor.call('userIsNotTyping', conversationId)

        // Send a message in existing conversation :
        if (!isNewMessage) {
          template.fm.batchUpload(files)
          .then(results => {
            let filesId = results.map(f => f._id)
            // TODO: fix bug send incident
            let dataForMessage = {
              conversationId,
              template,
              messages,
              files,
              filesId,
              sendNotif
            }

            addNewMessage(dataForMessage)
          })
        } else {
          // Send a message in new conversation :
          const target = FlowRouter.getParam('wildcard')
          const isOccupantRoles = DefaultRoles.findOne({ _id: target, name: 'Occupant' })
          const condoId = FlowRouter.getParam('condo_id')
          const lang = FlowRouter.getParam('lang')

          $('.workInProgress').css('display', 'block')
          // template.fmIncident.batchUpload(files)
          // .then(results => {
          //   let filesId = results.map(f => f._id)

            if (target === 'manager' /* !!informationForNewIncident === true */) {
              if (!informationForNewIncident) {
                template.messages.set(messages)
                template.files.set(files)
                template.busy.set(false)
                $('.workInProgress').css('display', 'none')
              } else {
                let currentFm = template.fm
                if (informationForNewIncident.isAnIncident) {
                  currentFm = template.fmIncident
                }
                currentFm.batchUpload(files)
                .then(results => {
                  let filesId = results.map(f => f._id)

                  let dataForIncident = {
                    template,
                    informationForNewIncident,
                    condoId,
                    messages,
                    filesId,
                    sendNotif,
                    selectConversationCb,
                    setIsNewMessageAsFalse,
                    onSelectUser,
                    lang,
                    files
                  }
                  newIncident(dataForIncident)
                }).catch(err => {
                  $('.workInProgress').css('display', 'none')
                  sAlert.error(translationCommonTranslation.commonTranslation[err.reason])
                })
              }
            } else if ((!!userForNewConversation === false || userForNewConversation.length === 0) && !!isOccupantRoles) {
              template.messages.set(messages)
              template.files.set(files)
              template.busy.set(false)
              $('.workInProgress').css('display', 'none')
            } else {
              console.log('yo')
              template.fm.batchUpload(files)
              .then(results => {
                let filesId = results.map(f => f._id)

                let dataForConversation = {
                  template,
                  target,
                  userForNewConversation,
                  condoId,
                  messages,
                  filesId,
                  files,
                  selectConversationCb,
                  setIsNewMessageAsFalse,
                  onSelectUser
                }
                newConversation(dataForConversation)
              }).catch(err => {
                $('.workInProgress').css('display', 'none')
                sAlert.error(translationCommonTranslation.commonTranslation[err.reason])
              })
            }
          // }).catch(err => {
          //   $('.workInProgress').css('display', 'none')
          //   sAlert.error(translationCommonTranslation.commonTranslation[err.reason])
          // })
        }

        const chatBody = $('.conversation-body')
        if (chatBody && chatBody[0]) {
          chatBody.stop().animate({
            scrollTop: chatBody[0].scrollHeight
          }, 800)
        }
      }
    }
  },
  setSendNotif: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new MessageInput(lang)
    const instance = Template.instance()
    return {
      checked: instance.sendNotif.get(),
      label: translate.messageInput['reminder'],
      onClick: () => {
        instance.sendNotif.set(!instance.sendNotif.get())
      }
    }
  },
  activeTooltipSendNotif: () => {
    if (timeout != null) {
      clearTimeout(timeout)
    }
    timeout = setTimeout(function () {
      $('[data-toggle="tooltip"]').tooltip({
        template: '<div class="tooltip messenger_tooltip_manager"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: 'hover'
      })
    }, 500)
  },
  isManagerTab: () => {
    let isManagerTab = Template.currentData().currentTab === 'manager'
    let conversationId = Template.currentData().currentConversation
    let conversation = Messages.findOne({ _id: conversationId })
    if (!!conversation === true) {
      return conversation.global !== true && isManagerTab
    } else {
      return isManagerTab
    }
  },
  amIBlocked: () => {
    return Template.instance().amIBlocked.get()
  },
  isBlockedUser: () => {
    const conversationId = Template.currentData().currentConversation
    const conversation = Messages.findOne({ _id: conversationId })
    const instance = Template.instance()

    if (!!conversation && !!conversation.participants && conversation.participants.length === 2) {
      const otherUserId = _.find(conversation.participants, (p) => p !== Meteor.user()._id)

      // if the receiver is blocked
      const blockedUsers = Meteor.user().blockedUsers
      if (!!blockedUsers && !!blockedUsers.includes(otherUserId)) {
        instance.amIBlocked.set(false)
        return true
      }

      // if the sender is blocked
      const blockedBy = Meteor.user().blockedBy
      if (!!blockedBy && !!blockedBy.includes(otherUserId)) {
        instance.amIBlocked.set(true)
        return true
      }
    }

    return false
  }
})

Template.conversation_footer.events({
  'mouseenter #info-bubble-reminder': function(e, t){
    $('#info-content-reminder').css("display", "block")
    $('#info-content-reminder').addClass("visible")
  },
  'mouseleave #info-bubble-reminder': function(e, t){
    $('#info-content-reminder').css("display", "none")
    $('#info-content-reminder').removeClass("visible")
  },
  'click .unblock-user' (event, template) {
    const currentConversation = Messages.findOne({ _id: template.data.currentConversation })
    if (!!currentConversation) {
      const blockedUser = _.find(currentConversation.participants, (p) => p !== Meteor.user()._id)
      if (!!blockedUser) {
        const translation_ = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
        bootbox.confirm({
          title: translation_.commonTranslation.unblock_user,
          message: translation_.commonTranslation.unblock_user_confirm,
          buttons: {
            confirm: {label: translation_.commonTranslation.yes, className: "btn-red-confirm"},
            cancel: {label: translation_.commonTranslation.no, className: "btn-outline-red-confirm"}
          },
          callback: function(res) {
            if (res) {
              Meteor.call('unBlockUser', blockedUser, () => {
                sAlert.success(translation_.commonTranslation.unblock_user_success);
              });
            }
          }
        });
      }
    }
  },
})

function newConversation ({ template, target, userForNewConversation, condoId, messages, filesId, files, selectConversationCb, setIsNewMessageAsFalse, onSelectUser }) {
  let data = {
    target,
    destinataires: userForNewConversation,
    condoId,
    message: messages,
    files: filesId,
    sujet: messages
  }
  data.destinataires.push(Meteor.userId())

  let meteorCall = 'addMessage'
  let defaultRole = DefaultRoles.findOne({ _id: target })
  if (!!defaultRole && defaultRole.name === 'Conseil Syndical') {
    meteorCall = 'addMessageCS'
    data.otherMembers = _.without(userForNewConversation, target)
  } else if (!!defaultRole && defaultRole.name === 'Gardien') {
    meteorCall = 'addMessageKeeper'
    data.otherMembers = _.without(userForNewConversation, target)
  }

  Meteor.call(meteorCall, data, (error, result) => {
    if (!error && result) {
      selectConversationCb(result)
      setIsNewMessageAsFalse()
      onSelectUser([])
    } else {
      template.messages.set(messages)
      template.files.set(files)
      sAlert.error(error)
    }

    template.busy.set(false)
    $('.workInProgress').css('display', 'none')
  })
}

function newIncident ({ template, informationForNewIncident, condoId, messages, filesId, sendNotif, selectConversationCb, setIsNewMessageAsFalse, onSelectUser, lang, files }) {
  if (informationForNewIncident.isAnIncident === false) {
    let data = {
      target: 'manager',
      condoId: condoId,
      message: messages,
      files: filesId,
      type: informationForNewIncident.incidentType,
      details: informationForNewIncident.otherDetails,
      priority: informationForNewIncident.priority,
      sujet: informationForNewIncident.otherDetails,
      reminder: sendNotif
    }
    Meteor.call('addMessageGestionnaire', data, (error, result) => {
      if (!error && result) {
        selectConversationCb(result)
        setIsNewMessageAsFalse()
        onSelectUser([])
      } else {
        template.messages.set(messages)
        template.files.set(files)
        sAlert.error(error)
      }
      $('.workInProgress').css('display', 'none')
      template.busy.set(false)
    })
  } else {
    let phone = `+${informationForNewIncident.phoneCode} ${informationForNewIncident.phone}`
    let incident = {
      'declarer': {
        'phone': phone
      },
      'info': {
        'incidentTypeId': informationForNewIncident.incidentType,
        'type': informationForNewIncident.incidentDetails,
        'priority': informationForNewIncident.priority,
        'zone': informationForNewIncident.area,
        'title': informationForNewIncident.description,
        'comment': messages
      }
    }
    Meteor.call('publishIncident', condoId, incident, 'resident', filesId, sendNotif, function (error, result) {
      if (!error) {
        let data = {
          target: 'manager',
          condoId: condoId,
          destinataires: [Meteor.userId()],
          message: messages,
          files: filesId,
          incidentId: result,
          details: informationForNewIncident.incidentDetails,
          type: informationForNewIncident.incidentType,
          priority: informationForNewIncident.priority,
          sujet: informationForNewIncident.description
        }
        Meteor.call('addIncidentToMessageList', data, function (error, result) {
          if (!error) {
            let translation = new ModuleMessagerieIndex(lang)
            bootbox.alert({
              title: translation.moduleMessagerieIndex['title_declaration'],
              message: translation.moduleMessagerieIndex['declaration_send'],
              buttons: {
                'ok': { label: 'OK', className: 'btn-red-confirm' }
              }
            })
            selectConversationCb(result)
            setIsNewMessageAsFalse()

            template.busy.set(false)
          }
          $('.workInProgress').css('display', 'none')
        })
      } else {
        $('.workInProgress').css('display', 'none')
        template.messages.set(messages)
        template.files.set(files)
        template.busy.set(false)
        sAlert.error(error)
      }
    })
  }
}

function addNewMessage ({ conversationId, template, messages, files, filesId, sendNotif }) {
  let conversation = Messages.findOne(conversationId)
  if (!!conversation === false) {
    template.messages.set(messages)
    template.files.set(files)
    sAlert.error('Error: Conversation not found.')

    template.busy.set(false);
  }
  if (conversation.target === 'manager' && !!conversation.incidentId === true) {
    template.busy.set(false)
    Meteor.call('newMessageIncidentFromMessenger', conversationId, messages, filesId, sendNotif, function (error) {
      template.$('#sendMessage').button('reset')
      if (!error) {
      } else {
        template.messages.set(messages)
        template.files.set(files)
        sAlert.error(error)
      }

      template.busy.set(false);
    })
  } else {
    Meteor.call('addComment', conversationId, messages, filesId, sendNotif, function (error) {
      template.$('#sendMessage').button('reset')
      if (!error) {
      } else {
        template.messages.set(messages)
        template.files.set(files)
        sAlert.error(error)
      }

      template.busy.set(false);
    })
  }
}
