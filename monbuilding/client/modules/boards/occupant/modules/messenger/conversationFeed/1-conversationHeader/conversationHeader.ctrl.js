import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import { CommonTranslation } from "/common/lang/lang.js"
import _ from 'lodash'
var PNF = require('google-libphonenumber').PhoneNumberFormat
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()

/**
* Created by vmariot on 30/04/18.
*/
const mimeType = (type) => {
  return type.split('/')[0]
}

Template.conversation_header.onCreated(function () {
  this.subscribe('PhoneCode')

  this.userForNewConversation = new ReactiveVar([])
  this.oldTab = new ReactiveVar(null)
  this.oldConversationName = new ReactiveVar(null)
  this.blockedAlertPopUpShown = new ReactiveVar(false)
  this.goodPhone = false

  const user = Meteor.user()

  this.form = new ReactiveDict()
  this.form.setDefault({
    incidentType: null,
    incidentDetails: null,
    otherDetails: null,
    priority: null,
    description: null,
    area: null,
    phone: (user && user.profile && user.profile.tel) || (user && user.profile && user.profile.tel2) || null,
    phoneCode: (user && user.profile && user.profile.telCode) || (user && user.profile && user.profile.tel2Code) || '33'
  })

  this.fm = new FileManager(MessengerFiles, { condoId: FlowRouter.getParam('condo_id'), msgId: Template.currentData().currentConversation })

  let self = this
  $('body').on('click', function (e) {
    const currentConversationId = self.data.currentConversation
    if ($(e.target).hasClass("editConversationPicture") || $(e.target).hasClass("cancelDropdown")) {
      $('.dropdownConversationPicture').toggleClass('displayNone')
    } else if ($(e.target).hasClass("uploadPhoto")) {
      $('#inputAvatar').click();
      $('.dropdownConversationPicture').addClass('displayNone')
    } else if ($(e.target).hasClass("removePhoto")) {
      Meteor.call('setConversationPicture', currentConversationId, null)
    } else if ($(e.target).hasClass("conversation-header-item-pencil")) {
      $('.conversation-header-title').toggleClass('displayNone')
      self.oldConversationName.set($('.conversation-header-title-input').val())
      $('.conversation-header-title-input').focus()
    } else {
      $('.dropdownConversationPicture').addClass('displayNone')
    }
  })
})

Template.conversation_header.onRendered(function () {
})

Template.conversation_header.onDestroyed(function () {
  $('body').off('click')
})

Template.conversation_header.helpers({
  getUsersInformation: () => Template.currentData().usersInformation,
  getSelectedTab: () => Template.currentData().currentTab,
  getIsNewMessage: () => Template.currentData().isNewMessage,
  getCurrentConversation: () => Messages.findOne({ _id: Template.currentData().currentConversation }),
  getConversationInitial: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      if (conversation.picture) {
        let conversationFile = MessengerFiles.findOne({ _id: conversation.picture })
        if (!!conversationFile === true && conversationFile.isImage === true) {
          let file = conversationFile
          return { isPicture: !!file === true, src: file.link('avatar') }
        }
        return {}
      } else if (conversation.name && conversation.name !== '') {
        return { isPicture: false, initial: conversation.name[0] }
      } else {
        let target = DefaultRoles.findOne({ _id: conversation.target })
        if (((!!target && target.name === 'Occupant') || conversationUsers.length === 1) || (conversation.target === 'manager' && conversation.global === true)) {
          let firstUser = conversationUsers.find(user => (user || {}).isCreator)
          if (!firstUser) {
            firstUser = conversationUsers[0]
          }
          if (!!firstUser === true) {
            return { isPicture: !!firstUser.avatarURL === true, src: firstUser.avatarURL, initial: firstUser.initial }
          }
        } else {
          let allName = target.name.split(' ')
          let ret = ''
          for (let index = 0; index < allName.length; index++) {
            const element = allName[index]
            ret += element[0]
          }
          return { isPicture: false, initial: ret }
        }
      }
    }
    return { isPicture: false, initial: '?' }
  },
  getConversationName: (conversation, conversationUsers) => {
    if (conversation) {
      if (conversation.name && conversation.name !== '') {
        return conversation.name
      } else {
        let target = DefaultRoles.findOne({ _id: conversation.target })
        if (((!!target && target.name === 'Occupant') || conversationUsers.length === 1) || (conversation.target === 'manager' && conversation.global === true)) {
          let firstUser = conversationUsers.find(user => (user || {}).isCreator)
          if (!firstUser) {
            firstUser = conversationUsers[0]
          }
          if (!!firstUser === true) {
            return firstUser.fullname
          } else {
            return !!firstUser === true ? firstUser[0].fullname : '' // === deactivated_account
          }
        } else {
          return target.name
        }
      }
    }
  },
  getUserStatus: (conversation, conversationUsers) => {
    if (conversation) {
      if (conversation.picture || conversation.name) {
        return null
      } else {
        let target = DefaultRoles.findOne({ _id: conversation.target })
        if (((!!target && target.name === 'Occupant') || conversationUsers.length === 1) || (conversation.target === 'manager' && conversation.global === true)) {
          let firstUser = conversationUsers.find(user => (user || {}).isCreator)
          if (!firstUser) {
            firstUser = conversationUsers[0]
          }
          if (firstUser) {
            const meteorUser = Meteor.users.findOne({ _id: firstUser._id })
            if (!meteorUser) {
              return 'offline'
            } else if (meteorUser.status && meteorUser.status.idle) {
              return 'idle'
            } else {
              return 'online'
            }
          }
        }
      }
      return null
    }
  },
  getUsersOfConv: (conversation, conversationUsers) => {
    if (!!conversation === true) {
      let tmpConversationUsers = _.cloneDeep(conversationUsers)
      let users = []
      let target = DefaultRoles.findOne({ _id: conversation.target })
      if (!!target && conversation.target === 'manager') {
      } else if ((!!target && target.name === 'Occupant') || (conversation.target === 'manager' && conversation.global === true)) {
        if (!(conversation.name && conversation.name !== '')) {
          let firstUser = tmpConversationUsers.find(user => (user || {}).isCreator)
          if (!firstUser) {
            firstUser = tmpConversationUsers[0]
          }
          let indexOf = tmpConversationUsers.indexOf(firstUser)
          if (indexOf !== -1) {
            delete tmpConversationUsers[indexOf] // It's the conversation name /!\
          }
        }
        users = tmpConversationUsers.map(u => u.fullname)
      } else {
        tmpConversationUsers = tmpConversationUsers.filter(u => u.roleId !== conversation.target)
        users = tmpConversationUsers.map(u => {
          return u.fullname
        })
      }
      return _.without(users, undefined, null)
    }
  },
  isManagerTab: (tab) => !!tab === true && tab === 'manager',
  isNotPersonnalMessage: (conversation) => !!conversation === true && conversation.global !== true,
  incidentIsValid: (incidentId) => {
    let incident = ResidentIncidents.findOne({ _id: incidentId })
    return !!incident === true && incident.state && incident.state.status !== 0
  },
  getCurrentType: (conversation) => {
    if (!!conversation === true) {
      let incidentType = IncidentType.findOne({_id: conversation.type})
      let lang = FlowRouter.getParam('lang') || 'fr'
      if (!!incidentType === true && !!incidentType[`value-${lang}`] === true) {
        return incidentType[`value-${lang}`]
      } else {
        return conversation.type
      }
    }
  },
  getCurrentDetail: (conversation) => {
    if (!!conversation === true) {
      let incidentDetail = IncidentDetails.findOne({ _id: conversation.details })
      let declarationDetails = DeclarationDetails.findOne({ _id: conversation.details })
      let lang = FlowRouter.getParam('lang') || 'fr'
      if (!!incidentDetail === true && !!incidentDetail[`value-${lang}`] === true) {
        return incidentDetail[`value-${lang}`]
      } else if (!!declarationDetails === true && !!declarationDetails[`value-${lang}`] === true) {
        return declarationDetails[`value-${lang}`]
      } else {
        return conversation.details
      }
    }
  },
  resetSelect: (target) => {
    const oldTab = Template.instance().oldTab
    if (target !== oldTab.get()) {
      const defaultRoleId = FlowRouter.getParam('wildcard')
      const defaultRole = DefaultRoles.findOne({ _id: defaultRoleId })
      const user = Meteor.user()

      Template.instance().form.clear()
      Template.instance().form.setDefault({
        incidentType: null,
        incidentDetails: null,
        otherDetails: null,
        priority: null,
        description: null,
        area: null,
        phone: (user && user.profile && user.profile.tel) || (user && user.profile && user.profile.tel2) || null,
        phoneCode: (user && user.profile && user.profile.telCode) || (user && user.profile && user.profile.tel2Code) || '33'
      })

      if (!!defaultRole === true && defaultRole.name !== 'Occupant') {
        Template.instance().userForNewConversation.set([defaultRoleId])
      } else {
        let sessionNewUser = Session.get('newMessageUserId')
        delete Session.keys['newMessageUserId']

        Template.instance().userForNewConversation.set(sessionNewUser ? [sessionNewUser] : [])
      }
      oldTab.set(target)
    }
  },
  oldTab: () => Template.instance().oldTab.get(),
  getUserForNewConversation: () => Template.instance().userForNewConversation.get(),
  onSelectUser: () => {
    let onSelectUser = Template.currentData().onSelectUser
    let userForNewConversation = Template.instance().userForNewConversation
    return (userInConv) => {
      userForNewConversation.set(userInConv)
      if (!!onSelectUser === true && typeof onSelectUser === 'function') {
        onSelectUser(userForNewConversation.get())
      }
    }
  },
  getUserInBuilding: () => {
    const condoId = FlowRouter.getParam('condo_id')
    // const defaultRoleId = FlowRouter.getParam('wildcard')
    // const defaultRole = DefaultRoles.findOne({ _id: defaultRoleId })

    // const condoId = Template.currentData().condoId
    let inConvUsers = [Meteor.userId()]

    let userInCondo = UsersRights.find({ condoId: condoId, userId: { $nin: inConvUsers } }, { fields: { userId: true, defaultRoleId: true } }).fetch()
    userInCondo = userInCondo.sort((a, b) => a.defaultRoleId.localeCompare(b.defaultRoleId))
    const thisCondo = Condos.findOne({ _id: condoId }, { fields: { settings: true } })
    let availableRole = ['manager']
    if (thisCondo && thisCondo.settings && thisCondo.settings.options && thisCondo.settings.options.messenger && thisCondo.settings.options.messengerGestionnaire) {
      availableRole.push('occupant')
    }

    let hasManager = false
    let hasOccupant = false
    let hasRightOccupant = false
    let hasRightManager = false

    inConvUsers = UsersRights.find({condoId: condoId, userId: {$in: inConvUsers}}, { fields: { userId: true, defaultRoleId: true, for: true } }).fetch();

    // Check if there is any manager in the convo
    hasManager = (inConvUsers.filter((el) => {
      let role = DefaultRoles.findOne({_id: el.defaultRoleId})
      let isManager = (role.for === 'manager')
      return isManager
    }).length > 0)

    // Check if there is any occupant in the convo
    hasOccupant = (inConvUsers.filter((el) => {
      let role = DefaultRoles.findOne({_id: el.defaultRoleId})
      let isOccupant = (role.for === 'occupant')
      return !isOccupant
    }).length > 0)

    // Check if everybody as the right to speak to occupant
    hasRightManager = (inConvUsers.filter((el) => {
      let convoCheck = false
      let isManager  = DefaultRoles.findOne({_id: el.defaultRoleId}).for === 'manager'
      if (isManager) {
        convoCheck = hasManager &&
          Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, el.userId) &&
          Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, el.userId)
      } else {
        convoCheck = ((hasManager && Meteor.userHasRight('messenger', 'writeToManager', condoId, el.userId)) || !hasManager) &&
        ((hasOccupant && Meteor.userHasRight('messenger', 'writeToResident', condoId, el.userId)) || !hasOccupant)
      }
      return (convoCheck)
    }).length === inConvUsers.length)

    // Check if everybody as the right to speak to occupant
    hasRightOccupant = (inConvUsers.filter((el) => {
      return Meteor.userHasRight('messenger', 'writeToResident', condoId, el)
    }).length === inConvUsers.length)
    userInCondo = userInCondo.filter((user) => {
      let role = DefaultRoles.findOne({_id: user.defaultRoleId})
      let isManager = (role.for === 'manager') // Checking if current user is Manager
      let isOccupant = (role.for === 'occupant') // Checking if current user is Occupant

      // Checking the current member of the conversation rights
      let convoCheck = false
      if (isManager) {
        convoCheck = hasManager &&
          Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, user.userId) &&
          Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, user.userId)
      } else {
        convoCheck = ((hasManager && Meteor.userHasRight('messenger', 'writeToManager', condoId, user.userId)) || Meteor.userHasRight('messenger', 'writeToManager', condoId, user.userId)) &&
        ((hasOccupant && Meteor.userHasRight('messenger', 'writeToResident', condoId, user.userId)) || Meteor.userHasRight('messenger', 'writeToResident', condoId, user.userId))
      }

      // Checking the element in the select menu
      let selectCheck = ((isManager && hasRightManager) || hasRightManager) &&
          ((isOccupant && hasRightOccupant) || hasOccupant)
      return selectCheck && convoCheck
    })

    const blocked = [...(!!Meteor.user().blockedUsers? Meteor.user().blockedUsers: []), ...(!!Meteor.user().blockedBy? Meteor.user().blockedBy: [])]
    return _.without(userInCondo.map(user => {
      let userProfile = UsersProfile.findOne({ _id: user.userId, role: { $in: availableRole } })
      if (userProfile && !blocked.includes(user.userId)) {
        let role = DefaultRoles.findOne({ _id: user.defaultRoleId })
        // console.log(userProfile)
        return {
          id: user.userId,
          text: `${userProfile.firstname} ${userProfile.lastname}${role ? '_//_' + role.name : ''}`
        }
      }
    }), null, undefined)
  },
  getAllIncidentType: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const lang = FlowRouter.getParam('lang') || 'fr'
    let incidentType = IncidentType.find({ condoId: condoId })
    if (incidentType.count() === 0) {
      incidentType = IncidentType.find({ condoId: 'default' })
    }
    incidentType = incidentType.fetch().filter(type => {
      return !(type.defaultType === 'incident' && !(Meteor.userHasRight('incident', 'see', condoId, Meteor.userId()) && Meteor.userHasRight('incident', 'declare', condoId, Meteor.userId())))
    })
    return incidentType.map(type => ({
      _id: type._id,
      defaultType: type.defaultType === 'incident',
      picto: type.picto,
      text: type['value-' + lang]
    }))
  },
  getAllIncidentDetails: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const lang = FlowRouter.getParam('lang') || 'fr'
    let incidentDetail = IncidentDetails.find({ condoId: condoId })
    if (incidentDetail.count() === 0) {
      incidentDetail = IncidentDetails.find({ condoId: 'default' })
    }
    return incidentDetail.fetch().map(detail => ({
      _id: detail._id,
      defaultType: detail.defaultDetail,
      picto: detail.picto,
      text: detail['value-' + lang]
    }))
  },
  getAllIncidentPriority: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const lang = FlowRouter.getParam('lang') || 'fr'
    let incidentPriority = IncidentPriority.find({ condoId: condoId })
    if (incidentPriority.count() === 0) {
      incidentPriority = IncidentPriority.find({ condoId: 'default' })
    }
    return incidentPriority.fetch().map(priority => ({
      _id: priority._id,
      defaultPriority: priority.defaultPriority,
      picto: priority.picto,
      text: priority['value-' + lang]
    }))
  },
  getAllIncidentZone: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const lang = FlowRouter.getParam('lang') || 'fr'
    let incidentArea = IncidentArea.find({ condoId: condoId })
    if (incidentArea.count() === 0) {
      incidentArea = IncidentArea.find({ condoId: 'default' })
    }
    return incidentArea.fetch().map(area => ({
      _id: area._id,
      defaultType: area.defaultArea,
      picto: area.picto,
      text: area['value-' + lang]
    }))
  },
  getAllOtherDetails: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let listDropdown = ContactManagement.findOne({ condoId: condoId })
    if (!listDropdown) {
      let condoType = Condos.findOne(condoId).settings.condoType
      listDropdown = ContactManagement.findOne({ forCondoType: condoType })
    }
    if (listDropdown) {
      let incidentTypeId = Template.instance().form.get('incidentType')
      let listOfIds = _.find(listDropdown.messengerSet, function (list) {
        return list.id === incidentTypeId
      })
      if (listOfIds) {
        listOfIds = listOfIds.declarationDetailIds
        let declarationDetails = DeclarationDetails.find({ '_id': { $in: listOfIds } }).fetch()
        let lang = FlowRouter.getParam('lang')
        return _.map(declarationDetails, declarationDetail => ({
          id: declarationDetail._id,
          text: declarationDetail['value-' + lang]
        }))
      }
    }
  },
  onSelectOtherDetails: () => {
    const template = Template.instance()
    return () => selected => {
      template.form.set('otherDetails', selected)
      checkValue(template)
    }
  },
  getForm: (key) => {
    const template = Template.instance()
    return () => template.form.get(key)
  },
  onSelectPhoneCode: () => {
    const template = Template.instance()
    return () => selected => {
      let number = '+' + selected + template.form.get('phone')
      try {
        number = phoneUtil.parse(number, 'FR')
        if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
          template.form.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
          template.goodPhone = true
        }
      } catch (e) {
        template.goodPhone = false
      }
    }
  },
  onInputDetails: (key) => {
    const template = Template.instance()
    return () => value => {
      if (key === 'phone') {
        let number = '+' + template.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, 'FR')
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            template.form.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            template.goodPhone = true
          } else {
            template.form.set(key, !value ? null : value)
            template.goodPhone = false
          }
        } catch (e) {
          template.form.set(key, !value ? null : value)
          template.goodPhone = false
        }
      } else if (key === 'description') {
        template.form.set(key, !value && value.trim() === '' ? null : value.trim())
      }
      checkValue(template);
    }
  },
  isFieldError: (key) => {
    const template = Template.instance()
    switch (key) {
      case 'phone': {
        const value = template.form.get(key)
        let number = '+' + template.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, 'FR')
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          template.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          template.goodPhone = false
          return value !== null && value !== ''
        }
      }
    }
  },
  checkDisplayIncidentDetails: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        return incidentType.defaultType === 'incident'
      }
    }
    return false
  },
  checkDisplayPriority: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        if (incidentType.defaultType === 'incident') {
          return !!form.incidentDetails
        } else {
          return true
        }
      }
    }
    return false
  },
  checkDisplayArea: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        if (incidentType.defaultType === 'incident') {
          return !!form.priority === true
        }
      }
    }
    return false
  },
  checkDisplayInformation: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        if (incidentType.defaultType === 'incident') {
          return !!form.area === true
        } else {
          return !!form.priority === true
        }
      }
    }
    return false
  },
  checkDisplayPhone: () => {
    let user = Meteor.user()
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        if (incidentType.defaultType === 'incident') {
          return !!((user && user.profile && user.profile.tel) || (user && user.profile && user.profile.tel2)) === false
        }
      }
    }
  },
  checkDisplayOtherDetails: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        if (incidentType.defaultType !== 'incident') {
          return !!form.priority === true
        }
      }
    }
    return false
  },
  checkDisplayDescription: () => {
    let form = Template.instance().form.all()
    let condoId = FlowRouter.getParam('condo_id')
    if (!!form === true && !!form.incidentType === true && !!form.priority === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (incidentType.defaultType === 'incident') {
        return !!form.area === true
      }
    }
    return false
  },
  canEditConversation: (target, users) => {
    return target && target !== 'manager' && users && users.length > 1
  },
  goToIncident: (incidentId) => {
    return () => () => {
      const lang = FlowRouter.getParam('lang')
      const condo_id = FlowRouter.getParam('condo_id')
      const params = {
        lang,
        condo_id,
        incidentId
      }
      FlowRouter.go('app.board.resident.condo.module.incident', params)
    }
  },
  isBlockedUser: () => {
    const instance = Template.instance()
    const conversationId = instance.data.currentConversation
    const conversation = Messages.findOne({ _id: conversationId })

    if (!!conversation && !!conversation.participants && conversation.participants.length === 2) {
      const otherUserId = _.find(conversation.participants, (p) => p !== Meteor.user()._id)

      // if the receiver is blocked
      const blockedUsers = Meteor.user().blockedUsers
      if (!!blockedUsers && !!blockedUsers.includes(otherUserId)) {
        return true
      }

      // if the sender is blocked
      const blockedBy = Meteor.user().blockedBy
      if (!!blockedBy && !!blockedBy.includes(otherUserId)) {
        return true
      }
    }

    return false
  },
  checkBlockedUsers: () => {
    const instance = Template.instance()
    const currentConversation = Messages.findOne({ _id: instance.data.currentConversation })
    if (!!currentConversation && instance.blockedAlertPopUpShown.get() !== instance.data.currentConversation) {
      instance.blockedAlertPopUpShown.set(instance.data.currentConversation)
      const blockedUsers = Meteor.user().blockedUsers
      if (!!blockedUsers && blockedUsers.some(r => currentConversation.participants.includes(r))) {
        const translation_ = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
        bootbox.alert({
          size: "large",
          title: translation_.commonTranslation.careful,
          message: translation_.commonTranslation.blocked_user_exist,
          backdrop: true
        });
      }
    }
  }

})

Template.conversation_header.events({
  // 'click .editConversationPicture, click .cancelDropdown' (event, template) {
  //   template.$('.dropdownConversationPicture').toggleClass('displayNone')
  // },
  // 'click .uploadPhoto' (event, template) {
  //   template.$('#inputAvatar').click()
  // },
  // 'click .removePhoto' (event, template) {
  //   const currentConversationId = template.data.currentConversation
  //   Meteor.call('setConversationPicture', currentConversationId, null)
  // },
  'change #inputAvatar' (event, template) {
    const currentConversationId = template.data.currentConversation

    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (event.currentTarget.files && event.currentTarget.files.length === 1 && mimeType(event.currentTarget.files[0].type) === 'image') {
      template.fm.insert(event.currentTarget.files[0], function (err, file) {
        event.currentTarget.value = ''
        if (!err && file) {
          template.fm.clearFiles()
          Meteor.call('setConversationPicture', currentConversationId, file._id)
        }
      })
    } else {
      sAlert.error(tr_common.commonTranslation['image_only'])
    }
  },
  'click .conversation-header-item-pencil' (event, template) {
    template.$('.conversation-header-title').toggleClass('displayNone')
    template.oldConversationName.set(template.$('.conversation-header-title-input').val())
    template.$('.conversation-header-title-input').focus()
  },
  'blur .conversation-header-title-input' (event, template) {
    template.$('.conversation-header-title-input').val(template.oldConversationName.get())
    template.$('.conversation-header-title').toggleClass('displayNone')
  },
  'keydown .conversation-header-title-input' (event, template) {
    if (event.keyCode === 13) {
      event.preventDefault()
      const currentConversationId = template.data.currentConversation
      const oldConversationName = template.oldConversationName.get()
      const newConversationName = event.currentTarget.value

      if (oldConversationName !== newConversationName) {
        if (!!newConversationName === false) {
          Meteor.call('setConversationName', currentConversationId, null)
        } else {
          Meteor.call('setConversationName', currentConversationId, newConversationName)
        }
      }
      template.$('.conversation-header-title-input').blur()
    }
  },
  'click .block-user' (event, template) {
    const currentConversation = Messages.findOne({ _id: template.data.currentConversation })
    if (!!currentConversation) {
      const blockedUser = _.find(currentConversation.participants, (p) => p !== Meteor.user()._id)
      if (!!blockedUser) {
        const translation_ = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
        bootbox.confirm({
          title: translation_.commonTranslation.block_user,
          message: translation_.commonTranslation.block_user_confirm,
          buttons: {
            confirm: {label: translation_.commonTranslation.yes, className: "btn-red-confirm"},
            cancel: {label: translation_.commonTranslation.no, className: "btn-outline-red-confirm"}
          },
          callback: function(res) {
            if (res) {
              Meteor.call('blockUser', blockedUser, () => {
                sAlert.success(translation_.commonTranslation.block_user_success);
              });
            }
          }
        });
      }
    }
  },
  'click .incident-declaration-header-bubble' (event, template) {
    let target = event.currentTarget.getAttribute('data')
    let idx = event.currentTarget.getAttribute('idx')
    let oldIdx = template.form.get(target)
    if (target === 'incident' || target === 'other') {
      oldIdx = template.form.get('incidentType')
    }
    if (oldIdx !== idx) {
      switch (target) {
        case 'incident':
        case 'other':
          template.form.set('incidentType', idx)
          if (oldIdx !== idx) {
            template.$('.incident-declaration-header-bubble[data=incident]').removeClass('selected')
            template.$('.incident-declaration-header-bubble[data=other]').removeClass('selected')
            template.$('.incident-declaration-header-bubble[data=incidentDetails]').removeClass('selected')
            template.$('.incident-declaration-header-bubble[data=priority]').removeClass('selected')
            template.$('.incident-declaration-header-bubble[data=area]').removeClass('selected')
            template.form.set('incidentDetails', null)
            template.form.set('otherDetails', null)
            template.form.set('priority', null)
            template.form.set('area', null)
            template.form.set('description', null)
          }
          break
        case 'incidentDetails':
          template.form.set('incidentDetails', idx)
          if (oldIdx !== idx) {
            template.$('.incident-declaration-header-bubble[data=priority]').removeClass('selected')
            template.form.set('priority', null)
            template.form.set('area', null)
            template.form.set('description', null)
          }
          break
        case 'priority':
          template.form.set('priority', idx)
          if (oldIdx !== idx) {
            template.$('.incident-declaration-header-bubble[data=area]').removeClass('selected')
            template.form.set('area', null)
            template.form.set('description', null)
          }
          break
        case 'area':
          template.form.set('area', idx)
          template.form.set('description', null)
          break
      }
      template.$('.incident-declaration-header-bubble[data=' + target + ']').removeClass('selected')
      template.$(event.currentTarget).addClass('selected')
      template.$('.incident-declaration-header-wrap').stop().animate({
        scrollTop: template.$('.incident-declaration-header-wrap')[0].scrollHeight
      }, 800)
      checkValue(template)
    }
  }
})

function checkValue (template) {
  if (!!template === true) {
    const condoId = FlowRouter.getParam('condo_id')
    const form = template.form.all()
    const setInformationForNewIncident = template.data.setInformationForNewIncident

    const checkIncidentType = !!form.incidentType === true
    const checkIncidentDetails = !!form.incidentDetails === true
    const checkArea = !!form.area === true
    const checkPriority = !!form.priority === true
    const checkOtherDetails = !!form.otherDetails === true
    const checkPhone = !!form.phone === true
    const checkPhoneCode = !!form.phoneCode === true
    const checkdescription = !!form.description === true

    let isAnIncident = null
    if (!!form === true && !!form.incidentType === true) {
      let incidentType = IncidentType.findOne({ _id: form.incidentType, $or: [{ condoId: condoId }, { condoId: 'default' }] })
      if (!!incidentType === true) {
        isAnIncident = incidentType.defaultType === 'incident'
      }
    }
    if (checkIncidentType && isAnIncident !== null) {
      if (isAnIncident === true && checkIncidentDetails && checkArea && checkPriority && checkPhone && checkPhoneCode && checkdescription) {
        // It's an incident valid
        if (!!setInformationForNewIncident && typeof setInformationForNewIncident === 'function') {
          setInformationForNewIncident({...form, isAnIncident})
        }
      } else if (isAnIncident === false && checkPriority && checkOtherDetails) {
        // It's an other incident valid
        if (!!setInformationForNewIncident && typeof setInformationForNewIncident === 'function') {
          setInformationForNewIncident({...form, isAnIncident})
        }
      } else {
        if (!!setInformationForNewIncident && typeof setInformationForNewIncident === 'function') {
          setInformationForNewIncident(null)
        }
      }
    }
  } else {
    console.log('Error: template is undefined in `checkValue`')
  }
}

function checkMessengerSettings (userId, condoId) {
  let resident = ResidentUsers.findOne({ _id: userId })
  if (resident) {
    resident = resident.resident
    let condo = resident.condos && resident.condos.find(c => c.condoId === condoId)
    if (condo) {
      return condo.preferences.messagerie
    }
  }
  return false
}

function checkRight (userId, condoId, defaultRoleName) {
  let call = null
  switch (defaultRoleName) {
    case 'Occupant':
      call = 'writeToResident'
      break
    case 'Conseil Syndical':
      call = 'writeToUnionCouncil'
      break
    case 'Garrdien':
      call = 'writeToSupervisors'
      break
  }
  return Meteor.userHasRight('messenger', call, condoId, userId) || false
}
