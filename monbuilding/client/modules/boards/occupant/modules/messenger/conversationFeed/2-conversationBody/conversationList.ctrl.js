import { ModuleMessagerieIndex } from '/common/lang/lang.js'
import { ReactiveVar } from 'meteor/reactive-var'
import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import moment from 'moment'

/**
* Created by vmariot on 30/04/18.
*/

Template.messenger_conversation_list.onCreated(function () {
  this.elemToDisplay = new ReactiveVar(20)
  this.userConnect = new ReactiveVar([])
  this.conversationId = null
  this.conversation = []
})

Template.messenger_conversation_list.onRendered(function () {
})

// Warning:
//   For Messenger, I use an array in 3D :
//     messengerArrray = [GroupOfDay][GroupOfUser][GroupOfMessage]

Template.messenger_conversation_list.helpers({
  updateData: () => {
    Template.instance().conversationId = Template.currentData().currentConversation
    Template.instance().subscribe('messengerFeed', Template.instance().conversationId, Template.instance().elemToDisplay.get())
    Template.instance().subscribe('messagerieFiles', Template.instance().conversationId)
  },
  getSelectedTab: () => Template.currentData().currentTab,
  getUsersInformation: () => Template.currentData().usersInformation,
  getCurrentConversation: () => Template.currentData().currentConversation,
  isSubscribeReady: () => Template.instance().subscriptionsReady(),
  getMessageFeed: () => {
    const msgId = Template.instance().conversationId
    const messenger = Messages.findOne({ _id: msgId })
    const limit = Template.instance().elemToDisplay.get()
    if (!!msgId && !!messenger) {
      const messages = MessagesFeed.find({
        messageId: msgId
      }, { limit: limit, sort: { date: -1 } }).fetch()
      let conversation = []
      conversation[0] = []
      conversation[0][0] = []
      let oldUserId = null
      let oldDate = null
      let indexOfDay = 0
      let indexOfUser = 0
      for (let index = 0; index < messages.length; index++) {
        const element = messages[index]
        let userId = element.userId
        if (!(element.isNewUserMessage && !UsersProfile.findOne({ _id: userId }))) {
          if (!!element.isNewUserMessage) {
            userId = 'newUser-' + userId
          }
          const date = element.date
          if (oldDate === null) oldDate = date
          if (moment(oldDate).local().format('DD/MM/YYYY') === moment(date).local().format('DD/MM/YYYY')) {
            if (oldUserId === null) {
              conversation[indexOfDay][indexOfUser] = [element]
              oldUserId = userId
            } else if (oldUserId === userId) {
              conversation[indexOfDay][indexOfUser].push(element)
              oldUserId = userId
            } else {
              indexOfUser++
              conversation[indexOfDay][indexOfUser] = [element]
              oldUserId = userId
            }
            oldDate = date
          } else if (oldDate !== date) {
            indexOfDay++
            indexOfUser = 0
            conversation[indexOfDay] = []
            conversation[indexOfDay][indexOfUser] = [element]
            oldUserId = userId
            oldDate = date
          }
        }
      }
      // Incident message:
      // if (!!messenger.incidentId && conversation[0] && conversation[0][0] && !!conversation[0][0].length) {
      //   const indexOfDayMax = conversation.length - 1
      //   const indexOfUserMax = conversation[indexOfDayMax].length - 1
      //   const indexOfMessageMax = conversation[indexOfDayMax][indexOfUserMax].length - 1

      //   const firstMessageSave = conversation[indexOfDayMax][indexOfUserMax][indexOfMessageMax]

      //   const incidentMessage = {
      //     messageId: msgId,
      //     date: firstMessageSave.date,
      //     isMessageGoToIncident: true,
      //     incidentId: messenger.incidentId,
      //     userId: 'msgIncident'
      //   }
      //   conversation[indexOfDayMax][indexOfUserMax + 1] = [incidentMessage]
      //   conversation[indexOfDayMax][indexOfUserMax + 2] = [firstMessageSave]

      //   if (indexOfMessageMax === 0) {
      //     const res = conversation[indexOfDayMax].slice(1)
      //     conversation[indexOfDayMax] = _.without(res, null, undefined, [])
      //   } else {
      //     conversation[indexOfDayMax][indexOfUserMax].splice(indexOfMessageMax, 1)
      //   }
      // }
      return conversation
    }
  },
  showDate: (elementsOfDay) => {
    if (!!elementsOfDay === true && Array.isArray(elementsOfDay) && !!elementsOfDay[0] === true && !!elementsOfDay[0][0] === true) {
      let date = elementsOfDay[0][0].date
      let lang = FlowRouter.getParam('lang') || 'fr'
      let translate = new ModuleMessagerieIndex(lang)
      if (moment(date).local().format('L') === moment().local().format('L')) {
        return translate.moduleMessagerieIndex['today']
      } else if (moment().local().diff(moment(date).local(), 'days') === 1) {
        return translate.moduleMessagerieIndex['yesterday']
      } else {
        return moment(date).local().format('LL')
      }
    }
  },
  scrollBottom: () => {
    setTimeout(() => {
      const chatBody = $('.conversation-body')
      if (chatBody && chatBody[0]) {
        chatBody.stop().animate({
          scrollTop: chatBody[0].scrollHeight
        }, 800)
      }
    }, 2000)
  },
  getSetMessageIdForEditing: () => Template.currentData().setMessageIdForEditing,
  isManagerTab: () => Template.currentData().currentTab === 'manager',
  setUsersStatus: async function () {
    const t = Template.instance()
    const conversationId = Template.currentData().currentConversation
    if (!conversationId) return
    const userConnect = await new Promise((resolve, reject) => {
      Meteor.call('getUserStatusInConversation', conversationId, (error, result) => {
        if (error) {
          reject()
        } else {
          resolve(result)
        }
      })
    })
    t.userConnect.set(userConnect)
  },
  getUsersStatus: () => {
    return Template.instance().userConnect.get()
  }
})

Template.messenger_conversation_list.events({
  'scroll .conversation-body': function (event, template) {
    if (template.$(event.currentTarget).scrollTop() === 0) {
      if (template.elemToDisplay.get() < 200) {
        template.elemToDisplay.set(template.elemToDisplay.get() + 25)
      } else if (template.elemToDisplay.get() < 500) {
        template.elemToDisplay.set(template.elemToDisplay.get() + 50)
      } else {
        template.elemToDisplay.set(template.elemToDisplay.get() + 75)
      }
    }
  }
})
