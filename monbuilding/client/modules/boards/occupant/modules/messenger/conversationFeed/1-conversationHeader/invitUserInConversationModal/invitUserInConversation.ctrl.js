import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
* Created by vmariot on 30/04/18.
*/

Template.invit_user_in_conversation.onCreated(function () {
  this.selectedUser = new ReactiveVar([])
  this.modalIsOpen = new ReactiveVar(false)
})

Template.invit_user_in_conversation.onRendered(function () {
})

Template.invit_user_in_conversation.helpers({
  modalIsOpen: () => Template.instance().modalIsOpen.get(),
  getCurrentConversation: () => Template.currentData().currentConversation,
  getSelected: () => Template.instance().selectedUser.get(),
  setSelected: () => {
    let template = Template.instance()
    return newUser => {
      template.selectedUser.set(newUser)
    }
  },
  getOptions: (conversation) => {
    if (conversation) {
      const condoId = conversation.condoId
      let inConvUsers = _.uniq(_.pluck(conversation.users, 'userId'))
      let userInCondo = UsersRights.find({ condoId: condoId, userId: { $nin: inConvUsers } }, { fields: { userId: true, defaultRoleId: true } }).fetch()
      userInCondo = userInCondo.sort((a, b) => a.defaultRoleId.localeCompare(b.defaultRoleId))
      const thisCondo = Condos.findOne({ _id: condoId }, { fields: { settings: true } })
      let availableRole = ['manager']
      if (thisCondo && thisCondo.settings && thisCondo.settings.options && thisCondo.settings.options.messenger && thisCondo.settings.options.messengerGestionnaire) {
        availableRole.push('occupant')
      }

      let hasManager = false
      let hasOccupant = false
      let hasRightOccupant = false
      let hasRightManager = false

      inConvUsers = UsersRights.find({condoId: condoId, userId: {$in: inConvUsers}}, { fields: { userId: true, defaultRoleId: true, for: true } }).fetch();

      // Check if there is any manager in the convo
      hasManager = (inConvUsers.filter((el) => {
        let role = DefaultRoles.findOne({_id: el.defaultRoleId})
        let isManager = (role.for === 'manager')
        return isManager
      }).length > 0)

      // Check if there is any occupant in the convo
      hasOccupant = (inConvUsers.filter((el) => {
        let role = DefaultRoles.findOne({_id: el.defaultRoleId})
        let isOccupant = (role.for === 'occupant')
        return !isOccupant
      }).length > 0)

      // Check if everybody as the right to speak to occupant
      hasRightManager = (inConvUsers.filter((el) => {
        let convoCheck = false
        let isManager  = DefaultRoles.findOne({_id: el.defaultRoleId}).for === 'manager'
        if (isManager) {
          convoCheck = hasManager &&
            Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, el.userId) &&
            Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, el.userId)
        } else {
          convoCheck = ((hasManager && Meteor.userHasRight('messenger', 'writeToManager', condoId, el.userId)) || !hasManager) &&
          ((hasOccupant && Meteor.userHasRight('messenger', 'writeToResident', condoId, el.userId)) || !hasOccupant)
        }
        return (convoCheck)
      }).length === inConvUsers.length)

      // Check if everybody as the right to speak to occupant
      hasRightOccupant = (inConvUsers.filter((el) => {
        return Meteor.userHasRight('messenger', 'writeToResident', condoId, el)
      }).length === inConvUsers.length)
      userInCondo = userInCondo.filter((user) => {
        let role = DefaultRoles.findOne({_id: user.defaultRoleId})
        let isManager = (role.for === 'manager') // Checking if current user is Manager
        let isOccupant = (role.for === 'occupant') // Checking if current user is Occupant

        // Checking the current member of the conversation rights
        let convoCheck = false
        if (isManager) {
          convoCheck = hasManager &&
            Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, user.userId) &&
            Meteor.userHasRight('messenger', 'writeAllMsgEnterprise', condoId, user.userId)
        } else {
          let ra = Meteor.userHasRight('messenger', 'writeToManager', condoId, user.userId)
          let rb = Meteor.userHasRight('messenger', 'writeToResident', condoId, user.userId)
          convoCheck = ((ra && hasManager) || ra) && ((rb && hasOccupant) || rb)
        }

        // Checking the element in the select menu
        let selectCheck = ((isManager && hasRightManager) || hasRightManager) && 
            ((isOccupant && hasRightOccupant) || hasOccupant)
        return selectCheck && convoCheck
      })

      return _.without(userInCondo.map(user => {
        let userProfile = UsersProfile.findOne({ _id: user.userId, role: { $in: availableRole } })
        if (userProfile) {
          let role = DefaultRoles.findOne({ _id: user.defaultRoleId })
          // console.log(userProfile)
          return {
            id: user.userId,
            text: `${userProfile.firstname} ${userProfile.lastname}${role ? '_//_' + role.name : ''}`
          }
        }
      }), null, undefined)
    }
  },
  checkDisable: () => !(Template.instance().selectedUser.get().length > 0)
})

Template.invit_user_in_conversation.events({
  'hidden.bs.modal #invitUserInConv': function (event, template) {
    if (template.modalIsOpen.get() === true) {
      template.modalIsOpen.set(false)
      Meteor.setTimeout(function () {
        template.selectedUser.set([])
      }, 100)
    }
    history.replaceState('', document.title, window.location.pathname)
  },
  'show.bs.modal #invitUserInConv': function (event, template) {
    if (template.modalIsOpen.get() === false) {
      template.modalIsOpen.set(true)
    }
    let modal = $(event.currentTarget)
    window.location.hash = '#invitUserInConv'
    window.onhashchange = function () {
      if (location.hash !== '#invitUserInConv') {
        modal.modal('hide')
      }
    }
  },
  'click #validInvit': function (event, template) {
    let translation = new CommonTranslation((FlowRouter.getParam('lang') || 'fr'))
    let participant = Template.instance().selectedUser.get()
    let msgId = Template.currentData().currentConversation._id
    if (participant && participant.length > 0) {
      Meteor.call('inviteUserToConversation', msgId, participant, (error, result) => {
        if (!error) {
          sAlert.success(translation.commonTranslation['invitation_success'])
        } else {
          sAlert.error(error)
        }
      });
    }
  }
})

function getMessengerSettings (userId, condoId) {
  // For occupant: He have only him profile in Residents
  let resident = ResidentUsers.findOne({ _id: userId })
  if (resident) {
    resident = resident.resident
    let condo = resident.condos && resident.condos.find(c => c.condoId === condoId)
    if (condo) {
      return condo.preferences.messagerie
    }
  }
  return false
}
