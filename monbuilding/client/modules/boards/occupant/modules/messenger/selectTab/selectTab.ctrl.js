import { Template } from 'meteor/templating'

Template.messenger_select_tab.onCreated(function () {

})

Template.messenger_select_tab.onRendered(function () {

})

Template.messenger_select_tab.helpers({
  getDefaultRoles: () => Template.currentData().getDefaultRole,
  getSelectedTab: () => Template.currentData().currentTab,
  userHasRightMessage: (currentTab) => {
    let call = null
    const condoId = FlowRouter.getParam('condo_id')

    if (Meteor.isManager) {
      return true
    } else {
      if (currentTab === 'manager') {
        call = 'writeToManager'
      } else {
        const defaultRole = DefaultRoles.findOne({ _id: currentTab }) || {}
        switch (defaultRole.name) {
          case 'Occupant':
            call = 'writeToResident'
            break
          case 'Conseil Syndical':
            call = 'writeToUnionCouncil'
            break
          case 'Gardien':
            call = 'writeToSupervisors'
            break
        }
      }
    }
    return Meteor.userHasRight('messenger', call, condoId)
  },
})

Template.messenger_select_tab.events({
  'click .tabBarContainer > div': function (event, template) {
    let selectingTab = template.data.selectingTab
    let newTab = event.currentTarget.getAttribute('data-tab')
    if (selectingTab && typeof selectingTab === 'function') {
      selectingTab(newTab)
    }
  }
})
