import { CommonTranslation } from "/common/lang/lang.js";

/**
* Created by vmariot on 05/04/18.
*/

/*
{{>MbEmptyMessage "}}
*/
Template.MbEmptyMessage.onCreated(function () {
});

Template.MbEmptyMessage.helpers({
  dateEmptyMessage: () => {
    let resident = Residents.findOne()
    if (resident) {
      let condo = resident.condos.find((c) => c.condoId === Template.currentData().condoId)
      if (condo) {
        return moment(condo.joined).format('DD/MM/YY')
      }
    }
  },
  currentTab: () => {
    let currentTab = Template.currentData().currentTab || FlowRouter.getParam('tab')
    if (currentTab === 'manager') {
      let lang = FlowRouter.getParam('lang') || 'fr'
      let translation = new CommonTranslation(lang)
      return translation.commonTranslation['manager']
    } else if (currentTab === 'global') {
      let lang = FlowRouter.getParam('lang') || 'fr'
      let translation = new CommonTranslation(lang)
      return translation.commonTranslation['messenger_personnal']
    }
    let defaultRole = DefaultRoles.findOne({ _id: currentTab })
    if (defaultRole) {
      return defaultRole.name
    }
  },
  isCurrentId: () => {
    return Template.currentData().currentConversation === 'MbEmptyMessage'
  },
  condoId: () => {
    return Template.currentData().condoId
  }
})

Template.MbEmptyMessage.events({
  'click .messenger-item': function (event, template) {
    let selectConversationCb = template.data.selectConversationCb
    let msgId = event.currentTarget.getAttribute('msgid')
    if (selectConversationCb && typeof selectConversationCb === 'function' && !!msgId) {
      selectConversationCb(msgId)
    }
  }
});
