import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

/**
* Created by vmariot on 27/04/18.
*/

Template.messenger_contact_list_item.onCreated(function () {

})

Template.messenger_contact_list_item.onRendered(function () {
})

Template.messenger_contact_list_item.helpers({
  getContactCardInfo: () => Template.currentData().conversation,
  getCurrentConversation: () => Template.currentData().currentConversation,
  getConversationInitial: (conversation) => {
    if (!!conversation === true) {
      if (conversation.picture) {
        let conversationFile = MessengerFiles.findOne({ _id: conversation.picture })
        if (!!conversationFile === true && conversationFile.isImage === true) {
          let file = conversationFile
          return { isPicture: !!file === true, src: file.link('avatar') }
        }
        return {}
      } else if (conversation.name && conversation.name !== '') {
        return { isPicture: false, initial: conversation.name[0] }
      } else {
        let target = DefaultRoles.findOne({ _id: conversation.target })
        if ((!!target && target.name === 'Occupant') || conversation.users.length === 2) {
          let user = conversation.users.find(user => {
            return user.userId !== Meteor.userId() && !!UsersProfile.findOne({ _id: (user || {}).userId }) === true
          })
          let profile = UsersProfile.findOne({ _id: (user || {}).userId })
          if (!!profile === true) {
            let profilePicture = Avatars.findOne({ _id: profile._id })
            if (profilePicture) {
              return { isPicture: true, src: profilePicture.avatar.avatar }
            } else {
              return { isPicture: false, initial: profile.firstname[0] + profile.lastname[0] }
            }
          }
        } else {
          if (!target && conversation.target === 'manager') {
            const lang = FlowRouter.getParam('lang')
            target = { name: lang === 'en' ? 'B M' : 'G E' }
          }
          let allName = target.name.split(' ')
          let ret = ''
          for (let index = 0; index < allName.length; index++) {
            const element = allName[index]
            ret += element[0]
          }
          return { isPicture: false, initial: ret }
        }
      }
    }
    return { isPicture: false, initial: '?' }
  },
  getConversationName: (conversation) => {
    const lang = FlowRouter.getParam('lang')
    const translation = new CommonTranslation(lang)
    if (conversation && conversation.name) {
      return conversation.name
    } else if (conversation) {
      let currentRoleName = ''
      let defaultRolesId = null
      const currentRole = DefaultRoles.findOne({ _id: conversation.target  }) || {}

      if (currentRole && currentRole.name !== 'Occupant' && conversation.target !== 'manager') {
        currentRoleName = `${currentRole.name}, `
        defaultRolesId = conversation.target
      } else if (conversation.target === 'manager' && !!conversation.incidentId === false) {
        currentRoleName = ''
      } else if (conversation.target === 'manager' && !!conversation.incidentId === true) {
        return translation.commonTranslation['manager']
      }

      let user = conversation.users
      if (!!user === true && Array.isArray(user) && conversation.target !== 'manager') {
        const displayAllName = user.length < 4
        let title = user.reduce((title, current) => {
          if (current.userId !== Meteor.userId() && !UsersRights.findOne({ userId: current.userId, defaultRoleId: defaultRolesId })) {
            let lastUser = UsersProfile.findOne({ _id: current.userId })
            let userName = ''
            if (!!lastUser === true) {
              if (!displayAllName) {
                userName = `${lastUser.firstname[0]}. ${lastUser.lastname}, `
              } else {
                userName = `${lastUser.firstname} ${lastUser.lastname}, `
              }
            } else {
              userName = `${translation.commonTranslation['deactivated_account']}, `
            }
            return title + userName
          } else {
            return title
          }
        }, currentRoleName)
        title = title.slice(0, -2)

        return title
      } else if (!!user === true && Array.isArray(user) && conversation.target === 'manager') {
        let occupantDefaultRolesId = DefaultRoles.find({ 'for': 'occupant' }).fetch()
        user = user.filter(u => !!UsersRights.findOne({ userId: u.userId, defaultRoleId: { $nin: occupantDefaultRolesId } }) === false)
        const displayAllName = user.length < 4
        let title = user.reduce((title, current) => {
          if (current.userId !== Meteor.userId()) {
            let lastUser = UsersProfile.findOne({ _id: current.userId })
            let userName = ''
            if (!!lastUser === true) {
              if (!displayAllName) {
                userName = `${lastUser.lastname[0]}. ${lastUser.firstname}, `
              } else {
                userName = `${lastUser.firstname} ${lastUser.lastname}, `
              }
            }
            if (userName) {
              return title + userName
            } else {
              return title
            }
          } else {
            return title
          }
        }, currentRoleName)
        title = title.slice(0, -2)

        return title
      }
    }
    return translation.commonTranslation['deactivated_account']
  },
  getConversationLastMessage: (userId, msg) => {
    let lastUser = UsersProfile.findOne({ _id: userId })
    let lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new CommonTranslation(lang)
    let userName = translation.commonTranslation['deactivated_account']
    if (!!lastUser === true) {
      userName = `${lastUser.firstname} ${lastUser.lastname[0]}.`
    }
    if (msg && msg !== '') {
      return `${userName} : ${msg}`
    } else {
      return `${userName} : ${translation.commonTranslation['attached_file_available']}`
    }
  },
  conversationHasNewElement: (conversation) => {
    let user = conversation && conversation.users.find(user => user.userId === Meteor.userId())
    let lastConnect = user.lastVisit
    if (!lastConnect) {
      return true
    } else {
      return conversation.lastMsg > lastConnect
    }
  },
  getUserStatus: (conversation) => {
    if (conversation) {
      if (conversation.picture || conversation.name) {
        return null
      } else {
        const target = DefaultRoles.findOne({ _id: conversation.target })
        if ((!!target && target.name === 'Occupant') || conversation.users.length === 2) {
          const user = conversation.users.find(user => {
            return user.userId !== Meteor.userId() && !!UsersProfile.findOne({ _id: (user || {}).userId }) === true
          })
          if (user) {
            const meteorUser = Meteor.users.findOne({ _id: user.userId })
            if (!meteorUser) {
              return 'offline'
            } else if (meteorUser.status && meteorUser.status.idle) {
              return 'idle'
            } else {
              return 'online'
            }
          }
        }
      }
    }
  }
})

Template.messenger_contact_list_item.events({
  'click .messenger-item': function (event, template) {
    let selectConversationCb = template.data.selectConversationCb
    let msgId = event.currentTarget.getAttribute('msgid')
    if (selectConversationCb && typeof selectConversationCb === 'function' && !!msgId) {
      Meteor.call('updateLastVisit', msgId)
      selectConversationCb(msgId)
    }
  }
})
