import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.messenger_contact_list.onCreated(function () {
  this.searchText = new ReactiveVar('')
})

Template.messenger_contact_list.onRendered(function () {
})

Template.messenger_contact_list.helpers({
  onSearch: () => {
    let template = Template.instance()
    return (searchText) => {
      template.searchText.set(searchText)
    }
  },
  searchIsActive: () => Template.instance().searchText.get() !== '',
  getSelectedTab: () => Template.currentData().currentTab,
  getCurrentConversation: () => Template.currentData().currentConversation,
  getSelectConversationCb: () => Template.currentData().selectConversationCb,
  getContact: (currentTab) => {
    let condoId = FlowRouter.getParam('condo_id')
    let conversation = Messages.find({ target: currentTab, condoId: condoId }, { sort: { lastMsg: -1 } }).fetch()
    let searchText = Template.instance().searchText.get()
    if (!!searchText === true) {
      let regexp = new RegExp(searchText, 'i');
      conversation = conversation.filter(item => {
        let founded = false
        for (let j = item.users.length - 1; j >= 0; j--) {
          if (item.users[j].userId !== Meteor.userId()) {
            let user = UsersProfile.findOne({ _id: item.users[j].userId })
            if (user) {
              if (regexp.test(`${user.firstname} ${user.lastname}`)) {
                founded = true
              }
            }
          }
        }
        return founded
      })
    }
    return conversation
  },
  condoId: () => {
    return FlowRouter.getParam('condo_id')
  }
})

Template.messenger_contact_list.events({
})
