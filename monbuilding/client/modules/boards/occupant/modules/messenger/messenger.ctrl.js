/* globals Messages */
/* globals DefaultRoles */

import { CommonTranslation } from '/common/lang/lang.js'
import { Template } from 'meteor/templating'
import { ReactiveVar } from 'meteor/reactive-var'
import { Meteor } from 'meteor/meteor'
import { FlowRouter } from 'meteor/kadira:flow-router'

Template.messenger.onCreated(function () {
  let condoId = FlowRouter.getParam('condo_id')
  let wildcard = FlowRouter.getParam('wildcard')
  this.subscribe('messagerie')
  this.subscribe('usersProfile')
  this.subscribe('module_incidents_resident', condoId)
  let isNewMessage = !!wildcard === true && wildcard.split('/')[1] === 'new'

  this.currentTab = new ReactiveVar(null)
  this.currentConversation = new ReactiveVar(null)
  this.isNewMessage = new ReactiveVar(isNewMessage)
})

Template.messenger.onRendered(function () {

})

Template.messenger.helpers({
  refreshAnalytics: () => {
    const currentTab = Template.instance().currentTab
    Meteor.call('setNewAnalytics', { type: 'module', module: 'messenger', submodule: currentTab.get(), accessType: 'web', condoId: FlowRouter.getParam('condo_id') }, function (err, res) {
    })
  },
  checkTab: () => {
    let newCurrentTab = null
    let newCurrentConversation = null
    let lang = FlowRouter.getParam('lang') || 'fr'
    let condoId = FlowRouter.getParam('condo_id')
    let oldWildcard = FlowRouter.getParam('wildcard')
    let isNewMessage = Template.instance().isNewMessage.get()

    if (FlowRouter.getParam('module_slug') !== 'messenger') return  // Fix this shitty dumb router manager :/

    oldWildcard = oldWildcard ? oldWildcard.split('/') : null

    // Search if selectTab is good
    let defaultRoles = getDefaultRole()
    if (defaultRoles.length === 0) {
      return FlowRouter.go('app.board.resident.condo.module', { lang, condo_id: condoId })
    }
    if (oldWildcard && oldWildcard[0]) {
      let defaultRoleExists = !!(defaultRoles.find(role => role._id === oldWildcard[0]))
      if (!defaultRoleExists) {
        newCurrentTab = defaultRoles[0]._id
      } else {
        newCurrentTab = oldWildcard[0]
      }
    } else {
      newCurrentTab = defaultRoles[0]._id
    }

    // Search if selectMessage is good
    if (!!isNewMessage === false) {
      if (oldWildcard && oldWildcard[1]) {
        if (oldWildcard[1] === 'new') {
          Template.instance().isNewMessage.set(true)
          newCurrentConversation = ''
        } else {
          let messageExist = Messages.findOne({ _id: oldWildcard[1], target: newCurrentTab, condoId: condoId })
          if (!messageExist) {
            newCurrentConversation = (Messages.findOne({ target: newCurrentTab, condoId: condoId }, { sort: { lastMsg: -1 } }) || {})._id
          } else {
            newCurrentConversation = oldWildcard[1]
          }
        }
      } else {
        newCurrentConversation = (Messages.findOne({ target: newCurrentTab, condoId: condoId }, { sort: { lastMsg: -1 } }) || {})._id
      }
      // else view EmptyState
      if (!newCurrentConversation) {
        newCurrentConversation = 'MbEmptyMessage'
      }
    }

    if (newCurrentTab !== null) {
      Template.instance().currentTab.set(newCurrentTab)
      Template.instance().currentConversation.set(newCurrentConversation)
      let newWildcard = (!!newCurrentConversation === true) ? `${newCurrentTab}/${newCurrentConversation}` : `${newCurrentTab}`
      let route = `/${lang}/resident/${condoId}/messenger/${newWildcard}`
      FlowRouter.go(route)
    }
  },
  userHasRightMessage: () => {
    let canWrite = false
    let call = null
    const currentTab = Template.instance().currentTab.get()
    const condoId = FlowRouter.getParam('condo_id')

    if (currentTab === 'manager') {
      call = 'writeToManager'
    } else {
      const defaultRole = DefaultRoles.findOne({ _id: currentTab }) || {}
      switch (defaultRole.name) {
        case 'Occupant':
          call = 'writeToResident'
          break
        case 'Conseil Syndical':
          call = 'writeToUnionCouncil'
          break
        case 'Gardien':
          call = 'writeToSupervisors'
          break
      }
    }
    canWrite = Meteor.userHasRight('messenger', call, condoId)
    return canWrite
  },
  getAddress: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let condo = Condos.findOne({ _id: condoId })
    if (condo) {
      return condo.getName()
    }
  },
  getDefaultRole: () => {
    const roleInBuilding = getDefaultRole()
    const condoId = FlowRouter.getParam('condo_id')
    const badge = BadgesResident.findOne({ _id: condoId })
    return _.map(roleInBuilding, role => {
      let badgeName = null
      switch (role.name) {
        case 'Occupant':
          badgeName = 'messagerieResident'
          break
        case 'Conseil Syndical':
          badgeName = 'messagerieConseilSyndical'
          break
        case 'Gardien':
          badgeName = 'messagerieGardien'
          break
        case 'Gestionnaire':
        case 'Manager':
          badgeName = 'messagerieGestionnaire'
      }
      return {
        _id: role._id,
        name: role.name,
        badges: badge[badgeName] || 0
      }
    })
  },
  currentTab: () => Template.instance().currentTab.get(),
  selectTabCb: () => {
    let template = Template.instance()
    return (newCurrentTab) => {
      let lang = FlowRouter.getParam('lang') || 'fr'
      let condoId = FlowRouter.getParam('condo_id')
      let oldWildcard = FlowRouter.getParam('wildcard')
      let isNewMessage = template.isNewMessage.get()

      if (FlowRouter.getParam('module_slug') !== 'messenger') return;  // Fix this shitty dumb router manager :/

      let newCurrentConversation = null
      oldWildcard = oldWildcard ? oldWildcard.split('/') : null
      // Search if selectMessage is good

      if (!!isNewMessage === false) {
        if (oldWildcard && oldWildcard[1]) {
          let messageExist = Messages.findOne({ _id: oldWildcard[1], target: newCurrentTab, condoId: condoId })
          if (!messageExist) {
            newCurrentConversation = (Messages.findOne({ target: newCurrentTab, condoId: condoId }, { sort: { lastMsg: -1 } }) || {})._id
          } else {
            newCurrentConversation = oldWildcard[1]
          }
        } else {
          newCurrentConversation = (Messages.findOne({ target: newCurrentTab, condoId: condoId }, { sort: { lastMsg: -1 } }) || {})._id
        }
        // else view EmptyState
        if (!newCurrentConversation) {
          newCurrentConversation = 'MbEmptyMessage'
        }
      }

      let newWildcard = !!newCurrentConversation === true ? `${newCurrentTab}/${newCurrentConversation}` : `${newCurrentTab}`
      let route = `/${lang}/resident/${condoId}/messenger/${newWildcard}`
      FlowRouter.go(route)
      template.currentTab.set(newCurrentTab)
      template.currentConversation.set(!!newCurrentConversation === true ? newCurrentConversation : null)
    }
  },
  currentConversation: () => Template.instance().currentConversation.get(),
  selectConversationCb: () => {
    let template = Template.instance()
    return (newCurrentConversation) => {

      if (FlowRouter.getParam('module_slug') !== 'messenger') return;  // Fix this shitty dumb router manager :/

      template.isNewMessage.set(false)
      let wildcard = FlowRouter.getParam('wildcard').split('/')
      let newWildcard = !!newCurrentConversation === true ? `${wildcard[0]}/${newCurrentConversation}` : `${wildcard[0]}`
      let lang = FlowRouter.getParam('lang') || 'fr'
      let condoId = FlowRouter.getParam('condo_id')
      let route = `/${lang}/resident/${condoId}/messenger/${newWildcard}`
      FlowRouter.go(route)
      template.currentConversation.set(newCurrentConversation)
    }
  },
  isSubscribeReady: () => Template.instance().subscriptionsReady(),
  isNewMessage: () => Template.instance().isNewMessage.get(),
  setIsNewMessageAsFalse: () => {
    let isNewMessage = Template.instance().isNewMessage
    return () => isNewMessage.set(false)
  }
})

Template.messenger.events({
  'click #newMessage': function (event, template) {
    template.isNewMessage.set(true)
    let wildcard = template.currentTab.get()
    if (wildcard) {

      if (FlowRouter.getParam('module_slug') !== 'messenger') return;  // Fix this shitty dumb router manager :/

      let lang = FlowRouter.getParam('lang') || 'fr'
      let condoId = FlowRouter.getParam('condo_id')
      let route = `/${lang}/resident/${condoId}/messenger/${wildcard}`
      FlowRouter.go(route)
      template.currentConversation.set(null)
    }
  },
})

function getDefaultRole () {
  const lang = FlowRouter.getParam('lang') || 'fr'
  const translation = new CommonTranslation(lang)
  const condoId = FlowRouter.getParam('condo_id')
  const defaultRoles = DefaultRoles.find({}, { fields: { name: 1 } }).fetch()
  const condo = Condos.findOne({ _id: condoId })

  if (!condo) return []

  const messengerOptions = {
    messenger: condo.settings.options.messenger,
    supervisor: condo.settings.options.messengerGardien,
    manager: condo.settings.options.messengerGestionnaire,
    occupant: condo.settings.options.messengerResident,
    council: condo.settings.options.messengerSyndic
  }

  let availableRoles = []
  if (messengerOptions.messenger) {
    availableRoles = defaultRoles.filter(item => {
      let ret = false
      switch (item.name) {
        case 'Occupant':
          ret = condo && messengerOptions.occupant && condo.hasResidentWithRoleId(item._id) && getMessengerSettings(condoId) // && Meteor.userHasRight('messenger', 'writeToResident', condoId)
          break
        case 'Conseil Syndical':
          ret = condo && messengerOptions.council && condo.hasResidentWithRoleId(item._id) // && Meteor.userHasRight('messenger', 'writeToUnionCouncil', condoId)
          break
        case 'Gardien':
          ret = condo && messengerOptions.supervisor && condo.hasResidentWithRoleId(item._id) // && Meteor.userHasRight('messenger', 'writeToSupervisors', condoId)
          break
        default:
          ret = false
          break
      }
      return ret
    })
    if (messengerOptions.manager) {
      availableRoles.push(
        {
          _id: 'manager',
          name: translation.commonTranslation['manager']
        }
      )
    }
  }
  return availableRoles
}

function getMessengerSettings (condoId) {
  // For occupant: He have only him profile in Residents
  let resident = Residents.findOne() || {}
  if (resident) {
    let condo = resident.condos && resident.condos.find(c => c.condoId === condoId)
    if (condo) {
      return condo.preferences.messagerie
    }
  }
  return false
}
