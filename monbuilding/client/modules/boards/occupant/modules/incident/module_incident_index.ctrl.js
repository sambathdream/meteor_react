import { Template } from 'meteor/templating';

import './module_incident_index.view.html';

Template.module_incident_index.onCreated(function() {
  this.board = this.data.board;
  this.condoId = FlowRouter.current().params.condo_id;
    let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
    if (condo && condo.settings.options.incidents === false) {
        // FlowRouter.go('/fr/resident');
    }
  Session.set("modal", false);
});

Template.module_incident_index.events({
  'click .newIncident' (e, t) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const condoId = FlowRouter.getParam('condo_id')
    const params = {
      lang,
      condo_id: condoId,
      module_slug: 'messenger',
      wildcard: 'manager/new'
    }
    FlowRouter.go('app.board.resident.condo.module', params)
  },
    'click #followIncident' (e, t) {
      Meteor.call('module-incident-follow-toggle', FlowRouter.getParam("condo_id"));
    }
});

Template.module_incident_index.helpers({
  board: () => {
    return Template.instance().board;
  },
  hasModal: () => {
    return Session.get("modal");
  },
  followIncident: () => {
      let resident = Residents.findOne({userId: Meteor.userId()});
      let currentCondo = FlowRouter.getParam("condo_id");
      let condo = _.find(resident.condos, (condo) => {return condo.condoId === currentCondo});
      if (condo)
        return condo.notifications["incident"];
  },
});
