import { Template } from "meteor/templating";

Template.module_incident_feed.onCreated(function() {
});

Template.module_incident_feed.events({
});

Template.module_incident_feed.helpers({
  last: () => {
    let hist = Template.instance().data.incident.history
    return hist[hist.length -1];
  }
});
