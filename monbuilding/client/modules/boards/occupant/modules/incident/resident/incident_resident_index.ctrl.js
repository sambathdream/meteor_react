import moment from 'moment';
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { IncidentResidentIndex, CommonTranslation } from "/common/lang/lang.js";
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import MbTextArea from '/client/components/MbTextArea/MbTextArea';

var translation;

Template.incident_resident_index.onCreated(function() {
	const lang = (FlowRouter.getParam("lang") || "fr");
	translation = new IncidentResidentIndex(lang);
	this.errorMessage = new ReactiveVar("");
	this.selectedItem = new ReactiveVar(-1);
	this.charLen = new ReactiveVar(0);
	this.fm = new FileManager(MessengerFiles);
  let currentContext = FlowRouter.current();

	this.evalStars = new ReactiveVar([
		{state: false},
		{state: false},
		{state: false},
		{state: false},
		{state: false}
		]);
	this.condoId = currentContext.params.condo_id;

	let self = this;
	this.subsHandler = Meteor.subscribe("module_incidents_resident", this.condoId, function (err, data) {
		let incidents = ResidentIncidents.find().fetch();
		for (i of incidents) {
			Meteor.call('updateLastViewIncident', i._id, false);
		}
	});
	Meteor.call('setNewAnalytics', {type: "module", module: "incident", accessType: "web", condoId: FlowRouter.current().params.condo_id});

  this.description = new ReactiveVar('');
  this.files = new ReactiveVar([]);
  this.existingFiles = new ReactiveVar([]);
});

Template.incident_resident_index.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "incident", accessType: "web", condoId: FlowRouter.current().params.condo_id});
})

Template.incident_resident_index.onRendered(function() {
  if (FlowRouter.getParam("wildcard")) {
		let id = FlowRouter.getParam("wildcard");
		let template = Template.instance();
		Meteor.subscribe("incident_files", id, function(error) {
			if (!error) {
				template.selectedItem.set(id);
				Meteor.call('updateLastViewIncident', id, true);
				$('#incident_detail_modal').modal('show');
			}
		});
	}
});

function saveGestionnaireContact(message, files) {
  $('.confirmAction').button('loading');
  $('.confirmAction').prop('disabled', true)

  const lang = FlowRouter.getParam("lang");
  translation = new IncidentResidentIndex(lang);
  const commonT = new CommonTranslation(lang)
  template = Template.instance();
  let condoId = Template.instance().data.condo._id;
  let incident = ResidentIncidents.findOne(Template.instance().selectedItem.get());

  if (!message && !files.length) {
    $('.confirmAction').button('reset');
    $('.confirmAction').prop('disabled', false)
    template.errorMessage.set(translation.incidentResidentIndex["missing_fields_message"]);
    return ;
  }

  template.fm.batchUpload(files).then(result => {
    let details = incident.info.type;
    let sujet = incident.info.title;
    let data = {
      details: details,
      target: "manager",
      urgent: incident.info.priority,
      message: message,
      condoId: condoId,
      sujet: sujet,
      statut: "Locataire",
      lot: "nc",
      files: _.map(result, (f) => f._id),
      incidentId: incident._id
    };

    Meteor.call('contactGestionnaireFromIncidents', data, function(error) {
      if (!error) {

        sAlert.success(translation.incidentResidentIndex["message_send"]);
        $('#contactGestionnaire').modal('hide');
      }
      else {
        sAlert.error(error);
      }

      $('.confirmAction').button('reset');
      $('.confirmAction').prop('disabled', false)
    });
  }).catch(e => {
    $('.confirmAction').button('reset');
    sAlert.error(commonT.commonTranslation[e.reason])
  })
}

Template.incident_resident_index.events({
	'show.bs.modal #contactGestionnaire': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		let modal = $(event.currentTarget);
		window.location.hash = "#contactGestionnaire";
		window.onhashchange = function() {
			if (location.hash != "#contactGestionnaire"){
				modal.modal('hide');
			}
		}
	},
	'hidden.bs.modal #contactGestionnaire': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		emptyFormByParentId('contactGestionnaire');
		$('.alert-danger').remove();
    template.errorMessage.set("");
    template.description.set('')
    template.files.set([])
		$('#chat-window').prop('style', 'margin-top: 0 !important;');
		if ($('#attachments li')[0]) {
			$('#attachments li').remove();
		}
		history.replaceState('', document.title, window.location.pathname);
		Session.set("modal", false);
	},
	'show.bs.modal #incident_detail_modal': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		let modal = $(event.currentTarget);

    setTimeout(() => {
      $(window).trigger('resize')  //prevent wrong image display, size problem
    }, 1000);

		window.location.hash = "#incident_detail_modal";
		window.onhashchange = function() {
			if (location.hash != "#incident_detail_modal"){
				modal.modal('hide');
			}
		}
	},
	'show.bs.modal #incident_eval_modal': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		let modal = $(event.currentTarget);
		window.location.hash = "#incident_eval_modal";
		window.onhashchange = function() {
			if (location.hash != "#incident_eval_modal"){
				modal.modal('hide');
			}
		}
	},
	'hidden.bs.modal #incident_eval_modal': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		emptyFormByParentId('incident_eval_modal');
		$('.alert-danger').remove();
		template.errorMessage.set("");
		$('#chat-window').prop('style', 'margin-top: 0 !important;');
		if ($('#attachments li')[0]) {
			$('#attachments li').remove();
		}
		history.replaceState('', document.title, window.location.pathname);
		Session.set("modal", false);
	},
	'hidden.bs.modal #incident_detail_modal': function(event, template) {
		window.dispatchEvent(new Event('resize'));
		emptyFormByParentId('incident_detail_modal');
		template.errorMessage.set('');
		$('.errorModalInput').removeClass('errorModalInput');
		if ($('#attachments li')[0]) {
			$('#attachments li').remove();
		}
		history.replaceState('', document.title, window.location.pathname);
		Session.set("modal", false);
	},
	'click .confirmEval': function(e, t) {
		const lang = FlowRouter.getParam("lang");
		translation = new IncidentResidentIndex(lang);
		let eval = _.reduce(t.evalStars.get(), function(acc, e) {return acc + (e.state == true);}, 0);
		let comment = $('[name=evalComment]').val();
		if (eval == 0) {
			t.errorMessage.set(translation.incidentResidentIndex["thanks_to"]);
			return;
		}
		Meteor.call('updateEvalIncident', t.selectedItem.get(), eval, comment);
		$('#incident_eval_modal').modal('hide');
	},
	'click .evalStar': function(e, t) {
		t.errorMessage.set('');
		let index = parseInt(e.currentTarget.getAttribute("myindex"));
		let stars = [];
		for (let i = 0; i < 5; i++)
			stars.push({state: i <= index});
		t.evalStars.set(stars);
	},
	'click .cancelAction': function(e, t) {
		$('#incident_detail_modal').modal('hide');
		$('#incident_eval_modal').modal('hide');
	},
	'click .evalIncident': function(e, t) {
		let id = e.currentTarget.getAttribute('incidentId');
		t.selectedItem.set(id);
		let i = ResidentIncidents.findOne(id);
		let eval = _.find(i.eval, function(e) {return e.userId === Meteor.userId()});
		let star = [];
		for (let i = 0; i < 5; i++)
			star.push({state: (eval ? i < eval.value : false)});
		t.evalStars.set(star);
		t.errorMessage.set('');
		$('#incident_eval_modal').modal('show');
	},
	'click .showDetails': function(e, t) {
		let id = e.currentTarget.getAttribute('incidentId');
		Meteor.subscribe("incident_files", id);
		t.selectedItem.set(id);
		Meteor.call('updateLastViewIncident', id, true);
		$('#incident_detail_modal').modal('show');
	},
	'click .contactGestion': function(e, t) {
		let id = e.currentTarget.getAttribute('incidentId');
		t.selectedItem.set(id);
		t.fm.setCustomFields({ incidentId: id, userId: Meteor.userId(), condoId: FlowRouter.getParam("condo_id") })
		$('#contactGestionnaire').modal('show');
	},
	'mouseenter .show-helper': function(e, t){
		if ($(e.currentTarget).children()[3]) {
			$(e.currentTarget.children[3]).addClass('visible');
		}
	},
	'mouseleave .show-helper': function(e, t){
		if ($(e.currentTarget).children()[3]) {
			$(e.currentTarget.children[3]).removeClass('visible');
		}
	},
	'mouseenter #show-helper': function(e, t){
		$("#help-windows-fr").addClass('visible');
		$("#help-windows-en").addClass('visible');
	},
	'mouseleave #show-helper': function(e, t){
		setTimeout(function() {
			$("#help-windows-fr").removeClass('visible');
			$("#help-windows-en").removeClass('visible');
		}, 500)
	},
	'click .fileViewerTemplate': (event, template) => {
		event.preventDefault()
  },
  'click .confirmAction': (event, template) => {
	  event.preventDefault();

    saveGestionnaireContact(template.description.get(), template.files.get())
  }
});

Template.incident_resident_index.helpers({
	getFilePath: (id) => {
		return IncidentFiles.findOne(id).link();
	},
	getFileName: (id) => {
		return IncidentFiles.findOne(id).name;
	},
	evalStars: () => {
		return Template.instance().evalStars.get();
	},
	charAvailable: () => {
		return 50 - Template.instance().charLen.get();
	},
	condo: () => {
		return Template.instance().condo;
	},
	incidents: () => {
		return ResidentIncidents.find({}, {sort: {"lastUpdate": -1}}).fetch();
	},
	modalItem: () => {
		let id = Template.instance().selectedItem.get();
		if (id !== -1) {
			return ResidentIncidents.findOne({ _id: id });
		}
	},
	seeShare: (history, type) => {
		if (!history.share[type])
			return false;
		return history.share[type].state;
	},
	getUsername: (userId) => {
		const lang = FlowRouter.getParam("lang");
		translation = new IncidentResidentIndex(lang);
		if (userId === Meteor.userId())
			return translation.incidentResidentIndex["you"];
		let u = Meteor.users.findOne(userId);
		if (u)
			return u.profile.firstname + " " + u.profile.lastname;
	},
	showHistoryItem: (item) => {
		return (item.title !== 2);
	},
	showHistoryItem2: (item) => {
		return (item.title == 2);
	},
	getMyName: () => {
		let u = Meteor.user();
		return (u.profile.firstname + " " + u.profile.lastname);
	},
	errorMessage: () => {
		return Template.instance().errorMessage.get();
	},
	getBadgeValue: (i) => {
		if (i.view.length === 0 || !i.view[0].lastViewHistoric)
			return (i.declarer && i.declarer.autoDeclare == false) ? i.history.length - 1 : i.history.length;
		let dViewHistoric = i.view[0].lastViewHistoric;
		let acc = 0;
		if (i.lastUpdate > dViewHistoric) {
			i.history.forEach(function(h) {
				if (h.date > dViewHistoric)
					acc++;
			});
		}
		return acc;
	},
	isOpenEval: (i) => {
		return i.eval !== undefined && i.eval.length !== 0 && i.eval[0].open;
	},
	isNewPost: (id, userId) => {
		let history = ResidentIncidents.findOne(id).history[ResidentIncidents.findOne(id).history.length - 1];
		if (history && userId) {
			return history.isNew && (userId != Meteor.userId());
		}
		else if (history){
			if (history.isNew) {
				return true;
			}
			else {
				return false
			}
		}
		else {
			return false;
		}
	},
	getFiles: (ids) => {
		const filteredFiles = ids.filter(file => !file.fileId)
		let files = IncidentFiles.find({ _id: { $in: filteredFiles }});
		if (!files)
			files = UserFiles.find({ _id: { $in: filteredFiles }});
		if (files)
			return files;
	},
	getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
	getItemClass: (i) => {
		if (!i.view.length || !i.view[0].lastView)
			return "new_item";
		if (i.view[0].lastView < i.lastUpdate)
			return "update_item";
		return "";
	},
	getFileManager: function() {
		return Template.instance().fm;
  },
  canSubmit: () => {
    const t = Template.instance()

    const description = t.description.get()
    const files = t.files.get()
    if (!description && (!files || files.length === 0)) return false

    return true
  },
	incidentGestionnaireCb: () => {
		const lang = FlowRouter.getParam("lang");
		translation = new IncidentResidentIndex(lang);
		template = Template.instance();
		let condoId = Template.instance().data.condo._id;
		let incident = ResidentIncidents.findOne(Template.instance().selectedItem.get());
		return (message, files) => {
			if (!message && !files) {
				$('.confirmAction').button('reset');
				$('.confirmAction').prop('disabled', true)
				template.errorMessage.set(translation.incidentResidentIndex["missing_fields_message"]);
				return ;
			}
			let details = incident.info.type;
			let sujet = incident.info.title;
			let data = {
        details: details,
        target: "manager",
        urgent: incident.info.priority,
        message: message,
        condoId: condoId,
        sujet: sujet,
        statut: "Locataire",
        lot: "nc",
        files: files,
        incidentId: incident._id
			};
			Meteor.call('contactGestionnaireFromIncidents', data, function(error) {
				$('.confirmAction').button('reset');
				$('.confirmAction').prop('disabled', true)
				if (!error) {
					sAlert.success(translation.incidentResidentIndex["message_send"]);
					$('#contactGestionnaire').modal('hide');
				}
				else
					sAlert.error(error);
			});
		};
	},
	getIncidentTitle: (title) => {
		const lang = FlowRouter.getParam("lang");
		translation = new IncidentResidentIndex(lang);
		return translation.incidentResidentIndex["title" + title];
	},
	displayDetails: (item) => {
    // let lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"))
    if (item.intervenantKeeper || item.intervenantOther) {
      let intervStr = translation.commonTranslation['technician'] + ' : '
      if (item.intervenantKeeper) {
        intervStr += translation.commonTranslation['keeper'] + ' ' + (item.intervenantOther ? ", " : "")
      }
      if (item.intervenantOther) {
        intervStr += (item.intervenantName !== "" ? "'" + item.intervenantName + "'" : translation.commonTranslation['other_technician'])
      }
      return intervStr

    } else {
      if (item.details && item.details != "") {
        if (item.detailsIsDate) {
          let dateDetail = (item.undefinedHour == true ? moment(item.detailsDate).format("DD/MM/YYYY") : moment(item.detailsDate).format("DD/MM/YYYY") + ' ' + translation.commonTranslation['at'] + ' ' + moment(item.detailsDate).format('HH:mm'));
          return translation.commonTranslation['intervention_scheduled'] + dateDetail + translation.commonTranslation['was_canceled']
        }
        return item.details
      } else if (item.date_interv && item.date_interv != "") {
        return translation.commonTranslation['programmed_the'] + ' ' + moment(item.date_interv).format("DD/MM/YYYY" + (item.undefinedHour ? "" : ' [' + translation.commonTranslation['at'] + '] ' + moment(item.date_interv).format('HH:mm')))
      }
    }
	},
	getLang: () => {
		return FlowRouter.getParam('lang') || 'fr';
	},
	getCustomShareRole: (share, roleId) => {
		if (share && share[roleId])
			return (share[roleId]);
	},
  openIncidentModal: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const condoId = FlowRouter.getParam('condo_id')
    const params = {
      lang,
      condo_id: condoId,
      module_slug: 'messenger',
      wildcard: 'manager/new'
    };
    return () => {
      FlowRouter.go('app.board.resident.condo.module', params);
		}
	},
	isSubscribeReady: () => {
		return Template.instance().subsHandler.ready()
	},
	MbFilePreview: () => MbFilePreview,
  MbTextArea: () => MbTextArea,
  onFileAdded: () => {
    const tpl = Template.instance();

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  getDescriptionValue: () => {
    return Template.instance().description.get();
  },
  descriptionChange: () => {
    const t = Template.instance();
    return (value) => {
      t.description.set(value);
    }
  },
  getPreviewFiles: () => {
    let fileIds = Template.instance().existingFiles.get()
		const reactiveFiles = Template.instance().files.get()
		
		const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      tpl.existingFiles.set(_.filter(tpl.existingFiles.get(), f => f.fileId ? f.fileId !== fileId : f !== fileId))
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  disableConfirm: () => {
    const tpl = Template.instance()

    const stars = tpl.evalStars.get().find(st => st.state === true)

    return stars ? false : true
  }
});
