export default class BaseModule {

	constructor(module, building, condo, moduleType) {
		this.feed = new ReactiveVar([]);

		this.data = module.data;
		this.building = building;
		this.condo = condo;
		this.moduleType = moduleType;
		this.module = module;
		this.init(this.data);
	}

	configure(options) {
		this.miniTemplateName = options.miniTemplateName;
	}

	getMini() {
		return this.miniTemplateName;
	}

	getFeed() {
		this.init(this.data);
		return this.feed.get();
	}
};
