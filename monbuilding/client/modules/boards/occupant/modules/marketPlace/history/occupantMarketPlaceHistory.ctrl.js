import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { MarketServices, MarketServiceTypes, MarketBasket, MarketReservations } from '/common/collections/marketPlace'
import { CommonTranslation } from '/common/lang/lang.js';
import { Meteor } from 'meteor/meteor'
import _ from 'lodash'
import moment from 'moment'

function getDate (t, archivedAt, time = null, lang) {
  moment.locale(lang)
  const translate = new CommonTranslation(lang)
  const newDayMoment = moment(time || archivedAt)
  const newDay = newDayMoment.format('YYYY-MM-DD')
  let date = moment(time || archivedAt).format('LL')

  if (newDay === t.today) {
    date = translate.commonTranslation.today
  } else if (newDay === t.yesterday) {
    date = translate.commonTranslation.yesterday
  }
  t.lastDay = date
  return { timestamp: newDayMoment.format('x'), dateShort: date }
}

function _getLocation (reservation) {
  if (reservation.delivered_by_staff) {
    return 'delivered_by_staff'
  }
  if (reservation.drop_off) {
    return 'drop_off'
  }
  if (reservation.pick_up) {
    return 'pick_up'
  }
  if (reservation.picked_up_by_staff) {
    return 'picked_up_by_staff'
  }
}

Template.occupant_marketPlace_history.onCreated(function () {
  this.search = new ReactiveVar('')
  this.clicked = new ReactiveVar('')
  this.selectedReservationId = new ReactiveVar(null)
  this.handler = Meteor.subscribe('market_place_reservations_details', FlowRouter.getParam('condo_id'))

  this.today = moment().format('YYYY-MM-DD')
  this.yesterday = moment().add(-1, 'days').format('YYYY-MM-DD')
  this.lastDay = null
})

Template.occupant_marketPlace_history.onRendered(function () {
})

Template.occupant_marketPlace_history.events({
  'click .clickToToggle': (e, t) => {
    const collapseSectionIndex = $(e.currentTarget).data('index')
    var section = document.querySelector(`.section-${collapseSectionIndex}`);
    var isCollapsed = section.getAttribute('data-collapsed') === 'true';

    $(`.chevron-container-${collapseSectionIndex}`).toggleClass('opened closed')
    if (isCollapsed) {
      expandSection(section)
      section.setAttribute('data-collapsed', 'false')
    } else {
      collapseSection(section)
    }
  },
  'click .clickToPreview': (e, t) => {
    const reservationId = $(e.currentTarget).data('reservationid')

    t.selectedReservationId.set(reservationId)
    $('#modal_market_place_reservation').modal('show')
  }
})

Template.occupant_marketPlace_history.helpers({
  isSubscribeReady: () => {
    return Template.instance().handler && Template.instance().handler.ready()
  },
  servicesReservations: () => {
    const t = Template.instance()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoIds = Template.currentData().condoIds

    const regexp = new RegExp(Template.instance().search.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    let reservations = []
    reservations = MarketReservations.find({ condoId: { $in: condoIds }, archivedAt: { $ne: null } }, { sort: { archivedAt: -1 } }).fetch()

    const returnedObject = []
    const dateIndex = {}

    let dates = {}
    if (reservations && reservations.length === 1) {
      dates[reservations[0]._id] = getDate(t, reservations[0].archivedAt, null, lang)
    } else {
      reservations.sort((rA, rB) => {
        if (!dates[rA._id]) {
          dates[rA._id] = getDate(t, rA.archivedAt, null, lang)
        }
        if (!dates[rB._id]) {
          dates[rB._id] = getDate(t, rB.archivedAt, null, lang)
        }
        return parseInt(dates[rB._id].timestamp) - parseInt(dates[rA._id].timestamp)
      })
    }
    reservations.forEach((reservation) => {
      const location = translate.commonTranslation[_getLocation(reservation)]
      const timeArchived = Meteor.getTimeOrDash(reservation.archivedAt, false)
      const date = dates[reservation._id]

      if (reservation.title.match(regexp) || location.match(regexp) || timeArchived.match(regexp) || date.dateShort.match(regexp)) {

        if (!returnedObject.find(s => s.date === date.dateShort)) {
          returnedObject.push({
            date: date.dateShort,
            displayDate: date.dateShort,
            reservations: []
          })
          dateIndex[date.shortDate] = returnedObject.length - 1
        }
        returnedObject[dateIndex[date.shortDate]].reservations.push({ ...reservation, location, timeArchived })
      }
    })
    return returnedObject
  },
  isSearchActive: () => {
    return !!Template.instance().search.get()
  },
  onSearch: () => {
    const t = Template.instance()

    return (search) => {
      t.search.set(search)
    }
  },
  onCheckboxClick: (resaId, condoId) => {
    const t = Template.instance()

    return () => () => {
      event.stopPropagation();
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      t.clicked.set(resaId)

      bootbox.confirm({
        title: translate.commonTranslation.bootbox_validation_mp_service_title,
        message: translate.commonTranslation.bootbox_validation_mp_service_subtitle,
        buttons: {
          'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
          'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
        },
        callback: function (result) {
          t.clicked.set('')
          if (result) {
            Meteor.call('archiveMarketReservation', resaId, condoId, (error) => {
              if (error) {
                sAlert.error(translate.commonTranslation.error_occured)
              }
            });
          }
        }
      });
    }
  },
  isClicked: (resaId) => {
    return Template.instance().clicked.get() === resaId
  },
  getUserName: (userId) => {
    const profile = UsersProfile.findOne({ _id: userId })

    return profile.firstname + ' ' + profile.lastname
  },
  getLocation: (reservation) => {
    return _getLocation(reservation)
  },
  getDataModal: () => {
    const t = Template.instance()
    return {
      reservationId: t.selectedReservationId.get(),
      closeCb: () => {
        t.selectedReservationId.set(null)
      }
    }
  },
  getUserIcon: (userId) => {
    const userProfile = UsersProfile.findOne({ _id: userId })
    const avatar = Avatars.findOne({ _id: userId })
    let ppUrl = null
    let initial = ''

    if (userProfile) {
      initial = userProfile.firstname[0] + userProfile.lastname[0]

      if (avatar) {
        ppUrl = avatar.avatar.original
      }
    }

    return {
      ppUrl,
      initial
    }
  }
})


function collapseSection (element) {
  var sectionHeight = element.scrollHeight;

  var elementTransition = element.style.transition;
  element.style.transition = '';
  requestAnimationFrame(function () {
    element.style.height = sectionHeight + 'px';
    element.style.transition = elementTransition;

    requestAnimationFrame(function () {
      element.style.height = 0 + 'px';
    });
  });
  element.setAttribute('data-collapsed', 'true');
}

function expandSection (element) {
  var sectionHeight = element.scrollHeight;

  element.style.height = sectionHeight + 'px';
  element.addEventListener('transitionend', function (e) {
    element.removeEventListener('transitionend', arguments.callee);
    element.style.height = null;
  });
  element.setAttribute('data-collapsed', 'false');
}
