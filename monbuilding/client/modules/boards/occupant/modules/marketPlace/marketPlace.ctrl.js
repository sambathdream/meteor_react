/* globals BadgesResident */
/* globals Condos */
/* globals $ */

import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { Meteor } from 'meteor/meteor'

Template.occupant_marketPlace.onCreated(function () {
  this.tabRouter = Meteor.isManager ? 'tab' : 'wildcard' // Not useful here, just here to be able to copy / paster this module and use it easily everywhere (occupant and manager)
  let tab = FlowRouter.getParam(this.tabRouter)
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), [this.tabRouter]: 'services' })
  }

  Meteor.call('setNewAnalytics', { type: "module", module: "marketPlace", accessType: "web", condoId: FlowRouter.current().params.condo_id });

  // this.servicesHandler = Meteor.subscribe('market_place_feed')
})

Template.occupant_marketPlace.onRendered(function () {

})

Template.occupant_marketPlace.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ [t.tabRouter]: tabName })
  }
})

Template.occupant_marketPlace.helpers({
  checkTab: () => {
    const tabRouter = Template.instance().tabRouter
    const tab = FlowRouter.getParam(tabRouter)
    const availableTab = ['services', 'upcoming', 'history']
    const moduleSlug = FlowRouter.current().params.module_slug

    if (moduleSlug === 'marketPlace' && (!tab || (!availableTab.includes(tab)))) {
      Meteor.defer(() => {
        FlowRouter.setParams({ [tabRouter]: availableTab[0] })
      })
    }
  },
  getTemplateName: () => {
    const tab = FlowRouter.getParam(Template.instance().tabRouter)
    return 'occupant_marketPlace_' + tab
  },
  getCondoName: () => {
    const condoId = FlowRouter.getParam('condo_id')
    return Condos.findOne({ _id: condoId }).getName()
  },
  getData: () => {
    const condoId = FlowRouter.getParam('condo_id')
    const tab = FlowRouter.getParam(Template.instance().tabRouter)
    let condoIds = []
    if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    return { condoIds: condoIds, isAll: condoId === 'all' }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam(Template.instance().tabRouter)
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), [Template.instance().tabRouter]: 'services' })
    }
    return tab
  },
  shouldDisplayRestrictedAccess: () => {
    const condoId = FlowRouter.getParam('condo_id')
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('marketPlace', 'see')
    } else if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
  getUpcomingCounter: () => {
    const condoId = FlowRouter.getParam('condo_id')
    let condoIds = []
    if (Meteor.userHasRight('marketPlace', 'see', condoId)) {
      condoIds = [condoId]
    }
    let counter = 0
    BadgesResident.find({ _id: { $in: condoIds } }).forEach(condoBadge => {
      counter += condoBadge.marketPlace || 0
    })
    return counter
  }
})
