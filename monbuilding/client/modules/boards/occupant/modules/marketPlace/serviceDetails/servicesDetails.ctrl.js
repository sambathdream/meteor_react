/* globals Condos */

import { Meteor } from 'meteor/meteor'
import { Template } from 'meteor/templating'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveDict } from 'meteor/reactive-dict'
import { BasketFiles } from '/common/userFiles'
import { ReactiveVar } from 'meteor/reactive-var'
import { FileManager } from '/client/components/fileManager/filemanager.js';
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import DateTime from 'react-datetime'
import _ from 'lodash'
import moment from 'moment'

import { MarketServices, MarketReservations } from '/common/collections/marketPlace'
import { CommonTranslation, Common } from '../../../../../../../common/lang/lang';

Template.occupant_marketPlace_servicesDetails.onCreated(function () {
  this.servicesHandler = Meteor.subscribe('market_place_service_details', FlowRouter.getParam('serviceId'))
  this.reservationId = FlowRouter.getParam('reservationId') || null
  this.hasUpdated = false
  this.servicesHandler = Meteor.subscribe('market_place_reservation_details', this.reservationId)
  this.fm = new FileManager(BasketFiles, { condoId: FlowRouter.getParam('condo_id'), userId: Meteor.userId() })

  this.actualFiles = new ReactiveVar([])
  this.form = new ReactiveDict()
  this.form.setDefault({
    pick_up: false,
    drop_off: false,
    delivered_by_staff: false,
    picked_up_by_staff: false,
    special_request: '',
    quantity: '0',
    day_selected: null,
    time_slot: ''
  })

  this.reservedTimeSlots = new ReactiveVar(null)
  this.hasOneDelivery = new ReactiveVar(false)
  this.singleDelivery = null
  this.files = new ReactiveVar([])

  this.canSubmit = new ReactiveVar(false)

})

function updateSubmit (t, isOffered, useTimeSlot) {
  const form = t.form.all()
  const hasOneDelivery = t.hasOneDelivery.get()
  const canSubmit =
    (hasOneDelivery
      ? true
      : (!!form.pick_up || !!form.drop_off || !!form.delivered_by_staff || !!form.picked_up_by_staff))
      && (isOffered || (!!form.quantity && form.quantity !== '0'))
      && (useTimeSlot ? (!!form.day_selected && !!form.time_slot) : true)

  t.canSubmit.set(canSubmit)
}


Template.occupant_marketPlace_servicesDetails.onCreated(function () {

})

Template.occupant_marketPlace_servicesDetails.events({
  'click #backbutton': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const condoId = FlowRouter.getParam('condo_id')
    FlowRouter.go('app.board.resident.condo.module', { lang, condo_id: condoId, module_slug: 'marketPlace', wildcard: 'services' })
  },
  'input .number_available-input': function (e, t) {
    let quantity = $(e.currentTarget).val().replace(/[^0-9]/g, '')
    const service = this.service

    if (!quantity || parseInt(quantity) < 0) quantity = '0'
    if (!service.unlimited_quantity && parseInt(quantity) > parseInt(service.quantity_available)) {
      quantity = service.quantity_available
    }
    $(e.currentTarget).val(quantity)
    t.form.set('quantity', quantity)
    const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)

    updateSubmit(t, isOffered, service.use_time_slot)
  }
})

const inputCheckbox = [
  'pick_up',
  'drop_off',
  'delivered_by_staff',
  'picked_up_by_staff',
]


Template.occupant_marketPlace_servicesDetails.helpers({
  isSubscribeReady: () => {
    return Template.instance().servicesHandler ? Template.instance().servicesHandler.ready() : true
  },
  updateData: () => {
    const t = Template.instance()
    if (!t.hasUpdated && t.reservationId) {
      t.hasUpdated = true
      const reservation = MarketReservations.findOne({ _id: t.reservationId })
      const service = MarketServices.findOne({ _id: FlowRouter.getParam('serviceId') })
      const requestFiles = []
      if (reservation.special_request_files) {
        reservation.special_request_files.forEach(f => {
          requestFiles.push({ ...f, uri: f.link, isPreviousFile: true })
        })
      }

      t.form.set({
        pick_up: service.pick_up === true ? reservation.pick_up : false,
        drop_off: service.drop_off === true ? reservation.drop_off : false,
        delivered_by_staff: service.delivered_by_staff === true ? reservation.delivered_by_staff : false,
        picked_up_by_staff: service.picked_up_by_staff === true ? reservation.picked_up_by_staff : false,
        special_request: reservation.special_request,
        quantity: (service.unlimited_quantity || parseInt(reservation.quantity) < parseInt(service.quantity_available)) ? reservation.quantity : '1',
        day_selected: reservation.day_selected || null,
        time_slot: reservation.time_slot || ''
      })
      Meteor.call('getReservedSlots', FlowRouter.getParam('serviceId'), reservation.day_selected, (error, result) => {
        if (!error) {
          t.reservedTimeSlots.set(result)
        }
      })
      t.actualFiles.set(requestFiles)
    }
  },
  getService: () => {
    const serviceId = FlowRouter.getParam('serviceId')
    const service = MarketServices.findOne({ _id: serviceId })
    return service
  },
  getCondoName: () => {
    const condoId = FlowRouter.getParam('condo_id')
    return Condos.findOne({ _id: condoId }).getName()
  },
  MbTextArea: () => MbTextArea,
  previewFiles: () => {
    const reactiveFiles = Template.instance().files.get()
    let files = Template.instance().actualFiles.get()

    const newFileIds = files.filter(file => file.fileId)
    files = files.filter(file => !file.fileId)

    if (files) {
      const filesWithPreview = _.map(files, file => {
        return {
          ...file,
          preview: {
            url: file.link,
            type: file.type.split('/')[0]
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []


    return reactiveFiles
  },
  onFileAdded: () => {
    const t = Template.instance();

    return (files) => {
      t.files.set([
        ...t.files.get(),
        ...files
      ])
    }
  },
  onFileRemoved: () => {
    const t = Template.instance()

    return (fileId) => {
      t.actualFiles.set(t.actualFiles.get().filter(f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
      t.files.set(t.files.get().filter(f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  MbFilePreview: () => MbFilePreview,
  getFiles: (files) => {
    const mappedFiles = files.filter(file => !file.fileId).map(file => {
      // const thisFile = MarketPlaceFiles.findOne({ _id: file })
      if (file) {
        return {
          lowRes: file.link,
          preview: file.link,
          large: file.link,
          original: file.link,
          filename: file.name,
          type: file.type.split('/')[0],
          downloadPath: file.link
        }
      }
    })
    return mappedFiles
  },
  getForm: (key) => {
    const t = Template.instance()

    return t.form.get(key) || ''
  },
  onInput: function(key) {
    const t = Template.instance()
    const service = this.service

    return () => (value) => {
      const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
      if (inputCheckbox.includes(key)) {
        const deliveryObject = {
          pick_up: false,
          drop_off: false,
          delivered_by_staff: false,
          picked_up_by_staff: false
        }
        deliveryObject[key] = !t.form.get(key)
        t.form.set(deliveryObject)
      } else {
        t.form.set(key, value)
      }
      updateSubmit(t, isOffered, service.use_time_slot)
    }
  },
  getDeliveryOptions: function () {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const service = this.service || {}
    const ret = []

    const isOffice = (Condos.findOne({ _id: FlowRouter.getParam('condo_id') }) || { settings: { condoType: 'copro' } }).settings.condoType === 'office'

    if (service.pick_up) {
      ret.push({
        id: 'pick_up',
        subLabel: service.pick_up_location
      })
    }
    if (service.drop_off) {
      ret.push({
        id: 'drop_off',
        subLabel: service.drop_off_location
      })
    }
    if (service.delivered_by_staff) {
      ret.push({
        id: 'delivered_by_staff',
        subLabel: isOffice ? translate.commonTranslation.delivered_by_staff_detail_occupant_office : translate.commonTranslation.delivered_by_staff_detail_occupant
      })
    }
    if (service.picked_up_by_staff) {
      ret.push({
        id: 'picked_up_by_staff',
        subLabel: isOffice ? translate.commonTranslation.picked_up_by_staff_detail_occupant_office : translate.commonTranslation.picked_up_by_staff_detail_occupant
      })
    }
    if (ret.length === 1) {
      Template.instance().hasOneDelivery.set(true)
      Template.instance().singleDelivery = ret[0].id
    } else {
      Template.instance().hasOneDelivery.set(false)
      Template.instance().singleDelivery = null
    }
    const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
    updateSubmit(Template.instance(), isOffered, service.use_time_slot)
    return ret
  },
  getClassesSubmit: function (hasStock = false) {
    let classes = 'classic-service-btn btn btn-red-confirm'
    if (!hasStock || !Template.instance().canSubmit.get()) {
      classes += ' disabled'
    }
    if (this.service.price && this.service.price !== '0' && this.service.price !== 0) {
      classes += ' margin-top-23'
    }
    return classes
  },
  getLabel: function (hasStock = false) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    let label = ''
    if (this.service.price && this.service.price !== '0' && this.service.price !== 0) {
      label = hasStock ? (!!Template.instance().reservationId ? translate.commonTranslation.update_cart: translate.commonTranslation.add_to_cart) : translate.commonTranslation.out_of_order
    } else {
      label = translate.commonTranslation.validate
    }
    return label
  },
  priceIsNotNull: (price) => {
    return price && price !== '0' && price !== 0
  },
  onDescChange: () => {
    const t = Template.instance()

    return () => (value) => {
      t.form.set('special_request', value)
    }
  },
  getDescriptionValue: () => {
    const t = Template.instance()
    return t.form.get('special_request')
  },
  saveItemBasket: function (hasStock = false) {
    const t = Template.instance()
    const marketPlaceService = this.service
    const condoId = FlowRouter.getParam('condo_id')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const marketPlaceServiceId = FlowRouter.getParam('serviceId')
    const reservationId = FlowRouter.getParam('reservationId') || null

    return () => () => {
      if (t.canSubmit.get() && hasStock) {
        const form = t.form.all()
        const __files = t.files.get()
        const actualFiles = t.actualFiles.get()
        const isOffered = !(!!marketPlaceService.price && marketPlaceService.price !== '0' && marketPlaceService.price !== 0)
        const hasOneDelivery = t.hasOneDelivery.get()
        const singleDelivery = t.singleDelivery

        $('.classic-service-btn').button('loading')
        let sendForm = {
          pick_up: form.pick_up,
          drop_off: form.drop_off,
          delivered_by_staff: form.delivered_by_staff,
          picked_up_by_staff: form.picked_up_by_staff,
          special_request: form.special_request,
          price: marketPlaceService.price,
          vat: marketPlaceService.vat,
          quantity: form.quantity,
          day_selected: form.day_selected,
          time_slot: form.time_slot,
          filesId: []
        }

        if (hasOneDelivery) {
          sendForm[singleDelivery] = true
        }

        t.fm.batchUpload(__files)
          .then(results => {
            results.forEach(r => {
              sendForm.filesId.push(r._id)
            })
            Meteor.call('saveItemToBasket', marketPlaceServiceId, condoId, sendForm, reservationId, isOffered, actualFiles, (error, result) => {
              $('.classic-service-btn').button('reset')
              if (!error) {
                sAlert.success(translate.commonTranslation.booking_made)
                if (isOffered) {
                  const params = {
                    condo_id: condoId,
                    lang,
                    module_slug: 'marketPlace',
                    wildcard: 'upcoming'
                  }
                  FlowRouter.go('app.board.resident.condo.module', params)
                } else {
                  const params = {
                    condo_id: condoId,
                    lang
                  }
                  FlowRouter.go('app.board.resident.condo.module.marketBasket', params)
                }
                // FlowRouter.go('app.gestionnaire.marketPlace.tab', { lang, tab: 'services' })
              } else {
                console.log('error', error)
                sAlert.error(error)
              }
            })
          })
          .catch((err) => {
            $('.classic-service-btn').button('reset')
            console.log('err', err)
            sAlert.error(translate.commonTranslation[err.reason])
          })
      }
    }
  },
  shouldDisplaySecondBloc: (price, useTimeSlot) => {
    const t = Template.instance()

    const hasOneDelivery = t.hasOneDelivery.get()
    return !hasOneDelivery || !!useTimeSlot || (price && price !== '0' && price !== 0)
  },
  getDateTimeComponent: () => DateTime,
  getDatePlaceholder: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    return {
      placeholder: translate.commonTranslation.please_select_date,
      readOnly: true
    }
  },
  handleChangeDate: function () {
    const t = Template.instance()
    const service = this.service

    return (date) => {
      if (date.isSameOrAfter(moment(), 'day')) {
        t.reservedTimeSlots.set(null)
        t.form.set({'day_selected': date.format('DD/MM/YYYY'), 'time_slot': ''})
        Meteor.call('getReservedSlots', FlowRouter.getParam('serviceId'), date.format('DD/MM/YYYY'), (error, result) => {
          if (!error) {
            t.reservedTimeSlots.set((result && result.slots) || {})
          }
        })
        const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
        updateSubmit(t, isOffered, true)
      }
    }
  },
  getLocal: () => {
    return FlowRouter.getParam('lang') || 'fr'
  },
  isValidDate: (openedDays) => {
    const avilableDays = Object.keys(openedDays).filter(e => !!openedDays[e].length)
    return () => (current) => {
      if (current.isAfter(moment().subtract(1, 'day'))) {
        const currentDay = current.locale('en').format('dddd').toLowerCase()
        return avilableDays.includes(currentDay)
      }
      return false
    }
  },
  getTimeSlots: (service, selected) => {
    const t = Template.instance()
    const reservedTimeSlots = t.reservedTimeSlots.get()
    const now = moment()
    const isSelectedToday = selected === now.format('DD/MM/YYYY')
    const nowHour = isSelectedToday && parseInt(now.format('HH'))
    const nowMinutes = isSelectedToday && parseInt(now.format('mm'))
    const daySelected = moment(selected, 'DD/MM/YYYY').locale('en').format('dddd').toLowerCase()
    const hours = service.openedDays[daySelected]

    if (hours) {
      let slotDuration = parseInt(service.time_slot_duration)
      const scale = service.time_slot_scale

      let mappedHours = []
      if (scale === 'days') {
        slotDuration = (slotDuration * 60) * 24
      } else if (scale === 'hours') {
        slotDuration = (slotDuration * 60)
      }
      hours.forEach(interval => {
        let start = interval.start.split(':')
        let end = interval.end.split(':')

        start[0] = parseInt(start[0])
        start[1] = parseInt(start[1])
        end[0] = parseInt(end[0])
        end[1] = parseInt(end[1])

        const endStamp = moment(`${selected} ${end[0]}:${end[1]}`, 'DD/MM/YYYY HH:mm').add(slotDuration, 'minutes')
        while (start[0] <= end[0]) {
          if (isSelectedToday && start[0] < nowHour) {
            start[0]++
          } else {
            while(start[1] < 60) {
              const nextSlot = moment(`${selected} ${start[0]}:${start[1]}`, 'DD/MM/YYYY HH:mm').add(slotDuration, 'minutes')
              const nextNextSlot = moment(nextSlot).add(slotDuration, 'minutes')
              if (nextNextSlot > endStamp) return

              const currentTimeSlot = moment(`${start[0]}:${start[1]}`, 'HH:mm').format('HH:mm')
              if (!((reservedTimeSlots && reservedTimeSlots[currentTimeSlot] >= parseInt(interval.simultaneous_time_slot)) || (isSelectedToday && start[0] === nowHour && start[1] <= nowMinutes))) {
                mappedHours.push(moment(start[0], 'HH').format('HH') + ':' + moment(start[1], 'mm').format('mm'))
              }
              const formatNextSlot = nextSlot.format('HH:mm').split(':')
              start[0] = parseInt(formatNextSlot[0])
              start[1] = parseInt(formatNextSlot[1])
            }
          }
        }
      })
      return mappedHours
    }
  },
  isSelectedSlot: (timeSlot) => {
    const t = Template.instance()

    return timeSlot === t.form.get('time_slot')
  },
  onSelectTimeSlot: function (timeSlot) {
    const t = Template.instance()
    const service = this.service

    return () => () => {
      t.form.set({ time_slot: timeSlot })
      const isOffered = !(!!service.price && service.price !== '0' && service.price !== 0)
      updateSubmit(t, isOffered, true)
    }
  },
  reservedTimeSlots: () => {
    return Template.instance().reservedTimeSlots.get()
  }
})
