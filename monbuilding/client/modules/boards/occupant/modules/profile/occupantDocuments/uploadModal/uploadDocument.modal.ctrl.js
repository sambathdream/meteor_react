import mime from 'mime-types'

import { FileManager } from '/client/components/fileManager/filemanager.js';
import { MessageInput, CommonTranslation } from "/common/lang/lang";

var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

const mimeType = (type) => {
  return type.split('/')[0]
}

const getImageByExtension = (ext) => {
  const basePath = '/img/icons/svg/'

  switch (ext) {
    case 'zip':
      return `${basePath}extensionzip.svg`
    case 'docx':
    case 'doc':
      return `${basePath}extensiondoc.svg`
    case 'pptx':
    case 'ppt':
      return `${basePath}extensionppt.svg`
    case 'xls':
    case 'xlsx':
      return `${basePath}extensionxls.svg`
    case 'pdf':
      return `${basePath}extensionpdf.svg`
    case 'txt':
      return `${basePath}extensiontxt.svg`
    default:
      return `${basePath}extensionunknown.svg`
  }
}

Template.uploadDocumentModal.onCreated(function () {
  this.subscribe('PhoneCode');

  if (Meteor.isManager || Meteor.isAdmin) {
    this.userId = FlowRouter.getParam('userId')
  } else {
    this.userId = Meteor.userId()
  }

  this.condoId = new ReactiveVar(null)
  this.title = new ReactiveVar(null)
  this.file = new ReactiveVar(null)

  this.isEditMode = new ReactiveVar(false)
  this.editDocumentId = new ReactiveVar(null)


  this.isOpen = new ReactiveVar(false)
  this.canSubmit = new ReactiveVar(false)
  this.fm = new FileManager(UserDocumentFiles, {userId: this.userId, uploader: Meteor.userId()});

  this.isClicked = new ReactiveVar(false)
})

Template.uploadDocumentModal.onDestroyed(() => {
})

Template.uploadDocumentModal.onRendered(() => {
})

Template.uploadDocumentModal.events({
  'change #documentAttachment': (e, t) => {
    const file = e.currentTarget.files[0]

    file.id = `file-${getRandomString()}`
    // Add preview, either image or file extension
    if (file.type && mimeType(file.type) === 'image') {
      file.preview = {
        type: 'image',
        url: window.URL.createObjectURL(file)
      }
    } else {
      file.preview = {
        type: 'file'
      }
    }

    t.file.set(file)
    e.currentTarget.value = ''
    let fileSrc = null
    const re = /gif|jpe?g|png|bmp|svg/i
    if (re.test(mime.extension(file.type))) {
      fileSrc = file.preview.url
      var reader = new FileReader();
      reader.onload = function (e) {
        $('.attachmentDisplay')
          .attr('src', e.target.result)
          .width(110)
          .height(110)
          .css('display', 'block');
      };
      reader.readAsDataURL(file);
    }
    else {
      if (file.ext) {
        fileSrc = getImageByExtension(file.ext)
      } else {
        fileSrc = getImageByExtension(mime.extension(file.type))
      }
      $('.attachmentDisplay').attr('src', fileSrc).css('display', 'block')
    }
    updateSubmit(t)
  },
  'click .attachmentContainer': (e, t) => {
    $('#documentAttachment').click()
  },
  'click .saveDocument': (e, t) => {
    if (!t.isClicked.get()) {
      t.isClicked.set(true)
      const uploadFile = t.file.get()
      const condoId = t.condoId.get()
      const title = t.title.get()
      if (t.canSubmit.get() === true) {
        $('.saveDocument').button('loading')
        t.fm.setCustomFields({
          userId: t.userId,
          uploader: Meteor.userId(),
          condoId: condoId
        })
        const lang = FlowRouter.getParam('lang') || 'fr'
        const translate = new MessageInput(lang);
        const translateCommon = new CommonTranslation(lang);

        // console.log('uploadFile', uploadFile)
        const id = t.fm.insert(uploadFile, function (err, file) {
          if (err) {
            sAlert.error(translate.messageInput[err])
            $('.saveDocument').button('reset')
            return clearInterval(interval);
          }
        });
        let interval = setInterval(function() {
          if (t.fm.getErrorMessage()) {
            sAlert.error(translate.messageInput[t.fm.getErrorMessage()])
            $('.saveDocument').button('reset')
            clearInterval(interval)
            return;
          }
          if (!t.fm.getFileList()[id] || t.fm.getFileList()[id].done) {
            $('.saveDocument').button('reset')
            let uploadedFile = t.fm.getFileList()[id] ? t.fm.getFileList()[id].file._id : null
            clearInterval(interval)
            const methodName = t.isEditMode.get() ? 'editOccupantDocument' : 'uploadNewOccupantDocument'
            const exisitingDocument = t.isEditMode.get() ? t.editDocumentId.get() : null
            let uploader = null
            let userIdDocument = null
            if (t.userId !== Meteor.userId()) {
              uploader = Meteor.userId()
              userIdDocument = t.userId
            }
            Meteor.call(methodName, condoId, title, uploadedFile, userIdDocument, uploader, exisitingDocument, (error, result) => {
              if (!error) {
                if (!id) {
                  sAlert.success(translateCommon.commonTranslation.upload_success)
                } else {
                  sAlert.success(translateCommon.commonTranslation.update_success)
                }
                $('#modal_uploadDocument').modal('hide')
              }
              else {
                sAlert.error(error)
              }
            })
          }
        }, 100);
      }
    }
  },
  'hidden.bs.modal #modal_uploadDocument': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)
    template.condoId.set(null)
    template.title.set(null)
    template.file.set(null)
    template.isEditMode.set(false)
    template.editDocumentId.set(null)
    template.isClicked.set(false)

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_uploadDocument': (event, template) => {
    template.canSubmit.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_uploadDocument";

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_uploadDocument") {
        modal.modal('hide');
      }
    }
  },
})

function updateSubmit(template) {
  let canSubmit = false
  canSubmit = template.condoId.get() !== null && !!template.title.get() && (!!template.file.get() || template.isEditMode.get())
  template.canSubmit.set(canSubmit)
}

Template.uploadDocumentModal.helpers({
  updateData: () => {
    Template.instance().isEditMode.set(Template.currentData().isEdit)
    Template.instance().editDocumentId.set(Template.currentData().documentId)

    if (Template.currentData().isEdit === true && !!Template.currentData().documentId) {
      let editingDocument = UserDocuments.findOne({ _id: Template.currentData().documentId })
      if (editingDocument) {
        Template.instance().condoId.set(editingDocument.condoId)
        Template.instance().title.set(editingDocument.title)

        let fileSrc = null
        const re = /gif|jpe?g|png|bmp|svg/i
        if (re.test(mime.extension(editingDocument.type))) {
          fileSrc = editingDocument.fileLink
        }
        else {
          if (editingDocument.ext) {
            fileSrc = getImageByExtension(editingDocument.ext)
          } else {
            fileSrc = getImageByExtension(mime.extension(editingDocument.type))
          }
        }

        Meteor.defer(() => {
          $('.attachmentDisplay').attr('src', fileSrc).css('display', 'block')
        });
      }
    }
  },
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  getCondos: (index) => {
    let allowedCondos = []
    CondosModulesOptions.find({ 'profile.documents': true }, { field: { condoId: true } }).forEach(option => {
      allowedCondos.push(option.condoId)
    })
    if (Meteor.isManager) {
      const userCondoIds = _.pluck(GestionnaireUsers.findOne({ _id: Template.instance().userId }).resident[0].condos, 'condoId')
      allowedCondos = _.intersection(allowedCondos, userCondoIds)
    }
    let condos = []
    Condos.find({ _id: { $in: allowedCondos } }).forEach(condo => {
      condos.push({
        text: condo.getName(),
        id: condo._id
      })
    })
    return condos
  },
  getSelectedCondo: () => {
    let t = Template.instance()
    if (t.condoId.get()) {
      return t.condoId.get()
    }

    let allowedCondos = []
    CondosModulesOptions.find({ 'profile.documents': true }, { field: { condoId: true } }).forEach(option => {
      allowedCondos.push(option.condoId)
    })
    if (Meteor.isManager) {
      const userCondoIds = _.pluck(GestionnaireUsers.findOne({ _id: Template.instance().userId }).resident[0].condos, 'condoId')
      allowedCondos = _.intersection(allowedCondos, userCondoIds)
    }
    if (allowedCondos.length === 1) {
      return allowedCondos[0]
    }
    return null
  },
  getTitle: () => {
    return Template.instance().title.get()
  },
  onCondoSelect: () => {
    let t = Template.instance()

    return (value) => {
      t.condoId.set(value)
      updateSubmit(t)
    }
  },
  onInputTitle: () => {
    let t = Template.instance()

    return (value) => {
      t.title.set(value)
      updateSubmit(t)
    }
  }
})
