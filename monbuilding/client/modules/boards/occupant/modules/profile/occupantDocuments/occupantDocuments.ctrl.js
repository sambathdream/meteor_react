import { CommonTranslation } from "/common/lang/lang.js"

Template.occupantDocuments.onCreated(function () {
  this.searchText = new ReactiveVar('')
  if (Meteor.isManager || Meteor.isAdmin) {
    this.userId = FlowRouter.getParam('userId')
  } else {
    this.userId = Meteor.userId()
  }

  this.isEditMode = new ReactiveVar(false)
  this.editDocumentId = new ReactiveVar(null)

})

Template.occupantDocuments.onDestroyed(function () {
})

Template.occupantDocuments.onRendered(() => {
})

Template.occupantDocuments.events({
  'hidden.bs.modal #modal_uploadDocument': (e, t) => {
    t.editDocumentId.set(null)
    t.isEditMode.set(false)
  },
  'click .trashColumn > div': (e, t) => {
    let documentId = $(e.currentTarget).data('documentid')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    bootbox.confirm({
      size: 'medium',
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.confirm_delete_document,
      buttons: {
        'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          const userId = (Meteor.isManager || Meteor.isAdmin) ? t.userId : null
          Meteor.call('deleteOccupantDocument', documentId, userId, (error, result) => {
            if (!error) {
              sAlert.success(translate.commonTranslation.delete_success)
            } else {
              sAlert.error('Something went wrong')
            }
          })
        }
      }
    });
  },
  'click .editColumn > div': (e, t) => {
    let documentId = $(e.currentTarget).data('documentid')
    t.editDocumentId.set(documentId)
    t.isEditMode.set(true)
    $('.uploadDocument').click()
  }
})

Template.occupantDocuments.helpers({
  getDocuments: () => {
    let allowedCondos = []
    let condoNames = {}
    CondosModulesOptions.find({ 'profile.documents': true }, { field: { condoId: true } }).forEach(option => {
      allowedCondos.push(option.condoId)
      const condo = Condos.findOne({ _id: option.condoId })
      if (condo) {
        if (condo.info.id != '-1')
          condoNames[option.condoId] = condo.info.id + ' - ' + condo.getName()
        else
          condoNames[option.condoId] = condo.getName()
      }
    })

    if (Meteor.isManager) {
      const userCondoIds = _.pluck(GestionnaireUsers.findOne({ _id: Template.instance().userId }).resident[0].condos, 'condoId')
      allowedCondos = _.intersection(allowedCondos, userCondoIds)
    }
    let ret = []
    const regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), "i");

    UserDocuments.find({ userId: Template.instance().userId, condoId: { $in: allowedCondos } }, { sort: { date: -1 } }).forEach(document => {
      document.condoName = condoNames[document.condoId] || '-1'
      if (document.condoName.match(regexp) || document.title.match(regexp)) {
        ret.push(document)
      }
    })
    return ret
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewDocument: () => {
    const t = Template.instance()

    return () => {
      $('.uploadDocument').click()
    }
  },
  getEditMode: () => {
    return Template.instance().isEditMode.get()
  },
  getEditId: () => {
    return Template.instance().editDocumentId.get()
  },
  emptySearch: () => {
    return !!Template.instance().searchText.get()
  }
})
