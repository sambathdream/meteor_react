import { CommonTranslation } from "/common/lang/lang.js"
import { FileManager } from '/client/components/fileManager/filemanager.js';

const mimeType = (type) => {
  return type.split('/')[0]
}

Template.occupantProfile.onCreated(function () {
  this.subscribe('UserDocumentsPublish')
  this.userId = Meteor.userId()
  this.fm = new FileManager(UserFiles, { userId: Meteor.userId(), publicPicture: true });

  let tab = FlowRouter.getParam('wildcard')
})

Template.occupantProfile.onDestroyed(function () {
})

Template.occupantProfile.onRendered(() => {
})

Template.occupantProfile.events({
  'click .tabBarContainer > div': (event, template) => {
    let tabName = $(event.currentTarget).data('tab');
    FlowRouter.setParams({wildcard: tabName})
  },
  'click': (event, template) => {
    if ($(event.currentTarget).hasClass("editProfilePicture") || $(event.currentTarget).hasClass("cancelDropdown")) {
      $('.dropdownProfilePicture').toggleClass('displayNone')
    } else if ($(event.currentTarget).hasClass("uploadPhoto")) {
      $('#inputAvatar').click();
      $('.dropdownProfilePicture').addClass('displayNone')
    } else if ($(event.currentTarget).hasClass("removePhoto")){
      Meteor.call('setAvatarId', null)
    } else {
      $('.dropdownProfilePicture').addClass('displayNone')
    }
  },
	'change #inputAvatar' (event, template) {
    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (event.currentTarget.files && event.currentTarget.files.length === 1 && mimeType(event.currentTarget.files[0].type) === 'image') {
			template.fm.insert(event.currentTarget.files[0], function(err, file) {
        event.currentTarget.value = ''
        if (!err && file) {
          template.fm.clearFiles()
					Meteor.call('setAvatarId', file._id, (err, res) => {
            if (err) {
              sAlert.error(err)
            } else {
              sAlert.success(tr_common.commonTranslation["modif_success"])
            }
          });
				}
			});
		} else {
      sAlert.error(tr_common.commonTranslation['image_only'])
    }
	}
})

Template.occupantProfile.helpers({
  checkTab: () => {
    const tab = FlowRouter.getParam('wildcard')
    const resident = Residents.findOne({ userId: Meteor.userId() });
    if (resident) {
      condoIds = _.map(resident.condos, function (elem) {
        return elem.condoId;
      });
    }
    const options = CondosModulesOptions.find({ condoId: { $in: condoIds } }).fetch()
    let hasDocuments = false;
    let hasPayments = false;
    _.each(options, function (option) {
      if (option.profile['documents'] == true) {
        hasDocuments = true;
      }
      if (option.profile['payments'] == true) {
        hasPayments = true;
      }
    });

    availableTab = []
    if (hasDocuments) {
      availableTab.push('documents')
    }
    if (hasPayments) {
      availableTab.push('payments')
    }
    availableTab.push('buildingsList', 'preferences', 'password')

    if (!tab || (!_.contains(availableTab, tab))) {
      Meteor.defer(() => {
        FlowRouter.setParams({ wildcard: availableTab[0] })
      })
    }
  },
  getProfileOptions: (name) => {
    if (name) {
      let resident = Residents.findOne({ userId: Meteor.userId() });
      if (resident) {
        condoIds = _.map(resident.condos, function (elem) {
          return elem.condoId;
        });
      }
      let options = CondosModulesOptions.find({ condoId: { $in: condoIds } }).fetch()
      let ret = false;
      _.each(options, function (option) {
        if (option.profile[name] == true)
          ret = true;
      });
      return ret;
    }
  },
  getUserProfile: () => {
    let profile = UsersProfile.findOne({ _id: Template.instance().userId })
    if (profile === undefined) {
      let params = {
        condo_id: FlowRouter.getParam('condo_id'),
        lang: FlowRouter.getParam('lang') || 'fr',
        module_slug: 'profile',
        wildcard: FlowRouter.getParam('wildcard')
      }
      FlowRouter.go('app.board.resident.condo.module', params)
    }
    return profile
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().userId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  selectedTab: () => {
    return FlowRouter.getParam('wildcard')
  },
  getTabTemplate: () => {
    switch (FlowRouter.getParam('wildcard')) {
      case 'buildingsList':
        return 'buildingsList'
        break;
      case 'preferences':
        return 'settingsList'
        break;
      case 'documents':
        return 'occupantDocuments'
        break;
      case 'payments':
        return 'paymentsList'
        break;
      case 'password':
        return 'changePassword'
        break;
      default:
        break;
    }
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady();
  },
  openInterestModal: () => {
    return () => () => {
      $('#modal_occupant_edit_interests').modal('show')
    }
  }
})
