import { Template } from 'meteor/templating'
import { FlowRouter } from 'meteor/kadira:flow-router'
import { ReactiveVar } from 'meteor/reactive-var'

import PayzenWrapper from '/client/components/PayZen/PayzenWrapper'
import MbPayment from '/client/components/PayZen/MbPayment/MbPayment'

Template.paymentsList.onCreated(function () {
  this.amount = new ReactiveVar(0)
  this.paymentWaiting = new ReactiveVar(true)

  this.subscribe('getUserTransactions', FlowRouter.getParam('condo_id'))
  this.transactionId = new ReactiveVar(null)
})

Template.paymentsList.onRendered(function () {
})

Template.paymentsList.events({
  'click .testMe': (e, t) => {
    t.amount.set(3700)
    t.paymentWaiting.set(false)
  }
})

Template.paymentsList.helpers({
  getPayzenWrapper: () => PayzenWrapper,
  getPaymentComponent: () => MbPayment,
  getAmount: () => Template.instance().amount.get(),
  saveTransactionId: () => {
    const t = Template.instance()
    return (transactionId) => {
      t.transactionId.set(transactionId)
    }
  },
  paymentAsked: () => Template.instance().paymentWaiting.get()
  // isCreatingPayment: () => Template.instance().creatingPayment.get(),
  // isWaitingTransaction: () => !!Template.instance().waitingTransactionId.get(),
  // checkTransactionStatus: () => {
  //   let transactionId = Template.instance().waitingTransactionId.get()

  //   if (transactionId) {
  //     const transaction = UserTransactions.findOne({ _id: transactionId })
  //     if (transaction && transaction.threeDsResponse && transaction.threeDsResponse.createPaymentResult && transaction.threeDsResponse.createPaymentResult.commonResponse.responseCode === 0) {
  //       Template.instance().waitingTransactionId.set(null)
  //     }
  //   }
  // }
})
