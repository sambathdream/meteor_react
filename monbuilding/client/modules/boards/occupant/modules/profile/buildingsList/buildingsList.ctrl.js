import { CommonTranslation } from "/common/lang/lang.js"

Template.buildingsList.onCreated(function () {
  this.subscribe('schools');
  this.userId = Meteor.userId()
  this.searchText = new ReactiveVar('')

  this.editingCondo = new ReactiveVar(null)
})

Template.buildingsList.onDestroyed(function () {
})

Template.buildingsList.onRendered(() => {
})

Template.buildingsList.events({
  'click .editCondo': (e, t) => {
    t.editingCondo.set($(e.currentTarget).data('condoid'))
    $('#modal_occupant_edit_building').modal('show')
  }
})

Template.buildingsList.helpers({
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  getOccupantCondos: () => {
    let userId = Template.instance().userId

    const occupant = Residents.findOne({ userId: userId })
    const occupantProfile = Meteor.user()

    let listCondo = []
    _.each(occupant.condos, (condoResident) => {
      const condo = Condos.findOne({ _id: condoResident.condoId })
      let condoName = !!condo.info.address ? condo.info.address : '-'
      if (condo.name && condo.name !== "" && (condo.info.address && condo.name !== condo.info.address)) {
        condoName = condo.name
        if (condo.info.id && condo.info.id !== '-1') {
          condoName = condo.info.id + ' - ' + condoName
        }
      }
      let adress = condo.info.address + ", " + condo.info.code + " " + condo.info.city
      let userRight = UsersRights.findOne({
        $and: [
          { "userId": userId },
          { "condoId": condo._id }
        ]
      })
      let defaultRoleName = 'non défini'
      if (userRight && userRight.defaultRoleId) {
        let defaultRole = DefaultRoles.findOne({ _id: userRight.defaultRoleId })
        if (defaultRole && defaultRole.name) {
          defaultRoleName = defaultRole.name
        }
      }
      if (!condoResident.userInfo) {
        condoResident.userInfo = {}
      }
      condoResident.buildings.forEach(building => {
        let thisBuilding = Buildings.findOne({ _id: building.buildingId })
        if (thisBuilding) {
          building.name = thisBuilding.info.address + ", " + thisBuilding.info.code + " " + thisBuilding.info.city
          if (thisBuilding.name && thisBuilding.name !== "" && (thisBuilding.info.address && thisBuilding.name !== thisBuilding.info.address)) {
            building.name = thisBuilding.name + '<br>' + building.name
          }
        } else if (building.buildingId === '-1') {
          building.buildingId = Math.random()
          building.name = '-'
        }
        listCondo.push({
          _id: building.buildingId, // to make it faster, with a UNIQ id
          condoId: condoResident.condoId,
          invited: condoResident.invited,
          joined: condoResident.joined,
          name: condoName,
          adress: adress,
          role: defaultRoleName,
          company: condoResident.userInfo.company || '-',
          school: condoResident.userInfo.school || '-',
          studies: condoResident.userInfo.studies || '-',
          diploma: condoResident.userInfo.diploma || '-',
          office: condoResident.userInfo.office || '-',
          door: condoResident.userInfo.porte || '-',
          floor: condoResident.userInfo.etage || '-',
          wifi: condoResident.userInfo.wifi || '-',
          building
        })
      })
    })
    return listCondo
  },
  getTotalCondos: () => {
    return 1
  },
  getEditingCondo: () => {
    return Template.instance().editingCondo.get()
  },
  getAllOptions: () => {
    return ['company',
      'floor',
      'office',
      'door',
      'school',
      'studies',
      'diploma',
      'wifi'
    ]
  }
})
