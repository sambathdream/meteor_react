import { CommonTranslation } from "/common/lang/lang.js"

Template.editInterestModal.onCreated(function () {
  this.isOpen = new ReactiveVar(false)
})

Template.editInterestModal.events({
  'hidden.bs.modal #modal_occupant_edit_interests': (event, template) => {
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_occupant_edit_interests': (event, template) => {
    const modal = $(event.currentTarget);
    window.location.hash = "#modal_occupant_edit_interests";

    const thisUser = Meteor.user()
    template.$('#interests').tagsinput('destroy')
    template.$('#interests').val(thisUser.profile.interests).tagsinput({
      trimValue: true
    });
    template.$('#interests').on('beforeItemAdd', function(event) {
      // event.item: contains the item
      // event.cancel: set to true to prevent the item getting added
      if (event.item !== event.item.toLowerCase()) {
        event.cancel = true;
        $(this).tagsinput('add', event.item.toLowerCase());
      }
    });

    window.onhashchange = function () {
      if (location.hash !== "#modal_occupant_edit_interests") {
        modal.modal('hide');
      }
    }
  },
  'click .saveInterests': (e, t) => {
    const that = $(e.currentTarget)
    that.button('loading')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    Meteor.call('updateInterests', t.$('#interests').val(), null, (error, result) => {
      that.button('reset')
      if (!error) {
        sAlert.success(translate.commonTranslation.changes_saved)
        $('#modal_occupant_edit_interests').modal('hide')
      } else {
        sAlert.error(error)
      }
    })
  }
})

Template.editInterestModal.helpers({
  renderTags: () => {
    const thisUser = Meteor.user()
    if (Template.instance().view.isRendered) { // prevent error in console when DOM not rendered on modal
      Template.instance().$('#interests').val(thisUser.profile.interests).tagsinput({
        trimValue: true
      })
    }
  }
})
