import { CommonTranslation } from "/common/lang/lang.js"

Template.changePassword.onCreated(function () {
  this.oldPassword = new ReactiveVar(null)
  this.newPassword = new ReactiveVar(null)
  this.newPasswordCheck = new ReactiveVar(null)
  this.canSubmit = new ReactiveVar(false)
  this.currentUser = Meteor.user()
})

Template.changePassword.onDestroyed(() => {
})

Template.changePassword.onRendered(() => {
})

Template.changePassword.events({
  'click .setUpPassword': (e, t) => {
    const email = t.currentUser.emails[0].address
    let translation = new CommonTranslation((FlowRouter.getParam("lang") || "fr"))
    Accounts.forgotPassword({ email }, function (e, r) {
      if (e) {
        sAlert.error(translation.commonTranslation['error_occured'])
      } else {
        sAlert.success(translation.commonTranslation['email_sent'])
      }
    });
  },
  'click .saveContact': (e, t) => {
    const canSubmit = t.canSubmit.get()

    console.log('user data', Template.instance(), Meteor.user())

    const lang = FlowRouter.getParam("lang") || "fr"
    const tr_common = new CommonTranslation(lang)

    if (canSubmit) {
      const oldPassword = t.oldPassword.get()
      const newPassword = t.newPassword.get()
      Accounts.changePassword(oldPassword, newPassword, function (error) {
        if (error) {
          sAlert.error(error.reason);
        }
        else {
          sAlert.success(tr_common.commonTranslation["modif_success"]);
        }
      });
    }

  }
})

function updateSubmit(template) {
  let canSubmit = false
  const oldPassword = template.oldPassword.get()
  const newPassword = template.newPassword.get()
  const newPasswordCheck = template.newPasswordCheck.get()
  canSubmit =
    !!oldPassword &&
    !!newPassword &&
    !!newPasswordCheck &&
    newPassword.length >= 5 &&
    newPasswordCheck.length >= 5 &&
    newPassword === newPasswordCheck
  template.canSubmit.set(canSubmit)
}

Template.changePassword.helpers({
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'oldPassword': {
        const oldPassword = t.oldPassword.get()
        if (oldPassword === null || !!oldPassword) {
          return false
        } else {
          return true
        }
      }
      case 'newPassword': {
        const newPassword = t.newPassword.get()
        if (newPassword === null || !!newPassword) {
          return false
        } else {
          return true
        }
      }
      case 'newPasswordCheck': {
        const newPassword = t.newPassword.get()
        const newPasswordCheck = t.newPasswordCheck.get()
        if (newPasswordCheck === null || !!newPasswordCheck) {
          if (newPassword !== newPasswordCheck) {
            return true
          } else {
            return false
          }
        } else {
          return true
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'oldPassword') {
        t.oldPassword.set(value)
      } else if (key === 'newPassword') {
        t.newPassword.set(value)
      } else if (key === 'newPasswordCheck') {
        t.newPasswordCheck.set(value)
      }
      updateSubmit(t)
    }
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  getNewPasswordTitle(first, second) {
    return first + ' (' + second + ')'
  }
})
