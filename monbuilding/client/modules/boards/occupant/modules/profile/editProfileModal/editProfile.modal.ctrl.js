import { CommonTranslation } from "/common/lang/lang.js"


var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.editProfileOccupant.onCreated(function () {
  this.subscribe('PhoneCode');

  let userProfile = Meteor.user()

  this.email = new ReactiveVar(null)
  this.phoneCode = new ReactiveVar(null)
  this.phone = new ReactiveVar(null)
  this.landlineCode = new ReactiveVar(null)
  this.landline = new ReactiveVar(null)
  this.isOpen = new ReactiveVar(false)

  this.goodPhone = false
  this.goodLandline = false
  this.canSubmit = new ReactiveVar(false)
})

Template.editProfileOccupant.onDestroyed(() => {
})

Template.editProfileOccupant.onRendered(() => {
})

Template.editProfileOccupant.events({
  'hidden.bs.modal #modal_occupant_edit_profile': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)
    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_occupant_edit_profile': (event, template) => {
    template.isOpen.set(false)
    template.canSubmit.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_occupant_edit_profile";

    let thisUser = Meteor.user()
    template.email.set(thisUser.emails[0].address)
    template.phoneCode.set(thisUser.profile.telCode || '33')
    template.phone.set(thisUser.profile.tel || null)
    template.landlineCode.set(thisUser.profile.tel2Code || '33')
    template.landline.set(thisUser.profile.tel2 || null)
    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_occupant_edit_profile") {
        modal.modal('hide');
      }
    }
  },
  'click .saveContact': (e, t) => {
    if (t.canSubmit.get() === true) {
      const that = $(e.currentTarget)
      that.button('loading')
      const email = t.email.get()
      const phone = t.phone.get()
      const landline = t.landline.get()
      const phoneCode = t.phoneCode.get()
      const landlineCode = t.landlineCode.get()
      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      Meteor.call('updateUserProfile', email, phone, phoneCode, landline, landlineCode, null, (error, result) => {
        that.button('reset')
        if (!error) {
          sAlert.success(translate.commonTranslation.changes_saved)
          $('#modal_occupant_edit_profile').modal('hide')
        } else {
          sAlert.error(error)
        }
      })
    }
  }
})

function updateSubmit(template) {
  let canSubmit = false
  const email = template.email.get()
  const phone = template.phone.get()
  const landline = template.landline.get()
  canSubmit =
    email !== null && (Isemail.validate(email) === true) &&
    (phone === null || phone === '' || template.goodPhone === true) &&
    (landline === null || landline === '' || template.goodLandline === true)
  template.canSubmit.set(canSubmit)
}

Template.editProfileOccupant.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.phone.get()
        t.phoneCode.set(selected)
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.phone.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.goodPhone = false
          }
        } catch (e) {
          t.goodPhone = false
        }
      } else if (key === 'phoneCodeLandline') {
        let number = '+' + selected + t.landline.get()
        t.landlineCode.set(selected)
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.landline.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.goodLandline = false
          }
        } catch (e) {
          t.goodLandline = false
        }
      }
      // t.form.set(!selected ? null : selected)
      updateSubmit(t)
    }
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.email.get()
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.phone.get()
        let number = '+' + t.phoneCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      case 'landline': {
        const value = t.landline.get()
        let number = '+' + t.landlineCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodLandline = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodLandline = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'phone') {
        let number = '+' + t.phoneCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.phone.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.phone.set(!value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.phone.set(!value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'landline') {
        let number = '+' + t.landlineCode.get() + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.landline.set(phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.landline.set(!value ? null : value)
            t.goodLandline = false
          }
        } catch (e) {
          t.landline.set(!value ? null : value)
          t.goodLandline = false
        }
      } else if (key === 'email') {
        t.email.set(value)
      }
      updateSubmit(t)
    }
  },
  getPhoneCode: () => {
    const t = Template.instance()
    return () => t.phoneCode.get() || ''
  },
  getEmail: () => {
    const t = Template.instance()
    return () => t.email.get() || ''
  },
  getPhone: () => {
    const t = Template.instance()
    return () => t.phone.get() || ''
  },
  getLandlineCode: () => {
    const t = Template.instance()
    return () => t.landlineCode.get() || ''
  },
  getLandline: () => {
    const t = Template.instance()
    return () => t.landline.get() || ''
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  }
})
