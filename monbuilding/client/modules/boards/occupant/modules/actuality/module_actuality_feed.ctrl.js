import { Template } from "meteor/templating";
import "./module_actuality_feed.view.html";

Template.module_actuality_feed.onCreated(function() {

});

Template.module_actuality_feed.events({
  'click .actuality-name': function(event, template) {
    event.preventDefault();
    const params = {
      lang: (FlowRouter.getParam("lang") || "fr"),
      condo_id: FlowRouter.getParam("condo_id"),
      module_slug: "information"
    };
    FlowRouter.go('app.board.resident.condo.module', params);
    setTimeout(function() {
      document.location = '#' + event.currentTarget.getAttribute("idx");;
      return false;
    }, 0);
  }
});

Template.module_actuality_feed.helpers({
  actu : () => {
    return Template.currentData().actu;
  },
  isInfo : () => {
    return Template.currentData().actu.startDate;
  },
  displayDate : (date) => {
    var day = date.getDate();
    day = (day < 9) ? ("0" + day) : day;
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var month = (monthIndex < 9) ? "0" + (monthIndex + 1) : (monthIndex + 1);
    return day + '/' + month + '/' + year;
  }
});
