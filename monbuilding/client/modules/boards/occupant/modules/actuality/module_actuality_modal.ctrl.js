import { Template } from "meteor/templating";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { ModuleActualityModal, ErrorMessagingControler, CommonTranslation } from "/common/lang/lang.js";
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import "./module_actuality_modal.view.html";

var translation;

Template.module_actuality_modal.onCreated(function() {
  this.isOpen = new ReactiveVar(false)
  this.canPublish = new ReactiveVar(false);
	this.title = new ReactiveVar(null);
  this.description = new ReactiveVar(null);
	this.startDate = new ReactiveVar(null);
	this.endDate = new ReactiveVar(null);
	this.isActuError = new ReactiveVar(0b11); // used for the error handler. Binary value (11: NO ERROR IN TITLE AND DESCRIPTION INPUTS, 10: ERROR IN DESCRIPTION INPUT, 01: ERROR IN TITLE INPUT)
	this.isHourError = new ReactiveVar(0b11); // used for the hour errors. Binary value (11: NO ERROR IN START HOUR AND END HOUR INPUTS, 10: ERROR IN END HOUR INPUT, 01: ERROR IN START HOUR INPUT)
	this.isDateError = new ReactiveVar(0b11); // used for the date errors. Binary value (11: NO ERROR IN START DATE AND END DATE INPUTS, 10: ERROR IN END DATE INPUT, 01: ERROR IN START DATE INPUT)
	this.sameDateError = new ReactiveVar(false); // used for a specific date error
	this.checkbox = new ReactiveVar(false);
	this.fm = new FileManager(UserFiles, {actualityId: Template.currentData().actualityId}); // used for uploading file(s) in the richtext
	translation = new ModuleActualityModal((FlowRouter.getParam("lang") || "fr"));
	this.files = new ReactiveVar([]);
  this.existingFiles = new ReactiveVar([]);
  this.locate = new ReactiveVar('')
  this.condoId = new ReactiveVar(null)

  this.visibility = new ReactiveVar('public');
  this.participants = new ReactiveVar([]);
  this.allowComment = new ReactiveVar(false);
  this.allowAction = new ReactiveVar(false);
  this.isLimited = new ReactiveVar(false);
  this.participantLimit = new ReactiveVar(20);
  this.condoUsers = new ReactiveVar([]);

  this.setUser = (ids) => {
    const users = _.uniq([...this.condoUsers.get(), ..._.map(UsersProfile.find({_id: {$in: ids}}).fetch(), (u) => {
      return {
        id: u._id,
        text: `${u.firstname} ${u.lastname}`
      }
    })], (u) => {return u.id});

    this.condoUsers.set(users);
  }

	Meteor.subscribe('actuFiles', Template.currentData().actualityId);
});

Template.module_actuality_modal.onRendered(function() {
	// init the toolbar options for the rich text
	const toolbarOptions = [
		['bold', 'italic', 'underline'],
		['link', 'blockquote'],
		[{
			'list': 'ordered'
		}, {
			'list': 'bullet'
		}],
		[
			'align',
			{
				'align': 'center'
			},
			{
				'align': 'right'
			},
			{
				'align': 'justify'
			}
		]
	];

	this.editor = new Quill(this.find('.editor'), {
		theme: 'snow',
		placeholder: 'Description',
		modules: {
			toolbar: toolbarOptions,
		},
	});

	// this.editor.clipboard.dangerouslyPasteHTML(this.data.html || '');
});

function resetForm() {
	$('.titleActu').val("");
	$('.locateActu').val("");
	$('.mb__textarea > textarea').val("");
	$('#startDateActu').val("").datepicker('update');
	$('.startHourActu').val("");
	$('#endDateActu').val("").datepicker('update');
	$('.endHourActu').val("");
	$('#toggle-calendar').attr('checked', false);
	$('#calendar-input').css('display', 'none');
	// if (Template.instance().editor !== undefined) {
	// 	Template.instance().editor.root.innerHTML = "";
	// }
  Template.instance().files.set([]);
	Template.instance().fm.clearFiles();
	Template.instance().checkbox.set(false);
  Template.instance().visibility.set('public');
  Template.instance().participants.set([]);
  Template.instance().allowComment.set(false);
  Template.instance().allowAction.set(false);
}
// fill an actu object with the entered values in inputs
/* @param = createActu: empty object
template: template parameter of an event */
function fillActuObject(createActu, template, editPost = undefined) {
	createActu = {
		title: $('.titleActu').val(),
		locate: $('.locateActu').val(), // optionnal input
		description: $('.mb__textarea > textarea').val(),
		startDate: $('#startDateActu').val() ? $('#startDateActu').val() : '', // optionnal input
		startHour: ($('.startHourActu').val() ? ($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.startHourActu').val() : '-1') : '-1'), // optionnal input
		endDate: $('#endDateActu').val() ? $('#endDateActu').val() : '', // optionnal input
		endHour: ($('.endHourActu').val() ? ($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.endHourActu').val() : '-1') : '-1'), // optionnal input
		actualityId: Template.currentData().actualityId,
		userId: Meteor.user()._id,
		createdAt: Date.now(),
		views: [],
		history: [],
		files: template.existingFiles.get(),
		condoId: FlowRouter.getParam("condo_id"),
    visibility: template.visibility.get(),
    participants: template.participants.get(),
    allowComment: template.allowComment.get(),
    allowAction: template.allowAction.get(),
		isEvent: ($('#startDateActu').val() != "") ? true : false,
    isLimited: template.isLimited.get(),
    participantLimit: template.participantLimit.get()
	};

  const __files = template.files.get();
  const lang = FlowRouter.getParam("lang") || "fr"
  const translate = new CommonTranslation(lang)


  template.fm.batchUpload(__files)
    .then(files => {
      $('#modal_publier').modal('hide');

      createActu.files = createActu.files.concat(_.map(files, f => f._id));

      if (editPost) {
        // createActu.isUpdate  = new Date();
        createActu.isUpdate  = Date.now();
        createActu.updatedAt = Date.now();
        Meteor.call('module-actu-edit-post', editPost, createActu, () => {

          $(".submitActu").button('reset');
        });
      } else {
        createActu.createdAt = Date.now();
        createActu.updatedAt = Date.now();

        Meteor.call('module-actu-create-post', createActu, function(error, result) {
          if (error) {
            console.log(error);
          }

          $(".submitActu").button('reset');
        });
      }
    })
    .catch(err => {
      console.error(err);
      sAlert.error(translate.commonTranslation[err.reason])
      $(".submitActu").button('reset');
    })
};

// function to know if a date like format (date = '09/06/2017' hour = '00:00') is before another date or not
/* @param = StartDate : first String french date like format '09/06/2017'
StartHour: first String hour like format '00:00'
EndDate: second String french date like format '09/06/2017'
EndHour: second String hour like format '00:00' */
function isThisDateBeforeTheOther(StartDate, StartHour, EndDate, EndHour) {
	if (StartDate && EndDate) {
		let startDate = new Date(`${StartDate.split('/')[1]}/${StartDate.split('/')[0]}/${StartDate.split('/')[2]}`);
		startDate.setHours(StartHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? StartHour.split(':')[0] : "00");
		startDate.setMinutes(StartHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? StartHour.split(':')[1] : "00");
		let endDate = new Date(`${EndDate.split('/')[1]}/${EndDate.split('/')[0]}/${EndDate.split('/')[2]}`);
		endDate.setHours(EndHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? EndHour.split(':')[0] : "00");
		endDate.setMinutes(EndHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? EndHour.split(':')[1] : "00");
		return startDate.valueOf() < endDate.valueOf();
	}
	return (StartDate && !EndDate) || (!StartDate && EndDate);
};

// function to check if the startDate (& starHour) or the endDate (& endHour) value entered in their linked input aren't before the Date and Hour now
/* @param = isStartDate : boolean to know if the check is for the startDate and StartHur or the endDate and endHour */
function checkValidDateTime(isStartDate) {
	if (isStartDate) {
		if ($('#startDateActu').val() && $('#startDateActu').val().split('/').length == 3) {
			let date = new Date(`${$('#startDateActu').val().split('/')[1]}/${$('#startDateActu').val().split('/')[0]}/${$('#startDateActu').val().split('/')[2]}`);
			date.setHours($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.startHourActu').val().split(':')[0] : "00");
			date.setMinutes($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.startHourActu').val().split(':')[1] : "00");
			if (date.valueOf() < new Date().valueOf())
      return false;
		}
	}
	else {
		if ($('#endDateActu').val() && $('#endDateActu').val().split('/').length == 3) {
			let date = new Date(`${$('#endDateActu').val().split('/')[1]}/${$('#endDateActu').val().split('/')[0]}/${$('#endDateActu').val().split('/')[2]}`);
			date.setHours($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.endHourActu').val().split(':')[0] : "00");
			date.setMinutes($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') ? $('.endHourActu').val().split(':')[1] : "00");
			if (date.valueOf() < new Date().valueOf())
      return false;
		}
	}
	return true;
};

// error handler for date and hour. Remove or add the 'errorModalInput' class for the linked input if there's an error or not. Set the linked reactive variables too.
/* @param = template : template parameter of an event */
function isDateHourError(template) {
	if (checkValidDateTime(true))
  $('#endDateActu').removeClass("errorModalInput");
	else {
		template.isHourError.set(template.isHourError.get() & 0b1);
		$('.startHourActu').addClass("errorModalInput");
		$(".submitActu").button('reset');
	}
	if (checkValidDateTime(false)) {
		template.sameDateError.set(false);
		$('#startDateActu').removeClass("errorModalInput");
	}
	else {
		template.isHourError.set(template.isHourError.get() & 0b10);
		$('.endHourActu').addClass("errorModalInput");
		$(".submitActu").button('reset');
	}
	if ($('#endDateActu').val() && $('#startDateActu').val() && !isThisDateBeforeTheOther($('#endDateActu').val(), $('.endHourActu').val(), $('#startDateActu').val(), $('.startHourActu').val())
  && !isThisDateBeforeTheOther($('#startDateActu').val(), $('.startHourActu').val(), $('#endDateActu').val(), $('.endHourActu').val())) {
		template.sameDateError.set(true);
		$('.endHourActu').addClass("errorModalInput");
		$(".submitActu").button('reset');
	}
	if ($('#endDateActu').val() && !$('#startDateActu').val()) {
		template.isDateError.set(template.isDateError.get() & 0b1);
		$('#startDateActu').addClass("errorModalInput");
		$(".submitActu").button('reset');
	}
	else
  $('.endHourActu').removeClass("errorModalInput");
	if($('#startDateActu').val() && $('#endDateActu').val() && isThisDateBeforeTheOther($('#endDateActu').val(), $('.endHourActu').val(), $('#startDateActu').val(), $('.startHourActu').val())) {
		template.isDateError.set(template.isDateError.get() & 0b10);
		$('#endDateActu').addClass("errorModalInput");
		$(".submitActu").button('reset');
	}
	else
  $('.startHourActu').removeClass("errorModalInput");
};

Template.module_actuality_modal.events({
  'show.bs.modal #modal_publier': function(event, template) {
    template.isOpen.set(false)
    let modal = $(event.currentTarget);
    window.location.hash = "#modal_publier";
    window.onhashchange = function() {
      if (location.hash != "#modal_publier"){
        template.isOpen.set(false)
        modal.modal('hide');
      }
    }

    console.log(template.data, 'ojbnnji')
    setTimeout(() => {
      if (!!template.data.editPost) {
        const oldInfo = ActuPosts.findOne(template.data.editPost);
        if (!!oldInfo) {
          template.title.set(oldInfo.title);
          template.description.set(oldInfo.description);
          template.startDate.set(oldInfo.startDate);
          template.endDate.set(oldInfo.endDate);
          template.locate.set(oldInfo.locate);
          template.condoId.set(oldInfo.condoId);
          template.visibility.set(oldInfo.visibility);
          template.participants.set(oldInfo.participants);
          template.allowComment.set(oldInfo.allowComment);
          template.allowAction.set(oldInfo.allowAction);
          template.isLimited.set(oldInfo.isLimited);
          template.participantLimit.set(oldInfo.participantLimit);
          template.existingFiles.set(oldInfo.files);
          template.checkbox.set(oldInfo.isEvent);
        }
      }
    }, 100)

    template.isOpen.set(true)
  },
  'hidden.bs.modal #modal_publier': function(event, template) {
    $('.titleActu').removeClass("mb-input-error");
    $('.info-msg-box').removeClass('mb-input-error');

    template.existingFiles.set([]);
    template.files.set([]);
    template.title.set(null);
    template.description.set(null);
    $('.buttonCondoView').val('');
    $('.mb__textarea > textarea').val("");
    // template.editor.root.innerHTML = "";
    template.fm.clearFiles();
    $('#startDateActu').val("");
    $('.startHourActu').val("");
    $('#endDateActu').val("");
    $('.endHourActu').val("");
    template.startDate.set(null)
    template.isActuError.set(0b11); // reset error handler
		template.isHourError.set(0b11);
		template.isDateError.set(0b11);
		template.title.set(null);
    template.description.set(null);
    template.canPublish.set(null);
    template.participants.set([]);
    template.visibility.set('public');
    template.allowComment.set(false);
    template.allowAction.set(false);
    template.isLimited.set(false);
		$('.errorModalInput').removeClass('errorModalInput');
    // template.condoId.set("");
    // $(".submitActu").button('reset');
    history.replaceState('', document.title, window.location.pathname);
    template.isOpen.set(false)
  },
	'click .submitActu': function(event, template) {
    if (template.canPublish.get() === false) {
      return
    }
		const descField = $('.mb__textarea > textarea');
    descField.parents('.info-msg-box').removeClass('mb-input-error');
		// event when you click on the submit button
    $(".submitActu").button('loading');
		template.isActuError.set(0b11); // reset error handler
		template.isHourError.set(0b11);
		template.isDateError.set(0b11);
		var createActu = {};
		if (!$('.titleActu').val()) { // error handler for the title input
			$('.titleActu').addClass("mb-input-error");
			template.isActuError.set(template.isActuError.get() & 0b1);
			$(".submitActu").button('reset');
		}
		// if (!$('.descrActu')[0].children[2].textContent) { // error handler for the rich text input
		// 	$($('.descrActu').children()[2]).addClass("errorModalInput");
		// 	template.isActuError.set(template.isActuError.get() & 0b10);
		// 	$(".submitActu").button('reset');
		// }
		if (descField.val().trim() === '') {
      descField.parents('.info-msg-box').addClass('mb-input-error');
      template.isActuError.set(template.isActuError.get() & 0b10);
      $(".submitActu").button('reset');
		}
		if ($('.startHourActu').length) {
      if ($('.startHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') && !$('#startDateActu').val()) // if a valid hour entered and no date
			$('#startDateActu').val(`${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`);
      if ($('.endHourActu').val().match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$') && !$('#endDateActu').val())
			$('#endDateActu').val(`${new Date().getDate()}/${new Date().getMonth() + 1}/${new Date().getFullYear()}`);
			isDateHourError(template);
		}
		if (template.isActuError.get() === 0b11) { // if there's no error
    $('.titleActu').removeClass('mb-input-error');
    $('.mb__textarea').parents('.info-msg-box').removeClass('mb-input-error');
    // $($('.descrActu').children()[4]).removeClass('errorModalInput');
    if (template.isHourError.get() === 0b11 && template.isDateError.get() === 0b11)
    fillActuObject(createActu, template, Template.currentData().editPost);
  }
  template.isActuError.get() === 0b10 ? $('.titleActu').removeClass('mb-input-error')
    : template.isActuError.get() === 0b1 ? $($('.descrActu').children()[2]).removeClass('errorModalInput')
    : template.isHourError.get() === 0b10 ? $('.startHourActu').removeClass('errorModalInput')
    : template.isHourError.get() === 0b1 ? $('.endHourActu').removeClass('errorModalInput')
    : template.isDateError.get() === 0b10 ? $('#startDateActu').removeClass('errorModalInput')
    : template.isDateError.get() === 0b1 ? $('#endDateActu').removeClass('errorModalInput')
    : _.noop; // update error handler if the user corrected some errors
  },
  'click li .timeSlotStart': function(event, template) {
    // fill the startHour input with the choosen value of the dropdown for hours
    $('.startHourActu').val(this.toString());
  },
  'click li .timeSlotEnd': function(event, template) {
    // fill the endHour input with the choosen value of the dropdown for hours
    $('.endHourActu').val(this.toString());
  },
  // 'change #fileUploadInput': function(event, template) {
  //   // display the uploaded file in the rich text box
  //   var attachmentListUL = $('#attachments');
  //   if (event.currentTarget.files) {
  //     for (let i = 0; i < event.currentTarget.files.length; i++) {
  //       var data = {};
  //       let id = template.fm.insert(event.currentTarget.files[i]); // insert file in the file manager
  //       data.name = event.currentTarget.files[i].name; // fill the data object
  //       data.size = (event.currentTarget.files[i].size / 1000).toFixed(2) + ' kb';
  //       data.id = 'file-' + id;
  //       attachmentListUL.append(constructAttachmentElement(data));
  //       var progress = $(document).find('#file-' + id +' .progress')
  //       var elapsed = 0;
  //       var timer = setInterval(function() { // progress bar animation for the uploading
  //         var wrapper = $('#file-' + id);
  //         if (template.fm.getFileList()[i].done) { // if uploading is done
  //           wrapper.find('.meter').remove();
  //           wrapper.addClass('completed');
  //           clearInterval(timer);
  //         }
  //         if (elapsed < template.fm.getFileList()[id].upload.progress.get()) {
  //           elapsed = template.fm.getFileList()[id].upload.progress.get();
  //           progress.animate({
  //             width: elapsed + '%'
  //           }, 200);
  //         }
  //       }, 100)
  //     }
  //   }
  // },
  'click .remove-item': function(event, template) {
    // event to remove an uploaded file
    let idx = parseInt(event.currentTarget.getAttribute("index"));
    for (let i = 0; i != template.fm.getFileList().length; ++i) {
      if ($(event.currentTarget.parentElement)[0].innerText.match(template.fm.getFileList()[i].file.name)) {
        index = i;
        break;
      }
    }
    $(event.currentTarget.parentElement.parentElement).remove();
    template.fm.remove(index);
    $("#fileUploadInput").val('');
  },
  // 'click .file-upload-icon-wrapper': function(event, template) {
  //   // event to simulate a click in another element
  //   var fileInput = $('#fileUploadInput')
  //   fileInput.click();
  // },
  'click #datepickerModal1': function(event, template) {
    // event when you change the current month in the startDate datepicker
    let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
    let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
    for (var i = 0; i != Object.keys(months).length; i++) {
      if (Object.keys(months)[i] == date)
      break;
    }
    if (new Date().getMonth() == i) {
      $(prev).css('visibility', 'visible');
      $(prev).removeClass('prev');
      $(prev).html('');
    }
  },
  // event when you change the current month in the endDate datepicker
  'click #datepickerModal2': function(event, template) {
    let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
    let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
    for (var i = 0; i != Object.keys(months).length; i++) {
      if (Object.keys(months)[i] == date)
      break;
    }
    if (new Date().getMonth() == i) {
      $(prev).css('visibility', 'visible');
      $(prev).removeClass('prev');
      $(prev).html('');
    }
  },
  'change #toggle-calendar': function(e, t) {
    if ($(e.currentTarget)[0].checked) {
      $("#calendar-input").css('display', 'block');
    } else {
      $("#calendar-input").css('display', 'none');
      $("#datepickerModal1").val("");
      $("#datepickerModal2").val("");
      $(".startHourActu").val("");
      $(".endHourActu").val("");
    }
  },
  'click #toggle-checkbox, click #toggle-checkbox-label': function(e, t) {
    t.checkbox.set(!(t.checkbox.get()));

    if (t.checkbox.get()) {
      $("#calendar-input").css('display', 'block');
    } else {
      $("#calendar-input").css('display', 'none');
      $("#datepickerModal1").val("");
      $("#datepickerModal2").val("");
      $(".startHourActu").val("");
      $(".endHourActu").val("");
    }
  },
  'click .input-group-addon': function(e, t) {
    let calendar = $(e.currentTarget).parent().find('input[data-provide=datepicker]');
    if (calendar.length) {
      calendar.focus();
      let prev = $(".datepicker-dropdown table thead tr:nth-child(2) th:first-child")[0];
      let date = $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.substr(0, $(".datepicker-dropdown table thead tr:nth-child(2) th:nth-child(2)")[0].textContent.indexOf(' '));
      for (var i = 0; i != Object.keys(months).length; i++) {
        if (Object.keys(months)[i] == date)
        break;
      }
      if (new Date().getMonth() == i) {
        $(prev).removeClass('prev');
        $(prev).html('');
      }
    }
  },
  'input .titleActu': function (event, template) {
    template.title.set(event.currentTarget.value)
  },
  'input .mb__textarea > textarea': function (event, template) {
    template.description.set(event.currentTarget.value)
  },
  'change #startDateActu': function (event, template) {
    template.startDate.set(event.currentTarget.value)
  }
});

Template.module_actuality_modal.helpers({
	checked: () => {
		return Template.instance().checkbox.get();
	},
	dateToday: () => {
		// helper to display the today's date with the wanted format
		let date = new Date(Date('now'));
		return date.getDate() + '/' + (date.getMonth() + 1 < 10 ? '0' : '') + (date.getMonth() + 1) + '/' + date.getFullYear();
	},
	timeSlot: () => {
		// dropdown for the inputs startHour and endHour
		let slots = [];
		for (let i = 0; i < 24; i++) {
			let hour = (i < 10) ? "0" + i.toString() : i.toString();
			slots.push(hour + ":" + "00");
			slots.push(hour + ":" + "15");
			slots.push(hour + ":" + "30");
			slots.push(hour + ":" + "45");
		}
		return slots;
	},
	isActuError: () => {
		// helper to know if the title input or the rich text input (or both) are not valid
		return Template.instance().isActuError.get() != 0b11;
	},
	isHourError: () => {
		// helper to know if the startDate (& startHour) or the endDate (& startHour) input (or all) are not valid
		return Template.instance().isHourError.get() !== 0b11 || Template.instance().isDateError.get() !== 0b11;
	},
	isError: () => {
		// helper to know if there's at leat one error
		return Template.instance().isActuError.get() !== 0b11 || Template.instance().isHourError.get() !== 0b11
		|| Template.instance().isDateError.get() !== 0b11;
	},
	sameDateError: () => {
		// helper to know if the entered startDate and the entered endDate are same or not
		return Template.instance().sameDateError.get();
	},
	linkedErrorSentence: () => {
		let lang = (FlowRouter.getParam("lang") || "fr");
		let message = new ErrorMessagingControler(lang);
		// helper to display the good error sentence(s) according to errors
		let errorSentence = "";
		if (Template.instance().isDateError.get() === 0b1 || !Template.instance().isDateError.get())
      errorSentence = errorSentence + message.error_message.start_date_missing;
		if (Template.instance().isHourError.get() === 0b1 || !Template.instance().isHourError.get())
      errorSentence = errorSentence + message.error_message.begin_hour;
		if (Template.instance().isHourError.get() === 0b10 || !Template.instance().isHourError.get())
      errorSentence = errorSentence + (!Template.instance().isHourError.get() ? message.error_message.and : message.error_message.the_hour) + message.error_message.of_end;
		if (Template.instance().isHourError.get() !== 0b11)
      errorSentence = errorSentence + (!Template.instance().isHourError.get() ? message.error_message.indicates_outdated : message.error_message.indicate_outdated);
		if (Template.instance().isDateError.get() === 0b10 || !Template.instance().isDateError.get())
      errorSentence = errorSentence + message.error_message.end_before_begin;
		return errorSentence;
	},
	ifEditPost: (input) => {
    // helper to fill the different input if the user is editing an actuality
    /* @param = input : the name of the actuality collection key
    @return = the matching value according to the key in paramter b*/
    if (Template.currentData().editPost) { // if the user is editing
      // const post = ActuPosts.findOne(Template.currentData().editPost);
      // if (post && !Template.instance().fm.getFileList().length) {
      //   const files = UserFiles.find({ _id: { $in: post.files }});
      //   Template.instance().fm.addFiles(files.fetch());
      //   if (Template.instance().fm.getFileList().length) {
      //     const post = ActuPosts.findOne(Template.currentData().editPost);
      //     var files = UserFiles.find({ _id: { $in: post.files }});
      //
      //     Template.instance().existingFiles.set([files])
      //   }
      // }
      var actuPosts = ActuPosts.findOne(Template.currentData().editPost);
      if (_.has(actuPosts, input) && !_.isEmpty(actuPosts[input])) {
        if (input === 'startHour' && actuPosts[input] === '-1') {
          return
        }
        if (input === 'endHour' && actuPosts[input] === '-1') {
          return
        }
        if (input !== 'startHour' && input !== 'endHour' && input !== 'locate') {
          Template.instance()[input].set(actuPosts[input])
        }
        return actuPosts[input];
      }
    }
    else { // else clear files
      Template.instance().fm.clearFiles();
      if ($('#attachments li')[0]) {
        $('#attachments li').remove();
      }
    }
    return "";
	},
	isEditPost: () => {
    // helper to know if the user is editing a post or not
    return Template.currentData().editPost;
	},
	richEdit: () => {
    // helper to fill or reset the richtext box value
    if (Template.instance().editor) {
      if (Template.currentData().editPost) {
        Template.instance().editor.root.innerHTML = ActuPosts.findOne(Template.currentData().editPost).description;
      }
      else if (Template.instance().editor)
			Template.instance().editor.root.innerHTML = "";
    }
	},
  isNumberError: () => {
    const value = Template.instance().participantLimit.get()
    return value !== null && !isInt(value);
  },
	editPost: () => {
    // helper to know if the user is editing a post or not
    return Template.currentData().editPost;
	},
	getFileList: () => {
    // helper to get the list of uploaded files
    return Template.instance().fm.getFileList();
	},
	errorMessage: () => {
		return Template.instance().fm.getErrorMessage();
	},
	getIconOfFile: (type) => {
		let arrType = type.split("/");
		if (arrType[0] == "image") {
			if (["png", "PNG"].indexOf(arrType[1]) != -1)
        return "png.jpg";
			if (["jpg", "JPG", "jpeg", "JPEG"].indexOf(arrType[1]) != -1)
        return "jpeg.jpg";
			if (["bmp", "BMP"].indexOf(arrType[1]) != -1)
        return "bmp.jpg";
			if (["gif", "GIF"].indexOf(arrType[1]) != -1)
        return "gif.jpg";
			return "image.jpg";
		}
		if (arrType[0] == "application") {
			if (arrType[1] == "pdf")
        return "pdf.jpg";
			let appType = arrType[1].split(".");
			if (appType[0] == "vnd" && appType[1] == "openxmlformats-officedocument") {
				if (appType[2] == "presentation" || appType[2] == "presentationml")
          return "ppt.jpg"
				if (appType[2] == "spreadsheetml" || appType[2] == "sheet")
          return "xlsx.jpg"
				if (appType[2] == "wordprocessingml" || appType[2] == "document")
          return "docx.jpg"
			}
		}
		if (arrType[0] == "text")
      return "txt.jpg"
		return "file.png"
	},
	topButton: (index) => {
		return 7 + index * 33;
	},
  getFileCursor: () => {
    // helper to show files linked to the current actuality
    const post = ActuPosts.findOne(Template.currentData().editPost);
    if (post) {
      const files = UserFiles.find({ _id: { $in: post.files }});
      return files;
    }
    else {
      return null
    }
  },
  getNewFiles: () => {
    const post = ActuPosts.findOne(Template.currentData().editPost);
    let newFiles = []
    if (post) {
      newFiles = post.files ? post.files.filter(file => file.fileId) : []
    }
    return newFiles
  },
	isEditInfo: () => {
		if (Template.currentData().editPost)
    return !ActuPosts.findOne(Template.currentData().editPost).startDate;
	},
	resetForm: () => {
		if (!Template.currentData().editPost) {
			resetForm();
		}
	},
	MbTextArea: () => MbTextArea,
	cantSendActu: () => {
	  const instance = Template.instance()
    let cantPublish = true
    let checkbox = Template.instance().checkbox.get()
		let title = Template.instance().title.get()
    let description = Template.instance().description.get()
    let startDate = Template.instance().startDate.get()
    let condoId = instance.condoId.get()
    let endDate = instance.endDate.get()
    let visibility = instance.visibility.get()
    let locate = instance.locate.get()
    let participantLimit = instance.participantLimit.get()
    let isLimited = instance.isLimited.get()
    let allowComment = instance.allowComment.get()
    let allowAction = instance.allowAction.get()
    if (!checkbox) {
      if (title && title.trim() !== '' && description && description.trim() !== '') {
        cantPublish = false
      }
    } else {
      if (title && title.trim() !== '' && description && description.trim() !== '' && startDate && startDate !== '') {
        cantPublish = (instance.visibility.get() === 'private' && instance.participants.get().length === 0) || (instance.isLimited.get() && !isInt(instance.participantLimit.get()))
      }
      if (!cantPublish && isLimited && (!isInt(participantLimit) || parseInt(participantLimit) < 1)) {
        cantPublish = true
      }
      console.log(1)
      const old = ActuPosts.findOne(Template.currentData().editPost);
      if (!cantPublish && !!old) {
        console.log(2)
        let isSame = true;
        let filesNew = instance.files.get()
        let filesLoaded = instance.existingFiles.get()
        filesHasChange = old
        isSame = isSame && old.title === title
        isSame = isSame && old.description === description
        isSame = isSame && old.condoId === condoId
        isSame = isSame && old.startDate === startDate
        isSame = isSame && old.endDate === endDate
        isSame = isSame && !filesNew.length
        isSame = isSame && old.files.length === filesLoaded.length
        isSame = isSame && old.visibility === visibility
        isSame = isSame && old.locate === locate
        isSame = isSame && old.participantLimit === participantLimit
        isSame = isSame && old.isLimited === isLimited
        isSame = isSame && old.allowComment === allowComment
        isSame = isSame && old.allowAction === allowAction

        if (isLimited && (!isInt(participantLimit) || (parseInt(participantLimit) < 1 || !!old.participantResponse && !!old.participantResponse.going && parseInt(participantLimit) < old.participantResponse.going.length))) {
          isSame = true
        }

        cantPublish = isSame // Must be edited
      }
    }
    Template.instance().canPublish.set(!cantPublish)
    return cantPublish
	},
  addToCalendarArgs: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleActualityModal(lang);
    const instance = Template.instance();

    return {
      checked : instance.checkbox.get(),
      label: translate.moduleActualityModal['addCalendar'],
      onClick: () => {
        const status = instance.checkbox.get();
        instance.checkbox.set(!status);

				if (!status) {
					$("#calendar-input").css('display', 'block');
					$("#privacy-container").css('display', 'block');
				} else {
					$("#calendar-input").css('display', 'none');
          $("#privacy-container").css('display', 'none');
					$("#datepickerModal1").val("");
					$("#datepickerModal2").val("");
					$(".startHourActu").val("");
					$(".endHourActu").val("");
				}
      }
    };
  },
  getFiles: () => {
    let fileIds = Template.instance().existingFiles.get()
    const reactiveFiles = Template.instance().files.get()

    const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onFileAdded: () => {
    const tpl = Template.instance();

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      tpl.existingFiles.set(_.filter(tpl.existingFiles.get(), f => f.fileId ? f.fileId !== fileId : f !== fileId))
      tpl.files.set(_.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  visibility: (visibility) => {
    return Template.instance().visibility.get() === visibility
  },
  changeVisibility: () => {
    const instance = Template.instance()
    return () => () => {
      const newVal = instance.visibility.get() === 'private' ? 'public' : 'private'
      instance.visibility.set(newVal)
      if (newVal === 'private') {
        instance.isLimited.set(false)
      }
    }
  },
  setUsers: () => {
    const instance = Template.instance()
    if (instance.condoUsers.get().length === 0) {
      Meteor.call('getResidentsOfCondoWithRight', FlowRouter.getParam("condo_id"), 'actuality', 'see', (err, res) => {
        if (!err) {
          instance.setUser(res.filter((u) => u !== Meteor.userId()))
        }
      });
    }
  },
  getValue: (key) => {
    return Template.instance()[key].get()
  },
  setValue: (key) => {
    const instance = Template.instance()

    return () => value => {
      instance[key].set(value)
    }
  },
  toggleCheck: (key) => {
    const instance = Template.instance()

    return () => () => {
      value = instance[key].get()
      instance[key].set(!value)
    }
  },
  getUsersList: () => {
    return Template.instance().condoUsers.get()
  },
  MbFilePreview: () => MbFilePreview,
  isOpen: () => Template.instance().isOpen.get()
});
