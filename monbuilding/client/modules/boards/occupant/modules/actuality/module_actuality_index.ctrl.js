import { Template } from "meteor/templating";
import { Pagination } from "/client/components/pagination/pagination.js"
import { ModuleActualityIndex, DeletePost, WhoCanSeeHeader, CommonTranslation } from "/common/lang/lang.js"
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import moment from 'moment'

import "./module_actuality_index.view.html";

Template.module_actuality_index.title = "Actualités";

const regexpSearch = {
	'a': '[a|à|â|ä]',
	'e': '[e|é|è|ê|ë]',
	'i': '[i|î|ï|ì]',
	'o': '[o|ô|ö|ò]',
	'u': '[u|ù|ü|û]',
	'y': '[y|ý|ŷ|ÿ|ỳ]'
};

Template.module_actuality_index.onCreated(function() {
	// Template.currentData().data = {actualityId: id}
	this.editingPost = new ReactiveVar(); // used to know if the user edit (and which post) or create an actuality
	this.seeMore = new ReactiveArray([]); // used for cut the message if it is too long
	this.filter = new ReactiveVar(0b11); // used to know which condo the user choose. Binary value (11: ALL, 10: ONLY ACTUALITIES, 01: ONLY INFORMATIONS)
	this.filterYear = new ReactiveVar(new Date().getFullYear());
	this.filterMonth = new ReactiveVar(new Date().getMonth());
	this.filterDay = new ReactiveVar(null);
	this.returnWho = new ReactiveVar(false);
	let query = FlowRouter.getQueryParam("date");
	if (query !== undefined) {
		let mom = moment(query, "DD-MM-YYYY", true);
		if (mom.isValid()) {
			this.filterYear.set(mom.year());
			this.filterMonth.set(mom.month());
			this.filterDay.set(mom.date());
		}
	}
  this.detailId = new ReactiveVar(Session.get('eventDetailId'));
	Session.set('eventDetailId', false)
	this.eventState = new ReactiveDict();
	this.showPastille = new ReactiveVar(true);
	this.searchInfo = new ReactiveVar("");
	const {data} = Template.currentData();
	this.dataActu = data;
	this.subscribe('ActuPosts', {actualityId: data.actualityId});
	this.subscribe('actus', data.actualityId);
	this.subscribe('eventCommentsFeedCounter', [FlowRouter.current().params.condo_id]);
	// this.subscribe('actuFiles', data.actualityId);
	this.subscribe('helpers', function(){
		if (Helpers.findOne() == undefined || Helpers.findOne().information != true) {
			setTimeout(function() {
				$('.help-module').show();
			}, 50)
		}
	});
	this.actualityId = data.actualityId;
	this.pagination = new Pagination(ActuPosts, 2); // used for the pagination component
	this.pagination.setQuery({actualityId: data.actualityId});
	this.who = new ReactiveVar('');
	Meteor.call('setNewAnalytics', {type: "module", module: "info", accessType: "web", condoId: FlowRouter.current().params.condo_id});
});

Template.module_actuality_index.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "info", accessType: "web", condoId: FlowRouter.current().params.condo_id});
})

function getQueryParams(qs) {
	qs = qs.split('+').join(' ');

	var params = {},
	tokens,
	re = /[?&]?([^=]+)=([^&]*)/g;

	while (tokens = re.exec(qs)) {
		params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
	}

	return params;
}

function initDatePicker(template, eventDates, lang) {
    const self = template;
    let query = getQueryParams(document.location.search);
    let date_param = null;
    if (typeof query.date !== 'undefined') {
        date_param = query.date;
    }
    let first_load = true;

    const selector = $("#mini-calendar");
    selector.datepicker("destroy");
    selector.datepicker({
        maxViewMode: 0,
        language: lang,
        weekStart: 1,
        autoclose: true,
        todayHighlight: true,
        defaultDate: '0d',
        beforeShowDay: function(date) {
            const formattedDate = moment(date).add(12, 'h').startOf('day').format('x');
            if (_.isArray(eventDates) && eventDates.includes(formattedDate)) {
                return {
                    classes: 'booked-date'
                };
            }
        },
        templates: {
            leftArrow: '<i class="fa fa-chevron-left"></i>',
            rightArrow: '<i class="fa fa-chevron-right"></i>'
        }
    })
	.on('changeMonth', function(event) {
		self.filterYear.set(new Date(event.date).getFullYear());
		self.filterMonth.set(new Date(event.date).getMonth());
		self.filterDay.set(firstActuInMonth(self));
		self.showPastille.set(!self.showPastille.get());
	})
	.on("changeDate", function(event) {
		if (date_param !== null && first_load) {
			self.filterDay.set(moment(date_param, 'DD-MM-YYYY').format('DD'));
			first_load = false;
		} else {
			self.filterDay.set(new Date(event.date).getDate());
		}
		self.showPastille.set(!self.showPastille.get());
		self.filterMonth.set(new Date(event.date).getMonth());
	});
    selector.datepicker("setDate", new Date());

    self.showPastille.set(!self.showPastille.get());
    setTimeout(function () {
        if (date_param !== null) {
            $("#cal").datepicker('setDates', date_param);
        } else {
            self.filterDay.set(firstActuInMonth(self));
        }
    }, 30);
}

Template.module_actuality_index.onRendered(function() {

});

// function for display only actualities which match with the selected month or day
/* @param = elemSDate : Date object. The starting date of an actuality
elemEDate : Date object. The ending date of an actuality
template : Template.instance() */
function filterPosts(elemSDate, elemEDate, template) {
	whichDay = parseInt(elemSDate.getDate()) <= parseInt(elemEDate.getDate()) || Template.instance().filterMonth.get() == elemSDate.getMonth() ? elemSDate.getDate() : elemEDate.getDate();
	selectedDay = Template.instance().filterDay.get() ? new Date(`${Template.instance().filterMonth.get() + 1}/${Template.instance().filterDay.get()}/${Template.instance().filterYear.get()}`)
	: new Date(`${Template.instance().filterMonth.get() + 1}/${whichDay}/${Template.instance().filterYear.get()}`);
	return elemSDate.valueOf() <= selectedDay.valueOf() && selectedDay.valueOf() <= elemEDate.valueOf();
};

// function for removing bubbles of the calendar when you remove an actuality
// @param = delDate: Date object. The date where the bubble will be remove. May be the starting date or the ending date of an actuality
function removePastille(delDate) {
	if (_.filter(ActuPosts.find({actualityId: Template.instance().dataActu.actualityId}).fetch(), function(elem) {
		elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
		elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
		return (delDate.getDate() == elemSDate.getDate() && elemSDate.getMonth() == delDate.getMonth())
		|| (delDate.getDate() == elemEDate.getDate() && elemEDate.getMonth() == delDate.getMonth());
	}).length === 1) {
		let pastille = _.find($("#cal table tbody tr td"), function(elem) {
			return $(elem)[0].innerText == delDate.getDate() && $(elem).hasClass('day')
			&& !$(elem).hasClass('old') && !$(elem).hasClass('new');
		});
		$(pastille).html(delDate.getDate());
	}
};

function firstActuInMonth(template) {
	if (template.filterMonth.get() == new Date().getMonth() && template.filterYear.get() == new Date().getFullYear()) return new Date().getDate()
		const posts = _.sortBy(_.filter(ActuPosts.find({condoId: FlowRouter.current().params.condo_id, startDate: {$ne: ''}}).fetch(), function(elem) {
			elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
			elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
			return elemSDate.getMonth() == template.filterMonth.get() && elemSDate.getFullYear() == template.filterYear.get()
			|| elemEDate.getMonth() == template.filterMonth.get() && elemEDate.getFullYear() == template.filterYear.get();
		}), function(elem) {
			return new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`)
			&& new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
		});
	if (!posts.length)
		return 1;
	return posts[0].startDate.split('/')[1] == template.filterMonth.get() + 1 ? posts[0].startDate.split('/')[0] : posts[0].endDate.split('/')[0];
};

Template.module_actuality_index.events({
	'click #new-post': function(event, template) {
		// reset the editingPost each time you click on a button linked to a modal
		template.editingPost.set(null);
	},
	'click #follow-actu': function(event, template) {
		// call the method to follow when you click on the button to follow
		Meteor.call('module-follow-actu', FlowRouter.current().params.condo_id);
	},
	'click .post-edit': function(event, template) {
		// set the editingPost used for the modal when you want to edit an actuality
		const id = event.currentTarget.getAttribute('data-id');
		template.editingPost.set(id);
	},
	'click .post-delete': function(event, template) {
		// event to delete an actuality. Update the calendar's bubbles and call the method to remove an actuality
		const id = event.currentTarget.getAttribute('data-id');
		const post = ActuPosts.findOne(id);
		const delDate = {
			startDate: new Date(`${post.startDate.split('/')[1]}/${post.startDate.split('/')[0]}/${post.startDate.split('/')[2]}`),
			endDate: new Date(`${post.endDate.split('/')[1]}/${post.endDate.split('/')[0]}/${post.endDate.split('/')[2]}`),
		};
		removePastille(delDate.startDate);
    removePastille(delDate.endDate);
    const lang = (FlowRouter.getParam("lang") || "fr")
    let translation = new DeletePost(lang);
    const tr_common = new CommonTranslation(lang)
    bootbox.confirm({
      title: tr_common.commonTranslation["title_delete_information"],
      message: post.userId == Meteor.userId() ? tr_common.commonTranslation["message_delete_information"] : tr_common.commonTranslation["message_delete_information_other"],
      buttons: {
        confirm: {label: translation.deletePost["yes"], className: "btn-red-confirm"},
        cancel: {label: translation.deletePost["no"], className: "btn-outline-red-confirm"}
      },
      callback: function(res) {
        if (res) {
          Meteor.call('module-actu-post-delete', id);
          sAlert.success(translation.deletePost["succes_delete_post"]);
        }
      }
    });
	},
	'click #goToCgu': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Session.set('backFromCgu', FlowRouter.current().path);
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},
	'click .filter-actu': function(event, template) {
		// set the filter if you choose actuality mode or information mode or both
		if (template.filter.get() != parseInt(event.currentTarget.getAttribute('filter')))
			template.filterDay.set(null);
		template.filter.set(parseInt(event.currentTarget.getAttribute('filter')));
	},
	'click .seeMore': function(event, template) {
		// event to see the full message when it is too long
		let translation = new ModuleActualityIndex((FlowRouter.getParam("lang") || "fr"));
		const id = event.currentTarget.getAttribute('post-id');
		let description = ActuPosts.findOne(id);
		if (description) {
			$(event.currentTarget.parentElement.parentElement)[0].innerHTML = description.description + `<a class='seeLess' post-id=${id} style='font-size: 12px'>` + translation.moduleActualityIndex["see_less"] + `</a>`;
		}
	},
	'click .seeMoreOrLess': function(event, template) {
		let contentObj = $(event.currentTarget.parentElement).find('.info-content');
        let translation = new ModuleActualityIndex((FlowRouter.getParam("lang") || "fr"));
		contentObj.find('.ellipsisedText').toggleClass('show');
		contentObj.find('.notEllipsisedText').toggleClass('show');
		contentObj.toggleClass('ellipsis');
		$(event.currentTarget).text(contentObj.hasClass('ellipsis') ? translation.moduleActualityIndex["see_more"] : translation.moduleActualityIndex["see_less"]);
	},
	'click .seeLess': function(event, template) {
		// event to see a cut message (unful) when it is too long (max character: 250)
		let translation = new ModuleActualityIndex((FlowRouter.getParam("lang") || "fr"));
		const id = event.currentTarget.getAttribute('post-id');
		var cutMessage = "";
		var count = 0;
		var isBalise = false;
		let description = ActuPosts.findOne(id);
		if (description) {
			for (let i = 0; i != description.description.length; i++) {
				if (description.description[i] == '<')
					isBalise = true;
				else if (description.description[i] == '>')
					isBalise = false;
				else {
					if (!isBalise)
						count += 1;
					if (count >= 250) {
						cutMessage = description.description.substr(0, i);
						break;
					}
				}
			}
			$(event.currentTarget.parentElement)[0].innerHTML = cutMessage + ` <span style="font-style: normal"> ... </span><a class='seeMore' post-id=${id} style='font-size: 12px'>` + translation.moduleActualityIndex["see_more"] + `</a>`;
		}
	},
	'click #backToMonth': function(event, template) {
		// back to the month filter when you zoomed on a day
		template.filterDay.set(null);
	},
	'click .descriptionActu': function(event, template) {
		// allow to see more information about an actuality
		const idx = event.currentTarget.getAttribute("idx");
		$(`#descriptionActu${idx}`).toggleClass("hidden");
	},
	'click button[data-close=help-module]': function(event, template) {
		Meteor.call('desactivateHelper', 'information', function(err, res) {
			$('.help-module').fadeOut(200);
		});
	},
  'click #back-btn': (e, t) => {
	  t.detailId.set(false);
  },
  'click .info-item': (e, t) => {
    const id = e.currentTarget.getAttribute('data-id');
    Meteor.call('module-actu-view-by-id', null, id);
  },
  'click .go-to-detail': (e, t) => {
    const id = e.currentTarget.getAttribute('data-id');
    t.detailId.set(id);
  }
});

Template.module_actuality_index.helpers({
	isActuFollowing: () => {
		// allow to know if a user follows an actuality or not
		let resident = Residents.findOne({userId: Meteor.userId()});
		let condoId = FlowRouter.current().params.condo_id;
		if (resident) {
			let condo = resident.condos.find((elem) => {return elem.condoId === condoId});
			if (condo)
				return condo.notifications.actualite;
		}
		else {
			if (gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()}))
				if (user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()}))
					if (condo = user.condosInCharge.find((c) => {return c.condoId == condoId}))
						return condo.notifications.actualite;
				}
			},
			emptyPosts: () => {
		// allow to know if none actuality will be display with the choosen filters (month or day) in the calendar
		let actu = _.filter(_.filter(ActuPosts.find({actualityId: Template.instance().dataActu.actualityId}).fetch(), function(elem) {
			return Template.instance().filter.get() === 0b10 ? elem.startDate
			: Template.instance().filter.get() === 0b1 ? !elem.startDate
			: elem;
		}), function(elem) {
			elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
			elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
			if (elemSDate == 'Invalid Date')
				return true;
			return filterPosts(elemSDate, elemEDate, Template.instance());
		});
		return !actu.length;
	},
	actuPosts: () => {
    moment.locale((FlowRouter.getParam("lang") || "fr"));
		let day = Template.instance().filterDay.get() !== null ? (Template.instance().filterDay.get() < 10 ? '0' : '') + Template.instance().filterDay.get() : moment().format('DD')
		let month = Template.instance().filterMonth.get() !== null ? (Template.instance().filterMonth.get() < 10 ? '0' : '') + (Template.instance().filterMonth.get()) : moment().format('MM')
		let year = Template.instance().filterYear.get() !== null ? Template.instance().filterYear.get() : moment().format('YYYY')
		let currentDay = moment({ day, month, year })
		let actusPost = ActuPosts.find({ actualityId: Template.instance().dataActu.actualityId, startDate: { $ne: '' } }).fetch()
		actusPost = actusPost.filter(event => {
			let eventStart = moment(event.startDate, 'DD/MM/YYYY')
			let eventEnd = moment(event.endDate, 'DD/MM/YYYY')
			if (eventStart.isValid() === true && eventEnd.isValid() === true) {
				return eventStart.isSameOrBefore(currentDay) && eventEnd.isSameOrAfter(currentDay)
			} else if (eventStart.isValid() === true && eventEnd.isValid() === false) {
				return eventStart.isSame(currentDay)
			}
			return false
		})
    const sorted = actusPost.sort((a, b) => moment(a.startDate, 'DD/MM/YYYY').format('x') > moment(a.endDate, 'DD/MM/YYYY').format('x'))
		const instance = Template.instance()
    sorted.forEach((e) => {
      Meteor.call('canResponseToEvent', e._id, (err, res) => {
        if (!err) {
          instance.eventState.set(e._id, res)
        }
      })
    })

    return sorted
	},
	infoPosts: () => {
		// return only informations (actuality with no date)
		return _.filter(_.filter(/*(for pagination) Template.instance().pagination.getResults().fetch()*/ActuPosts.find({actualityId: Template.instance().dataActu.actualityId}, {sort: {createdAt: -1}}).fetch(), function(elem) {
			return !elem.startDate;
		}), function(elem) {
			let regexp = new RegExp(Template.instance().searchInfo.get(), "i");
			return elem.title.match(regexp) || elem.locate.match(regexp) || elem.description.match(regexp);
		});
	},
	isSearchActive: () => {
		return Template.instance().searchInfo.get() !== '';
	},
	infoPostsLen: () => {
		// return how many informations there are
		return _.filter(ActuPosts.find({actualityId: Template.instance().dataActu.actualityId}).fetch(), function(elem) {
			return !elem.startDate;
		}).length;
	},
	actualityId: () => {
		// return the linked actuality collection id
		return Template.instance().dataActu.actualityId;
	},
	editingPost: () => {
		// return the value of editingPost
		return Template.instance().editingPost.get();
	},
	setFilter: () => {
		// set filter for mobile or not
		Template.instance().filter.set($(window).innerWidth() < 768 ? 0b10 : 0b11);
	},
	getFilter: () => {
		// return the value of filter (actuality, information or both)
		return Template.instance().filter.get();
	},
	selectedDate: () => {
		moment.locale((FlowRouter.getParam("lang") || "fr"));
		// return the selected date
		let day = Template.instance().filterDay.get() ? (Template.instance().filterDay.get() < 10 ? '0' : '') + Template.instance().filterDay.get() : moment().format('DD')
		let month = Template.instance().filterMonth.get() ? (Template.instance().filterMonth.get() + 1 < 10 ? '0' : '') + (Template.instance().filterMonth.get() + 1) : moment().format('MM')
		let year = Template.instance().filterYear.get() ? Template.instance().filterYear.get() : moment().format('YYYY')
		let date = moment(day + month + year, 'DD/MM/YYYY')
		return date.format('D MMMM Y')
	},
	sameDate: (startDate, endDate) => {
		let momentStartDate = moment(startDate, 'DD/MM/YYYY');
		let momentEndDate = moment(endDate, 'DD/MM/YYYY');
		if (momentStartDate.isValid() && momentEndDate.isValid()) {
			return momentStartDate.diff(momentEndDate, 'days') === 0
		}
	},
	showPastille: () => {
		// helper to display bubbles in the calendar
		var days = $("#cal table tbody tr td");
		for (var i = 0; i != days.length; i++) {
			if ($(days[i]).hasClass('day') && !$(days[i]).hasClass('old') && !$(days[i]).hasClass('new')) {
				if (_.filter(ActuPosts.find({
						actualityId: Template.instance().dataActu.actualityId
					}).fetch(), function(elem) {
						elemSDate = new Date(`${elem.startDate.split('/')[1]}/${elem.startDate.split('/')[0]}/${elem.startDate.split('/')[2]}`);
						elemEDate = new Date(`${elem.endDate.split('/')[1]}/${elem.endDate.split('/')[0]}/${elem.endDate.split('/')[2]}`);
					return ($(days[i])[0].innerText == elemSDate.getDate() && elemSDate.getMonth() == Template.instance().filterMonth.get() && elemSDate.getFullYear() == Template.instance().filterYear.get())
						|| ($(days[i])[0].innerText == elemEDate.getDate() && elemEDate.getMonth() == Template.instance().filterMonth.get() && elemEDate.getFullYear() == Template.instance().filterYear.get());
				}).length) {
					let date = $(days[i])[0].innerText;
					$(days[i]).html('');
					$(days[i]).css("position", "relative");
					$(days[i]).append("<div style='border: 1px solid #69D2E7!important; width: 100%; height: 100%; position: absolute; left: 0px; top: 0px; border-radius: 24px;'></div>" + date);
				}
			}
		}
		return Template.instance().showPastille.get();
	},
	getPosts: () => {
		return Template.instance().pagination;
	},
	getIndex: (idx) => {
		return idx;
	},
	isEdited: (postId) => {
		let info = ActuPosts.findOne(postId);
		if (info) {
			return (info.isUpdate);
		}
		return (false);
	},
	displayActu: (rightsList) => {
		return rightsList.displayActuality
	},
	isSubscribeReady: () => {
		return Template.instance().subscriptionsReady()
	},
  initDatePicker: () => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const instance = Template.instance();
    const events = ActuPosts.find({
      actualityId: Template.instance().dataActu.actualityId
    }).fetch();

    const eventDate = _.uniq(_.reduce(events, (arr, e) => {
      if (!!e.endDate) {
        const start = moment(e.startDate, 'DD/MM/YYYY').startOf('day');
        const end = moment(e.endDate, 'DD/MM/YYYY').startOf('day');
        const diff = end.diff(start, 'days');
        for (let i = 0; i <= diff; i++) {
          arr.push(start.add(i === 0 ? 0 : 1, 'd').format('x'))
        }
      } else {
        arr.push(moment(e.startDate, 'DD/MM/YYYY').startOf('day').format('x'))
      }

      return arr
    }, []))
    setTimeout(() => {
      initDatePicker(instance, eventDate, lang);
    }, 10);
  },
  setHeight: () => {
    setTimeout(() => {
      $(".mb-info-container").animate({height: window.innerHeight},0);
    }, 10)
  },
    onSearch: () => {
		const template = Template.instance();
		return (val) => {
            let input = val;
            for (let i = 0; i !== Object.keys(regexpSearch).length; i++) {
                input = input.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]]);
            }
            template.searchInfo.set(input);
		}
	},
	MbFilePreview: () => MbFilePreview,
	getFiles: (fileIds) => {
		const files = UserFiles.find({_id: {$in: fileIds.filter(file => !file.fileId)}})

		return files
	},
	getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  canResponse: (id) => {
    return !!Template.instance().eventState.get(id)
  },
  goingTo: (id) => {
    const instance = Template.instance()
    Meteor.call('module-actu-view-by-id', instance.actualityId, instance.detailId.get());
    return () => () => {
      Meteor.call('eventResponse', id, 'going', (err, res) => {
        if (!err) {
          instance.eventState.set(id, false)
        }
        if (!!err && err.error === 602) {
          const translation = new CommonTranslation(FlowRouter.getParam("lang") || "fr")

          bootbox.alert({
            size: "large",
            title: translation.commonTranslation.laundryRoom_availability_alert,
            message: translation.commonTranslation.no_more_even_space,
            backdrop: true
          });
        }
      })
    }
  },
  goToDetail: (id) => {
    const instance = Template.instance()
	  return () => () => {
      Meteor.call('module-actu-view-by-id', null, id);
      instance.detailId.set(id)
    }
  },
  getNewClass: (item) => {
	  return !item.userUpdateView.find(view => view === Meteor.userId()) ? 'is-new': '';
  },
  isEvent: () => {
    const event = ActuPosts.findOne(Template.instance().detailId.get())

    return !!event && !!event.startDate
  },
  isDetail: () => {
	  const instance = Template.instance();
	  const isDetail = !!instance.detailId.get();
	  // if (isDetail) {
    //   Meteor.call('module-actu-view-by-id', instance.actualityId, instance.detailId.get());
    // }
	  return instance.detailId.get()
  },
  canResponseDetail: () => {
	  const event = ActuPosts.findOne(Template.instance().detailId.get())

    return !!event && !!event.allowAction
  },
  getButtonClass: (key) => {
	  const event = ActuPosts.findOne(Template.instance().detailId.get())
    return (!!event && !!event.participantResponse && !!event.participantResponse[key] && !!event.participantResponse[key].find((p) => { return p === Meteor.userId()})) ? 'red' : 'danger'
  }
});
