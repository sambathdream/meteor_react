import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import { CommonTranslation, Email } from '/common/lang/lang'
import moment from 'moment'
import { ForumLang, Home, ModuleActualityIndex, ModuleReservationIndex} from '../../../../../../../common/lang/lang';

Template.ForumCard.onCreated(function () {
  this.elemFiles = new ReactiveVar([])
  // Meteor.subscribe('forumCommentPosts', FlowRouter.getParam('condo_id'), Template.instance().data.post._id)
})

Template.ForumCard.events({
  'click #go-to-detail, .goToDetailPost': function(e) {
    const parent = $(e.target).parents('.mb-file-wrapper');

    if (parent.length === 0) {
      const condoId = FlowRouter.getParam('condo_id');
      const lang = FlowRouter.getParam('lang') || 'fr';
      const postId = Template.instance().data.post._id;

      if (Meteor.isManager) {
        FlowRouter.go("app.gestionnaire.forum.postId", { condo_id: condoId, postId: postId })
      } else {
        FlowRouter.go("app.board.resident.condo.module.forum.post", { lang: lang, condo_id: condoId, postId: postId })
      }
    }
  }
})

Template.ForumCard.helpers({
  getName: (userId, name) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    let user = UsersProfile.findOne({ _id: userId })
    if (!!user) {
      return user[name]
    } else if (name === 'firstname') {
      return translate.commonTranslation.account_deactivated
    }
  },
  parseDate: (isEdited, timestamp, endDate) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const expiredPoll = new ForumLang(lang)
    const homeTranslate = new Home(lang)
    const moduleActualityIndex = new ModuleActualityIndex(lang)
    // const homeTranslate = new Home(lang)
    if (endDate && !isNaN(endDate)) {
      const commonTl = new CommonTranslation(lang)
      let x = new Date();

      if (endDate < x.getTime()) {
        return `${commonTl.commonTranslation.expire_on} ${moment(endDate).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')}`
      } else {
        let e;
        if (isEdited) {
          e = homeTranslate.home.edited
        } else {
          e = moduleActualityIndex.moduleActualityIndex.published_on
        }
        let t = moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
        return `${e} ${t} ${expiredPoll.forumLang.expire_in} ${moment(endDate).locale(lang).fromNow()}`
      }
    }
    return moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  getSubject: () => Template.instance().data.post.subject,
  onVote: () => {
    const poll =  Template.instance().data.post;
    return (index) => {
      Meteor.call(
        'addVote',
        FlowRouter.getParam('condo_id'),
        poll._id,
        index
      );
    }
  },
  getViewCount: (forumId) => {
    let ret = ForumViewsCounter.findOne({ _id: forumId })
    return ret ? ret.counter : 0
  },

/*   getViewCount: () => {
    const views = Template.instance().data.post.views;

    return !!views ? Object.keys(views).length : 0;
  },
 */
  getCommentCount: () => {
    let counter = ForumFeedCounter.findOne({ _id: Template.instance().data.post._id });

    if (typeof counter === 'undefined' || !counter || counter.length === 0)
      return 0
    else
      return counter.counter
  },
  getFiles: () => {
    let files = Template.instance().elemFiles.get()
    return _.map(_.compact(files), (file) => {
      if (file) {
        if (file.type.split("/")[0] === "image") {
          return {
            lowRes: file.versions.thumb100 || file.versions.original,
            preview: file.versions.preview || file.versions.original,
            large: file.versions.large || file.versions.original,
            original: file.versions.original,
            filename: file.name,
            type: 'image',
            downloadPath: file.versions.original
          }
        } else if (file && file.thumb) {
          return {
            lowRes: file.thumb,
            preview: file.thumb,
            large: file.thumb,
            type: 'image',
            filename: file.name,
            downloadPath: `${file.thumb}?download=true`,
            isVideo: true,
            videoPath: file.versions.original,
            videoType: file.type
          }
        } else {
          return {
            lowRes: `/img/icons/svg/${getImageByExtension(file.type)}`,
            preview: `/img/icons/svg/${getImageByExtension(file.type)}`,
            large: `/img/icons/svg/${getImageByExtension(file.type)}`,
            filename: file.name,
            type: 'file',
            downloadPath: file.versions.original
          }
        }
      }
    })
  },
  MbFilePreview: () => MbFilePreview,
  onMore: () => {
    const condoId = FlowRouter.getParam('condo_id');
    const lang = FlowRouter.getParam('lang') || 'fr';
    const postId = Template.instance().data.post._id;

    return () => {
      if (Meteor.isManager) {
        FlowRouter.go("app.gestionnaire.forum.postId", { condo_id: condoId, postId: postId })
      } else {
        FlowRouter.go("app.board.resident.condo.module.forum.post", { lang: lang, condo_id: condoId, postId: postId })
      }
    }
  },
  showExpireDate: ({endDate}) => {
    if (endDate && !isNaN(endDate)) {

      const lang = FlowRouter.getParam('lang') || 'fr'
      const translate = new CommonTranslation(lang)
      return `${translate.commonTranslation['expire_on']} ${moment(endDate).locale(lang).format('lll')}`
    }
  },
  hasFiles: (hasFiles) => {
    return !!hasFiles
  },
  getFilesFromServer: (filesId) => {
    let template = Template.instance()
    if (filesId && filesId.length > 0) {
      Meteor.call('getForumFiles', filesId.filter(file => typeof file === 'string'), (error, result) => {
        if (!error && result && Object.keys(result).length > 0) {
          template.elemFiles.set(result)
        }
      })
    }
  },
  isUser: (userId) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const t = new ModuleReservationIndex(lang)
    if (userId === Meteor.userId()) {
      return '(' + t.moduleReservationIndex.you + ')'
    }
    return ''
  },
  newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  isEdited: (postId) => {
    let post = ForumFeed.findOne(postId)
    return post.editedAt !== null
	}
})
