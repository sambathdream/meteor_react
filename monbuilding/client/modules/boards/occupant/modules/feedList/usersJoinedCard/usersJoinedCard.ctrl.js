import moment from 'moment'
import { Email, ResourceForm } from './../../../../../../../common/lang/lang'

Template.UsersJoinedCard.onCreated(function () {

})

Template.UsersJoinedCard.onRendered(function () {

})

Template.UsersJoinedCard.events({

})

Template.UsersJoinedCard.helpers({
  parseJoinedDate: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    return moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  parseDate (date) {
    if (date === undefined || date == null || !moment(date).isValid()) {
      return
    }
    const lang = FlowRouter.getParam('lang') || 'fr'
    const today = moment().locale(lang)
    const currentDate = moment(date).clone()
    if (currentDate === undefined || currentDate == null) {
      return
    }
    const difDays = today.diff(currentDate, 'days')
    let resp = null
    if (difDays < 2) {
      resp = moment(currentDate).locale(lang).fromNow()
    } else {
      if (today.isSame(currentDate, 'year') !== true) {
        resp = (lang === 'fr' ? moment(currentDate).locale(lang).format('Do MMMM YYYY') : moment(currentDate).locale(lang).format('Do MMMM YYYY'))
      } else {
        resp = (lang === 'fr' ? moment(currentDate).locale(lang).format('Do MMMM') : moment(currentDate).locale(lang).format('MMMM Do'))
      }
      resp = (lang === 'fr' ? 'le' : 'on') + ' ' + resp
    }
    resp = resp.charAt(0).toUpperCase() + resp.slice(1)
    return resp
  },
  isUser: (userId) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new ResourceForm(lang)
    if (userId === Meteor.userId())
      return '(' + translate.resourceForm.you + ')'
    return ''
  },
  getName: (userId, name) => {
    let user = UsersProfile.findOne({ _id: userId })
    if (!!user) {
      return user[name]
    }
  },
})
