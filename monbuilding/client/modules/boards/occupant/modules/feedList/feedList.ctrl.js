Template.feedList.onCreated(function () {
  this.pageCounter = new ReactiveVar(30)
  this.totalElem = null
  this.requestDate = new ReactiveVar(Date.now())  // Actu, Forum, Classified, UsersJoined
  this.oldWayRequestDate = new Date() // Incident
  this.recentlyJoinedUsers = new ReactiveVar([])
  this.mostViewedAds = new ReactiveVar([])
  this.mostViewedForum = new ReactiveVar([])

  this.elemRequested = {
    incidentCounter: 0,
    forumCounter: 0,
    classifiedCounter: 0,
    actuCounter: 0,
    usersJoinedCounter: 0
  }

  this.elemDisplayed = {
    Incident: 0,
    Forum: 0,
    Classified: 0,
    Actu: 0,
    UsersJoined: 0
  }

  Meteor.subscribe('classifiedsFeedAnswerCounter', [FlowRouter.getParam('condo_id')])
  Meteor.subscribe('forumFeedAnswerCounter', [FlowRouter.getParam('condo_id')])
  Meteor.subscribe('feedIncidentInformation', [FlowRouter.getParam('condo_id')]);

  this.newPostFeedHandler = this.subscribe('newPostFeed', FlowRouter.getParam('condo_id'), this.requestDate.get(), this.oldWayRequestDate)
  let self = this
  this.autorun(function() {
    const pageCounter = self.pageCounter.get()
    const requestedDate = self.requestDate.get()
    const previousRequested = self.elemRequested

    if (previousRequested.incidentCounter === 0) {
      self.elemRequested.incidentCounter = 30
      self.elemRequested.forumCounter = 30
      self.elemRequested.classifiedCounter = 30
      self.elemRequested.actuCounter = 30
      self.elemRequested.usersJoinedCounter = 30

    } else {
      self.elemRequested.incidentCounter = (previousRequested.incidentCounter - self.elemDisplayed.Incident) > 10 ? previousRequested.incidentCounter : previousRequested.incidentCounter + (10 - (previousRequested.incidentCounter - self.elemDisplayed.Incident))
      self.elemRequested.forumCounter = (previousRequested.forumCounter - self.elemDisplayed.Forum) > 10 ? previousRequested.forumCounter : previousRequested.forumCounter + (10 - (previousRequested.forumCounter - self.elemDisplayed.Forum))
      self.elemRequested.classifiedCounter = (previousRequested.classifiedCounter - self.elemDisplayed.Classified) > 10 ? previousRequested.classifiedCounter : previousRequested.classifiedCounter + (10 - (previousRequested.classifiedCounter - self.elemDisplayed.Classified))
      self.elemRequested.actuCounter = (previousRequested.actuCounter - self.elemDisplayed.Actu) > 10 ? previousRequested.actuCounter : previousRequested.actuCounter + (10 - (previousRequested.actuCounter - self.elemDisplayed.Actu))
      self.elemRequested.usersJoinedCounter = (previousRequested.usersJoinedCounter - self.elemDisplayed.UsersJoined) > 10 ? previousRequested.usersJoinedCounter : previousRequested.usersJoinedCounter + (10 - (previousRequested.usersJoinedCounter - self.elemDisplayed.UsersJoined))
    }

    Meteor.call('recentlyJoinedUsers', FlowRouter.getParam('condo_id'), function(error, result) {
      if (!error && result) {
        self.recentlyJoinedUsers.set(result)
      }
    })
    Meteor.call('mostViewedAds', FlowRouter.getParam('condo_id'), function(error, result) {
      if (!error && result) {
        self.mostViewedAds.set(result)
      }
    })
		Meteor.call('mostViewedForum', FlowRouter.getParam('condo_id'), function(error, result) {
			if (!error && result) {
				self.mostViewedForum.set(result)
			}
		})
    self.subscribe('feedIncident', FlowRouter.getParam('condo_id'), self.elemRequested.incidentCounter, self.oldWayRequestDate)
    self.subscribe('feedForum', FlowRouter.getParam('condo_id'), self.elemRequested.forumCounter, requestedDate)
    self.subscribe('feedClassified', FlowRouter.getParam('condo_id'), self.elemRequested.classifiedCounter, requestedDate)
    self.subscribe('feedActu', FlowRouter.getParam('condo_id'), self.elemRequested.actuCounter, requestedDate)
    self.subscribe('feedUsersJoined', FlowRouter.getParam('condo_id'), self.elemRequested.usersJoinedCounter, requestedDate)
  });

	Meteor.call('setNewAnalytics', {type: "module", module: "feed", accessType: "web", condoId: FlowRouter.getParam('condo_id')});
})

Template.feedList.onDestroyed(function () {
	// Meteor.call('updateAnalytics', {type: "module", module: "feed", accessType: "web", condoId: FlowRouter.getParam('condo_id')})
})

Template.feedList.events({
  'scroll .blue_bg_content': function(event, template) {
    if (event.currentTarget.offsetHeight + event.currentTarget.scrollTop == event.currentTarget.scrollHeight) {
      if (template.totalElem === null || template.totalElem >= template.pageCounter.get()) {
        template.pageCounter.set(template.pageCounter.get() + 10)
      }
    }
  },
  'click .feedNewPostCounter': function(event, template) {
    $('.blue_bg_content').animate({ scrollTop: 0 }, 500);
    template.elemRequested = {
      incidentCounter: 0,
      forumCounter: 0,
      classifiedCounter: 0,
      actuCounter: 0,
      usersJoinedCounter: 0
    }

    template.elemDisplayed = {
      Incident: 0,
      Forum: 0,
      Classified: 0,
      Actu: 0,
      UsersJoined: 0
    }
    template.requestDate.set(Date.now())  // Actu, Forum, Classified, UsersJoined
    template.oldWayRequestDate = new Date() // Incident
    template.newPostFeedHandler.stop()
    template.newPostFeedHandler = template.subscribe('newPostFeed', FlowRouter.getParam('condo_id'), template.requestDate.get(), template.oldWayRequestDate, (error, result) => {
      template.pageCounter.set(30)
    })
  },
  'click .goToAds': function() {
    const params = {
      lang: (FlowRouter.getParam("lang") || "fr"),
      condo_id: FlowRouter.getParam("condo_id"),
      module_slug: "annonces"
    };
    FlowRouter.go('app.board.resident.condo.module', params);
  },
	'click #goToForum': function() {
		const params = {
			lang: (FlowRouter.getParam("lang") || "fr"),
			condo_id: FlowRouter.getParam("condo_id"),
			module_slug: "forum"
		};
		FlowRouter.go('app.board.resident.condo.module', params);
  },
  'click .card-forum-post': function(event, template) {
    const params = {
      lang: (FlowRouter.getParam("lang") || "fr"),
      condo_id: FlowRouter.getParam("condo_id"),
      postId: event.currentTarget.getAttribute('data-postId') // this = post from #each template
    };
    FlowRouter.go('app.board.resident.condo.module.forum.post', params);
  },
  'click .card-ads-post': function (event, template) {
    const params = {
      lang: (FlowRouter.getParam("lang") || "fr"),
      condo_id: FlowRouter.getParam("condo_id"),
      ad_id: event.currentTarget.getAttribute('data-adId') // this = post from #each template
    };
    FlowRouter.go('app.board.resident.condo.module.classifieds.ad', params);
  }
})

Template.feedList.helpers({
  isSubscribeReady: () => {
    let subsReady = Template.instance().subscriptionsReady()
    if (subsReady) {
      // console.log('new Date()3', moment(Date.now()).format("ssSSSS"))
    }
    return subsReady
  },
  getFeedElements: () => {
    let incidentsElem = Incidents.find()
    let forumsElem = ForumFeed.find()
    let classifiedsElem = ClassifiedsAds.find()
    let actusElem = ActuPosts.find()
    let usersJoinedElem = UsersJoinedDate.find()
    Template.instance().elemDisplayed = {
      Incident: 0,
      Forum: 0,
      Classified: 0,
      Actu: 0,
      UsersJoined: 0
    }

    let allElements = []
    let template = Template.instance()
    if (incidentsElem.count()) {
      allElements = [...allElements, ..._.map(incidentsElem.fetch(), (elem) => {
        // elem.feedDate = parseInt(moment(elem.lastUpdate).format('x'))
        elem.feedDate = elem.lastUpdate ? parseInt(moment(elem.lastUpdate).format('x')) : parseInt(moment(elem.createdAt).format('x'))
        elem.createdAt = parseInt(moment(elem.createdAt).format('x'))
        elem.feedType = 'Incident'
        delete elem.lastUpdate
        if (elem && elem.history) {
          let length = elem.history.length - 1
          if (elem.history[length] && elem.history[length].share && elem.history[length].share.declarer && elem.history[length].share.declarer.files && elem.history[length].share.declarer.files.length > 0) {
            elem.filesId = elem.history[length].share.declarer.files
            if (elem.filesId.length > 0) {
              elem.hasFiles = true
            }
          }
          delete elem.history
        }
        return elem
      })]
    }
    if (forumsElem.count()) {
      allElements = [...allElements, ..._.map(forumsElem.fetch(), (elem) => {
        elem.feedDate = elem.updatedAt ? elem.updatedAt : elem.date
        elem.feedType = 'Forum'
        delete elem.updatedAt
        if (elem && elem.filesId) {
          if (elem.filesId.length > 0) {
            elem.hasFiles = true
          }
        }
        return elem
      })]
    }
    if (classifiedsElem.count()) {
      allElements = [...allElements, ..._.map(classifiedsElem.fetch(), (elem) => {
        elem.feedDate = elem.updatedAt ? elem.updatedAt : elem.createdAt
        elem.feedType = 'Classified'
        delete elem.updatedAt
        if (elem && elem.files) {
          if (elem.files.length > 0) {
            elem.hasFiles = true
          }
          elem.filesId = elem.files
          delete elem.files
        }
        return elem
      })]
    }
    if (actusElem.count()) {
      allElements = [...allElements, ..._.map(actusElem.fetch(), (elem) => {
        elem.feedDate = elem.updatedAt ? elem.updatedAt : elem.createdAt
        elem.feedType = 'Actu'
        delete elem.updatedAt
        if (elem && elem.files) {
          elem.filesId = elem.files
          if (elem.files.length > 0) {
            elem.hasFiles = true
          }
          delete elem.files
        }
        return elem
      })]
    }
    if (usersJoinedElem.count()) {
      allElements = [...allElements, ..._.map(usersJoinedElem.fetch(), (elem) => {
        elem.feedDate = elem.joined
        elem.feedType = 'UsersJoined'
        delete elem.joined
        return elem
      })]
    }
    let pageCounter = Template.instance().pageCounter.get()
    let totalCount = 0
    let incidentCount = 0
    let forumCount = 0
    let classifiedCount = 0
    let actuCount = 0
    let usersJoinedCount = 0

    allElements.sort((a, b) => b.feedDate - a.feedDate)
    // console.log('allElements', allElements)
    // let ret = _.sortBy(allElements, 'feedDate').reverse()
    Template.instance().totalElem = allElements.length
    return allElements.slice(0, pageCounter)
  },
  incrementElemDisplayed: (type) => {
    Template.instance().elemDisplayed[type] = Template.instance().elemDisplayed[type] + 1
  },
  feedUserHasRight: (data, right) => {
    if (!data) {
      return false
    }
    switch (data.feedType) {
      case 'Classified':
        return right.Classified.see
        break;

      case 'Forum':
        return data.type === 'post' ? right[data.feedType].seePost : right[data.feedType].seePost
        break;

      case 'Actu':
        return right.Actu.see
        break;

      case 'Incident':
        return right.Incident.see
        break;

      case 'UsersJoined':
        return right.UsersJoined.see
        break;

      default:
        return false
        break;
    }
  },
  getFeedRight: () => {
    let condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})

    return {
      Classified: {
        see: condo && condo.settings && condo.settings.options && condo.settings.options.classifieds && Meteor.userHasRight('ads', 'see')
      },
      Forum: {
        seePost: condo && condo.settings && condo.settings.options && condo.settings.options.forum && Meteor.userHasRight('forum', 'seeSubject'),
        seePoll: condo && condo.settings && condo.settings.options && condo.settings.options.forum && Meteor.userHasRight('forum', 'seePool')
      },
      Actu: {
        see: condo && condo.settings && condo.settings.options && condo.settings.options.informations && Meteor.userHasRight('actuality', 'see')
      },
      Incident: {
        see: condo && condo.settings && condo.settings.options && condo.settings.options.incidents && Meteor.userHasRight('incident', 'see')
      },
      UsersJoined: {
        see: true //Meteor.userHasRight('')
      },
    }
  },
  getCardTemplate: (type) => {
    return type + "Card"
  },
  getData: (element) => {
    return {post: element}
  },
  getNewPostCounter: () => {
    let condoId = FlowRouter.getParam('condo_id')
    let counter = FeedNewPostsCounter.findOne({ _id: condoId })
    if (!counter || counter._id !== condoId || counter.newPosts === 0) {
      $('.feedNewPostCounter').css('display', 'none')
      return 0
    } else {
      $('.feedNewPostCounter').css('display', 'block')
      return counter.newPosts
    }
  },
  getRecentlyJoined: () => {
    let recentlyJoined = Template.instance().recentlyJoinedUsers.get()
    return recentlyJoined.map((item) => {
      let thisUser = UsersProfile.findOne(item.userId)
      return { _id: thisUser._id, lastname: thisUser.lastname, firstname: thisUser.firstname, joined: parseInt(moment(item.joined).format('x')) }
    })
  },
  showEditedTag: (ad) => {
    return !!ad.editedAt
  },
  mostViewedAds: () => {
    return Template.instance().mostViewedAds.get()
  },
	mostViewedForum: () => {
    return Template.instance().mostViewedForum.get();
  },
  showCondoImage: () => {
    const condo = Condos.findOne(FlowRouter.getParam('condo_id'));
    return !!condo && !!condo.settings && !!condo.settings.showPicture
  },
  getCondoPic: () => {
    let avatar = CondoPhotos.findOne({ condoId: FlowRouter.getParam('condo_id') })
    if (avatar && avatar.avatar) {
      return avatar.avatar.preview || avatar.avatar.large || avatar.avatar.thumb100
    }
    return null
  },
})
