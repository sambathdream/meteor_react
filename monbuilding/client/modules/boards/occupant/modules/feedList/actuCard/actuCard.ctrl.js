import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import { Email, ModuleReservationIndex, CommonTranslation } from './../../../../../../../common/lang/lang'
import moment from 'moment'

Template.ActuCard.onCreated(function () {
  this.elemFiles = new ReactiveVar([])
})

Template.ActuCard.events({
  'click .mb-post-content': (event, template) => {
    const eventId = event.currentTarget.getAttribute('data-id')
    Meteor.call('module-actu-view-by-id', null, eventId);
    Session.set('eventDetailId', eventId);
    if (!$(event)[0].isDefaultPrevented()) {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const params = {
        lang: lang,
        condo_id: FlowRouter.getParam("condo_id"),
        module_slug: 'information'
      };
      FlowRouter.go("app.board.resident.condo.module", params)
    }
  },
  'click .fileViewerTemplate': (event, template) => {
    event.preventDefault()
  },

})

Template.ActuCard.helpers({
  renderDate: (d) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new Email(lang)
    let hour = ''
    if (!d) {
      return ''
    }
    let startDate = d.startDate + ''
    let endDate = d.endDate + ''
    let startHour = d.startHour + ''
    let endHour = d.endHour + ''
    if (!endDate || startDate === endDate) {
      if (startHour !== '-1' && endHour !== '-1' && startHour.split(':').length > 0) {
        hour = (startHour.split(':')[0] < 10) ? ('', startHour.split(':')[0]) : startHour.split(':')[0]
        hour += `:${startHour.split(':')[1]}`
      }
      if (startHour !== '-1' && (endHour === '-1' || endHour === '')) {
        hour = `${translation.email['_at_']} ${startHour}`
      } else if (startHour !== '-1' && endHour !== '-1') {
        hour = `${translation.email['_from_']} ${startHour} ${translation.email['_to_'].toLowerCase()} ${endHour}`
      } else if (startHour === '-1') {
        hour = ''
      }
      return `${moment(startDate, 'DD/MM/YYYY').format('DD/MM')} ${hour}`
    }
    if (endDate) {
      let startMonth = moment(startDate, 'DD/MM/YYYY').format(('MM'))
      let endMonth = moment(endDate, 'DD/MM/YYYY').format(('MM'))
      // check start date and end date has the same month show 3-5/4
      if (startMonth === endMonth) {
        hour = (startHour === '-1' && endHour === '-1') ? '' : `${translation.email['_from_']} ${startHour} ${translation.email['_to_'].toLowerCase()} ${endHour}`
        return `${moment(startDate, 'DD/MM/YYYY').format('DD')}-${moment(endDate, 'DD/MM/YYYY').format('DD/MM')} ${hour}`
      } else { // check if start date and end date not has the same month 3/4-4/5
        hour = (startHour === '-1' && endHour === '-1') ? '' : `${translation.email['_from_']} ${startHour} ${translation.email['_to_'].toLowerCase()} ${endHour}`
        return `${moment(startDate, 'DD/MM/YYYY').format('DD/MM')}-${moment(endDate, 'DD/MM/YYYY').format('DD/MM')} ${hour}`
      }
    }
  },
  getName: (userId, name) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new CommonTranslation(lang)

    let user = UsersProfile.findOne({ _id: userId })

    if (!!user) {
      return user[name];
    } else {
      if (name === 'firstname') {
        return translation.commonTranslation['account_deactivated']
      } else {
        return '';
      }
    }
  },
  parseDate: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    return moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  stripHtml: (html) => {
    return html.replace(/<(?:.|\n)*?>/gm, '');
  },
  getFiles: () => {
    let files = Template.instance().elemFiles.get()
    return _.map(_.compact(files), (file) => {
      if (file) {
        if (file.type.split("/")[0] === "image") {
          return {
            lowRes: file.versions.thumb100 || file.versions.original,
            preview: file.versions.preview || file.versions.original,
            large: file.versions.large || file.versions.original,
            original: file.versions.original,
            filename: file.name,
            type: 'image',
            downloadPath: file.versions.original
          }
        } else if (file && file.thumb) {
          return {
            lowRes: file.thumb || file.versions.original,
            preview: file.thumb || file.versions.original,
            large: file.thumb || file.versions.original,
            type: 'image',
            filename: file.name,
            downloadPath: `${file.thumb}?download=true`,
            isVideo: true,
            videoPath: file.versions.original,
            videoType: file.type
          }
        } else {
          return {
            lowRes: `/img/icons/svg/${getImageByExtension(file.type)}`,
            preview: `/img/icons/svg/${getImageByExtension(file.type)}`,
            large: `/img/icons/svg/${getImageByExtension(file.type)}`,
            filename: file.name,
            type: 'file',
            downloadPath: file.versions.original
          }
        }
      }
    })
  },
  newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  MbFilePreview: () => MbFilePreview,
  hasFiles: (hasFiles) => {
    return !!hasFiles
  },
  getFilesFromServer: (filesId) => {
    let template = Template.instance()

    if (filesId && filesId.length > 0) {
      Meteor.call('getActusFiles', filesId.filter(file => typeof file === 'string'), (error, result) => {
        if (!error && result && Object.keys(result).length > 0) {
          template.elemFiles.set(result)
        }
      })
    }
  },
  isEdited: (postId) => {
    let info = ActuPosts.findOne(postId)
    return info && info.createdAt !== info.updatedAt
  },
  // onMore: () => {
  //   const lang = (FlowRouter.getParam("lang") || "fr");
  //   const params = {
  //     lang: lang,
  //     condo_id: FlowRouter.getParam("condo_id"),
  //     module_slug: 'information'
  //   };
  //   return () => FlowRouter.go("app.board.resident.condo.module", params)
  // },
  getViewCount: (postId) => {
    let ret = ActuPosts.findOne({_id: postId}) || {}
    return ret.viewsCount || 0
  },
  getCommentCount: (postId) => {
    let ret = ActuPosts.findOne({_id: postId}) || {}
    return ret.commentCount || 0
  },
  eventResponse: (postId, response) => {
    return () => () => {
      Meteor.call('eventResponse', postId, response, (err, res) => {
        if (!!err && err.error === 602) {
          const translation = new CommonTranslation(FlowRouter.getParam("lang") || "fr")

          bootbox.alert({
            size: "large",
            title: translation.commonTranslation.laundryRoom_availability_alert,
            message: translation.commonTranslation.no_more_even_space,
            backdrop: true
          });
        }
      })
    }
  },
  isUser: (userId) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const t = new ModuleReservationIndex(lang)
    if (userId === Meteor.userId()) {
      return '(' + t.moduleReservationIndex.you + ')'
    }
    return ''
  },
  getButtonClass: (key, participantResponse) => {
    return (!!participantResponse && !!participantResponse[key] && !!participantResponse[key].find((p) => { return p === Meteor.userId()})) ? 'red' : 'danger'
  }
})
