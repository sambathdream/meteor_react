import { ModuleClassifiedsList } from "/common/lang/lang.js"
import { Email, ModuleReservationIndex } from './../../../../../../../common/lang/lang'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import moment from 'moment'

Template.ClassifiedCard.onCreated(function () {
  this.elemFiles = new ReactiveVar([])

})

Template.ClassifiedCard.events({
  'click .mb-goToAd': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr")
    const params = {
      lang,
      condo_id: FlowRouter.getParam('condo_id'),
      ad_id: e.currentTarget.getAttribute('data-ad-id')
    }

    FlowRouter.go('app.board.resident.condo.module.classifieds.ad', params);
  },
})

Template.ClassifiedCard.helpers({
  getName: (userId, name) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    let translation = new ModuleClassifiedsList((FlowRouter.getParam("lang") || "fr"));

    let user = UsersProfile.findOne({ _id: userId })
    if (!!user) {
      return user[name]
    } else if (name === 'firstname') {
      return translation.moduleClassifiedsList['deactivate_account']
    }
  },
  parseAdCreated: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    return moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  stripHtmlTag: (text) => {
    return text && text.replace(/<(.|\n)*?>/g, '');
  },
  getCounter: (adId) => {
    let counter = ClassifiedsFeedCounter.findOne({ _id: adId });
    if (counter == undefined || !counter || counter.length == 0)
      return 0
    else
      return counter.counter
  },
  nbViews: (classifiedId) => {
    let ret = ClassifiedsAds.findOne(classifiedId)
    return ret ? ret.viewsCount || 0 : 0
  },
  isNewAd: (ad) => {
    return false
  },

/*   nbViews: (views) => {
    let nb = views ? Object.keys(views).length : 0
    return nb;
  },
  isNewAd: (ad) => {
    let newState = true
    let exist = ad.views[Meteor.userId()]
    if (exist !== undefined && (!ad.feedDate || ad.feedDate < exist)) {
      newState = false
    }

    return newState
  },
 */
  getFiles: () => {
    let files = Template.instance().elemFiles.get()
    return _.map(_.compact(files), (file) => {
      if (file) {
        if (file.type.split("/")[0] === "image") {
          return {
            lowRes: file.versions.thumb100 || file.versions.original,
            preview: file.versions.preview || file.versions.original,
            large: file.versions.large || file.versions.original,
            original: file.versions.original,
            filename: file.name,
            type: 'image',
            downloadPath: file.versions.original
          }
        } else if (file && file.thumb) {
          return {
            lowRes: file.thumb,
            preview: file.thumb,
            large: file.thumb,
            type: 'image',
            filename: file.name,
            downloadPath: `${file.thumb}?download=true`,
            isVideo: true,
            videoPath: file.versions.original,
            videoType: file.type
          }
        } else {
          return {
            lowRes: `/img/icons/svg/${getImageByExtension(file.type)}`,
            preview: `/img/icons/svg/${getImageByExtension(file.type)}`,
            large: `/img/icons/svg/${getImageByExtension(file.type)}`,
            filename: file.name,
            type: 'file',
            downloadPath: file.versions.original
          }
        }
      }
    })
  },
  MbFilePreview: () => MbFilePreview,
  hasFiles: (hasFiles) => {
    return !!hasFiles
  },
  getFilesFromServer: (filesId) => {
    let template = Template.instance()
    if (filesId && filesId.length > 0) {
      Meteor.call('getClassifiedsFiles', filesId.filter(file => typeof file === 'string'), (error, result) => {
        if (!error && result && Object.keys(result).length > 0) {
          template.elemFiles.set(result)
        }
      })
    }
  },
  newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  isEdited: (postId) => {
    let post = ClassifiedsAds.findOne(postId)
    return post.createdAt !== post.updatedAt
  },
  isUser: (userId) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const t = new ModuleReservationIndex(lang)
    if (userId === Meteor.userId()) {
      return '(' + t.moduleReservationIndex.you + ')'
    }
    return ''
  },
  // onMore: (ad_id) => {
  //   const lang = (FlowRouter.getParam("lang") || "fr")
  //   const params = {
  //     lang,
  //     condo_id: FlowRouter.getParam('condo_id'),
  //     ad_id: ad_id
  //   }
  //   return () => FlowRouter.go('app.board.resident.condo.module.classifieds.ad', params);
  // }
})
