import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
import { Email, ModuleReservationIndex } from './../../../../../../../common/lang/lang'
import moment from 'moment'

Template.IncidentCard.onCreated(function () {
  this.elemFiles = new ReactiveVar([])

})

Template.IncidentCard.events({
  'click .mb-post-content, click .go-to-detail': (event, template) => {
    if (!$(event)[0].isDefaultPrevented()) {
      const lang = (FlowRouter.getParam("lang") || "fr");
      const params = {
        lang: lang,
        condo_id: FlowRouter.getParam("condo_id"),
        incidentId: $(event.currentTarget).attr("incidentId")
      };
      FlowRouter.go("app.board.resident.condo.module.incident", params)
    }
  },
  'click .fileViewerTemplate': (event, template) => {
    event.preventDefault()
  },

})

Template.IncidentCard.helpers({
  parseDate: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    return moment(timestamp).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  getFiles: () => {
    let files = Template.instance().elemFiles.get()
    return _.map(_.compact(files), (file) => {
      if (file) {
        if (file.type.split("/")[0] === "image") {
          return {
            lowRes: file.versions.thumb100 || file.versions.original,
            preview: file.versions.preview || file.versions.original,
            large: file.versions.large || file.versions.original,
            original: file.versions.original,
            filename: file.name,
            type: 'image',
            downloadPath: file.versions.original
          }
        } else if (file && file.thumb) {
          return {
            lowRes: file.thumb,
            preview: file.thumb,
            large: file.thumb,
            type: 'image',
            filename: file.name,
            downloadPath: `${file.thumb}?download=true`,
            isVideo: true,
            videoPath: file.versions.original,
            videoType: file.type
          }
        } else {
            return {
              lowRes: `/img/icons/svg/${getImageByExtension(file.type)}`,
              preview: `/img/icons/svg/${getImageByExtension(file.type)}`,
              large: `/img/icons/svg/${getImageByExtension(file.type)}`,
              filename: file.name,
              type: 'file',
              downloadPath: file.versions.original
            }
        }
      }
    })
  },
  MbFilePreview: () => MbFilePreview,
  hasFiles: (hasFiles) => {
    return !!hasFiles
  },
  getFilesFromServer: (filesId) => {
    let template = Template.instance()
    if (filesId && filesId.length > 0) {
      Meteor.call('getIncidentFiles', filesId.filter(file => typeof file === 'string'), (error, result) => {
        if (!error && result && Object.keys(result).length > 0) {
          template.elemFiles.set(result)
        }
      })
    }
  },
  newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  isUserDeclarer: function () {
    const post = this.post
    return post.declarer.userId === Meteor.userId()
  },
  isUser: (userId) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const t = new ModuleReservationIndex(lang)
    if (userId === Meteor.userId()) {
      return '(' + t.moduleReservationIndex.you + ')'
    }
    return ''
  },
  onMore: (postId) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const params = {
      lang: lang,
      condo_id: FlowRouter.getParam("condo_id"),
      incidentId: postId
    };
    return () => () => {
      FlowRouter.go("app.board.resident.condo.module.incident", params)
    }
  }
})
