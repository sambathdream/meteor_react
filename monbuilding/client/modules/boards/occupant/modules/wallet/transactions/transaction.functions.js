import { CommonTranslation } from '/common/lang/lang';

export default setTotalValues = function (basket) {
  const services = basket.services
  const isBook = basket.isBook
  const lang = FlowRouter.getParam('lang') || 'fr'
  const translate = new CommonTranslation(lang)
  let differentVat = []
  let subtotal = 0
  let total = 0

  const vats = {
    '2.1': null,
    '5.5': null,
    '10': null,
    '20': null
  }

  if (services) {
    services.forEach(service => {
      if (isBook && !service.pay_outside_platform) return
      if (!isBook && service.pay_outside_platform) return
      if (service.quantity) {
        if (service.price === null) {
          // return 'FREE'
        } else if (service.price !== '0') {
          const vatNum = 1 + (parseFloat(service.vat) / 100)
          subtotal += (service.price / vatNum) * service.quantity
          total += service.price * service.quantity
          const vatPrice = (service.price - (service.price / vatNum)) * service.quantity
          if (vats[service.vat] === null) {
            vats[service.vat] = vatPrice
          } else {
            vats[service.vat] += vatPrice
          }
        } else {
          if (vats[service.vat] === null) {
            vats[service.vat] = 0 // to remove null value :)
          }
        }
      }
    })
  }
  if (vats['2.1'] !== null) {
    differentVat.push({
      title: translate.commonTranslation.vat + ' 2.1%',
      value: vats['2.1'].toFixed(2) + ' €'
    })
  }
  if (vats['5.5'] !== null) {
    differentVat.push({
      title: translate.commonTranslation.vat + ' 5.5%',
      value: vats['5.5'].toFixed(2) + ' €'
    })
  }
  if (vats['10'] !== null) {
    differentVat.push({
      title: translate.commonTranslation.vat + ' 10%',
      value: vats['10'].toFixed(2) + ' €'
    })
  }
  if (vats['20'] !== null) {
    differentVat.push({
      title: translate.commonTranslation.vat + ' 20%',
      value: vats['20'].toFixed(2) + ' €'
    })
  }
  basket.differentVat = differentVat
  basket.subtotal = subtotal.toFixed(2)
  basket.total = total.toFixed(2)
}
