
import { CommonTranslation } from "/common/lang/lang.js"
import { MarketBasket } from '/common/collections/marketPlace'
// import { MarketReservations } from '/common/collections/marketPlace'
// import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact'
// import PayzenWrapper from '/client/components/PayZen/PayzenWrapper'
import setTotalValues from '../../../marketBasket/basket.functions'

Template.marketBasketSummary.onCreated(function () {
  this.isOpen = new ReactiveVar(false)

  this.amount = new ReactiveVar(0)
  this.billingAddress = new ReactiveVar(null)
  this.paymentError = new ReactiveVar(null)
})

Template.marketBasketSummary.onDestroyed(() => {
})

Template.marketBasketSummary.onRendered(() => {
})

Template.marketBasketSummary.events({
  'hidden.bs.modal #modal_market_basket_summary': (event, template) => {
    template.isOpen.set(false)

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_market_basket_summary': (event, template) => {
    template.isOpen.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_market_basket_summary";

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_market_basket_summary") {
        modal.modal('hide');
      }
    }
  },
  'click .button-ok': (e, t) => {
    $('#modal_market_basket_summary').modal('hide')
  }
})

Template.marketBasketSummary.helpers({
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  getModalTitle: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    return translate.commonTranslation.summary
  },
  getUserName: () => {
    const user = Meteor.user()
    return user.profile.firstname + ' ' + user.profile.lastname
  },
  getBuildingAddress: () => {
    const billingAddress = Template.instance().billingAddress.get()
    if (billingAddress) {
      return billingAddress.address + ', ' + billingAddress.zipCode + ' ' + billingAddress.city + ', ' + billingAddress.country
    }

    const condoId = FlowRouter.getParam('condo_id')
    const thisCondo = Condos.findOne({ _id: condoId })

    if (thisCondo) {
      return thisCondo.getAddress()
    } else {
      return '-'
    }
  },
  getBasket: () => {
    const basketId = Template.currentData().basketId
    const isBook = Template.currentData().isBook
    const basket = MarketBasket.findOne({ _id: basketId })
    let retBasket = {
      hasPaid: false,
      subtotal: 0,
      isBook,
      differentVat: [],
      total: 0,
      services: []
    }

    retBasket.services = basket.services.filter(s => {
      if ((isBook && !s.pay_outside_platform) || (!isBook && s.pay_outside_platform)) return false
      return true
    })
    setTotalValues(retBasket)
    return retBasket
  },
  getTransactionId: () => Template.currentData().transactionId,
  getPaidDate: (services) => {
    return (services.find(s => !s.pay_outside_platform) || {}).validatedAt
  }
})
