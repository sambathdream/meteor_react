import { CommonTranslation } from "/common/lang/lang.js"
import { UserTransactions } from '/common/collections/Payments'
import { MarketBasket } from '/common/collections/marketPlace'
import setTotalValues from './transaction.functions'
Template.wallet_transactions.onCreated(function () {
  this.searchText = new ReactiveVar('')
  this.selectedBasketId = new ReactiveVar(null)
  this.transactionId = new ReactiveVar(null)

  Meteor.subscribe('market_place_all_basket', FlowRouter.getParam('condo_id'));
})

Template.wallet_transactions.onRendered(function () {
})

Template.wallet_transactions.events({
  'click .donwload-invoice': function(e, t){
    const transaction = this
    const condoId = FlowRouter.getParam('condo_id');
    let logo = '/logo/enterpriseLarge.png';  //  we set it as default.
    let logoExt = 'ext'; //  we set it as default.
    const condoLogos = CondoLogos.findOne({ condoId: condoId });
    if (condoLogos && condoLogos.avatar && condoLogos.avatar.original) {
      logo =  condoLogos.avatar.original
      logoExt = condoLogos.ext;
    }
    getLogoDate(logo,logoExt,transaction, t, createPDF);
  },
  'click .openBasketSummary': function(e, t) {
    const transaction = this
    // avoid the duplicat events with above.
    if (e.target != undefined && !String (e.target.className).indexOf('download')){
      // pass above event only.
      return true;
    }
    if (transaction.basketId) {
      const response = transaction.threeDsResponse ? transaction.threeDsResponse.createPaymentResult : (transaction.createPayment && transaction.createPayment.result && transaction.createPayment.result.createPaymentResult) || 'unknown'
      const transactionId = (response && response.paymentResponse && response.paymentResponse.transactionId) || 'unknown'
      t.selectedBasketId.set(transaction.basketId)
      t.transactionId.set(transactionId)
      $('#modal_market_basket_summary').modal('show')
    }
  }
})
function getPrice(price, quantity) {
    return parseFloat(price * quantity).toFixed(2)
};
function getLogoDate(url,logoExt, transaction, t, callback){
  let img = new Image();
  img.onError = function() {
   console.log('Cannot load image: "'+url+'"');
  };
  img.onload = function() {
    let canvas = document.createElement('canvas');
    canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
    canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size
    canvas.getContext('2d').drawImage(this, 0, 0);
    console.log(canvas.toDataURL('image/'+logoExt));
    callback(canvas.toDataURL('image/'+logoExt), logoExt,transaction, t);
  };
  img.src = url;
};
function createPDF(imgData, imgExtension, transaction, t){
  console.log(transaction);
  if (transaction && transaction.basketId) {
    const response = transaction.threeDsResponse ? transaction.threeDsResponse.createPaymentResult : (transaction.createPayment && transaction.createPayment.result && transaction.createPayment.result.createPaymentResult) || 'unknown'
    const transactionId = (response && response.paymentResponse && response.paymentResponse.transactionId) || 'unknown'
    const card = {
      cardNumber: (response && response.cardResponse && response.cardResponse.number) || 'unknown',
      type: (response && response.cardResponse && response.cardResponse.brand) || 'unknown'
     }
    t.selectedBasketId.set(transaction.basketId)
    t.transactionId.set(transactionId)
    const condoId = FlowRouter.getParam('condo_id');
    const enterprise = Enterprises.find({'condos' : condoId}).fetch()||[];
   
    if(!enterprise.length){
      console.log('enterprise not found');
      return false;
    }
    const basket = MarketBasket.findOne({ _id: transaction.basketId })
    const companyInfo = enterprise[0];
    let retBasket = {
      hasBook: false,
      hasPaid: false,
      isBook: false,
      subtotal: 0,
      differentVat: [],
      total: 0,
      services: []
    }
    if (basket && basket.services) {
      retBasket.hasBook = basket && basket.services && basket.services.length > 0 && !!basket.services.find(s => (s.pay_outside_platform === true))
      retBasket.hasPaid = retBasket.hasBook && basket.services && basket.services.length > 0 && !!basket.services.find(s => (s.pay_outside_platform !== true))
      retBasket.isBook = retBasket.hasBook && !!basket.services.find(s => (s.pay_outside_platform === true && s.validatedAt === null))
      retBasket.services = basket.services.filter(s => {
        console.log(s);
        if (retBasket.isBook && !s.pay_outside_platform) return false
        if (retBasket.isBook && s.validatedAt !== null) return false
        if (!retBasket.isBook && s.pay_outside_platform) return false
        return true
      })
      setTotalValues(retBasket)
    }
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang);
    const width = 157.4257;
    const height = 222.9616;
    const doc = new jsPDF('p', 'mm', [width, height]); // init PDF layout.
    let pdfFileName = 'Invoice-'+transactionId;
    // Draw contents.
    doc.setFontSize(14);
    doc.setTextColor(93, 94, 98);
    doc.text(translate.commonTranslation.invoice_title, 6.35, 12.69)
    doc.addImage(imgData, imgExtension, 110.195, 3.2916, 62, 11.69)
    // border
    doc.setLineWidth(4.2)
    doc.setDrawColor(241, 245, 246)
    doc.line(4.23, 17.34564, 153.1951, 17.34564)
    // company and invoice information.
    doc.setFontSize(10)
    doc.setTextColor(174,184,188);
    doc.text(translate.commonTranslation.receive_from+":", 4.23, 24.87052)
    const invoiceWidth = doc.getTextWidth(translate.commonTranslation.invoice_number+":")
    doc.text(translate.commonTranslation.invoice_number+":", 153.1951 - invoiceWidth, 24.87052)
    doc.text(translate.commonTranslation.date+":", 153.1951 - invoiceWidth, 39.15784)
    doc.setFontSize(11);
    doc.setTextColor(105, 201, 213);
    doc.text(companyInfo.name, 4.23, 30.4267)
    doc.setFontSize(10);
    doc.setTextColor(100, 101, 105);
    doc.text(transactionId, 153.1951 - invoiceWidth, 29.42006)
    doc.setFontSize(10);
    doc.setTextColor(120, 121, 125);
    doc.text(companyInfo.info.address, 4.23, 35.42006)
    doc.text(companyInfo.info.city, 4.23, 40.42006)
    doc.text(new Date().toJSON().slice(0,10) , 153.1951 - invoiceWidth, 44.15784)
    doc.text(companyInfo.info.code, 4.23, 45.42006)
    // transactions.
    doc.setLineWidth(8.4)
    doc.setDrawColor(241, 245, 246)
    doc.line(4.23, 60.059, 153.1951, 60.059)
    doc.setFontSize(10)
    doc.setTextColor(139,153,159);
    doc.text(translate.commonTranslation.transaction, 8.46656, 60.70546)
    const amountWidth = doc.getTextWidth(translate.commonTranslation.amount);
    doc.text(translate.commonTranslation.amount, 153.1951 -amountWidth - 4.23, 60.70546)
    // draw the transaction items
    let initLine = 72.49492;
    retBasket.services.forEach(barket =>{
      doc.setFontSize(10);
      doc.setTextColor(93, 94, 98);
      doc.text(barket.title, 8.46656, initLine)
      const priceWidth = doc.getTextWidth(getPrice(barket.price, barket.quantity)+' €');
      doc.text(getPrice(barket.price, barket.quantity)+' €', 153.1951 - priceWidth - 4.23 ,initLine)
      initLine = initLine+ 8;
    })
    doc.setLineWidth(0.3)
    doc.setDrawColor(93, 94, 98)
    doc.line(6.35, initLine, 153.1951, initLine)
    // Sub Total
    doc.setFontSize(10);
    doc.setTextColor(93, 94, 98);
    doc.text(translate.commonTranslation.subtotal_vat, 8.46656, initLine+8)
    const subtotalTextWidth = doc.getTextWidth(retBasket.subtotal+' €');
    doc.text(retBasket.subtotal+' €', 153.1951 - subtotalTextWidth - 4.23, initLine+8)
    doc.setLineWidth(0.2)
    doc.setDrawColor(224, 224, 224)
    doc.line(6.35, initLine+14, 153.1951, initLine+14)
    // vat
    initLine = initLine+22
    let i = 0;
    retBasket.differentVat.forEach(vat =>{
      doc.setFontSize(10);
      doc.setTextColor(93, 94, 98);
      if(i > 0){
        doc.text(vat.title, 8.46656, initLine+ 3.6)  
      }else{
        doc.text(vat.title, 8.46656, initLine)  
      }
      const vatTextWidth = doc.getTextWidth(vat.value);
      if(i > 0){
        doc.text(vat.value, 153.1951 - vatTextWidth - 4.23, initLine + 3.6)
      }else{
        doc.text(vat.value, 153.1951 - vatTextWidth - 4.23, initLine)
      }
      doc.setLineWidth(0.1)
      if(i <= retBasket.differentVat.length - 2){
        console.log(initLine);
        doc.setDrawColor(224, 224, 224)
        doc.line(6.35, initLine+4.3, 153.1951, initLine + 4.3)
      }
      initLine = initLine+ 8;
      i++;
    });
    doc.setLineWidth(0.3)
    doc.setDrawColor(93, 94, 98)
    doc.line(6.35, initLine, 153.1951, initLine)
    // Total
    doc.setFontSize(11);
    doc.setTextColor(93, 94, 98);
    doc.text(translate.commonTranslation.total_vat, 8.46656 , initLine+8)
    const vatTextWidth = doc.getTextWidth(retBasket.total+' €');
    doc.text(retBasket.total+' €', 153.1951 - vatTextWidth- 4.23,initLine+8)
    doc.setFontSize(10);
    doc.setTextColor(139, 153, 159);
    doc.text(translate.commonTranslation.paid_via+":", 8.46656, initLine+23)
    doc.text(card.type+"(" +card.cardNumber.substr(card.cardNumber.length - 4)+")", 27, initLine+23)
    // Bottom line Address of MB.
    doc.setFontSize(8);
    const companyNameTextWidth = doc.getTextWidth(companyInfo.name);
    doc.text(companyInfo.name, (width/2 - companyNameTextWidth/2), (height - 17.4));
    const addressTextWidth = doc.getTextWidth(translate.commonTranslation.siret+': '+ companyInfo.info.siret);
    doc.text(translate.commonTranslation.siret+': '+ companyInfo.info.siret, (width/2 - addressTextWidth/2), (height - 13.5));
    const cityWidth = doc.getTextWidth(companyInfo.info.address+' '+companyInfo.info.code+' '+companyInfo.info.city);
    doc.text(companyInfo.info.address+' '+companyInfo.info.code+' '+companyInfo.info.city, (width/2 - cityWidth/2), (height - 9.5));
    doc.save(pdfFileName)
  }
}
const regexpSearch = {
  'a': '[a|à|â|ä]',
  'e': '[e|é|è|ê|ë]',
  'i': '[i|î|ï|ì]',
  'o': '[o|ô|ö|ò]',
  'u': '[u|ù|ü|û]',
  'y': '[y|ý|ŷ|ÿ|ỳ]'
};


Template.wallet_transactions.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady();
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  getTransactions: () => {
    const t = Template.instance()
    let searchText = t.searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')
    for (let i = 0; i != Object.keys(regexpSearch).length; i++) {
      searchText = searchText.replace(new RegExp(Object.keys(regexpSearch)[i], 'g'), regexpSearch[Object.keys(regexpSearch)[i]]);
    }
    const regexp = new RegExp(searchText, 'i')
    let transac = UserTransactions.find({}, { sort: { date: -1 } }).fetch()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

    transac = transac.filter(t => {
      const dateAsSearch = moment(t.date).format('DD/MM/YYYY')
      let statusSearch = translate.commonTranslation['wallet_' + t.status.toLowerCase()]
      let priceSearch = parseFloat(t.amount / 100).toFixed(2)
      let titleSearch = ''
      let transacNumber = ''
      if (t.basketId) {
        const response = t.threeDsResponse ? t.threeDsResponse.createPaymentResult : (t.createPayment && t.createPayment.result && t.createPayment.result.createPaymentResult) || 'unknown'
        const transactionId = (response && response.paymentResponse && response.paymentResponse.transactionId) || 'unknown'
        transacNumber = transactionId
        titleSearch = translate.commonTranslation.command_number
      } else {
        titleSearch = translate.commonTranslation[t.type]
      }
      return !!(dateAsSearch.match(regexp) || (!!titleSearch && titleSearch.match(regexp)) || (!!transacNumber && transacNumber.match(regexp))) || statusSearch.match(regexp) || priceSearch.match(regexp)
    })
    return transac
  },
  getAmount: (amount) => {
    return parseFloat(amount / 100).toFixed(2)
  },
  getTitle: function () {
    const transaction = this
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    if (transaction.basketId) {
      const response = transaction.threeDsResponse ? transaction.threeDsResponse.createPaymentResult : (transaction.createPayment && transaction.createPayment.result && transaction.createPayment.result.createPaymentResult) || 'unknown'
      const transactionId = (response && response.paymentResponse && response.paymentResponse.transactionId) || 'unknown'
      return `${translate.commonTranslation.command_number}: ${transactionId}`
    }
    return translate.commonTranslation[transaction.type]
  },
  getDataModal: function() {
    return {
      basketId: Template.instance().selectedBasketId.get(),
      transactionId: Template.instance().transactionId.get()
    }
  },
  searchActive: () => !!Template.instance().searchText.get()
})

