import { CommonTranslation } from "/common/lang/lang"
import { UsersCreditCard, UserTransactions } from '/common/collections/Payments'
import { payzenApiId, updateSignaturePayzen, payzenCallBack } from '/common/collections/API'
import CryptoJS from 'crypto-js'

Template.modal_topUp.onCreated(function () {
  this.isOpen = new ReactiveVar(false)
  this.canSubmit = new ReactiveVar(false)

  const condoId = FlowRouter.getParam('condo_id')
  const integrationConfig = IntegrationConfig.findOne({ condoId: condoId, integrationId: payzenApiId })

  this.isIntegrationError = new ReactiveVar(false)
  if (!integrationConfig) {
    this.isIntegrationError.set(true)
    return
  }

  this.certificate = integrationConfig.prodCert === null ? integrationConfig.testCert : integrationConfig.prodCert

  this.formPayZen = {
    vads_amount: '0',
    vads_ctx_mode: integrationConfig.prodCert === null ? 'TEST' : 'PRODUCTION',
    vads_currency: '978',
    vads_page_action: 'PAYMENT',
    vads_payment_config: 'SINGLE',
    vads_site_id: integrationConfig.siteId,
    vads_trans_date: moment.utc().format("YYYYMMDDHHmmss"),
    vads_trans_id: '' /* + transId */,
    vads_version: 'V2',
    vads_capture_delay: '0',
    vads_url_return: window.location.href,
    vads_action_mode: 'SILENT',
    // vads_theme_config: 'MODE_IFRAME',
    vads_order_id: '',
    vads_cust_email: Meteor.user().emails[0].address,
    vads_cust_id: Meteor.userId(),
    vads_cust_first_name: Meteor.user().profile.firstname,
    vads_cust_last_name: Meteor.user().profile.lastname,
    vads_cust_address: '',
    vads_card_number: '',
    vads_cvv: '',
    vads_expiry_month: '',
    vads_expiry_year: '',
    vads_payment_cards: '',
    vads_url_check: payzenCallBack,
    vads_url_refused: Meteor.absoluteUrl('api/payzen/refused/' /* + transId */),
    vads_url_error: Meteor.absoluteUrl('api/payzen/error/' /* + transId */),
    vads_url_success: Meteor.absoluteUrl('api/payzen/success/' /* + transId */),
    signature: null
  }

  this.isCustomAmount = new ReactiveVar(false)
  this.isNumberError = new ReactiveVar(false)
  this.formData = new ReactiveDict([])
  this.formData.setDefault({
    cardSelect: null,
    amount: null
  })
})

Template.modal_topUp.onDestroyed(() => {
})

Template.modal_topUp.onRendered(() => {
})

Template.modal_topUp.events({
  'click .topUpButton': function (e, t) {
    e.preventDefault()
    e.stopPropagation()
    if (t.canSubmit.get() === true) {
      $(e.currentTarget).button('loading')
      const lang = FlowRouter.getParam('lang')
      const translate = new CommonTranslation(lang)

      const password_title = translate.commonTranslation.password_title
      const password_details = translate.commonTranslation.password_details
      const password_placeholder = translate.commonTranslation.password_placeholder
      bootbox.dialog({
        title: password_title,
        message: '<div class="mb-input" style="margin-top: 30px;">\
            <label for="mb-select">' + password_details + '<i class="image-icon mandatory-icon"></i></label>\
            <input placeholder="' + password_placeholder + '" id="mb-input-password-secure-topup" maxlength="-1" type="password">\
          </div>',
        buttons: {
          'cancel': {
            label: translate.commonTranslation.cancel, className: 'btn-outline-red-confirm'
          },
          'confirm': {
            label: translate.commonTranslation.confirm, className: 'btn-red-confirm',
            callback: function (result) {
              const password = $('#mb-input-password-secure-topup').val()
              const digest = Package.sha.SHA256(password)
              Meteor.call('checkPassword', digest, async function (err, result) {
                if (result) {
                  const formData = t.formData.all()

                  let result = UsersCreditCard.findOne({ _id: formData.cardSelect })
                  const transId = moment.utc().format("HHmmss")
                  const amount = formData.amount.replace(/,/gi, '.') * 10 * 10 // prevent errors instead of * 100

                  t.formPayZen.vads_trans_id = transId

                  t.formPayZen.vads_amount = amount || 0

                  const number = CryptoJS.AES.decrypt(result.number, digest).toString(CryptoJS.enc.Utf8)
                  const cvc = CryptoJS.AES.decrypt(result.cvc, digest).toString(CryptoJS.enc.Utf8)
                  const expiration = CryptoJS.AES.decrypt(result.expiration, digest).toString(CryptoJS.enc.Utf8)

                  t.formPayZen.vads_card_number = number
                  t.formPayZen.vads_cvv = cvc
                  t.formPayZen.vads_expiry_month = expiration.slice(0, 2)
                  t.formPayZen.vads_expiry_year = expiration.slice(3, 7)
                  t.formPayZen.vads_payment_cards = result.type.toUpperCase()

                  const condo = Condos.findOne({ _id: FlowRouter.getParam('condo_id') })

                  t.formPayZen.vads_cust_address = result.sameBillingAddress === true ? (condo.info.address + ',' + condo.info.code + ' ' + condo.info.city) : (result.street + ', ' + result.code_city + ', ' + result.country)

                  const resident = Residents.findOne({ userId: Meteor.userId() })
                  const condoId = FlowRouter.getParam('condo_id')
                  let ret = false
                  if (resident && condoId) {
                    const residentCondo = resident.condos.find((condo) => condo.condoId === condoId)
                    if (residentCondo) {
                      ret = residentCondo.preferences.emailPaymentReceipts || false
                    }
                  }

                  if (ret === false) {
                    t.formPayZen.vads_cust_email = ''
                  }

                  let transactionId = null

                  await new Promise((resolve, reject) => {
                    Meteor.call('topUp', condoId, formData.cardSelect, amount || 0, t.formPayZen, (error, result) => {
                      if (error) {
                        $(e.currentTarget).button('reset')
                        sAlert.error(error)
                        reject()
                      } else {
                        transactionId = result
                        resolve()
                      }
                    })
                  })

                  if (transactionId !== null) {
                    const formPayZen = t.formPayZen

                    formPayZen.vads_url_refused += transactionId
                    formPayZen.vads_url_error += transactionId
                    formPayZen.vads_url_success += transactionId


                    formPayZen.vads_order_id = transactionId
                    formPayZen.signature = updateSignaturePayzen(formPayZen, t.certificate)

                    let form = $('<form>', {
                      action: 'https://secure.payzen.eu/vads-payment/',
                      id: 'FormFromJs',
                      target: 'PayzenIframe'
                    })
                    _.each(t.formPayZen, (data, index) => {
                      form.append($('<input>', {
                        name: index,
                        value: data,
                        type: 'hidden'
                      }))
                    })
                    $('.modal-body.clearfix').css('display', 'none')
                    const iFrame = $("<iframe name='PayzenIframe' frameborder='0' style='display: block; width: 100%; height: 90vh;' class='modal-body clearfix'></iframe>")
                    $('.modal-content').append(iFrame)
                    $('.modal-content').append(form)
                    form.submit()
                    $('#FormFromJs').remove()

                    const startTimer = Date.now()
                    let thisTimer = Meteor.setInterval(function () {
                      const transaction = UserTransactions.findOne({ _id: transactionId })
                      if (transaction && transaction.status !== 'pending') {
                        if (transaction.status === 'success') {
                          sAlert.success(translate.commonTranslation.topup_success)
                        } else {
                          sAlert.error(translate.commonTranslation.topup_error)
                        }
                        Meteor.clearInterval(thisTimer)
                        $('[name="PayzenIframe"]').remove()
                        $('.topUpButton').button('reset')
                        $('#modal_topUp').modal('hide')
                      } else if ((Date.now() - startTimer) > 300000) { // 5 minutes
                        sAlert.error('Did not get response from PayZen wait a few minutes to see if the top up is added to your account')
                        Meteor.clearInterval(thisTimer)
                        $('[name="PayzenIframe"]').remove()
                        $('.topUpButton').button('reset')
                        $('#modal_topUp').modal('hide')
                      }
                    }, 100)
                  }
                } else {
                  $(e.currentTarget).button('reset')
                  sAlert.error(translate.commonTranslation.wrong_password)
                }
              })
            }
          }
        }
      })
    }
  },
  'hidden.bs.modal #modal_topUp': (event, template) => {
    $('.modal-body.clearfix').css('display', '')
    template.isOpen.set(false)
    template.canSubmit.set(false)
    template.isCustomAmount.set(false)
    template.isNumberError.set(false)
    template.formData.clear()
    template.formData.setDefault({
      cardSelect: null,
      amount: null
    })
    template.formPayZen = {
      vads_amount: '0',
      vads_ctx_mode: template.formPayZen.vads_ctx_mode,
      vads_currency: '978',
      vads_page_action: 'PAYMENT',
      vads_payment_config: 'SINGLE',
      vads_site_id: template.formPayZen.vads_site_id,
      vads_trans_date: moment.utc().format("YYYYMMDDHHmmss"),
      vads_trans_id: '' /* + transId */,
      vads_version: 'V2',
      vads_capture_delay: '0',
      vads_url_return: window.location.href,
      vads_action_mode: 'SILENT',
      // vads_theme_config: 'MODE_IFRAME',
      vads_order_id: '',
      vads_cust_email: Meteor.user().emails[0].address,
      vads_cust_id: Meteor.userId(),
      vads_cust_first_name: Meteor.user().profile.firstname,
      vads_cust_last_name: Meteor.user().profile.lastname,
      vads_card_number: '',
      vads_cust_address: '',
      vads_cvv: '',
      vads_expiry_month: '',
      vads_expiry_year: '',
      vads_payment_cards: '',
      vads_url_check: payzenCallBack,
      vads_url_refused: Meteor.absoluteUrl('api/payzen/refused/' /* + transId */),
      vads_url_error: Meteor.absoluteUrl('api/payzen/error/' /* + transId */),
      vads_url_success: Meteor.absoluteUrl('api/payzen/success/' /* + transId */),
      signature: null
    }

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_topUp': (event, template) => {
    $('.modal-body.clearfix').css('display', '')
    template.canSubmit.set(false)
    template.isCustomAmount.set(false)
    template.isNumberError.set(false)
    template.formData.clear()
    template.formData.setDefault({
      cardSelect: null,
      amount: null
    })

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_topUp";

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_topUp") {
        modal.modal('hide');
      }
    }
  },
})

function updateSubmit(t) {
  let canSubmit = false
  const formData = t.formData.all()
  canSubmit =
    (formData.cardSelect !== null) &&
    (formData.amount !== null && t.isNumberError.get() === false)

  t.canSubmit.set(canSubmit)
}

Template.modal_topUp.helpers({
  isIntegrationError: () => {
    return Template.instance().isIntegrationError.get()
  },
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  paymentMethods: () => {
    return UsersCreditCard.find({}, { sort: { createdAt: -1 } })
  },
  getForm: (key, data) => {
    if (key === 'amount' && data === 'custom') {
      return Template.instance().isCustomAmount.get()
    } else if (key === 'amount' && data === 'customVal') {
      if (Template.instance().isCustomAmount.get()) {
        return Template.instance().formData.get(key)
      } else {
        if (Template.instance().formData.get(key) !== null) {
          return ''
        }
      }
    } else if (key === 'amount') {
      return Template.instance().formData.get(key) === data && Template.instance().isCustomAmount.get() === false
    } else {
      return Template.instance().formData.get(key) === data
    }
  },
  getIsNumberError: () => {
    return Template.instance().isNumberError.get() || false
  },
  onInputDetails: (key, data) => {
    const t = Template.instance()

    return () => value => {
      t.isNumberError.set(false)

      if (key === 'amount' && data === 'custom') {
        t.isCustomAmount.set(true)
      } else if (key === 'amount' && data === 'customVal') {
        t.isCustomAmount.set(true)
        if (isNaN(value) || value <= 0) {
          t.isNumberError.set(true)
        } else {
          t.isNumberError.set(false)
        }
        t.formData.set(key, value)
      } else {
        if (key === 'amount') {
          t.isNumberError.set(false)
          t.isCustomAmount.set(false)
        }
        t.formData.set(key, data)
      }

      updateSubmit(t)
    }
  },
})
