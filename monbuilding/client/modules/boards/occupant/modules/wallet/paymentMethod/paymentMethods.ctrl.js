import { CommonTranslation } from "/common/lang/lang.js"
// import { UsersCreditCard } from '/common/collections/Payments'
import { UsersPayzen } from '/common/collections/Payments'
import {
  RemovePayZenAlias
} from '/client/components/PayZen/MbPayzenAlias/MbPayzenAlias'

Template.wallet_paymentMethod.onCreated(function () {
  this.searchText = new ReactiveVar('')
  this.isEditMode = new ReactiveVar(false)
  this.editCardId = new ReactiveVar(null)
  this.digest = new ReactiveVar(null)

  this.addClicked = new ReactiveVar(false)

  // this.subscribe('creditCard')
  Meteor.subscribe('gePayzenAlias', FlowRouter.getParam('condo_id'))
})

Template.wallet_paymentMethod.onRendered(function () {
})

Template.wallet_paymentMethod.events({
  'click .trashIcon': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const condoId = FlowRouter.getParam('condo_id')

    bootbox.confirm({
      size: 'medium',
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.you_sure_delete_card,
      buttons: {
        'cancel': { label: translate.commonTranslation.cancel, className: 'btn-outline-red-confirm' },
        'confirm': { label: translate.commonTranslation.confirm, className: 'btn-red-confirm' }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          const paymentToken = $(e.currentTarget).attr('paymentToken')
          if (paymentToken) {
            RemovePayZenAlias(condoId, paymentToken).then(res => {
              if (!res.success) {
                sAlert.error(res.detail)
              }
              // this.__nextStep()
            }).catch(e => {
              sAlert.error(e)
            })
          }
        }
      }
    })
  },
  // 'click .editIcon': (e, t) => {
  //   e.preventDefault()
  //   e.stopPropagation()
  //   const lang = FlowRouter.getParam('lang') || 'fr'
  //   const translate = new CommonTranslation(lang)

  //   const password_title = translate.commonTranslation.password_title
  //   const password_details = translate.commonTranslation.password_details
  //   const password_placeholder = translate.commonTranslation.password_placeholder
  //   bootbox.dialog({
  //     title: password_title,
  //     message: '<div class="mb-input" style="margin-top: 30px;">\
  //           <label for="mb-select">' + password_details + '<i class="image-icon mandatory-icon"></i></label>\
  //           <input placeholder="' + password_placeholder + '" id="mb-input-password-secure-edit" maxlength="-1" type="password">\
  //         </div>',
  //     buttons: {
  //       'cancel': {
  //         label: translate.commonTranslation.cancel, className: 'btn-outline-red-confirm'
  //       },
  //       'confirm': {
  //         label: translate.commonTranslation.confirm, className: 'btn-red-confirm',
  //         callback: function (result) {
  //           const password = $('#mb-input-password-secure-edit').val()
  //           const digest = Package.sha.SHA256(password)
  //           Meteor.call('checkPassword', digest, function (err, result) {
  //             if (result) {
  //               t.isEditMode.set(true)
  //               t.digest.set(digest)
  //               t.editCardId.set($(e.currentTarget).attr('cardId'))
  //               $('#modal_addCard').modal('show')
  //             } else {
  //               sAlert.error(translate.commonTranslation.wrong_password)
  //             }
  //           })
  //         }
  //       }
  //     }
  //   })
  // },
  'click .addTestCard': (e, t) => {
    $(e.currentTarget).button('loading')
    Meteor.call('addTestsCards', (error, result) => {
      if (error) {
        sAlert.error(error)
      }
      $(e.currentTarget).button('reset')
    })
  }
})

Template.wallet_paymentMethod.helpers({
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady()
  },
  paymentMethods: () => {
    return UsersPayzen.find().fetch()
    // return UsersCreditCard.find({}, { sort: { createdAt: -1 } })
  },
  addNewPaymentMethod: () => {
    const t = Template.instance()

    return () => {
      t.isEditMode.set(false)
      t.editCardId.set(null)
      t.digest.set(null)
      $('#modal_addCard').modal('show')
    }
  },
  getEditMode: () => {
    return Template.instance().isEditMode.get()
  },
  getCardId: () => {
    return Template.instance().editCardId.get()
  },
  getDigest: () => {
    return Template.instance().digest.get()
  },
})
