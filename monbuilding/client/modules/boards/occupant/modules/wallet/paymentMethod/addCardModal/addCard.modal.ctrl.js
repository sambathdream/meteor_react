import { CommonTranslation } from "/common/lang/lang"
// import { UsersCreditCard } from '/common/collections/Payments'
// import CryptoJS from 'crypto-js'
import MbCreditCardForm from '/client/components/PayZen/MbCreditCardForm/MbCreditCardForm'
import {
  CreatePayZenAlias
} from '/client/components/PayZen/MbPayzenAlias/MbPayzenAlias'

Template.modal_addCard.onCreated(function () {
  this.isOpen = new ReactiveVar(false)
  // this.canSubmit = new ReactiveVar(false)
  // this.isEditMode = new ReactiveVar(false)

  // this.formData = new ReactiveDict([])
  // this.formData.setDefault({
  //   name: '',
  //   number: '',
  //   cvc: '',
  //   expiration: '',
  //   type: '',
  //   sameBillingAddress: true,
  //   street: '',
  //   code_city: '',
  //   country: ''
  // })
  // this.errorData = new ReactiveDict([])
  // this.errorData.setDefault({
  //   name: false,
  //   number: false,
  //   cvc: false,
  //   expiration: false,
  //   type: false
  // })
})

Template.modal_addCard.onDestroyed(() => {
})

Template.modal_addCard.onRendered(() => {
})

Template.modal_addCard.events({
  // 'click .saveNewCard': (e, t) => {
  //   e.preventDefault()
  //   e.stopPropagation()
  //   if (t.canSubmit.get() === true) {
  //     $(e.currentTarget).button('loading')
  //     const formData = t.formData.all()
  //     let editCardId = null
  //     if (t.isEditMode.get() === true) {
  //       editCardId = t.data.cardId
  //     }
  //     const lang = FlowRouter.getParam('lang') || 'fr'
  //     const translate = new CommonTranslation(lang)

  //     const password_title = translate.commonTranslation.password_title
  //     const password_details = translate.commonTranslation.password_details
  //     const password_placeholder = translate.commonTranslation.password_placeholder
  //     bootbox.dialog({
  //       title: password_title,
  //       message: '<div class="mb-input" style="margin-top: 30px;">\
  //           <label for="mb-select">' + password_details + '<i class="image-icon mandatory-icon"></i></label>\
  //           <input placeholder="' + password_placeholder + '" id="mb-input-password-secure" maxlength="-1" type="password">\
  //         </div>',
  //       buttons: {
  //         'cancel': { label: translate.commonTranslation.cancel, className: 'btn-outline-red-confirm', callback: function (result) {
  //             $(e.currentTarget).button('reset')
  //           }
  //         },
  //         'confirm': { label: translate.commonTranslation.confirm, className: 'btn-red-confirm',
  //           callback: function (result) {
  //             const password = $('#mb-input-password-secure').val()
  //             const digest = Package.sha.SHA256(password)
  //             Meteor.call('checkPassword', digest, function (err, result) {
  //               if (result) {
  //                 formData.displayableNumber = '**** ' + formData.number.slice(-4)
  //                 formData.number = CryptoJS.AES.encrypt(formData.number.replace(/ /gi, ''), digest).toString()
  //                 formData.cvc = CryptoJS.AES.encrypt(formData.cvc, digest).toString()
  //                 formData.expiration = CryptoJS.AES.encrypt(formData.expiration, digest).toString()
  //                 Meteor.call('saveCreditCard', formData, editCardId, (error, result) => {
  //                   if (error) {
  //                     sAlert.error(error)
  //                   }
  //                   $(e.currentTarget).button('reset')
  //                   $('#modal_addCard').modal('hide')
  //                 })
  //               } else {
  //                 $(e.currentTarget).button('reset')
  //                 sAlert.error(translate.commonTranslation.wrong_password)
  //               }
  //             });
  //           }
  //         }
  //       }
  //     });
  //   }
  // },
  'hidden.bs.modal #modal_addCard': (event, template) => {
    template.isOpen.set(false)
    // template.canSubmit.set(false)
    // template.isEditMode.set(false)

    // template.formData.clear()
    // template.formData.setDefault({
    //   name: '',
    //   number: '',
    //   cvc: '',
    //   expiration: '',
    //   type: '',
    //   sameBillingAddress: true,
    //   street: '',
    //   code_city: '',
    //   country: ''
    // })
    // template.errorData.clear()
    // template.errorData.setDefault({
    //   name: false,
    //   number: false,
    //   cvc: false,
    //   expiration: false,
    //   type: false
    // })

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_addCard': (event, template) => {
    // template.canSubmit.set(false)
    // template.isEditMode.set(false)

    let modal = $(event.currentTarget);
    window.location.hash = "#modal_addCard";

    // template.formData.clear()
    // template.formData.setDefault({
    //   name: '',
    //   number: '',
    //   cvc: '',
    //   expiration: '',
    //   type: '',
    //   sameBillingAddress: true,
    //   street: '',
    //   code_city: '',
    //   country: ''
    // })
    // template.errorData.clear()
    // template.errorData.setDefault({
    //   name: false,
    //   number: false,
    //   cvc: false,
    //   expiration: false,
    //   type: false
    // })

    template.isOpen.set(true)
    window.onhashchange = function () {
      if (location.hash != "#modal_addCard") {
        modal.modal('hide');
      }
    }
  },
})

// function updateSubmit(t) {
//   let canSubmit = false
//   const formData = t.formData.all()
//   const errorData = t.errorData.all()
//   canSubmit =
//   ( formData.name !== '' && errorData.name === false ) &&
//   ( formData.number !== '' && errorData.number === false ) &&
//   ( formData.cvc !== '' && errorData.cvc === false ) &&
//   ( formData.expiration !== '' && errorData.expiration === false ) &&
//   ( formData.type !== '' && errorData.type === false ) &&
//   ( formData.sameBillingAddress === true || (formData.sameBillingAddress === false && ( formData.street !== '' && formData.code_city !== '' && formData.country !== '' )))
//   t.canSubmit.set(canSubmit)
// }

Template.modal_addCard.helpers({
  // updateData: () => {
  //   const t = Template.instance()
  //   if (t.isOpen.get() === true) {
  //     t.isEditMode.set(Template.currentData().isEdit)

  //     if (Template.currentData().isEdit === true && !!Template.currentData().cardId) {
  //       let editingCard = UsersCreditCard.findOne({ _id: Template.currentData().cardId })
  //       const digest = Template.currentData().digest

  //       const number = CryptoJS.AES.decrypt(editingCard.number, digest).toString(CryptoJS.enc.Utf8)
  //       const cvc = CryptoJS.AES.decrypt(editingCard.cvc, digest).toString(CryptoJS.enc.Utf8)
  //       const expiration = CryptoJS.AES.decrypt(editingCard.expiration, digest).toString(CryptoJS.enc.Utf8)
  //       if (editingCard) {
  //         Meteor.defer(() => {
  //           t.formData.set({
  //             name: editingCard.name,
  //             number: number.match(/.{1,4}/g).join(' '),
  //             cvc: cvc,
  //             expiration: expiration,
  //             type: editingCard.type,
  //             sameBillingAddress: !!editingCard.sameBillingAddress,
  //             street: editingCard.street || '',
  //             code_city: editingCard.code_city || '',
  //             country: editingCard.country || ''
  //           })
  //           updateSubmit(t)
  //         })
  //       }
  //     }
  //   }
  // },
  isOpen: () => {
    return Template.instance().isOpen.get()
  },
  // canSubmit: () => {
  //   return Template.instance().canSubmit.get()
  // },
  // getForm: (key) => {
  //   return Template.instance().formData.get(key)
  // },
  // getIsError: (key) => {
  //   return Template.instance().errorData.get(key) || false
  // },
  // onInputDetails: (key) => {
  //   const t = Template.instance()

  //   return () => value => {
  //     t.errorData.set(key, false)
  //     if (key === 'number') {
  //       const withoutSpace =  value.replace(/ /gi, '')
  //       const previousValue = t.formData.get('number')
  //       if (withoutSpace && withoutSpace.length < 4) {
  //         t.formData.set('type', '')
  //       } else if (withoutSpace && (withoutSpace.length === 4 || withoutSpace.length === 16)) {
  //         if (withoutSpace[0] === '4') {
  //           t.formData.set('type', 'visa')
  //         } else if ((withoutSpace[0] === '3') && ((withoutSpace[1] === '4') || (withoutSpace[1] === '7'))) {
  //           t.formData.set('type', 'amex')
  //         } else if (parseInt(withoutSpace.slice(0, 4)) >= 5100 && parseInt(withoutSpace.slice(0, 4)) <= 5599) {
  //           t.formData.set('type', 'mastercard')
  //         } else if (parseInt(withoutSpace.slice(0, 4)) >= 2221 && parseInt(withoutSpace.slice(0, 4)) <= 2720) {
  //           t.formData.set('type', 'mastercard')
  //         } else if (parseInt(withoutSpace.slice(0, 4)) >= 5600 && parseInt(withoutSpace.slice(0, 4)) <= 5899) {
  //           t.formData.set('type', 'maestro')
  //         } else if (withoutSpace[0] === '6') {
  //           t.formData.set('type', 'maestro')
  //         } else if (withoutSpace[0] === '5' && withoutSpace[1] === '0') {
  //           t.formData.set('type', 'maestro')
  //         } else {
  //           t.formData.set('type', '')
  //           t.errorData.set('number', true)
  //         }
  //       } else if (withoutSpace && withoutSpace.length > 4 && t.formData.get('type') === '') {
  //         t.errorData.set('number', true)
  //       } else if (withoutSpace && withoutSpace.length > 16) {
  //         t.errorData.set('number', true)
  //       }
  //       if (withoutSpace) {
  //         value = withoutSpace.match(/.{1,4}/g).join(' ')
  //       }
  //       if (isNaN(withoutSpace) || withoutSpace.length !== 16) {
  //         t.errorData.set('number', true)
  //       }
  //     } else if (key === 'cvc') {
  //       if (value && (value.length > 3 || value.length < 3 || isNaN(value))) {
  //         t.errorData.set('cvc', true)
  //       }
  //     } else if (key === 'expiration') {
  //       const previousValue = t.formData.get('expiration')
  //       if (previousValue.length < value.length) {
  //         if (moment(value, 'MM/YYYY').isValid()) {
  //           const res = moment().isSameOrBefore(moment(value, 'MM/YYYY'), 'month')
  //           if (res === false) {
  //             t.errorData.set(key, true)
  //           } else {
  //             value = moment(value, 'MM/YYYY').format('MM/YYYY')
  //           }
  //         } else {
  //           t.errorData.set(key, true)
  //         }
  //       }
  //     }
  //     t.formData.set(key, value)
  //     updateSubmit(t)
  //   }
  // },
  // onClickCheckbox: (key) => {
  //   const t = Template.instance()

  //   return () => () => {
  //     const value = t.formData.get(key)
  //     t.formData.set(key, !value)
  //     updateSubmit(t)
  //   }
  // },
  // getUserName: () => {
  //   const user = Meteor.user()
  //   return user.profile.firstname + ' ' + user.profile.lastname
  // },
  // getBuildingName: () => {
  //   const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
  //   if (condo.name && condo.name !== '' && (condo.info.address && condo.name !== condo.info.address)) {
  //     return (condo.info.id !== '-1' ? condo.info.id + ' ' : '') + condo.name
  //   } else {
  //     return '-'
  //   }
  // },
  // getBuildingAddress: () => {
  //   const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
  //   return condo && (condo.info.address + ',')
  // },
  // getBuildingAddress2: () => {
  //   const condo = Condos.findOne({_id: FlowRouter.getParam('condo_id')})
  //   return condo && (condo.info.code + ' ' + condo.info.city)
  // },
  MbCreditCardForm: () => {
    return MbCreditCardForm
  },
  __saveCrediCard: () => {
    const t = Template.instance()

    return (cardInfo) => {
      const condoId = FlowRouter.getParam('condo_id')
      $('.saveNewCard').button('loading')
      CreatePayZenAlias(condoId, cardInfo).then(res => {
        if (res.success) {
          $('.saveNewCard').button('reset')
          $('#modal_addCard').modal('hide')
          // this.setState({ isRunning: false, selectedCard: res.token, forceNextStep: true })
        } else {
          $('.saveNewCard').button('reset')
          sAlert.error(res.detail)
          // this.setState({ isRunning: false })
        }
      }).catch(e => {
        $('.saveNewCard').button('reset')
        sAlert.error(e)
        // this.setState({ isRunning: false, error: e })
      })
    }
  },
  canSubmit: () => {
    return true
  },
  commonTranslation: () => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    return new CommonTranslation(lang)
  }
})
