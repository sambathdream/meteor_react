import { CommonTranslation } from "/common/lang/lang.js"
import { UserTransactions } from '/common/collections/Payments'
import uuid from "uuid"
import CryptoJS from 'crypto-js'

Template.wallet_recurringPayment.onCreated(function () {
  this.subscribe('creditCard')

  const generatedUUID = uuid()
  const generatedTimestamp = moment.utc().format('', moment.ISO_8601)

  const hmac_2565 = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(generatedUUID + generatedTimestamp, '2846414230635531'))
  console.log('generatedUUID', generatedUUID)
  console.log('generatedTimestamp', generatedTimestamp)
  console.log('hmac_2565', hmac_2565)
  console.log('hmac_2565', hmac_2565.toString())
})

Template.wallet_recurringPayment.onRendered(function () {
})

Template.wallet_recurringPayment.events({
  'click .trashIcon': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    console.log('to')
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    // bootbox.confirm({
    //   size: 'medium',
    //   title: translate.commonTranslation.confirmation,
    //   message: translate.commonTranslation.you_sure_delete_card,
    //   buttons: {
    //     'cancel': { label: translate.commonTranslation.cancel, className: 'btn-outline-red-confirm' },
    //     'confirm': { label: translate.commonTranslation.confirm, className: 'btn-red-confirm' }
    //   },
    //   backdrop: true,
    //   callback: function (result) {
    //     if (result) {
    //       const cardId = $(e.currentTarget).attr('cardId')
    //       if (cardId) {
    //         Meteor.call('removeCreditCard', cardId, (error, result) => {
    //           if (error) {
    //             sAlert.error(error)
    //           } else {
    //             sAlert.success(translate.commonTranslation.delete_success)
    //           }
    //         })
    //       }
    //     }
    //   }
    // })
  },
  'click .editIcon': (e, t) => {
    e.preventDefault()
    e.stopPropagation()
    console.log('to')
    // t.isEditMode.set(true)
    // t.editCardId.set($(e.currentTarget).attr('cardId'))
    // $('#modal_addCard').modal('show')
  },

})

Template.wallet_recurringPayment.helpers({
  getReceiptsValue: () => {
    const resident = Residents.findOne({ userId: Meteor.userId() })
    const condoId = FlowRouter.getParam('condo_id')
    let ret = false
    if (resident && condoId) {
      const residentCondo = resident.condos.find((condo) => condo.condoId === condoId)
      if (residentCondo) {
        ret = residentCondo.preferences.emailPaymentReceipts || false
      }
    }
    return ret
  },
  getRecurringTopUpStatus: (recurringTopUp) => {
    return true
  },
  switchReceiptsValue: () => {
    const t = Template.instance()
    const condoId = FlowRouter.getParam('condo_id')

    return () => {
      Meteor.call('switchReceiptsEmailCondoValue', condoId)
    }
  },
  switchRecurringValue: () => {
    const t = Template.instance()
    const condoId = FlowRouter.getParam('condo_id')

    return () => {
      return 'TODOS'
    }
  },
  addRecurringPayment: () => {
    const t = Template.instance()

    return () => {
      const recurringTopUp = UserTransactions.findOne({ condoId: FlowRouter.getParam('condo_id'), isRecurring: true, isCredit: true, status: 'success' }, { sort: { date: -1 } })
      if (!recurringTopUp) {
        $('#modal_recurringTopUp').modal('show')
      }
    }
  },
  getAddRecurringClass: (alreadyRecurring) => {
    return alreadyRecurring && 'disabled'
  },
  getRecurringTopUp: () => {
    const recurringTopUp = UserTransactions.findOne({ condoId: FlowRouter.getParam('condo_id'), isRecurring: true, isCredit: true, status: 'success' }, { sort: { date: -1 } })
    return recurringTopUp
  },
  getRecurringValue: (recurringTopUp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const amount = recurringTopUp.amount / 100
    const frequency = translate.commonTranslation[recurringTopUp.frequency || 'monthly']

    return amount + ' €/' + frequency

  }
})
