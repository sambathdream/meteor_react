import { MainStats } from '/client/modules/boards/occupant/modules/wallet/stats/mainStats.js'
Template.module_walletController.onCreated(function () {
  let tab = FlowRouter.getParam('wildcard')
  if (!tab || (tab !== 'stats' && tab !== 'transactions' && tab !== 'payment_method')) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id'), wildcard: 'stats' })
  }
  // this.subscribe('creditCard')
  this.subscribe('payzenInformations')
  this.subscribe('getUserTransactions', FlowRouter.getParam('condo_id'))
  Meteor.subscribe('market_place_reservations_details', FlowRouter.getParam('condo_id'))
  Meteor.subscribe('market_place_services_stats', FlowRouter.getParam('condo_id'))
})

Template.module_walletController.onRendered(function () {
})

Template.module_walletController.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab');
    FlowRouter.setParams({ wildcard: tabName })
  },
})

Template.module_walletController.helpers({
  isSubscribeReady: () => {
    const isReady = Template.instance().subscriptionsReady()
    if (isReady === true) {
      if (!Meteor.userHasRight('wallet', 'see')) {
        FlowRouter.go('app.board.resident.condo', {
          lang: FlowRouter.getParam('lang'),
          condo_id: FlowRouter.getParam('condo_id')
        })
      }
    }
    return isReady
  },
  getTemplateName: () => {
    let tab = FlowRouter.getParam('wildcard')
    if (tab === 'stats') {
      return 'wallet_stats'
    } else if (tab === 'transactions') {
      return 'wallet_transactions'
    } else if (tab === 'payment_method') {
      return 'wallet_paymentMethod'
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('wildcard')
    if (!tab || (tab !== 'stats' && tab !== 'transactions' && tab !== 'payment_method')) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id'), wildcard: 'stats' })
    }
    return tab
  },
  getCondoName: () => {
    return Condos.findOne({ _id: FlowRouter.getParam('condo_id') }).getName()
  },
  MainStats: () => MainStats
})
