import { UserTransactions } from '/common/collections/Payments'
import { CommonTranslation } from '/common/lang/lang';

Template.wallet_generalInformations.onCreated(function () {
})

Template.wallet_generalInformations.onRendered(function () {

})

Template.wallet_generalInformations.events({
  'click .seeAllTransactions': (e, t) => {
    let tabName = 'transactions'
    FlowRouter.setParams({ wildcard: tabName })
  },
  'click .editProfile': (e, t) => {
    $(e.currentTarget).toggleClass('spinMe')
  }
})

Template.wallet_generalInformations.helpers({
  topUp: () => {
    const t = Template.instance()
    return () => {
      $('#modal_topUp').modal('show')
    }
  },
  getWalletBalance: () => {
    return "(FAKE) 25.47"
  },
  getLatestTransactions: () => {
    return UserTransactions.find({}, { sort: { date: -1 }, limit: 2 })
  },
  hasTransactions: () => {
    return UserTransactions.find({}, { sort: { date: -1 }, limit: 2 }).fetch().length > 0
  },
  getAmount: (amount) => {
    return (parseFloat(amount) / 100).toFixed(2)
  },
  getTitle: function() {
    const transaction = this
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    if (transaction.basketId) {
      const response = transaction.threeDsResponse ? transaction.threeDsResponse.createPaymentResult : (transaction.createPayment && transaction.createPayment.result && transaction.createPayment.result.createPaymentResult) || 'unknown'
      const transactionId = (response && response.paymentResponse && response.paymentResponse.transactionId) || 'unknown'
      return `${translate.commonTranslation.command_number}: ${transactionId}`
    }
    return translate.commonTranslation[transaction.type]
  }
})

