import React, { Component } from 'react'
import DateTime from 'react-datetime'
import moment from 'moment'

export default class DateTimePicker extends Component {
  constructor (props) {
    super(props)
    const { defaultDate } = this.props
    this.state = {
      date: defaultDate
    }
    this.onDatePicked = this.onDatePicked.bind(this)
    this.isValidDate = this.isValidDate.bind(this)
  }

  onDatePicked (date) {
    if (!moment.isMoment(date)) {
      return
    }
    date.second(0)
    date.millisecond(0)
    const { onDatePicked } = this.props
    if (this.isValidDate(date) && typeof onDatePicked === 'function') {
      this.setState({ date })
      onDatePicked(date)
    }
  }

  isValidDate (currentDate) {
    const { checkValidDate } = this.props
    if (!checkValidDate || typeof checkValidDate !== 'function') {
      return false
    }
    return checkValidDate(currentDate)
  }

  render () {
    const { date } = this.state
    const { inputStyle } = this.props
    return (
      <DateTime
        value={date}
        onChange={this.onDatePicked}
        dateFormat='DD/MM/YYYY'
        timeFormat='HH:mm'
        locale={FlowRouter.getParam('lang') || 'fr'}
        isValidDate={this.isValidDate}
        inputProps={{...inputStyle, readOnly: true}}
      />
    )
  }
}
