import React, { Component } from 'react'
import DateTimePicker from './dateTimePicker'

export default class SelectPeriod extends Component {
  constructor (props) {
    super(props)
    const { defaultStartDate, defaultEndDate } = this.props
    this.state = {
      startDate: defaultStartDate,
      endDate: defaultEndDate
    }
    this.onStartDatePicked = this.onStartDatePicked.bind(this)
    this.onEndDatePicked = this.onEndDatePicked.bind(this)
    this.checkValidStartDate = this.checkValidStartDate.bind(this)
    this.checkValidEndDate = this.checkValidEndDate.bind(this)
  }

  onStartDatePicked (startDate) {
    const { onStartDatePicked } = this.props
    this.setState({ startDate })
    if (onStartDatePicked && typeof onStartDatePicked === 'function') {
      onStartDatePicked(startDate)
    }
  }

  onEndDatePicked (endDate) {
    const { onEndDatePicked } = this.props
    this.setState({ endDate })
    if (onEndDatePicked && typeof onEndDatePicked === 'function') {
      onEndDatePicked(endDate)
    }
  }

  checkValidStartDate (date) {
    const { endDate } = this.state
    if (!endDate) {
      return true
    }
    return date.isBefore(endDate)
  }

  checkValidEndDate (date) {
    const { startDate } = this.state
    if (!startDate) {
      return true
    }
    return date.isAfter(startDate)
  }

  render () {
    const { defaultStartDate, defaultEndDate } = this.props
    return (
      <div className='period-container'>
        <div className='period-sub-container'>
          <img
            src='/img/icons/svg/calendar2.svg'
            width={16}
            height={16}
          />
          <DateTimePicker
            onDatePicked={this.onStartDatePicked}
            checkValidDate={this.checkValidStartDate}
            defaultDate={defaultStartDate}
            inputStyle={{className: 'date-time-input'}}
          />
          <div>-</div>
          <DateTimePicker
            onDatePicked={this.onEndDatePicked}
            checkValidDate={this.checkValidEndDate}
            defaultDate={defaultEndDate}
            inputStyle={{className: 'date-time-input'}}
          />
        </div>
      </div>
    )
  }
}
