import React, { Component } from 'react'
import { withTracker } from 'meteor/react-meteor-data'
// import { UserTransactions } from '/common/collections/Payments'
import { MarketServiceTypes, MarketReservations, MarketServices } from '/common/collections/marketPlace'
import Totals from './totals'
import PieAndCaption from './pieAndCaption'

class DataProvider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      transactions: null
    }
  }

  render () {
    const { reservations, servicesType, services, tl } = this.props
    if (!reservations || reservations.length === 0) {
      return (
        <div className='stats-no-data'>
          <p className='text-no-data'>
            { tl.no_data }
          </p>
        </div>
      )
    }
    return (
      <div>
        <Totals
          reservations={reservations}
          tl={tl}
        />
        <PieAndCaption
          reservations={reservations}
          services={services}
          servicesType={servicesType}
          tl={tl}
        />
      </div>
    )
  }
}

export default withTracker((props) => {
  const condoId = FlowRouter.getParam('condo_id')
  const query = {}
  let startDate
  let endDate
  if (props.startDate) {
    startDate = props.startDate.getTime()
  }
  if (props.endDate) {
    endDate = props.endDate.getTime()
  }
  if (props.startDate && props.endDate) {
    query.validatedAt = { $ne: null, $gte: startDate, $lte: endDate }
  } else if (props.startDate) {
    query.validatedAt = { $ne: null, $gte: startDate }
  } else if (props.endDate) {
    query.validatedAt = { $ne: null, $lte: endDate }
  }
  return {
    // transReady: Meteor.subscribe('getUserTransactions', condoId).ready(),
    // transactions: UserTransactions.find(query).fetch(),
    reservations: MarketReservations.find(query).fetch(),
    services: MarketServices.find({ condoId: FlowRouter.getParam('condo_id') }).fetch(),
    servicesType: MarketServiceTypes.find({ condoId: FlowRouter.getParam('condo_id') }).fetch()
  }
})(DataProvider)
