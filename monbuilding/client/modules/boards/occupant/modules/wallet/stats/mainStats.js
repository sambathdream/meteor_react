import React, { Component } from 'react'
import SelectPeriod from './selectPeriod'
import './walletStats.less'
import DataProvider from './dataProvider'
import moment from 'moment/moment'
import { withTracker } from 'meteor/react-meteor-data'

class MainStats extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment().subtract(1, 'months'),
      endDate: moment()
    }
    this.onStartDatePicked = this.onStartDatePicked.bind(this)
    this.onEndDatePicked = this.onEndDatePicked.bind(this)
  }

  onStartDatePicked (date) {
    this.setState({
      startDate: date
    })
  }

  onEndDatePicked (date) {
    this.setState({
      endDate: date
    })
  }

  render () {
    const { startDate, endDate } = this.state
    const { tl } = this.props
    return (
      <div className='stats-container'>
        <SelectPeriod
          onStartDatePicked={this.onStartDatePicked}
          onEndDatePicked={this.onEndDatePicked}
          defaultStartDate={startDate}
          defaultEndDate={endDate}
          tl={tl}
        />
        <DataProvider
          startDate={new Date(startDate.format())}
          endDate={new Date(endDate.format())}
          tl={tl}
        />
      </div>
    )
  }
}

export default withTracker((props) => {
  return {
    tl: props.dicoLang.commonTranslation
  }
})(MainStats)
