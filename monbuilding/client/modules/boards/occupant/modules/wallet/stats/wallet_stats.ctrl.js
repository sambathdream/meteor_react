import MainStats from './mainStats.js'
import { CommonTranslation } from '../../../../../../../common/lang/lang'
Template.wallet_stats.onCreated(function () {
})

Template.wallet_stats.onRendered(function () {
})

Template.wallet_stats.events({
})

Template.wallet_stats.helpers({
  MainStats: () => MainStats,
  getDicoLang: () => {
    return new CommonTranslation(FlowRouter.getParam('lang') || 'fr')
  }
})
