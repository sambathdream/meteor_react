import React, { Component } from 'react'
import { Pie } from 'react-chartjs-2'

export default class PieAndCaption extends Component {
  static parseDataForChartJs (data) {
    const series = []
    const labels = []
    data.forEach((item) => {
      series.push(item.total)
      labels.push(item.type)
    })
    return {
      labels,
      datasets: [{
        data: series,
        backgroundColor,
        hoverBackgroundColor
      }]
    }
  }

  static parseData (servicesType, services, reservations, tl) {
    let data = []
    if (!reservations) {
      return data
    }
    let i = 0
    const uniqServices = _.uniq(services.map(s => s.type))
    data = servicesType.filter(s => uniqServices.includes(s.key)).map(s => {
      return {
        id: s._id,
        typeKey: s.key,
        type: tl[`mp_service_${s.key}`],
        total: 0,
        color: backgroundColor[i++]
      }
    })
    data.push({
      id: 'other',
      typeKey: 'other',
      type: tl[`other`],
      total: 0,
      color: backgroundColor[i]
    })
    reservations.forEach((item) => {
      let index
      if ((index = data.findIndex(value => value.typeKey === item.serviceTypeKey)) !== -1) {
        data[index].total += parseFloat(item.price * item.quantity)
        return
      } else {
        index = data.findIndex(value => value.typeKey === 'other')
        data[index].total += parseFloat(item.price * item.quantity)
        return
      }
    })
    return data
  }

  static renderItemCaption (data) {
    let i = 0
    return data.map((item) =>
      <div
        key={item.id}
        className='caption-item-container'
      >
        {/* /!\ make sure to have the same number of css classes that we have number of transaction types
         *  check walletStats.less
        */}
        <div
          id={`pie-caption-color-${i++}`}
          className='pie-caption-square'
        />
        <span className='item-caption-text'>
          { `${item.type} (${item.total}€)` }
        </span>
      </div>
    )
  }

  render () {
    const { tl } = this.props
    const data = PieAndCaption.parseData(this.props.servicesType, this.props.services, this.props.reservations, tl)  // todo pass transactions props instead of data test set
    const dataForChartJs = PieAndCaption.parseDataForChartJs(data)
    if (data.length === 0) {
      return (
        <div className='stats-no-data'>
          <p className='text-no-data'>
            { tl.no_data }
          </p>
        </div>
      )
    }
    return (
      <div className='pie-container'>
        <span className='stats-title'>
          { tl.wallet_stats_title_2 }
        </span>
        <span className='stats-sub-title'>
          { tl.wallet_stats_sub_title_2 }
        </span>
        <div className='pie-and-caption-container'>
          <div className='pie-wrapper'>
            <Pie
              data={dataForChartJs}
              legend={{display: false}}
              width={300}
              height={300}
              options={{
                ...customOptions,
                tooltips: customTooltip
              }}
            />
          </div>
          <div className='caption-container'>
            { PieAndCaption.renderItemCaption(data) }
          </div>
        </div>
      </div>
    )
  }
}
// /!\ make sure to have the same number of colors that we have number of transaction types
const backgroundColor = ['#fe0900', '#69d2e7', '#cff1f8', '#fdb813', '#8b999f', '#d9d9d9', '#007aff']
const hoverBackgroundColor = ['#fe0900', '#69d2e7', '#cff1f8', '#fdb813', '#8b999f', '#d9d9d9', '#007aff']

// data set for test
const dataSetTest = [
  { _id: 'a', type: 'Laundry', amount: 1500 },
  { _id: 'b', type: 'Printing', amount: 1000 },
  { _id: 'c', type: 'Printing', amount: 1000 },
  { _id: 'd', type: 'Cleaning', amount: 2000 },
  { _id: 'e', type: 'Laundry', amount: 1500 },
  { _id: 'f', type: 'Cafeteria', amount: 500 },
  { _id: 'g', type: 'Bicycle rental', amount: 2000 },
  { _id: 'h', type: 'Cleaning', amount: 1000 },
  { _id: 'i', type: 'Cleaning', amount: 1000 },
  { _id: 'j', type: 'Cafeteria', amount: 100 }
]

const customOptions = {
  plugins: {
    datalabels: false
  },
  maintainAspectRatio: false,
  responsive: true,
  hover: {mode: null},
  elements: {
    arc: {
      borderWidth: 0
    }
  }
}

const customTooltip = {
  // Disable the on-canvas tooltip
  enabled: false,

  custom: function (tooltipModel) {
    // Tooltip Element
    let tooltipEl = document.getElementById('chartjs-tooltip')

    // Create element on first render
    if (!tooltipEl) {
      tooltipEl = document.createElement('div')
      tooltipEl.id = 'chartjs-tooltip'
      tooltipEl.innerHTML = '<table/>'
      document.body.appendChild(tooltipEl)
    }

    // Hide if no tooltip
    if (tooltipModel.opacity === 0) {
      tooltipEl.style.opacity = 0
      return
    }

    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform')
    if (tooltipModel.yAlign) {
      tooltipEl.classList.add(tooltipModel.yAlign)
    } else {
      tooltipEl.classList.add('no-transform')
    }

    function getBody (bodyItem) {
      return bodyItem.lines
    }

    // Set Text
    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || []
      var bodyLines = tooltipModel.body.map(getBody)

      var innerHtml = '<thead>'

      titleLines.forEach(function (title) {
        innerHtml += '<tr><th>' + title + '</th></tr>'
      })
      innerHtml += '</thead><tbody>'

      bodyLines.forEach(function (body, i) {
        const data = body[0].split(': ')
        const colors = tooltipModel.labelColors[i]
        let style = 'background:' + colors.backgroundColor
        style += '; border-color:' + colors.borderColor
        style += '; border-width: 2px'
        const span = '<span style="' + style + '"></span>'
        innerHtml += '<tr><td>' + span + (!!data[0] ? data[0] : '') + (!!data[1] ? `<br/>${data[1]}€` : '') + '</td></tr>'
      })
      innerHtml += '</tbody>'

      const tableRoot = tooltipEl.querySelector('table')
      tableRoot.innerHTML = innerHtml
    }

    // `this` will be the overall tooltip
    const position = this._chart.canvas.getBoundingClientRect()

    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1
    tooltipEl.style.position = 'absolute'
    tooltipEl.style.left = position.left + tooltipModel.caretX + 'px'
    tooltipEl.style.top = position.top + tooltipModel.caretY - 50 + 'px'
    tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily
    tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px'
    tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle
    tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px'
  }
}
