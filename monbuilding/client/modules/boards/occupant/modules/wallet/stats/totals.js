import React, { Component } from 'react'

export default class Totals extends Component {
  constructor (props) {
    super(props)
    this.state = {}
    this.calcSpentOrCredit = this.calcSpentOrCredit.bind(this)
    this.calcTotalTrans = this.calcTotalTrans.bind(this)
  }

  calcSpentOrCredit (credit = false) {
    const { reservations } = this.props
    if (!reservations) {
      return 0
    }
    let total = 0
    reservations.forEach((item) => {
      // console.log('trans', trans)
      // if ((trans.status === 'AUTHORISED') && trans.isCredit === credit) {
        total += parseFloat(item.price * item.quantity)
      // }
    })
    return `${total}€`
  }

  calcTotalTrans () {
    const { reservations } = this.props
    if (!reservations) {
      return 0
    }
    return reservations.length
  }

  static renderItem (number, color, description) {
    return (
      <div>
        <p className='total-value'>
          { number }
        </p>
        <div
          className='total-bar'
          id={`total-bar-color-${color}`}
        />
        <p className='total-description'>
          { description }
        </p>
      </div>
    )
  }

  render () {
    const { tl } = this.props
    return (
      <div className='totals-container'>
        <span className='stats-title'>
          { tl.wallet_stats_title_1 }
        </span>
        <span className='stats-sub-title'>
          { tl.wallet_stats_sub_title_1 }
        </span>
        <div className='totals-sub-container'>
          { Totals.renderItem(this.calcSpentOrCredit(false), 1, tl.spent) }
          { Totals.renderItem(this.calcTotalTrans(), 2, tl.transactions) }
          {/* { Totals.renderItem(this.calcSpentOrCredit(true), 3, tl.top_up) } */}
        </div>
      </div>
    )
  }
}
