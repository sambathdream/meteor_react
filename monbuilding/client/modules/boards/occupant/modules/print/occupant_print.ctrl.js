Template.occupant_print.onCreated(function () {
  let tab = FlowRouter.getParam('tab')
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), tab: '1' })
  }

  // this.servicesHandler = Meteor.subscribe('market_place_feed')

})

Template.occupant_print.onRendered(function () {
})

Template.occupant_print.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab')
    FlowRouter.setParams({ 'tab': tabName })
  }
})

Template.occupant_print.helpers({
  checkTab: () => {
    const tab = FlowRouter.getParam('tab')
    const availableTab = ['1', '2']
    const moduleSlug = FlowRouter.current().params.module_slug

    if (moduleSlug === 'print' && (!tab || (!availableTab.includes(tab)))) {
      Meteor.defer(() => {
        FlowRouter.setParams({ 'tab': availableTab[0] })
      })
    }
  },
  getTemplateName: () => {
    // const tab = FlowRouter.getParam('tab')
    // return 'occupant_marketPlace_' + tab
  },
  getData: () => {
    // const condoId = FlowRouter.getParam('condo_id')
    // const tab = FlowRouter.getParam('tab')
    // let condoIds = []
    // if (Meteor.userHasRight('print', 'see', condoId)) {
    //   condoIds = [condoId]
    // }
    // return { condoIds: condoIds, isAll: condoId === 'all' }
  },
  getCondoName: () => {
    const condoId = FlowRouter.getParam('condo_id')
    return Condos.findOne({ _id: condoId }).getName()
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('tab')
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), 'tab': 'services' })
    }
    return tab
  },
  shouldDisplayRestrictedAccess: () => {
    const condoId = FlowRouter.getParam('condo_id')
    let condoIds = []
    if (condoId === 'all') {
      condoIds = Meteor.listCondoUserHasRight('print', 'see')
    } else if (Meteor.userHasRight('print', 'see', condoId)) {
      condoIds = [condoId]
    }
    return !condoIds.length
  },
})

