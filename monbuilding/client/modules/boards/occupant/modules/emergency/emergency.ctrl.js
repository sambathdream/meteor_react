/**
 * Created by kuyarawa on 18/05/18.
 */
import { ModuleTrombinoscopeIndex, ModuleReservationIndex } from '/common/lang/lang.js';

Template.module_emergency.onCreated(function() {
  this.subscribe('emergencyContactPhotos', FlowRouter.getParam('condo_id'));
  this.subscribe('emergencyContacts', FlowRouter.getParam('condo_id'));
  this.search = new ReactiveVar("");
  this.selectedContact = new ReactiveVar(null);
  this.hasRight = new ReactiveVar(false)
})

Template.module_emergency.onDestroyed(function() {
})

Template.module_emergency.onRendered(() => {
})

Template.module_emergency.events({
  'click .open-hours' (e, t) {
    const element = $(e.currentTarget);
    const id = element.data('id');
    t.selectedContact.set(id);

    $("#emergency-contact-open-hour").modal("show");
  },
  'hide.bs.modal #emergency-contact-open-hour': (e, t) => {
    t.selectedContact.set(null);
  },
})

Template.module_emergency.helpers({
  isSubscribeReady: () => {
    const isReady = Template.instance().subscriptionsReady()
    if (isReady === true) {
      Template.instance().hasRight.set(Meteor.userHasRight('emergencyContact', 'see'))
    }
    return isReady
  },
  getCondoName: () => {
    return Condos.findOne({ _id: FlowRouter.getParam('condo_id') }).getName()
  },
  getContact: () => {
    if (!Meteor.condoHasModule('emergencyContact')) {
      Template.instance().hasRight.set(false)
    } else {
      Template.instance().hasRight.set(true)
      const instance = Template.instance();
      const regexp = new RegExp(instance.search.get(), "i");
      return EmergencyContact.find({$or: [
        {"firstName": regexp},
        {"lastName": regexp},
        {"companyName": regexp},
        {"phone": regexp},
        {"landLine": regexp},
        {"inChargeOf": regexp}
      ]}, {sort: {firstName: 1}}).fetch()
    }
  },
  getContactName: (contact, initial) => {
    return !!initial ? `${contact.firstName[0]}${contact.lastName[0]}`: `${contact.firstName} ${contact.lastName}`;
  },
  onSearch: () => {
    const instance = Template.instance();

    return (text) => {
      instance.search.set(text);
    }
  },
  getProfileUrl: (contactId) => {
    let avatar = EmergencyContactPhotos.findOne({ contactId: contactId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  messageButton: (userId) => {
    const lang = (FlowRouter.getParam("lang") || "fr");
    const translate = new ModuleTrombinoscopeIndex(lang);
    return {
      label: translate.moduleTrombinoscopeIndex["message"],
      class: 'red',
      onClick: () => {
        console.log(userId)
      }
    }
  },
  getDetail: () => {
    return EmergencyContact.findOne(Template.instance().selectedContact.get())
  },
  renderSchedule: () => {
    const contact = EmergencyContact.findOne(Template.instance().selectedContact.get());
    if (!!contact) {
      const condo = _.find(contact.condos, c => c.id === FlowRouter.getParam('condo_id'))
      if (!!condo) {
        const lang = (FlowRouter.getParam("lang") || "fr");
        const translate = new ModuleReservationIndex(lang);
        const label = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];

        return _.reduce(condo.schedule, (string, s) => {
          const day = translate.moduleReservationIndex[label[s.dow]].substring(0, 3);
          if (!!s.open) {
            return string + '<p>' + day + ': ' + _.map(s.openHours, (o) => { return o.start + ' - ' + o.end}).join(' / ') + '</p>'
          } else {
            return string + '<p>' + day + ': ' + translate.moduleReservationIndex['close'] + '</p>'
          }
        }, '')
      }
    }
  },
  isSearchMode: () => {
    return Template.instance().search.get() !== ""
  },
  getInChargeOf: (contact) => {
    const lang = FlowRouter.getParam("lang") || "fr"

    return !!contact ? (!!contact['inChargeOf-' + lang] ? contact['inChargeOf-' + lang] : contact.inChargeOf) : ''
  },
  hasRight: () => Template.instance().hasRight.get()
})
