var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
import { CommonTranslation } from "/common/lang/lang.js";

Template.pendingUsers.onCreated(function() {
  this.condoId = FlowRouter.getParam('condo_id')
  this.searchText = new ReactiveVar('')
})

Template.pendingUsers.onDestroyed(function() {
})

Template.pendingUsers.onRendered(() => {
})

Template.pendingUsers.events({
  'click .containerLine': (event, template) => {
    let userId = $(event.currentTarget).attr('userId')
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'detailUser',
      wildcard: userId
    });
  },
  'click .validOccupantColumn': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const userId = $(event.currentTarget).attr('userId')
    const condoId = $(event.currentTarget).attr('condoId')

    bootbox.confirm({
      title: "Confirmation",
      message: "Voulez-vous vraiment accepter cet occupant ? ",
      buttons: {
        cancel: { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        confirm: { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      callback: function (result) {
        if (result) {
          Meteor.call('newResidentUserConfirm', userId, condoId, (err, res) => {
            if (err) {
              sAlert.error(translate.commonTranslation.error_occured)
            }
            else
              sAlert.success("Validation réussie")
          });
        }
      }
    })
  },
  'click .denyOccupantColumn': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const userId = $(event.currentTarget).attr('userId')
    const condoId = $(event.currentTarget).attr('condoId')

    bootbox.confirm({
      title: "Confirmation",
      message: "Voulez-vous vraiment supprimer cet occupant ? ",
      buttons: {
        cancel: { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        confirm: { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      callback: function (result) {
        if (result) {
          Meteor.call('newResidentUserDeny', userId, condoId, (err, res) => {
            if (err)
              sAlert.error(translate.commonTranslation.error_occured)
            else
              sAlert.success("Suppression réussie")
          })
        }
      }
    })
  }
})

Template.pendingUsers.helpers({
  getOccupants: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    const thisResident = Residents.findOne({ userId: Meteor.userId() })
    const condoId = Template.instance().condoId
    const thisResidentCondo = thisResident.condos.find( condo => {
      return condo.condoId === condoId
    })

    let thisCondo = Condos.findOne({ _id: condoId })
    let condoName = ''
    if (thisCondo.info.id != "-1")
      condoName = thisCondo.info.id + ' - ' + thisCondo.getName()
    else
      condoName = thisCondo.getName();
    _userCondosHasMatched = thisCondo.name.match(regexp) || thisCondo.info.address.match(regexp) || thisCondo.info.city.match(regexp) || thisCondo.info.code.match(regexp)

    thisCondo = {
      _id: condoId,
      name: condoName
    }


    let occupants = []
    let users = []

    const condosOptions = CondosModulesOptions.findOne({ condoId: condoId })
    if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
      const companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
      occupants = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch: {
            'condoId': condoId,
            'userInfo.company': companyId
          }
        },
        'resident.validation.type': 'gestionnaire'
      })
    } else {
      occupants = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings.condoId': condoId,
        'resident.validation.type': 'gestionnaire'
      })
    }
    occupants.forEach(user => {
      let thisUserProfile = UsersProfile.findOne({ _id: user._id })
      if (thisUserProfile) {

        let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
        let phoneMatch = false
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
          }
        } catch (e) { }

        const _userHasMatched =
          thisUserProfile.email.match(regexp) ||
          thisUserProfile.firstname.match(regexp) ||
          thisUserProfile.lastname.match(regexp) ||
          thisUserProfile.tel.match(regexp) ||
          (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          phoneMatch
        thisUserCondo = user.resident.pendings.find(condo => { return condo.condoId === condoId })
        if (_userHasMatched || _userCondosHasMatched) {
          let avatar = Avatars.findOne({ _id: thisUserProfile._id })
          if (avatar && avatar.avatar.original) {
            thisUserProfile.avatar = avatar.avatar.original
          } else {
            thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
          }
          users.push({
            ...thisUserProfile,
            condo: thisCondo,
            invited: thisUserCondo.invited || null,
          })
        }
      }
    })
    users.sort((a, b) => { return b.invited - a.invited })

    return users
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: () => {
    const thisResident = Residents.findOne({ userId: Meteor.userId() })
    const condoId = Template.instance().condoId
    const thisResidentCondo = thisResident.condos.find(condo => {
      return condo.condoId === condoId
    })

    let occupants = null

    const condosOptions = CondosModulesOptions.findOne({ condoId: condoId })

    if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
      const companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
      occupants = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch: {
            'condoId': condoId,
            'userInfo.company': companyId
          }
        },
        'resident.validation.type': 'gestionnaire'
      })
    } else {
      occupants = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings.condoId': condoId,
        'resident.validation.type': 'gestionnaire'
      })
    }
    return occupants ? occupants.count() : 0
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitOccupant').click()
    }
  },
  getCondoIdsRights: () => {
    return [Template.instance().condoId.get()]
  }
})
