Template.occupantListForOccupant.onCreated(function() {
  let tab = FlowRouter.getParam('wildcard')
  if (!tab) {
    FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id'), wildcard: 'active'})
  }
})

Template.occupantListForOccupant.onDestroyed(function() {
})

Template.occupantListForOccupant.onRendered(() => {
})

Template.occupantListForOccupant.events({
  'click .tabBarContainer > div': (e, t) => {
    let tabName = $(e.currentTarget).data('tab');
    FlowRouter.setParams({ wildcard: tabName })
  },
  'click .invitOccupant': (e, t) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'createOccupant',
      wildcard: null
    });
  }
})

Template.occupantListForOccupant.helpers({
  getTemplateName: () => {
    let tab = FlowRouter.getParam('wildcard')
    if (tab === 'active') {
      return 'activeUsers'
    } else {
      return 'pendingUsers'
    }
  },
  checkRights: () => {
    if (!Meteor.userHasRight("trombi", "seeOccupantList")) {
      FlowRouter.go('app.board.resident.condo', { lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id') });
    }
  },
  selectedTab: () => {
    let tab = FlowRouter.getParam('wildcard')
    if (!tab) {
      FlowRouter.setParams({ lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id'), wildcard: 'active' })
    }
    return tab
  },
  getCondoName: () => {
    return Condos.findOne({ _id: FlowRouter.getParam('condo_id') }).getName()
  },
  getPendingCounter: () => {
    const condoId = FlowRouter.getParam('condo_id')
    let counter = 0
    BadgesResident.find({ _id: condoId }).forEach(condoBadge => {
      counter += condoBadge.newUser || 0
    });
    return counter
  }
})
