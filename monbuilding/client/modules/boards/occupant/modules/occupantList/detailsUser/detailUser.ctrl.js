Template.detailUser.onCreated(function () {
  this.userId = FlowRouter.getParam('wildcard')
})

Template.detailUser.onDestroyed(function () {
})

Template.detailUser.onRendered(() => {
})

Template.detailUser.events({
  'click #backbutton': (event, template) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'occupantList',
      wildcard: 'active'
    });
  },
  'click .invitOccupant': (e, t) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'createOccupant',
      wildcard: null
    });
  }
})

Template.detailUser.helpers({
  getUserProfile: () => {
    let profile = UsersProfile.findOne({ _id: Template.instance().userId })
    const lang = FlowRouter.getParam("lang") || "fr";
    return profile
  },
  checkRights: () => {
    if (!Meteor.userHasRight("trombi", "seeOccupantList")) {
      FlowRouter.go('app.board.resident.condo', { lang: FlowRouter.getParam('lang'), condo_id: FlowRouter.getParam('condo_id') });
    }
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().userId })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  getCondoName: () => {
    return Condos.findOne({ _id: FlowRouter.getParam('condo_id') }).getName()
  }
})
