import { CommonTranslation } from "/common/lang/lang.js";
var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.activeUsers.onCreated(function() {
  this.condoId = FlowRouter.getParam('condo_id')
  this.searchText = new ReactiveVar('')
})

Template.activeUsers.onDestroyed(function() {
})

Template.activeUsers.onRendered(() => {
})

Template.activeUsers.events({
  'click .containerLine': (event, template) => {
    let userId = $(event.currentTarget).attr('userId')
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'detailUser',
      wildcard: userId
    });
  },
  'click .commonColumn.resendInvitation > button': function (event, template) {
    event.preventDefault()
    event.stopPropagation()
  },
  'click .commonColumn.trashColumn > div': (event, template) => {
    event.preventDefault()
    event.stopPropagation()
    const userId = $(event.currentTarget).attr('userId')
    const condoId = $(event.currentTarget).attr('condoId')
    const condoName = $(event.currentTarget).attr('condoName')
    const userName = $(event.currentTarget).attr('userName')

    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const user = UsersProfile.findOne({ _id: userId })
    bootbox.confirm({
      size: "medium",
      title: translate.commonTranslation.confirmation,
      message: translate.commonTranslation.are_you_sure_remove + userName + translate.commonTranslation.are_you_sure_remove2 + condoName + translate.commonTranslation.interrogation_point,
      buttons: {
        'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
        'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
      },
      backdrop: true,
      callback: function (result) {
        if (result) {
          Meteor.call('removeOccupantFromCondo', userId, condoId, (err, res) => {
            if (err)
              sAlert.error(translate.commonTranslation.error_occured)
            else {
              sAlert.success(translate.commonTranslation.occupant_removed_from_building)
            }
          })
        }
      }
    })

  }
})

Template.activeUsers.helpers({
  getOccupants: () => {
    let regexp = new RegExp(Template.instance().searchText.get().replace(/[|\\{}()[\]^$+*?.]/g, '\\$&'), 'i')
    const thisResident = Residents.findOne({ userId: Meteor.userId() })
    const condoId = Template.instance().condoId
    const thisResidentCondo = thisResident.condos.find( condo => {
      return condo.condoId === condoId
    })

    let thisCondo = Condos.findOne({ _id: condoId })
    let condoName = ''
    if (thisCondo.info.id != "-1")
      condoName = thisCondo.info.id + ' - ' + thisCondo.getName()
    else
      condoName = thisCondo.getName();
    _userCondosHasMatched = thisCondo.name.match(regexp) || thisCondo.info.address.match(regexp) || thisCondo.info.city.match(regexp) || thisCondo.info.code.match(regexp)

    thisCondo = {
      _id: condoId,
      name: condoName
    }


    let occupants = []
    let occupantsPending = []
    let users = []

    const condosOptions = CondosModulesOptions.findOne({ condoId: condoId })
    if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
      const companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
      occupants = ResidentUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.condos': {
          $elemMatch:
            { 'condoId': condoId,
              'userInfo.company': companyId },
        }
      })
      occupantsPending = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch:
            { 'condoId': condoId,
              'userInfo.company': companyId,
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      })
    } else {
      occupants = ResidentUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.condos.condoId': condoId
      })
      occupantsPending = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch:
            { 'condoId': condoId,
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      })
    }
    occupants = [...occupants, { pendings: true }, ...occupantsPending]
    let isPending = false
    occupants.forEach(user => {
      if (user.pendings && user.pendings === true) {
        isPending = true
        return ;
      }
      let thisUserProfile = UsersProfile.findOne({ _id: user._id })
      if (thisUserProfile) {

        let number = '+' + thisUserProfile.telCode + thisUserProfile.tel
        let phoneMatch = false
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            phoneMatch = phoneUtil.format(number, PNF.ORIGINAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).match(regexp) || phoneUtil.format(number, PNF.NATIONAL).replace(/ /gi, '').match(regexp) || phoneUtil.format(number, PNF.INTERNATIONAL).match(regexp) || phoneUtil.format(number, PNF.E164).match(regexp)
          }
        } catch (e) { }

        const _userHasMatched =
          thisUserProfile.email.match(regexp) ||
          thisUserProfile.firstname.match(regexp) ||
          thisUserProfile.lastname.match(regexp) ||
          thisUserProfile.tel.match(regexp) ||
          (thisUserProfile.firstname + ' ' + thisUserProfile.lastname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          (thisUserProfile.lastname + ' ' + thisUserProfile.firstname).match(regexp) ||
          phoneMatch
        if (!isPending) {
          thisUserCondo = user.resident.condos.find(condo => { return condo.condoId === condoId})
        } else {
          thisUserCondo = user.resident.pendings.find(condo => { return condo.condoId === condoId })
        }
        if (_userHasMatched || _userCondosHasMatched) {
          let avatar = Avatars.findOne({ _id: thisUserProfile._id })
          if (avatar && avatar.avatar.original) {
            thisUserProfile.avatar = avatar.avatar.original
          } else {
            thisUserProfile.initials = thisUserProfile.firstname[0] + thisUserProfile.lastname[0]
          }
          users.push({
            ...thisUserProfile,
            condo: thisCondo,
            isPending: isPending,
            isDeferred: (user.deferredRegistrationDate && user.deferredRegistrationDate > Date.now()) || null,
            deferredRegistrationDate: user.deferredRegistrationDate || null,
            invited: thisUserCondo.invited || null,
          })
        }
      }
    })

    users.sort((a, b) => { return b.invited - a.invited })

    return users
  },
  shouldDisplaySearch: () => {
    return true
  },
  getTotal: () => {
    const thisResident = Residents.findOne({ userId: Meteor.userId() })
    const condoId = Template.instance().condoId
    const thisResidentCondo = thisResident.condos.find(condo => {
      return condo.condoId === condoId
    })

    let occupants = null
    let occupantsPending = null

    const condosOptions = CondosModulesOptions.findOne({ condoId: condoId })

    if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
      const companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
      occupants = ResidentUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.condos': {
          $elemMatch:
            { 'condoId': condoId,
              'userInfo.company': companyId },
        }
      })
      occupantsPending = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch:
            { 'condoId': condoId,
              'userInfo.company': companyId,
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      })
    } else {
      occupants = ResidentUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.condos.condoId': condoId
      }, { fields: { _id: true } })
      occupantsPending = ResidentPendingUsers.find({
        '_id': { $ne: Meteor.userId() },
        'resident.pendings': {
          $elemMatch:
            { 'condoId': condoId,
              'invitByGestionnaire': { $exists: true },
              'invitByGestionnaire': true },
        }
      }, { fields: { _id: true } })
    }
    const count = (occupants ? occupants.count() : 0) + (occupantsPending ? occupantsPending.count() : 0)
    return count
  },
  searchCallback: () => {
    let template = Template.instance()
    return (value) => {
      $('.searchResult').css('display', !!value ? 'block' : 'none')
      template.searchText.set(value)
    }
  },
  addNewManager: () => {
    return () => {
      $('.invitOccupant').click()
    }
  },
  getCondoIdsRights: () => {
    return [Template.instance().condoId.get()]
  },
  resendInvitation: (user) => {
    const t = Template.instance()

    return () => () => {
      const userId = user._id

      const lang = FlowRouter.getParam("lang") || "fr"
      const translate = new CommonTranslation(lang)

      let title = translate.commonTranslation.confirmation
      let message = translate.commonTranslation.confirm_sending_validation + user.firstname + ' ?'
      bootbox.confirm({
        size: "medium",
        title,
        message,
        buttons: {
          'cancel': { label: translate.commonTranslation.cancel, className: "btn-outline-red-confirm" },
          'confirm': { label: translate.commonTranslation.confirm, className: "btn-red-confirm" }
        },
        backdrop: true,
        callback: function (result) {
          if (result) {
            Meteor.call('resendValidationToUser', userId, user.condo._id, (err, res) => {
              if (err) {
                sAlert.error(translate.commonTranslation.error_occured)
              } else {
                sAlert.success(translate.commonTranslation.invitation_send_success)
              }
            })
          }
        }
      })
    }
  }
})
