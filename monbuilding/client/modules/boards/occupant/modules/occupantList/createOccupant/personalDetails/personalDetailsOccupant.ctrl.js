
import { CommonTranslation } from "/common/lang/lang.js";
import moment from 'moment'
import DateTime from 'react-datetime'


var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Template.personalDetailsOccupantFromOccupant.onCreated(function () {
  this.subscribe('PhoneCode');
  this.subscribe('schools');

  this.condoId = FlowRouter.getParam('condo_id') || null
  const condo = Condos.findOne({ _id: this.condoId }, { fields: { 'settings': true } })
  this.condoType = condo && (condo.settings.condoType === 'etudiante' ? 'student' : condo.settings.condoType)


  this.searchText = new ReactiveVar('')
  this.canSubmit = new ReactiveVar(false)
  this.form = new ReactiveDict()
  this.allCondosSelected = false
  this.showCustimizeRights = new ReactiveVar(false)
  this.deferredRegistrationDate = new ReactiveVar('')

  let companyId = null
  let self = this
  const condosOptions = CondosModulesOptions.findOne({ condoId: this.condoId })
  if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
    const thisResident = Residents.findOne({ userId: Meteor.userId() })
    const thisResidentCondo = thisResident.condos.find(condo => {
      return condo.condoId === self.condoId
    })
    companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
  }

  this.goodPhone = false
  this.goodLandline = false
  this.customizeRoleId = null
  this.customizeCondoId = null
  this.customizeIndex = null
  this.form.setDefault({
    civilite: null,
    firstname: null,
    lastname: null,
    email: null,
    phone: null,
    phoneCode: '33',
    phoneCodeLandline: '33',
    landline: null,
    condos: [this.condoId],
    roles: [null],
    building: [null],
    status: [null],
    company: [companyId || null],
    diploma: [null],
    school: [null],
    studies: [null],
    door: [null],
    floor: [null],
    office: [null],
    deferredRegistrationDate: null
  })
  this.customRights = [null]
  this.shouldDisplayAddCompany = new ReactiveVar([null])

  this.registrationType = new ReactiveDict()
  this.registrationType.setDefault({
    immediateRegistration: true,
    deferredRegistration: false
  })
})

Template.personalDetailsOccupantFromOccupant.onDestroyed(function () {
})

Template.personalDetailsOccupantFromOccupant.onRendered(() => {
})

Template.personalDetailsOccupantFromOccupant.events({
  'hidden.bs.modal #modal_rights_manager': function (event, template) {
    template.showCustimizeRights.set(false)
    template.customizeRoleId = null
    template.customizeCondoId = null

    history.replaceState('', document.title, window.location.pathname);
  },
  'show.bs.modal #modal_rights_manager': function (event, template) {
    let modal = $(event.currentTarget);
    window.location.hash = "#modal_rights_manager";
    window.onhashchange = function () {
      if (location.hash != "#modal_rights_manager") {
        modal.modal('hide');
      }
    }
  },
  'click #backbutton': (event, template) => {
    const lang = FlowRouter.getParam("lang") || "fr";
    FlowRouter.go('app.board.resident.condo.module', {
      lang: lang,
      condo_id: FlowRouter.getParam('condo_id'),
      module_slug: 'occupantList',
      wildcard: 'active'
    });
  },
  'click .submitNewOccupant': (event, template) => {
    if (template.canSubmit.get() === true) {
      $(event.currentTarget).button('loading')
      let form = template.form.all()
      let customRights = template.customRights
      Meteor.call('createNewOccupantFromManagerSide', form, customRights, (error, result) => {
        if (!error && result) {
          const lang = FlowRouter.getParam("lang") || "fr";
          FlowRouter.go('app.board.resident.condo.module', {
            lang: lang,
            condo_id: FlowRouter.getParam('condo_id'),
            module_slug: 'detailUser',
            wildcard: result
          });
        } else {
          sAlert.error(error)
        }
        $(event.currentTarget).button('reset')
      })
    }
  },
  'click .labelForCheckbox': (e, t) => {
    const stat = $(e.currentTarget).attr("for")
    if (!t.registrationType.get(stat)) {
      t.registrationType.set(stat, !t.registrationType.get(stat))
      if (stat === 'immediateRegistration') {
        t.registrationType.set('deferredRegistration', !t.registrationType.get(stat))
        t.form.set('deferredRegistrationDate', null)
      } else {
        t.registrationType.set('immediateRegistration', !t.registrationType.get(stat))
      }
    }
  },
})

function updateSubmit(template) {
  let canSubmit = false
  const form = template.form.all()
  canSubmit =
    form.firstname !== null &&
    form.lastname !== null &&
    form.email !== null && (Isemail.validate(form.email) === true) &&
    (form.phone === null || form.phone === '' || template.goodPhone === true ) &&
    (form.landline === null || form.landline === '' || template.goodLandline === true ) &&
    form.condos[0] !== null &&
    form.status[0] !== null &&
    form.roles[0] !== null
  template.canSubmit.set(canSubmit)
}

Template.personalDetailsOccupantFromOccupant.helpers({
  onSelectCb: (key, index) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'condos') {
        let { building, status, company, diploma, school, studies, door, floor, office } = t.form.all()

        building[index] = null
        status[index] = null
        company[index] = company[index]
        diploma[index] = null
        school[index] = null
        studies[index] = null
        door[index] = null
        floor[index] = null
        office[index] = null

        t.form.set({building,
          status,
          company,
          diploma,
          school,
          studies,
          door,
          floor,
          office }
        )
      }
      if (key === 'condos' || key === 'roles') {
        t.customRights[index] = null
      }
      let form = t.form.all()[key]
      form[index] = !selected ? null : selected
      t.form.set(key, form)
      updateSubmit(t)
    }

  },
  canCustomizeRights: (index) => {
    const t = Template.instance()

    let form = t.form.get('roles')
    let formCondo = t.form.get('condos')

    if (!!form[index] && !!formCondo[index]) {
      return true
    } else {
      return false
    }
  },
  customizeRightsOn: () => {
    return Template.instance().showCustimizeRights.get()
  },
  customizeRights: (index) => {
    const t = Template.instance()

    return () => e => {
      t.customizeIndex = index
      t.customizeRoleId = t.form.get('roles')[index]
      t.customizeCondoId = t.form.get('condos')[index]
      t.showCustimizeRights.set(true)
      Meteor.setTimeout(function() {
        $("#modal_rights_manager").modal('show')
      }, 500);
    }
  },
  getCustomRights: () => {
    let t = Template.instance()
    return t.customRights[t.customizeIndex]
  },
  saveModalRights: () => {
    let t = Template.instance()

    return (newRights) => {
      t.customRights[t.customizeIndex] = newRights
      t.customizeRoleId = null
      t.customizeCondoId = null
      $("#modal_rights_manager").modal('hide')
    }

  },
  getCustomizeRoleId: () => {
    return Template.instance().customizeRoleId
  },
  getCustomizeCondoId: () => {
    return Template.instance().customizeCondoId
  },
  selectedCondo: () => {
    return Template.instance().form.get('condos')
  },
  onSelect: (key) => {
    const t = Template.instance()

    return () => selected => {
      if (key === 'phoneCode') {
        let number = '+' + selected + t.form.get('phone')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set('phone', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          }
        } catch (e) {
          t.goodPhone = false
        }
      } else if (key === 'phoneCodeLandline') {
        let number = '+' + selected + t.form.get('landline')
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set('landline', phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          }
        } catch (e) {
          t.goodLandline = false
        }
      }
      t.form.set(key, !selected ? null : selected)
      updateSubmit(t)
    }
  },
  canSubmit: () => {
    return Template.instance().canSubmit.get()
  },
  isFieldError: (key) => {
    const t = Template.instance();
    switch (key) {
      case 'email': {
        const value = t.form.get(key)
        return value !== null && value !== '' && (Isemail.validate(value) === false)
      }
      case 'phone': {
        const value = t.form.get(key)
        let number = '+' + t.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodPhone = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodPhone = false
          return value !== null && value !== ''
        }
      }
      case 'landline': {
        const value = t.form.get(key)
        let number = '+' + t.form.get('phoneCodeLandline') + value
        try {
          number = phoneUtil.parse(number, "FR")
          let res = phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)
          t.goodLandline = res
          return value !== null && value !== '' && (res === false)
        } catch (e) {
          t.goodLandline = false
          return value !== null && value !== ''
        }
      }
      default:
        return false
    }
  },
  onInputDetails: (key, index) => {
    const t = Template.instance()

    return () => value => {
      if (key === 'phone') {
        let number = '+' + t.form.get('phoneCode') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodPhone = true
          } else {
            t.form.set(key, !value ? null : value)
            t.goodPhone = false
          }
        } catch (e) {
          t.form.set(key, !value ? null : value)
          t.goodPhone = false
        }
      } else if (key === 'landline') {
        let number = '+' + t.form.get('phoneCodeLandline') + value
        try {
          number = phoneUtil.parse(number, "FR")
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            t.form.set(key, phoneUtil.format(number, PNF.ORIGINAL))
            t.goodLandline = true
          } else {
            t.form.set(key, !value ? null : value)
            t.goodLandline = false
          }
        } catch (e) {
          t.form.set(key, !value ? null : value)
          t.goodLandline = false
        }
      } else if (key === 'company' || key === 'diploma' || key === 'door' || key === 'floor' || key === 'office') {
        if (key === 'company') {
          const name = new RegExp(`^${value}$`, 'ig')
          const thisCompany = CompanyName.findOne({ name: name })
          let shouldDisplayAddCompany = t.shouldDisplayAddCompany.get()
          shouldDisplayAddCompany[index] = !thisCompany && !!value
          t.shouldDisplayAddCompany.set(shouldDisplayAddCompany)
          let thisVal = t.form.get(key)
          thisVal[index] = (shouldDisplayAddCompany[index] || !value) ? (thisVal[index] ? thisVal[index] : null) : thisCompany._id
          if (value === '') {
            thisVal[index] = null
          }
          t.form.set(key, thisVal)
        } else {
          let thisVal = t.form.get(key)
          thisVal[index] = !value ? null : value
          t.form.set(key, thisVal)
        }
      } else {
        t.form.set(key, !value ? null : value)
      }
      updateSubmit(t)
    }
  },
  getCiviliteOptions: () => {
    const lang = (FlowRouter.getParam("lang") || "fr")
    const translation = new CommonTranslation(lang);
    return [
      {text: translation.commonTranslation['mister'], id: 'm'},
      {text: translation.commonTranslation['miss'], id: 'mme'}
    ]
  },
  getSchoolOptions: () => {
    let school = Schools.findOne();
    if (school) {
      return _.map(school.schools, (elem) => {
        return {
          text: elem.name,
          id: elem.name
        };
      });
    }
  },
  getStudiesOptions: () => {
    return schoolSubject
  },
  getStudiesGroup: () => {
    return [
      "Business & Management",
      "Finance",
      "Admin, RH & Juridique",
      "IT & Digital",
      "Sciences naturelles & Ingénierie",
      "Production & Logistique",
      "Humanités et Création",
      "Sciences Médicales",
      "Autres"
    ]
  },
  getBuildingOptions: (condoId) => {
    const lang = (FlowRouter.getParam('lang') || 'fr')
    const translation = new CommonTranslation(lang)
    let buildings = [{
      text: translation.commonTranslation['undefined'],
      id: '-1'
    }]
    Buildings.find({ condoId: condoId }).forEach(building => {
      buildings.push({
        text: building.getName(),
        id: building._id
      })
    })
    return buildings
  },
  getStatusOptions: () => {
    const lang = (FlowRouter.getParam("lang") || "fr")
    const translation = new CommonTranslation(lang);
    let buildings = [{
      text: translation.commonTranslation['user'],
      id: 'user'
    }, {
      text: translation.commonTranslation['owner'],
      id: 'owner'
    }, {
      text: translation.commonTranslation['tenant'],
      id: 'tenant'
    }]
    return buildings
  },
  getForm: (key, index) => {
    const t = Template.instance()

    if (index === null) {
      return () => t.form.get(key) || ''
    } else {
      return () => {
        if (key === 'company') {
          const companyId = t.form.get(key)[index]
          let company = companyId ? CompanyName.findOne({ _id: companyId }).name : null
          return company
        } else {
          return t.form.get(key)[index] || ''
        }
      }
    }
  },
  getroleOptions: () => {
    const condoType = Template.instance().condoType
    let listRoles = DefaultRoles.find({ for: 'occupant', ['forCondoType.' + condoType]: true })
    let roles = []
    if (listRoles) {
      listRoles.forEach(role => {
        roles.push({
          text: role.name,
          id: role._id
        })
      })
    }
    return roles
  },
  getCondoOptions: (index) => {
    const condo = Condos.findOne({ _id: Template.instance().condoId})
    let condos = [{
      text: condo.getName(),
      id: condo._id
    }]

    return condos
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady();
  },
  setOpacity: () => {
    setTimeout(() => {
      $('.condoListContainerOccupant').css('opacity', 1)
    }, 1);
  },
  getCondoName: () => {
    return Condos.findOne({ _id: FlowRouter.getParam('condo_id') }).getName()
  },
  getDateTimeComponent: () => DateTime,
  getDelayedDate: () => {
    const tpl = Template.instance()
    return tpl.deferredRegistrationDate.get() === '' ? null : tpl.deferredRegistrationDate.get()
  },
  getDatePlaceholder: () => {
    return {
      placeholder: moment().add(2, 'hour').format('MMMM Do YYYY, h:mm:ss a'),
      readOnly: true
    }
  },
  handleChangeDate: () => {
    const tpl = Template.instance()

    return (date) => {
      if (parseInt(moment(date).format('x')) > parseInt(moment().format('x'))) {
        tpl.deferredRegistrationDate.set(date)
        tpl.form.set('deferredRegistrationDate', parseInt(moment(tpl.deferredRegistrationDate.get()).format('x')))
        updateSubmit(tpl)
      }
    }
  },
  getLocal: () => {
    return FlowRouter.getParam('lang') || 'fr'
  },
  defaultDateTime: () => {
    return moment().add(2, 'hour')
  },
  isValidDate: () => {
    return (current) => {
      return current.isAfter(moment().subtract(1, 'day'))
    }
  },
  getChecked: (key) => {
    const tpl = Template.instance()

    const value = tpl.registrationType.get(key)
    return value ? 'checked' : ''
  },
  useDeferredRegistration: () => {
    const tpl = Template.instance()

    if (tpl.registrationType.get('deferredRegistration') && tpl.form.get('deferredRegistrationDate') === null) {
      tpl.deferredRegistrationDate.set(moment().add(2, 'hour'))
      tpl.form.set('deferredRegistrationDate', parseInt(moment().add(2, 'hour').format('x')))
    }

    return tpl.registrationType.get('deferredRegistration')
  }
})


let schoolSubject = [
  [
    "Achat",
    "Commercial & Business Development",
    "Communication, RP & Evénementiel",
    "Management, Conseil & Stratégie",
    "Marketing & Webmarketing",
    "Relations publiques, publicité",
    "Service & Relations Clients",
    "Tourisme, Restauration & Hotellerie"
  ],
  [
    "Actuariat",
    "Audit",
    "Contrôle de Gestion & Comptabilité",
    "Economie",
    "Finance d'entreprise",
    "Finance de marché",
    "Gestion d'actifs"
  ],
  [
    "Administratif",
    "Droit",
    "Ressources Humaines"
  ],
  [
    "Développement informatique",
    "Electronique & Traitement du signal",
    "Gestion de projet IT & Product",
    "Infra, Réseaux & Télécoms",
    "Intelligence artificielle",
    "Webdesign & Ergonomie"
  ],
  [
    "Agronomie",
    "Aéronautique",
    "Architecture & Urbanisme",
    "Biologie",
    "Chimie & Procédés",
    "Energie, Matériaux & Mécanique",
    "Environnement",
    "Génie Civil & Structures",
    "Génie Industriel & Conception",
    "Statistiques, Data & Math App"
  ],
  [
    "Logistique & Supply Chain",
    "Production & Exploitation",
    "Qualité & Maintenance",
    "Travaux & Chantier"
  ],
  [
    "Audiovisuel",
    "Création & Graphisme",
    "Journalisme & Edition",
    "Musique",
    "Philosophie et autres sciences"
  ],
  [
    "Dentisterie",
    "Infirmière",
    "Médecine",
    "Pharmacie",
    "Psychiatrie",
    "Santé publique",
    "Technologie Médicale",
    "Vétérinaire"
  ],
  [
    "Education & Formation",
    "Géographie & Géologie",
    "Langues",
    "Sciences Sociales",
    "Jeune travailleur",
    "Autres"
  ]
]
