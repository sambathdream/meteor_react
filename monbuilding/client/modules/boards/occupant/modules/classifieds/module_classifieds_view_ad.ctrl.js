import { Template } from 'meteor/templating';
import { FileManager } from '/client/components/fileManager/filemanager.js';
import { ModuleClassifiedsViewAd, DeletePost, ModuleClassifiedsList, ForumLang, CommonTranslation, Email, ResourceForm } from "/common/lang/lang.js";
import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import moment from 'moment'

import './views/module_classifieds_index.less';
import './views/module_classifieds_view_ad.view.html';

var translation;
Template.module_classifieds_view_ad.onRendered(function() {
});

Template.module_classifieds_view_ad.onCreated(function(props) {
	this.fm = new FileManager(UserFiles, {
		fromAds: true,
		classifiedsId: Template.currentData().classifiedsId
	});
	this.ad = new ReactiveVar();
  
	this.editingPost = new ReactiveVar();
	this.oldad = new ReactiveVar();
	this.replySuccess = new ReactiveVar(false);
	this.classifiedFeedEditing = new ReactiveVar(null)
	translation = new ModuleClassifiedsViewAd((FlowRouter.getParam('lang') || 'fr'));
	const data = Template.currentData();
	if (!data.classifiedsAdsId)
		data.classifiedsAdsId = FlowRouter.current().queryParams.ad;

	this.subscribe('classifiedsFiles', Template.currentData().classifiedsId)
	if (data.classifiedsAdsId) {
		this.subscribe('ClassifiedsAds', { _id: data.classifiedsAdsId });
	}

  this.isDisabled = new ReactiveVar(false)
  this.isDisabled.set(false)

	this.files = new ReactiveVar([])

	this.editMode = new ReactiveVar(false)
  this.editedAd = new ReactiveDict()

  this.editedAd.setDefault({
    title: null,
    price: null,
    description: null
  })

  // this.oldad.setDefault({
  //   title: null,
  //   price: null,
  //   description: null
  // })

	let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id');

	let condoIds = []

  if (Meteor.isManager) {
    let enterprise = Enterprises.findOne({ 'users.userId': Meteor.userId() })
    if (!!enterprise) {
      if (user = enterprise.users.find((u) => { return u.userId === Meteor.userId() })) {
        condoIds = _.pluck(user.condosInCharge, 'condoId')
      }
    }
  } else {
    condoIds = [ FlowRouter.getParam('condo_id') ]
  }

  this.subscribe('classifiedsFeed', condoId, data.classifiedsAdsId);
  this.subscribe('classifiedsFeedAnswerCounter', condoIds)

  this.ad.set(data.classifiedsAdsId);
  this.oldad.set(ClassifiedsAds.findOne(data.classifiedsAdsId))
	Meteor.call('module-classifieds-view-ad', Template.currentData().classifiedsAdsId);
});

Template.module_classifieds_view_ad.events({
	'hidden.bs.modal #modal_edit_classifiedAnswer': function(event, template) {
		template.classifiedFeedEditing.set(null)
		$('.ql-editor').empty();
		$("#attachmentsEditAnswer").html("")
		$('#sendAnswerEditClassified').button('reset');
		emptyFormByParentId('modal_edit_classifiedAnswer');
	},
	'click .backToClassifieds': function(event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		const params = {
			lang,
			condo_id: _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id'),
			isBack: 1,
			ad_id: null
		};

		FlowRouter.go(_.has(Template.currentData(), 'dataGestionnaire') ? 'app.gestionnaire.classifieds.ad' : 'app.board.resident.condo.module.classifieds.ad', params);
	},
	'click #newPost': function(event, template) {
		template.editingPost.set(null);
	},
	'click a#ad-edit': function(event, template) {
		const id = event.currentTarget.getAttribute("data-id");
		template.editingPost.set(id);
	},
	'click #delete-ad': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		let translation_ = new DeletePost((FlowRouter.getParam("lang") || "fr"));
		const id = event.currentTarget.getAttribute('data-ad-id');
		const thisPost = ClassifiedsAds.findOne({_id: id});
		const templ = Template.currentData();
		if (Meteor.isManager || (thisPost && thisPost.userId == Meteor.userId()) || (Meteor.userHasRight("ads", "delete"))) {
			bootbox.confirm({
				title: translation_.deletePost["title_delete_post"],
				message: thisPost.userId == Meteor.userId() ? translation_.deletePost["message_delete_post"] : translation_.deletePost["message_delete_post_other"],
				buttons: {
					confirm: {label: translation_.deletePost["yes"], className: "btn-red-confirm"},
					cancel: {label: translation_.deletePost["no"], className: "btn-outline-red-confirm"}
				},
				callback: function(res) {
					if (res) {
						Meteor.call('module-classifieds-ad-delete', id, null);
						sAlert.success(translation_.deletePost["succes_delete_post"]);
					}
				}
			});
		}
	},
	'click #goToCgu': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Session.set('backFromCgu', FlowRouter.current().path);
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},
	'click .directMessage': function(event, template) {
    Session.set("isReplyAds", Template.instance().ad.get());
    if (!!_.has(Template.currentData(), 'dataGestionnaire') === true) {
      const lang = FlowRouter.getParam("lang") || "fr"
      const condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
      const defaultRole = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })

      if (!!defaultRole === true) {
        Meteor.call('checkManagerRoomExist', [event.currentTarget.getAttribute("data-user-id")], FlowRouter.getParam('condo_id'), function (error, result) {
          if (!!result === true) {
            const params = {
              lang,
              tab: 'global',
              msgId: result
            }
            FlowRouter.go('app.gestionnaire.messenger', params)
          } else {
            const params = {
              lang,
              tab: 'global'
            }
            FlowRouter.go('app.gestionnaire.messenger.new', params)
          }
        })
      }
    } else {
      const lang = FlowRouter.getParam("lang") || "fr"
      const condoId = FlowRouter.getParam("condo_id")
      const defaultRole = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })

      if (!!defaultRole === true) {
        Meteor.call('checkResidentRoomExist', [event.currentTarget.getAttribute("data-user-id")], FlowRouter.getParam('condo_id'), function (error, result) {
          if (!!result === true) {
            const params = {
              lang,
              condo_id: FlowRouter.getParam('condo_id'),
              module_slug: 'messenger',
              wildcard: `${defaultRole._id}/${result}`
            }
            FlowRouter.go('app.board.resident.condo.module', params)
          } else {
            const params = {
              lang,
              condo_id: FlowRouter.getParam('condo_id'),
              module_slug: 'messenger',
              wildcard: `${defaultRole._id}/new`
            }
            FlowRouter.go('app.board.resident.condo.module', params)
          }
        })
      } else {
        console.log('Default Role `Occupant` not found.')
      }
    }
	},
	'click .seeMore': function(event, template) {
		translation = new ModuleClassifiedsViewAd(FlowRouter.getParam('lang') || 'fr')
		const id = event.currentTarget.getAttribute('post-id');
		let description = ClassifiedsAds.findOne(id);
		if (description) {
			$(event.currentTarget.parentElement.parentElement)[0].innerHTML = description.description + `<a class='seeLess' post-id=${id} style='font-size: 12px'> ` + translation.moduleClassifiedsViewAd["see_less"] + `</a>`;
		}
	},
	'click .seeLess': function(event, template) {
		translation = new ModuleClassifiedsViewAd(FlowRouter.getParam('lang') || 'fr')
		const id = event.currentTarget.getAttribute('post-id');
		var cutMessage = "";
		var count = 0;
		var isBalise = false;
		let description = ClassifiedsAds.findOne(id);
		if (description) {
			for (let i = 0; i != description.description.length; i++) {
				if (description.description[i] == '<')
					isBalise = true;
				else if (description.description[i] == '>')
					isBalise = false;
				else {
					if (!isBalise)
						count += 1;
					if (count >= 250) {
						cutMessage = description.description.substr(0, i);
						break;
					}
				}
			}
			$(event.currentTarget.parentElement)[0].innerHTML = cutMessage + ` <span style="font-style: normal"> ... </span><a class='seeMore' post-id=${id} style='font-size: 12px'> ` + translation.moduleClassifiedsViewAd["see_more"] + `</a>`;
		}
	},
	'click .seeMoreOrLess': function(event, template) {
		let contentObj = $(event.currentTarget.parentElement).find('.info-content');
		contentObj.find('.ellipsisedText').toggleClass('show');
		contentObj.find('.notEllipsisedText').toggleClass('show');
		contentObj.toggleClass('ellipsis');
		$(event.currentTarget).text(contentObj.hasClass('ellipsis') ? "Voir plus" : "Voir moins");
	},
	'click #classifiedAnswer-edit': function(event, template) {
		let feedId = $(event.currentTarget).attr('idx');
		template.classifiedFeedEditing.set(feedId);
	},
	'click #classifiedAnswer-delete': function(event, template) {
		translation = new ModuleClassifiedsViewAd(FlowRouter.getParam('lang') || 'fr')
		let feedId = $(event.currentTarget).attr('idx');
		let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
		const tl_deletePost = new ForumLang(FlowRouter.getParam("lang") || "fr");

		bootbox.confirm({
			title: "Confirmation",
			message: translation.moduleClassifiedsViewAd["delete_answer"],
			buttons: {
				confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
				cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
			},
			callback: function(res) {
				if (res) {
					Meteor.call('deleteClassifiedAnswer', condoId, feedId, function(error, result) {
						if (!error) {
							sAlert.success(translation.moduleClassifiedsViewAd["success_delete_answer"])
						}
					})
				}
			}
		});
	},
	'click #modify-ad': (e, t) => {
		Template.instance().editMode.set(true)

    const ads = ClassifiedsAds.findOne(Template.instance().ad.get());

    Template.instance().editedAd.set({
			...ads
		})
    Template.instance().oldad.set({
			...ads
		})
	},
	'click #cancel-edit': (e, t) => {
    Template.instance().editMode.set(false)
    Template.instance().editedAd.set({})
	},
	'click #update-ad': (e, t) => {
		const __files = t.files.get()

		$('#update-ad').button('loading');
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)

		t.fm.batchUpload(__files)
      .then(files => {
        const data = {
          title: t.editedAd.get('title'),
          price: t.editedAd.get('price'),
          description: t.editedAd.get('description') ? t.editedAd.get('description').replace(/<(.|\n)*?>/g, '') : '',
          classifiedsId: t.editedAd.get('classifiedsId'),
          userId: t.editedAd.get('userId'),
          history: t.editedAd.get('history'),
          updatedAt: Date.now(),
          files: t.editedAd.get('files').concat(_.map(files, f => f._id))
        }

        Meteor.call('module-classifieds-ad-edit', t.ad.get(), data, function (error, result) {
          $('#update-ad').button('reset');
          if (error) {
            sAlert.error(error)
          } else {
            t.editMode.set(false)
            t.files.set([])
          }
        })
      })
      .catch(err => {
        console.log(err);
        sAlert.error(translate.commonTranslation[err.reason]);
        $('#update-ad').button('reset');
      })

		// if (__files) {
    //
		// 	for (let i = 0; i < __files.length; i++) {
		// 		let fileName = __files[i].name || 'undefined'
		// 		filesError = false
		// 		let id = t.fm.insert(__files[i], (err, fileObj) => {
		// 			if (err) {
		// 				filesError = true
		// 				let errorText = translation.moduleClassifiedsList['errorImage1'] + fileName + translation.moduleClassifiedsList['errorImage2'] + translation.moduleClassifiedsList[err.reason]
		// 				return sAlert.error(errorText);
		// 			}
		// 		});
    //
		// 		if (!filesError) {
		// 			let infos = t.fm.getFileList()[id];
		// 			additionalFiles.push(infos.upload.config.fileId)
		// 		} else {
		// 			break
		// 		}
		// 	}
		// }

		// if (!filesError) {
		// 	const data = {
		// 		title: t.editedAd.get('title'),
		// 		price: t.editedAd.get('price'),
		// 		description: t.editedAd.get('description') ? t.editedAd.get('description').replace(/<(.|\n)*?>/g, '') : '',
		// 		classifiedsId: t.editedAd.get('classifiedsId'),
		// 		userId: t.editedAd.get('userId'),
		// 		history: t.editedAd.get('history'),
		// 		updatedAt: Date.now(),
		// 		files: t.editedAd.get('files').concat(additionalFiles)
		// 	}
    //
		// 	Meteor.call('module-classifieds-ad-edit', Template.instance().ad.get(), data, function (error, result) {
		// 		$('#update-ad').button('reset');
		// 		if (error) {
		// 			sAlert.error(error)
		// 		} else {
		// 			t.editMode.set(false)
		// 			t.files.set([])
		// 		}
		// 	})
    //
		// } else {
		// 	$('#update-ad').button('reset');
		// }
	}
});

const validateFields = (t) => {
  const requireFields = ['title', 'description']
  const error = _.filter(requireFields, field => !t.editedAd.get(field))

  const oldad = t.oldad.get()
  console.log('old', oldad)
  console.log('edited', t.editedAd.get('files'));
  // const editedAd = t.editedAd.get()
  if (oldad.description === t.editedAd.get('description') &&
    (oldad.title === t.editedAd.get('title')) &&
    (oldad.files.length === t.editedAd.get('files').length) &&
    (t.files.get().length === 0 ) &&
    (oldad.price === t.editedAd.get('price'))
  ) {
    error.push('title')
    error.push('description')
  }
  /**
   * Price is not mandatory so if price is set we need to make sure
   * it only have string of numbers and 10 char limit
   */
  if (t.editedAd.get('price').length === 0 || isNaN(t.editedAd.get('price')) || t.editedAd.get('price').length >= 10) {
    error.push('price')
  }
  t.isDisabled.set(error.length !== 0)
}

Template.module_classifieds_view_ad.helpers({
	nbViews: (viewsCount) => {
		return viewsCount || 0;
	},
	onFileAdded: () => {
		const t = Template.instance()

		return files => {
			t.files.set([
				...t.files.get(),
				...files
			])
		}
	},
	onFileRemoved: () => {
		const t = Template.instance()
		return (fileId) => {
			t.editedAd.set('files', _.filter(t.editedAd.get('files'), f => f.fileId ? f.fileId !== fileId : f !== fileId))
			t.files.set(_.filter(t.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
		}
	},
  getPreviewFiles: () => {
    let fileIds = Template.instance().editedAd.get('files')
		const reactiveFiles = Template.instance().files.get()

		const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
      	return {
					...file,
					id: file._id,
					preview: {
						url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
					}
				}
			})

			return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

  	return []
	},
	onTextEntered: (key) => {
		const tpl = Template.instance()
		return () => value => {
      console.log(key, value)
      tpl.editedAd.set(key, value)
      validateFields(tpl)
    }
	},
  disableSubmit: () => {
    const tpl = Template.instance()
    validateFields(tpl)
    return Template.instance().isDisabled.get()
	},
	getFormValue: key => {
		return Template.instance().editedAd.get(key)
	},
	ad: () => {
		const classifiedsAds = ClassifiedsAds.findOne(Template.instance().ad.get());
		if (classifiedsAds)
			return classifiedsAds;
		else
			$(".backToClassifieds").click();
	},
	isNotMyAd: () => {
		const classifiedsAds = ClassifiedsAds.findOne(Template.instance().ad.get());
		if (classifiedsAds) {
			return classifiedsAds.userId != Meteor.userId();
		} else {
			return false;
		}
	},
	isEditMode: () => {
		return Template.instance().editMode.get()
	},
	editingPost: () => {
		return Template.instance().editingPost.get();
	},
	classifiedsId: () => {
		return Template.instance().data.classifiedsId;
	},
	classifiedFeedId: () => {
		return Template.instance().classifiedFeedEditing.get();
	},
	condoId: () => {
		let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
		return condoId
	},
	instance: () => {
		return Template.instance();
	},
	profilePicture: (profile) => {
		if (profile) {
			let user = _.has(Template.currentData(), 'dataGestionnaire') ? GestionnaireUsers.findOne(profile.userId) : ResidentUsers.findOne(profile.userId);
			if (!user) {
				user = _.has(Template.currentData(), 'dataGestionnaire') ? Meteor.users.findOne(profile.userId) : Enterprises.findOne({'users.userId': profile.userId});
				if (!(_.has(user, 'profile')))
					user = _.find(user.users, function(elem) {
						return elem.userId == profile.userId;
					});
			}
			else
				return user.profile.firstname[0].toUpperCase() + user.profile.lastname[0].toUpperCase();
			if (user)
				return _.has(user, 'profile') ? user.profile.firstname[0].toUpperCase() + user.profile.lastname[0].toUpperCase() : user.firstname[0].toUpperCase() + user.lastname[0].toUpperCase();
		}
	},
	getName: (userId, name) => {
		let user = UsersProfile.findOne(userId);
		if (user) {
			return user[name]
		}
	},
	postedDate: (postedDate) => {
		return new Date(postedDate).toString().split(' ')[2];
	},
	postedDayDate: (postedDate) => {
		let listDay = ["JAN", "FEV", "MAR", "AVR", "MAI", "JUI", "JUIL", "AOU", "SEP", "OCT", "NOV", "DEC"];
		let date = new Date(postedDate);
		return listDay[date.getMonth()];
	},
	compDate: (endDate, endHour) => {
		if (!endDate)
			return true;
		date = new Date(endDate);
		hours = endHour.split(":");
		date.setHours(hours[0], hours[1]);
		return new Date().valueOf() <= date.valueOf()
	},
	getFiles: () => {
		const ad = ClassifiedsAds.findOne(Template.instance().ad.get());
		if (ad) {
      const files = UserFiles.find({ _id: { $in: ad.files }});

      return files;
		}
	},
	getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
	isGestionnaireView: () => {
		return _.has(Template.currentData(), 'dataGestionnaire');
	},
	getGestionnaireAddress: () => {
		const condo = Condos.findOne(Template.currentData().dataGestionnaire.condoId);
		return condo.name;
	},
	getAccess: (whichAccess) => {
		if (_.has(Template.currentData(), 'dataGestionnaire')) {
			return _.find(Template.currentData().dataGestionnaire.access, function(elem) {
				return elem === whichAccess;
			}) !== undefined;
		}
		return true;
	},
	hasMessenger: () => {
		const ad = ClassifiedsAds.findOne(Template.instance().ad.get());

		if (ad) {
			const userId = ad.userId

      if (userId) {
        if (Enterprises.find({"users.userId": userId}).fetch().length == 1)
          return false;
        if (userId === Meteor.userId())
          return false;
        let condoId = FlowRouter.getParam("condo_id");
        let resident = ResidentUsers.findOne(userId);
        if (resident) {
          let condo = _.find(resident.resident.condos, (c) => {
            return c.condoId === condoId
					});
					let Condo = Condos.findOne(condoId);
					if (Condo) {
						console.log(Condo.settings.options.messengerResident);
					}
					return (condo && condo.preferences && condo.preferences.messagerie === true && Condo && Condo.settings && Condo.settings.options && Condo.settings.options.messengerResident === true);
        }
      }
		}
		return true;
	},
	isOwnAd: (ad) => {
		return ad && (ad.userId === Meteor.userId())
	},
	canModify: (ad) => {
		return ad && ad.userId === Meteor.userId() && Meteor.userHasRight("ads", "write")
	},
  canDelete: (ad) => {
		return ad && ((ad.userId === Meteor.userId()) || Meteor.userHasRight("ads", "delete"))
	},
	isManager: () => {
		return Meteor.isManager
	},
	getFileManager: () => {
		return Template.instance().fm;
	},
	answerMessageCb: function() {
		const tpl = Template.instance()
		let classifiedId = Template.instance().ad.get();
		const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new ModuleClassifiedsList(lang)
    const translate = new CommonTranslation(lang)
		let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
		// let template = Template.instance();
		return (message, files, answerTemplate) => {
			if (message !== '' || files.length !== 0) {
			  tpl.fm.batchUpload(files)
          .then(results => {
            Meteor.call("replyClassified", condoId, classifiedId, message, _.map(results, f => f._id), function (error, result) {
              $('.mb-comment-save').button('reset')
              if (error) {
                sAlert.error(error)
              } else {
                setTimeout(function() {
                  answerTemplate.files.set([])
                  answerTemplate.message.set('')
                  answerTemplate.isEmpty.set(true)
                }, 0);
              }
            })
          })
          .catch(err => {
            sAlert.error(translate.commonTranslation[err.reason])
            $('.mb-comment-save').button('reset')
          })
			} else {
        $('.mb-comment-save').button('reset')
      }
		}
	},
	isSubscribeReady: () => {
		return Template.instance().subscriptionsReady();
	},
	getFeed: () => {
    return ClassifiedsFeed.find({}, {sort: {date: -1}}).fetch();
	},
	isInactive: (userId) => {
		let user = _.has(Template.currentData(), 'dataGestionnaire') ? GestionnaireUsers.findOne(userId) : ResidentUsers.findOne(userId);
		if (!user)
			user = _.has(Template.currentData(), 'dataGestionnaire') ? Meteor.users.findOne(userId) : Enterprises.findOne({'users.userId': userId});
		if (!user)
			return "Compte désactivé";
	},
	getFilesReply: (filesId) => {
		return UserFiles.find({ _id: { $in: filesId }});
	},
  parseAdCreated: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  stripHtmlTag: (text) => {
    return text && text.replace(/<(.|\n)*?>/g, '');
  },
  getCounter: (adId) => {
    const counter = ClassifiedsFeedCounter.findOne({_id: adId});

    return (!counter || typeof counter === 'undefined') ? 0 : counter.counter
  },
  MbTextArea: () => MbTextArea,
  MbFilePreview: () => MbFilePreview,
	classifiedsId: () => Template.currentData().classifiedsId
});
