/* global _ */
/* global Meteor */
/* global FlowRouter */

import { Template } from 'meteor/templating'
import './views/classifieds_feed.view.html'

Template.module_classifieds_feed.onCreated(function () {
})

Template.module_classifieds_feed.events({
  'click .go-to-classified' (event) {
    const params = {
      lang: (FlowRouter.getParam('lang') || 'fr'),
      condo_id: FlowRouter.getParam('condo_id'),
      ad_id: event.currentTarget.getAttribute('idx')
    }
    FlowRouter.go('app.board.resident.condo.module.classifieds.ad', params)
  }
})

Template.module_classifieds_feed.helpers({
  ad: () => {
    return Template.currentData().ad
  },
  isNewAd: (ad) => {
    let newState = true
    let exist = ad.views[Meteor.userId()]
    if (exist !== undefined) {
      newState = false
    }

    return newState
  }
})
