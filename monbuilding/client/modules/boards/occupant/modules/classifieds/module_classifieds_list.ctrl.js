import { Template } from 'meteor/templating';
import { Pagination } from "/client/components/pagination/pagination.js"
import { DeletePost, WhoCanSeeHeader, ModuleClassifiedsList, ModuleClassifiedsViewAd, Email } from "/common/lang/lang.js";
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import moment from 'moment'

import './views/module_classifieds_index.less';
import './views/module_classifieds_list.view.html';

const WIDGET_POST_LIMIT = 3

Template.module_classifieds_list.onCreated(function(props) {
	this.isNewAds = new ReactiveDict()
	this.ads = new ReactiveVar([]);
	this.classifieds = new ReactiveVar();
	this.editingAd = new ReactiveVar();
	this.pagination = new Pagination(ClassifiedsAds, 10);
	this.pagination.setQuery({classifiedsId: Template.currentData().classifiedsId});

	this.widgetLimit = new ReactiveDict({
		activeUser: WIDGET_POST_LIMIT,
		followingAds: WIDGET_POST_LIMIT,
		myAds: WIDGET_POST_LIMIT
	})

	let condoIds = null

	if (Meteor.isManager) {
	let enterprise = Enterprises.findOne({ "users.userId": Meteor.userId() })
	if (!!enterprise) {
		if (user = enterprise.users.find((u) => { return u.userId === Meteor.userId() })) {
		condoIds = _.pluck(user.condosInCharge, 'condoId')
		}
	}
	} else {
		condoIds = [ FlowRouter.getParam('condo_id') ]
	}

	this.subscribe('classifiedsFeedAnswerCounter', condoIds)
});

Template.module_classifieds_list.events({
	'show.bs.modal #modal_first_button': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#edit_forum";
		window.onhashchange = function() {
			if (location.hash != "#edit_forum"){
				modal.modal('hide');
			}
		}
	},
	'hidden.bs.modal #modal_first_button': function(event, template) {
		history.replaceState('', document.title, window.location.pathname);
	},
	'show.bs.modal #modal_second_button': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#edit_pool";
		window.onhashchange = function() {
			if (location.hash != "#edit_pool"){
				modal.modal('hide');
			}
		}
	},
	'hidden.bs.modal #modal_second_button': function(event, template) {
		history.replaceState('', document.title, window.location.pathname);
	},
	'click a#ad-delete': function(event, template) {
		let translation = new DeletePost((FlowRouter.getParam("lang") || "fr"));
		const id = event.currentTarget.getAttribute('data-id');
		const thisPost = ClassifiedsAds.findOne({_id: id});
		if (event.currentTarget.getAttribute('manager') || (thisPost && thisPost.userId == Meteor.userId()) || (Meteor.userHasRight("ads", "delete"))) {
			bootbox.confirm({
				title: translation.deletePost["title_delete_post"],
				message: thisPost.userId == Meteor.userId() ? translation.deletePost["message_delete_post"] : translation.deletePost["message_delete_post_other"],
				buttons: {
					confirm: {label: translation.deletePost["yes"], className: "btn-red-confirm"},
					cancel: {label: translation.deletePost["no"], className: "btn-outline-red-confirm"}
				},
				callback: function(res) {
					if (res) {
						Meteor.call('module-classifieds-ad-delete', id);
						sAlert.success(translation.deletePost["succes_delete_post"]);
					}
				}
			});
		}
	},
	'click #goToCgu': function(event, template) {
		event.preventDefault();
		event.stopPropagation();
		Session.set('backFromCgu', FlowRouter.current().path);
		let lang = FlowRouter.getParam('lang') || 'fr'
		const win = window.open("/" + lang + "/services/cgu/", '_blank');
		win.focus();
	},
	'click a#ad-edit': function(event, template) {
		const id = event.currentTarget.getAttribute("data-id");
		template.editingAd.set(id);
	},
	'click #follow-classifieds-toggle': function(event, template) {
		Meteor.call('module-classifieds-follow-toggle', _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam("condo_id"));
	},
	'click #newPost': function(event, template) {
		template.editingAd.set(null);
	},
	'click #goMessage': function(event, template) {
		let ads = ClassifiedsAds.findOne(event.currentTarget.getAttribute('idx'));
		if (ads) {
			Session.set("messageId", ads.userId);
			Session.set("messagerie", "Resident");
			const params = {
				lang: (FlowRouter.getParam("lang") || "fr"),
				condo_id: FlowRouter.getParam('condo_id'),
				module_slug: "messagerie"
			};
			FlowRouter.go('app.board.resident.condo.module', params);
			setTimeout(function() {
				recalculateHeigh();
			}, 100);

		}
	},
	'click button[data-close=help-module]': function(event, template) {
		Meteor.call('desactivateHelper', 'ad', function(err, res) {
			$('.help-module').fadeOut(200);
		});
	},
	'click .ad-action-unfollow': (e, t) => {
    // Meteor.call('unfollowPost', {
    	// condoId: _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam("condo_id"),
		// 	postId: e.currentTarget.getAttribute('data-ad-id')
		// });
	},
	'click .mb-goToAd': (e, t) => {
    const lang = (FlowRouter.getParam("lang") || "fr")
    const params = {
      lang,
      condo_id: _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id'),
      ad_id: e.currentTarget.getAttribute('data-ad-id')
    }

    FlowRouter.go(_.has(Template.currentData(), 'dataGestionnaire') ? 'app.gestionnaire.classifieds.ad' : 'app.board.resident.condo.module.classifieds.ad', params);
  },
	'click .action-toggle-see-more': (e, t) => {
		const widget = e.currentTarget.getAttribute('data-widget')

		t.widgetLimit.set(widget, t.widgetLimit.get(widget) === 0 ? 3 : 0)
	},
	'click .ad-navigate-view': (e) => {
		const lang = (FlowRouter.getParam("lang") || "fr")
		const params = {
			lang,
			condo_id: _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id'),
			ad_id: e.currentTarget.getAttribute('data-ad-id')
		}

		FlowRouter.go(_.has(Template.currentData(), 'dataGestionnaire') ? 'app.gestionnaire.classifieds.ad' : 'app.board.resident.condo.module.classifieds.ad', params);
	},
	'click .ad-navigate-edit': (e) => {
		const lang = (FlowRouter.getParam("lang") || "fr")
		const params = {
		lang,
		condo_id: _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id'),
		ad_id: e.currentTarget.getAttribute('data-ad-id')
		}

		FlowRouter.go(_.has(Template.currentData(), 'dataGestionnaire') ? 'app.gestionnaire.classifieds.ad' : 'app.board.resident.condo.module.classifieds.ad', params);
	}
});

Template.module_classifieds_list.helpers({
  shouldShowMore: (length, key) => {
  	return length >= WIDGET_POST_LIMIT
	},
	isLimited: (key) => {
		return Template.instance().widgetLimit.get(key) !== -0
	},
	getFormData: (key) => {
		return Template.instance().form.get(key) ? Template.instance().form.get(key) : '';
	},
	ads: () => {
		return ClassifiedsAds.find({ condoId: FlowRouter.getParam('condo_id') }, { sort: { createdAt: -1 } }).fetch()
	},
	followingAds: () => {
    const ads = ClassifiedsAds.find({
      userId: Meteor.userId(),
      classifiedsId: Template.currentData().classifiedsId
    }, {limit: Template.instance().widgetLimit.get('followingAds')}).fetch()

    return ads.length !== 0 ? ads : false
	},
	myAds: () => {
    const ads = ClassifiedsAds.find({
			userId: Meteor.userId(),
			classifiedsId: Template.currentData().classifiedsId
    }, {
    	sort: {createdAt: -1},
    	limit: Template.instance().widgetLimit.get('myAds')
    }).fetch()

		return ads.length !== 0 ? ads : false
	},
	getUniqUserFromAds: () => {
		const ads = ClassifiedsAds.find({classifiedsId: Template.currentData().classifiedsId}).fetch()
			const uniqUserIds = _.uniq(_.map(ads, a => a.userId))

		const uniqUsers = UsersProfile.find({'_id': {
		'$in': uniqUserIds
		}}, {limit: Template.instance().widgetLimit.get('activeUser')}).fetch()

		return uniqUsers
	},
	emptyAds: () => {
		const ad = ClassifiedsAds.find({classifiedsId: Template.currentData().classifiedsId}).fetch();
		return ad.length === 0;
	},
	classifieds: () => {
		const classifieds = Classifieds.findOne(Template.currentData().classifiedsId);
		if (classifieds)
			return classifieds;
	},
	classifiedsId: () => {
		return Template.currentData().classifiedsId;
	},
	profilePicture: (profile) => {
		let user = _.has(Template.currentData(), 'dataGestionnaire') ? GestionnaireUsers.findOne(profile.userId) : ResidentUsers.findOne(profile.userId);
		if (!user) {
			user = _.has(Template.currentData(), 'dataGestionnaire') ? Meteor.users.findOne(profile.userId) : Enterprises.findOne({'users.userId': profile.userId});
			if (!(_.has(user, 'profile')))
				user = _.find(user.users, function(elem) {
					return elem.userId == profile.userId;
				});
		}
		else
			return user.profile.firstname[0].toUpperCase() + user.profile.lastname[0].toUpperCase();
		if (user)
			return _.has(user, 'profile') ? user.profile.firstname[0].toUpperCase() + user.profile.lastname[0].toUpperCase() : user.firstname[0].toUpperCase() + user.lastname[0].toUpperCase();
	},
	getName: (userId, name) => {
		const lang = FlowRouter.getParam('lang') || 'fr'
		let translation = new ModuleClassifiedsList((FlowRouter.getParam("lang") || "fr"));

		let user = UsersProfile.findOne({_id: userId})
		if (!!user) {
			return user[name]
		} else if (name === 'firstname') {
			return translation.moduleClassifiedsList['deactivate_account']
		}
	},
	postedDate: (postedDate) => {
		return new Date(postedDate).toString().split(' ')[2];
	},
	postedDayDate: (postedDate) => {
		let listDay = ["JAN", "FEV", "MAR", "AVR", "MAI", "JUI", "JUIL", "AOU", "SEP", "OCT", "NOV", "DEC"];
		let date = new Date(postedDate);

		return listDay[date.getMonth()];
	},
	compDate: (endDate, endHour) => {
		let date = new Date(endDate);
		let hours = endHour.split(":");
		date.setHours(hours[0], hours[1]);
		return new Date().valueOf() <= date.valueOf()
	},
	editingAd: () => {
		return Template.instance().editingAd.get();
	},
	nbViews: (views) => {
		return views;
	},
	getPosts: () => {
		return Template.instance().pagination;
	},
	isNewPost: (postId) => {
		let classifieds = ClassifiedsAds.findOne(postId)
		let view = classifieds.views[Meteor.userId()];
		if (view !== undefined)
			return (false);
		return (true);
	},
	isGestionnaireView: () => {
		return _.has(Template.currentData(), 'dataGestionnaire');
	},
	getGestionnaireAddress: () => {
		let condo = Condos.findOne(Template.currentData().dataGestionnaire.condoId);
		if ((condo && condo.info && condo.info.address && condo.name && condo.info.address == condo.name) || !condo.name)
			return condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city;
		else
			return condo.name;
	},
	getAccess: (whichAccess) => {
		if (_.has(Template.currentData(), 'dataGestionnaire')) {
			return _.find(Template.currentData().dataGestionnaire.access, function(elem) {
				return elem == whichAccess;
			}) !== undefined;
		}
		return true;
	},
	displayCorrectHTML: (right, isGestionnaire) => {
		if (right) {
			return right;
		}
		return isGestionnaire;
	},
	notificationsClassifieds : () => {
		let resident;
		let condo;
		let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam("condo_id");
		if (resident = Residents.findOne({userId: Meteor.userId()})) {
			if (condo = resident.condos.find((elem) => {return elem.condoId === condoId})) {
				return condo.notifications.classifieds;
			}
		}
		else {
			if (gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()})) {
				if (user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()})) {
					if (condo = user.condosInCharge.find((c) => {return c.condoId == condoId})) {
						return condo.notifications.classifieds;
					}
				}
			}
		}
	},
	isOwnAd: (ad) => {
		return ad.userId == Meteor.userId();
	},
	getCounter: (adId) => {
		let counter = ClassifiedsFeedCounter.findOne({_id: adId});
		if (counter == undefined || !counter || counter.length == 0)
			return 0
		else
			return counter.counter
	},
	parseAdCreated: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
	},
	stripHtmlTag: (text) => {
		return text && text.replace(/<(.|\n)*?>/g, '');
	},
	empty: (arr) => arr.length === 0,
	files: fileIds => UserFiles.find({_id: {$in: fileIds ? fileIds.filter(file => typeof file === 'string') : [] }}),
	newFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
	getMbTimeStamp: () => {
		if (Meteor.user().createdAt) {
			// return moment(Meteor.user().createdAt).locale(FlowRouter.getParam('lang') || 'fr').format('lll')
			return moment(Meteor.user().createdAt).locale(FlowRouter.getParam('lang') || 'fr').format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
		} else {
			// return moment().locale(FlowRouter.getParam('lang') || 'fr').format('lll')
			return moment().locale(FlowRouter.getParam('lang') || 'fr').format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
		}
	},
	isParentReady: () => {
		return Template.currentData().isSubscribeReady
	},
  	showEditedTag: (ad) => {
		let translation = new ModuleClassifiedsViewAd((FlowRouter.getParam("lang") || "fr"));

		return !ad.editedAt ? '' : ' ' + '(' + translation.moduleClassifiedsViewAd["edited"].toLowerCase() + ')'
	},
	isNewAd: (ad) => {
		let condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
		Template.instance().isNewAds.set(ad._id, false)
		let template = Template.instance()
		Meteor.call('isNewAds', ad._id, condoId, (error, result) => {
			if (!error) {
				template.isNewAds.set(ad._id, result)
			}
		});
		return true
	},
	isNewAdVar: (adId) => {
		return Template.instance().isNewAds.get(adId)
	},
	MbFilePreview: () => MbFilePreview,
	goToDetail: (ad) => {
		const lang = (FlowRouter.getParam("lang") || "fr")
		const params = {
		lang,
		condo_id: Meteor.isManager ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id'),
		ad_id: ad._id
		}

		const target =  Meteor.isManager ? 'app.gestionnaire.classifieds.ad' : 'app.board.resident.condo.module.classifieds.ad'
		const run = () => {
      FlowRouter.go(target, params)
    }

		return () => run
	},
	AdsUserHasRight: () => {
		let condo_id = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')
		return Meteor.userHasRight('ads', 'see', condo_id, Meteor.userId())
	}
});
