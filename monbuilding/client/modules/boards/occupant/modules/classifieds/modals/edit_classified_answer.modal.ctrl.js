import { FileManager } from '/client/components/fileManager/filemanager.js';
import { MessageInput } from "/common/lang/lang";


var index = 0;
var timer = {};

function constructAttachmentElement(data, isAlreadyPresent)  {
	var progress = $('<span></span>')
	.addClass('progress')
	.css('width', '0%');

	var meter = $('<div></div>')
	.addClass('meter')
	.append(progress);

	var iconFile = $('<span></span>')
	.addClass('icon-file')
	.append($('<img class="icon icons8-Open-Folder" width="16" height="16" src="/img/icons/uploadFile.png" style="margin-top: -8px">'));

	var checkmark = $('<span class="checkmark"></span>')
	.append($('<img width="16" height="16" src="/img/icons/validUploadFile.png" style="margin-top: -8px">'));

	var fileSize = $('<span class="file-size"></span>')
	.html(data.size)
	.append(checkmark);

	var fileName = $('<span class="file-name" style="margin: 0 4px 0 3px"></span>').text(data.name);

	var fileRemove = $(`<span class="remove-item remove-item-answer" index="${isAlreadyPresent == true ? data.id : index}" originalFile="${isAlreadyPresent}" fileId="${data.id}">`)
	.append($('<img class="icon icons8-Delete" width="16" height="16" src="/img/icons/deleteFile.png" style="margin-top: -8px">'));
	if (isAlreadyPresent == true)
		index += 1;

	var file = $('<div></div>')
	.addClass('file')
	.append(iconFile)
	.append(fileName)
	.append(fileSize)
	.append(fileRemove)
	.append(meter);

	var list = $('<li></li>')
	.attr('id', data.id)
	.addClass('attachment-item')
	.append(file);

	return list;
}

Template.edit_classified_answer_modal.onCreated(function() {
	this.fm = new FileManager(UserFiles);
	this.errorMessage = new ReactiveVar("");
	this.presentFiles = [];
});

Template.edit_classified_answer_modal.onRendered(function() {
});

Template.edit_classified_answer_modal.events({
	'show.bs.modal #modal_edit_classifiedAnswer': function(event, template) {
		let modal = $(event.currentTarget);
		window.location.hash = "#modal_edit_classifiedAnswer";
		window.onhashchange = function() {
			if (location.hash != "#modal_edit_classifiedAnswer"){
				modal.modal('hide');
			}
		}
	},

	'hidden.bs.modal #modal_edit_classifiedAnswer': function(event, template) {
		$('.ql-editor').empty();
		$("#attachmentsEditAnswer").html("")
		$('#sendAnswerEditClassified').button('reset');
		emptyFormByParentId('modal_edit_classifiedAnswer');
	},

	'click #attachment' : function(event, template) {
		var fileInput = $('#fileUploadInputClassifiedEditAnswer');
		fileInput.click();
	},
	'change #fileUploadInputClassifiedEditAnswer': function (event, template) {
		var attachmentListUL = $('#attachmentsEditAnswer');
		$('#sendAnswerEditClassified').attr('disabled', 'disabled');
		if (event.currentTarget.files) {
			for (let i = 0; i < event.currentTarget.files.length; i++) {
				var data = {};
				let error = false;
				let id = template.fm.insert(event.currentTarget.files[i], (err, fileObj) => {
					if (err) {
						error = true;
						return sAlert.error(err);
					}
				});

				data.name = event.currentTarget.files[i].name;
				data.size = (event.currentTarget.files[i].size / 1000).toFixed(2) + ' kb';
				data.id = "file-" + id;

				attachmentListUL.append(constructAttachmentElement(data));

				var progress = template.$('#file-' + id +' .progress');
				var elapsed = 0;

				timer[id] = setInterval(function() {
					if (error)
						return template.$("#file-" + id).remove();
					var wrapper = template.$('#file-' + id);
					if (!template || !template.fm || !template.fm.getFileList() || !template.fm.getFileList()[id]|| template.fm.getFileList()[id] == undefined || template.fm.getFileList()[id].done) {
						wrapper.find('.meter').remove();
						wrapper.addClass('completed');
						$('#sendAnswerEditClassified').removeAttr('disabled');
						clearInterval(timer[id]);
					}
					if (template && template.fm && template.fm.getFileList() && template.fm.getFileList()[id] && elapsed < template.fm.getFileList()[id].upload.progress.get()) {
						elapsed = template.fm.getFileList()[id].upload.progress.get();
						progress.animate({
							width: elapsed + '%'
						}, 200);
					}
				}, 100);

			}
		}
		else
			$('#sendAnswerEditClassified').removeAttr('disabled');
	},
	'click .remove-item-answer' : function (event, template) {
		if ($(event.currentTarget).attr('originalFile') == "true") {
			Template.instance().presentFiles.splice(Template.instance().presentFiles.indexOf($(event.currentTarget).attr('fileId')), 1);

			let index2 = $(event.currentTarget).attr('index')
			$('#' + index2).remove();
			$("#fileUploadInputClassifiedEditAnswer").val('');
		}
		else {
			for (let i = 0; i !== template.fm.getFileList().length; ++i) {
				if (template.$(event.currentTarget.parentElement)[0].innerText.match(template.fm.getFileList()[i].file.name)) {
					template.$(event.currentTarget.parentElement.parentElement).remove();
					template.fm.remove(i);
					$("#fileUploadInputClassifiedEditAnswer").val('');
					break;
				}
			}
		}
	},

	'click #sendAnswerEditClassified' : function(event, template) {
		$('#sendAnswerEditClassified').button('loading');
		$('#answer').height('70px');
		let translation = new MessageInput((FlowRouter.getParam("lang") || "fr"));
		event.preventDefault();
		let files = template.fm.getFileList();
		for (let i = 0; i < files.length; i++){
			if (files[i].done === false) {
				return (template.errorMessage.set(translation.messageInput["waiting"]));
			}
		}
		let text = document.getElementsByName("classifiedAdAnswer_edit")[0].value;
		text = text.trim();
		if (text.length < 1) {
			$('#sendAnswerEditClassified').button('reset');
			$('#chat-window').prop('style', 'margin-top: 0 !important;');
			return template.errorMessage.set(translation.messageInput["type_message"]);
		}

		let classifiedFeedId = template.data.classifiedFeedId;
		let condoId = template.data.condoId

		files = _.union(template.presentFiles, template.fm.getFileListIds());
		if (text != "") {
			Meteor.call("editClassifiedAnswer", condoId, classifiedFeedId, text, files, function(error, result) {
				$('#sendAnswerEditClassified').button('reset');
				if (error)
					sAlert.error(error)
				else {
					template.errorMessage.set('');
					index = 0;
					document.getElementsByName("classifiedAdAnswer_edit")[0].value = "";
					$('#attachmentsEditAnswer')[0].innerHTML = "";
					template.fm.clearFiles();
					$('#modal_edit_classifiedAnswer').hide();
					$('#modal_edit_classifiedAnswer').modal("hide");
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();

				}
			})
		}
	},


})

Template.edit_classified_answer_modal.helpers({
	isAnswerSelected: () => {
		let classifiedFeedId = Template.instance().data.classifiedFeedId;
		if (classifiedFeedId != null)
			return true;
		Template.instance().errorMessage.set("")
		return false;
	},
	getDetails: () => {
		let classifiedFeedId = Template.instance().data.classifiedFeedId;
		let classifiedAnswer = ClassifiedsFeed.findOne({_id: classifiedFeedId})
		if (classifiedAnswer)
			return ClassifiedsFeed.findOne({_id: classifiedFeedId})
		else
			return [];
	},
	getFileManagerEdit: () => {
		return Template.instance().fm;
	},
	presentFiles: (files) => {
		var attachmentListUL = $('#attachmentsEditAnswer');
		Template.instance().presentFiles = [];
		_.each(files, function(fileId) {
			Template.instance().presentFiles.push(fileId);
			let thisFile = UserFiles.findOne(fileId);
			if (thisFile != undefined && thisFile != null) {
				let data = {};
				data.name = thisFile.name;
				data.size = (thisFile.size / 1000).toFixed(2) + ' kb';
				data.id = "file-" + thisFile._id;

				attachmentListUL.append(constructAttachmentElement(data, true));

				var wrapper = Template.instance().$('#file-' + thisFile._id);
				wrapper.find('.meter').remove();
				wrapper.addClass('completed');
			}
		})
	},
	errorMessage: () => {
		return Template.instance().errorMessage.get();
	},
})
