/* globals Template */
/* globals FlowRouter */
/* globals ClassifiedsFeed */
/* globals _ */
/* globals Meteor */
/* globals ReactiveVar */
import { ModuleClassifiedsViewAd, ModuleClassifiedsList, ForumLang, CommonTranslation, Email } from "/common/lang/lang.js"
import MbTextArea from '/client/components/MbTextArea/MbTextArea'
import { FileManager } from '/client/components/fileManager/filemanager.js'
import MbFilePreview from '/client/components/MbFilePreview/MbFilePreviewReact';
import moment from 'moment'

const saveComment = (t) => {
  if (t.data.onSubmit && typeof t.data.onSubmit === 'function') {
    t.data.onSubmit(t.message.get(), t.files.get(), t)

    // t.files.set([])
    // t.message.set('')
  }
}

Template.module_classifieds_comment.onCreated(function () {
  this.fm = new FileManager(UserFiles, {
    fromAds: true,
    classifiedsId: Template.currentData().classifiedsId
  });

  this.isEmpty = new ReactiveVar(true)
  this.isEmptyEdit = new ReactiveVar(true)

  this.files = new ReactiveVar([])
  this.message = new ReactiveVar('')

  this.editedCommentTmpFiles = new ReactiveVar([])
  this.condoId = _.has(Template.currentData(), 'dataGestionnaire') ? Template.currentData().dataGestionnaire.condoId : FlowRouter.getParam('condo_id')

  this.editedComment = new ReactiveDict()
  this.editedComment.setDefault({
    id: null,
    message: null,
    files: []
  })
})

Template.module_classifieds_comment.events({
  'click .mb-comment-save': (e, t) => {
    if (!t.isEmpty.get() || t.files.get().length > 0) {
      $('.mb-comment-save').button('loading')
      e.preventDefault()
      saveComment(t)
    }
  },
  'click .delete-comment': (e, t) => {
    const feedId = e.currentTarget.getAttribute('data-comment-id')
    const lang = FlowRouter.getParam("lang") || "fr";
    const tl_deletePost = new ForumLang(lang);
    const tl_common = new CommonTranslation(lang)
    const tl_classsified = new ModuleClassifiedsViewAd(lang)

    bootbox.confirm({
      title: tl_common.commonTranslation["confirmation"],
      message: tl_classsified.moduleClassifiedsViewAd["delete_answer"],
      buttons: {
        confirm: {label: tl_deletePost.forumLang["delete"], className: "btn-red-confirm"},
        cancel: {label: tl_deletePost.forumLang["cancel"], className: "btn-outline-red-confirm"}
      },
      callback: function(res) {
        if (res) {
          Meteor.call('deleteClassifiedAnswer', t.condoId, feedId, function (error, result) {
            if (!error) {
              sAlert.success(tl_classsified.moduleClassifiedsViewAd["success_delete_answer"])
            }
          })
        }
      }
    });
  },
  'click .edit-comment': (e, t) => {
    const commentId = e.currentTarget.getAttribute('data-comment-id')
    const comment = _.find(Template.instance().data.messages, message => message._id === commentId)

    if (comment) {
      t.editedComment.set('id', comment._id)
      t.editedComment.set('message', comment.message)
      t.editedComment.set('files', comment.files)
    }
  },
  'click #cancel-update': (e, t) => {
    t.editedComment.set('id', null)
    t.editedComment.set('message', '')
    t.editedComment.set('files', [])
    t.editedCommentTmpFiles.set([])
  },
  'click #commit-update': (e, t) => {
    if (!t.isEmptyEdit.get() || t.editedCommentTmpFiles.get().length > 0) {
      $('#commit-update').button('loading')
      updateComment(t, (e) => {
        $('#commit-update').button('reset')
        if (!e) {
          t.editedComment.set('id', null)
          t.editedComment.set('message', '')
          t.editedComment.set('files', [])
          t.editedCommentTmpFiles.set([]);

          t.isEmptyEdit.set(true);
        } else {
          console.log(e)
        }
      })
    }
  },
  'input .mb-comment-textarea > div > textarea': (e, t) => {
    let text = e.currentTarget.value.trim()
    t.isEmpty.set(!(!!text && text.length > 0))
  },
  'input .mb-comment-text-edit > div > div > textarea': (e, t) => {
    let text = e.currentTarget.value.trim()
    t.isEmptyEdit.set(!(!!text && text.length > 0))
  }
})

const updateComment = (t, cb) => {
  const __files = t.editedCommentTmpFiles.get()
  let additionalFiles = []

  const lang = FlowRouter.getParam('lang') || 'fr'
  const tl_common = new CommonTranslation(lang)

  t.fm.batchUpload(__files)
    .then(files => {
      Meteor.call('editClassifiedAnswer',
        t.condoId,
        t.editedComment.get('id'),
        t.editedComment.get('message'),
        t.editedComment.get('files').concat(_.map(files, (f) => f._id)),
        (e) => cb(e)
      )
    })
    .catch(err => {
      sAlert.error(tl_common.commonTranslation[err.reason])
      cb('Error while uploading files')
    })
}

Template.module_classifieds_comment.helpers({
  getMessage: () => Template.instance().message.get(),
  onEditCommentKeyDown: () => {
    const t = Template.instance()

    return (e) => {
      if (e.key === 'Enter' && !e.shiftKey && t.isEmptyEdit.get() !== true) {
        e.preventDefault()
        $('#commit-update').button('loading')

        updateComment(t, (e) => {
          $('#commit-update').button('reset')
          if (!e) {
            t.editedComment.set('id', null)
            t.editedComment.set('message', '')
            t.editedComment.set('files', [])
            t.editedCommentTmpFiles.set([]);

            t.isEmptyEdit.set(true);
          } else {
            console.log(e)
          }
        })
      }
    }
  },
  bindKeyDown: () => {
    const t = Template.instance()

    return (e) => {
      if (e.key === 'Enter' && !e.shiftKey && t.isEmpty.get() !== true) {
        e.preventDefault()
        $('.mb-comment-save').button('loading')
        saveComment(t)
      }
    }
  },
  updateMessage: () => {
    const t = Template.instance()

    return (value) => {
      t.message.set(value)
    }
  },
  canModify: (comment) => {
    return comment.userId === Meteor.userId()
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      const files = _.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId)
      tpl.files.set(files)
    }
  },
  onFileAdded: () => {
    const tpl = Template.instance()
    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])
    }
  },
  getFiles: () => Template.instance().files.get(),
  getName: (userId, name) => {
    let user = UsersProfile.findOne(userId)
    if (user) {
      return user[name]
    }
  },
  parseAdCreated: (timestamp) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translation = new Email(lang)
    const ts = parseInt(moment(timestamp).format('x'))
    return moment(ts).locale(lang).format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm')
  },
  getMessages: () => {
    return Template.instance().data.messages
  },
  getFilesCursor: (fileIds) => UserFiles.find({_id: {$in: fileIds.filter(file => !file.fileId)}}),
  getNewFiles: fileIds => fileIds ? fileIds.filter(file => file && file.fileId) : [],
  MbTextArea: () => MbTextArea,
  isCurrentlyEdit: (commentId) => {
    return Template.instance().editedComment.get('id') === commentId
  },
  getEditedCommentMessage: () => {
    return Template.instance().editedComment.get('message') || ''
  },
  onEditedCommentChange: () => {
    const t = Template.instance()

    return value => t.editedComment.set('message', value)
  },
  getPreviewFiles: () => {
    let fileIds = Template.instance().editedComment.get('files')
    const reactiveFiles = Template.instance().editedCommentTmpFiles.get()

    const newFileIds = fileIds.filter(file => file.fileId)
    fileIds = fileIds.filter(file => !file.fileId)

    if (fileIds) {
      const files = UserFiles.find({ _id: { $in: fileIds }});

      const filesWithPreview = _.map(files.fetch(), file => {
        return {
          ...file,
          id: file._id,
          preview: {
            url: `${file._downloadRoute}/${file._collectionName}/${file._id}/preview/${file._id}${file.extensionWithDot}`
          }
        }
      })

      return [
        ...filesWithPreview.concat(reactiveFiles),
        ...newFileIds
      ]
    }

    return []
  },
  onEditCommentFileAdded: () => {
    const t = Template.instance()



    return files => {
      t.isEmptyEdit.set(false);

      t.editedCommentTmpFiles.set([
        ...t.editedCommentTmpFiles.get(),
        ...files
      ])
    }
  },
  onEditCommentFileRemoved: () => {
    const t = Template.instance()
    return (fileId) => {
      t.isEmptyEdit.set(false);

      // Remove file from existing files set
      t.editedComment.set('files', _.filter(t.editedComment.get('files'), f => f.fileId ? f.fileId !== fileId : f !== fileId))

      // Remove file from pre-uploaded files
      t.editedCommentTmpFiles.set(_.filter(t.editedCommentTmpFiles.get('files'), f => f.fileId ? f.fileId !== fileId : f.id !== fileId))
    }
  },
  getPostedState: (feed) => {
    const tl_classsified = new ModuleClassifiedsViewAd(FlowRouter.getParam("lang") || "fr")
    return feed.isEdited ? tl_classsified.moduleClassifiedsViewAd["edited"] : tl_classsified.moduleClassifiedsViewAd["posted"]
  },
  isManager: () => Meteor.isManager,
  disableSubmit: () => {
    return Template.instance().isEmpty.get() && Template.instance().files.get().length === 0
  },
  disableSubmitEdit: () => {
    return Template.instance().isEmptyEdit.get() && Template.instance().editedCommentTmpFiles.get().length === 0
  },
  MbFilePreview: () => MbFilePreview,
})
