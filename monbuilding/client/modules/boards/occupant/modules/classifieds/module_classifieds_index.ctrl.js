import { Template } from 'meteor/templating';

import './views/module_classifieds_index.less';
import './views/module_classifieds_index.view.html';
import './views/module_classifieds_view_ad.view.html';
import './views/module_classifieds_list.view.html';

function onPathChange() {
  FlowRouter.watchPathChange();
  if (FlowRouter.current().queryParams.back)
    Template.instance().oldPost.set("");
  if (Template.instance().oldPost.get() && !_.has(FlowRouter.current().queryParams, 'ad') && FlowRouter.current().context.hash) {
    return Template.instance().oldPost.get();
  }
  if (FlowRouter.current().queryParams.ad && ClassifiedsAds.findOne(FlowRouter.current().queryParams.ad)) {
      Template.instance().oldPost.set(FlowRouter.current().queryParams.ad);
  }
  this.activePost.set(!!FlowRouter.current().queryParams.ad);
};

Template.module_classifieds_index.onCreated(function() {
  this.oldPost = new ReactiveVar();
  this.activePost = new ReactiveVar(false);
  const data = Template.currentData();
  if (!_.has(data.data, 'gestionnaire')) {
    this.subscribe('ClassifiedsAds', { classifiedsId: data.data.classifiedsId });
    this.subscribe('classifieds', data.data.classifiedsId);
  }
  let condo = Condos.findOne(_.has(data.data, 'gestionnaire') ? data.data.condoId : FlowRouter.getParam("condo_id"));
  if (condo && condo.settings.options.classifieds === false) {}
      // FlowRouter.go('/fr/resident');
  if (_.has(data.data, 'gestionnaire')) {
    this.autorun(onPathChange.bind(this));
  }
  Meteor.call('setNewAnalytics', {type: "module", module: "ads", accessType: "web", condoId: FlowRouter.current().params.condo_id});
});

Template.module_classifieds_index.onDestroyed(function() {
  // Meteor.call('updateAnalytics', {type: "module", module: "ads", accessType: "web", condoId: FlowRouter.current().params.condo_id});
})

Template.module_classifieds_index.events({

});

Template.module_classifieds_index.helpers({
  currentPost: () => {
    if (FlowRouter.current().queryParams.back) {
      Template.instance().oldPost.set("");
      return "";
    }
    if (Template.instance().oldPost.get())
      return Template.instance().oldPost.get();
    if (FlowRouter.current().queryParams.ad && ClassifiedsAds.findOne(FlowRouter.current().queryParams.ad)) {
        Template.instance().oldPost.set(FlowRouter.current().queryParams.ad);
        return FlowRouter.current().queryParams.ad;
    }
    return "";
  },
  classifiedsId: () => {
    return Template.currentData().data.classifiedsId;
  },
  profilePicture: (profile) => {
    return profile.firstname[0].toUpperCase() + profile.lastname[0].toUpperCase();
  },
  postedDate: (postedDate) => {
    return new Date(postedDate).toString().split(' ')[2];
  },
  postedDayDate: (postedDate) => {
    let listDay = ["JAN", "FEV", "MAR", "AVR", "MAI", "JUI", "JUIL", "AOU", "SEP", "OCT", "NOV", "DEC"];
    let date = new Date(postedDate);

    return listDay[date.getMonth()];
  },
  compDate: (endDate, endHour) => {
    let date = new Date(endDate);
    let hours = endHour.split(":");
    date.setHours(hours[0], hours[1]);
    return new Date().valueOf() <= date.valueOf()
  },
  editingPost: () => {
    return Template.instance().editingPost.get();
  },
  isNewPost: (post) => {
      let view = post.views[Meteor.userId()];
      if (view)
        return (false);
      return (true);
  },
  nbNewMsg: (post) => {
      if (post.message.length === 0)
          return "";
      let view = post.views[Meteor.userId()];
      if (!view)
        return post.message.length;
      let i = post.message.length - 1;
      let nb = 0;
      while (i >= 0 && post.message[i].createdAt > view) {i--; nb++;};
      return (nb > 0 ? nb : "");
  },
  isGestionnaireView: () => {
    return _.has(Template.currentData().data, 'gestionnaire');
  },
  getDataGestionnaire: () => {
    return {
      condoId: Template.currentData().data.condoId,
      access: Template.currentData().data.access,
      gestionnaire: Template.currentData().data.gestionnaire,
    };
  },
  activePost: () => {
    return Template.instance().activePost.get();
  },
  isSubscribeReady: () => {
    return Template.instance().subscriptionsReady();
  },
});
