/* globals Template */
/* globals ReactiveDict */
/* globals ReactiveVar */
/* globals UserFiles */
/* globals _ */

import MbTextArea from '/client/components/MbTextArea/MbTextArea';
import { FileManager } from '/client/components/fileManager/filemanager.js';
import { CommonTranslation } from '/common/lang/lang.js'

const updateValue = (t, key, value) => {
  t.form.set(key, value)
  validateFields(t)
}

const validateNumber = (str) => {
  return /^\d{0,7}(\.\d{0,2})?$/.test(str);
}

const validateFields = (t) => {
  const requireFields = ['title', 'description']
  const error = _.filter(requireFields, field => !t.form.get(field) || t.form.get(field) === '')

  /**
   * Price is not mandatory so if price is set we need to make sure
   * it only have string of numbers and 10 char limit
   */
  if (t.form.get('price') && t.form.get('price') !== '' && !validateNumber(t.form.get('price'))) {
    error.push('price')
  }

  t.isDisabled.set(error.length !== 0)
}

Template.module_classifieds_form.onCreated(function () {
  this.subscribe('classifiedsFiles', Template.currentData().classifiedsId)
  this.fm = new FileManager(UserFiles, {
    fromAds: true,
    classifiedsId: Template.currentData().classifiedsId
  })

  this.isDisabled = new ReactiveVar(true)

  this.files = new ReactiveVar([])
  this.form = new ReactiveDict()
  this.form.setDefault({
    title: null,
    price: null,
    description: null
  })
})

Template.module_classifieds_form.helpers({
  getFormData: (key) => {
    return Template.instance().form.get(key) || ''
  },
  // Weird blaze, we need to pass 3 function instead 2 :/
  // @ref https://forums.meteor.com/t/blaze-bugs-when-passing-callbacks/23482
  setFormData (key) {
    const tpl = Template.instance()

    return () => value => {
      updateValue(tpl, key, value)
    }
  },
  onFileRemoved: () => {
    const tpl = Template.instance()

    return (fileId) => {
      const files = _.filter(tpl.files.get(), f => f.fileId ? f.fileId !== fileId : f.id !== fileId)

      tpl.files.set(files)
    }
  },
  onFileAdded: () => {
    const tpl = Template.instance()

    return (files) => {
      tpl.files.set([
        ...tpl.files.get(),
        ...files
      ])

      return false
    }
  },
  files: () => Template.instance().files.get(),
  disableSubmit: () => {
    return Template.instance().isDisabled.get()
  },
  MbTextArea: () => MbTextArea,
  isFieldError: (key) => {
    switch (key) {
      case 'price': {
        const value = Template.instance().form.get('price')
        return value !== null && !validateNumber(value);
      }
      default:
        return false
    }
  }
})

Template.module_classifieds_form.events({
  'click #saveAd': (e, t) => {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr_common = new CommonTranslation(lang)

    const data = Template.currentData()
    const __files = Template.instance().files.get();

    $(e.currentTarget).button('loading')
    t.fm.batchUpload(__files)
      .then(files => {

        const Ad = {
          title: t.form.get('title'),
          price: t.form.get('price'),
          description: t.form.get('description') ? t.form.get('description').replace(/<(.|\n)*?>/g, '') : '',
          classifiedsId: data.classifiedsId,
          userId: Meteor.user()._id,
          condoId: FlowRouter.getParam('condo_id'),
          createdAt: Date.now(),
          updatedAt: Date.now(),
          views: {},
          history: [],
          files: _.map(files, file => file._id),
        }

        Meteor.call('module-classifieds-create-ad', Ad, (err) => {
          $(e.currentTarget).button('reset')
          if (!err) {

            setTimeout(function() {
              t.form.set({
                title: null,
                price: null,
                description: null
              })

              t.files.set([])
              validateFields(t)
            }, 0);

          } else {
            bootbox.alert({
              size: 'medium',
              title: 'Oops!',
              message: 'Something went wrong!',
              backdrop: true
            })
          }
        })
      })
      .catch(err => {
        $(e.currentTarget).button('reset')
        return sAlert.error(tr_common.commonTranslation[err.reason]);
      })
  }
})
