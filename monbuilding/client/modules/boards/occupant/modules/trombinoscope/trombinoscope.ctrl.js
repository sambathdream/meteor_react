import { Template } from "meteor/templating";
import { FileManager } from "/client/components/fileManager/filemanager.js";
import { ModuleTrombinoscopeIndex, WhoCanSeeHeader, CommonTranslation } from "/common/lang/lang.js";

import "./trombinoscope.view.html";

var translation;

Template.module_trombinoscope_index.onCreated(function(){
	translation = new ModuleTrombinoscopeIndex((FlowRouter.getParam("lang") || "fr"));
	this.search = new ReactiveVar("");
	this.searchStudent = new ReactiveVar("");
	this.searchSchool = new ReactiveVar("");
	this.searchMatiere = new ReactiveVar("");
	this.selectedUser = new ReactiveVar(null);
	this.user = new ReactiveVar("");
	this.school = new ReactiveVar("");
	this.matiere = new ReactiveVar("");
	this.warning = true;
	this.fm = new FileManager(SchoolPhotos);
	this.who = new ReactiveVar('');
	this.whoReturn = new ReactiveVar(false);
	let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
	if (condo && condo.settings.options.trombi === false) {
    // FlowRouter.go('/fr/resident');
  }
	this.subscribe('schools');
	Meteor.subscribe('infoTrombiGestionnaire', FlowRouter.getParam("condo_id"));
	this.subscribe("GestionnaireUnregistered");
	this.subscribe('helpers', function(){
		if (Helpers.findOne() == undefined || Helpers.findOne().trombi != true) {
			setTimeout(function() {
				$('.help-module').show();
			}, 50)
		}
	});
	Meteor.call('setNewAnalytics', {type: "module", module: "trombi", accessType: "web", condoId: FlowRouter.current().params.condo_id});
});

Template.module_trombinoscope_index.onDestroyed(function() {
	// Meteor.call('updateAnalytics', {type: "module", module: "trombi", accessType: "web", condoId: FlowRouter.current().params.condo_id});
})

Template.module_trombinoscope_index.onRendered(function() {

});

Template.module_trombinoscope_index.events({
	'click .messenger': function(event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		FlowRouter.go('app.board.resident.home', {lang});
	},
	'click .directMessage': function(e, t) {
    const lang = (FlowRouter.getParam('lang') || 'fr')
    let defaultRole = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })

    if (!!defaultRole === true) {
      delete Session.keys['newMessageUserId']
      Meteor.call('checkResidentRoomExist', [e.currentTarget.getAttribute('userid')], FlowRouter.getParam('condo_id'), function (error, result) {
        if (!!result === true) {
          const params = {
            lang,
            condo_id: FlowRouter.getParam('condo_id'),
            module_slug: 'messenger',
            wildcard: `${defaultRole._id}/${result}`
          }
          FlowRouter.go('app.board.resident.condo.module', params)
        } else {
          const receipId = e.currentTarget.getAttribute('userid')
          Session.set('newMessageUserId', receipId)
          const params = {
            lang,
            condo_id: FlowRouter.getParam('condo_id'),
            module_slug: 'messenger',
            wildcard: `${defaultRole._id}/new`
          }
          FlowRouter.go('app.board.resident.condo.module', params)
        }
      })
    } else {
      console.log('Default Role `Occupant` not found.')
    }
	},
	'input #searchTrombi': function(event, template) {
		let search = event.currentTarget.value.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&');
		template.search.set(search);
	},
	'input #searchStudent': function(event, template) {
		template.searchStudent.set(event.currentTarget.value);
	},
	'input #searchSchool': function(event, template) {
		template.searchSchool.set(event.currentTarget.value);
	},
	'input #searchMatiere': function(event, template) {
		template.searchMatiere.set(event.currentTarget.value);
	},
	'click .usertrombi': function(e, t) {
		t.user.set(e.currentTarget.getAttribute("userid"));
		t.school.set("");
		t.matiere.set("");
	},
	'click .school': function(e, t) {
		t.school.set(e.currentTarget.getAttribute("val"));
		t.matiere.set('');
		t.user.set("");
		t.fm.clearFiles();
	},
	'click .matiere': function(e, t) {
		t.matiere.set(e.currentTarget.getAttribute("val"));
		t.user.set("");
	},
	'change #fileUploadInput': function(e, t) {
		if (e.currentTarget.files && e.currentTarget.files.length === 1) {
			t.fm.insert(e.currentTarget.files[0], function(err, file) {
				if(!err && file) {
					Meteor.call('setSchoolId', t.school.get(), file._id);
					t.fm.clearFiles();
				}
			});
		}
	},
	'click #fileUploadInput': function(e, t) {
		if (t.warning) {
			e.preventDefault();
			t.warning = false;
			sAlert.info(translation.moduleTrombinoscopeIndex["be_serious"], {
				onClose: () => {$("#fileUploadInput").click();}});
		}
	},
	'click #resetMatiere': function(e, t) {
		t.matiere.set("");
		t.searchMatiere.set("");
    $('#searchMatiere').val('')
	},
	'click #resetStudent': function(e, t) {
		t.user.set("")
    t.searchStudent.set("")
	},
	'click #resetSchool': function(e, t) {
		t.school.set("")
    t.searchSchool.set("")
    $('#searchSchool').val('')
	},
	'click .more-user-interest': function (e, t) {
    t.selectedUser.set(e.currentTarget.getAttribute('data-user-id'));
    $('#modal-interest').modal('show');
  },
  'shown.bs.modal #modal-interest': function(event, t) {

  },
  'hidden.bs.modal #modal-interest': function(event, t) {
    t.selectedUser.set(null);
  },
	'click button[data-close=help-module]': function(event, template) {
		Meteor.call('desactivateHelper', 'trombi', function(err, res) {
			$('.help-module').fadeOut(200);
		});
	},
});

Template.module_trombinoscope_index.helpers({
  getUserDetail: () => {
    return UsersProfile.findOne(Template.instance().selectedUser.get())
  },
  getProfileUrl: () => {
    let avatar = Avatars.findOne({ _id: Template.instance().selectedUser.get() })
    if (avatar && avatar.avatar) {
      return avatar.avatar.original
    }
    return null
  },
  getUserInitial: (profile) => {
    if (profile) {
      return profile.firstname[0] + profile.lastname[0]
    }
  },
  onSearch: () => {
    const template = Template.instance();
    return (val) => {
      const search = val.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&');
      template.search.set(search);
    }
  },
	infoGest: () => {
		return Enterprises.findOne();
	},
	getNbGestionnaire: (gestionnaireLength, unregisteredLength) => {
		return (gestionnaireLength ? gestionnaireLength : 0) + (unregisteredLength ? unregisteredLength : 0);
	},
	currentCondo: () => {
		return Condos.findOne(FlowRouter.getParam("condo_id"));
  },
  getCondoName: (condo) => {
    return condo.getName()
  },
	users: () => {

		if (Template.instance().user.get() !== "")
			return (ResidentUsers.find({$and: [{_id: Template.instance().user.get()}, {"resident.condos.condoId": FlowRouter.getParam("condo_id")}]}));
		let regexp = new RegExp(Template.instance().search.get(), "i");
		let optionsProfile = [{"profile.lastname": regexp}, {"profile.firstname": regexp}];
		if (Template.instance().school.get() !== "") {
			let condo = Condos.findOne(FlowRouter.getParam("condo_id"));

			if (condo && condo.settings && condo.settings.options && condo.settings.options.schoolSearch)
				optionsProfile.push({"profile.school": regexp});
			if (condo && condo.settings && condo.settings.options && condo.settings.options.studSearch)
				optionsProfile.push({"profile.diplome": regexp});
			if (condo && condo.settings && condo.settings.options && condo.settings.options.subjectSearch)
				optionsProfile.push({"profile.matiere": regexp});
			if (condo && condo.settings && condo.settings.options && condo.settings.options.enterpriseSearch)
				optionsProfile.push({"profile.company": regexp});
      if (condo && condo.settings && condo.settings.options && condo.settings.options.displayInterest)
        optionsProfile.push({"profile.interests": regexp});

			if (Template.instance().matiere.get() !== "")
				return (ResidentUsers.find({$and: [{"profile.school": Template.instance().school.get(), "profile.matiere": Template.instance().matiere.get(), $or: optionsProfile}]}).fetch());
			else
				return (ResidentUsers.find({$and: [{"profile.school": Template.instance().school.get(), $or: optionsProfile}]}).fetch());
		}
		else {
			if (Template.instance().matiere.get() !== "")
				return (ResidentUsers.find({$and: [{"profile.matiere": Template.instance().matiere.get(), $or: optionsProfile}]}).fetch());
			else
			{
				let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
				let orCondition = [
					{"profile.lastname": regexp},
					{"profile.firstname": regexp},
				];

				if (condo && condo.settings && condo.settings.options && condo.settings.options.floorSearch)
					orCondition.push({"resident.condos.0.buildings.0.etage": regexp});
				if (condo && condo.settings && condo.settings.options && condo.settings.options.doorSearch)
					orCondition.push({"resident.condos.0.buildings.0.porte": regexp});
				if (condo && condo.settings && condo.settings.options && condo.settings.options.schoolSearch)
					orCondition.push({"profile.school": regexp});
				if (condo && condo.settings && condo.settings.options && condo.settings.options.studSearch)
					orCondition.push({"profile.diplome": regexp});
				if (condo && condo.settings && condo.settings.options && condo.settings.options.subjectSearch)
					orCondition.push({"profile.matiere": regexp});
				if (condo && condo.settings && condo.settings.options && condo.settings.options.enterpriseSearch)
					orCondition.push({"profile.company": regexp});
        if (condo && condo.settings && condo.settings.options && condo.settings.options.displayInterest)
          orCondition.push({"profile.interests": regexp});

				let result =  (ResidentUsers.find({
					$and: [
					{
						$or: orCondition
					},
					{
						"resident.condos.condoId": FlowRouter.getParam("condo_id")
					}
					]
				}).fetch());
				return result;
			}
		}
	},
  showOnTrombiAndNotBlocked: (user) => {
    const condo = user.resident.condos.find(c => c.condoId === FlowRouter.getParam("condo_id"))
    let blocked = false
    const blockedBy = Meteor.user().blockedBy
    if (!!blockedBy && !!blockedBy.includes(user._id)) {
      blocked = true
    }
    return !(condo.preferences.trombi === false) && !blocked
  },
	trombiGestionnaireUnregistered: () => {
		let regexp = new RegExp(Template.instance().search.get(), "i");
		let trombiGestionnaire = GestionnaireUnregistered.find(
		{
			$and: [
			{
				$or: [
				{"lastname": regexp},
				{"firstname": regexp}
				]
			},
			{
				condoId: FlowRouter.getParam("condo_id")
			}
			]
		}).fetch();

		if (trombiGestionnaire) {
			return trombiGestionnaire;
		}
	},
	trombiGestionnaire: () => {
		let regexp = new RegExp(Template.instance().search.get(), "i");
		let declarationDetail = DeclarationDetails.findOne({key: "managerWAccount"});
		let condoContact = CondoContact.findOne({condoId: FlowRouter.getParam("condo_id")});
		let trombiGestionnaireId = [];
		let trombiGestionnaire = [];
		if (condoContact) {
			trombiGestionnaireId = _.find(condoContact.contactSet, function(contact) {
				return (contact.declarationDetailId == declarationDetail._id);
			})
			if (!!trombiGestionnaireId) {
			let enterprise = Enterprises.findOne({condos: FlowRouter.getParam("condo_id")});
				_.each(trombiGestionnaireId.userIds, function(elem) {
					trombiGestionnaire.push(_.find(enterprise.users, function(e) {
						if (regexp.test(e.firstname) == false && regexp.test(e.lastname) == false)
							return false;
						return (e.userId === elem);
					}))
				});
			}
		}
		trombiGestionnaire = trombiGestionnaire.filter((user) => !!user)
		return trombiGestionnaire;
	},
	hasGestionnaireToDisplay: () => {
		let declarationDetail = DeclarationDetails.findOne({key: "managerWAccount"});
		let condoContact = CondoContact.findOne({condoId: FlowRouter.getParam("condo_id")});
		let trombiGestionnaireId = [];
		let trombiGestionnaire = [];
		if (condoContact) {
			trombiGestionnaireId = _.find(condoContact.contactSet, function(contact) {
				return (contact.declarationDetailId == declarationDetail._id);
			})
			if (!!trombiGestionnaireId) {
				let enterprise = Enterprises.findOne();
				_.each(trombiGestionnaireId.userIds, function(elem) {
					trombiGestionnaire.push(_.find(enterprise.users, function(e) {
						return (e.userId === elem);
					}))
				});
			}
		}
		let trombiGestionnaireU = GestionnaireUnregistered.find({condoId: FlowRouter.getParam("condo_id")}).fetch();
		if ((trombiGestionnaire && trombiGestionnaire.length > 0) || (trombiGestionnaireU && trombiGestionnaireU.length > 0))
			return true;
		return false
	},
	nbUsersDisplay: () => {
		let regexp = new RegExp(Template.instance().search.get(), "i");
		let cursor;

		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
    let optionsProfile = [{"profile.lastname": regexp}, {"profile.firstname": regexp}];

    let lang = FlowRouter.getParam("lang") || "fr"

		if (condo && condo.settings && condo.settings.options && condo.settings.options.schoolSearch)
			optionsProfile.push({"profile.school": regexp});
		if (condo && condo.settings && condo.settings.options && condo.settings.options.studSearch)
			optionsProfile.push({"profile.diplome": regexp});
		if (condo && condo.settings && condo.settings.options && condo.settings.options.subjectSearch)
			optionsProfile.push({"profile.matiere": regexp});
		if (condo && condo.settings && condo.settings.options && condo.settings.options.enterpriseSearch)
			optionsProfile.push({"profile.company": regexp});

		if (Template.instance().school.get() !== "") {
			if (Template.instance().matiere.get() !== "")
				cursor = (ResidentUsers.find({"profile.school": Template.instance().school.get(), "profile.matiere": Template.instance().matiere.get(), $or: optionsProfile}));
			else
				cursor = (ResidentUsers.find({"profile.school": Template.instance().school.get(), $or: optionsProfile}));
		}
		else {
			if (Template.instance().matiere.get() !== "")
				cursor = (ResidentUsers.find({"profile.matiere": Template.instance().matiere.get(), $or: optionsProfile}));
			else
				cursor = (ResidentUsers.find({$or: optionsProfile}));
    }
    const tr_common = new CommonTranslation(lang)
		return (cursor.fetch().length > 1 ? cursor.fetch().length + " " + tr_common.commonTranslation["student"] + tr_common.commonTranslation["s"] : cursor.fetch().length + " " + tr_common.commonTranslation["student"]);
	},
	avatarLink : (userId) => {
		let file = Avatars.findOne({userId: userId});
		if (file) {
			return file.avatar.original.replace("localhost", location.hostname);
		}
		return false;
	},
	getFirstLetter: (value) => {
		var name = "";
		_.each(value.split(' '), function(elem, index) {
			name += elem[0];
		});
		return name;
	},
	getGestionnaireType: (gestionnaire) => {
		const translateTab = {'gardien': 'Gardien',
		'mainteneurAvance': 'Mainteneur du site avancé',
		'assistantCopro': 'Assistant de copropriété',
		'gestionnaireCopro': 'Gestionnaire de copropriété',
		'respoCondo': 'Responsable de l\'immeuble',
		'respoCopro': 'Responsable de copropriété',
		'directeurRegional': 'Directeur régional',
		'directeurEtablissement': "Directeur d'établissement",
		'directeurMarque': 'Directeur de marque',
		'directionSiege': 'Direction siège'};
		return translateTab[gestionnaire.type];
	},
	getAddressOfCondo: () => {
		let condo = Condos.findOne(Template.instance().data.condo._id);
		if (condo)
			return (condo.info.address + " - " + condo.info.city);
	},
	schools: () => {
		let regexp = new RegExp(Template.instance().searchSchool.get(), "i");
		let users = ResidentUsers.find({"profile.school": regexp}).fetch();
		let tab = _.map(users, (user) => {return user.profile.school});
		let filterTab = tab.filter((item, pos) => {
			if (!item) {
				return false;
			}
			return (tab.indexOf(item) === pos);
		});
		return (filterTab);
	},
	matieres: () => {
		let regexp = new RegExp(Template.instance().searchMatiere.get(), "i");
		let users = []
		if (Template.instance().school.get() !== "") {
			users = ResidentUsers.find({ "profile.matiere": regexp, "profile.school": Template.instance().school.get() }).fetch();
		} else {
			users = ResidentUsers.find({ "profile.matiere": regexp }).fetch();
		}
		let tab = _.map(users, (user) => {return user.profile.matiere;});
		let filterTab = tab.filter((item, pos) => {
			if (!item)
				return false;
			return (tab.indexOf(item) === pos);
		});
		return (filterTab);
	},
	displaySchoolName: () => {
		if (Template.instance().school.get() !== "")
			return Template.instance().school.get();
		return translation.moduleTrombinoscopeIndex["all_school"];
	},
	firstLettersSchool: () => {
		if (Template.instance().school.get() !== '') {
			let school = Template.instance().school.get();
			return (school[0].toUpperCase() + school[1].toUpperCase());
		}
	},
	firstLetterMatiere: () => {
		if (Template.instance().matiere.get() !== '') {
			let matiere = Template.instance().matiere.get();
			return (matiere[0].toUpperCase())
		}
	},
	displaySpeciality: () => {
		if (Template.instance().matiere.get() !== "")
		return  Template.instance().matiere.get();
		return "";
	},
	specificFilter: () => {
		if (Template.instance().school.get() !== "")
		return true;
		if (Template.instance().matiere.get() !== "")
		return true;
		return false;
	},
	specificSchool: () => {
		if (Template.instance().school.get() !== "")
			return true;
		return false;
	},
	specificMatiere: () => {
		if (Template.instance().matiere.get() !== "")
			return true;
		return false;
	},
	hasSchoolPhoto: () => {
		if (Template.instance().school.get() !== "") {
			let school = Schools.findOne();
			let name = Template.instance().school.get();
			let specific = _.find(school.schools, (elem) => {
				return elem.name === name;
			});
			if (specific && specific.fileId) {
				return true;
			}
		}
		return false;
	},
	schoolLink: (name) => {
		let school = Schools.findOne();
		if (school) {
			let schoolName = (name === "selected") ? Template.instance().school.get() : name;
			let specific = _.find(school.schools, (elem) => {
				return elem.name === schoolName;
			});
			if (specific && specific.fileId) {
				let file = SchoolPhotos.findOne(specific.fileId);
				if (file)
					return file.link().replace("localhost", location.hostname);
			}
		}
		return "/img/anonymous.png";
	},
	condoOptions: (option) => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		if (condo && condo.settings) {
			return condo.settings.options[option];
		}
	},
	hasMessenger: (userId) => {
		if (userId === Meteor.userId())
			return false;
		let condoId = FlowRouter.getParam("condo_id");
		let resident = ResidentUsers.findOne(userId);
		if (resident) {
			let condo = _.find(resident.resident.condos, (c) => {
				return c.condoId === condoId
			});
			return (condo && condo.preferences && condo.preferences.messagerie === true);
		}
		return true;
	},
	isMessengerGestionnaireActive: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		if (condo && condo.settings) {
			return condo.settings.options.messengerGestionnaire;
		}
  },
  getResidentProfile: (residentCondos) => {
    const selectedCondo = FlowRouter.getParam("condo_id")
    return residentCondos.find(condo => condo.condoId === selectedCondo).userInfo || {}
  },
	displayStatut: (user) => {
		let condoId = FlowRouter.getParam("condo_id");

		let userRight = UsersRights.findOne({userId: user._id, condoId: condoId});
		if (userRight) {
			let defaultRole = DefaultRoles.findOne(userRight.defaultRoleId);
      if (defaultRole && defaultRole.name != "Occupant") {
				return "<p class='info'>" + defaultRole.name + "</p>";
			}
		}
		return "<p class='info'>" + "-" + "</p>";
	},
	displayFloor: (user) => {
    if (user.etage)
      return "<p class='info'>" + translation.moduleTrombinoscopeIndex["floor"] + " : " + user.etage + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displayDoor: (user) => {
    if (user.porte)
      return "<p class='info'>" + translation.moduleTrombinoscopeIndex["door"] + " " + user.porte + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displaySchool: (user) => {
    if (user.school)
      return "<p class='info'>" + user.school + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displayStud: (user) => {
    if (user.diploma)
      return "<p class='info'>" + user.diploma + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displaySubject: (user) => {
    if (user.studies)
      return "<p class='info'>" + user.studies + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displayCompany: (user) => {
    if (user.company != undefined ) {
      let company = CompanyName.findOne({ _id: user.company })
      let name = 'Not defined'
      if (company) {
        return company.name
      }
      return "<p class='info'>" + name + "</p>"
    }
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	displayOffice: (user) => {
    if (user.office != undefined )
      return "<p class='info'>" + translation.moduleTrombinoscopeIndex["office"] + user.office + "</p>";
    else
      return "<p class='info'>" + "-" + "</p>";
	},
	doDisplaySchoolDropdown: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));

		return condo.settings.options.displaySchoolDropdown;
	},
	doDisplaySubjectDropdown: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));

		return condo.settings.options.displaySubjectDropdown;
	},
  renderInterest: (userId) => {
    const userProfile = UsersProfile.findOne({ _id: userId })
    if (!!userProfile && !!userProfile.interests) {
      const interests = userProfile.interests.split(',')
      const length = interests.length
      if (length > 0) {
        const trans = new CommonTranslation((FlowRouter.getParam("lang") || "fr"));
        let html = "<p class='info'>" + trans.commonTranslation["interests"] + ':</p>'
        html += '<div class="tags-container align-center">'
        interests.forEach((interest, i) => {
          if (i > 4 && length > 6 ) {
            if (i === 5) {
              html += '<span class="more-user-interest" data-user-id="'+userId+'">'+'+'+ (length - i + 1) + ' ' + trans.commonTranslation["more_interests"]+'</span>'
            }
          } else {
            html += '<span class="user-interest">'+interest+'</span>'
          }
        })
        html += "</div>"

        return html
      }
    }
  },
	allCanSee: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		var template = Template.instance();
		Meteor.call('getKeeperOfCondo', FlowRouter.getParam("condo_id"), function(error, result) {
			keeper = result;
			Meteor.call('getCSOfCondo', FlowRouter.getParam("condo_id"), function(error, result) {
				cs = result
				if (condo) {
					template.whoReturn.set(((cs && condo.settings.options.trombiCS) || condo.settings.options.trombi || (keeper && condo.settings.options.trombiKeeper)) &&
						!(condo.settings.options.trombiCS && condo.settings.options.trombi && condo.settings.options.trombiKeeper));
				}
			})
		})
		if (Template.instance().whoReturn.get())
			return Template.instance().whoReturn.get();
	},
	whoCanSee: () => {
		var translation = new WhoCanSeeHeader(FlowRouter.getParam('lang') || 'fr');
		var ret = "";
		var condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		var keeper, cs;
		var template = Template.instance();
		Meteor.call('getKeeperOfCondo', FlowRouter.getParam("condo_id"), function(error, result) {
			keeper = result;
			Meteor.call('getCSOfCondo', FlowRouter.getParam("condo_id"), function(error, result) {
				cs = result
				if (condo) {
					let UnionCouncil = (!_.isEmpty(cs) && condo.settings.options.trombiCS);
					let Supervisor = (!_.isEmpty(keeper) && condo.settings.options.trombiKeeper);
					if (UnionCouncil)
						ret += translation.whoCanSeeHeader["union_council"];
					if (UnionCouncil && condo.settings.options.trombi)
						ret += translation.whoCanSeeHeader["and_occupant"];
					else if (condo && condo.settings && condo.settings.options && condo.settings.options.trombi)
						ret += translation.whoCanSeeHeader["occupant"];
					if (!UnionCouncil && !condo.settings.options.trombi && Supervisor)
						ret += translation.whoCanSeeHeader["building_supervisor"];
					else if (Supervisor)
						ret += translation.whoCanSeeHeader["and_building_supervisor"];
					ret = ret.substring(0, ret.length-2);
					ret += ' ';
					template.who.set(ret);
				}
			});
		});
		if (Template.instance().who.get())
			return Template.instance().who.get();
	},
	doPrintHelpModule: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		return !condo.settings.options.trombiAll;
	},
	textSearchBar: () => {
		let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
		let options = [
			translation.moduleTrombinoscopeIndex["lastname"],
			translation.moduleTrombinoscopeIndex["firstname"]
		];

		if (condo && condo.settings && condo.settings.options && condo.settings.options.floorSearch)
			options.push(translation.moduleTrombinoscopeIndex["floor"]);
		if (condo && condo.settings && condo.settings.options && condo.settings.options.doorSearch)
			options.push(translation.moduleTrombinoscopeIndex["door"]);
		if (condo && condo.settings && condo.settings.options && condo.settings.options.schoolSearch)
			options.push(translation.moduleTrombinoscopeIndex["school"]);
		if (condo && condo.settings && condo.settings.options && condo.settings.options.studSearch)
			options.push(translation.moduleTrombinoscopeIndex["degree"]);
		if (condo && condo.settings && condo.settings.options && condo.settings.options.subjectSearch)
			options.push(translation.moduleTrombinoscopeIndex["material"]);
		if (condo && condo.settings && condo.settings.options && condo.settings.options.enterpriseSearch)
			options.push(translation.moduleTrombinoscopeIndex["company"]);

		return options.join(" / ");
	},
  isSearchActive: () => Template.instance().search.get() !== "",
  getAvailabledFields: (users) => {
    const condoId = FlowRouter.getParam("condo_id")
    const condo = Condos.findOne({ _id: condoId });
    const condoOptions = CondosModulesOptions.findOne({ condoId: condoId })
    const defaultOccupantRole = DefaultRoles.findOne({ name: 'Occupant' })

    let availableFields = {
      displayStatus: false,
      displayFloor: false,
      displayDoor: false,
      displaySchool: false,
      displayStud: false,
      displaySubject: false,
      displayCompany: false,
      displayOffice: false,
      displayInterest: false
    }

    users.forEach(user => {
      const userProfile = UsersProfile.findOne({ _id: user._id })
      const userInfo = user.resident.condos.find(condo => condo.condoId === condoId).userInfo || {}

      // if (availableFields.displayStatus === false) {
      //   let userRight = UsersRights.findOne({ userId: user._id, condoId: condoId }, { fields: { defaultRoleId: true } })
      //   if (userRight.defaultRoleId !== defaultOccupantRole._id) {
      //     availableFields.displayStatus = true
      //   }
      // }

      if (userInfo.etage) availableFields.displayFloor = true
      if (userInfo.porte) availableFields.displayDoor = true
      if (userInfo.school) availableFields.displaySchool = true
      if (userInfo.diploma) availableFields.displayStud = true
      if (userInfo.studies) availableFields.displaySubject = true
      if (userInfo.company) availableFields.displayCompany = true
      if (userInfo.office) availableFields.displayOffice = true
      if (userProfile.interests) availableFields.displayInterest = true
    })

    if (condoOptions && condoOptions.profile) {
      availableFields.displayFloor = condoOptions.profile.floor && availableFields.displayFloor
      availableFields.displayDoor = condoOptions.profile.door && availableFields.displayDoor
      availableFields.displaySchool = condoOptions.profile.school && availableFields.displaySchool
      availableFields.displayStud = condoOptions.profile.diploma && availableFields.displayStud
      availableFields.displaySubject = condoOptions.profile.studies && availableFields.displaySubject
      availableFields.displayCompany = condoOptions.profile.company && availableFields.displayCompany
      availableFields.displayOffice = condoOptions.profile.office && availableFields.displayOffice
    }
    if (condo && condo.settings && condo.settings.options) {
      availableFields.displayFloor = condo.settings.options.displayFloor && availableFields.displayFloor
      availableFields.displayDoor = condo.settings.options.displayDoor && availableFields.displayDoor
      availableFields.displaySchool = condo.settings.options.displaySchool && availableFields.displaySchool
      availableFields.displayStud = condo.settings.options.displayStud && availableFields.displayStud
      availableFields.displaySubject = condo.settings.options.displaySubject && availableFields.displaySubject
      availableFields.displayCompany = condo.settings.options.displayEnterprise && availableFields.displayCompany
      availableFields.displayOffice = condo.settings.options.displayOffice && availableFields.displayOffice
      availableFields.displayInterest = condo.settings.options.displayInterest && availableFields.displayInterest
    }
    return availableFields
  },
  isFieldAvailable: (availableFields, field) => !!availableFields[field],
  getUserName: (profile, initial) => {
    return !!initial ? `${profile.firstname[0]}${profile.lastname[0]}`: `${profile.firstname} ${profile.lastname}`;
  },
  getUserProfile: (id) => {
    return UsersProfile.findOne(id)
  },
  isNotEmpty: (users, managers, unregisteredManagers) => {
    return (users && users.length) || (managers && managers.length) || (unregisteredManagers && unregisteredManagers.length)
  },
  getBlockedClass: (userId) => {
    const blockedUsers = Meteor.user().blockedUsers
    if (!!blockedUsers) {
      return !!blockedUsers.includes(userId)? 'blocked': ''
    }

    return ''
  }
});
