import { ModuleManual, ModuleMap, DeletePost, CommonTranslation } from '/common/lang/lang.js';
import { FileManager } from '/client/components/fileManager/filemanager.js';

Template.module_manual_plan.onCreated(function() {
  let x = ''
  if (Meteor.userHasRight('manual','see'))
    x = 'manual'
  if (Meteor.userHasRight('map','see') && x === '')
    x = 'map'
  this.tab = new ReactiveVar(x)
  this.fm = new FileManager(CondoDocuments, { condoId: FlowRouter.getParam("condo_id") });
});

Template.module_manual_plan.events({
  'click .mb-nav-tabs li a': function(e, t) {
    e.preventDefault();

    // $(e.currentTarget).parents('ul').find('li').removeClass('active');
    // $(e.currentTarget).parent('li').addClass('active');

    const tab = $(e.currentTarget).attr('data-tab');

    t.tab.set(tab);
  },
  'click #deleteManual': function (e, t) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const bootConfirm = new DeletePost(lang)

    bootbox.confirm({
      title: translate.commonTranslation.delete_manual,
      message: translate.commonTranslation.sure_delete_manual,
      buttons: {
        confirm: { label: bootConfirm.deletePost["yes"], className: "btn-red-confirm" },
        cancel: { label: bootConfirm.deletePost["no"], className: "btn-outline-red-confirm" }
      },
      callback: function (result) {
        if (result)
          Meteor.call('deleteManual', FlowRouter.getParam("condo_id"));
      }
    });
  },
  'click #deleteMap': function (e, t) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const translate = new CommonTranslation(lang)
    const bootConfirm = new DeletePost(lang);

    bootbox.confirm({
      title: translate.commonTranslation.delete_modal_title_map,
      message: translate.commonTranslation.delete_modal_content_map,
      buttons: {
        confirm: { label: bootConfirm.deletePost["yes"], className: "btn-red-confirm" },
        cancel: { label: bootConfirm.deletePost["no"], className: "btn-outline-red-confirm" }
      },
      callback: function (result) {
        if (result)
          Meteor.call('deleteMap', FlowRouter.getParam("condo_id"));
      }
    });
  },
  'change #fileUploadInputMap': function (e, t) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr_common = new CommonTranslation(lang)
    if (e.currentTarget.files && e.currentTarget.files.length == 1) {
      t.fm.insert(e.currentTarget.files[0], function (err, file) {
        if (!err && file && file.ext === 'pdf' && file.extension === 'pdf') {
          Meteor.call('setMap', FlowRouter.getParam("condo_id"), file._id, "file");
          t.fm.clearFiles();
        } else {
          if (!err && file.ext !== 'pdf' && file.extension !== 'pdf') {
            sAlert.error(tr_common.commonTranslation['pdf_only'])
          } else {
            sAlert.error(err.message);
          }
        }
      });
    }
  },
  'click #addMap': function (e, t) {
    $("#fileUploadInputMap").click();
  },
})

Template.module_manual_plan.helpers({
  updateTab: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo) {
      if (!condo.settings.options['manual'] && !condo.settings.options['map']) {
        const lang = FlowRouter.getParam('lang') || 'fr'
        const condoId = FlowRouter.getParam('condo_id')
        return FlowRouter.go('app.board.resident.condo', {
          lang,
          condo_id: condoId
        })

      } else if (!condo.settings.options['manual'] && Template.instance().tab.get() === 'manual') {
        Template.instance().tab.set('map')
      } else if (!condo.settings.options['map'] && Template.instance().tab.get() === 'map') {
        Template.instance().tab.set('manual')
      }
    }
    if (!Template.instance().tab.get()) {
      Template.instance().tab.set('manual')
    }
  },
  tab: () => {
    return Template.instance().tab.get();
  },
  manualType: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne({_id: condoId});
    if (condo && condo.manual && condo.manual.type) {
      return condo.manual.type;
    }
  },
  hasManual: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo && condo.manual) {
      return !!condo.manual;
    }

    return false;
  },
  hasMap: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo && condo.map) {
      return !!condo.map.data;
    }

    return false;
  },
  getTabClass: (tab) => {
    return Template.instance().tab.get() === tab ? 'active' : '';
  },
  getTranslatedActiveTab: () => {
    const lang = FlowRouter.getParam("lang") || 'fr';
    const manualLang = new ModuleManual(lang);
    const planLang = new ModuleMap(lang);

    return Template.instance().tab.get() === 'manual' ? manualLang.moduleManual.manual : planLang.moduleMap.plan;
  },
  manualActive: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo) {
      return condo.settings.options['manual'];
    }
  },
  mapActive: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo) {
      return condo.settings.options['map'];
    }
  },
  canShow: () => {
    return Meteor.userHasRight('manual','see') ||
    Meteor.userHasRight('map','see')
  }
})
