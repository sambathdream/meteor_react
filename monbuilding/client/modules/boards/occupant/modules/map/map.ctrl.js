import { FileManager } from '/client/components/fileManager/filemanager.js';
import { DeletePost } from '/common/lang/lang.js'

function createPdfViewer (condoId) {
  let condo = Condos.findOne({ _id: condoId });
  if (condo && condo.map) {
    let file = CondoDocuments.findOne({ _id: condo.map.data });
    let url = file.link().replace("localhost", location.hostname);
    PDFJS.workerSrc = '/packages/pascoual_pdfjs/build/pdf.worker.js';

    PDFJS.getDocument(url).then(function renderPages (pdf) {
      for (let currentPage = 1; currentPage <= pdf.numPages; currentPage++) {
        pdf.getPage(currentPage).then(function renderPage (page) {
          var scale = 1;
          var viewport = page.getViewport(scale);

          var canvas = document.createElement('canvas');
          var context = canvas.getContext('2d');
          canvas.style.border = '1px solid #000'
          canvas.style.margin = '5px auto';
          canvas.width = viewport.width;
          canvas.height = viewport.height;

          page.render({ canvasContext: context, viewport: viewport });
          document.getElementById('pdfCanvas').appendChild(canvas);
        });
      }
    });
  }
}

Template.module_map.onCreated(function () {
  Meteor.subscribe('condodocuments');
  this.fm = new FileManager(CondoDocuments, { condoId: FlowRouter.getParam("condo_id") });
  let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
  if (condo && condo.settings.options.map === false) {
    // FlowRouter.go('/fr/resident');
  }
  Meteor.call('setNewAnalytics', { type: "module", module: "map", accessType: "web", condoId: FlowRouter.current().params.condo_id });
  Meteor.call('setDocumentHasView', FlowRouter.getParam('condo_id'), 'map');
});


Template.module_map.onRendered(function () {
});

Template.module_map.onDestroyed(function () {
  // Meteor.call('updateAnalytics', {type: "module", module: "map", accessType: "web", condoId: FlowRouter.current().params.condo_id});
})

Template.module_map.events({
  'change #fileUploadInputMap': function (e, t) {
    if (e.currentTarget.files && e.currentTarget.files.length == 1) {
      t.fm.insert(e.currentTarget.files[0], function (err, file) {
        if (!err && file) {
          Meteor.call('setMap', FlowRouter.getParam("condo_id"), file._id, "file");
          t.fm.clearFiles();
        }
        else
          sAlert.error(err.message);
      });
    }
  },
  'click #addMap': function (e, t) {
    $("#fileUploadInputMap").click();
  },
});

Template.module_map.helpers({
  isSmallerDevice: () => {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      return true;
    } else {
      return false;
    }
  },
  mapData: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo && condo.map) {
      let data = condo.map.data;
      if (data) {
        let file = CondoDocuments.findOne({ _id: data });
        if (file) {
          const link = `/download/CondoDocuments/${file._id}/original/${file.name || file._id + '.pdf'}`
          return link
        }
      }
    }
  },
  mapFile: (id) => {
    let condoId = FlowRouter.getParam("condo_id");
    if (condoId != "") {
      createPdfViewer(condoId);
      return true;
    }
    return false;
  },
  mapType: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne({ _id: condoId });
    if (condo && condo.map && condo.map.type) {
      return condo.map.type;
    }
  },
  hasMap: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo && condo.map) {
      let data = condo.map.data;
      if (data) {
        return true;
      }
      return false;
    }
    return false;
  },
  lastUpdate: () => {
    let condoId = FlowRouter.getParam("condo_id");
    let condo = Condos.findOne(condoId);
    if (condo) {
      return (condo.map.mapUpdate);
    }
  },
  getAddressOfCondo: () => {
    let condo = Condos.findOne(FlowRouter.getParam("condo_id"));
    if (condo)
      return (condo.info.address + " - " + condo.info.city);
  },
});
