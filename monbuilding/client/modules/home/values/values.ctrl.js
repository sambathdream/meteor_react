
import "./values.view.html"

Template.ourvalues.onCreated(function () {
    this.selectedTab = new ReactiveVar('data');
});


Template.ourvalues.onRendered(function() {

});



Template.ourvalues.events({
    'click #backHome': function(event, template) {
        FlowRouter.go('app.home', {lang: FlowRouter.getParam("lang") || "fr"});
    },

    'mouseenter .tabUser': function(event, template) {
        $('[name="' + $(event.currentTarget).attr("tab") + '"]').toggleClass('animON', 'animOFF');
        template.selectedTab.set($(event.currentTarget).attr("tab"));
    },
    'mouseleave .tabUser': function(event, template) {
        $('[name="' + $(event.currentTarget).attr("tab") + '"]').toggleClass('animON', 'animOFF');
    },

});

Template.ourvalues.helpers({
    selectedTab: (name) => {
        return (Template.instance().selectedTab.get()==name);
    },
});