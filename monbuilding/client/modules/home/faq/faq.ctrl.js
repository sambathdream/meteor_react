
import "./faq.view.html"

Template.faq.onCreated(function () {
});

Template.faq.onRendered(function() {

});

Template.faq.events({
    'click #backHome': function(event, template) {
        FlowRouter.go('app.home', {lang: FlowRouter.getParam("lang") || "fr"});
    },

    'click #aboutUsLink': function(event, template) {
        FlowRouter.go('app.infos.ourvalues', {lang: FlowRouter.getParam("lang") || "fr"});        
    }

});

Template.faq.helpers({

});