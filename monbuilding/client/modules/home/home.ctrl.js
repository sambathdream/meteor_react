import { Template } from "meteor/templating";
import { FileManager } from "../../components/fileManager/filemanager.js";
import {TweenMax, Power2, TimelineLite} from 'gsap';
import { Satisfaction, HomePage } from "/common/lang/lang.js";

import "./home.view.html";
/* Disable. */
const testiMoniesData = [
	{
		en:
		{
			testimonies_content: '"MonBuilding offers a complete and flexible offer to animate Student Factory student residences. It is a partner of choice that accompanies our teams in the management of our residences, and allows residents to benefit from a range of services. The team is very responsive and attentive."',
			testimonies_user: '',
			testimonies_company: ''
		},
		fr: {
			testimonies_content: '"MonBuilding nous propose une offre complète et flexible pour animer les résidences étudiants Student Factory. C’est un partenaire de choix qui accompagne nos équipes dans la gestion de nos résidences, et permet aux résidents de bénéficier d\'un ensemble de services. L’équipe est très réactive et à l\'écoute."',
			testimonies_user: '',
			testimonies_company: ''
		}
	},
];
/* */
Template.home.onCreated(function () {
  if (domainConfig.restrictToLoginPage) {
    if (FlowRouter.getRouteName() === 'app.home') {
      const lang = FlowRouter.getParam("lang") || "fr"
      FlowRouter.go("app.login.login", { lang })
    }
  } else {
    document.body.style.setProperty("--primary-color", '#69d2e7');
    document.body.style.setProperty("--secondary-color", '#fe0900');
    document.body.style.setProperty("--third-color", '#fdb813');
  }
	let url = new URL(location.href);
	this.testimonies = new ReactiveVar(testiMoniesData);
	let currentContext = localStorage.getItem("getappViewed") || 0
	this.emailContact = new ReactiveVar(null)
  	this.textContact = new ReactiveVar('')
	if ($(window).width() < 490 && parseInt(currentContext) < 3 && url.searchParams.get("nogetapp") != "true") {
    FlowRouter.go("app.infos.getapp", {lang: 'fr'});
  }
});

let maxHeight = function(elems) {
	return Math.max.apply(null, elems.map(function ()
	{
		return $(this).height();
	}).get());
};

function recalculateFeatureHeigh() {
	let elem = $(".feature-box p");
	let height = maxHeight(elem);
	let elem2 = $(".feature-box");

	if ($( window ).width() > 425) {
		elem.height(height);
		elem2.height(height + 150);
		elem2.css('margin-bottom', '');
	} else {
		elem.css('height', '');
		elem2.css('height', '');
		elem2.css('margin-bottom', '20px');
	}
}
function calculateTestiMoniesLayout(){
	$('.arrows > a').css('margin-top', ($('.testimonies-box').height()/2 - ($('.testimonies-profile').height() - 46)-30)+'px');
	$('.arrow-mobile-view > a').children().css('top', (($('.testimonies-box').height()+23) - ($('.testimonies-profile').height()-46))+'px');
}

Template.home.onRendered(function() {
	let lang = FlowRouter.getParam("lang");
	translation = new Satisfaction(lang);
	if (Session.get('satisfaction')) {
		Session.set('satisfaction', undefined);
		sAlert.success(translation.satisfaction["surveyComplete"]);
	}

	setTimeout(function () {
		recalculateFeatureHeigh();
		calculateTestiMoniesLayout();
	}, 500);
	$(window).on('resize', function(){
		recalculateFeatureHeigh();
		calculateTestiMoniesLayout();
	});
	var $slogans = $("p.slogan").find("small");
	var $holder = $("#holder");
	$slogans.parent().css({position : "absolute", top:"0px", left:"0px"});

	var transitionTime = .8;
	var slogansDelayTime = 3;

	var totalSlogans = $slogans.length;

	var oldSlogan = 0;
	var currentSlogan = -1;

	switchSlogan();

	function switchSlogan(){

		oldSlogan = currentSlogan;

		if(currentSlogan < totalSlogans-1){
			currentSlogan ++
		} else {
			currentSlogan = 0;
		}

		TweenLite.to($slogans.eq(oldSlogan), transitionTime, {top:-20, alpha:0, rotationX: 90});
		TweenLite.fromTo($slogans.eq(currentSlogan), transitionTime, {top:20, alpha:0, rotationX: -90 }, {top:0, alpha:1, rotationX:0});

		TweenLite.delayedCall(slogansDelayTime, switchSlogan);
	};


	$('.js-scrollTo').on('click', function () {
		var page = $(this).attr('href');
		var speed = 1500;
		$('html, body').animate({
			scrollTop: $(page).offset().top-85
		}, speed);
		return false;
	});

});

const isValidEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

Template.home.events({
	'click a[href^="#"]': function(event, template) {
		let href = $(event.currentTarget).attr('href')
		event.preventDefault()
		let val = ($(href).offset().top === 0) ? 0 : $('body').scrollTop() + $(href).offset().top
		$('body').animate({
			scrollTop: val
		}, 500)
	},
	'input [name="contact_email"]': function(event, template) {
		let value = $(event.currentTarget).val()
		if (value.length === 0) {
			template.emailContact.set(null)
		}
		else if (isValidEmail(value)) {
			template.emailContact.set(value)
		} else {
			template.emailContact.set(false)
		}
	},
	'input [name="contact_text"]': function(event, template) {
		let value = $(event.currentTarget).val()
		template.textContact.set(value)
	},
	'click [name="sendFormContact"]': function(event, template) {
		let email = template.emailContact.get()
		let text = template.textContact.get()

		if (isValidEmail(email) && text.length > 0) {
			const lang = (FlowRouter.getParam("lang") || "fr")
			translation = new HomePage(lang)
			Meteor.call('sendEmailContactHomePage', email, text, (error, result) => {
				if (!!error) {
					sAlert.error(error)
				} else {
          $('input[name="contact_email"]').val('')
          $('textarea[name="contact_text"]').val('')
          template.emailContact.set('')
          template.textContact.set('')
					sAlert.success(translation.homePage["contactComplete"])
				}
			})
		}
	},
	'click #button_signin': function(event, template) {
		const lang = (FlowRouter.getParam("lang") || "fr");
		FlowRouter.go("app.login.signup", {lang: lang});
	},
	'submit #formJoin': function (event, template) {
		event.preventDefault();
		event.stopPropagation();
		const lang = (FlowRouter.getParam("lang") || "fr");
		const data = {
			firstname: template.$("#i-fname").val(),
			lastname: template.$("#i-lname").val(),
			mail: template.$("#usermail").val(),
		};

		FlowRouter.go("app.login.signup", {lang: lang}, {formData: data});
	},
	'click .toOffice': function(event, template) {
		event.preventDefault();
		$('.home_body').find('.office-section').removeClass('hidden');
		$('.home_body').find('.residential-section').addClass('hidden');
	},
	'click .toResidential': function(event, template) {
		event.preventDefault();
		$('.home_body').find('.office-section').addClass('hidden');
		$('.home_body').find('.residential-section').removeClass('hidden');
	}
});

Template.home.helpers({
	getRandom: () => {
		return _.random(0, 100000);
	},

	langSelected: () => {
        return FlowRouter.getParam('lang') || 'fr'
	},
	isEmailError: () => {
		if (Template.instance().emailContact.get() === false) {
			return true
		} else {
			return false
		}
	},
	disableButtonContact: () => {
		if (!!Template.instance().emailContact.get() && Template.instance().textContact.get().length > 0) {
			return false
		} else {
			return true
		}
	},
	visibleArrowbuttons: ()=>{
		if(Template.instance().testimonies.get().length > 1){
			return true
		}else{
			return false;
		}
	},
	getTestimoniesList: () => {
		return Template.instance().testimonies.get();
	}
});
