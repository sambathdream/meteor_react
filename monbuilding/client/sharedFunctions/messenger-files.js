import FileServer from '/client/utils/file-server.js'

const getMessageFiles = (Template) => {
  return (message) => {
    const instance = Template.instance()
    const messengerFiles = message.files ? message.files.filter(file => !file.fileId) : []
    const incidentFiles = message.incidentFiles ? message.incidentFiles.filter(file => !file.fileId) : []

    let returnFiles = []
    if (messengerFiles.length > 0) {
      returnFiles = instance.getFiles(MessengerFiles.find({ _id: { $in: messengerFiles } }).fetch())
    } else if (incidentFiles.length > 0) {
      returnFiles = instance.getFiles(IncidentFiles.find({ _id: { $in: incidentFiles } }).fetch())
    }

    const newMessengerFiles = message.files ? message.files.filter(file => file.fileId) : []
    const newIncidentFiles = message.incidentFiles ? message.incidentFiles.filter(file => file.fileId) : []

    const populateUrls = (file) => {
      let videoRelatedUrls = {
        isVideoFile: false
      }

      if (file.type === 'video') {
        videoRelatedUrls.thumbnailUrl = FileServer.getFileUrl(file, 'thumbnail')
        videoRelatedUrls.videoUrl = FileServer.getFileUrl(file, 'video')
        videoRelatedUrls.isVideoFile = true
      }
      return {
        ...file,
        ...videoRelatedUrls
      }
    }

    return [
      ...returnFiles,
      ...newMessengerFiles.map(populateUrls),
      ...newIncidentFiles.map(populateUrls)
    ]
  }
}

export {
  getMessageFiles
}
