import { Meteor } from 'meteor/meteor'
import { i18n, i18nSectionWrapped } from '/common/lang/lang.js'

let i18nAD
let lang

const confirmationBody = () => {
  return `
    <p>${i18nAD('modalConfirmationMessage')}</p>
    <div>
      <label>${i18nAD('deferredDate')}</label>
      <input type="date" id="deferred-date"/>
    </div>
  `
}

const loadingBody = () => {
  return `
    <p>
      <i class="fa fa-spin fa-spinner"></i> ${i18n(lang, 'loading')}...
    </p>
  `
}

const handleActiveDirectoryModal = (condoId) => {
  lang = FlowRouter.getParam('lang') || 'fr'
  i18nAD = i18nSectionWrapped({
    lang,
    section: 'activeDirectory'
  })
  const title = i18nAD('modalTitle')
  const genericAlert = (message) => {
    bootbox.alert({ title, message, size: 'small' })
  }

  Meteor.call('integrations.verifyAD', { condoId }, (error, result) => {
    if (error) {
      genericAlert(i18nAD('errorMessage'))
    } else {
      if (result) {
        const confirm = bootbox.confirm({
          size: 'medium',
          title,
          message: confirmationBody,
          buttons: {
            'cancel': { label: i18n(lang, 'cancel'), className: 'btn-outline-red-confirm' },
            'confirm': { label: i18n(lang, 'confirm'), className: 'btn-red-confirm' }
          },
          backdrop: true,
          callback: function (result) {
            if (result) {
              const chosenDate = confirm.find('.bootbox-body #deferred-date').val().trim()
              const dialog = bootbox.dialog({
                title,
                message: loadingBody,
                closeButton: false
              })
              Meteor.call('integrations.refreshUsersFromAD', {
                condoId,
                dateToSendEmail: chosenDate
              }, (error, result) => {
                dialog.modal('hide')
                if (error) {
                  genericAlert(error.message)
                } else {
                  genericAlert(i18nAD('completedMessage'))
                }
              })
            }
          }
        })
      } else {
        genericAlert(i18nAD('withoutAccessMessage'))
      }
    }
  })
}

export {
  handleActiveDirectoryModal
}
