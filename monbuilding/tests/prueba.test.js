/* eslint-env mocha */
/* eslint-disable func-names, prefer-arrow-callback */

// These are Chimp globals
/* globals browser assert server */

function login() {
  browser.waitForExist('#loginForm');
  browser.setValue('input[name=email]', 'resident1@gmail.com');
  const elements = browser.elements('input[name=email]');
  console.log(elements)
  return elements.value.length;
}

describe('load page', function () {
  beforeEach(function () {
    browser.url('http://localhost:3000')
    browser.waitForExist('#button_login');
    browser.click('#button_login')
  })

  it('should login to page @watch', function () {
    browser.waitForExist('#loginForm')
    browser.setValue('input[name=email]', 'resident1@gmail.com')
    browser.setValue('input[name=password]', 'resident')
    browser.click('#button_connect')
    let startLogin = new Date()
    browser.waitForExist('#nav_side_menu', 5000, false)
    let endLogin = new Date()
    let loadSeconds = endLogin.getSeconds() - startLogin.getSeconds()
    console.log(`load page in: ${loadSeconds} seconds`)
    assert.ok(loadSeconds < 3)
  })

  // it('can create a list @watch', function () {
  //   server.call('addMessage', {});
  //   const initialCount = login();
  //   browser.click('.js-new-list');
  //   assert.equal(countLists(), initialCount + 1);
  // });
})

// it('can create a list @watch', function () {
//   server.call('addMessage', {});
//   const initialCount = countLists();
//   browser.click('.js-new-list');
//   assert.equal(countLists(), initialCount + 1);
// });