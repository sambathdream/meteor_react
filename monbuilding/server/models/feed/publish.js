Meteor.startup(
  function () {

    /* Publish X element per collections	*/
    /* condoId: String 						*/

    Meteor.publish('newPostFeed', function(condoId, requestedDate, oldWayRequestedDate) {
      if (Meteor.userId()) {
        let self = this
        this.added("feedNewPostsCounter", condoId, { newPosts: 0 })
        self.feedCounter = 0

        let userId = Meteor.userId()
        let residentRoleId = (UsersRights.findOne({
          $and: [
            { "userId": userId },
            { "condoId": condoId }
          ]
        }) || {}).defaultRoleId
        let isOccupant = !!DefaultRoles.findOne({ _id: residentRoleId, name: 'Occupant' })

        let IncidentQuery = {
          condoId: condoId,
          createdAt: { $gt: oldWayRequestedDate },
          $or: [
            {
              "state.status": {
                $in: [1, 2]
              }
            },
            {
              $or: [
                {
                  $and: [
                    { "state.status": { $in: [0, 3] } },
                    { "declarer.userId": userId }
                  ]
                }
              ]
            }
          ]
        }
        if (!isOccupant) {
          let field = 'history.share.' + residentRoleId + '.state'
          IncidentQuery.$or[1].$or.push({
            $and: [
              { "state.status": 3 },
              { [field]: true }
            ]
          })
        }

        let newIncidentCursor = Incidents.find(IncidentQuery, { fields: { _id: 1, createdAt : 1 } })

        let forumCursor = ForumPosts.find({ condoId, date: { $gt: requestedDate } }, { fields: { _id: 1, date: 1 } })
        let classifiedCursor = ClassifiedsAds.find({ condoId: condoId, createdAt: { $gt: requestedDate } }, { fields: { _id: 1, createdAt: 1 } })
        let actuCursor = ActuPosts.find({
          condoId: condoId,
          createdAt: { $gt: requestedDate },
          ...Meteor.call('getNewEventFilter', Meteor.userId())
        }, { fields: { _id: 1, createdAt: 1 } })
        let adminIds = []
        Meteor.users.find({ 'identities.adminId': { $exists: true } }).forEach(userAdmin => {
          adminIds.push(userAdmin._id)
        })
        let usersJoinedCursor = Residents.find({ "condos.condoId": condoId, userId: { $nin : adminIds } }, { fields: { "condos.joined": true, userId: true, "condos.condoId": true } })

        const handleIncident = newIncidentCursor.observe({
          added: function (document) {
            self.feedCounter++
            self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
          }
        })
        const handleForum = forumCursor.observe({
          added: function (document) {
            self.feedCounter++
            self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
          }
        })
        const handleClassified = classifiedCursor.observe({
          added: function (document) {
            self.feedCounter++
            self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
          }
        })
        const handleActu = actuCursor.observe({
          added: function (document) {
            self.feedCounter++
            self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
          }
        })
        const handleUsersJoined = usersJoinedCursor.observe({
          added: function (document) {
            let joined = _.find(document.condos, (condo) => { return condo.condoId == condoId }).joined
            joined = parseInt(moment(joined).format('x'))
            if (joined > requestedDate) {
              self.feedCounter++
              self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
            }
          },
          changed: function (newDocument, oldDocument) {
            let joined = _.find(newDocument.condos, (condo) => { return condo.condoId == condoId }).joined
            joined = parseInt(moment(joined).format('x'))
            if (joined > requestedDate) {
              self.feedCounter++
              self.changed("feedNewPostsCounter", condoId, { newPosts: self.feedCounter })
            }
          },
        })
        this.onStop(() =>  {
          return handleIncident.stop() && handleForum.stop() && handleClassified.stop() && handleActu.stop() && handleUsersJoined.stop()
        })
        return self.ready()
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }

    })

    Meteor.publish('feedIncident', function (condoId, count, requestedDate) {
      if (Meteor.userId()) {
        let userId = Meteor.userId()
        const userRight = UsersRights.findOne({
          $and: [
            { "userId": userId },
            { "condoId": condoId }
          ]
        })
        if (userRight) {
          const residentRoleId = userRight.defaultRoleId
          let isOccupant = !!DefaultRoles.findOne({ _id: residentRoleId, name: 'Occupant' })

          let IncidentQuery = {
            condoId: condoId,
            createdAt: { $lte: +moment(requestedDate).format('x') },
            $or: [
              {
                "state.status": {
                  $in: [1, 2]
                }
              },
              {
                $or: [
                  {
                    $and: [
                      { "state.status": { $in: [0, 3] } },
                      { "declarer.userId": userId }
                    ]
                  }
                ]
              }
            ]
          }
          if (!isOccupant) {
            // don't touch ! Ask to me ! @vmariot
            let field = 'history.share.' + residentRoleId + '.state'
            IncidentQuery.$or[1].$or.push({
              $and: [
                { "state.status": 3 },
                { [field]: true }
              ]
            })
          }
          let incidentCursor = Incidents.find(IncidentQuery, { fields: {
            createdAt: 1,
            declarer: 1,
            condoId: 1,
            info: 1,
            lastUpdate: 1,
            state: 1,
            history: 1,
          }, sort: { lastUpdate: -1 }, limit: count })

          return incidentCursor
        } else {
          this.stop()
          throw new Meteor.Error(404, "Invalid user", "Not user right")
        }
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }
    })
    Meteor.publish('feedForum', function (condoId, count, requestedDate) {
      if (Meteor.userId()) {
        let self = this
        let forumCursor = ForumPosts.find({condoId, date: { $lte: requestedDate } }, { fields: {
          date: 1,
          editedAt: 1,
          condoId: 1,
          subject: 1,
          type: 1,
          updatedAt: 1,
          userId: 1,
          filesId: 1,
          inputs: 1,
          votes: 1,
          endDate: 1,
          commentCount: 1,
          viewsCount: 1,
          // views: 1, /* TO REMOVE */
        }, sort: { updatedAt: -1 }, limit: count })

        let forumViewsCursor = ForumPosts.find({ condoId, date: { $lte: requestedDate } }, { fields: { views: 1 } }, { sort: { updatedAt: -1 }, limit: count })
        let forumViewsHandler = forumViewsCursor.observeChanges({
          added: function (documentId, document) {
            self.added('forumViewsCounter', documentId, { counter: Object.keys(document.views).length })
          },
          changed: function (documentId, document) {
            self.changed('forumViewsCounter', documentId, { counter: Object.keys(document.views).length })
          },
          removed: function (documentId, document) {
            self.removed('forumViewsCounter', documentId)
          }
        })

        let forumHandler = forumCursor.observe({
          added: function (document) {
            self.added("forumFeed", document._id, document)
          },
          changed: function (newDocument, oldDocument) {
            self.changed("forumFeed", oldDocument._id, newDocument)
          },
          removed: function (oldDocument) {
            self.removed("forumFeed", oldDocument._id)
          },
        })

        this.onStop(() =>  {
          return forumHandler.stop() && forumViewsHandler.stop()
        })


        return self.ready()
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }
    })
    Meteor.publish('feedClassified', function (condoId, count, requestedDate) {
      if (Meteor.userId()) {
        let classifiedCursor = ClassifiedsAds.find({ condoId: condoId, createdAt: { $lte: requestedDate } }, { fields: {
          createdAt: 1,
          description: 1,
          editedAt: 1,
          condoId: 1,
          files: 1,
          price: 1,
          title: 1,
          updatedAt: 1,
          userId: 1,
          commentCount: 1,
          viewsCount: 1,
          // views: 1 /* TO REMOVE */
        }, sort: { updatedAt: -1 }, limit: count })

        let self = this
        let classifiedViewsCursor = ClassifiedsAds.find({ condoId: condoId, createdAt: { $lte: requestedDate } }, { fields: { views: 1 } }, { sort: { updatedAt: -1 }, limit: count })
        let classifiedViewsHandler = classifiedViewsCursor.observeChanges({
          added: function (documentId, document) {
            self.added('classifiedViewsCounter', documentId, { counter: Object.keys(document.views).length })
          },
          changed: function (documentId, document) {
            self.changed('classifiedViewsCounter', documentId, { counter: Object.keys(document.views).length })
          },
          removed: function (documentId, document) {
            self.removed('classifiedViewsCounter', documentId)
          }
        })

        this.onStop(() => {
          return classifiedViewsHandler.stop()
        })

        return classifiedCursor
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }
    })
    Meteor.publish('feedActu', function (condoId, count, requestedDate) {
      if (Meteor.userId()) {
        let actuCursor = ActuPosts.find({
          condoId: condoId,
          createdAt: { $lte: requestedDate },
          ...Meteor.call('getNewEventFilter', Meteor.userId())
        }, { fields: {
          createdAt: 1,
          description: 1,
          condoId: 1,
          endDate: 1,
          endHour: 1,
          files: 1,
          locate: 1,
          startDate: 1,
          startHour: 1,
          title: 1,
          allowComment: 1,
          allowAction: 1,
          participantResponse: 1,
          updatedAt: 1,
          userId: 1,
          commentCount: 1,
          viewsCount: 1
        }, sort: { createdAt: -1 }, limit: count })

        let self = this

        // views counter
        let actuCounterCursor = ActuPosts.find({
          condoId: condoId,
          createdAt: { $lte: requestedDate },
          ...Meteor.call('getNewEventFilter', Meteor.userId())
        }, { fields: { views: 1 } }, { sort: { createdAt: -1 }, limit: count })
        let actuCounterHandler = actuCounterCursor.observeChanges({
          added: function (documentId, document) {
            self.added('actuCounter', documentId, { views: Object.keys(document.views).length})
          },
          changed: function (documentId, document) {
            self.changed('actuCounter', documentId, { views: Object.keys(document.views).length})
          },
          removed: function (documentId, document) {
            self.removed('actuCounter', documentId)
          }
        })

        // comment counter
        if (EventCommentsFeed[condoId] === undefined || !EventCommentsFeed[condoId]) {
          name = "EventCommentsFeed" + condoId;
          EventCommentsFeed[condoId] = new Mongo.Collection(name);
        }
        let feed = EventCommentsFeed[condoId].find();
        let result = [];

        const handlerFeed = feed.observe({
          added: function (document) {
            if (!result[document.eventId]) {
              result[document.eventId] = 1;
              self.added("eventCommentsFeedCounter", document.eventId, {_id: document.eventId, counter: 1});
            }
            else {
              result[document.eventId] += 1;
              self.changed("eventCommentsFeedCounter", document.eventId, {_id: document.eventId, counter: result[document.eventId]});
            }
          },
          removed: function (oldDocument) {
            if (result[oldDocument.eventId]) {
              result[oldDocument.eventId] -= 1;
              if (result[oldDocument.eventId] < 1) {
                delete result[oldDocument.eventId];
                self.removed("eventCommentsFeedCounter", oldDocument.eventId);
              }
              else
                self.changed("eventCommentsFeedCounter", oldDocument.eventId, {_id: oldDocument.eventId, counter: result[oldDocument.eventId]});
            }
          },
        });

        this.onStop(() =>  {
          return actuCounterHandler.stop() && handlerFeed.stop()
        })

        return actuCursor
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }
    })
    Meteor.publish('feedUsersJoined', function (condoId, count, requestedDate) {
      if (Meteor.userId()) {
        let adminIds = []
        Meteor.users.find({ 'identities.adminId': { $exists: true } }).forEach(userAdmin => {
          adminIds.push(userAdmin._id)
        })
        let usersJoinedCursor = Residents.find({ "condos.condoId": condoId, userId: { $nin: adminIds } }, { fields: { "condos.joined": true, userId: true, "condos.condoId": true }, sort: { $natural: -1 }, limit: count })
        let self = this
        usersJoinedCursor.observe({
          added: function (document) {
            let joined = _.find(document.condos, (condo) => { return condo.condoId == condoId }).joined
            joined = parseInt(moment(joined).format('x'))
            if (joined <= requestedDate) {
              self.added('usersJoinedDate', document.userId, { joined: joined, condoId: condoId })
            }
          },
          changed: function (newDocument, oldDocument) {
            let joined = _.find(newDocument.condos, (condo) => { return condo.condoId == condoId }).joined
            joined = parseInt(moment(joined).format('x'))
            if (joined <= requestedDate) {
              self.changed('usersJoinedDate', newDocument.userId, { joined: joined, condoId: condoId })
            }
          },
          removed: function (oldDocument) {
            self.removed('usersJoinedDate', oldDocument.userId)
          }
        })
        self.ready()
      } else {
        this.stop()
        throw new Meteor.Error(404, "Invalid user", "Not logged")
      }
    })

    Meteor.publish('feedIncidentInformation', function(condoIds) {
      let incidentDetails = IncidentDetails.find({$or: [{condoId: {$in: condoIds}}, {condoId: "default"}]})
      if (incidentDetails.fetch() == undefined || incidentDetails.fetch()[0] == undefined) {
        incidentDetails = IncidentDetails.find({condoId: "default"})
      }

      let incidentType = IncidentType.find({$or: [{condoId: {$in: condoIds}}, {condoId: "default"}]})
      if (incidentType.fetch() == undefined || incidentType.fetch()[0] == undefined) {
        incidentType = IncidentType.find({condoId: "default"})
      }

      return [incidentDetails, incidentType]
    })
  }
)
