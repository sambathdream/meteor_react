Meteor.startup(function () {
	Meteor.methods({
		recentlyJoinedUsers: function(condoId) {
			let recentlyJoined = Residents.aggregate([
				{ $unwind: '$condos' },
				{ $match: { 'condos.condoId': condoId, 'condos.joined': { $exists: true } } },
				{ $project: { _id: 1, userId: 1, 'joined': '$condos.joined' } },
				{ $sort: { 'joined': -1 } },
				{ $limit: 5 }
			])
			return recentlyJoined
		},
		mostViewedForum: (condoId, limit = 3) => {
			let posts = ['asd'];

      posts = ForumPosts.aggregate([
        { $unwind: "$views" },
        { $match: { 'condoId': condoId } },
        { $project: { _id: 1, subject: 1, type: 1, editedAt: 1, views: { $size: { $objectToArray: "$views" } } } },
        { $sort: { views: -1 } },
        { $limit: limit }
      ])

			return posts;
		},
		mostViewedAds: function(condoId) {
			let mostViewedAds = ClassifiedsAds.aggregate([
				{ $unwind: "$views" },
				{ $match: { 'condoId': condoId } },
				{ $project: { _id: 1, title: 1, price: 1, editedAt: 1, views: { $size: { $objectToArray: "$views" } } } },
				{ $sort: { views: -1 } },
				{ $limit: 3 }
			])
			return mostViewedAds
		},
		getIncidentFiles: function(filesId) {
			let files = IncidentFiles.find({ _id: { $in: filesId } }).each()
			let retFiles = {}
			_.each(files, (elem) => {
				let versions = {}
				_.each(elem.versions, (elemV, key) => {
					versions[key] = elem.link(key)
				})
				retFiles[elem._id] = { parsedFile: true, type: elem.type, name: (elem.name || 'unknown'), versions, thumb: (elem.meta.thumb || null) }
			})
			return retFiles
		},
		getForumFiles: function(filesId) {
			let files = ForumFiles.find({ _id: { $in: filesId } }).each()
			let retFiles = {}
			_.each(files, (elem) => {
				let versions = {}
				_.each(elem.versions, (elemV, key) => {
					versions[key] = elem.link(key)
				})
				retFiles[elem._id] = { parsedFile: true, type: elem.type, name: (elem.name || 'unknown'), versions, thumb: (elem.meta.thumb || null) }
			})
			return retFiles
		},
		getClassifiedsFiles: function(filesId) {
			let files = UserFiles.find({ _id: { $in: filesId } }).each()
			let retFiles = {}
			_.each(files, (elem) => {
				let versions = {}
				_.each(elem.versions, (elemV, key) => {
					versions[key] = elem.link(key)
				})
				retFiles[elem._id] = { parsedFile: true, type: elem.type, name: (elem.name || 'unknown'), versions, thumb: (elem.meta.thumb || null) }
			})
			return retFiles
		},
		getActusFiles: function(filesId) {
			let files = UserFiles.find({ _id: { $in: filesId } }).each()
			let retFiles = {}
			_.each(files, (elem) => {
				let versions = {}
				_.each(elem.versions, (elemV, key) => {
					versions[key] = elem.link(key)
				})
				retFiles[elem._id] = { parsedFile: true, type: elem.type, name: (elem.name || 'unknown'), versions, thumb: (elem.meta.thumb || null) }
			})
			return retFiles
		},
		getFeedElements: function(condoId, params) {
			if (Meteor.userId()) {
				if (params.incident != undefined)
					params.incident.skip = params.incident.end - params.incident.start
				if (params.forum != undefined)
					params.forum.skip = params.forum.end - params.forum.start
				if (params.classified != undefined)
					params.classified.skip = params.classified.end - params.classified.start
				if (params.actu != undefined)
					params.actu.skip = params.actu.end - params.actu.start
				if (params.usersJoined != undefined)
					params.usersJoined.skip = params.usersJoined.end - params.usersJoined.start


				let returnedObject = {
					incident: null,
					forum: null,
					classified: null,
					actu: null,
					usersJoined: null
				}

				if (params.incident != undefined)
					returnedObject.incident = Incidents.find({
						condoId: condoId, $or: [
							{ "state.status": { $in: [1, 2] } },
							{ $and: [{ "state.status": { $in: [0, 3] }, "declarer.userId": this.userId }] }
						]
					}, { sort: { lastUpdate: -1 }, skip: params.incident.start, limit: params.incident.skip}).fetch()

				if (params.forum != undefined) {
					returnedObject.forum = ForumPosts.find({condoId}, {sort: {updatedAt: -1}, skip: params.forum.start, limit: params.forum.skip}).fetch()
				}

				if (params.classified != undefined)
					returnedObject.classified = ClassifiedsAds.find({condoId: condoId}, {sort: {updatedAt: -1}, skip: params.classified.start, limit: params.classified.skip}).fetch()

				if (params.actu != undefined)
					returnedObject.actu = ActuPosts.find({condoId: condoId}, {sort: {createdAt: -1}, skip: params.actu.start, limit: params.actu.skip}).fetch()

				if (params.usersJoined != undefined)
					returnedObject.usersJoined = Residents.find({"condos.condoId": condoId}, {fields: {"condos.joined": true, userId: true, "condos.condoId": true}, sort: {$natural: -1}, skip: params.usersJoined.start, limit: params.usersJoined.skip}).fetch()

				if (returnedObject.incident == undefined || returnedObject.incident.length == 0 || returnedObject.incident == null)
					delete returnedObject.incident;
				if (returnedObject.forum == undefined || returnedObject.forum.length == 0 || returnedObject.forum == null)
					delete returnedObject.forum;
				if (returnedObject.classified == undefined || returnedObject.classified.length == 0 || returnedObject.classified == null)
					delete returnedObject.classified;
				if (returnedObject.actu == undefined || returnedObject.actu.length == 0 || returnedObject.actu == null)
					delete returnedObject.actu;
				if (returnedObject.usersJoined == undefined || returnedObject.usersJoined.length == 0 || returnedObject.usersJoined == null)
					delete returnedObject.usersJoined;
				else {
					_.each(returnedObject.usersJoined, function(user, index) {
						returnedObject.usersJoined[index].joined = _.find(user.condos, function(elem) { return elem.condoId == condoId }).joined;
						delete returnedObject.usersJoined[index].condos
					})
				}
				return (returnedObject)
			}
			else
				throw new Meteor.Error(404, "Invalid user", "Not logged");
		},
	});
});
