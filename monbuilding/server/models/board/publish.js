Meteor.startup(
	function () {
		/* GLOBAL PUBLISH FOR RESIDENT BOARD (CONDOS, BUILDINGS, GESTIONNAIRE, RESIDENTUSERS FAKE TABLE)*/
		Meteor.publish('board_resident', function () {
			if (this.userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let resident = Residents.findOne({userId: this.userId},
				{fields: {userId: true, condos: true}});
			if (!resident)
				throw new Meteor.Error(301, "Access denied", "User dont have 'resident' identity");
			/*   IDs OF CONDOS AND BUILDINGS OF CURRENT USER*/
			let condosIds = _.map(resident.condos, function(c){return c.condoId});
			let buildingsIds = [];
			for (c of resident.condos) {
				buildingsIds = buildingsIds.concat(_.map(c.buildings, function(b) {return b.buildingId}));
			}
			/*      FIND CONDOS, BUILDINGS AND GESTIONNAIRE*/
			let rCondos = Condos.find({_id: {$in: condosIds}});

			let rBuildings = Buildings.find({_id: {$in: buildingsIds}});
			let rEnterprises = Enterprises.find({condos: {$in: condosIds}},
				{name: true, condos: true});

			/*      AGGREGATION BETWEEN USERS AND RESIDENT*/
			/*      CREATE A FAKE TABLE (CLIENT ONLY COLLECTION : "residentUsers") WITH USERS IN CONDOS OF CURRENT USER*/
			/*      WITH FIELD "resident" IN RELATION WITH RESIDENT COLLECTION*/

			let aggUsers;
			let aggResidents;
			let aggPendingResidents;
			let userIds = [];
			let self = this;
      aggResidents = Residents.find({"condos.condoId": {$in: condosIds}});
      aggPendingResidents = Residents.find({"pendings.condoId": {$in: condosIds}});
      const handlerResident = letsObserveChanges(aggResidents, self);
      const handlerPending = letsObserveChangesPending(aggPendingResidents, self);

      condoContact = CondoContact.find({condoId: {$in: condosIds}});
      declarationDetails = DeclarationDetails.find();

      let occupantRolesIds = _.map(DefaultRoles.find({for: 'occupant'}).fetch(), '_id')

      const adminId = Meteor.users.findOne({ 'identities.adminId': { $exists: true } })._id
      let userRights = UsersRights.find({ defaultRoleId: { $in: occupantRolesIds }, condoId: { $in: condosIds }, userId: { $nin: [adminId] } });

      this.onStop(() => {
        return handlerResident.stop() && handlerPending.stop()
      })
      return [declarationDetails, condoContact, CondoPhotos.find({condoId: {$in: condosIds}}), CondoLogos.find({condoId: {$in: condosIds}}), rCondos, rBuildings, rEnterprises, userRights, CondosModulesOptions.find({ condoId: { $in: condosIds } }), BuildingStatus.find(), CompanyName.find()];

			function letsObserveChanges(aggResidents, self) {
				return aggResidents.observeChanges({
					added: function(id) {
            let users = Meteor.users.findOne({"identities.residentId": id});
            if (users && !users.identities.adminId) {
              users.resident = Residents.findOne(id, {fields: {pendings: false}});
              self.added("residentUsers", users._id, users);
            }
					},
					changed: function(id) {
						let users = Meteor.users.findOne({"identities.residentId": id});
            if (users && !users.identities.adminId) {
              users.resident = Residents.findOne(id, {fields: {pendings: false}});
              self.changed("residentUsers", users._id, users);
            }
					},
					removed: function(id) {
						let users = Meteor.users.findOne({"identities.residentId": id});
            if (!users || (users && !users.identities) || (users && !users.identities.adminId)) {
              self.removed("residentUsers", users._id);
            }
					}
				});
			}
      function letsObserveChangesPending(aggResidents, self) {
				return aggResidents.observeChanges({
					added: function(id) {
            let users = Meteor.users.findOne({"identities.residentId": id});
            if (users && !users.identities.adminId) {
              users.resident = Residents.findOne(id, {fields: {condos: false}});
              self.added("residentPendingUsers", users._id, users);
            }
					},
					changed: function(id) {
						let users = Meteor.users.findOne({"identities.residentId": id});
            if (users && !users.identities.adminId) {
              users.resident = Residents.findOne(id, { fields: { condos: false }});
              self.changed("residentPendingUsers", users._id, users);
            }
					},
					removed: function(id) {
						let users = Meteor.users.findOne({"identities.residentId": id});
            if (users && (!users || !users.identities || !users.identities.adminId)) {
              self.removed("residentPendingUsers", users._id);
            }
					}
				});
			}
		});


		Meteor.publish('condodocuments', function () {
			if (this.userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let documentIds = []
			let condoIds = []
			if (Meteor.user() && Meteor.user().identities.gestionnaireId !== undefined) {
				enterpriseId = Meteor.user().identities.gestionnaireId
				let enterprise = Enterprises.findOne(enterpriseId)
				let thisUser = _.find(enterprise.users, (elem) => {
					return elem.userId === this.userId
				})
				if (thisUser !== undefined) {
					condoIds = _.map(thisUser.condosInCharge, 'condoId')
					if (condoIds !== undefined) {
						let condos = Condos.find({_id: {$in: condoIds}}).fetch()
						_.each(condos, (condo) => {
							if (condo.map !== undefined && condo.map.type === "file") {
								documentIds.push(condo.map.data)
							}
							if (condo.manual !== undefined && condo.manual.type === "file") {
								documentIds.push(condo.manual.data)
							}
						})
					}
				}
			} else if (Meteor.user() && Meteor.user().identities.residentId !== undefined) {
				let resident = Residents.findOne({userId: this.userId})
				if (resident !== undefined) {
					condoIds = _.map(resident.condos, 'condoId')
					if (condoIds !== undefined) {
						let condos = Condos.find({ _id: { $in: condoIds } }).fetch()
						_.each(condos, (condo) => {
							if (condo.map !== undefined && condo.map.type === "file") {
								documentIds.push(condo.map.data)
							}
							if (condo.manual !== undefined && condo.manual.type === "file") {
								documentIds.push(condo.manual.data)
							}
						})
					}
				}
			}
			if (condoIds === undefined) {
				condoIds = []
			}
			return CondoDocuments.find({$or: [{_id: {$in: documentIds}}, {"meta.condoId": {$in: condoIds}}]}).cursor;
		});

		Meteor.publish('avatarLinks', function (condoId) {
			let __DEBUG = false;
			if (__DEBUG == true) console.log("First command, condoId => ", condoId, " , userId => ", Meteor.userId());
			if (!Meteor.call("isResidentOfCondo", Meteor.userId(), condoId))
				return false;
			if (__DEBUG == true) console.log("Second, passed isResidentOfCondo");

			let residentsCursor = Residents.find({
        $or: [
          { "condos.condoId": condoId },
          { "pendings.condoId": condoId }
        ]
			});

			let enterprise = Enterprises.find({
				"condos": condoId
			});

			let self = this;
			if (__DEBUG == true) console.log("Third, passed self=this");
			let unregisteredGestionnaire = GestionnaireUnregistered.find({condoId: condoId}).fetch();
			_.each(unregisteredGestionnaire, function(gestionnaire) {
				if (gestionnaire.pictureId) {
					let uf = UserFiles.findOne(gestionnaire.pictureId);
					if (uf)
          self.added("avatars", gestionnaire._id, {
            userId: gestionnaire._id,
            fileId: gestionnaire.pictureId,
            avatar: {
              original: uf.link(),
              large: uf.link("large"),
              thumbnail40: uf.link("thumbnail40"),
              thumb100: uf.link("thumb100"),
              avatar: uf.link("avatar"),
              preview: uf.link("preview"),
            },
            from: "unregisteredGestionnaire"
          });
				}
			})

			const handlerResident = residentsCursor.observe({
				added: function (resident) {
					if (resident.fileId != undefined && resident.fileId != "")
					{
						let uf = UserFiles.findOne(resident.fileId);
						if (uf)
							self.added("avatars", resident.userId, {
								userId: resident.userId,
								fileId: resident.fileId,
								avatar: {
									original: uf.link(),
									large: uf.link("large"),
									thumbnail40: uf.link("thumbnail40"),
									thumb100: uf.link("thumb100"),
									avatar: uf.link("avatar"),
									preview: uf.link("preview"),
								},
								from: "residents"
							});
					}
				},
				changed: function (resident, oldResident) {
					if (resident.fileId != undefined && resident.fileId != "")
					{
						let uf = UserFiles.findOne(resident.fileId);
						if (uf)
						{
							try {
								self.changed("avatars", oldResident.userId, {
									userId: oldResident.userId,
									fileId: resident.fileId,
									avatar: {
										original: uf.link(),
										large: uf.link("large"),
										thumbnail40: uf.link("thumbnail40"),
										thumb100: uf.link("thumb100"),
										avatar: uf.link("avatar"),
										preview: uf.link("preview"),
									}
								});
							} catch (e) {
								this.added(resident);
							}
						}
						else {
							self.removed("avatars", oldResident.userId);
						}
					}
					else {
						try {
							self.removed("avatars", oldResident.userId);
						} catch(e) {
						}
					}
				}
			});

			const handlerEnterprise = enterprise.observe({
				added: function (enterprise) {
					_.each(enterprise.users, function(val){
						if (_.find(val.condosInCharge, function(condo){
							if (condo.condoId == condoId)
								return true;
						}) != undefined) {
							if (val.profile.fileId != undefined && val.profile.fileId != "")
							{
								let uf = UserFiles.findOne(val.profile.fileId);
								if (uf)
									self.added("avatars", val.userId, {
										userId: val.userId,
										fileId: val.profile.fileId,
										avatar: {
											original: uf.link(),
											large: uf.link("large"),
											thumbnail40: uf.link("thumbnail40"),
											thumb100: uf.link("thumb100"),
											avatar: uf.link("avatar"),
											preview: uf.link("preview"),
										},
										from: "enterprise"
									});
							}
						}
					});
				},
				changed: function (enterprise, oldEnterprise) {
					let subthis = this;
					_.each(enterprise.users, function(val, key){
						if (_.find(val.condosInCharge, function(condo){
							if (condo.condoId == condoId)
								return true;
						}) != undefined) {
							if (val.profile.fileId != undefined && val.profile.fileId != "")
							{
								let uf = UserFiles.findOne(val.profile.fileId);
								if (uf)
								{
									try {
										self.changed("avatars", val.userId, {
											userId: val.userId,
											fileId: val.profile.fileId,
											avatar: {
												original: uf.link(),
												large: uf.link("large"),
												thumbnail40: uf.link("thumbnail40"),
												thumb100: uf.link("thumb100"),
												avatar: uf.link("avatar"),
												preview: uf.link("preview"),
											}
										});
									} catch (e) {
										subthis.added(enterprise);
									}
								}
								else {
									try {self.removed("avatars", oldEnterprise.users[key].userId);} catch (e) {}
								}
							}
							else {
								try {self.removed("avatars", oldEnterprise.users[key].userId);} catch (e) {}
							}
						}
					});
				}
      });
      this.onStop(() => {
        return handlerResident.stop() && handlerEnterprise.stop()
      })
			this.ready();
		});
	}
);
