Meteor.startup(function () {
	Meteor.methods({
	  getCondoLogo: function (logoId) {
			if (!Meteor.userId)
				return;
			let file = UserFiles.findOne({"_id": logoId});
			if (file) {
				// console.log(file.link());
				return file.link();
			}
	  }
	});
});