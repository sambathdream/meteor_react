import json2csv from 'json2csv'
import mkdirp from 'mkdirp'
import archiver from 'archiver'
import fs from 'fs'
import { IncidentsCSV } from './stats/incidents.stats'
import { ResourcesUseCSV } from './stats/resourcesUse.stats'
import { BookedResourcesCSV } from './stats/bookedResources.stats'
import { OccupancyRateCSV } from './stats/occupancyRate.stats'
import { BookedByTimeCSV } from './stats/bookedByTime.stats'
import { ResourcesDistributionCSV } from './stats/resourcesDistribution.stats'
import { MessengerCSV } from './stats/messenger.stats'
import { UtilizationCSV } from './stats/utilization.stats'
import { TotalUsersAccessTypeCSV } from './stats/totalUsersAccessType.stats'
import { TotalUsersSectionCSV } from './stats/totalUsersSection.stats'
import { ComparisonCSV } from './stats/comparison.stats'
import { SatisfactionCSV } from './stats/satisfaction.stats'
import { _, Object } from 'core-js';
import { listen } from 'soap';

function createAnalyticsLogin(params) {
  let tolerance = Date.now() - (1000 * 30);
  let analytic = Analytics.findOne({ $and: [{ userId: params.userId }, { type: "login" }, { $or: [{ logoutDate: null }, { logoutDate: { $gte: tolerance } }] }, { accessType: params.accessType }, { condoId: params.condoId }] }, { sort: { $natural: -1 } });
  // console.log("tolerance: ", tolerance.toLocaleString())
  // console.log("analytic: ", analytic)

  if (analytic && ((Date.now() - analytic.loginDate) / 1000) > 86400) { /* 1 day */
    // console.log("Not been logged for more than 1 day")
    // console.log("analytic: ", analytic)
    Analytics.update({ _id: analytic._id }, { $set: { 'logoutDate': Date.now() } });
    analytic = undefined;
  }
  if (!analytic || analytic == undefined) {
    // console.log("Never been logged")
    Analytics.insert({
      userId: params.userId,
      condoId: params.condoId,
      type: "login",
      accessType: params.accessType,
      loginDate: Date.now(),
      logStatus: {
        loggedTime: 0,
        idleTime: 0
      },
      logoutDate: null,
    })
  }
  else if (analytic && analytic.logoutDate != null) {
    // console.log("Have already been logged today!")
    // console.log("analytic: ", analytic)
    Analytics.update({ _id: analytic._id }, { $set: { 'logoutDate': null } });
  }
}

function updateAnalyticsLogin(params) {
  // console.log("$$$$$$$$$$$$\nupdate analytics: ", params)
  let analytic = Analytics.findOne({ $and: [{ userId: params.userId }, { type: "login" }, { logoutDate: null }, { accessType: params.accessType }, { condoId: params.condoId }] }, { sort: { $natural: -1 } });
  if (analytic && analytic != undefined && params.status == "idle") {
    let loggedTime = (Date.now() - analytic.loginDate) - analytic.logStatus.idleTime;
    // console.log("Logged time, now enter idle mode: ", loggedTime)
    Analytics.update({ _id: analytic._id }, { $set: { 'logStatus.loggedTime': loggedTime, lastStatus: 'idle' } });
  }
  else if (analytic && analytic != undefined && params.status == "active") {
    let idleTime = (Date.now() - analytic.loginDate) - analytic.logStatus.loggedTime;
    // console.log("Idle time, now enter logged mode: ", idleTime)
    Analytics.update({ _id: analytic._id }, { $set: { 'logStatus.idleTime': idleTime, lastStatus: 'active' } });
  }
  else if (analytic && analytic != undefined && params.status == "logout") {
    // console.log("------------\nSorry gonna logout...")
    // This list is not exhaustive
    // let modules = ["concierge", "manual", "ads", "info", "resa"];
    // modules.forEach(function(e) {
    // })
    const currentDate = Date.now()
    Analytics.find({ $and: [{ userId: params.userId }, { type: "module" }, { module: params.module }, { submodule: params.submodule }, { endDate: null }, { accessType: params.accessType }, { condoId: params.condoId }] }, { sort: { $natural: -1 } }).forEach(el => {
      // console.log("############\nanalytic element:\n", el)
      // console.log("current date: ", currentDate)
      let loggedTime = (currentDate - el.startDate)
      Analytics.update({ _id: el._id }, { $set: { 'loggedTime': loggedTime, endDate: currentDate } });
      // console.log("############\n")
    })

    if (analytic.lastStatus == undefined || analytic.lastStatus == 'idle') {
      // console.log("analytic: ", analytic)
      let idleTime = (Date.now() - analytic.loginDate) - analytic.logStatus.loggedTime;
      // console.log("Idle time, now enter logged mode: ", idleTime)
      Analytics.update({ _id: analytic._id }, { $set: { 'logStatus.idleTime': idleTime, logoutDate: Date.now() } });
    }
    else if (analytic.lastStatus == 'active') {
      // console.log("analytic: ", analytic)
      let loggedTime = (Date.now() - analytic.loginDate) - analytic.logStatus.idleTime;
      // console.log("Logged time, now enter idle mode: ", loggedTime)
      Analytics.update({ _id: analytic._id }, { $set: { 'logStatus.loggedTime': loggedTime, logoutDate: Date.now() } });
    }
  }
}

function createAnalyticsModule(params) {
  Analytics.find({
    userId: params.userId,
    type: "module",
    endDate: null
  }).fetch().forEach(user => {
    // console.log("user: ", user)
    Meteor.call('updateAnalytics', { type: user.type, module: user.module, accessType: user.accessType, condoId: user.condoId })
  })

  Analytics.insert({
    userId: params.userId,
    condoId: params.condoId,
    type: "module",
    module: params.module,
    submodule: params.submodule || null,
    accessType: params.accessType,
    startDate: Date.now(),
    loggedTime: 0,
    endDate: null,
  })
}

function updateAnalyticsModule(params) {
  // console.log("************\nupdate analytics: ", params)
  let analytic = Analytics.findOne({ $and: [{ userId: params.userId }, { type: "module" }, { module: params.module }, { submodule: params.submodule }, { endDate: null }, { accessType: params.accessType }, { condoId: params.condoId }] }, { sort: { $natural: -1 } });
  // console.log("analytics: ", analytic)
  if (analytic && analytic != undefined) {
    let loggedTime = (Date.now() - analytic.startDate);
    Analytics.update({ _id: analytic._id }, { $set: { 'loggedTime': loggedTime, endDate: Date.now() } });
  }
}

function createAnalyticsDownload(params) {
  let analytic = Analytics.findOne({
    $and: [
      { type: "download" },
      { store: params.store },
      { date: moment(params.date, "DD-MM-YYYY").utc().valueOf() },
    ]
  });
  if (!analytic || analytic == undefined) {
    Analytics.insert({
      type: "download",
      store: params.store,
      date: moment(params.date, "DD-MM-YYYY").utc().valueOf(),
      count: parseInt(params.count),
    })
  } else {
    Analytics.update({ _id: analytic._id }, { $set: { 'count': parseInt(params.count) } });
  }
}

createStatsZip = (files) => {
  return new Promise((resolve, reject) => {
    const zipId = getRandomString()
    const currentTime = moment().format('YYYY-MM-DD')

    const _zipRef = {
      _id: zipId,
      name: 'monbuilding-stats' + '-' + zipId + '-' + currentTime + '.zip',
      path: 'assets/app/downloads/StatsCSV/' + 'monbuilding-stats' + '-' + zipId + '-' + currentTime + '.zip',
      meta: {
        zip: '/download/StatsCSV/' + zipId + '/original/' + 'monbuilding-stats' + '-' + zipId + '-' + currentTime + '.zip'
      }
    }

    let output = fs.createWriteStream(_zipRef.path)
    let archive = archiver('zip', {
      zlib: { level: 9 }
    })

    output.on('close', () => {
      console.log(archive.pointer() + ' total bytes');
      console.log('archiver has been finalized and the output file descriptor has closed.');

      StatsCSV.addFile(_zipRef.path, {
        fileName: _zipRef.name,
        type: 'application/zip',
        fileId: _zipRef._id,
        meta: {}
      }, (err, zipRef) => {
        if (err) throw err
        eval(StatsCSV).collection.update({
          _id: zipRef._id
        }, {
            $set: {
              'meta.zip': '/download/StatsCSV/' + zipRef._id + '/original/' + zipRef.name
            }
          })
        resolve(_zipRef.meta)
      })
    })

    output.on('end', () => {
      console.log('Data has been drained');
    })

    archive.on('warning', err => {
      if (err.code === 'ENOENT') {
        // log warning
      } else {
        throw err;
      }
    })

    archive.on('error', err => {
      throw err;
    })

    archive.pipe(output)

    files.map(f => {
      archive.file(f.path, { name: f.name })
    })

    archive.finalize()
  })
}

jsonToCSVStats = (statName, opts, data, condoName) => {
  opts.fields = Object.keys(data[0])

  const parser = new json2csv.Parser(opts)
  const csv = parser.parse(data)
  const fileId = getRandomString()
  const currentTime = moment().format('YYYY-MM-DD')

  const _fileRef = {
    name: condoName + '-' + statName + '-' + fileId +  '-' + currentTime + '.csv',
    path: 'assets/app/downloads/StatsCSV/' + condoName + '-' + statName + '-' + fileId +  '-' + currentTime + '.csv',
  }

  return new Promise((resolve, reject) => {
    mkdirp('assets/app/downloads/StatsCSV', err => {
      if (err) {
        if (err.code !== 'EEXIST') throw err
        fs.writeFile(_fileRef.path, csv, err => {
          if (err) throw err
          resolve(_fileRef)
        })
      } else {
        fs.writeFile(_fileRef.path, csv, err => {
          if (err) throw err
          resolve(_fileRef)
        })
      }
    })
  })
}

createAllCSV = (params, fields) => {
  const condoName = getCondoName(params.condoId)

  return new Promise(async (resolve, reject) => {
    let files = []

    for (var stat in params.stats) {
      switch (stat) {
        case 'incidents':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            IncidentsCSV.getIncidentsStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'resourcesUse':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            ResourcesUseCSV.getResourcesUseStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'bookedResources':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            BookedResourcesCSV.getBookedResourcesStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'occupancyRate':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            OccupancyRateCSV.getOccupancyRateStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'bookedByTime':
          files.push(await jsonToCSVStats('bookedByDate',
            { fields: fields[stat + 'Fields'] },
            BookedByTimeCSV.getBookedByTimeStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            BookedByTimeCSV.getBookedByTimeStats(params.startDate, params.endDate, params.condoId, params.lang, true),
            condoName))
          break;
        case 'resourcesDistribution':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            ResourcesDistributionCSV.getResourcesDistributionStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            ResourcesDistributionCSV.getResourcesDistributionStats(params.startDate, params.endDate, params.condoId, params.lang, true),
            condoName))
          break;
        case 'messenger':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            MessengerCSV.getMessengerStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'utilization':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            UtilizationCSV.getUtilizationStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'totalUsersAccessType':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            TotalUsersAccessTypeCSV.getTotalUsersAccessTypeStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'totalUsersSection':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            TotalUsersSectionCSV.getTotalUsersSectionStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'comparison':
          files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            ComparisonCSV.getComparisonStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
          break;
        case 'satisfaction':
            files.push(await jsonToCSVStats(stat,
            { fields: fields[stat + 'Fields'] },
            SatisfactionCSV.getSatisfactionStats(params.startDate, params.endDate, params.condoId, params.lang),
            condoName))
        default:
          break;
      }
    }
    resolve(files)
  })
}

getCondoName = (condoId) => {
  if (condoId === 'all') {
    return condoId
  }

  const condo = Condos.findOne(condoId);
  if (condo) {
    if (condo.info.id != -1) {
      return condo.info.id + ' - ' + condo.getName();
    } else {
      return condo.getName();
    }
  }
}

Meteor.startup(function () {
  Meteor.methods({
    setNewAnalytics: function (params) {
      if (!params.userId)
        params.userId = Meteor.userId();
      let user = Meteor.users.findOne(params.userId);

      if (user && user != undefined && params.type == "login") {
        createAnalyticsLogin(params)
      }

      else if (user && user != undefined && params.type == "module") {
        createAnalyticsModule(params)
      }

      else if (user && user != undefined && params.type == "download") {
        createAnalyticsDownload(params)
      }
    },
    updateAnalytics: function (params) {
      if (!params.userId)
        params.userId = Meteor.userId();

      let user = Meteor.users.findOne(params.userId);

      if (user && user != undefined && params.type == "login") {
        updateAnalyticsLogin(params)
      }
      else if (user && user != undefined && params.type == "module") {
        updateAnalyticsModule(params)
      }
    },
    exportToCSV: async function (params) {
      console.log('is in function to export csv')
      const fields = {
        incidentsFields: [],
        resourcesUseFields: [],
        bookedResourcesFields: [],
        occupancyRateFields: [],
        bookedByTimeFields: [],
        resourcesDistributionFields: [],
        messengerFields: [],
        utilizationFields: [],
        totalUsersAccessTypeFields: [],
        totalUsersSectionFields: [],
        comparisonFields: [],
        satisfactionFields: []
      }

      try {
        StatsCSV.remove({})

        const files = await createAllCSV(params, fields)

        if (files.length > 0) {
          return await createStatsZip(files)
        }
      } catch (error) {
        throw error
      }
    }
  });
});
