getBookedByTimeResourceTypes = (condoId, lang) => {
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId } )

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]

  let resources = attachResourceType(Resources.find({condoId: {$in: condos}}).fetch(), lang)

  const data = {};
  let resourceIds = [];

  _.each(resources, (res) => {
      if (!data[res.type]) {
          data[res.type] = {
              name: res.typeName,
              ids: [res._id]
          }
      } else {
          data[res.type].ids.push(res._id)
      }
      resourceIds.push(res._id)
  });

  return {data, condos, resourceIds}
}

prepareBookedByTimeData = (condoId, lang) => {
  const resourcesType = getBookedByTimeResourceTypes(condoId, lang)
  return {
    types: _.map(resourcesType.data, (d, i) => {
        return {
          type: i,
          ...d
        }
    }),
    condos: resourcesType.condos,
    resourceIds: resourcesType.resourceIds
  }
}

setDefaultBookedByTimeData = o => {
  return {
      ids: o.ids,
      label: o.name,
      data: [],
  }
}

dayBookedByTimeData = (book, dataset, types, start, end, lang, condosData) => {
  const date = [];
  let data = []

  let duration = end.diff(start, 'days')
  for (let i = 0; i < duration; i++) {
      dataset = _.map(dataset, (d) => {
          const resourceBook = _.filter(book, b => {
              return _.includes(d.ids, b.resourceId);
          });

          const tmpData = {}
          condosData.forEach(cd => {
            tmpData[cd.condoName] = 0
          })

          const count = _.countBy(
            _.flattenDeep([
              _.map(resourceBook, function(book) {
                if (moment(book.start).locale(lang).format("DD[/]MM[/]YY") === start.locale(lang).format("DD[/]MM[/]YY")) {
                  tmpData[condosData.find(
                    condo => condo.condoId === book.condoId
                  ).condoName] += 1
                }
                return moment(book.start).locale(lang).format("DD[/]MM[/]YY");
              })
            ])
          )[start.locale(lang).format("DD[/]MM[/]YY")];

          const newData = d.data
          newData.push({
            type: d.label,
            date: start.locale(lang).format('ddd DD MMM'),
            count: !!count ? count : 0,
            tmpData,
          })
          return {
              ...d,
              data: newData
          }
      })

      date.push(start.locale(lang).format('ddd DD MMM'));
      start.add(1, "d");
  }

  date.forEach(dt => {
    types.forEach(t => {
      let pushData = {}
      pushData['Date'] = dt
      pushData['Type'] = t.name
      const nData = dataset.find(d => d.label === t.name).data.find(d => d.date === dt)
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = nData.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = nData.count
      data.push(pushData)
    })
  })

  return data
}

weekBookedByTimeData = (book, dataset, types, start, end, lang, condosData) => {
  const date = []
  let data = []

  const tr_common = new CommonTranslation(lang)

  let duration = end.diff(start, 'weeks')
  for (let i = 0; i <= duration; i++) {
      let tmpDate = start.startOf('weeks').clone();
      dataset = _.map(dataset, (d) => {
          const resourceBook = _.filter(book, function(b) {
              return _.includes(d.ids, b.resourceId);
          });

          const tmpData = {}
          condosData.forEach(cd => {
            tmpData[cd.condoName] = 0
          })

          const count = _.countBy(
            _.flattenDeep([
              _.map(resourceBook, function(book) {
                if (moment(book.start).locale(lang).format("W YYYY") === start.locale(lang).format("W YYYY")) {
                  tmpData[condosData.find(
                    condo => condo.condoId === book.condoId
                  ).condoName] += 1
                }
                return moment(book.start).locale(lang).format("W YYYY");
              })
            ])
          )[start.locale(lang).format("W YYYY")];

          const newData = d.data;
          newData.push({
            type: d.label,
            date: tr_common.commonTranslation["week"] + ' ' + start.locale(lang).format('W') + ' ' + (!(start.startOf('weeks').locale(lang).format('MM[/]YYYY') == tmpDate.add(1, "weeks").locale(lang).format('MM[/]YYYY')) ? (start.startOf('weeks').locale(lang).format('DD MMM')  + ' ' + start.endOf('week').locale(lang).format('DD MMM')) : (start.startOf('weeks').locale(lang).format('DD')+'-'+start.endOf('week').locale(lang).format('DD MMM'))),
            count: !!count ? count : 0,
            tmpData,
          })

          return {
              ...d,
              data: newData
          }
      })

      date.push(tr_common.commonTranslation["week"] + ' ' + start.locale(lang).format('W') + ' ' + (!(start.startOf('weeks').locale(lang).format('MM[/]YYYY') == tmpDate.add(1, "weeks").locale(lang).format('MM[/]YYYY')) ? (start.startOf('weeks').locale(lang).format('DD MMM')  + ' ' + start.endOf('week').locale(lang).format('DD MMM')) : (start.startOf('weeks').locale(lang).format('DD')+'-'+start.endOf('week').locale(lang).format('DD MMM'))));
      start.add(1, "weeks");
  }

  date.forEach(dt => {
    types.forEach(t => {
      let pushData = {}
      pushData['Date'] = dt
      pushData['Type'] = t.name
      const nData = dataset.find(d => d.label === t.name).data.find(d => d.date === dt)
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = nData.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = nData.count
      data.push(pushData)
    })
  })

  return data
};

monthBookedByTimeData = (book, dataset, types, start, end, lang, condosData) => {
  const date = [];
  let data = []

  let duration = end.diff(start, 'month')
  for (let i = 0; i <= duration; i++) {
      dataset = _.map(dataset, (d) => {
          const resourceBook = _.filter(book, b => {
              return _.includes(d.ids, b.resourceId);
          });

          const tmpData = {}
          condosData.forEach(cd => {
            tmpData[cd.condoName] = 0
          })

          const count = _.countBy(
            _.flattenDeep([
              _.map(resourceBook, book => {
                if (moment(book.start).locale(lang).format("MM[/]YYYY") === start.locale(lang).format("MM[/]YYYY")) {
                  tmpData[condosData.find(
                    condo => condo.condoId === book.condoId
                  ).condoName] += 1
                }
                return moment(book.start).locale(lang).format("MM[/]YYYY")
              })
            ])
          )[start.locale(lang).format("MM[/]YYYY")];

          const newData = d.data;
          newData.push({
            type: d.label,
            date: !(start.locale(lang).format('YYYY') === moment().locale(lang).format('YYYY')) ? start.locale(lang).format('MMMM YYYY') :  start.locale(lang).format('MMMM'),
            count: !!count ? count : 0,
            tmpData,
          })

          return {
              ...d,
              data: newData
          }
      })

      date.push(!(start.locale(lang).format('YYYY') === moment().locale(lang).format('YYYY')) ? start.locale(lang).format('MMMM YYYY') :  start.locale(lang).format('MMMM'));
      start.add(1, "M");
  }

  date.forEach(dt => {
    types.forEach(t => {
      let pushData = {}
      pushData['Date'] = dt
      pushData['Type'] = t.name
      const nData = dataset.find(d => d.label === t.name).data.find(d => d.date === dt)
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = nData.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = nData.count
      data.push(pushData)
    })
  })

  return data
}

yearBookedByTimeData = (book, dataset, types, start, end, lang, condosData) => {
  const date = [];
  let data = []

  let duration = end.diff(start, 'year')
  for (let i = 0; i <= duration; i++) {
      dataset = _.map(dataset, (d) => {
          const resourceBook = _.filter(book, b => {
              return _.includes(d.ids, b.resourceId);
          });

          const tmpData = {}
          condosData.forEach(cd => {
            tmpData[cd.condoName] = 0
          })

          const count = _.countBy(
            _.flattenDeep([
              _.map(resourceBook, function(book) {
                if (moment(book.start).locale(lang).format("YYYY") === start.locale(lang).format("YYYY")) {
                  tmpData[condosData.find(
                    condo => condo.condoId === book.condoId
                  ).condoName] += 1
                }
                return moment(book.start).locale(lang).format("YYYY");
              })
            ])
          )[start.locale(lang).format("YYYY")];

          const newData = d.data;
          newData.push({
            type: d.label,
            date: start.locale(lang).format("YYYY"),
            count: !!count ? count : 0,
            tmpData
          })

          return {
              ...d,
              data: newData
          }
      })

      date.push(start.locale(lang).format("YYYY"));
      start.add(1, "y");
  }

  date.forEach(dt => {
    types.forEach(t => {
      let pushData = {}
      pushData['Date'] = dt
      pushData['Type'] = t.name
      const nData = dataset.find(d => d.label === t.name).data.find(d => d.date === dt)
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = nData.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = nData.count
      data.push(pushData)
    })
  })

  return data
}

getBookedByTimeCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getBookedByTime = (start, end, typeFilter, lang, timeType) => {
  // console.log('typeFilter ', typeFilter )
  const condosData = getBookedByTimeCondoName(typeFilter.condos)
  // console.log('condosData ', condosData )
  let dataset = typeFilter.types.map(o => {
    return setDefaultBookedByTimeData(o)
  })
  // console.log('dataset ', dataset )

  let res = []
  if (timeType === 'date') {
      // DATE TYPE
      let book = Reservations.find({
        $and: [
            {resourceId: {$in: typeFilter.resourceIds}},
            {start: {$gte: +start.locale(lang).format('x')}},
            {start: {$lte: +end.locale(lang).format('x')}},
            {condoId: {$in: typeFilter.condos}},
            {
                $or: [
                    {pending: { $exists: false }},
                    {pending: false}
                ]
            },
            {
                $or: [
                    {rejected: { $exists: false }},
                    {rejected: false}
                ]
            }
        ]
    }).fetch()
      // console.log('book.length ', book.length )

      if (end.diff(start, 'year') > 1) {
        res =  yearBookedByTimeData(book, dataset, typeFilter.types, start, end, lang, condosData)
      }
      else if (end.diff(start, 'month') > 2) {
        res = monthBookedByTimeData(book, dataset, typeFilter.types, start, end, lang, condosData)
      }
      else if (end.diff(start, 'days') > 31) {
        res = weekBookedByTimeData(book, dataset, typeFilter.types, start, end, lang, condosData)
      }
      else {
        res = dayBookedByTimeData(book, dataset, typeFilter.types, start, end, lang, condosData)
      }
  } else {
    // TIME TYPE
    const label = ['0:00', '2:00','4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '24:00' ];

    const loop = _.map(dataset, (d) => {
      const book = Reservations.find({
        $and: [
          {resourceId: {$in: d.ids}},
          {start: {$gte: +start.locale(lang).format('x')}},
          {start: {$lte: +end.locale(lang).format('x')}},
          {
            $or: [
              {pending: { $exists: false }},
              {pending: false}
            ]
          },
          {
            $or: [
              {rejected: { $exists: false }},
              {rejected: false}
            ]
          }
        ]
      }).fetch();
      const counter = _.countBy(_.flattenDeep([ _.map(book, function(book) {
        return moment(book.start).locale(lang).format("h");
      }) ]));

      const newData = _.map(label, (time) => {
        const hour = parseInt(time.split(':')[0], 10);
        let count = !!counter[hour] ? counter[hour] : 0;

        res.push({
          'Time': time,
          'Type': d.label,
          'Total': count
        })
      });

      return true
    })
  }
  return res
}

export const BookedByTimeCSV = {
  getBookedByTimeStats(startDate, endDate, condoId, lang, time = false) {
    console.time('booked by time')
    const typeFilter = prepareBookedByTimeData(condoId, lang)
    // console.log('typeFilter : ', typeFilter)
    const data = getBookedByTime(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      typeFilter,
      lang,
      time ? 'time': 'date'
    )
    console.timeEnd('booked by time')
    // console.log('data ', data )
    return data
  }
}
