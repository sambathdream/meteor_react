import { CommonTranslation } from "/common/lang/lang.js";
import { Map } from "core-js";

getComparisonGestionnaireCondosInCharge = (gestionnaire) => {
  if (gestionnaire) {
    const enterprise = Enterprises.findOne({
      _id: gestionnaire.identities.gestionnaireId
    }, {
      fields: {
        users: true
      }
    })
    const mapUser = enterprise.users
    const findUser = _.find(mapUser, function (elem) {
      return elem.userId == Meteor.userId();
    })
    const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);

    if (condoIds) {
      return condoIds;
    }
  }
}

getComparisonIncidents = (start, end, condos, condosData) => {
  let cols = []
  let total = 0

  let condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let count = Incidents.find({
      $and: [
        {
          condoId: {
            $in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id )
          }
        },
        { createdAt: { $gte: new Date(parseInt(start.format('x'))) } },
        { createdAt: { $lt: new Date(parseInt(end.format('x'))) } },
      ]
    }).fetch()
    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = count.length

    cols.push(tmpData)
  })

  let total_count = 0
  _.each(cols, c => {
    total_count += c.count
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos).settings.condoType
    _.each(cols, (c, i) => {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1)
    })
  }
  return { cols: cols, total: total }
}

getComparisonInformation = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;

  let condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  let condosInCharge = Meteor.listCondoUserHasRight("actuality", "see")

  let actuList = []
  condosInCharge.forEach(cd => {
    let c = Condos.findOne(cd);
    if (c && c.settings && c.settings.options && c.settings.options.informations === true) {
      let m = _.find(c.modules, (e) => e.name === "actuality");
      if (m) {
        actuList.push(m.data.actualityId)
      }
    }
  })

  _.each(_.without(_.uniq(condoType), undefined, null), function (cType) {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let count = ActuPosts.find({
      $and: [
        {
          condoId: {$in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id)
          }
        },
        { createdAt: { $gte: parseInt(start.format('x')) } },
        { createdAt: { $lt: parseInt(end.format('x')) } },
      ]
    }, { sort: { createdAt: -1 }, limit: 200 }).fetch()
    // console.log('count.length ', count.length )

    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = count.length

    cols.push(tmpData)
  })

  let total_count = 0;
  _.each(cols, c => {
    total_count += c.count;
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, function (c, i) {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total }
}

getComparisonSatisfaction = (start, end, condos, condosData) => {
  let cols = []
  let total = 0
  let condoType = []

  condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let evaluation = _.flattenDeep(_.map(Incidents.find({
      $and: [
        {
          condoId: {
            $in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id)
          }
        },
        { createdAt: { $gte: new Date(parseInt(start.format('x'))) } },
        { createdAt: { $lt: new Date(parseInt(end.format('x'))) } },
      ]
    }).fetch(), incident => {
      if (incident.eval.length) {
        tmpData[condosData.find(
          condo => condo.condoId === incident.condoId
        ).condoName] += 1
        return _.map(incident.eval, eval => {
          return eval.value
        });
      }
    })).filter(eval => { return eval != 0 }).filter(eval => { return eval != undefined });

    let total = 0;
    _.each(evaluation, e => {
      total += parseInt(e)
    })

    tmpData.count = parseInt(total / evaluation.length) || 0
    cols.push(tmpData)
  })

  evaluation = _.flattenDeep(_.map(Incidents.find({
    $and: [
      { createdAt: { $gte: new Date(parseInt(start.format('x'))) } },
      { createdAt: { $lt: new Date(parseInt(end.format('x'))) } },
    ]
  }).fetch(), incident => {
    if (incident.eval.length) {
      return _.map(incident.eval, eval => {
        return eval.value
      })
    }
  })).filter(eval => { return eval != 0 }).filter(eval => { return eval != undefined })
  total = 0;
  _.each(evaluation, e => {
    total += parseInt(e)
  })

  cols.push({
    type: 'moyenne',
    count: (total / evaluation.length) / Condos.find({
      _id: { $in: condos }
    }).fetch().length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType
    _.each(cols, c, i => {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1)
    })
  }
  return { cols: cols, total: total }
}

getComparisonMessenger = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;

  const condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let conversationIds = Messages.find({
      "condoId": {
        $in:
          _.map(Condos.find({
            _id: { $in: condos },
            "settings.condoType": cType
          }).fetch(), c => c._id)
      },
      target: 'manager'
    }).fetch()
    let map = new Map()
    conversationIds.forEach(cv => {
      map.set(cv._id, cv.condoId)
    })
    conversationIds = conversationIds.map(c => c._id)
    let count = MessagesFeed.find({
      $and: [
        { messageId: { $in: conversationIds } },
        { date: { $gte: parseInt(start.format('x')) } },
        { date: { $lt: parseInt(end.format('x')) } },
      ]
    }).fetch()
    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === map.get(c.messageId)
      ).condoName] += 1
    })
    tmpData.count = count.length
    cols.push(tmpData)
  })

  let total_count = 0;
  _.each(cols, function (c) {
    total_count += c.count;
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, function (c, i) {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total };
}

getComparisonForum = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;
  let condoType = [];

  condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), function (cType) {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let condosId = _.flattenDeep(
      _.map(Condos.find({
        _id: { $in: condos },
        "settings.condoType": cType
      }).fetch(), '_id')).filter(postId => postId !== undefined)

    let forumPost = ForumPosts.find({condoId: {$in: condosId}}).fetch();

    let postOfForum = _.filter(forumPost, function (post) {
      return moment(post.data).diff(start) >= 0 && moment(post.data).diff(end) <= 0;
    })

    let replyOfForum = _.flattenDeep(_.map(forumPost, (post) => {
      let forumComments = ForumCommentPosts.find({ postId: post._id }).fetch();
      return _.filter(forumComments, reply => {
        return moment(reply.commentDate).diff(start) >= 0 && moment(reply.commentDate).diff(end) <= 0;
      })
    }))

    postOfForum.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })

    replyOfForum.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = postOfForum.length + replyOfForum.length
    cols.push(tmpData)
  })

  let total_count = 0;
  _.each(cols, function (c) {
    total_count += c.count;
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, (c, i) => {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total };
}

getComparisonAds = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;
  let condoType = [];

  condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let count = ClassifiedsAds.find({
      $and: [
        {
          condoId: {
            $in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id)
          }
        },
        { createdAt: { $gte: parseInt(start.format('x')) } },
        { createdAt: { $lt: parseInt(end.format('x')) } },
      ]
    }).fetch()

    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = count.length
    cols.push(tmpData)
  })

  let total_count = 0;
  _.each(cols, c => {
    total_count += c.count;
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, function (c, i) {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total };
}

getComparisonConcierge = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;
  let condoType = [];

  condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let count = ConciergeMessage.find({
      $and: [
        {
          condoId: {
            $in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id)
          }
        },
        { date: { $gte: parseInt(start.format('x')) } },
        { date: { $lt: parseInt(end.format('x')) } },
      ]
    }).fetch()

    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = count.length
    cols.push(tmpData)
  })


  let total_count = 0;
  _.each(cols, function (c) {
    total_count += c.count;
  })
  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, (c, i) => {
      if (!(c.type == condoTypeToSearch || c.type == "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total };
}

getComparisonResa = (start, end, condos, condosData) => {
  let cols = [];
  let total = 0;

  let condoType = _.uniq(_.map(Condos.find({
    "_id": {
      $in: getComparisonGestionnaireCondosInCharge(Meteor.user())
    }
  }).fetch(), condo => condo.settings.condoType))

  const resourceIds = _.map(Resources.find({
    condoId: { $in: condos }
  }).fetch(), r =>  r._id)
  // const resourceIds = _.map(
  //   _.filter(
  //     condos.map(c => {
  //       resource = Resources.findOne({ condoId: c })
  //       if (resource) {
  //         return resource
  //       }
  //   }), v => { return v }),
  //       r => { return r._id });


  _.each(_.without(_.uniq(condoType), undefined, null), cType => {
    const tmpData = {}
    tmpData.type = cType
    condosData.forEach(cd => {
      tmpData[cd.condoName] = 0
    })

    let count = Reservations.find({
      $and: [
        { resourceId: { $in: resourceIds } },
        {
          condoId: {$in:
              _.map(Condos.find({
                _id: { $in: condos },
                "settings.condoType": cType
              }).fetch(), c => c._id)
          }
        },
        { type: { $ne: "edl" } },
        { start: { $gte: +start.format('x') } },
        { start: { $lte: +end.format('x') } },
        {
          $or: [
            { pending: { $exists: false } },
            { pending: false }
          ]
        },
        {
          $or: [
            { rejected: { $exists: false } },
            { rejected: false }
          ]
        }
      ]
    }).fetch()

    count.forEach(c => {
      tmpData[condosData.find(
        condo => condo.condoId === c.condoId
      ).condoName] += 1
    })
    tmpData.count = count.length
    cols.push(tmpData)
  })

  let total_count = 0;
  _.each(cols, function (c) {
    total_count += c.count;
  })

  cols.push({
    type: 'moyenne',
    count: total_count / condoType.length
  })

  if (condos === 1) {
    let condoTypeToSearch = Condos.findOne(condos[0]).settings.condoType;
    _.each(cols, function (c, i) {
      if (!(c.type === condoTypeToSearch || c.type === "moyenne"))
        cols.splice(i, 1);
    });
  }
  return { cols: cols, total: total };
}

getComparisonCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getType = (type, tr_common) => {
  switch (type) {
    case 'copro':
      return tr_common.commonTranslation["co_ownership"]
    case 'mono':
      return tr_common.commonTranslation["sole_ownership"]
    case 'etudiante':
      return tr_common.commonTranslation["student_residence"]
    case 'office':
      return tr_common.commonTranslation["office"]
    default:
      return tr_common.commonTranslation["average"]
  }
}

getComparison = (start, end, condoId, lang) => {
  const condos = condoId === 'all' ? Meteor.getManagerCondoIds() : [condoId]
  const condosData = getComparisonCondoName(condos)
  let data = [];

  const tr_common = new CommonTranslation(lang)

  const incidentRightLength = Meteor.listCondoUserHasRight('incident', 'see').length
  const infoRightLength = Meteor.listCondoUserHasRight('actuality', 'see').length
  const messengerRightLength = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise').length
  const forumRightLength = Meteor.listCondoUserHasRight('forum', 'seeSubject').length
  const adsRightLength = Meteor.listCondoUserHasRight('ads', 'see').length
  const conciergeRightLength = Meteor.listCondoUserHasRight('view', 'concierge').length
  const resaRightLength = Meteor.listCondoUserHasRight('reservation', 'seeAll').length

  const userId = Meteor.userId()
  const incidentRight = Meteor.userHasRight('incident', 'see', condoId, userId)
  const infoRight = Meteor.userHasRight('actuality', 'see', condoId, userId)
  const messengerRight = Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, userId)
  const forumRight = Meteor.userHasRight('forum', 'seeSubject', condoId, userId)
  const adsRight = Meteor.userHasRight('ads', 'see', condoId, userId)
  const conciergeRight = Meteor.userHasRight('view', 'concierge', condoId, userId)
  const resaRight = Meteor.userHasRight('reservation', 'seeAll', condoId, userId)

  if ((condos.length > 1 && incidentRightLength > 0) || incidentRight) {
    const incident = getComparisonIncidents(start, end, condos, condosData)
    incident.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['incident_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && infoRightLength > 0) || infoRight) {
    const information = getComparisonInformation(start, end, condos, condosData);
    information.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['information_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && incidentRightLength > 0) || incidentRight) {
    const satisfaction = getComparisonSatisfaction(start, end, condos, condosData);
    satisfaction.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['average_stats']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && messengerRightLength) || messengerRight) {
    const messenger = getComparisonMessenger(start, end, condos, condosData)
    messenger.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['message_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && forumRightLength) || forumRight) {
    const forum = getComparisonForum(start, end, condos, condosData);
    forum.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['post_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && adsRightLength > 0) || adsRight) {
    const ads = getComparisonAds(start, end, condos, condosData);
    ads.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['ads_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && conciergeRightLength > 0) || conciergeRight) {
    const concierge = getComparisonConcierge(start, end, condos, condosData);
    concierge.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['concierge_services_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }

  if ((condos.length > 1 && resaRightLength > 0) || resaRight) {
    const resa = getComparisonResa(start, end, condos, condosData)
    resa.cols.forEach(cl => {
      let pushData = {}
      pushData['Module'] = tr_common.commonTranslation['reservation_nbr']
      pushData['Type'] = getType(cl.type, tr_common)
      condosData.forEach(cd => {
        pushData[cd.condoName] = cl[cd.condoName]
      })
      pushData['Total'] = cl.count
      data.push(pushData)
    })
  }
  // console.log('data ', data )
  return data
}

export const ComparisonCSV = {
  getComparisonStats(startDate, endDate, condoId, lang) {
    console.time('comparison')
    const data = getComparison(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      condoId,
      lang
    )
    console.timeEnd('comparison')
    return data
  }
}
