import moment from 'moment'
import { CommonTranslation } from "/common/lang/lang.js"

// TIMES FUNCTIONS
DayIncidents = (start, end, lang, incidents) => {
  let data = [];

  let duration = end.diff(start, 'day');
  for (var i = 0; i < duration; i++) {
    let tmpData = {
      types: [],
      date: start.locale(lang).format('ddd DD MMM'),
      total: 0
    };
    let incidentsOfDay = _.filter(incidents, (i) => {
      return moment(i.createdAt).format("DD[/]MM[/]YY") == start.format('DD[/]MM[/]YY');
    });
    _.each(incidentsOfDay, (i) => {
      if (_.find(tmpData.types, (t) => { return t.category === i.info.type; })) {
        for (let idx = 0; idx < tmpData.types.length; idx++) {
          if (tmpData.types[idx].category === i.info.type) {
            tmpData.types[idx].value += 1;
            break;
          }
        }
      } else {
        tmpData.types.push({ value: 1, condoId: i.condoId, category: i.info.type });
      }
      tmpData.total += 1;
    });
    data.push(tmpData)
    start.add(1, 'd');
  }
  return data
}

WeekIncidents = (start, end, lang, incidents) => {
  let data = [];

  const tr_common = new CommonTranslation(lang)

  let duration = end.diff(start, 'weeks');
  for (var i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone()
    let tmpData = {
      types: [],
      date: tr_common.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') === tmpDate.add(1, "weeks").format('MM[/]YYYY'))
        ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM'))
        : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').locale(lang).format('DD MMM'))),
      total: 0
    };
    let incidentsOfWeek = _.filter(incidents, (i) => {
      return moment(i.createdAt).format("W YYYY") === start.format('W YYYY');
    });
    // console.log('incidentsOfWeek : ', incidentsOfWeek)
    _.each(incidentsOfWeek, (i) => {
      if (_.find(tmpData.types, (t) => { return t.category === i.info.type; })) {
        for (let idx = 0; idx < tmpData.types.length; idx++) {
          if (tmpData.types[idx].category === i.info.type) {
            tmpData.types[idx].value += 1;
            break;
          }
        }
      } else {
        tmpData.types.push({ value: 1, condoId: i.condoId, category: i.info.type });
      }
      tmpData.total += 1;
    });
    data.push(tmpData)
    start.add(1, 'weeks');
  }
  return data
}

MonthIncidents = (start, end, lang, incidents) => {
  let data = [];

  let duration = end.diff(start, 'month')
  for (var i = 0; i <= duration; i++) {
    let tmpData = {
      types: [],
      date: !(start.format('YYYY') == moment().format('YYYY'))
        ? start.locale(lang).format('MMMM YYYY')
        : start.locale(lang).format('MMMM'),
      total: 0
    };

    let incidentsOfMonth = _.filter(incidents, (i) => {
      return moment(i.createdAt).format("MM[/]YYYY") === start.format('MM[/]YYYY');
    });

    _.each(incidentsOfMonth, (i) => {
      if (_.find(tmpData.types, (t) => { return t.category === i.info.type; })) {
        for (let idx = 0; idx < tmpData.types.length; idx++) {
          if (tmpData.types[idx].category === i.info.type) {
            tmpData.types[idx].value += 1;
            break;
          }
        }
      } else {
        tmpData.types.push({ value: 1, condoId: i.condoId, category: i.info.type });
      }
      tmpData.total += 1
    });
    data.push(tmpData)
    start.add(1, 'M')
  }
  return data
}

YearIncidents = (start, end, incidents) => {
  let data = []

  let duration = end.diff(start, 'year')
  for (var i = 0; i <= duration; i++) {
    let tmpData = {
      types: [],
      date: start.format('YYYY'),
      total: 0
    };

    let incidentsOfYear = _.filter(incidents, (i) => {
      return moment(i.createdAt).format("YYYY") == start.format('YYYY');
    });
    // console.log("incidentsOfYear : ", incidentsOfYear)
    _.each(incidentsOfYear, (i) => {
      if (_.find(tmpData.types, (t) => { return t.category === i.info.type; })) {
        for (let idx = 0; idx < tmpData.types.length; idx++) {
          if (tmpData.types[idx].category === i.info.type) {
            tmpData.types[idx].value += 1;
            break;
          }
        }
      } else {
        tmpData.types.push({ value: 1, condoId: i.condoId, category: i.info.type });
      }
      tmpData.total += 1;
    });
    data.push(tmpData);
    start.add(1, 'y');
  }
  return data
}

getIncidentsCategories = (condoId) => {
  if ((!condoId || condoId === undefined) || condoId !== "" || condoId !== "all")
    return IncidentDetails.find({
      $or:
        [
          { isNewEmpty: { $exists: false } },
          { isNewEmpty: { $ne: true } }
        ]
    }).fetch();
  else if (condoId != "" && condoId != "all") {
    let getNotDefault = IncidentDetails.find(
      {
        $and: [
          { condoId: condoId },
          {
            $or:
              [
                { isNewEmpty: { $exists: false } },
                { isNewEmpty: { $ne: true } }
              ]
          }
        ]
      }).fetch();
    if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
      return IncidentDetails.find(
        {
          $and: [
            { condoId: "default" },
            {
              $or:
                [
                  { isNewEmpty: { $exists: false } },
                  { isNewEmpty: { $ne: true } }
                ]
            }
          ]
        }).fetch();
    else
      return getNotDefault;
  }
  return [];
}

getIncidentsCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getIncidents = (start, end, condoId, lang) => {
  const categories = getIncidentsCategories(condoId)

  let mergedCategories = []
  categories.forEach(ct => {
    const d = _.find(mergedCategories, x => x.defaultDetail === ct.defaultDetail)
    if (d) {
      let _id = []
      mergedCategories.forEach(mc => {
        if (mc.defaultDetail === ct.defaultDetail) {
          _id = _id.concat(mc._id)
          _id.push(ct._id)
          mc._id = _id
        }
      })
    } else {
      ct._id = [ct._id]
      mergedCategories.push(ct)
    }
  })

  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId })

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? condosId : [condoId]

  const condosData = getIncidentsCondoName(condos)
  const incidents = Incidents.find({ condoId: { $in: condos } }).fetch()

  let parseIncidents = []

  if ((duration = end.diff(start, 'year')) > 1) {
    parseIncidents = YearIncidents(start, end, incidents)
  } else if ((duration = end.diff(start, 'month')) > 2) {
    parseIncidents = MonthIncidents(start, end, lang, incidents, condos)
  } else if ((duration = end.diff(start, 'days')) > 31) {
    parseIncidents = WeekIncidents(start, end, lang, incidents)
  } else {
    parseIncidents = DayIncidents(start, end, lang, incidents)
  }

  let dataset = []

  parseIncidents.forEach(i => {
    mergedCategories.forEach(c => {
      let data = {
        Date: i.date,
        Type: c['value-'+lang],
      }
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          data[cd.condoName] = 0
        })
      }
      let total = 0
      if (i.types.length > 0) {
        c._id.forEach(cid => {
          const d = _.find(i.types, x => x.category === cid)
          if (d && condosData.length > 1) { data[condosData.find(
            condo => condo.condoId === d.condoId
          ).condoName] += d.value }
          total += d ? d.value : 0
        })
      }
      data['Total'] = total
      dataset.push(data)
    })
  })
  return dataset
}

export const IncidentsCSV = {
  getIncidentsStats(startDate, endDate, condoId, lang) {
    console.time('incident')
    const data = getIncidents(moment(parseInt(startDate)), moment(parseInt(endDate)), condoId, lang)
    console.timeEnd('incident')
    return data
  }
}