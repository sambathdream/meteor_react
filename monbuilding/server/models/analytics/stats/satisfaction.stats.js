getSatisfactionCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

calculRealSatisfactionPercentage = value => {
  return (value * 100).toFixed(0)
}

getSatisfaction = (start, end, condoId, lang) => {
  const condos = condoId === 'all' ? Meteor.getManagerCondoIds() : [condoId]
  const condosData = getSatisfactionCondoName(condos)

  let data = []

  let tmpData = {}
  condosData.forEach(cd => {
    tmpData[cd.condoName] = []
  })

  let evaluation = _.flatten(_.map(Incidents.find({
    $and: [
      {condoId: {
        $in: condos
      }},
      {createdAt: {$gte: new Date(parseInt(moment(start, "DD[/]MM[/]YY").format('x')))}},
      {createdAt: {$lt: new Date(parseInt(moment(end, "DD[/]MM[/]YY").format('x')))}},
    ]
  }).fetch(), incident => {
    // console.log('incident ', incident )
    if (incident.eval.length) {
      return _.map(incident.eval, eval => {
        tmpData[condosData.find(
          condo => condo.condoId === incident.condoId
        ).condoName].push(eval.value)
        return eval.value
      })
    }
  })).filter(eval => eval !== 0).filter(eval => eval !== undefined)

  condosData.forEach(cd => {
    tmpData[cd.condoName] = tmpData[cd.condoName].filter(e => e !== 0).filter(e => e !== undefined)
  })

  const range = [5, 4, 3, 2, 1]
  range.forEach(r => {
    const evaluationOfRange = _.filter(evaluation, e => e === r)
    // const preEval = (evaluationOfRange.length / evaluation.length) || 0
    // const realPercent = calculRealSatisfactionPercentage(preEval)

    let pushData = {}
    pushData['Stars'] = r
    condosData.forEach(cd => {
      const cdRange = _.filter(tmpData[cd.condoName], f => f === r)
      const cdEval = (cdRange.length / evaluationOfRange.length) || 0
      pushData[cd.condoName + '(%)'] = calculRealSatisfactionPercentage(cdEval)
    })
    pushData['Total Notes'] = evaluationOfRange.length
    data.push(pushData)
  })

  return data
}

export const SatisfactionCSV = {
  getSatisfactionStats(startDate, endDate, condoId, lang) {
    console.time('satisfaction')
    const data = getSatisfaction(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      condoId,
      lang,
    )
    console.timeEnd('satisfaction')
    return data
  }
}