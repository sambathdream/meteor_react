import { CommonTranslation } from '/common/lang/lang.js'

getUsersByDay = (collection) => {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('DD[/]MM[/]YY')} && moment n: ${moment(u.loginDate).format('DD[/]MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('DD[/]MM[/]YY') == moment(u.endDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

getUsersByWeek = (collection) => {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('W YYYY')} && moment n: ${moment(u.loginDate).format('W YYYY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('W YYYY') == moment(u.endDate).format('W YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

getUsersByMonth = (collection) => {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.endDate).format('MM[/]YY')} && moment n: ${moment(u.endDate).format('MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.endDate).format('MM[/]YY') == moment(u.endDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

// function getUsersByYear(collection) {
//   let users = []
//   collection.map(w => {
//     let exist = false
//     users.map(u => {
//       // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
//       // console.log(`moment w: ${moment(w.endDate).format('YYYY')} && moment n: ${moment(u.endDate).format('YYYY')}`)
//       if (w.userId == u.userId && moment(w.endDate).format('YYYY') == moment(u.endDate).format('YYYY'))
//         exist = true
//     })
//     if (!exist)
//       users.push(w)
//   })
//   return users
// }

// function containCategory(categories, categoryName) {
//   return _.find(categories, (c) => { return c.slug === categoryName }) !== undefined
// }

DayTotalUsersModule = (start, end, lang, collection, condosData) => {
	let data = []
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)

  const tr_common = new CommonTranslation(lang)

	const duration = end.diff(start, 'days')
	for (var i = 0; i < duration; i++) {
		let tmpData = {
			types: [],
			date: start.locale(lang).format('ddd DD MMM'),
			total: 0
    }
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        feed: 0,
        forum: 0,
        incidents: 0,
        classifieds: 0,
        actus: 0,
        resources: 0,
        concierge: 0,
        messenger: 0,
        trombi: 0
      }
    })

    let feedOfDay = _.filter(feed, (f) => {
      if (moment(f.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].feed += 1
        return true
      }
      return false
		})
    let forumOfDay = _.filter(forum, (f) => {
      if (moment(f.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		})
		let incidentsOfDay = _.filter(incidents, (i) => {
      if (moment(i.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		})
		let classifiedsOfDay = _.filter(classifieds, (c) => {
      if (moment(c.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		})
		let actusOfDay = _.filter(actus, (a) => {
      if (moment(a.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].actus += 1
        return true
      }
			return false
		})
		let resourcesOfDay = _.filter(resources, (r) => {
      if (r !== undefined) {
        if (moment(r.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
          tmpData[condosData.find(
            condo => condo.condoId === r.condoId
          ).condoName].resources += 1
          return true
        }
        return false
      }
		})
		let conciergeOfDay = _.filter(concierge, (c) => {
      if (moment(c.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].concierge += 1
        return true
      }
			return false
		})
		let messengerOfDay = _.filter(messenger, (m) => {
      if (moment(m.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === m.condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		})
		let trombiOfDay = _.filter(trombi, (t) => {
      if (moment(t.endDate).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === t.condoId
        ).condoName].trombi += 1
        return true
      }
			return false
		})
		tmpData.types.push({value: feedOfDay.length, category: { cat: 'feed', name: getCategoryName('feed', tr_common)}})
		tmpData.types.push({value: forumOfDay.length, category: { cat: 'forum', name: getCategoryName('forum', tr_common)}})
		tmpData.types.push({value: incidentsOfDay.length, category: { cat: 'incidents', name: getCategoryName('incidents', tr_common)}})
		tmpData.types.push({value: classifiedsOfDay.length, category: { cat: 'classifieds', name: getCategoryName('classifieds', tr_common)}})
		tmpData.types.push({value: actusOfDay.length, category: { cat: 'actus', name: getCategoryName('actus', tr_common)}})
		tmpData.types.push({value: resourcesOfDay.length, category: { cat: 'resources', name: getCategoryName('resources', tr_common)}})
		tmpData.types.push({value: conciergeOfDay.length, category: { cat: 'concierge', name: getCategoryName('concierge', tr_common)}})
		tmpData.types.push({value: messengerOfDay.length, category: { cat: 'messenger', name: getCategoryName('messenger', tr_common)}})
		tmpData.types.push({value: trombiOfDay.length, category: { cat: 'trombi', name: getCategoryName('trombi', tr_common)}})
		// tmpData.total = (feedOfDay.length + forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length + trombiOfDay.length)
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = d.category.name
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = tmpData[cd.condoName][d.category.cat]
        })
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'd')
	}
	return data
}

WeekTotalUsersModule = (start, end, lang, collection, condosData) => {
	let data = []
  const translation = new CommonTranslation(lang)
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)

  const tr_common = new CommonTranslation(lang)

	const duration = end.diff(start, 'weeks')
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		let tmpData = {
			types: [],
      date: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') === tmpDate.add(1, "weeks").format('MM[/]YYYY'))
            ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM'))
            : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
		}
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        feed: 0,
        forum: 0,
        incidents: 0,
        classifieds: 0,
        actus: 0,
        resources: 0,
        concierge: 0,
        messenger: 0,
        trombi: 0
      }
    })

    let feedOfWeek = _.filter(feed, (f) => {
      if (moment(f.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].feed += 1
        return true
      }
      return false
		})
    let forumOfWeek = _.filter(forum, (f) => {
      if (moment(f.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		})
		let incidentsOfWeek = _.filter(incidents, (i) => {
      if (moment(i.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		})
		let classifiedsOfWeek = _.filter(classifieds, (c) => {
      if (moment(c.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		})
		let actusOfWeek = _.filter(actus, (a) => {
      if (moment(a.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].actus += 1
        return true
      }
			return false
		})
		let resourcesOfWeek = _.filter(resources, (r) => {
      if (r !== undefined) {
        if (moment(r.endDate).format('W YYYY') === start.format('W YYYY')) {
          tmpData[condosData.find(
            condo => condo.condoId === r.condoId
          ).condoName].resources += 1
          return true
        }
        return false
      }
		})
		let conciergeOfWeek = _.filter(concierge, (c) => {
      if (moment(c.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].concierge += 1
        return true
      }
			return false
		})
		let messengerOfWeek = _.filter(messenger, (m) => {
      if (moment(m.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === m.condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		})
		let trombiOfWeek = _.filter(trombi, (t) => {
      if (moment(t.endDate).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === t.condoId
        ).condoName].trombi += 1
        return true
      }
			return false
		})
		tmpData.types.push({value: feedOfWeek.length, category: { cat: 'feed', name: getCategoryName('feed', tr_common)}})
		tmpData.types.push({value: forumOfWeek.length, category: { cat: 'forum', name: getCategoryName('forum', tr_common)}})
		tmpData.types.push({value: incidentsOfWeek.length, category: { cat: 'incidents', name: getCategoryName('incidents', tr_common)}})
		tmpData.types.push({value: classifiedsOfWeek.length, category: { cat: 'classifieds', name: getCategoryName('classifieds', tr_common)}})
		tmpData.types.push({value: actusOfWeek.length, category: { cat: 'actus', name: getCategoryName('actus', tr_common)}})
		tmpData.types.push({value: resourcesOfWeek.length, category: { cat: 'resources', name: getCategoryName('resources', tr_common)}})
		tmpData.types.push({value: conciergeOfWeek.length, category: { cat: 'concierge', name: getCategoryName('concierge', tr_common)}})
		tmpData.types.push({value: messengerOfWeek.length, category: { cat: 'messenger', name: getCategoryName('messenger', tr_common)}})
		tmpData.types.push({value: trombiOfWeek.length, category: { cat: 'trombi', name: getCategoryName('trombi', tr_common)}})
		// tmpData.total = (feedOfWeek.length + forumOfWeek.length + incidentsOfWeek.length + classifiedsOfWeek.length + actusOfWeek.length + resourcesOfWeek.length + conciergeOfWeek.length + messengerOfWeek.length + trombiOfWeek.length)
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = d.category.name
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = tmpData[cd.condoName][d.category.cat]
        })
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'weeks')
	}
	return data
}

MonthTotalUsersModule = (start, end, lang, collection, condosData) => {
  // console.log('condosData ', condosData )
	let data = []
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)

  const tr_common = new CommonTranslation(lang)

  const duration = end.diff(start, 'month')
	for (var i = 0; i <= duration; i++) {
		let tmpData = {
			types: [],
      date: !(start.format('YYYY') === moment().format('YYYY'))
            ? start.locale(lang).format('MMMM YYYY')
            : start.locale(lang).format('MMMM'),
		}
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        feed: 0,
        forum: 0,
        incidents: 0,
        classifieds: 0,
        actus: 0,
        resources: 0,
        concierge: 0,
        messenger: 0,
        trombi: 0
      }
    })

    // console.log('tmpData ', tmpData )

    let feedOfMonth = _.filter(feed, (f) => {
      if (moment(f.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        // console.log('f', f)
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].feed += 1
        return true
      }
      return false
		})
    let forumOfMonth = _.filter(forum, (f) => {
      if (moment(f.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        // console.log('f.condoId ', f.condoId )
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		})
		let incidentsOfMonth = _.filter(incidents, (i) => {
      if (moment(i.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		})
		let classifiedsOfMonth = _.filter(classifieds, (c) => {
      if (moment(c.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		})
		let actusOfMonth = _.filter(actus, (a) => {
      if (moment(a.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].actus += 1
        return true
      }
			return false
		})
		let resourcesOfMonth = _.filter(resources, (r) => {
      if (r !== undefined) {
        if (moment(r.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
          tmpData[condosData.find(
            condo => condo.condoId === r.condoId
          ).condoName].resources += 1
          return true
        }
        return false
      }
		})
		let conciergeOfMonth = _.filter(concierge, (c) => {
      if (moment(c.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].concierge += 1
        return true
      }
			return false
		})
		let messengerOfMonth = _.filter(messenger, (m) => {
      if (moment(m.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === m.condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		})
		let trombiOfMonth = _.filter(trombi, (t) => {
      if (moment(t.endDate).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === t.condoId
        ).condoName].trombi += 1
        return true
      }
			return false
		})
		tmpData.types.push({value: feedOfMonth.length, category: { cat: 'feed', name: getCategoryName('feed', tr_common)}})
		tmpData.types.push({value: forumOfMonth.length, category: { cat: 'forum', name: getCategoryName('forum', tr_common)}})
		tmpData.types.push({value: incidentsOfMonth.length, category: { cat: 'incidents', name: getCategoryName('incidents', tr_common)}})
		tmpData.types.push({value: classifiedsOfMonth.length, category: { cat: 'classifieds', name: getCategoryName('classifieds', tr_common)}})
		tmpData.types.push({value: actusOfMonth.length, category: { cat: 'actus', name: getCategoryName('actus', tr_common)}})
		tmpData.types.push({value: resourcesOfMonth.length, category: { cat: 'resources', name: getCategoryName('resources', tr_common)}})
		tmpData.types.push({value: conciergeOfMonth.length, category: { cat: 'concierge', name: getCategoryName('concierge', tr_common)}})
		tmpData.types.push({value: messengerOfMonth.length, category: { cat: 'messenger', name: getCategoryName('messenger', tr_common)}})
    tmpData.types.push({value: trombiOfMonth.length, category: { cat: 'trombi', name: getCategoryName('trombi', tr_common)}})
		// tmpData.total = (feedOfMonth.length + forumOfMonth.length + incidentsOfMonth.length + classifiedsOfMonth.length + actusOfMonth.length + resourcesOfMonth.length + conciergeOfMonth.length + messengerOfMonth.length + trombiOfMonth.length)
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = d.category.name
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = tmpData[cd.condoName][d.category.cat]
        })
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'M')
  }
	return data
}

YearTotalUsersModule = (start, end, collection, condosData) => {
	let data = []
  let feed = getUsersByDay(collection.feed)
  let forum = getUsersByDay(collection.forum)
  let incidents = getUsersByDay(collection.incidents)
  let classifieds = getUsersByDay(collection.ads)
  let actus = getUsersByDay(collection.actus)
  let resources = getUsersByDay(collection.resources)
  let concierge = getUsersByDay(collection.concierge)
  let messenger = getUsersByDay(collection.messenger)
  let trombi = getUsersByDay(collection.trombi)

  const tr_common = new CommonTranslation(lang)

	const duration = end.diff(start, 'year')
	for (var i = 0; i <= duration; i++) {
		let tmpData = {
			types: [],
			date: start.format('YYYY'),
    }
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        feed: 0,
        forum: 0,
        incidents: 0,
        classifieds: 0,
        actus: 0,
        resources: 0,
        concierge: 0,
        messenger: 0,
        trombi: 0
      }
    })

    let feedOfYear = _.filter(feed, (f) => {
      if (moment(f.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].feed += 1
        return true
      }
      return false
		})
    let forumOfYear = _.filter(forum, (f) => {
      if (moment(f.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		})
		let incidentsOfYear = _.filter(incidents, (i) => {
      if (moment(i.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		})
		let classifiedsOfYear = _.filter(classifieds, (c) => {
      if (moment(c.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		})
		let actusOfYear = _.filter(actus, (a) => {
      if (moment(a.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].actus += 1
        return true
      }
			return false
		})
		let resourcesOfYear = _.filter(resources, (r) => {
      if (r !== undefined) {
        if (moment(r.endDate).format('YYYY') === start.format('YYYY')) {
          tmpData[condosData.find(
            condo => condo.condoId === r.condoId
          ).condoName].resources += 1
          return true
        }
        return false
      }
		})
		let conciergeOfYear = _.filter(concierge, (c) => {
      if (moment(c.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].concierge += 1
        return true
      }
			return false
		})
		let messengerOfYear = _.filter(messenger, (m) => {
      if (moment(m.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === m.condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		})
		let trombiOfYear = _.filter(trombi, (t) => {
      if (moment(t.endDate).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === t.condoId
        ).condoName].trombi += 1
        return true
      }
			return false
		})
		tmpData.types.push({value: feedOfYear.length, category: { cat: 'feed', name: getCategoryName('feed', tr_common)}})
		tmpData.types.push({value: forumOfYear.length, category: { cat: 'forum', name: getCategoryName('forum', tr_common)}})
		tmpData.types.push({value: incidentsOfYear.length, category: { cat: 'incidents', name: getCategoryName('incidents', tr_common)}})
		tmpData.types.push({value: classifiedsOfYear.length, category: { cat: 'classifieds', name: getCategoryName('classifieds', tr_common)}})
		tmpData.types.push({value: actusOfYear.length, category: { cat: 'actus', name: getCategoryName('actus', tr_common)}})
		tmpData.types.push({value: resourcesOfYear.length, category: { cat: 'resources', name: getCategoryName('resources', tr_common)}})
		tmpData.types.push({value: conciergeOfYear.length, category: { cat: 'concierge', name: getCategoryName('concierge', tr_common)}})
		tmpData.types.push({value: messengerOfYear.length, category: { cat: 'messenger', name: getCategoryName('messenger', tr_common)}})
		tmpData.types.push({value: trombiOfYear.length, category: { cat: 'trombi', name: getCategoryName('trombi', tr_common)}})
		// tmpData.total = (feedOfYear.length + forumOfYear.length + incidentsOfYear.length + classifiedsOfYear.length + actusOfYear.length + resourcesOfYear.length + conciergeOfYear.length + messengerOfYear.length + trombiOfYear.length)
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = d.category.name
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = tmpData[cd.condoName][d.category.cat]
        })
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'y')
	}
	return data
}

getCategoryName = (category, tr_common) => {
  switch (category) {
    case  'feed':
      return tr_common.commonTranslation["total_feed_users"]
    case  'forum':
    return tr_common.commonTranslation["total_forum_users"]
    case  'incidents':
    return tr_common.commonTranslation["total_incidents_users"]
    case  'classifieds':
    return tr_common.commonTranslation["total_ads_users"]
    case  'actus':
    return tr_common.commonTranslation["total_informations_users"]
    case  'resources':
    return tr_common.commonTranslation["total_reservations_users"]
    case  'concierge':
    return tr_common.commonTranslation["total_concierge_users"]
    case  'messenger':
    return tr_common.commonTranslation["total_messenger_users"]
    case  'trombi':
    return tr_common.commonTranslation["total_trombi_users"]
    default:
      break;
  }
}

getModulesForByAccessType = (platform, usersId, condos) => {
  let modules = []
  switch (platform) {
    case "all":
      modules = Analytics.find({
        userId: { $in: usersId },
        $or: [
          {
            module: 'feed',
          },
          {
            module: 'forum',
            condoId: { $in: Meteor.listCondoUserHasRight('forum', 'seeSubject') }
          },
          {
            module: 'incident',
            condoId: { $in: Meteor.listCondoUserHasRight('incident', 'see') }
          },
          {
            module: 'ads',
            condoId: { $in: Meteor.listCondoUserHasRight('ads', 'see') }
          },
          {
            module: 'info',
            condoId: { $in: Meteor.listCondoUserHasRight('actuality', 'see') }
          },
          {
            module: 'resa',
            condoId: { $in: Meteor.listCondoUserHasRight('reservation', 'seeAll') }
          },
          {
            module: 'concierge',
            condoId: { $in: Meteor.listCondoUserHasRight('view', 'concierge') }
          },
          {
            module: 'messenger',
            condoId: { $in: Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise') }
          },
          {
            module: 'trombi',
            condoId: { $in: Meteor.listCondoUserHasRight('trombi', 'see') }
          }
        ],
        type: "module"
      }).fetch()
      break;
    // case "web":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "web",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "ios":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "ios",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "android":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: "android",
    //     type: "module"
    //   }).fetch()
    //   break;
    // case "others":
    //   modules = Analytics.find({
    //     userId: { $in: usersId },
    //     accessType: undefined,
    //     type: "module"
    //   }).fetch()
    //   break;
    default:
      break;
  }
  modules = modules.filter(f => condos.includes(f.condoId))
  return modules
}

getTotalUsersSectionCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getTotalUsersSection = (platform, start, end, condoId, lang) => {
  const condos = condoId === 'all' ? Meteor.getManagerCondoIds() : [condoId]
  const condosData = getTotalUsersSectionCondoName(condos)

  let modules = {
    feed: [],
		forum: [],
		incidents: [],
		classifieds: [],
		actus: [],
		resources: [],
		concierge: [],
    messenger: [],
    trombi: []
  }

  let res;
  let usersId = []
  usersId = _.map(Residents.find({
    'condos.condoId': { $in: condos }
  }, { fields: { userId: true } }).fetch(), "userId")
  // let categories = []
  // const translation = {stats: new CommonTranslation((FlowRouter.getParam("lang") || "fr"))}

  const forumRightLength = Meteor.listCondoUserHasRight('forum', 'seeSubject').length
  const incidentRightLength = Meteor.listCondoUserHasRight('incident', 'see').length
  const adsRightLength = Meteor.listCondoUserHasRight('ads', 'see').length
  const infoRightLength = Meteor.listCondoUserHasRight('actuality', 'see').length
  const resaRightLength = Meteor.listCondoUserHasRight('reservation', 'seeAll').length
  const conciergeRightLength = Meteor.listCondoUserHasRight('view', 'concierge').length
  const messengerRightLength = Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise').length
  const trombiRightLength = Meteor.listCondoUserHasRight('trombi', 'see').length

  const userId = Meteor.userId()
  const forumRight = Meteor.userHasRight('forum', 'seeSubject', condoId, userId)
  const incidentRight = Meteor.userHasRight('incident', 'see', condoId, userId)
  const adsRight = Meteor.userHasRight('ads', 'see', condoId, userId)
  const infoRight = Meteor.userHasRight('actuality', 'see', condoId, userId)
  const resaRight = Meteor.userHasRight('reservation', 'seeAll', condoId, userId)
  const conciergeRight = Meteor.userHasRight('view', 'concierge', condoId, userId)
  const messengerRight = Meteor.userHasRight('messenger', 'seeAllMsgEnterprise', condoId, userId)
  const trombiRight = Meteor.userHasRight('trombi', 'see', condoId, userId)

  getModulesForByAccessType(platform, usersId, condos).forEach((user) => {
		switch (user.module) {
      case "feed":
        modules.feed.push(user)
        break;
      case "forum":
        if ((condoId === 'all' && forumRightLength > 0) || forumRight) {
          modules.forum.push(user)
        }
				break;
			case "incident":
        if ((condoId === 'all' && incidentRightLength > 0) || incidentRight) {
          modules.incidents.push(user)
        }
				break;
			case "ads":
        if ((condoId === 'all' && adsRightLength > 0) || adsRight) {
          modules.classifieds.push(user)
        }
				break;
			case "info":
        if ((condoId === 'all' && infoRightLength > 0) || infoRight) {
          modules.actus.push(user)
        }
				break;
      case "resa":
        if ((condoId === 'all' && resaRightLength > 0) || resaRight) {
          modules.resources.push(user)
        }
				break;
			case "concierge":
        if ((condoId === 'all' && conciergeRightLength > 0) || conciergeRight) {
          modules.concierge.push(user)
        }
				break;
			case "messenger":
        if ((condoId === 'all' && messengerRightLength > 0) || messengerRight) {
          modules.messenger.push(user)
        }
        break;
      case "trombi":
        if ((condoId === 'all' && trombiRightLength > 0) || trombiRight) {
          modules.trombi.push(user)
        }
        break;
			default:
				break;
		}
  })
  // console.log('categories : ', categories)

	// console.log("modules: ", modules)
	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearTotalUsersModule(start, end, {
      "feed": modules.feed,
			"forum": modules.forum,
			"incidents": modules.incidents,
			"ads": modules.classifieds,
			"actus": modules.actus,
			"resources": modules.resources,
			"concierge": modules.concierge,
      "messenger": modules.messenger,
      "trombi": modules.trombi
		}, condosData)
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthTotalUsersModule(start, end, lang, {
      "feed": modules.feed,
			"forum": modules.forum,
			"incidents": modules.incidents,
			"ads": modules.classifieds,
			"actus": modules.actus,
			"resources": modules.resources,
			"concierge": modules.concierge,
			"messenger": modules.messenger,
      "trombi": modules.trombi
		}, condosData)
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekTotalUsersModule(start, end, lang, {
      "feed": modules.feed,
			"forum": modules.forum,
			"incidents": modules.incidents,
			"ads": modules.classifieds,
			"actus": modules.actus,
			"resources": modules.resources,
			"concierge": modules.concierge,
			"messenger": modules.messenger,
      "trombi": modules.trombi
		}, condosData)
	}
	else {
		res = DayTotalUsersModule(start, end, lang, {
      "feed": modules.feed,
			"forum": modules.forum,
			"incidents": modules.incidents,
			"ads": modules.classifieds,
			"actus": modules.actus,
			"resources": modules.resources,
			"concierge": modules.concierge,
			"messenger": modules.messenger,
      "trombi": modules.trombi
		}, condosData)
	}

	return res
}

export const TotalUsersSectionCSV = {
  getTotalUsersSectionStats(startDate, endDate, condoId, lang) {
    console.time('total users section')
    const data = getTotalUsersSection(
      'all',
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      condoId,
      lang
    )
    // console.log('data ', data )
    console.timeEnd('total users section')
    return data
  }
}
