import { CommonTranslation } from "/common/lang/lang.js"

getOccupancyRateResourceTypes = (condoId, lang) => {
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId })

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]

  let resources = attachResourceType(Resources.find({ condoId: {$in: condos } }).fetch(), lang)

  const data = {}
  let resourceIds = []

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id]
      }
    } else {
      data[res.type].ids.push(res._id)
    }
    resourceIds.push(res._id)
  })
  return { data, condos, resourceIds }
}

prepareOccupancyRateData = (condoId, lang) => {
  const resourcesType = getOccupancyRateResourceTypes(condoId, lang)
  // console.log('resourceType : ', resourcesType)
  const translation = new CommonTranslation(lang)
  const typeFilter = (_.size(resourcesType) > 0 ? _.map(resourcesType.data, (d, i) => {
    return {
      type: i,
      ...d
    }
  }) : [{
    type: 'empty',
    name: translation.commonTranslation["no_resources"],
    ids: []
  }])
  return {
    types: typeFilter,
    condos: resourcesType.condos,
    resourceIds: resourcesType.resourceIds
  }
}

setDefaultOccupancyRateData = (id) => {
  const resource = Resources.findOne({ _id: id }) || {}
  return {
    id: id,
    label: resource.name,
    data: [],
  }
}

getAmountOfWeekDaysInMonth = (date, weekday) => {
  date.date(1);
  const dif = (7 + (weekday - date.weekday()))%7+1;
  // console.log("weekday: "+ weekday +", FirstOfMonth: "+ date.weekday() +", dif: "+dif);
  return Math.floor((date.daysInMonth()-dif) / 7)+1;
}

getOccupancyRateResourceOpenHourTotal = (id, type, date) => {
  const resource = Resources.findOne({ _id: id })

  if (!!resource) {
    return _.reduce(resource.horaires, (countTot, hor) => {
      const dailyOpenHour = _.reduce(hor.openHours, (count, h) => {
        const s = +h.start.split(':')[0]
        const e = +h.end.split(':')[0]

        return count + (e - s)
      }, 0)

      let multiplier = 0
      if (type === 'month') {
        multiplier = getAmountOfWeekDaysInMonth(date, hor.dow[0])
      } else if (type === 'year') {
        multiplier = Math.floor(365 / 7)
      } else if (type === 'week') {
        multiplier = 1
      }

      return countTot + (multiplier * dailyOpenHour)
    }, 0)
  } else {
    return 0
  }
}

dayOccupancyRateData = (book, dataset, types, start, end) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'days')
  for (let i = 0; i < duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, b => {
        return b.resourceId === d.id && b.start > +start.startOf('day').format('x') && b.start <= +start.endOf('day').format('x')
      })

      let resourceOpenHourTotal = 0
      let resource = Resources.findOne({ _id: d.id })

      if (!!resource) {
        // console.log('start', start)
        const horaire = resource.horaires.find((h) => { return h.dow[0] === start.day() })
        if (horaire && horaire.open) {
          resourceOpenHourTotal = _.reduce(horaire.openHours, (count, h) => {
            const s = +h.start.split(':')[0]
            const e = +h.end.split(':')[0]

            return count + (e - s)
          }, 0)
        }
      }

      const number = _.reduce(resourceBook, (count, d) => {
        return count + moment(d.end).diff(moment(d.start), 'h')
      }, 0)

      const count = resourceBook !== 0 ? number / resourceOpenHourTotal * 100 : 0

      const type = types.find(t => t.ids.includes(d.id))
      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: start.format('ddd DD MMM'),
        count: !!count && count !== null && count !== Infinity ? count : 0
      })

      return {
        ...d,
        data: newData
      }
    })

    date.push(start.format('ddd DD MMM'))
    start.add(1, "d")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Type'] = d.type
      pushData['Date'] = d.date
      pushData['Total(%)'] = d.count.toFixed(2)
      data.push(pushData)
    })
  })

  return data
}

weekOccupancyRateData = (book, dataset, types, start, end, lang) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'weeks')
  const translation = new CommonTranslation(lang)
  for (let i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone()
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return b.resourceId === d.id && b.start > +start.startOf('week').format('x') && b.start <= +start.endOf('week').format('x')
      })

      const resourceOpenHourTotal = getOccupancyRateResourceOpenHourTotal(d.id, 'week', start)

      const number = _.reduce(resourceBook, (count, d) => {
        return count + moment(d.end).diff(moment(d.start), 'h')
      }, 0)

      const count = resourceBook !== 0 ? number / resourceOpenHourTotal * 100 : 0

      const type = types.find(t => t.ids.includes(d.id))
      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
        count: !!count && count !== null && count !== Infinity ? count : 0
      })

      return {
        ...d,
        data: newData
      }
    })

    date.push(translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))))
    start.add(1, "weeks")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Type'] = d.type
      pushData['Date'] = d.date
      pushData['Total(%)'] = d.count.toFixed(2)
      data.push(pushData)
    })
  })

  return data
}

monthOccupancyRateData = (book, dataset, types, start, end) => {
  const date = []
  let data = []

  // console.log('dataset ', dataset )
  // console.log('book ', book )
  let duration = end.diff(start, 'month')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, d => {
      const resourceBook = _.filter(book, b => {
        return b.resourceId === d.id && b.start > +start.startOf('month').format('x') && b.start <= +start.endOf('month').format('x')
      })

      const resourceOpenHourTotal = getOccupancyRateResourceOpenHourTotal(d.id, 'month', start)
      // console.log('resourceOpenHourTotal ', resourceOpenHourTotal )

      const number = _.reduce(resourceBook, (count, d) => {
        return count + moment(d.end).diff(moment(d.start), 'h')
      }, 0)
      // console.log('number ', number )

      const count = resourceBook !== 0
                    ? number / resourceOpenHourTotal * 100
                    : 0
      // console.log('count ', count )
      const type = types.find(t => t.ids.includes(d.id))
      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: !(start.format('YYYY') === moment().format('YYYY')) ? start.format('MMMM YYYY') : start.format('MMMM'),
        count: !!count && count !== null && count !== Infinity ? count : 0
      })

      return {
        ...d,
        data: newData
      }
    })

    date.push(!(start.format('YYYY') === moment().format('YYYY')) ? start.format('MMMM YYYY') : start.format('MMMM'))
    start.add(1, "M")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Type'] = d.type
      pushData['Date'] = d.date
      pushData['Total(%)'] = d.count.toFixed(2)
      data.push(pushData)
    })
  })

  return data
}

yearOccupancyRateData = (book, dataset, types, start, end) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'year')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, function(b) {
        return b.resourceId === d.id && b.start > +start.startOf('year').format('x') && b.start <= +start.endOf('year').format('x')
      })

      const resourceOpenHourTotal = getOccupancyRateResourceOpenHourTotal(d.id, 'year', start)

      const number = _.reduce(resourceBook, (count, d) => {
        return count + moment(d.end).diff(moment(d.start), 'h')
      }, 0)

      const count = resourceBook !== 0 ? number / resourceOpenHourTotal * 100 : 0

      const type = types.find(t => t.ids.includes(d.id))
      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: start.format("YYYY"),
        count: !!count && count !== null && count !== Infinity ? count : 0
      })

      return {
        ...d,
        data: newData
      }
    })

    date.push(start.format("YYYY"))
    start.add(1, "y")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Type'] = d.type
      pushData['Date'] = d.date
      pushData['Total(%)'] = d.count.toFixed(2)
      data.push(pushData)
    })
  })

  return data
}

getOccupancyRateCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getOccupancyRate = (start, end, typeFilter, lang) => {
  // console.log('typeFilter ', typeFilter )
  let dataset = _.map(typeFilter.resourceIds, o => {
    return setDefaultOccupancyRateData(o)
  })

  // const condosData = getOccupancyRateCondoName(typeFilter.condos)

  let res = []

  let book = Reservations.find({
    $and: [
      { resourceId: { $in: typeFilter.resourceIds } },
      { start: { $gte: +start.locale(lang).format('x') } },
      { start: { $lte: +end.locale(lang).format('x') } },
      { condoId: {$in: typeFilter.condos } },
      {
        $or: [
          { pending: { $exists: false } },
          { pending: false }
        ]
      },
      {
        $or: [
          { rejected: { $exists: false } },
          { rejected: false }
        ]
      }
    ]
  }).fetch()

  if (end.diff(start, 'year') > 1) {
    res = yearOccupancyRateData(book, dataset, typeFilter.types, start, end)
  } else if (end.diff(start, 'month') > 2) {
    res = monthOccupancyRateData(book, dataset, typeFilter.types, start, end)
  } else if (end.diff(start, 'days') > 31) {
    res = weekOccupancyRateData(book, dataset, typeFilter.types, start, end, lang)
  } else {
    res = dayOccupancyRateData(book, dataset, typeFilter.types, start, end)
  }

  if (res.length === 0) {
    let pushData = {}
    pushData['Type'] = 'No data'
    pushData['Date'] = 'No data'
    pushData['Total(%)'] = 'No data'
    res.push(pushData)
  }

  return res
}

export const OccupancyRateCSV = {
  getOccupancyRateStats(startDate, endDate, condoId, lang) {
    console.time('occupancy rate')
    const typeFilter = prepareOccupancyRateData(condoId, lang)
    // console.log('typeFilter : ', typeFilter)
    const data = getOccupancyRate(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      typeFilter,
      lang
    )
    // console.log('data ', data )
    console.timeEnd('occupancy rate')
    return data
  }
}
