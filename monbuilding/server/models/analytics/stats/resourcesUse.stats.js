getResourcesUseTypes = (condoId, lang) => {
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId })

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]

  let resources = attachResourceType(Resources.find({ condoId: {$in: condos} }).fetch(), lang)
  // console.log('resources ', resources )

  const data = {}
  let resourceIds = []

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id],
        condoId: res.condoId
      }
    } else {
      data[res.type].ids.push(res._id)
    }
    resourceIds.push(res._id)
  })

  return { data, condos, resourceIds }
}

prepareResourcesUseData = (condoId, lang) => {
  const resourcesType = getResourcesUseTypes(condoId, lang)

  const typeFilter = _.size(resourcesType.data) > 0 ? [
    {
      type: 'all',
      name: 'all',
      ids: []
    },
    ..._.map(resourcesType.data, (d, i) => {
      return {
        type: i,
        ...d
      }
    })
  ] : [{
    type: 'empty',
    name: 'empty',
    ids: []
  }]

  return {
    types: typeFilter,
    condos: resourcesType.condos,
    resourceIds: resourcesType.resourceIds
  }
}

collectResourcesUseCompany = (condos) => {
  const resident = Residents.find({
    'condos.userInfo.company': { $exists: true },
    'condos.condoId': { $in: condos }
  }).fetch()
  // console.log('resident ', resident )

  const companyIds = _.compact(_.uniq(_.flattenDeep(resident.map((r) => {
    return r.condos.map((c) => {
      const status = condos.length > 1 ? true : c.condoId === condos[0]
      if (status && !!c.userInfo && !!c.userInfo.company) {
        return c.userInfo.company
      }
    })
  }))))

  return CompanyName.find({ _id: { $in: companyIds } }).fetch()
}

collectResourcesUseUserQuery = (companyId, condos) => {
  const resident = Residents.find({
    'condos.userInfo.company': companyId,
    'condos.condoId': { $in: condos }
  }).fetch()

  const or = _.flattenDeep(resident.map((r) => {
    const condo = r.condos.map((c) => {
      const status = condos.length > 1 ? true : c.condoId === condos[0]
      if (status && !!c.userInfo && !!c.userInfo.company && c.userInfo.company === companyId) {
        return {
          $and: [
            { condoId: c.condoId },
            { origin: r.userId }
          ]
        }
      }
    })

    return _.compact(condo)
  }))

  return or.length > 0 ? {
    $or: [...or]
  } : {}
}

getResourcesUseCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getResourcesUse = (start, end, typeFilter, lang) => {
  // console.log('typeFilter ', typeFilter )
  const companies = collectResourcesUseCompany(typeFilter.condos);
  // console.log('companies ', companies )
  const condosData = getResourcesUseCondoName(typeFilter.condos)

  let dataset = []
  typeFilter.types.forEach(type => {
    companies.forEach((company, i) => {
      const userQuery = collectResourcesUseUserQuery(company._id, typeFilter.condos)
      let data = Reservations.find({
        $and: [
            {resourceId: {$in: typeFilter.resourceIds}},
            {start: {$gte: +start.locale(lang).format('x')}},
            {start: {$lte: +end.locale(lang).format('x')}},
            {condoId: {$in: typeFilter.condos}},
            {
                $or: [
                    {pending: { $exists: false }},
                    {pending: false}
                ]
            },
            {
                $or: [
                    {rejected: { $exists: false }},
                    {rejected: false}
                ]
            },
            userQuery
        ]
      }).fetch()
      // console.log('data ', data )
      const filtResourceIds = type.type === 'all' ? typeFilter.resourceIds : type.ids

      let filtData = data.filter(e => {
        return filtResourceIds.includes(e.resourceId)
      })

      let preData = {
        Type: type.name + '_' + company.name
      }

      condosData.forEach(cd => {
        preData[cd.condoName] = 0;
      })

      let totalDataCount = _.reduce(filtData, (count, d) => {
        preData[condosData.find(
          condo => condo.condoId === d.condoId
        ).condoName] += moment(d.end).locale(lang).diff(moment(d.start), 'm')

        return count + moment(d.end).locale(lang).diff(moment(d.start), 'm')
      }, 0)

      condosData.forEach(cd => {
        if (preData[cd.condoName] >= 60) {
          if (preData[cd.condoName] % 60 === 0) {
            preData[cd.condoName] = parseInt(preData[cd.condoName] / 60) + 'h'
          } else {
            preData[cd.condoName] = parseInt(preData[cd.condoName] / 60) + 'h' + preData[cd.condoName] % 60
          }
        } else if (preData[cd.condoName] !== 0) {
          preData[cd.condoName] =  preData[cd.condoName] + 'min';
        }
      })

      if (totalDataCount >= 60) {
        if (totalDataCount % 60 === 0) {
          totalDataCount = parseInt(totalDataCount / 60) + 'h'
        } else {
          totalDataCount = parseInt(totalDataCount / 60) + 'h' + totalDataCount % 60
        }
      } else if (totalDataCount !== 0) {
        totalDataCount = totalDataCount + 'min';
      }

      preData['Total'] = totalDataCount

      dataset.push(preData)
    })
  })

  if (dataset.length === 0) {
    let preData = {
      Type: 'No data'
    }

    preData['Total'] = 'No data'
    dataset.push(preData)
  }

  return dataset
}

export const ResourcesUseCSV = {
  getResourcesUseStats(startDate, endDate, condoId, lang) {
    console.time('resources use')
    const typeFilter = prepareResourcesUseData(condoId, lang)
    const data = getResourcesUse(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      typeFilter,
      lang
    )
    console.timeEnd('resources use')
    return data
  }
}
