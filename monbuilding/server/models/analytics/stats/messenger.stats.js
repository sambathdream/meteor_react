import { Stats, CommonTranslation } from '/common/lang/lang.js'
import { MongoObservable } from 'meteor-rxjs';

DayMessengers = (messengers, start, end, lang, condos, condosData) => {
	let data = [];
  let divisor = 0;
  const tr_common = new CommonTranslation(lang)

	let receive = _.filter(messengers, function(msg) {
		return !Residents.find({ userId: msg.userId })
	})
	let send = _.filter(messengers, function(msg) {
		return Residents.find({ userId: msg.userId })
	})
	let receiveEachDay = _.countBy(
    _.flatten([
      _.map(receive, function(msgOfGestionnaire) {
    		return moment(msgOfGestionnaire.date).format("DD[/]MM[/]YY");
      })
    ])
  )
	let sendEachDay = _.countBy(
    _.flatten([
      _.map(send, function (msgOfOccupant) {
    		return moment(msgOfOccupant.date).format("DD[/]MM[/]YY");
      })
    ])
  )

	let duration = end.diff(start, 'days')
	for (var i = 0; i < duration; i++) {
		divisor = 0;
		let day = {
			types: {
				send: sendEachDay[start.format("DD[/]MM[/]YY")] || 0,
				receive: receiveEachDay[start.format("DD[/]MM[/]YY")] || 0,
				time: 0,
			},
			date: start.locale(lang).format('ddd DD MMM'),
			total: (receiveEachDay[start.format("DD[/]MM[/]YY")] || 0) + (sendEachDay[start.format("DD[/]MM[/]YY")] || 0)
		};
		let msgOfDay = _.filter(receive, function(response) { return moment(response.date).format("DD[/]MM[/]YY") == start.format("DD[/]MM[/]YY")});
		_.each(msgOfDay, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, { sort: {
          date: -1
        }
      })
			if (conv && Residents.find({ userId: conv.userId })) {
				day.types.time += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});

    day.types.time /= divisor;
		if (!day.types.time) {
			if (data[i-1]) {
				day.types.time = data[i-1].time;
			}
		}

    day.types.time = day.types.time || 0; // for remove NaN;
    day.types.time = parseFloat(day.types.time.toFixed(1)) || 0

    if (day.types.time >= 1440) {
      if (day.types.time % 1440 == 0) {
        day.types.time = parseInt(day.types.time / 1440) + tr_common.commonTranslation["day_short"]
      } else {
        if (day.types.time % 60 == 0) {
          day.types.time = parseInt(day.types.time / 1440) + tr_common.commonTranslation["day_short"];
        }
        day.types.time = parseInt(day.types.time / 1440) + tr_common.commonTranslation["day_short"] + ' ' + parseInt(day.types.time % 1440 / 60) + 'h';
      }
    } else if (day.types.time >= 60) {
        if (day.types.time % 60 == 0) {
          day.types.time = parseInt(day.types.time / 60) + 'h'
        } else {
          day.types.time = parseInt(day.types.time / 60) + 'h' + parseInt(day.types.time % 60)
        }
    } else if (day.types.time >= 1) {
      day.types.time = parseInt(day.types.time) + ' min';
    }

    for (type in day.types) {
      let pushData = {}
      pushData['Date'] = day.date
      if (type === 'send') {
        pushData['Type'] = tr_common.commonTranslation['message_sent']
        pushData['Total'] = day.types.send
      } else if (type === 'receive') {
        pushData['Type'] = tr_common.commonTranslation['message_received']
        pushData['Total'] = day.types.receive
      } else {
        pushData['Type'] = tr_common.commonTranslation['average_response_time']
        pushData['Total'] = day.types.time
      }
      data.push(pushData)
    }
		start.add(1, "d");
	}
	return data
}

WeekMessengers = (messengers, start, end, lang, condos, condosData) => {
	let data = [];
	let divisor = 0;
  const tr_common = new CommonTranslation(lang)

	let receive = _.filter(messengers, msg => {
		return !Residents.find({ userId: msg.userId })
	})

  let send = _.filter(messengers, msg => {
    return Residents.find({ userId: msg.userId })
	})

  let receiveEachWeek = _.countBy(
    _.flatten([
      _.map(receive, msgOfGestionnaire => {
      		return moment(msgOfGestionnaire.date).format("W YYYY");
      })
    ])
  )

  let sendEachWeek = _.countBy(
    _.flatten([
      _.map(send, msgOfOccupant => {
    		return moment(msgOfOccupant.date).format("W YYYY");
      })
    ])
  )

  let duration = end.diff(start, 'weeks')
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		divisor = 0;
		let week = {
			types: {
				send: sendEachWeek[start.format("W YYYY")] || 0,
				receive: receiveEachWeek[start.format("W YYYY")] || 0,
				time: 0,
			},
			date: tr_common.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
			total: (receiveEachWeek[start.format("W YYYY")] || 0) + (sendEachWeek[start.format("W YYYY")] || 0)
		};

    let msgOfWeek = _.filter(receive, response => moment(response.date).format("W YYYY") == start.format("W YYYY"))

    _.each(msgOfWeek, function(_msg) {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, { sort: {
          date: -1
        }
      })
			if (conv && Residents.find({ userId: conv.userId })) {
				week.types.time += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		});

    week.types.time /= divisor;
		if (!week.types.time) {
			if (data[i-1]) {
				week.types.time = data[i-1].time;
			}
		}

    week.types.time = week.types.time || 0; // for remove NaN;
    week.types.time = parseFloat(week.types.time.toFixed(1)) || 0

    if (week.types.time >= 1440) {
      if (week.types.time % 1440 == 0) {
        week.types.time = parseInt(week.types.time / 1440) + tr_common.commonTranslation["day_short"]
      } else {
        if (week.types.time % 60 == 0) {
          week.types.time = parseInt(week.types.time / 1440) + tr_common.commonTranslation["day_short"];
        }
        week.types.time = parseInt(week.types.time / 1440) + tr_common.commonTranslation["day_short"] + ' ' + parseInt(week.types.time % 1440 / 60) + 'h';
      }
    } else if (week.types.time >= 60) {
        if (week.types.time % 60 == 0) {
          week.types.time = parseInt(week.types.time / 60) + 'h'
        } else {
          week.types.time = parseInt(week.types.time / 60) + 'h' + parseInt(week.types.time % 60)
        }
    } else if (week.types.time >= 1) {
      week.types.time = parseInt(week.types.time) + ' min';
    }

    for (type in week.types) {
      let pushData = {}
      pushData['Date'] = week.date
      if (type === 'send') {
        pushData['Type'] = tr_common.commonTranslation['message_sent']
        pushData['Total'] = week.types.send
      } else if (type === 'receive') {
        pushData['Type'] = tr_common.commonTranslation['message_received']
        pushData['Total'] = week.types.receive
      } else {
        pushData['Type'] = tr_common.commonTranslation['average_response_time']
        pushData['Total'] = week.types.time
      }
      data.push(pushData)
    }
		start.add(1, "weeks");
	}
	return data
}

MonthMessengers = (messengers, start, end, lang, condos, condosData) => {
	let data = [];
  let divisor = 0;
  const tr_common = new CommonTranslation(lang)

  let receive = _.filter(messengers, msg => {
    return !Residents.findOne({
      userId: msg.userId
    })
  });
  // console.log('receive.length ', receive.length )

  let send = _.filter(messengers, msg => {
		return Residents.findOne({
      userId: msg.userId
    })
  })
  // console.log('send.length   ', send.length   )

  let receiveEachMonth = _.countBy(
    _.flatten([
      _.map(receive, msgOfGestionnaire => moment(msgOfGestionnaire.date).format("MM[/]YYYY"))
    ])
  )
  // console.log('receiveEachMonth ', receiveEachMonth )

  let sendEachMonth = _.countBy(
    _.flatten([
      _.map(send, msgOfOccupant => moment(msgOfOccupant.date).format("MM[/]YYYY"))
    ])
  )
  // console.log('sendEachMonth ', sendEachMonth )

	let duration = end.diff(start, 'month')
	for (var i = 0; i <= duration; i++) {
		divisor = 0;
		let month = {
			types: {
				send: sendEachMonth[start.format("MM[/]YYYY")] || 0,
				receive: receiveEachMonth[start.format("MM[/]YYYY")] || 0,
				time: 0,
			},
      date: !(start.format('YYYY') === moment().format('YYYY'))
            ? start.locale(lang).format('MMMM YYYY')
            : start.locale(lang).format('MMMM'),
			total: (receiveEachMonth[start.format("MM[/]YYYY")] || 0) + (sendEachMonth[start.format("MM[/]YYYY")] || 0)
		};

    let msgOfMonth = _.filter(receive, response => moment(response.date).format("MM[/]YYYY") === start.format("MM[/]YYYY"))

    _.each(msgOfMonth, _msg => {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, { sort: {
          date: -1
        }
      })
			if (conv && Residents.find({ userId: conv.userId })) {
				month.types.time += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
    })

    month.types.time /= divisor;
		if (!month.types.time) {
			if (data[i-1]) {
				month.types.time = data[i-1].time
			}
		}

    month.types.time = month.types.time || 0; // for remove NaN;
    month.types.time = parseFloat(month.types.time.toFixed(1)) || 0

    if (month.types.time >= 1440) {
      if (month.types.time % 1440 == 0) {
        month.types.time = parseInt(month.types.time / 1440) + tr_common.commonTranslation["day_short"]
      } else {
        if (month.types.time % 60 == 0) {
          month.types.time = parseInt(month.types.time / 1440) + tr_common.commonTranslation["day_short"];
        }
        month.types.time = parseInt(month.types.time / 1440) + tr_common.commonTranslation["day_short"] + ' ' + parseInt(month.types.time % 1440 / 60) + 'h';
      }
    } else if (month.types.time >= 60) {
        if (month.types.time % 60 == 0) {
          month.types.time = parseInt(month.types.time / 60) + 'h'
        } else {
          month.types.time = parseInt(month.types.time / 60) + 'h' + parseInt(month.types.time % 60)
        }
    } else if (month.types.time >= 1) {
      month.types.time = parseInt(month.types.time) + ' min';
    }

    for (type in month.types) {
      let pushData = {}
      pushData['Date'] = month.date
      if (type === 'send') {
        pushData['Type'] = tr_common.commonTranslation['message_sent']
        pushData['Total'] = month.types.send
      } else if (type === 'receive') {
        pushData['Type'] = tr_common.commonTranslation['message_received']
        pushData['Total'] = month.types.receive
      } else {
        pushData['Type'] = tr_common.commonTranslation['average_response_time']
        pushData['Total'] = month.types.time
      }
      data.push(pushData)
    }
		start.add(1, "M");
  }

  return data
}

YearMessengers = (messengers, start, end, lang, condos, condosData) => {
	let data = [];
  let divisor = 0;
  const tr_common = new CommonTranslation(lang)

	let receive = _.filter(messengers, msg => {
		return !Residents.find({ userId: msg.userId })
  })
	let send = _.filter(messengers, msg => {
		return Residents.find({ userId: msg.userId })
	})
	let receiveEachYear = _.countBy(
    _.flatten([
      _.map(receive, msgOfGestionnaire => {
		    return moment(msgOfGestionnaire.date).format("YYYY");
      })
    ])
  )
	let sendEachYear = _.countBy(
    _.flatten([
      _.map(send, msgOfOccupant => {
		    return moment(msgOfOccupant.date).format("YYYY");
      })
    ])
  )

	let duration = end.diff(start, 'year')
	for (var i = 0; i <= duration; i++) {
		divisor = 0;
		let year = {
			types: {
				send: sendEachYear[start.format("YYYY")] || 0,
				receive: receiveEachYear[start.format("YYYY")] || 0,
				time: 0,
			},
			date: start.format("YYYY"),
			total: (receiveEachYear[start.format("YYYY")] || 0) + (sendEachYear[start.format("YYYY")] || 0)
		}

    let msgOfYear = _.filter(receive, response => { return moment(response.date).format("YYYY") == start.format("YYYY")});
		_.each(msgOfYear, _msg => {
			let conv = MessagesFeed.findOne({
				"messageId": _msg.messageId,
				"date": {$lt: _msg.date}
			}, { sort: {
          date: -1
        }
      })
			if (conv && Residents.find({ userId: conv.userId })) {
				year.types.time += ((((_msg.date - conv.date) / 1000) / 60));
				divisor++;
			}
		})

    year.types.time /= divisor;
		if (!year.types.time) {
			if (data[i-1]) {
				year.types.time = data[i-1].time;
			}
		}

    year.types.time = year.types.time || 0; // for remove NaN;
    year.types.time = parseFloat(year.types.time.toFixed(1)) || 0

    if (year.types.time >= 1440) {
      if (year.types.time % 1440 == 0) {
        year.types.time = parseInt(year.types.time / 1440) + tr_common.commonTranslation["day_short"]
      } else {
        if (year.types.time % 60 == 0) {
          year.types.time = parseInt(year.types.time / 1440) + tr_common.commonTranslation["day_short"];
        }
        year.types.time = parseInt(year.types.time / 1440) + tr_common.commonTranslation["day_short"] + ' ' + parseInt(year.types.time % 1440 / 60) + 'h';
      }
    } else if (year.types.time >= 60) {
        if (year.types.time % 60 == 0) {
          year.types.time = parseInt(year.types.time / 60) + 'h'
        } else {
          year.types.time = parseInt(year.types.time / 60) + 'h' + parseInt(year.types.time % 60)
        }
    } else if (year.types.time >= 1) {
      year.types.time = parseInt(year.types.time) + ' min';
    }

    for (type in year.types) {
      let pushData = {}
      pushData['Date'] = year.date
      if (type === 'send') {
        pushData['Type'] = tr_common.commonTranslation['message_sent']
        pushData['Total'] = year.types.send
      } else if (type === 'receive') {
        pushData['Type'] = tr_common.commonTranslation['message_received']
        pushData['Total'] = year.types.receive
      } else {
        pushData['Type'] = tr_common.commonTranslation['average_response_time']
        pushData['Total'] = year.types.time
      }
      data.push(pushData)
    }
		start.add(1, "y");
	}
	return data
}

getMessengerCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getMessenger = (start, end, condoId, lang) => {
  const condos = condoId === 'all' ? Meteor.getManagerCondoIds() : [condoId]
  // const condosData = getMessengerCondoName(condos)

  const types = ['send-receive', 'response']

	let duration = 0;

  const conversationIds = _.map(Messages.find({
    condoId: { $in: condos },
    target: 'manager'
  }).fetch(), conv => conv._id)
	const messengers = MessagesFeed.find({
		$and: [
      { "messageId": { $in: conversationIds } },
      { "date": { $gte: parseInt(start.format('x')), $lt: parseInt(end.format('x')) } }
    ]
  }).fetch()


	if ((duration = end.diff(start, 'year')) > 1) {
		res = YearMessengers(messengers, start, end, lang, condos)
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
		res = MonthMessengers(messengers, start, end, lang, condos)
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
		res = WeekMessengers(messengers, start, end, lang, condos)
	}
	else {
		res = DayMessengers(messengers, start, end, lang, condos)
	}

  return res;
}

export const MessengerCSV = {
  getMessengerStats(startDate, endDate, condoId, lang) {
    console.time('messenger')
    const data = getMessenger(moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      condoId,
      lang
    )
    // console.log('data.length ', data.length )
    console.timeEnd('messenger')
    return data
  }
}