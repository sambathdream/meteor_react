import moment from 'moment'

getBookedResourcesTypes = (condoId, lang) => {
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId } )

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]

  let resources = attachResourceType(Resources.find({condoId: {$in: condos}}).fetch(), lang)

  const data = {}

  resources.forEach((res) => {
      if (!data[res.type]) {
          data[res.type] = {
              name: res.typeName,
              ids: [res._id],
              condoId: res.condoId
          }
      } else {
          data[res.type].ids.push(res._id)
      }
  })

  return {data, condos}
}

getBookedResourcesCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

prepareBookedResourcesDataset = (start, end, condoId, lang) => {
  const resources = getBookedResourcesTypes(condoId, lang)
  const condosData = getBookedResourcesCondoName(resources.condos)

  let dataset = _.map(resources.data, type => {
      let data = Reservations.find({
        $and: [
            {resourceId: {$in: type.ids}},
            {start: {$gte: +moment(parseInt(start)).locale(lang).format('x')}},
            {start: {$lte: +moment(parseInt(end)).locale(lang).format('x')}},
            {condoId: {$in: resources.condos}},
            {
                $or: [
                    {pending: { $exists: false }},
                    {pending: false}
                ]
            },
            {
                $or: [
                    {rejected: { $exists: false }},
                    {rejected: false}
                ]
            }
        ]
      }).fetch()


      let preData = {
        Type: type.name
      }

      if (condosData.length > 1) {
        condosData.forEach(cd => {
          preData[cd.condoName] = 0
        })
      }

      let dataCount = _.reduce(data, (count, d) => {
        if (condosData.length > 1) {
          preData[condosData.find(
            condo => condo.condoId === d.condoId
          ).condoName] += moment(d.end).locale(lang).diff(moment(d.start), 'm')
        }
        return count + moment(d.end).diff(moment(d.start), 'm')
      }, 0)

      if (condosData.length > 1) {
        condosData.forEach(cd => {
          if (preData[cd.condoName] >= 60) {
            if (preData[cd.condoName] % 60 === 0) {
              preData[cd.condoName] = parseInt(preData[cd.condoName] / 60) + 'h'
            } else {
              preData[cd.condoName] = parseInt(preData[cd.condoName] / 60) + 'h' + preData[cd.condoName] % 60
            }
          } else if (preData[cd.condoName] !== 0) {
            preData[cd.condoName] =  preData[cd.condoName] + 'min';
          }
        })
      }

      if (dataCount >= 60) {
        if (dataCount % 60 == 0)
            dataCount =  parseInt(dataCount / 60) + 'h';
        else
            dataCount =  parseInt(dataCount / 60) + 'h' + dataCount % 60;
      } else if (dataCount != 0) {
          dataCount =  dataCount + 'min';
      }

      preData['Total'] = dataCount

      return preData
  })
  return dataset
}

getBookedResources = (start, end, condoId, lang) => {
  const bookedResources = prepareBookedResourcesDataset(start, end, condoId, lang)
  return bookedResources
}

export const BookedResourcesCSV = {
  getBookedResourcesStats(startDate, endDate, condoId, lang) {
    console.time('booked resources')
    const data = getBookedResources(startDate, endDate, condoId, lang)
    console.timeEnd('booked resources')
    return data
  }
}
