import { CommonTranslation } from "/common/lang/lang";

getResourcesDistributionTypes = (condoId, lang) => {
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({
    'users.userId': userId
  }).users, u => { return u.userId === userId })

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({
    _id: { $in: condosId }
  }).fetch(), '_id') : [condoId]

  let resources = attachResourceType(Resources.find({ condoId: {$in: condos} }).fetch(), lang)

  const data = {}
  const resourceIds = []

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id]
      }
    } else {
      data[res.type].ids.push(res._id)
    }
    resourceIds.push(res._id)
  })

  return { data, condos, resourceIds }
}

prepareResourcesDistributionData = (condoId, lang) => {
  const resourcesType = getResourcesDistributionTypes(condoId, lang)
  // console.log('resourcesType : ', resourcesType)
  const tr_common = new CommonTranslation(lang)
  const typeFilter = (_.size(resourcesType.data) > 0 ? _.map(resourcesType.data, (d, i) => {
    return {
      type: i,
      ...d
    }
  }) : [{
    type: 'empty',
    name: tr_common.commonTranslation["no_resources"],
    ids: []
  }])

  // console.log('typeFilter : ', typeFilter)
  // if (_.size(resourcesType) > 0) {
  //    this.selectedType.set(Object.keys(resourcesType)[0])
  // } else {
  //     this.selectedType.set('empty')
  // }
  return {
    types: typeFilter,
    condos: resourcesType.condos,
    resourceIds: resourcesType.resourceIds
  }
}

setDefaultResourcesDistributionData = (id) => {
  const resource = Resources.findOne({ _id: id }) || {}
  return {
    id: id,
    label: resource.name,
    data: [],
  }
}

dayResourcesDistributionData = (book, dataset, types, start, end, condosData) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'days')
  for (let i = 0; i < duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, b => {
        return b.resourceId === d.id
      })

      const tmpData = {}
      condosData.forEach(cd => {
        tmpData[cd.condoName] = 0
      })

      const count = _.countBy(
        _.flattenDeep([
          _.map(resourceBook, book => {
              if (moment(book.start).format("DD[/]MM[/]YY") === start.format("DD[/]MM[/]YY")) {
                tmpData[condosData.find(
                  condo => condo.condoId === book.condoId
                ).condoName] += 1
              }
              return moment(book.start).format("DD[/]MM[/]YY")
            })
          ])
        )[start.format("DD[/]MM[/]YY")]

      const type = types.find(t => t.ids.includes(d.id))

      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: start.format('ddd DD MMM'),
        count: !!count ? count : 0,
        tmpData,
      })
      return {
        ...d,
        data: newData
      }
    })

    date.push(start.format('ddd DD MMM'))
    start.add(1, "d")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Date'] = d.date
      pushData['Type'] = d.type
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = d.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = d.count
      data.push(pushData)
    })
  })

  return data
}

weekResourcesDistributionData = (book, dataset, types, start, end, lang, condosData) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'weeks')
  const translation = new CommonTranslation(lang)
  for (let i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone();
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, b => {
        return b.resourceId === d.id
      })

      const tmpData = {}
      condosData.forEach(cd => {
        tmpData[cd.condoName] = 0
      })

      const count = _.countBy(
        _.flattenDeep([
          _.map(resourceBook, book => {
              if (moment(book.start).format("W YYYY") === start.format("W YYYY")) {
                tmpData[condosData.find(
                  condo => condo.condoId === book.condoId
                ).condoName] += 1
              }
              return moment(book.start).format("W YYYY")
            })
          ])
        )[start.format("W YYYY")]

      const type = types.find(t => t.ids.includes(d.id))

      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') === tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').format('DD MMM'))),
        count: !!count ? count : 0,
        tmpData,
      })
      return {
        ...d,
        data: newData
      }
    })

    date.push(translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') === tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').format('DD MMM'))))
    start.add(1, "weeks")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Date'] = d.date
      pushData['Type'] = d.type
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = d.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = d.count
      data.push(pushData)
    })
  })

  return data
}

monthResourcesDistributionData = (book, dataset, types, start, end, condosData) => {
  const date = []
  let data = []

  // console.log('dataset ', dataset )
  let duration = end.diff(start, 'month')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, d => {
      const resourceBook = _.filter(book, (b) => {
        return b.resourceId === d.id
      })

      const tmpData = {}
      condosData.forEach(cd => {
        tmpData[cd.condoName] = 0
      })

      const count = _.countBy(
        _.flattenDeep([
          _.map(resourceBook, (book) => {
              if (moment(book.start).format("MM[/]YYYY") === start.format("MM[/]YYYY")) {
                tmpData[condosData.find(
                  condo => condo.condoId === book.condoId
                ).condoName] += 1
              }
              return moment(book.start).format("MM[/]YYYY")
            })
          ])
        )[start.format("MM[/]YYYY")]

      const type = types.find(t => t.ids.includes(d.id))
      // console.log('d ', d )

      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: !(start.format('YYYY') === moment().format('YYYY'))
              ? start.format('MMMM YYYY')
              : start.format('MMMM'),
        count: !!count ? count : 0,
        tmpData,
      })
      return {
        ...d,
        data: newData
      }
    })

    date.push(!(start.format('YYYY') === moment().format('YYYY'))
      ? start.format('MMMM YYYY')
      : start.format('MMMM'))
    start.add(1, "M")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Date'] = d.date
      pushData['Type'] = d.type
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = d.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = d.count
      data.push(pushData)
    })
  })

  return data
}

yearResourcesDistributionData = (book, dataset, types, start, end, condosData) => {
  const date = []
  let data = []

  let duration = end.diff(start, 'year')
  for (let i = 0; i <= duration; i++) {
    dataset = _.map(dataset, (d) => {
      const resourceBook = _.filter(book, b => {
        return b.resourceId === d.id
      })

      const tmpData = {}
      condosData.forEach(cd => {
        tmpData[cd.condoName] = 0
      })

      const count = _.countBy(
        _.flattenDeep([
          _.map(resourceBook, book => {
              if (moment(book.start).format("YYYY") === start.format("YYYY")) {
                tmpData[condosData.find(
                  condo => condo.condoId === book.condoId
                ).condoName] += 1
              }
              return moment(book.start).format("YYYY")
            })
          ])
        )[start.format("YYYY")]

      const type = types.find(t => t.ids.includes(d.id))

      const newData = d.data
      newData.push({
        type: type.name + '_' + d.label,
        date: start.format("YYYY"),
        count: !!count ? count : 0,
        tmpData,
      })
      return {
        ...d,
        data: newData
      }
    })

    date.push(start.format("YYYY"))
    start.add(1, "y")
  }

  dataset.forEach(ds => {
    ds.data.forEach(d => {
      let pushData = {}
      pushData['Date'] = d.date
      pushData['Type'] = d.type
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = d.tmpData[cd.condoName]
        })
      }
      pushData['Total'] = d.count
      data.push(pushData)
    })
  })

  return data
}

getResourcesDistributionCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getResourcesDistribution = (start, end, typeFilter, lang, timeType) => {
  const condosData = getResourcesDistributionCondoName(typeFilter.condos)
  // console.log('typeFilter ', typeFilter )
  // let selected = _.find(typeFilter.types, (o) => {
  //   return o.type === type
  // });
  // console.log('selected : ', selected)

  // if (!selected) {
  //   selected = {
  //     ids: []
  //   }
  // };
  const dataset = _.map(typeFilter.resourceIds, id => {
    return setDefaultResourcesDistributionData(id)
  })
  // dataset.forEach(d => {
  //   console.log('d.label ', d.label )
  // })
  // console.log('dataset : ', dataset)

  let res = []
  if (timeType === 'date') {
    // DATE TYPE
    let book = Reservations.find({
      $and: [
        { resourceId: { $in: typeFilter.resourceIds } },
        { start: { $gte: +start.locale(lang).format('x') } },
        { start: { $lte: +end.locale(lang).format('x') } },
        { condoId: {$in: typeFilter.condos} },
        {
          $or: [
            { pending: { $exists: false } },
            { pending: false }
          ]
        },
        {
          $or: [
            { rejected: { $exists: false } },
            { rejected: false }
          ]
        }
      ]
    }).fetch()

    // console.log('book : ', book)
    // console.log('book.length : ', book.length)

    if (end.diff(start, 'year') > 1) {
      res = yearResourcesDistributionData(book, dataset, typeFilter.types, start, end, condosData)
    } else if (end.diff(start, 'month') > 2) {
      res = monthResourcesDistributionData(book, dataset, typeFilter.types, start, end, condosData)
    } else if (end.diff(start, 'days') > 31) {
      res = weekResourcesDistributionData(book, dataset, typeFilter.types, start, end, condosData)
    } else {
      res = dayResourcesDistributionData(book, dataset, typeFilter.types, start, end, condosData)
    }
  } else {
    // TIME TYPE
    const label = ['0:00', '2:00','4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00', '24:00' ];

    const loop = _.map(dataset, (d) => {
      const book = Reservations.find({
        $and: [
          { resourceId: d.id },
          { start: { $gte: +start.locale(lang).format('x') } },
          { start: { $lte: +end.locale(lang).format('x') } },
          {
            $or: [
              { pending: { $exists: false } },
              { pending: false }
            ]
          },
          {
            $or: [
              { rejected: { $exists: false } },
              { rejected: false }
            ]
          }
        ]
      }).fetch();
      const counter = _.countBy(_.flattenDeep([_.map(book, function (book) {
        return moment(book.start).format("h");
      })]));

      const newData = _.map(label, (time) => {
        const hour = parseInt(time.split(':')[0], 10);
        let count = !!counter[hour] ? counter[hour] : 0;

        res.push({
          'Time': time,
          'Type': d.label,
          'Total': count
        })
      });

      return true
    })
  }

  return res
}

export const ResourcesDistributionCSV = {
  getResourcesDistributionStats(startDate, endDate, condoId, lang, time = false) {
    console.time('resource distribution')
    const typeFilter = prepareResourcesDistributionData(condoId, lang)
    // console.log('typeFilter : ', typeFilter)
    const data = getResourcesDistribution(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      typeFilter,
      lang,
      time ? 'time': 'date'
    )
    console.timeEnd('resource distribution')
    // console.log('data : ', data)
    return data
  }
}
