import { CommonTranslation } from '/common/lang/lang.js'

function getUsersByDay(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('DD[/]MM[/]YY')} && moment n: ${moment(u.loginDate).format('DD[/]MM[/]YY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('DD[/]MM[/]YY') == moment(u.loginDate).format('DD[/]MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByWeek(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('W YYYY')} && moment n: ${moment(u.loginDate).format('W YYYY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('W YYYY') == moment(u.loginDate).format('W YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByMonth(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('MM[/]YY')} && moment n: ${moment(u.loginDate).format('MM[/]YY')}`)
      if (w.userId === u.userId && moment(w.loginDate).format('MM[/]YY') === moment(u.loginDate).format('MM[/]YY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function getUsersByYear(collection) {
  let users = []
  collection.map(w => {
    let exist = false
    users.map(u => {
      // console.log(`user id w: ${w.userId} && user id n: ${u.userId}`)
      // console.log(`moment w: ${moment(w.loginDate).format('YYYY')} && moment n: ${moment(u.loginDate).format('YYYY')}`)
      if (w.userId == u.userId && moment(w.loginDate).format('YYYY') == moment(u.loginDate).format('YYYY'))
        exist = true
    })
    if (!exist)
      users.push(w)
  })
  return users
}

function DayTotalUsersAccessType(start, end, lang, collection) {
  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByDay(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByDay(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByDay(collection.others)
  // console.log("mobile users: ", otherUsers)
  let data = []

  const duration = end.diff(start, 'days');
  for (var i = 0; i < duration; i++) {
    let tmpData = {
      types: [],
      date: start.locale(lang).format('ddd DD MMM'),
      total: 0
    }

    let webOfDay = _.filter(webUsers, (a) => {
      return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    })
    let mobileOfDay = _.filter(mobileUsers, (a) => {
      return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    })
    // let iosOfDay = _.filter(iosUsers, (a) => {
    // 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    // })
    // let androidOfDay = _.filter(androidUsers, (a) => {
    // 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    // })
    // let othersOfDay = _.filter(othersUsers, (a) => {
    // 	return moment(a.loginDate).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
    // })
    tmpData.types.push({ value: webOfDay.length, category: "web" })
    tmpData.types.push({ value: mobileOfDay.length, category: "mobile" })
    // tmpData.types.push({value: iosOfDay.length, category: "ios"})
    // tmpData.types.push({value: androidOfDay.length, category: "android"})
    // tmpData.types.push({value: othersOfDay.length, category: "others"})
    tmpData.total = (webOfDay.length + mobileOfDay.length)
    tmpData.types.map(d => {
      data.push({ Date: tmpData.date, Type: d.category, Total: d.value })
    })
    start.add(1, 'd')
  }
  return data
}

function WeekTotalUsersAccessType(start, end, lang, collection) {
  const translation = new CommonTranslation(lang)

  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByWeek(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByWeek(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByWeek(collection.others)
  // console.log("mobile users: ", otherUsers)
  let data = []

  const duration = end.diff(start, 'weeks');
  for (var i = 0; i <= duration; i++) {
    let tmpDate = start.startOf('weeks').clone();
    let tmpData = {
      types: [],
      date: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') === tmpDate.add(1, "weeks").format('MM[/]YYYY'))
        ? (start.startOf('weeks').format('DD MMM') + ' ' + start.endOf('week').format('DD MMM'))
        : (start.startOf('weeks').format('DD') + '-' + start.endOf('week').format('DD MMM'))),
      total: 0
    };

    let webOfWeek = _.filter(webUsers, (a) => {
      return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
    });
    let mobileOfWeek = _.filter(mobileUsers, (a) => {
      return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
    });
    // let iosOfWeek = _.filter(iosUsers, (a) => {
    // 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
    // })
    // let androidOfWeek = _.filter(androidUsers, (a) => {
    // 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
    // })
    // let othersOfWeek = _.filter(othersUsers, (a) => {
    // 	return moment(a.loginDate).format('W YYYY') == start.format('W YYYY');
    // })
    tmpData.types.push({ value: webOfWeek.length, category: "web" });
    tmpData.types.push({ value: mobileOfWeek.length, category: "mobile" });
    // tmpData.types.push({value: iosOfWeek.length, category: "ios"})
    // tmpData.types.push({value: androidOfWeek.length, category: "android"})
    // tmpData.types.push({value: othersOfWeek.length, category: "others"})
    tmpData.total = (webOfWeek.length + mobileOfWeek.length);
    tmpData.types.map(d => {
      data.push({ Date: tmpData.date, Type: d.category, Total: d.value })
    })
    start.add(1, 'weeks');
  }
  return data
}

function MonthTotalUsersAccessType(start, end, lang, collection) {
  let webUsers = getUsersByDay(collection.web)
  let mobileUsers = getUsersByDay(collection.mobile)
  // let iosUsers = getUsersByMonth(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByMonth(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByMonth(collection.others)
  // console.log("mobile users: ", otherUsers)
  let data = []

  const duration = end.diff(start, 'month');
  for (var i = 0; i <= duration; i++) {
    let tmpData = {
      types: [],
      date: !(start.format('YYYY') === moment().format('YYYY'))
        ? start.locale(lang).format('MMMM YYYY')
        : start.locale(lang).format('MMMM'),
      total: 0
    };

    let webOfMonth = _.filter(webUsers, (a) => {
      return moment(a.loginDate).format('MM[/]YY') === start.format('MM[/]YY');
    });
    let mobileOfMonth = _.filter(mobileUsers, (a) => {
      return moment(a.loginDate).format('MM[/]YY') === start.format('MM[/]YY');
    });
    // let iosOfMonth = _.filter(iosUsers, (a) => {
    // 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
    // })
    // let androidOfMonth = _.filter(androidUsers, (a) => {
    // 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
    // })
    // let othersOfMonth = _.filter(othersUsers, (a) => {
    // 	return moment(a.loginDate).format('MM[/]YY') == start.format('MM[/]YY');
    // })
    tmpData.types.push({ value: webOfMonth.length, category: "web" });
    tmpData.types.push({ value: mobileOfMonth.length, category: "mobile" })
    // tmpData.types.push({value: iosOfMonth.length, category: "ios"})
    // tmpData.types.push({value: androidOfMonth.length, category: "android"})
    // tmpData.types.push({value: othersOfMonth.length, category: "others"})
    tmpData.total = (webOfMonth.length + mobileOfMonth.length);
    tmpData.types.map(d => {
      data.push({ Date: tmpData.date, Type: d.category, Total: d.value })
    })
    start.add(1, 'M');
  }
  return data
}

function YearTotalUsersAccessType(start, end, collection) {
  let webUsers = getUsersByDay(collection.web)
  // console.log("web users: ", webUsers)
  let mobileUsers = getUsersByDay(collection.mobile)
  // console.log("mobile users: ", iosUsers)
  // let iosUsers = getUsersByYear(collection.ios)
  // console.log("mobile users: ", iosUsers)
  // let androidUsers = getUsersByYear(collection.android)
  // console.log("mobile users: ", androidUsers)
  // let othersUsers = getUsersByYear(collection.others)
  // console.log("mobile users: ", otherUsers)

  const duration = end.diff(start, 'year');
  for (var i = 0; i <= duration; i++) {
    let tmpData = {
      types: [],
      date: start.format('YYYY'),
      total: 0
    };

    let webOfYear = _.filter(webUsers, (a) => {
      return moment(a.loginDate).format('YYYY') == start.format('YYYY');
    });
    let mobileOfYear = _.filter(mobileUsers, (a) => {
      return moment(a.loginDate).format('YYYY') == start.format('YYYY');
    });
    // let iosOfYear = _.filter(iosUsers, (a) => {
    // 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
    // })
    // let androidOfYear = _.filter(androidUsers, (a) => {
    // 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
    // })
    // let othersOfYear = _.filter(othersUsers, (a) => {
    // 	return moment(a.loginDate).format('YYYY') == start.format('YYYY');
    // })
    tmpData.types.push({ value: webOfYear.length, category: "web" });
    tmpData.types.push({ value: mobileOfYear.length, category: "mobile" });
    // tmpData.types.push({value: iosOfYear.length, category: "ios"})
    // tmpData.types.push({value: androidOfYear.length, category: "android"})
    // tmpData.types.push({value: othersOfYear.length, category: "others"})
    tmpData.total = (webOfYear.length + mobileOfYear.length);
    tmpData.types.map(d => {
      data.push({ Date: tmpData.date, Type: d.category, Total: d.value })
    })
    start.add(1, 'y');
  }
  return data
}

// getTotalUsersAccessTypeCondoName = (condoId) => {
//   const userId = Meteor.userId()

//   const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId })

//   let condosId = []
//   if (users) {
//     condosId = _.map(users.condosInCharge, 'condoId')
//   }
//   let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]

//   return condos.map(c => {
//     const condo = Condos.findOne(c);
//     if (condo) {
//       if (condo.info.id != -1) {
//         return {
//           condoName: condo.info.id + ' - ' + condo.getName(),
//           condoId: c
//         }
//       } else {
//         return {
//           condoName: condo.getName(),
//           condoId: c
//         }
//       }
//     }
//   })
// }

getTotalUsersAccessType = (start, end, condoId, lang) => {
  let condos = condoId === 'all' ? Meteor.getManagerCondoIds() : [condoId]

  let res;
  // console.log('condoId : ', condoId)
  // console.log('getManager : ', Meteor.getManagerCondoIds());
  // console.log('condos ', condos )
  const usersId = _.map(Residents.find({
    'condos.condoId': { $in: condos }
  }, { fields: { userId: true } }).fetch(), "userId")
  // console.log("userIds: ", usersId)

  // Retrieve web users
  const web = Analytics.find({
    $and: [
      { loginDate: { $gte: +start.format('x') } },
      { loginDate: { $lte: +end.format('x') } },
      { userId: { $in: usersId } },
      { condoId: { $in: condos } },
      { accessType: "web" },
      { type: "login" }
    ]
  }).fetch()

  // Retrieve Mobile users
  const mobile = Analytics.find({
    $and: [
      { loginDate: { $gte: +start.format('x') } },
      { loginDate: { $lte: +end.format('x') } },
      { userId: { $in: usersId } },
      { condoId: { $in: [...condos, null] } },
      {
        $or: [{
          accessType: "ios"
        }, {
          accessType: "android"
        }]
      },
      { type: "login" }
    ]
  }).fetch()

  // Retrieve IOS users
  // const ios = Analytics.find({
  //   $and: [
  //     {loginDate: {$gte: +start.format('x')}},
  //     {loginDate: {$lte: +end.format('x')}},
  //     {userId: { $in: usersId }},
  //     {accessType: "ios"},
  //     {type: "login"}
  //   ]
  // }).fetch()

  // // Retrieve Android users
  // const android = Analytics.find({
  //   $and: [
  //     {loginDate: {$gte: +start.format('x')}},
  //     {loginDate: {$lte: +end.format('x')}},
  //     {userId: { $in: usersId }},
  //     {accessType: "android"},
  //     {type: "login"}
  //   ]
  // }).fetch()

  // Retrieve Other OS users
  // const others = Analytics.find({
  //   userId: { $in: usersId },
  //   accessType: undefined,
  //   type: "login"
  // }).fetch()

  // console.log("web: ", web)
  // console.log("mobile: ", mobile)
  if ((duration = end.diff(start, 'year')) > 1) {
    res = YearTotalUsersAccessType(start, end, {
      "web": web,
      "mobile": mobile,
      // "ios": ios,
      // "android": android,
      // "others": others
    })
  }
  else if ((duration = end.diff(start, 'month')) > 2) {
    res = MonthTotalUsersAccessType(start, end, lang, {
      "web": web,
      "mobile": mobile,
      // "ios": ios,
      // "android": android,
      // "others": others
    })
  }
  else if ((duration = end.diff(start, 'days')) > 31) {
    res = WeekTotalUsersAccessType(start, end, lang, {
      "web": web,
      "mobile": mobile,
      // "ios": ios,
      // "android": android,
      // "others": others
    })
  }
  else {
    res = DayTotalUsersAccessType(start, end, lang, {
      "web": web,
      "mobile": mobile,
      // "ios": ios,
      // "android": android,
      // "others": others
    })
  }
  // console.log('res ', res )
  return res
}

export const TotalUsersAccessTypeCSV = {
  getTotalUsersAccessTypeStats(startDate, endDate, condoId, lang) {
    console.time('total users access type')
    const data = getTotalUsersAccessType(moment(parseInt(startDate)), moment(parseInt(endDate)), condoId, lang)
    console.timeEnd('total users access type')
    return data
  }
}
