import { Stats, CommonTranslation } from '/common/lang/lang.js'
// import { Stats } from "fs";

DayUtilisations = (start, end, lang, collection, condosData, conversations) => {
	let duration = 0;
  let tmpData = [];
  let data = []

	duration = end.diff(start, 'days');
	for (var i = 0; i < duration; i++) {
		tmpData = {
			types: [],
			date: start.locale(lang).format('ddd DD MMM'),
		}
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        forum: 0,
        incidents: 0,
        classifieds: 0,
        informations: 0,
        reservations: 0,
        conciergerie: 0,
        messenger: 0,
      }
    })
		let forumOfDay = _.filter(collection.forum, (a) => {
      if (moment(a.date).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].forum += 1
        return true
      }
			return false
    });
		let incidentsOfDay = _.filter(collection.incidents, (a) => {
      if (moment(a.createdAt).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		});
		let classifiedsOfDay = _.filter(collection.ads, (a) => {
      if (moment(a.createdAt).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		});
		let actusOfDay = _.filter(collection.actus, (a) => {
      if (moment(a.createdAt).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].informations += 1
        return true
      }
			return false
		});
		let resourcesOfDay = _.filter(collection.resources, (a) => {
			if (a != undefined) {
        return moment(a.start).format('DD[/]MM[/]YY') == start.format('DD[/]MM[/]YY');
        if (moment(a.date).format('W YYYY') === start.format('W YYYY')) {
          tmpData[condosData.find(
            condo => condo.condoId === a.condoId
          ).condoName].reservations += 1
          return true
        }
        return false
      }
		});
		let conciergeOfDay = _.filter(collection.concierge, (a) => {
      if (moment(a.date).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].conciergerie += 1
        return true
      }
			return false
		});
		let messengerOfDay = _.filter(collection.messenger, (a) => {
      if (moment(a.date).format('DD[/]MM[/]YY') === start.format('DD[/]MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === conversations.find(conv => conv._id === a.messageId).condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		});
		tmpData.types.push({value: forumOfDay.length, category: "forum"});
		tmpData.types.push({value: incidentsOfDay.length, category: "incidents"});
		tmpData.types.push({value: classifiedsOfDay.length, category: "classifieds"});
		tmpData.types.push({value: actusOfDay.length, category: "informations"});
		tmpData.types.push({value: resourcesOfDay.length, category: "reservations"});
		tmpData.types.push({value: conciergeOfDay.length, category: "conciergerie"});
		tmpData.types.push({value: messengerOfDay.length, category: "messenger"});
		// tmpData.total = (forumOfDay.length + incidentsOfDay.length + classifiedsOfDay.length + actusOfDay.length + resourcesOfDay.length + conciergeOfDay.length + messengerOfDay.length);
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = getCategoryTranslation(d.category, lang)
      if (condosData.length > 1) {
        if (condosData.length > 1) {
          condosData.forEach(cd => {
            pushData[cd.condoName] = tmpData[cd.condoName][d.category]
          })
        }
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'd');
	}
	return data
}

WeekUtilisations = (start, end, lang, collection, condosData, conversations) => {
	let duration = 0;
  let tmpData = [];
  let data = []
  const translation = new CommonTranslation(lang)

	duration = end.diff(start, 'weeks');
	for (var i = 0; i <= duration; i++) {
		let tmpDate = start.startOf('weeks').clone();
		tmpData = {
			types: [],
			date: translation.commonTranslation["week"] + ' ' + start.format('W') + ' ' + (!(start.startOf('weeks').format('MM[/]YYYY') == tmpDate.add(1, "weeks").format('MM[/]YYYY')) ? (start.startOf('weeks').format('DD MMM')  + ' ' + start.endOf('week').format('DD MMM')) : (start.startOf('weeks').format('DD')+'-'+start.endOf('week').format('DD MMM'))),
		}
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        forum: 0,
        incidents: 0,
        classifieds: 0,
        informations: 0,
        reservations: 0,
        conciergerie: 0,
        messenger: 0,
      }
    })
		let forumOfWeek = _.filter(collection.forum, (a) => {
      if (moment(a.date).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		});
		let incidentsOfWeek = _.filter(collection.incidents, (i) => {
      if (moment(i.createdAt).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		});
		let classifiedsOfWeek = _.filter(collection.ads, (i) => {
      if (moment(i.createdAt).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		});
		let actusOfWeek = _.filter(collection.actus, (i) => {
      if (moment(i.createdAt).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].informations += 1
        return true
      }
			return false
		});
		let resourcesOfWeek = _.filter(collection.resources, (i) => {
      if (i != undefined) {
        if (moment(i.start).format('W YYYY') === start.format('W YYYY')) {
          tmpData[condosData.find(
            condo => condo.condoId === i.condoId
          ).condoName].reservations += 1
          return true
        }
        return false
      }
		});
		let conciergeOfWeek = _.filter(collection.concierge, (i) => {
      if (moment(i.date).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].conciergerie += 1
        return true
      }
			return false
		});
		let messengerOfWeek = _.filter(collection.messenger, (i) => {
      if (moment(i.date).format('W YYYY') === start.format('W YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === conversations.find(conv => conv._id === i.messageId).condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		});
		tmpData.types.push({value: forumOfWeek.length, category: "forum"});
		tmpData.types.push({value: incidentsOfWeek.length, category: "incidents"});
		tmpData.types.push({value: classifiedsOfWeek.length, category: "classifieds"});
		tmpData.types.push({value: actusOfWeek.length, category: "informations"});
		tmpData.types.push({value: resourcesOfWeek.length, category: "reservations"});
		tmpData.types.push({value: conciergeOfWeek.length, category: "conciergerie"});
		tmpData.types.push({value: messengerOfWeek.length, category: "messenger"});
		// tmpData.total = (forumOfWeek.length + incidentsOfWeek.length + classifiedsOfWeek.length + actusOfWeek.length + resourcesOfWeek.length + conciergeOfWeek.length + messengerOfWeek.length);
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = getCategoryTranslation(d.category, lang)
      if (condosData.length > 1) {
        if (condosData.length > 1) {
          condosData.forEach(cd => {
            pushData[cd.condoName] = tmpData[cd.condoName][d.category]
          })
        }
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'weeks');
	}
	return data
}

MonthUtilisations = (start, end, lang, collection, condosData, conversations) => {
	let duration = 0;
  let tmpData = [];
  let data = []

  duration = end.diff(start, 'month');
	for (var i = 0; i <= duration; i++) {
		tmpData = {
			types: [],
      date: !(start.format('YYYY') === moment().format('YYYY'))
            ? start.locale(lang).format('MMMM YYYY')
            : start.locale(lang).format('MMMM'),
    }
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        forum: 0,
        incidents: 0,
        classifieds: 0,
        informations: 0,
        reservations: 0,
        conciergerie: 0,
        messenger: 0,
      }
    })
		let forumOfMonth = _.filter(collection.forum, (f) => {
      if (moment(f.date).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === f.condoId
        ).condoName].forum += 1
        return true
      }
			return false
    })
		let incidentsOfMonth = _.filter(collection.incidents, (i) => {
      if (moment(i.createdAt).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		})
		let classifiedsOfMonth = _.filter(collection.ads, (a) => {
      if (moment(a.createdAt).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
    })
    // console.log('classifiedsOfMonth ', classifiedsOfMonth.length)
		let actusOfMonth = _.filter(collection.actus, (a) => {
      if (moment(a.createdAt).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].informations += 1
        return true
      }
			return false
		})
		let resourcesOfMonth = _.filter(collection.resources, (r) => {
      if (r != undefined) {
        if (moment(r.start).format('MM[/]YY') === start.format('MM[/]YY')) {
          tmpData[condosData.find(
            condo => condo.condoId === r.condoId
          ).condoName].reservations += 1
          return true
        }
        return false
      }
		})
		let conciergeOfMonth = _.filter(collection.concierge, (c) => {
      if (moment(c.date).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === c.condoId
        ).condoName].conciergerie += 1
        return true
      }
			return false
		})
		let messengerOfMonth = _.filter(collection.messenger, (m) => {
      if (moment(m.date).format('MM[/]YY') === start.format('MM[/]YY')) {
        tmpData[condosData.find(
          condo => condo.condoId === conversations.find(conv => conv._id === m.messageId).condoId
        ).condoName].messenger += 1
        return true
      }
			return false
		})
		tmpData.types.push({value: forumOfMonth.length, category: "forum"})
		tmpData.types.push({value: incidentsOfMonth.length, category: "incidents"})
		tmpData.types.push({value: classifiedsOfMonth.length, category: "classifieds"})
		tmpData.types.push({value: actusOfMonth.length, category: "informations"})
		tmpData.types.push({value: resourcesOfMonth.length, category: "reservations"})
		tmpData.types.push({value: conciergeOfMonth.length, category: "conciergerie"})
		tmpData.types.push({value: messengerOfMonth.length, category: "messenger"})
    // tmpData.total = (forumOfMonth.length + incidentsOfMonth.length + classifiedsOfMonth.length + actusOfMonth.length + resourcesOfMonth.length + conciergeOfMonth.length + messengerOfMonth.length)
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = getCategoryTranslation(d.category, lang)
      if (condosData.length > 1) {
        if (condosData.length > 1) {
          condosData.forEach(cd => {
            pushData[cd.condoName] = tmpData[cd.condoName][d.category]
          })
        }
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'M')
  }
	return data
}

YearUtilisations = (start, end, collection, condosData, conversations, lang) => {
	let duration = 0;
  let tmpData = [];
  let data = []

	duration = end.diff(start, 'year');
	for (var i = 0; i <= duration; i++) {
		tmpData = {
			types: [],
			date: start.format('YYYY'),
		}
    condosData.forEach(cd => {
      tmpData[cd.condoName] = {
        forum: 0,
        incidents: 0,
        classifieds: 0,
        informations: 0,
        reservations: 0,
        conciergerie: 0,
        messenger: 0,
      }
    })
		let forumOfYear = _.filter(collection.forum, (a) => {
      if (moment(a.date).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === a.condoId
        ).condoName].forum += 1
        return true
      }
			return false
		});
		let incidentsOfYear = _.filter(collection.incidents, (i) => {
      if (moment(i.createdAt).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].incidents += 1
        return true
      }
			return false
		});
		let classifiedsOfYear = _.filter(collection.ads, (i) => {
      if (moment(i.createdAt).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].classifieds += 1
        return true
      }
			return false
		});
		let actusOfYear = _.filter(collection.actus, (i) => {
      if (moment(i.createdAt).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].informations += 1
        return true
      }
			return false
		});
		let resourcesOfYear = _.filter(collection.resources, (i) => {
      if (i != undefined) {
        if (moment(i.start).format('YYYY') === start.format('YYYY')) {
          tmpData[condosData.find(
            condo => condo.condoId === i.condoId
          ).condoName].reservations += 1
          return true
        }
        return false
      }
		});
		let conciergeOfYear = _.filter(collection.concierge, (i) => {
      if (moment(i.date).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === i.condoId
        ).condoName].conciergerie += 1
        return true
      }
      return false
    });
		let messengerOfYear = _.filter(collection.messenger, (i) => {
      if (moment(i.date).format('YYYY') === start.format('YYYY')) {
        tmpData[condosData.find(
          condo => condo.condoId === conversations.find(conv => conv._id === i.messageId).condoId
        ).condoName].messenger += 1
        return true
      }
      return false
		});
		tmpData.types.push({value: forumOfYear.length, category: "forum"});
		tmpData.types.push({value: incidentsOfYear.length, category: "incidents"});
		tmpData.types.push({value: classifiedsOfYear.length, category: "classifieds"});
		tmpData.types.push({value: actusOfYear.length, category: "informations"});
		tmpData.types.push({value: resourcesOfYear.length, category: "reservations"});
		tmpData.types.push({value: conciergeOfYear.length, category: "conciergerie"});
		tmpData.types.push({value: messengerOfYear.length, category: "messenger"});
		// tmpData.total = (forumOfYear.length + incidentsOfYear.length + classifiedsOfYear.length + actusOfYear.length + resourcesOfYear.length + conciergeOfYear.length + messengerOfYear.length);
    tmpData.types.forEach(d => {
      let pushData = {}
      pushData['Date'] = tmpData.date
      pushData['Type'] = getCategoryTranslation(d.category, lang)
      if (condosData.length > 1) {
        condosData.forEach(cd => {
          pushData[cd.condoName] = tmpData[cd.condoName][d.category]
        })
      }
      pushData['Total'] = d.value
      data.push(pushData)
    })
		start.add(1, 'y');
	}
	return data
}

getCategoryTranslation = (category, lang) => {
  const tr_common = new Stats(lang)

  switch (category) {
    case 'forum':
      return tr_common.stats['n_posts']
    case 'incidents':
      return tr_common.stats['n_incidents']
    case 'classifieds':
      return tr_common.stats['n_ads']
    case 'informations':
      return tr_common.stats['new_info']
    case 'reservations':
      return tr_common.stats['n_resa']
    case 'conciergerie':
      return tr_common.stats['n_keeper']
    case 'messenger':
      return tr_common.stats['n_sent']
    default:
      break;
  }
}

getUtilizationCondoName = (condos) => {
  return condos.map(c => {
    const condo = Condos.findOne(c);
    if (condo) {
      if (condo.info.id != -1) {
        return {
          condoName: condo.info.id + ' - ' + condo.getName(),
          condoId: c
        }
      } else {
        return {
          condoName: condo.getName(),
          condoId: c
        }
      }
    }
  })
}

getUtilization = (start, end, condoId, lang) => {
  // Just so I don't have to deal with 'all'
  const userId = Meteor.userId()

  const users = _.find(Enterprises.findOne({ 'users.userId': userId }).users, u => { return u.userId === userId } )

  let condosId = []
  if (users) {
    condosId = _.map(users.condosInCharge, 'condoId')
  }
  let condos = condoId === 'all' ? _.map(Condos.find({ _id: { $in: condosId } }).fetch(), '_id') : [condoId]
  const condosData = getUtilizationCondoName(condos)
  // ===================================================================================================

  let condosInCharge = Meteor.listCondoUserHasRight("actuality", "see")
  let actuList = [];
  for (condo of condosInCharge) {
    let c = Condos.findOne(condo);
    if (c && c.settings && c.settings.options && c.settings.options.informations === true) {
      let m = _.find(c.modules, (e) => e.name == "actuality")
      if (m) {
        actuList.push(m.data.actualityId);
      }
    }
  }

  // Don't ask why pls
  const count = undefined
  const search = undefined
  const regexp = !!search ? new RegExp(search, "i") : null
  let actus = ActuPosts.find({
    $and: [
      !!search ? {
        $or: [
          { "title": regexp },
          { "locate": regexp },
          { "description": regexp },
        ]
      } : {},
      { actualityId: { $in: actuList } }
    ]
  }, { sort: { createdAt: -1 }, limit: !!count && _.isNumber(count) ? count : 200 }).fetch()
  actus = _.filter(actus, at => condos.includes(at.condoId))

  // Classifieds
  // let condoIds = Meteor.listCondoUserHasRight("ads", "see")

  let classifiedsIds = []
  condos.forEach(condoId => {
    let condo = Condos.findOne(condoId)
    if (condo) {
      let modules = _.find(condo.modules, mod => mod.name == "classifieds")
      if (modules && modules.data)
        classifiedsIds.push(modules.data.classifiedsId)
    }
  })
  let classifiedsAdsIds = _.map(ClassifiedsAds.find({
    condoId: { $in: condos }
  }).fetch(), c => c._id)
  let classifieds = ClassifiedsAds.find({
    classifiedsId: { $in: classifiedsIds }
  }).fetch().concat(ClassifiedsFeed.find({
    classifiedId: { $in: classifiedsAdsIds }
  }).fetch())
  // console.log('classifieds.length ', classifieds.length )
  // ===================================================================================================

	let incidents = Incidents.find({condoId: { $in: condos } }).fetch()

  // Resources
  let resourcesTab = Resources.find({condoId: {$in: condos}}).fetch()
  const resourceIds = _.map(resourcesTab, (r) => {return r._id});
  // ====================================================================================================

  let resources = Reservations.find({
    $and: [
        {resourceId: {$in: resourceIds}},
        {start: {$gte: +start.locale(lang).format('x')}},
        {start: {$lte: +end.locale(lang).format('x')}},
        {condoId: {$in: condos}},
        {
            $or: [
                {pending: { $exists: false }},
                {pending: false}
            ]
        },
        {
            $or: [
                {rejected: { $exists: false }},
                {rejected: false}
            ]
        }
    ]
  }).fetch()

  let concierge = ConciergeMessage.find({condoId: { $in: condos } }).fetch()

  let forum = ForumPosts.find({condoId: {$in: condos}}).fetch().concat(ForumCommentPosts.find({condoId: {$in: condos}}).fetch())

  let conversations = Messages.find({
    condoId: { $in: condos },
    target: 'manager'
  }).fetch()

  const conversationIds = _.map(conversations, conv => conv._id)

  let managerIds = [];

  if (condoId === "all") {
		managerIds = _.map(Meteor.users.find({"identities.gestionnaireId": {$exists: true}}).fetch(), u => u._id)
	} else {
		managerIds = _.map(Enterprises.findOne({condos: condoId}).users, function(user) {
			inCharge = false;
			_.each(user.condosInCharge, function(condo) { if (condo.condoId == condoId) inCharge = true; });
			if (inCharge == true)
				return user.userId;
		});
  }

  let messenger = MessagesFeed.find({messageId: {$in: conversationIds}, userId: {$in: managerIds}}).fetch();

  // forum = forum.filter(f => condos.includes(f.condoId))
  // incidents = incidents.filter(f => condos.includes(f.condoId))
  // classifieds = classifieds.filter(f => condos.includes(f.condoId))
  // actus = actus.filter(f => condos.includes(f.condoId))
  // resources = resources.filter(f => condos.includes(f.condoId))
  // concierge = concierge.filter(f => condos.includes(f.condoId))
  // console.log('messenger ', messenger )
  // // messenger = messenger.filter(f => condos.includes(f.condoId))

  if ((duration = end.diff(start, 'year')) > 1) {
    // console.log('year')
		res = YearUtilisations(start, end, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		}, condosData, conversations, lang)
	}
	else if ((duration = end.diff(start, 'month')) > 2) {
    // console.log('month')
		res = MonthUtilisations(start, end, lang, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		}, condosData, conversations)
	}
	else if ((duration = end.diff(start, 'days')) > 31) {
    // console.log('week')
		res = WeekUtilisations(start, end, lang, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		}, condosData, conversations)
	}
	else {
    // console.log('day')
		res = DayUtilisations(start, end, lang, {
			"forum": forum,
			"incidents": incidents,
			"ads": classifieds,
			"actus": actus,
			"resources": resources,
			"concierge": concierge,
			"messenger": messenger
		}, condosData, conversations)
  }
  // console.log('res ', res )
	return res;
}

export const UtilizationCSV = {
  getUtilizationStats(startDate, endDate, condoId, lang) {
    console.time('utilization')
    const data = getUtilization(
      moment(parseInt(startDate)),
      moment(parseInt(endDate)),
      condoId,
      lang
    )
    console.timeEnd('utilization')
    return data
  }
}
