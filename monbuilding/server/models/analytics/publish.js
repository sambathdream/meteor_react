Meteor.startup(function () {
	Meteor.publish("managerAnalytics", function () {
		const userId = Meteor.userId()

    const enterprise = Enterprises.findOne({ 'users.userId': userId }, { fields: { users: true } })
    if (enterprise) {
      let condoIds = _.map(_.find(
        enterprise.users, user => user.userId === userId
      ).condosInCharge, "condoId")
      // console.log('condoIds ', condoIds )

      return Analytics.find({ condoId: { $in: [...condoIds, null] } })
    } else {
      return []
    }
	});
});
