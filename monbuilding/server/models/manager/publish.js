Meteor.startup(function () {
	Meteor.publish('enterpriseHistory', function(enterpriseId, count) {
		if (Meteor.call('isGestionnaire', this.userId) || Meteor.call('isAdmin', this.userId)) {
			if (!EnterpriseHistoryTab[enterpriseId]) {
				name = "EnterpriseHistory" + enterpriseId;
				EnterpriseHistoryTab[enterpriseId] = new Mongo.Collection(name);
			}
			let self = this
			if (EnterpriseHistoryTab[enterpriseId]) {
				let feed = EnterpriseHistoryTab[enterpriseId].find({
					$or: [
						{
							'module': 'event',
							'condoId': { $in: Meteor.listCondoUserHasRight('actuality', 'see') }
						},
						{
							'module': 'info',
							'condoId': { $in: Meteor.listCondoUserHasRight('actuality', 'see') }
						},
						{
							'module': 'incident',
							'condoId': { $in: Meteor.listCondoUserHasRight('incident', 'see') }
						},
						{
							'module': 'messenger',
							'condoId': { $in: Meteor.listCondoUserHasRight('messenger', 'seeAllMsgEnterprise') }
						},
						{
							'module': 'manual',
							'condoId': { $in: Meteor.listCondoUserHasRight('manual', 'see') }
						},
						{
							'module': 'map',
							'condoId': { $in: Meteor.listCondoUserHasRight('map', 'see') }
            },
            {
							'module': 'ads',
							'condoId': { $in: Meteor.listCondoUserHasRight('ads', 'see') }
            },
            {
							'module': 'forum',
							'condoId': { $in: _.union(Meteor.listCondoUserHasRight("forum", "seePoll"), Meteor.listCondoUserHasRight("forum", "seeSubject")) }
            },
            {
							'module': 'reservation',
							'condoId': { $in: _.union(Meteor.listCondoUserHasRight("reservation", "seeResources"), Meteor.listCondoUserHasRight("reservation", "addResources")) }
            },
            {
							'module': 'trombi',
							'condoId': { $in: Meteor.listCondoUserHasRight('trombi', 'see') }
            },
            {
							'module': 'managerList',
							'condoId': { $in: Meteor.listCondoUserHasRight('managerList', 'seeManager') }
            },
            {
							'module': 'setContact',
							'condoId': { $in: Meteor.listCondoUserHasRight('setContact', 'seeMessenger') }
            },
            {
							'module': 'emergencyContact',
							'condoId': { $in: Meteor.listCondoUserHasRight('emergencyContact', 'see') }
            },
					]
				}, { sort: { date: -1 }, limit: count })

				const handler = feed.observe({
					added: function (document) {
						self.added("enterpriseHistory", document._id, document);
					}
				});
        this.onStop(() => {
          return handler.stop()
        })
      }
			self.ready()
			return EnterpriseHistoryAction.find()
		} else {
			return this.stop()
		}
	})
	Meteor.publish('contactManagementFromManager', function(condoId) {
		if (!condoId || condoId == undefined)
			throw new Meteor.Error(301, "Access Denied", "No condoId");
		let contactManagement = ContactManagement.find({condoId: condoId});
		if (contactManagement.count() == 0) {
      let thisCondo = Condos.findOne(condoId);
      if (thisCondo) {
        contactManagement = ContactManagement.find({condoId: "default", forCondoType: thisCondo.settings.condoType})
      }
		}
		return [contactManagement];
	});

  Meteor.publish('managerResident', function() {
    if (Meteor.call('isGestionnaire', this.userId) || Meteor.call('isAdmin', this.userId)) {
      let self = this;
      let userGestionnaire = _.find(gestionnaire.users, function (u) {return u.userId === self.userId});
      const condosIds = _.map(userGestionnaire.condosInCharge, function(c) {return c.condoId});

      return Residents.find({'condos.condoId': {$in: condosIds}});
    } else {
      return this.stop();
    }
  })

	Meteor.publish('board_gestionnaire', function() {
		if (Meteor.call('isGestionnaire', this.userId) || Meteor.call('isAdmin', this.userId)) {
			const __DEBUG = false;
			if (__DEBUG == true) console.log("FIRST           => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			let self = this;
			var monCompteurDeLaMort = 0;
			let user = Meteor.users.findOne(this.userId);
			let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
			let userGestionnaire = _.find(gestionnaire.users, function (u) {return u.userId === self.userId});
			var listMembers = _.map(gestionnaire.users, function(u) {return u.userId});
      var condosIds = _.map(userGestionnaire.condosInCharge, function(c) {return c.condoId});
			let rCondos = Condos.find({_id: {$in: condosIds}});
      let condoPhoto = CondoPhotos.find({condoId: {$in: condosIds}});
      let condoLogo = CondoLogos.find({condoId: {$in: condosIds}});

			let buildingsId = [];
			if (__DEBUG == true) console.log("Buildings_start => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			for (c of rCondos.fetch()) {
				buildingsId = buildingsId.concat(c.buildings);
			}
			if (__DEBUG == true) console.log("Buildings_end   => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			var rBuildings = Buildings.find({_id: {$in: buildingsId}});

			let query = Residents.find({$or: [{"condos.condoId": {$in: condosIds}}, {"pendings.condoId": {$in: condosIds}}]});

			if (__DEBUG == true) console.log("Pipeline        => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			if (__DEBUG == true) console.log("Observer_start  => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			const handler = query.observe({
        changed: function(document, oldDocument) {
          update_gestionnaire_users("changed", self, document._id);
				},
				added: function(document) {
          update_gestionnaire_users("added", self, document._id);
				},
				removed: function(document) {
          self.removed('gestionnaireUsers', document.userId);
				}
      });
      this.onStop(() => {
        return handler.stop()
      })
      this.ready();
			if (__DEBUG == true) console.log("Observer_end    => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));


			let rUsers = Meteor.users.find({_id: {$in: listMembers}},
				{fields: {profile: true, emails: true}});

			if (__DEBUG == true) console.log("Profile_picture => ", moment.utc(Date.now()).format("HH:mm:ss:SSSS"));
			if (__DEBUG == true) console.log(" ");

			incidentType = IncidentType.find({$or: [{condoId: {$in: condosIds}}, {condoId: "default"}]});
			if (incidentType.fetch() == undefined || incidentType.fetch()[0] == undefined) {
				incidentType = IncidentType.find({condoId: "default"});
			}

			incidentDetails = IncidentDetails.find({$or: [{condoId: {$in: condosIds}}, {condoId: "default"}]});
			if (incidentDetails.fetch() == undefined || incidentDetails.fetch()[0] == undefined) {
				incidentDetails = IncidentDetails.find({condoId: "default"});
			}

			incidentPriority = IncidentPriority.find({$or: [{condoId: {$in: condosIds}}, {condoId: "default"}]});
			if (incidentPriority.fetch() == undefined || incidentPriority.fetch()[0] == undefined) {
				incidentPriority = IncidentPriority.find({condoId: "default"});
			}

			incidentArea = IncidentArea.find({$or: [{condoId: {$in: condosIds}}, {condoId: "default"}]});
			if (incidentArea.fetch() == undefined || incidentArea.fetch()[0] == undefined) {
				incidentArea = IncidentArea.find({condoId: "default"});
			}

			condoContact = CondoContact.find({condoId: {$in: condosIds}});
			declarationDetails = DeclarationDetails.find();

			self.ready();

      const adminId = Meteor.users.findOne({ 'identities.adminId': { $exists: true } })._id
      let userRights = UsersRights.find({ condoId: { $in: condosIds }, userId: { $nin: [adminId] }});

      return [ declarationDetails,
            condoContact,
            incidentType,
            incidentDetails,
            incidentPriority,
            incidentArea,
            rCondos,
            condoPhoto,
            condoLogo,
            rBuildings,
            rUsers,
            Messages.find({
              condoId: {$in: condosIds},
              target: "manager"
            }),
            EDL.find({condoId: {$in: condosIds}}),
            userRights,
            CondosModulesOptions.find({condoId: {$in: condosIds}}),
            BuildingStatus.find(),
            CompanyName.find()
          ];
		}
		else {
			return this.stop();
		}

		function update_gestionnaire_users(action, self, userAgg) {
			if (action === "removed") {
				self.changed('gestionnaireUsers', userAgg.userId, {});
			}
			if (Meteor.call('isAdmin', this.userId))
				var pipeline = [
					{$match: {$and: [{"identities.residentId": {$exists: true}}, {"identities.residentId": userAgg}]}},
					{
						$lookup: {
							from: "residents",
							localField: "identities.residentId",
							foreignField: "_id",
							as: "resident"
						}
					},
					{
						$match: {
							$or: [
							{"resident.condos.condoId": {$in: condosIds}},
							{"resident.pendings.condoId": {$in: condosIds}}
							]
						}
					}
				];
			else
				var pipeline = [
					{$match: {$and: [{"identities.residentId": {$exists: true}}, {"identities.adminId": {$exists: false}}, {"identities.residentId": userAgg}]}},
					{
						$lookup: {
							from: "residents",
							localField: "identities.residentId",
							foreignField: "_id",
							as: "resident"
						}
					},
					{
						$match: {
							$or: [
							{"resident.condos.condoId": {$in: condosIds}},
							{"resident.pendings.condoId": {$in: condosIds}}
							]
						}
					}
				];

			let aggUsers = Meteor.users.aggregate(pipeline);
			let u = aggUsers[0]
			if (u && u != undefined) {
				for (let i = 0; i < u.resident[0].condos.length; i++) {
					if (_.find(condosIds, (c2) => {return c2._id === u.resident[0].condos._id}) === undefined)
						delete u.resident[0].condos[i];
				}
				delete u.services;
				if (action === "added" || action === "removed")
					self.added('gestionnaireUsers', u._id, u);
				else if (action === "changed")
					self.changed('gestionnaireUsers', u._id, u);
			}
		}
	});

	Meteor.publish('avatarLinksGestionnaire', function () {
		if (!Meteor.call('isAdmin', Meteor.userId()) && !Meteor.call("isGestionnaire", Meteor.userId())) {
      return false;
    }

    let condosIds = []
    if (!Meteor.call('isAdmin', Meteor.userId())) {
      let enterpriseUsers = Enterprises.findOne({"users.userId": Meteor.userId()}, {fields: {"users": true}});
      let enterpriseUser = _.find(enterpriseUsers.users, function(enterpriseUser) {
        return enterpriseUser.userId == Meteor.userId();
      });
      condoIds = _.map(enterpriseUser.condosInCharge, function(condo){
        return condo.condoId;
      });
    } else {
      condoIds = _.map(Condos.find({}, { fields: { _id: true } }).fetch(), '_id')
    }

		let residentsCursor = Residents.find({
      $or: [
        { "condos.condoId": { $in: condoIds } },
        { "pendings.condoId": { $in: condoIds } }
      ]
		});

		let enterprise = Enterprises.find({
			"condos": {$in: condoIds}
		});

		let self = this;
		const handlerResident = residentsCursor.observe({
			added: function (resident) {
				if (resident.fileId != undefined && resident.fileId != "")
				{
					let uf = UserFiles.findOne(resident.fileId);
					if (uf)
						self.added("avatars", resident.userId, {
							userId: resident.userId,
							fileId: resident.fileId,
							avatar: {
								original: uf.link(),
								large: uf.link("large"),
								thumbnail40: uf.link("thumbnail40"),
								thumb100: uf.link("thumb100"),
								avatar: uf.link("avatar"),
								preview: uf.link("preview"),
							},
							from: "residents"
						});
				}
			},
			changed: function (resident, oldResident) {
				if (resident.fileId != undefined && resident.fileId != "")
				{
					let uf = UserFiles.findOne(resident.fileId);
					if (uf)
					{
						try {
							self.changed("avatars", oldResident.userId, {
								userId: oldResident.userId,
								fileId: resident.fileId,
								avatar: {
									original: uf.link(),
									large: uf.link("large"),
									thumbnail40: uf.link("thumbnail40"),
									thumb100: uf.link("thumb100"),
									avatar: uf.link("avatar"),
									preview: uf.link("preview"),
								}
							});
						} catch (e) {
							this.added(resident);
						}
					}
					else {
						self.removed("avatars", oldResident.userId);
					}
				}
        else if (oldResident.fileId != undefined && oldResident.fileId != "") {
					try {
						self.removed("avatars", oldResident.userId);
					} catch(e) {
						// console.log('e', e);
					}
				}
			}
		});

		const handlerEnterprise = enterprise.observe({
			added: function (enterprise) {
				_.each(enterprise.users, function(val){
					if (_.find(val.condosInCharge, function(condo){
						if (_.includes(condoIds, condo.condoId))
							return true;
					}) != undefined) {
						if (val.profile.fileId != undefined && val.profile.fileId != "")
						{
							let uf = UserFiles.findOne(val.profile.fileId);
							if (uf)
								self.added("avatars", val.userId, {
									userId: val.userId,
									fileId: val.profile.fileId,
									avatar: {
										original: uf.link(),
										large: uf.link("large"),
										thumbnail40: uf.link("thumbnail40"),
										thumb100: uf.link("thumb100"),
										avatar: uf.link("avatar"),
										preview: uf.link("preview"),
									},
									from: "enterprise"
								});
						}
					}
				});
			},
			changed: function (enterprise, oldEnterprise) {
				let subthis = this;
				_.each(enterprise.users, function(val, key){
					if (_.find(val.condosInCharge, function(condo){
						if (_.includes(condoIds, condo.condoId))
							return true;
					}) != undefined) {
						if (val.profile.fileId != undefined && val.profile.fileId != "")
						{
							let uf = UserFiles.findOne(val.profile.fileId);
							if (uf) {
								try {
									self.changed("avatars", val.userId, {
										userId: val.userId,
										fileId: val.profile.fileId,
										avatar: {
											original: uf.link(),
											large: uf.link("large"),
											thumbnail40: uf.link("thumbnail40"),
											thumb100: uf.link("thumb100"),
											avatar: uf.link("avatar"),
											preview: uf.link("preview"),
										}
									});
								} catch (e) {
									subthis.added(enterprise);
								}
							}
							else {
								try {self.removed("avatars", val.userId);} catch (e) {}
							}
						}
						else {
							try {self.removed("avatars", val.userId);} catch (e) {}
						}
					}
				});
			}
    });
    this.onStop(() => {
      return handlerResident.stop() && handlerEnterprise.stop()
    })
		this.ready();
	});
});
