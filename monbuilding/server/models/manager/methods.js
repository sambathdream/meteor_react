
import { Email } from '/common/lang/lang'

function sendCondoInvitationEmailToNewUser(email, condoId) {
  const user = Accounts.findUserByEmail(email);
  const condo = Condos.findOne(condoId);
  const gestionnaire = Enterprises.findOne({ "condos": condoId });
  const lang = Meteor.users.findOne({_id: user._id}).profile.lang || "fr"
  Meteor.call(
    'sendEmailSync',
    email,
    condoId,
    'Vous avez été invité(e) à rejoindre l\'espace de votre immeuble',
    'invite-new-user-to-condo-' + lang,
    {
      condoName: condo.getName(),
      gestionnaireName: gestionnaire.name,
      setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, condoId),
    }
  );
}

Meteor.startup(function() {
  Meteor.methods({
    saveNewCompanyName: function(value) {
      if (Meteor.userId()) {
        CompanyName.insert({ name: value })
      }
    },
    removeManagerFromCondo: function(userId, condoId) {
      if (Meteor.call('user_isGestionnaire')) {
        const condo = Condos.findOne({ _id: condoId })
        if (!condo) return
        const user = Meteor.users.findOne({ _id: userId })
        if (!user) return
        Enterprises.update({ 'condos': condoId, 'users.userId': userId }, { $pull: { 'users.$.condosInCharge': { 'condoId': condoId } } })
        Meteor.removeUserRights(userId, condoId)
        sendNotifRemoveCondo(userId, condoId)

        const currentUserEnterprise = Enterprises.findOne({ 'condos': condoId, 'users.userId': userId })
        const currentCondoInCharge = (currentUserEnterprise.users.find(user => user.userId === userId) || {}).condosInCharge
        if (!currentCondoInCharge || currentCondoInCharge.length === 0) {
          Enterprises.update({ _id: currentUserEnterprise._id }, {
            $pull: {
              'users': { userId: userId },
            }
          });
          if (!Enterprises.findOne({ 'users.userId': userId })) {
            Meteor.call('addToGestionnaireHistory',
              'managerList',
              userId,
              'deletedManager',
              condoId,
              `${user.profile.firstname} ${user.profile.lastname}`,
              Meteor.userId()
            );

            Meteor.users.remove({ _id: userId })
          }
        }

        let lang = user.profile.lang || 'fr'
        let translation = new Email(lang)
        Meteor.call('sendEmailSync',
          user.emails[0].address,
          condoId,
          translation.email['delete_condo'] + condo.name,
          "remove-access-condo-" + lang,
          {
            userId: userId,
            condoId: condoId,
            building: condo.name,
            formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, condoId)
          }
        )
      }
    },
    createNewOccupantFromManagerSide: function (newUser, customRights, from = 'web', avoidEmail = false) {
      check(newUser, Match.ObjectIncluding({
        firstname: String,
        lastname: String,
        email: Match.Where((val) => {
            check(val, String);
            return Isemail.validate(val);
          }),
        phone: Match.OneOf(null, String),
        phoneCode: String,
        landline: Match.OneOf(null, String),
        phoneCodeLandline: String,
        condos: [String],
        roles: [String],
        building: [String],
        status: [Match.OneOf('tenant', 'owner', 'user')],
        company: [Match.OneOf(null, String)],
        diploma: [Match.OneOf(null, String)],
        school: [Match.OneOf(null, String)],
        studies: [Match.OneOf(null, String)],
        door: [Match.OneOf(null, String)],
        floor: [Match.OneOf(null, String)],
        office: [Match.OneOf(null, String)],
        civilite: Match.OneOf(null, 'm', 'mme'),
        deferredRegistrationDate: Match.OneOf(null, Number)
      }));
      let userIsManager = Meteor.call('isGestionnaire', Meteor.userId())
      let userIsOccupantWithRight = Meteor.userHasRight('trombi', 'addOccupant', newUser.condos[0])
      if (userIsManager || userIsOccupantWithRight) {
        Accounts.createUser({ email: newUser.email });

        const user = Accounts.findUserByEmail(newUser.email);
        let pendingsCondo = []

        let civilite = null
        switch (newUser.civilite) {
          case 'm':
            civilite = 'Monsieur'
            break;
          case 'mme':
            civilite = 'Madame'
            break;

          default:
            civilite = null
            break;
        }

        let profile = {
          civilite: civilite,
          lastname: newUser.lastname,
          firstname: newUser.firstname,
          tel: newUser.phone,
          telCode: newUser.phoneCode,
          tel2: newUser.landline,
          role: 'Resident',
          tel2Code: newUser.phoneCodeLandline,
          telCodeCountry: "France"
        }
        newUser.condos.forEach((condoId, index) => {
          pendingsCondo.push({
            condoId: condoId,
            roleId: newUser.roles[index],
            invited: Date.now(),
            invitByGestionnaire: true,
            notifications: { "actualite": true, "incident": true, "forum_forum": true, "forum_syndic": true, "forum_reco": true, "forum_boite": true, "classifieds": true, "resa_new": true, "resa_rappel": true, "msg_resident": true, "msg_conseil": true, "msg_gardien": true, "msg_gestion": true, "edl": true },
            preferences: {
              messagerie: true
            },
            buildings: [
              {
                buildingId: newUser.building[index],
                porte: newUser.door[index],
                etage: newUser.floor[index],
                type: newUser.status[index]
              }
            ],
            userInfo: {
              company: newUser.company[index],
              office: newUser.office[index],
              diploma: newUser.diploma[index],
              school: newUser.school[index],
              studies: newUser.studies[index],
              porte: newUser.door[index],
              etage: newUser.floor[index]
            }
          })
          if (newUser.company[index]) profile.company = newUser.company[index]
          if (newUser.office[index]) profile.office = newUser.office[index]
          if (newUser.diploma[index]) profile.diplome = newUser.diploma[index]
          if (newUser.school[index]) profile.school = newUser.school[index]
          if (newUser.studies[index]) profile.matiere = newUser.studies[index]

          if (newUser.fromAD) {
            profile.fromAD = true
          }

          Meteor.createDefaultRightsForUserWithCondo(user._id, condoId, newUser.roles[index])
          if (customRights[index] !== null && customRights[index].module) {
            UsersRights.upsert({
              "condoId": condoId,
              "userId": user._id,
            }, { $set: { module: customRights[index].module } })
          }
        })

        const residentId = Residents.insert({
          userId: user._id,
          condos: [],
          registeredFrom: (from ? from : "web"),
          pendings: pendingsCondo
        });

        Meteor.users.update({
          _id: user._id,
        }, {
            $set: {
              profile: profile,
              'identities.residentId': residentId,
              deferredRegistrationDate: newUser.deferredRegistrationDate,
              source: newUser.source
            },
        });

        Meteor.call('addToGestionnaireHistory',
          'trombi',
          user._id,
          'invitedOccupant',
          newUser.condos[0],
          `${newUser.firstname} ${newUser.lastname}`,
          Meteor.userId()
        );

        if (!avoidEmail) {
          if (newUser.deferredRegistrationDate) {
            const condo = Condos.findOne(newUser.condos[0]);
            const gestionnaire = Enterprises.findOne({ "condos": newUser.condos[0] });
            const lang = Meteor.users.findOne({_id: user._id}).profile.lang || "fr"
  
            Meteor.call('scheduleAnEmail', {
              templateName: 'invite-new-user-to-condo-' + lang,
              to: newUser.email,
              condoId: newUser.condos[0],
              subject: 'Vous avez été invité(e) à rejoindre l\'espace de votre immeuble',
              data: {
                condoName: condo.getName(),
                gestionnaireName: gestionnaire.name,
                setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, newUser.condos[0]),
              },
              dueTime: new Date(newUser.deferredRegistrationDate)
            })
          } else {
            sendCondoInvitationEmailToNewUser(newUser.email, newUser.condos[0])
          }
        }
        return user._id
      }
    },
    createNewManagerFromManagerSide: function (manager, customRights) {
      if (!Meteor.userId() || (!Meteor.call('isAdmin', this.userId) && (!Meteor.call('isGestionnaire', Meteor.userId()) || !Meteor.userHasRight('managerList', 'addManager', manager.condos[0], Meteor.userId())))) {
        throw new Meteor.Error(403, 'Not Allowed')
      }
      Accounts.createUser({ email: manager.email })
      const user = Accounts.findUserByEmail(manager.email);
      let civilite = manager.civilite;
      delete manager.civilite;
      manager.userId = user._id;

      let managerEnterprise = {
        userId: manager.userId,
        firstname: manager.firstname,
        lastname: manager.lastname,
        level: 5,
        type: "directionSiege",
        condosInCharge: [
        ],
        profile: { picture: "", fileId: "" },
        isAdmin: false,
        tel: manager.phone,
        telCode: manager.phoneCode,
        telCodeCountry: null,
        tel2: manager.landline,
        tel2Code: manager.phoneCodeLandline,
        statusToOccupant: manager.statusToOccupant,
        civilite: civilite
      }

      manager.condos.forEach((condoId, index) => {
        managerEnterprise.condosInCharge.push({
          joined: Date.now(),
          access: [
            1, 2
          ],
            condoId: condoId,
              notifications: { incidents: true, forum_forum: true, forum_syndic: true, actualites: true, classifieds: true, messagerie: true, edl: true, new_user: true, messenger: true, resa_new: true, resa_rappel: true, msg_resident: true, msg_gardien: true, msg_conseil: true, msg_gestion: true }
        })
        Meteor.createDefaultRightsForManagerWithCondo(user._id, condoId, manager.roles[index])
        if (customRights[index] !== null && customRights[index].module) {
          UsersRights.upsert({
            "condoId": condoId,
            "userId": manager.userId,
          }, { $set: { module: customRights[index].module } })
        }
      });

      Enterprises.update({_id: manager.entity}, {
        $push: { users: managerEnterprise },
        $set: { inactive: false },
      });
      Meteor.users.update({ // update Meteor.users collection
        _id: user._id,
      }, {
          $set: {
            profile: { civilite: civilite, lastname: manager.lastname, firstname: manager.firstname, tel: manager.phone, telCode: manager.phoneCode, telCodeCountry: null, tel2: manager.landline, tel2Code: manager.phoneCodeLandline, role: "Gestionnaire", lang: "fr", statusToOccupant: manager.statusToOccupant },
            'identities.gestionnaireId': manager.entity,
            deferredRegistrationDate: manager.deferredRegistrationDate
          },
        });

      Meteor.call('addToGestionnaireHistory',
        'managerList',
        user._id,
        'invitedManager',
        manager.condos[0],
        `${manager.firstname} ${manager.lastname}`,
        Meteor.userId()
      );

      const lang = Meteor.users.findOne(user._id).profile.lang || 'fr'
      var translation = new Email(lang); // send email manager account creation
      if (manager.deferredRegistrationDate) {
        Meteor.call('scheduleAnEmail', {
          templateName: 'invite-new-manager-to-enterprise',
          to: manager.email,
          condoId: manager.condos[0],
          subject: translation.email['invit_join'],
          data: {
            setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, null, null, manager.entity),
          },
          dueTime: new Date(manager.deferredRegistrationDate)
        })
      } else {
        Meteor.call('sendEmailSync',
        manager.email,
        manager.condos[0],
        translation.email["invit_join"],
        'invite-new-manager-to-enterprise',
        {
          setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, null, null, manager.entity),
        })
      }
      return user._id
    },
    removePictureForUnregiteredManager: function(fileId) {
      if (Meteor.call('user_isGestionnaire')) {
        let file = UserFiles.findOne(fileId);
        if (file && file.meta && file.meta.isUnregisteredManager && file.meta.isUnregisteredManager == true) {
          UserFiles.remove(fileId);
          return true;
        }
      }
    },
    updateCondoReminder: function(condoId, elementId, elementValue) {
      if (Meteor.call('user_isGestionnaire')) {
        if (Meteor.userHasRight("setContact", "writeMessenger", condoId) != true)
          throw new Meteor.Error(403, "Not allowed");
        if (elementValue > 1000) {
          throw new Meteor.Error(403, "Forbidden")
        }
        CondoContact.update({
            condoId: condoId,
            'contactReminder.priorityId': elementId
          },
          {
            $set: {
              'contactReminder.$': {
                priorityId: elementId,
                timeValue: +elementValue
              }
            }
          }
        );
      }
    },
    updateCondoDefault: function(condoId, elementValue) {
      if (Meteor.call('user_isGestionnaire')) {
        if (Meteor.userHasRight("setContact", "writeMessenger", condoId)  != true)
          throw new Meteor.Error(403, "Not allowed");
        let condoContact = CondoContact.findOne({
          condoId: condoId
        });
        if (!condoContact) {
          throw new Meteor.Error(601, "CondoContact not found");
        }
        let oldManagerInCharge = condoContact && condoContact.defaultContact && condoContact.defaultContact.userId
        let newManagerInCharge = elementValue
        CondoContact.update({
            condoId: condoId,
          },
          {
            $set: {
              'defaultContact.userId': elementValue
            }
          }
        );
        if (newManagerInCharge && newManagerInCharge.length > 0) {
          OneSignal.MbNotification.sendToUserIds([newManagerInCharge], {
            type: 'setContact',
            key: 'addDefault',
            dataId: 'defaultContact',
            condoId: condoId,
            data: {
              declaration: 'defaultContact'
            }
          })
        }
        if (oldManagerInCharge && oldManagerInCharge.length > 0) {
          OneSignal.MbNotification.sendToUserIds([oldManagerInCharge], {
            type: 'setContact',
            key: 'removeDefault',
            dataId: 'defaultContact',
            condoId: condoId,
            data: {
              declaration: 'defaultContact'
            }
          })
        }

      }
    },
    updateCondoContact: function(condoId, elementId, elementValue, messageTypeId, fromTab) {
      if (Meteor.call('user_isGestionnaire')) {
        if (Meteor.userHasRight("setContact", "write" + fromTab, condoId) != true) {
          throw new Meteor.Error(403, "Not allowed");
        }
        try {
          const declaration = DeclarationDetails.findOne({ _id: elementId });
          if (!declaration) {
            throw new Meteor.Error(601, "DeclarationDetails not found");
          }
          let condoContact = CondoContact.findOne({
            condoId: condoId
          });
          let category = {}
          if (elementId && messageTypeId) {
            category = condoContact.contactSet.find((elem) => elem.messageTypeId === messageTypeId && elem.declarationDetailId === elementId)
          } else {
            category = condoContact.contactSet.find((elem) => elem.declarationDetailId === elementId)
          }
          let oldManagerInCharge = _.difference(category && category.userIds, elementValue)
          let newManagerInCharge = _.difference(elementValue, category && category.userIds)
          // console.log("oldManagerInCharge : ", oldManagerInCharge)
          // console.log("newManagerInCharge : ", newManagerInCharge)
          let ret = CondoContact.update({
              condoId: condoId,
              'contactSet': {
                $elemMatch: {
                  'messageTypeId': messageTypeId,
                  'declarationDetailId': elementId
                }
              }
            },
            {
              $set: {
                'contactSet.$': {
                  messageTypeId: messageTypeId,
                  declarationDetailId: elementId,
                  userIds: elementValue
                }
              }
            }
          );
          if (ret === 0) {
            CondoContact.update({
              condoId: condoId,
            },
              {
                $addToSet: {
                  'contactSet': {
                    messageTypeId: messageTypeId,
                    declarationDetailId: elementId,
                    userIds: elementValue
                  }
                }
              }
            );
          }
          if (declaration.key === 'edl') {
            Meteor.call("updateEdlValidator", condoId, elementValue);
            if (newManagerInCharge && newManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(newManagerInCharge, {
                type: 'setContact',
                key: 'addReservation',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
            if (oldManagerInCharge && oldManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(oldManagerInCharge, {
                type: 'setContact',
                key: 'removeReservation',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
          } else if (declaration.key === 'validEntry') {
            if (newManagerInCharge && newManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(newManagerInCharge, {
                type: 'setContact',
                key: 'addValidEntry',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
            if (oldManagerInCharge && oldManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(oldManagerInCharge, {
                type: 'setContact',
                key: 'removeValidEntry',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
          } else {
            if (newManagerInCharge && newManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(newManagerInCharge, {
                type: 'setContact',
                key: 'addOther',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
            if (oldManagerInCharge && oldManagerInCharge.length > 0) {
              OneSignal.MbNotification.sendToUserIds(oldManagerInCharge, {
                type: 'setContact',
                key: 'removeOther',
                dataId: declaration._id,
                condoId: condoId,
                data: {
                  declaration
                }
              })
            }
            // remove old user in email reminder
            if (declaration.key === 'incident') {
              for (const userId of oldManagerInCharge) {
                MbEraseEmailReminderForIncident(userId, condoId, messageTypeId)
              }
            } else if (declaration.isDropdownValue === true) {
              for (const userId of oldManagerInCharge) {
                MbEraseEmailReminderForOther(userId, condoId, elementId, messageTypeId)
              }
            }

            // console.log("condoId : ", condoId)
            // console.log("elementId : ", elementId)
            // console.log("elementValue : ", elementValue)
            // console.log("messageTypeId : ", messageTypeId)
            // console.log("fromTab : ", fromTab)
            if (newManagerInCharge.length > 0) {
              Meteor.call('addToGestionnaireHistory',
                'setContact',
                newManagerInCharge[0],
                'addedContact',
                condoId,
                elementId,
                Meteor.userId()
              )
            } else {
              Meteor.call('addToGestionnaireHistory',
                'setContact',
                oldManagerInCharge[0],
                'removedContact',
                condoId,
                elementId,
                Meteor.userId()
              );
            }

          }
        } catch (e) {
          throw new Meteor.Error(e);
        }
      } else {
        throw new Meteor.Error(403, "Not Logged As Manager");
      }
    },
    archiveConversation: function(conversationId) {
      if (this.userId) {
        let conversation = Messages.findOne(conversationId);
        if (conversation) {
          let reminder = conversation.reminder;
          if (reminder) {
            EmailReminder.remove({_id: reminder});
          }
          Messages.update({_id: conversationId}, {$set : {isArchived: true}});
        }
      }
    },
    gestionnaireCondosInCharge: function(gestionnaireMeteorUser) {
      if (gestionnaireMeteorUser) {
        const enterprise = Enterprises.findOne({ _id: gestionnaireMeteorUser.identities.gestionnaireId }, { fields: { users: true } })
        const mapUser = enterprise.users
        const findUser = _.find(mapUser, function (elem) {
          return elem.userId == Meteor.userId();
        })
        const condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);

        if (condoIds) {
          return condoIds;
        }
      }
    },

    /*  ADD A NEW ENTRY TO MANAGER HISTORY
    module										(String) Module name
    modulePostId								(String) id refered to the module post
    action                                      (String) TYPE OF ENTRY, key reference to the EnterpriseHistoryAction
    condoId                                     (String) CONDO ID RELATED TO ENTRY
    detail                                      (String) detail OF ENTRY (message posted etc)
    userId                                      (String) USER ID OF ORIGIN OF ENTRY
    */
    // module, modulePostId, action, condoId, detail, userId, date
    addToGestionnaireHistory: function (module, modulePostId, action, condoId, detail, userId) {
      console.log(module, modulePostId, action, condoId, detail, userId)
      if (!Match.test(module, String) ||
        !Match.test(modulePostId, String) ||
        !Match.test(action, String) ||
        !Match.test(condoId, String) ||
        !Match.test(detail, String) ||
        !Match.test(userId, String)) {
        throw new Meteor.Error(401, "Invalid parameters manager addToGestionnaireHistory");
      }
      if (!Meteor.userId()){
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let enterprise = Enterprises.findOne({"condos": condoId});
      if (!enterprise){
        enterprise = Enterprises.findOne({ _id: modulePostId })
        if (!enterprise) {
          return;
        }
      }
      // throw new Meteor.Error(601, "Invalid id", "Cannot find manager");
      if (detail.length > 50){
        detail = detail.substr(0, 47) + "...";
      }
      if (!EnterpriseHistoryTab[enterprise._id]) {
        name = "EnterpriseHistory" + enterprise._id;
        EnterpriseHistoryTab[enterprise._id] = new Mongo.Collection(name);
      }
      if (EnterpriseHistoryTab[enterprise._id]) {
        EnterpriseHistoryTab[enterprise._id].insert({
          module,
          modulePostId,
          action,
          condoId,
          detail,
          userId,
          date: Date.now(),
        });
      } else {
        console.log('no EnterpriseHistoryTab');
      }
    },
    /*  CHECK IF THE USER AS PARAMETER IS MANGER OF CONDO AS PARAMETER
    id                                          (String) ID OF USER TO CHECK
    condoId                                     (String) ID OF CONDO TO CHECK
    */
    isGestionnaireOfCondo: function(id, condoId) {
      if (id === undefined)
        return false;
      let user = Meteor.users.findOne(id, {fields: {"identities": true}});
      if (!user || !user.identities || user.identities.gestionnaireId === undefined)
        return false;
      let gestionnaire = Enterprises.findOne({"users.userId": id});
      if (gestionnaire === undefined)
        return false;
      if (gestionnaire.condos.indexOf(condoId) === -1)
        return false;
      let me = _.find(gestionnaire.users, function (u) {return u.userId === id;});
      if (!me)
        return false;
      return (true);
    },
    // CHECK IF THE USER AS PARAMETER HAS MANAGER IDENTITY
    // id                                          (String) ID OF USER TO CHECK
    isGestionnaire: function(id) {
      if (id === undefined)
        return false;
      user = Meteor.users.findOne(id, {fields: {"identities": true}});
      if (!user || !user.identities || user.identities.gestionnaireId === undefined)
        return false;
      gestionnaire = Enterprises.findOne({"users.userId": id});
      if (gestionnaire === undefined)
        return false;
      return (true);
    },
    // CHECK IF CURRENT USER HAS MANAGER IDENTITY
    user_isGestionnaire: function() {
      if (this.userId === undefined)
        throw new Meteor.Error(300, "Not Authenticated");
      user = Meteor.users.findOne(this.userId, {fields: {"identities": true}});
      if (!user || !user.identities || user.identities.gestionnaireId === undefined)
        throw new Meteor.Error(301, "Access denied", "User dont have 'admin' identity");
      gestionnaire = Enterprises.findOne({"users.userId": this.userId});
      if (gestionnaire === undefined)
        throw new Meteor.Error(601, "Invalid id", "Cannot find manager identity for this user");
      return (true);
    },

    /*  SAVE NOTIFICATIONS PREFERENCES OF CURRENT MANAGER FOR SPECIFIC CONDO
    condoId                                     (String) ID OF CONDO
    dataName                                        (String) CONTAINS VALUE FOR NOTIFICATIONS PREFERENCES
    {
      incident                                    (String) TRUE IF WANTS TO RECEIVE INCIDENT NOTIFICATIONS
      forum                                       (String) // FORUM
      actualites                                  (String) // NEWS
      classifieds                                 (String) // CLASSIFIEDS
      messagerie                                  (String) // MESSAGES
    }
    value                                           (Boolean)
    */
    saveNotificationManager: function(condoId, dataName, value) {
      if (!Match.test(condoId, String))
        throw new Meteor.Error(401, "Invalid parameters manager saveNotificationManager");
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()});
      let user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()});
      let __condo = user.condosInCharge.find((c) => {return c.condoId == condoId});
      if (__condo) {
        __condo.notifications[dataName] = value;
        _.each(user.condosInCharge, function(elem) {
          if (elem.condoId === condoId) {
            elem = __condo;
          }
        });
        Enterprises.update({
          '_id': gestionnaire._id,
          'users.userId': Meteor.userId()
        },
        {
          $set: {'users.$.condosInCharge': user.condosInCharge},
        });
      }
    },
    /*  UPDATE MANUAL OF SPECIFIC CONDO
    condoId                                     (String) ID OF CONDO
    data                                        (RichText or String) RICHTEXT OR FILE ID
    type                                        (String) "file" OR "text"
    */
    setManual: function(condoId, data, type) {
      let condo = Condos.findOne(condoId)
      let isEdited = false
      if (condo && condo.manual !== undefined && condo.manual.type == 'file') {
        CondoDocuments.remove({ _id: condo.manual.data })
        isEdited = true
      }
      Condos.update(
        {_id: condoId},
        {$set: {manual: {type: type, data: data, manualUpdate: Date.now(), userId: this.userId}}}
      );
      let date = Date.now()

      ViewCondoDocument.update(
        { condoId: condoId },
        {
          $setOnInsert: {
            condoId: condoId,
            type: 'manual',
          },
          $set: {
            users: [this.userId],
            views: {
              [this.userId]: date + 1
            }
          }
        },
        {upsert: true}
      );

      if (type == "file") {
        CondoDocuments.update({_id: data}, {$set: {"meta.condoId": condoId}})
      }
      OneSignal.MbNotification.sendToFilter([
        {
          field: 'tag',
          key: condoId,
          relation: '=',
          value: 'true'
        }
      ], {
        type: 'document',
        key: 'new_manual',
        condoId: condoId
      })

      if (!isEdited) {
        Meteor.call('addToGestionnaireHistory',
          'manual',
          data,
          'newManual',
          condoId,
          "",
          Meteor.userId()
        );
      } else {
        Meteor.call('addToGestionnaireHistory',
          'manual',
          data,
          'editManual',
          condoId,
          "",
          Meteor.userId()
        );
      }
    },
    //  DELETE CURRENT MANUAL OF SPECIFIC CONDO
    //  condoId                                     (String) ID OF CONDO
    deleteManual: function(condoId) {
      let condo = Condos.findOne(condoId)
      if (condo && condo.manual !== undefined && condo.manual.type == 'file') {
        CondoDocuments.remove({ _id: condo.manual.data })
      }
      if (ViewCondoDocument.findOne({ condoId: condoId, type: 'manual' })) {
        ViewCondoDocument.remove({
          condoId: condoId,
          type: 'manual'
        })
      }
      Condos.update(
        {_id: condoId},
        {$unset: {manual: ""}}
        );

      Meteor.call('addToGestionnaireHistory',
        'manual',
        condo.manual.data,
        'deleteManual',
        condoId,
        "",
        Meteor.userId()
      );
    },
    /*  UPDATE MAP OF SPECIFIC CONDO
    condoId                                     (String) ID OF CONDO
    data                                        (RichText or String) RICHTEXT OR FILE ID
    type                                        (String) "file" OR "text"
    */
    setMap: function (condoId, data, type) {
      let condo = Condos.findOne(condoId)
      let isEdited = false
      if (condo && condo.map !== undefined && condo.map.type == 'file') {
        CondoDocuments.remove({ _id: condo.map.data })
        isEdited = true
      }
      let date = Date.now()
      Condos.update(
        { _id: condoId },
        { $set: { map: { type: type, data: data, mapUpdate: date, userId: this.userId } } }
      );
      if (type == "file") {
        CondoDocuments.update({ _id: data }, { $set: { "meta.condoId": condoId } });
      }
      ViewCondoDocument.update(
        { condoId: condoId },
        {
          $setOnInsert: {
            condoId: condoId,
            type: 'map',
          },
          $set: {
            users: [this.userId],
            views: {
              [this.userId]: date + 1
            }
          }
        },
        {upsert: true}
      );
      OneSignal.MbNotification.sendToFilter([
        {
          field: 'tag',
          key: condoId,
          relation: '=',
          value: 'true'
        }
      ], {
        type: 'document',
        key: 'new_map',
        condoId: condoId
      })

      if (!isEdited) {
        Meteor.call('addToGestionnaireHistory',
          'map',
          data,
          'newMap',
          condoId,
          "",
          Meteor.userId()
        );
      } else {
        Meteor.call('addToGestionnaireHistory',
          'map',
          data,
          'editMap',
          condoId,
          "",
          Meteor.userId()
        );
      }
    },
    //  DELETE CURRENT MAP OF SPECIFIC CONDO
    // condoId                                     (String) ID OF CONDO
    deleteMap: function (condoId) {
      let condo = Condos.findOne(condoId)
      if (condo && condo.manual !== undefined && condo.manual.type == 'file') {
        CondoDocuments.remove({ _id: condo.manual.data })
      }
      if (ViewCondoDocument.findOne({ condoId: condoId, type: 'map' })) {
        ViewCondoDocument.remove({
          condoId: condoId,
          type: 'map'
        })
      }
      Condos.update(
        { _id: condoId },
        { $unset: { map: "" } }
      );

      Meteor.call('addToGestionnaireHistory',
        'map',
        condo.manual.data,
        'deleteMap',
        condoId,
        "",
        Meteor.userId()
      );
    },
    /*  UPDATE MAP OF SPECIFIC CONDO
    condoId                                     (String) ID OF CONDO
    data                                        (RichText or String) RICHTEXT OR FILE ID
    type                                        (String) "file" OR "text"
    */
    setDocumentHasView: function (condoId, type) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let condo = Condos.findOne({_id: condoId})
      if (condo && condo.settings.options[type]) {
        let condoview = ViewCondoDocument.findOne({ 'condoId': condoId, 'type': type }) || {}
        let views = condoview.views || {}
        views[this.userId] = Date.now() + 10000
        ViewCondoDocument.update(
          { 'condoId': condoId, 'type': type },
          {
            $set: {
              views
            },
            $addToSet: {
              users: this.userId
            }
          }
        )
        return true
      } else {
        return false
      }
    },
    //  DELETE CURRENT MAP OF SPECIFIC CONDO
    // condoId                                     (String) ID OF CONDO
    deleteMapHasView: function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let condo = Condos.findOne(condoId)

    }
  });
});

function MbEraseEmailReminderForIncident(userId, condoId, incidentTypeId) {
  const user = Meteor.users.findOne({ _id: userId })
  if (!user) return
  const incidents = Incidents.find({
    $and: [
      { 'info.incidentTypeId': incidentTypeId },
      { 'condoId': condoId }
    ]
  }, { fields: { _id: true } }).fetch()
  const incidentsId = !!incidents && incidents.map((i) => i._id)
  EmailReminder.update({ 'emailData.incidentId': { $in: incidentsId } }, {
    $pull: {
      'to': user.emails[0].address
    }
  }, { multi: true })
  // secure: if EmailReminder.$.to == [] -> set default contact of condo
  const condoContact = CondoContact.findOne({ condoId: condoId })
  const defaultContact = Meteor.users.findOne(condoContact.defaultContact.userId)
  if (!defaultContact) return
  const emailList = [defaultContact.emails[0].address]
  EmailReminder.update({
    $and: [
      { 'emailData.incidentId': { $in: incidentsId } },
      { 'to.0': { $exists: false } }
    ]
  }, {
    $set: { 'to': emailList }
  }, { multi: true })
}

function MbEraseEmailReminderForOther(userId, condoId, detailsId, typeId) {
  const user = Meteor.users.findOne({ _id: userId })
  if (!user) return
  let messages = Messages.find({
    $and: [
      { 'type': typeId },
      { 'details': detailsId },
      { 'condoId': condoId }
    ]
  }, { fields: { _id: true } }).fetch()
  const messagesId = !!messages && messages.map((i) => i._id)
  EmailReminder.update({ 'emailData.msgId': { $in: messagesId } }, {
    $pull: {
      'to': user.emails[0].address
    }
  }, { multi: true })
  // secure: if EmailReminder.$.to == [] -> set default contact of condo
  const condoContact = CondoContact.findOne({ condoId: condoId })
  if (!condoContact) return
  const defaultContact = Meteor.users.findOne(condoContact.defaultContact.userId)
  if (!defaultContact) return
  const emailList = [defaultContact.emails[0].address]
  EmailReminder.update({
    $and: [
      { 'emailData.msgId': { $in: messagesId } },
      { 'to.0': { $exists: false } }
    ]
  }, {
    $set: { 'to': emailList }
  }, { multi: true })
}
