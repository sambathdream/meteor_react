// Report = new Mongo.Collection('report');

Meteor.startup(function () {
	Meteor.methods({
		/*  SAVE NEW REPORT FROM RESIDENT
			report : {
				from = USER WHO SEND THE REPORT,

			}
         */
		newReportFromResident(report) {
			let id = Report.insert({
				declarerId: report.from,
				authorId: report.author,
				condoId: report.condoId,
				content: {
					message: report.message,
					category: report.category,
					files: report.files
				},
				status: "pending",
				createdAt: Date.now(),
				updatedAt: Date.now(),
			});
			const senderUser = Meteor.users.findOne(report.from);
			const condo = Condos.findOne(report.condoId);
			let authorName = '';
			_.each(report.author, function(elem) {
				let thisUser = Meteor.users.findOne(elem);
				if (thisUser) {
					if (authorName != "")
						authorName += ", " + thisUser.profile.firstname + " " + thisUser.profile.lastname;
					else
						authorName += thisUser.profile.firstname + " " + thisUser.profile.lastname;
				}
			});
			Meteor.call('scheduleAnEmail', {
				templateName: 'new-report',
				to: "monbuilding75@gmail.com",
				condoId: report.condoId,
				subject: "Un probleme a été signalé par " + senderUser.profile.firstname + ' ' + senderUser.profile.lastname ,
				data: {
					condoName : condo.getName(),
					from      : senderUser.profile.firstname + ' ' + senderUser.profile.lastname,
					rebrique  : report.category,
					author    : authorName,
					message   : report.message,
          viewLink: Meteor.absoluteUrl("backoffice/report/report_details/" + id, report.condoId)
				},
				dueTime: new Date()
			});

		}
	})
});
