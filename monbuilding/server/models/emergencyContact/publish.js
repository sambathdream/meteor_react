/**
 * Created by kuyarawa on 13/05/18.
 */
Meteor.startup(
  function () {
    /* GLOBAL PUBLISH FOR RESIDENT BOARD (CONDOS, BUILDINGS, GESTIONNAIRE, RESIDENTUSERS FAKE TABLE)*/
    Meteor.publish('emergencyContactPhotos', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;

      if (condoId) {
        return EmergencyContactPhotos.find({'condos': condoId})
      } else if (Meteor.call('isGestionnaire', self.userId)) {
        const condoIds = _.map(_.find(Enterprises.findOne({'users.userId': self.userId}, {fields: {users: true}}).users, (user) => { return user.userId === self.userId}).condosInCharge, 'condoId');
        if (!!condoIds) {
          return EmergencyContactPhotos.find({'condos': {$in: condoIds}})
        }
      }
    })
    Meteor.publish('emergencyContacts', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;

      if (condoId) {
        return EmergencyContact.find({'condos.id': condoId})
      } else if (Meteor.call('isGestionnaire', self.userId)) {
        const condoIds = _.map(_.find(Enterprises.findOne({'users.userId': self.userId}, {fields: {users: true}}).users, (user) => { return user.userId === self.userId}).condosInCharge, 'condoId');
        if (!!condoIds) {
          return EmergencyContact.find({'condos.id': {$in: condoIds}})
        }
      }
    })
  }
)
