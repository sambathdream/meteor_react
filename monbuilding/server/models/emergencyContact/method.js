/**
 * Created by kuyarawa on 13/05/18.
 */
Meteor.startup(function () {
  Meteor.methods({
    updateEmergencyContactPhoto: function (contactId, fileId, uploaderId, defaultCondoId) {
      check(contactId, String)
      check(fileId, String)
      check(uploaderId, Match.Maybe(String))

      let self = this;
      let condoId = defaultCondoId
      if (!defaultCondoId || defaultCondoId === 'all') {
        const condoIds = _.map(_.find(Enterprises.findOne({'users.userId': self.userId}, {fields: {users: true}}).users, (user) => { return user.userId === self.userId}).condosInCharge, 'condoId');
        if (!!condoIds && !!condoIds[0]) {
          condoId = condoIds[0]
        }
      }

      const file = EmergencyContactPhotoFiles.findOne({_id: fileId})
      const isExist = EmergencyContactPhotos.findOne({contactId: contactId})
      const data = {
        contactId,
        fileId,
        avatar: {
          original: file.link(),
          large: file.link("large"),
          thumbnail40: file.link("thumbnail40"),
          thumb100: file.link("thumb100"),
          avatar: file.link("avatar"),
          preview: file.link("preview")
        },
        ext: file.ext || null,
        type: file.type || null,
        uploaderId,
        date: Date.now(),
        condos: [condoId]
      }
      if (!isExist) {
        EmergencyContactPhotos.insert(data)
      } else {
        EmergencyContactPhotos.update({ _id: isExist._id }, {...data, condos: [condoId, ...(!!isExist.condos ? isExist.condos : [])]})
      }
    },
    removeEmergencyContactPhoto: function (contactId) {
      const isExist = EmergencyContactPhotos.findOne({contactId: contactId})
      if (!!isExist) {
        EmergencyContactPhotoFiles.remove({_id: isExist.fileId})
        EmergencyContactPhotos.remove({contactId: contactId})
      }
    },
    submitEmergencyContact: function (id, data) {
      check(id, String)
      check(data, Object)

      const exist = EmergencyContact.findOne(id)
      const photoExist = EmergencyContactPhotos.findOne({contactId: id})
      if (!!photoExist) {
        EmergencyContactPhotos.update({ contactId: id }, {...photoExist, condos: _.map(data.condos, r => r.id)})
      }

      Meteor.call('addToGestionnaireHistory',
        'emergencyContact',
        id,
        'addedEmergencyContact',
        data.condos[0].id,
        `${data.firstName} ${data.lastName}`,
        Meteor.userId()
      )

      if (!!exist) {
        return EmergencyContact.update({ _id: id }, data)
      } else {
        return EmergencyContact.insert({_id: id, ...data})
      }
    },
    deleteEmergencyContact: function (condoId, contactId) {
      check(condoId, String)
      check(contactId, String)

      let exist = EmergencyContact.findOne(contactId)
      if (!!exist) {
        let condos = []
        if (condoId === 'all') {
          const condoDeleteRights = Meteor.listCondoUserHasRight('emergencyContact', 'delete')
          condos = _.reject(exist.condos, (c) => {return _.includes(condoDeleteRights, c.id)})
        } else {
          condos = _.reject(exist.condos, (c) => {return c.id === condoId})
        }

        Meteor.call('addToGestionnaireHistory',
          'emergencyContact',
          contactId,
          'removedEmergencyContact',
          condoId,
          '',
          Meteor.userId()
        )

        if (condos.length > 0) {
          // remove condo
          return EmergencyContact.update({ _id: contactId }, {...exist, condos: condos})
        } else {
          // condo is empty, delete contact
          return EmergencyContact.remove({ _id: contactId })
        }
      }
    }
  });
});
