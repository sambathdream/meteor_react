class SwitchBoard {
	constructor() {
		this.adminUserTemplate = {
			"userId" : "",
			"firstname" : "Admin",
			"lastname" : "ADMIN",
			"tel" : "",
			"level" : 5,
			"type" : "directionSiege",
			"condosInCharge" : [
			],
			"profile" : {
			},
			"isAdmin" : true
		}

		this.condoInChargeTemplate = {
			"access" : [
			1,
			2
			],
			"condoId" : "",
			"notifications" : {
				"incidents" : false,
				"forum_forum" : false,
				"forum_syndic" : false,
				"actualites" : false,
				"classifieds" : false,
				"messagerie" : false,
				"edl" : false,
				"new_user" : false
			}
		}

		this.residentTemplate = {
			"userId" : "",
			"condos" : [],
			"pendings" : []
		}

		this.condoResidentTemplate = {
			"condoId" : "",
			"notifications" : {
				"actualite" : false,
				"incident" : false,
				"forum_forum" : false,
				"forum_syndic" : false,
				"forum_reco" : false,
				"forum_boite" : false,
				"classifieds" : false,
				"resa_new" : false,
				"resa_rappel" : false,
				"msg_resident" : false,
				"msg_conseil" : false,
				"msg_gardien" : false,
				"msg_gestion" : false,
				"edl" : false
			},
			"invited" : Date.now(),
			"joined" : Date.now(),
			"preferences" : {
				"messagerie" : true
			},
			"buildings" : [
			{
				"buildingId" : "",
				"type" : "tenant"
			}
			]
		}
	}
}

export {SwitchBoard};
