import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { FilesCollection } from 'meteor/ostrio:files';
import { MarketServiceTypes } from '/common/collections/marketPlace'
import { Email } from './../../../common/lang/lang';
import { SwitchBoard } from './switchBoard.class.js';
import moment from 'moment'


var PNF = require('google-libphonenumber').PhoneNumberFormat;
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

Meteor.startup(function () {
  Meteor.methods({
    ServerScriptAddRolesInUserCollection: () => {
      let users = []
      const updateUsers = ({ userIndex, userId, condos }) => {
        if (userIndex >= 0) {
          users[userIndex].condos = condos.concat(users[userIndex].condos)
        } else {
          users.push({
            userId: userId,
            condos: condos
          })
        }
      }

      const enterprises = Enterprises.aggregate([
        { $project: { users: '$users.userId', condos: '$users.condosInCharge.condoId' } }
      ])
      const residents = Residents.aggregate([
        { $project: { userId: 1, condos: '$condos.condoId' } }
      ])

      if (enterprises.length > 0) {
        enterprises.forEach(enterprise => {
          if (enterprise.users && enterprise.users.length > 0) {
            enterprise.users.forEach((eUser, index) => {
              if (enterprise.condos.length > 0) {
                updateUsers({
                  userIndex: users.findIndex(user => user.userId === eUser),
                  userId: eUser,
                  condos: enterprise.condos[index].map(condoId => condoId + '.manager')
                })
              }
            })
          }
        })
      }
      if (residents.length > 0) {
        residents.forEach(resident => {
          if (resident.condos && resident.condos.length > 0) {
            updateUsers({
              userIndex: users.findIndex(user => user.userId === resident.userId),
              userId: resident.userId,
              condos: resident.condos.map(condoId => condoId + '.resident')
            })
          }
        })
      }

      users.forEach(user => {
        Meteor.users.update({
          _id: user.userId
        }, {
            $set: {
              roles: user.condos
            }
          })
      })
    },

    ServerScriptClearUsersRightNotUsedAnymore: () => {
      let notFound = 0
      let found = 0
      UsersRights.find({}, { fields: { userId: true, condoId: true } }).forEach(userRight => {
        const managerFound = !!Enterprises.findOne({
          users: {
            $elemMatch: {
              userId: userRight.userId,
              'condosInCharge.condoId': userRight.condoId
            }
          }
        }, { fields: { _id: true } })
        const residentFound = !!Residents.findOne({
          userId: userRight.userId,
          $or: [
            { 'condos.condoId': userRight.condoId },
            { 'pendings.condoId': userRight.condoId }
          ]
        }, { fields: { _id: true } })
        if (!managerFound && !residentFound) {
          notFound++
          UsersRights.remove({ _id: userRight._id })
          // console.log('userRight._id', userRight._id)
        } else {
          found++
        }
      })
      console.log('notFound', notFound)
      console.log('found', found)
    },

    ServerScriptUpdatePhoneCodeEnglishValues: () => {
      const values = { 'Afghanistan': 'Afghanistan', 'Afrique du Sud': 'South Africa', 'Albanie': 'Albania', 'Algérie': 'Algeria', 'Allemagne': 'Germany', 'Andorre': 'Andorra', 'Angola': 'Angola', 'Anguilla': 'Anguilla', 'Antigua-et-Barbuda': 'Antigua and Barbuda', 'Arabie saoudite': 'Saudi Arabia', 'Argentine': 'Argentina', 'Arménie': 'Armenia', 'Aruba': 'Aruba', 'Australie': 'Australia', 'Autriche': 'Austria', 'Azerbaïdjan': 'Azerbaijan', 'Bahamas': 'The Bahamas', 'Bahreïn': 'Bahrain', 'Bangladesh': 'Bangladesh', 'Barbade': 'Barbados', 'Belgique': 'Belgium', 'Belize': 'Belize', 'Bermudes': 'Bermuda', 'Bhoutan': 'Bhutan', 'Birmanie': 'Burma', 'Biélorussie': 'Belarus', 'Bolivie': 'Bolivia', 'Bosnie-Herzégovine': 'Bosnia and Herzegovina', 'Botswana': 'Botswana', 'Brunei': 'Brunei', 'Brésil': 'Brazil', 'Bulgarie': 'Bulgaria', 'Burkina Faso': 'Burkina Faso', 'Burundi': 'Burundi', 'Bénin': 'Benin', 'Cambodge': 'Cambodia', 'Cameroun': 'Cameroon', 'Canada': 'Canada', 'Cap-Vert': 'Cape Verde', 'Centrafrique': 'The Centrafrique', 'Chili': 'Chile', 'Chypre': 'Cyprus', 'Colombie': 'Colombia', 'Comores': 'Comoros', 'Corée du Nord': 'North Korea', 'Corée du Sud': 'South Korea', 'Costa Rica': 'Costa Rica', 'Croatie': 'Croatia', 'Cuba': 'Cuba', 'Côte dIvoire': 'Ivory Coast', 'Danemark': 'Denmark', 'Djibouti': 'Djibouti', 'Dominique': 'Dominique', 'Espagne': 'Spain', 'Estonie': 'Estonia', 'Fidji': 'Fiji', 'Finlande': 'Finland', 'France': 'France', 'Gabon': 'Gabon', 'Gambie': 'Gambia', 'Ghana': 'Ghana', 'Gibraltar': 'Gibraltar', 'Grenade': 'Grenada(Granada)', 'Groenland': 'Greenland', 'Grèce': 'Greece', 'Guadeloupe': 'Guadeloupe', 'Guam': 'Guam', 'Guatemala': 'Guatemala', 'Guinée': 'Guinea', 'Guinée équatoriale': 'Equatorial Guinea', 'Guinée-Bissau': 'Guinea-Bissau', 'Guyana': 'Guyana', 'Guyane': 'Guiana', 'Géorgie': 'Georgia', 'Haïti': 'Haiti', 'Honduras': 'Honduras', 'Hong Kong': 'Hong-Kong', 'Hongrie': 'Hungary', 'Inde': 'India', 'Indonésie': 'Indonesia', 'Irak': 'Iraq', 'Iran': 'Iran', 'Irlande': 'Ireland', 'Islande': 'Iceland', 'Israël': 'Israel', 'Italie': 'Italy', 'Jamaïque': 'Jamaica', 'Japon': 'Japan', 'Jordanie': 'Jordan', 'Kazakhstan': 'Kazakhstan', 'Kenya': 'Kenya', 'Kirghizistan': 'Kyrgyzstan', 'Kiribati': 'Kiribati', 'Koweït': 'Kuwait', 'La Réunion': 'Reunion', 'Laos': 'Laos', 'Lesotho': 'Lesotho', 'Lettonie': 'Latvia', 'Liban': 'Lebanon', 'Liberia': 'Liberia', 'Libye': 'Libya', 'Liechtenstein': 'Liechtenstein', 'Lituanie': 'Lithuania', 'Luxembourg': 'Luxembourg', 'Macao': 'Macao', 'Madagascar': 'Madagascar', 'Malaisie': 'Malaysia', 'Malawi': 'Malawi', 'Maldives': 'The Maldive Islands', 'Mali': 'Mali', 'Malouines': 'Falklands', 'Malte': 'Malta', 'Maroc': 'Morocco', 'Martinique': 'Martinique', 'Maurice': 'Maurice', 'Mauritanie': 'Mauritania', 'Mayotte': 'Mayotte', 'Mexique': 'Mexico', 'Micronésie': 'Micronesia', 'Moldavie': 'Moldavia(Moldova)', 'Monaco': 'Monaco', 'Mongolie': 'Mongolia', 'Montserrat': 'Montserrat', 'Monténégro': 'Montenegro', 'Mozambique': 'Mozambique', 'Namibie': 'Namibia', 'Nauru': 'Nauru', 'Nicaragua': 'Nicaragua', 'Niger': 'Niger', 'Nigeria': 'Nigeria', 'Niue': 'Niue', 'Norvège': 'Norway', 'Nouvelle-Calédonie': 'New Caledonia', 'Nouvelle-Zélande': 'New Zealand', 'Népal': 'Nepal', 'Oman': 'Oman', 'Ouganda': 'Uganda', 'Ouzbékistan': 'Uzbekistan', 'Pakistan': 'Pakistan', 'Palaos': 'Palaos', 'Panama': 'Panama', 'Papouasie-Nouvelle-Guinée': 'Papua New Guinea', 'Paraguay': 'Paraguay', 'Pays-Bas': 'Netherlands', 'Philippines': 'The Philippines(Filipinos)', 'Pologne': 'Poland', 'Polynésie française': 'French Polynesia', 'Porto Rico': 'Puerto Rico', 'Portugal': 'Portugal', 'Pérou': 'Peru', 'Qatar': 'Qatar', 'Roumanie': 'Romania', 'Royaume-Uni': 'The United Kingdom', 'Russie': 'Russia', 'Rwanda': 'Rwanda', 'République de Macédoine': 'Republic of Macedonia', 'République dominicaine': 'Dominican Republic', 'République du Congo': 'Republic of the Congo', 'République démocratique du Congo': 'Democratic Republic of the Congo', 'République tchèque': 'Czech Republic', 'Saint-Christophe-et-Niévès': 'Saint Kitts and Nevis', 'Saint-Marin': 'San Marino', 'Saint-Pierre-et-Miquelon': 'Saint Pierre and Miquelon', 'Saint-Vincent-et-les Grenadines': 'Saint-Vincent-et-les Grenadines', 'Sainte-Lucie': 'Saint Lucia', 'Salomon': 'Salomon', 'Salvador': 'Salvador', 'Samoa': 'Samoa', 'Samoa américaines': 'American Samoa', 'Sao Tomé-et-Principe': 'Sao Tomé-et-Principe', 'Serbie': 'Serbia', 'Seychelles': 'Seychelles', 'Sierra Leone': 'Sierra Leone', 'Singapour': 'Singapore', 'Slovaquie': 'Slovakia', 'Slovénie': 'Slovenia', 'Somalie': 'Somalia', 'Soudan': 'Sudan', 'Soudan du Sud': 'South Sudan', 'Sri Lanka': 'Sri Lanka', 'Suisse': 'Switzerland(Swiss)', 'Suriname': 'Suriname', 'Suède': 'Sweden', 'Swaziland': 'Swaziland', 'Syrie': 'Syria', 'Sénégal': 'Senegal', 'Tadjikistan': 'Tadjkistan', 'Tanzanie': 'Tanzania', 'Tchad': 'Chad', 'Thaïlande': 'Thailand', 'Timor oriental': 'East Timor', 'Togo': 'Togo', 'Tokelau': 'Tokelau', 'Tonga': 'Tonga', 'Trinité-et-Tobago': 'Trinidad and Tobago', 'Tunisie': 'Tunisia', 'Turkménistan': 'Turkmenistan', 'Turquie': 'Turkey', 'Tuvalu': 'Tuvalu', 'Ukraine': 'Ukraine', 'Uruguay': 'Uruguay', 'Vanuatu': 'Vanuatu', 'Venezuela': 'Venezuela', 'Viêt Nam': 'Vietnam', 'Wallis-et-Futuna': 'Wallis and Futuna', 'Yémen': 'Yemen', 'Zambie': 'Zambia', 'Zimbabwe': 'Zimbabwe', 'Égypte': 'Egypt', 'Émirats arabes unis': 'United Arab Emirates', 'Équateur': 'Ecuador', 'Érythrée': 'Eritrea', 'États-Unis': 'The United States', 'Éthiopie': 'Ethiopia', 'Îles Caïmans': 'Cayman Islands', 'Îles Cook': 'Cook Islands', 'Îles Féroé': 'Faroe Islands', 'Îles Mariannes du Nord': 'Northern Mariana Islands', 'Îles Marshall': 'Marshall Islands', 'Îles Turks-et-Caïcos': 'Islands Turks-et-Caïcos', 'Îles Vierges britanniques': 'British Virgin Islands', 'Îles Vierges des États-Unis': 'Virgin Islands of the United States' }
      _.each(values, (valueEn, valueFr) => {
        PhoneCode.update({ country: valueFr }, {
          $set: {
            country_en: valueEn
          }
        })
      })
    },
    ServerScriptUpdateCondoSettingsOptions: () => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      const modulesToChange = {
        // 'digitalWallet': false,
        // 'marketPlace': false,
        // 'buildingsList': true,
        // 'emergencyContact': false,
        'integrations': false,
        'print': false
      }
      _.each(modulesToChange, (value, key) => {
        console.log('key', key)
        console.log('value', value)
        Condos.update({ ['settings.options.' + key]: { $exists: false } }, {
          $set: {
            ['settings.options.' + key]: value
          }
        }, { multi: true })
      })
    },
    ServerScriptAddTwoHundredUsersToGrenelle: () => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      const condoId = Condos.findOne({ name: 'Résidence de Grenelle' })._id
      for (let i = 20; i < 221; i++) {
        const name = 'resident' + i
        const email = name + '@gmail.com'
        console.log('email', email)
        Accounts.createUser({ email: email })


        const user = Accounts.findUserByEmail(email)
        Meteor.createDefaultRightsForUserWithCondo(user._id, condoId, null)

        const residentId = Residents.insert({
          userId: user._id,
          condos: [{
            "condoId": condoId,
            "isSyndical": true,
            "isKeeper": false,
            "notifications": {
              "incident": true,
              "actualite": true,
              "forum_forum": true,
              "classifieds": true,
              "reservation": true,
              "msg_resident": true,
              "msg_conseil": true,
              "msg_gardien": true,
              "msg_gestion": true
            },
            "invited": 1511861293500.0,
            "joined": 1511861293500.0,
            "preferences": {
              "messagerie": true
            },
            "buildings": [
              {
                "buildingId": null,
                "type": "owner"
              }
            ],
            "userInfo": {
              "company": null,
              "diploma": null,
              "school": null,
              "studies": null,
              "porte": "14",
              "etage": "7",
              "office": null,
              "wifi": null
            }
          }],
          registeredFrom: ("web"),
          pendings: []
        });

        Meteor.users.update({
          _id: user._id,
        }, {
            $set: {
              profile: {
                "civilite": "Monsieur",
                "lastname": name + '_lastname',
                "firstname": name,
                "role": "Resident",
                "tel": "6 73 28 28 38",
                "state": true,
                "lang": "fr",
                "poste": null,
                "telCode": "33",
                "telCodeCountry": "France",
                "tel2": "1 33 23 45 34",
                "avatarId": null,
                "school": "3IS Institut international de l'image et du son (3IS BORDEAUX)",
                "matiere": "Management, Conseil & Stratégie",
                "diplome": "1231231",
                "company": "MY NAME IS HERE",
                "tel2Code": "33"
              },
              'identities.residentId': residentId,
            },
          });
        Accounts.setPassword(user._id, 'resident');

        console.log('added num. ' + i + ' user')

      }
    },
    changeUsersPassword: (userId, newPassword) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      check(userId, String)
      check(newPassword, String)

      return Accounts.setPassword(userId, newPassword)
    },
    updateEntity: (entityId, entityForm) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      check(entityId, String)
      check(entityForm, Match.ObjectIncluding({
        entityName: String,
        adressInfos: Match.ObjectIncluding({
          address: String,
          city: String,
          code: String,
          country: String,
          id: String,
          lat: Number,
          lng: Number,
          road: String,
          street_number: String
        }),
        email: Match.Where((val) => {
          check(val, String);
          return Isemail.validate(val);
        }),
        siretNumber: Match.OneOf(null, String),
        legalGuardian: String,
        phone: Match.OneOf(null, String),
        phoneCode: Match.OneOf(null, String),
        logoMobile: Match.OneOf(null, String),
        logoWeb: Match.OneOf(null, String),
        domain: String,
        validCopro: Boolean,
        validMono: Boolean,
        validOffice: Boolean,
        validStudent: Boolean
      }))

      const oldEntity = Enterprises.findOne({ _id: entityId })

      if (oldEntity) {

        if (oldEntity.logo.fileId && oldEntity.logo.fileId !== entityForm.logoWeb) {
          UserFiles.remove({ _id: oldEntity.logo.fileId })
        }
        if (oldEntity.logo.fileMobileId && oldEntity.logo.fileMobileId !== entityForm.logoMobile) {
          UserFiles.remove({ _id: oldEntity.logo.fileMobileId })
        }

        return Enterprises.update({ _id: entityId }, {
          $set: {
            name: entityForm.entityName,
            info: {
              address: entityForm.adressInfos.street_number + ' ' + entityForm.adressInfos.road,
              city: entityForm.adressInfos.city,
              code: entityForm.adressInfos.code,
              email: entityForm.email,
              siret: entityForm.siretNumber,
              chef: entityForm.legalGuardian,
              tel: entityForm.phone,
              telCode: entityForm.phoneCode
            },
            logo: {
              showLogo: entityForm.logoWeb !== null,
              fileId: entityForm.logoWeb,
              fileMobileId: entityForm.logoMobile,
              showMonBuilding: entityForm.logoWeb === null,
            },
            settings: {
              domain: entityForm.domain,
              validUsers: {
                copro: entityForm.validCopro,
                mono: entityForm.validMono,
                etudiante: entityForm.validStudent,
                office: entityForm.validOffice
              }
            }
          }
        })
      } else {
        throw new Meteor.Error(300, "Wrong entity Id");
      }
    },
    createEntity: (newEntity) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      check(newEntity, Match.ObjectIncluding({
        entityName: String,
        adressInfos: Match.ObjectIncluding({
          address: String,
          city: String,
          code: String,
          country: String,
          id: String,
          lat: Number,
          lng: Number,
          road: String,
          street_number: String
        }),
        email: Match.Where((val) => {
          check(val, String);
          return Isemail.validate(val);
        }),
        siretNumber: Match.OneOf(null, String),
        legalGuardian: String,
        phone: Match.OneOf(null, String),
        phoneCode: Match.OneOf(null, String),
        logoMobile: Match.OneOf(null, String),
        logoWeb: Match.OneOf(null, String),
        validCopro: Boolean,
        validMono: Boolean,
        validOffice: Boolean,
        validStudent: Boolean
      }))

      return Enterprises.insert({
        name: newEntity.entityName,
        createdAt: Date.now(),
        info: {
          address: newEntity.adressInfos.street_number + ' ' + newEntity.adressInfos.road,
          city: newEntity.adressInfos.city,
          code: newEntity.adressInfos.code,
          email: newEntity.email,
          siret: newEntity.siretNumber,
          chef: newEntity.legalGuardian,
          tel: newEntity.phone,
          telCode: newEntity.phoneCode
        },
        logo: {
          showLogo: newEntity.logoWeb !== null,
          fileId: newEntity.logoWeb,
          fileMobileId: newEntity.logoMobile,
          showMonBuilding: newEntity.logoWeb === null,
        },
        condos: [],
        inactive: true,
        users: [],
        settings: {
          validUsers: {
            copro: newEntity.validCopro,
            mono: newEntity.validMono,
            etudiante: newEntity.validStudent,
            office: newEntity.validOffice
          }
        }
      })
    },
    addBuildings: (newBuildings) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let newCondoIds = []
      check(newBuildings, [Match.ObjectIncluding({
        formBuilding: Match.ObjectIncluding({
          buildingName: Match.OneOf(null, String),
          buildingId: Match.OneOf(null, String),
          type: Match.OneOf('copro', 'mono', 'etudiante', 'office'),
          subBuildings: Match.OneOf(null, [
            Match.ObjectIncluding({
              id: String,
              name: String,
              adressInfos: Match.ObjectIncluding({
                address: String,
                city: String,
                code: String,
                country: String,
                id: String,
                lat: Number,
                lng: Number,
                road: String,
                street_number: String
              }),
              selected: Boolean
            })
          ]
          )
        })
      })])

      newBuildings.forEach(condo => {
        newCondoIds.push(createNewBuilding(condo))

      })
      return newCondoIds
    },
    addBuildingsToEntity: (entityId, newBuildings) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let newCondoIds = []
      check(entityId, String)
      check(newBuildings, [Match.ObjectIncluding({
        formBuilding: Match.ObjectIncluding({
          buildingName: Match.OneOf(null, String),
          buildingId: Match.OneOf(null, String),
          type: Match.OneOf('copro', 'mono', 'etudiante', 'office'),
          subBuildings: Match.OneOf(null, [
            Match.ObjectIncluding({
              id: String,
              name: String,
              adressInfos: Match.ObjectIncluding({
                address: String,
                city: String,
                code: String,
                country: String,
                id: String,
                lat: Number,
                lng: Number,
                road: String,
                street_number: String
              }),
              selected: Boolean
            })
          ]
          )
        })
      })])

      newBuildings.forEach(condo => {
        newCondoIds.push(createNewBuilding(condo))

      })
      const entity = Enterprises.findOne({ _id: entityId })
      const allCondoIds = [...entity.condos, ...newCondoIds]
      Enterprises.update({ _id: entityId }, { $set: { condos: allCondoIds } })
      return newCondoIds
    },
    saveCondoCustomEmail: function (condoId, value, linkValue) {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      return Condos.update({ _id: condoId }, { $set: { 'settings.customEmail': (value || ''), 'settings.signatureLink': (linkValue || '') } })
    },
    ServerScriptMessengerFileCollection: function () {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let fs = require('fs');
      let path = require('path');
      const dir = 'assets/app/uploads/MessengerFiles'

      let files = _.without(_.flattenDeep(_.map(MessagesFeed.find().fetch(), 'files')), undefined, null, '')
      let userFiles = UserFiles.find({ _id: { $in: files } }).fetch()
      let conversationIdToRemove = []
      userFiles.forEach((file) => {
        try {
          console.log('start')
          const oldPath = file.path
          file._collectionName = 'MessengerFiles'
          file.path = file.path.replace(/UserFiles/, 'MessengerFiles')
          file._storagePath = file._storagePath.replace(/UserFiles/, 'MessengerFiles')

          let shouldMove = true
          if (!!file.meta.msgId && !!Messages.findOne({ _id: file.meta.msgId })) {
            let condoId = Messages.findOne({ _id: file.meta.msgId }).condoId
            file.meta.condoId = condoId
          } else if (!!file.meta.msgId && !Messages.findOne({ _id: file.meta.msgId })) {
            shouldMove = false
            if (!_.includes(conversationIdToRemove, file.meta.msgId)) {
              conversationIdToRemove.push(file.meta.msgId)
            }
          }

          if (shouldMove === true) {
            UserFiles.collection.remove(file._id)

            const f = path.basename(oldPath);
            let dest = path.resolve(dir, f)
            console.log('after resolve')

            _.each(file.versions, (elem, index) => {
              if (index !== 'original') {
                const oldVersionPath = file.versions[index].path
                const f = path.basename(oldVersionPath);
                const dest = path.resolve(dir, f)
                fs.rename(oldVersionPath, dest, Meteor.bindEnvironment((err) => {
                  if (err) {
                    console.log('Unknown file')
                  } else {
                    console.log('Successfully moved versions ' + index)
                  }
                }))
              }
              file.versions[index].path = file.versions[index].path.replace(/UserFiles/, 'MessengerFiles')
            })
            fs.rename(oldPath, dest, Meteor.bindEnvironment((err) => {
              if (err) {
                console.log('Unknown file')
              } else {
                MessengerFiles.collection.insert(file, Meteor.bindEnvironment((error, result) => {
                  console.log('error', error)
                }))
                console.log('Successfully moved')
              }
            }))
          } else {
            UserFiles.remove(file._id)
          }


        } catch (e) {
          console.log('e', e)
        }

        // console.log('moved messenger file._id', file._id)
      })
      if (conversationIdToRemove.length > 0) {
        console.log('conversationIdToRemove', conversationIdToRemove)
        MessagesFeed.remove({ messageId: { $in: conversationIdToRemove } })
      }
    },
    ServerScriptSetNewAdsViews: function () {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
        throw new Meteor.Error(300, "Not Authenticated");
      let ads = ClassifiedsAds.find().fetch()
      _.each(ads, function (ad) {
        let newViews = {}
        _.each(ad.views, function (view) {
          newViews[view.userId] = (newViews[view.userId] && newViews[view.userId] > view.date) ? newViews[view.userId] : view.date
        })
        ClassifiedsAds.update(ad._id, {
          $set: {
            views: newViews
          }
        })
      })
    },
    ServerScriptSetTypoToOption: function () {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
        throw new Meteor.Error(300, "Not Authenticated");
      let condos = Condos.find().fetch();
      _.each(condos, function (condo) {
        if (!!condo.settings.options.typologie) {
          let typo = condo.settings.options.typologie;
          console.log(condo)
          delete condo.settings.options.typologie;
          condo.settings.options.couronne = typo.couronne
          condo.settings.options.forumSyndic = typo.forumSyndic
          condo.settings.options.messengerSyndic = typo.messengerSyndic

          Condos.update(
            {
              _id: condo._id
            },
            {
              $set: {
                "settings.options": condo.settings.options
              }
            }
          )
        }
      })
    },
    addNewCondoToOccupant: function (userId, newCondo) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let user = Meteor.users.findOne(userId);
      if (!user) {
        throw new Meteor.Error(601, "Invalid id", "Cannot find user");
      }

      let buildingStatus = BuildingStatus.findOne(newCondo.statusId).key;
      condoToAdd = {
        condoId: newCondo.condoId,
        invited: new Date(),
        roleId: newCondo.roleId,
        invitByGestionnaire: false,
        notifications: {
          "actualite": true,
          "incident": true,
          "forum_forum": true,
          "forum_syndic": true,
          "forum_reco": true,
          "forum_boite": true,
          "classifieds": true,
          "resa_new": true,
          "resa_rappel": true,
          "msg_resident": true,
          "msg_conseil": true,
          "msg_gardien": true,
          "msg_gestion": true,
          "edl": true
        },
        preferences: {
          messagerie: true
        },
        buildings: [{
          buildingId: newCondo.buildingId,
          type: buildingStatus,
          etage: newCondo.floor,
          porte: newCondo.door
        }]
      };
      let thisResident = Residents.findOne({ _id: user.identities.residentId });

      if (thisResident.pendings == undefined || thisResident.pendings.length == 0) {
        condoToAdd.joined = new Date().getTime();
        Meteor.defer(function () {
          Residents.update({ _id: user.identities.residentId }, { $push: { condos: condoToAdd } });

          Incidents.update({ "state.status": 1 }, { $push: { 'view': { "lastView": Date.now(), "userId": userId, "lastViewHistoric": Date.now() } } }, { multi: true });
          // ClassifiedsAds.update({}, {$push: {'views': {userId: userId, date: Date.now()}}}, {multi: true});
          ActuPosts.update({}, { $push: { 'views': userId } }, { multi: true });

          Meteor.createDefaultRightsForUserWithCondo(userId, condoToAdd.condoId, condoToAdd.roleId);
        })
      }
      else {
        Residents.update({ _id: user.identities.residentId }, { $push: { pendings: condoToAdd } });
      }
      sendCondoInvitationEmailToExistingUser(user.emails[0].address, condoToAdd.condoId, user.profile.lang)
      sendNotifForNewCondo(userId, condoToAdd.condoId)
      return userId
    },
    ServerScriptTransformPhone: function () {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
        throw new Meteor.Error(300, "Not Authenticated");
      let users = Meteor.users.find().fetch();
      let defaultCode = 33;
      let defaultCountry = "France";
      _.each(users, function (user) {
        console.log("------------------------")
        console.log(user.profile.firstname + " > " + user.profile.telCode + " : " + user.profile.tel);

        let newCode = defaultCode;
        let newCountry = defaultCountry;
        if (user.profile.telCode == undefined || user.profile.telCode == null || user.profile.telCode == "")
          newCode = defaultCode;
        else
          newCode = user.profile.telCode;

        if (user.profile.telCodeCountry == undefined || user.profile.telCodeCountry == null || user.profile.telCodeCountry == "")
          newCountry = defaultCountry;
        else
          newCountry = user.profile.telCodeCountry;


        let number = '+' + (newCode ? newCode : "") + (user.profile.tel ? user.profile.tel : "");
        let number2 = '+33' + (user.profile.tel2 ? user.profile.tel2 : "");
        console.log("BEFORE PARSE =>   ", number, "  ;  ", number2);
        let tel = user.profile.tel;
        let tel2 = user.profile.tel2;
        let telCode = user.profile.telCode;
        let telCodeCountry = user.profile.telCodeCountry;

        let newTel = "";
        let newTel2 = "";

        try {
          number = phoneUtil.parse(number, "FR");
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            if (phoneUtil.getNumberType(number, PNF.INTERNATIONAL) == 1) {
              newTel = phoneUtil.format(number, PNF.NATIONAL);
              telCode = newCode;
              telCodeCountry = newCountry;
            }
            else {
              newTel2 = phoneUtil.format(number, PNF.NATIONAL);
            }
          }
        } catch (e) {
          console.log("Not valid number");
        }
        try {
          number = phoneUtil.parse(number2, "FR");
          if (phoneUtil.isValidNumber(number, PNF.INTERNATIONAL)) {
            if (phoneUtil.getNumberType(number, PNF.INTERNATIONAL) == 1) {
              if (newTel != "") {
                newTel2 = phoneUtil.format(number, PNF.NATIONAL);
              }
              else {
                newTel = phoneUtil.format(number, PNF.NATIONAL);
                telCode = newCode;
                telCodeCountry = newCountry;
              }
            }
            else {
              newTel2 = phoneUtil.format(number, PNF.NATIONAL);
            }
          }
        } catch (e) {
          console.log("Not valid number");
        }
        tel = newTel;
        tel2 = newTel2
        console.log(tel)
        console.log(telCode)
        console.log(telCodeCountry)
        console.log(tel2)
        let ret = Meteor.users.update({ _id: user._id }, {
          $set: {
            "profile.tel": tel,
            "profile.telCode": telCode,
            "profile.telCodeCountry": telCodeCountry,
            "profile.tel2": tel2
          }
        })
        if (user.identities.gestionnaireId != undefined) {
          let enterpriseId = user.identities.gestionnaireId;
          Enterprises.update({
            _id: enterpriseId,
            "users.userId": user._id
          },
            {
              $set: {
                "users.$.tel": tel,
                "users.$.telCode": telCode,
                "users.$.telCodeCountry": telCodeCountry,
                "users.$.tel2": tel2
              }
            }
          )
        }
      })
    },
    updateOccupantInfo: function (userId, condoId, buildingId, type, value, indexBuilding) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");

      let resident = Residents.findOne({ 'userId': userId });
      let buildings = [];

      _.each(resident.condos, function (condo, index) {
        if (condo.condoId == condoId) {
          if (condo.buildings[indexBuilding].buildingId == buildingId) {
            resident.condos[index].buildings[indexBuilding][type] = value;
            buildings = resident.condos[index].buildings;
          }
        }
      });

      Residents.update(
        {
          userId: userId,
          "condos.condoId": condoId
        },
        {
          $set: {
            "condos.$.buildings": buildings
          }
        })
    },
    updateCondosModulesOptions: function (condoId, data) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let options = CondosModulesOptions.findOne({ condoId: condoId });
      if (options && options[data.module]) {
        if (data.toChange != null) {
          if (data.newValue == false && _.includes(options[data.module][data.param], data.toChange)) {
            let thisIndex = _.indexOf(options[data.module][data.param], data.toChange);
            options[data.module][data.param].splice(thisIndex, 1);
          }
          if (data.newValue == true && !_.includes(options[data.module][data.param], data.toChange)) {
            if (!options[data.module][data.param]) {
              options[data.module][data.param] = []
            }
            options[data.module][data.param].push(data.toChange);
          }
        }
        else {
          options[data.module][data.param] = data.newValue;
        }
        query = {};
        obj = data.module + "." + data.param;
        if (_.isArray(options[data.module][data.param]) || _.isObject(options[data.module][data.param])) {
          query[obj] = _.without(options[data.module][data.param], null, undefined);
        }
        else {
          query[obj] = options[data.module][data.param];
        }

        CondosModulesOptions.update({ condoId: condoId }, {
          $set: query
        })
      } else {

      }
    },
    ServerScriptCreateCondosModulesOptions: function (forceRecreate) {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
        throw new Meteor.Error(300, "Not Authenticated");
      let condos = Condos.find().fetch();
      if (forceRecreate == true)
        CondosModulesOptions.remove({});
      _.each(condos, function (condo) {
        let existingOptions = CondosModulesOptions.findOne({ condoId: condo._id })
        if (!existingOptions) {
          let condoType = condo.settings.condoType;
          if (condoType == "etudiante")
            condoType = "student";
          var query = "forCondoType." + condoType;
          let rolesId = _.map(DefaultRoles.find({ [query]: true, for: "occupant", name: { $ne: "Occupant" } }).fetch(), "_id");
          CondosModulesOptions.insert({
            condoId: condo._id,
            incident: {
              managerActionVisibleBy: rolesId,
            },
            profile: {
              school: condoType === 'student',
              diploma: condoType === 'student',
              studies: condoType === 'student',
              floor: true,
              door: condoType !== 'office',
              company: condoType === 'office',
              office: condoType === 'office',
              documents: false,
              payments: false,
              wifi: false
            },
            forum: {
              type: 'normal'
            }
          })
        } else {
          let condoType = condo.settings.condoType;
          if (condoType == "etudiante")
            condoType = "student";

          CondosModulesOptions.update({ _id: existingOptions._id }, {
            condoId: condo._id,
            incident: {
              managerActionVisibleBy: existingOptions.incident.rolesId,
            },
            profile: {
              school: existingOptions.profile.school || condoType === 'student',
              diploma: existingOptions.profile.diploma || condoType === 'student',
              studies: existingOptions.profile.studies || condoType === 'student',
              floor: existingOptions.profile.floor || true,
              door: existingOptions.profile.door || condoType !== 'office',
              company: existingOptions.profile.company || condoType === 'office',
              office: existingOptions.profile.office || condoType === 'office',
              documents: existingOptions.profile.documents || false,
              payments: existingOptions.profile.payments || false,
              wifi: existingOptions.profile.wifi || false
            },
            forum: {
              type: 'normal'
            }
          })
        }
      })
    },
    creatCondoContactForNewCondo: function (condoId, managerId) {
      let condo = Condos.findOne(condoId);
      CondoContact.remove({ condoId: condo._id }, { multi: true });
      let condoEnterprise = Enterprises.findOne({ condos: condo._id });
      let type = condo.settings.condoType;
      let contactCondo = condo.contacts;
      let newDefaultContact = {
        condoId: condo._id,
        defaultContact: {
          declarationDetailId: DeclarationDetails.findOne({ "key": "defaultContact" })._id,
          userId: managerId,
        },
        contactSet: [],
        contactReminder: []
      };
      let incidentPriorityUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "urgent" })._id;
      let incidentPriorityNonUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "no-urgent" })._id;

      newDefaultContact.contactReminder = [
        {
          priorityId: incidentPriorityUrgent,
          timeValue: 24
        },
        {
          priorityId: incidentPriorityNonUrgent,
          timeValue: 48
        }
      ]

      let defaultContactCase = ContactManagement.findOne({ condoId: "default", forCondoType: type });

      _.each(defaultContactCase.messengerSet, function (list) {
        _.each(list.declarationDetailIds, function (declarationId) {
          if (DeclarationDetails.findOne({ _id: declarationId }).key != "defaultContact") {
            newDefaultContact.contactSet.push({
              messageTypeId: list.id,
              declarationDetailId: declarationId,
              userIds: []
            });
          }
        })
      });

      CondoContact.upsert({ condoId: condo._id }, newDefaultContact);
    },
    saveIncidentTypeForCondo: function (condoId, incidentType) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let error = "";
      if (!incidentType.pictureLink)
        error += "spanPicto";
      if (!incidentType.value_fr || incidentType.value_fr == "" || incidentType.value_fr == "-")
        error += (error == "" ? "" : "|") + "inputFr";
      if (!incidentType.value_en || incidentType.value_en == "" || incidentType.value_en == "-")
        error += (error == "" ? "" : "|") + "inputEn";
      if (error != "")
        throw new Meteor.Error(531, error);
      if (!ContactManagement.findOne({ condoId: condoId })) {
        let condoType = Condos.findOne(condoId).settings.condoType;
        defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });

        delete defaultContactManagement._id;
        delete defaultContactManagement.forCondoType;
        delete defaultContactManagement.condoId;
        defaultContactManagement.condoId = condoId;
        ContactManagement.insert(defaultContactManagement);
      }
      let fromOtherToIncident = false;
      let fromIncidentToOther = false;
      let thisType = IncidentType.findOne(incidentType.typeId);
      let newId = null;
      if (thisType && thisType.condoId == "default") {
        delete thisType.condoId;
        delete thisType._id;
        delete thisType.defaultType;

        thisType.condoId = condoId;
        thisType["value-fr"] = incidentType.value_fr;
        thisType["value-en"] = incidentType.value_en;
        thisType.picto = incidentType.pictureLink;
        thisType.defaultType = null;
        if (thisType.defaultType != "incident" && incidentType.isIncident == true) {
          fromOtherToIncident = true;
        }
        if (thisType.defaultType == "incident" && incidentType.isIncident == false) {
          fromIncidentToOther = true;
        }
        if (incidentType.isIncident == true)
          thisType.defaultType = "incident";
        newId = IncidentType.insert(thisType);
        ContactManagement.update(
          {
            condoId: condoId,
            "messengerSet.id": incidentType.typeId
          }, {
            $set: {
              "messengerSet.$.id": newId
            }
          }
        );

        let condoContact = CondoContact.findOne({ condoId: condoId });
        _.each(condoContact.contactSet, function (elem, index) {
          if (elem.messageTypeId == incidentType.typeId)
            condoContact.contactSet[index].messageTypeId = newId;
        })
        CondoContact.update(condoContact._id, {
          $set: {
            contactSet: _.without(condoContact.contactSet, null, undefined)
          }
        });
      }
      else {
        if (thisType.defaultType != "incident" && incidentType.isIncident == true) {
          fromOtherToIncident = true;
        }
        if (thisType.defaultType == "incident" && incidentType.isIncident == false) {
          fromIncidentToOther = true;
        }
        newId = incidentType.typeId;
        IncidentType.update({ _id: incidentType.typeId }, {
          $set: {
            "value-fr": incidentType.value_fr,
            "value-en": incidentType.value_en,
            "picto": incidentType.pictureLink,
            "defaultType": (incidentType.isIncident == true ? "incident" : null),
          },
          $unset: {
            "isNewEmpty": 1
          }
        });
      }
      if (fromIncidentToOther == true)
        setContactManagementFromOneToOtherOne(condoId, newId, "other");
      if (fromOtherToIncident == true)
        setContactManagementFromOneToOtherOne(condoId, newId, "incident");
    },
    createNewIncidentType: function (condoId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!IncidentType.findOne({ condoId: condoId })) {
        setIncidentTypeForCondoFromDefault(condoId);
      }
      let incidentTypeId = createNewEmptyIncidentType(condoId);
      updateManagementWithNewIncidentType(condoId, incidentTypeId);
    },
    deleteIncidentType: function (condoId, typeId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (IncidentType.findOne({ _id: typeId, condoId: "default" })) {
        setIncidentTypeForCondoFromDefaultWithoutOne(condoId, typeId);
      }
      else
        deleteIncidentTypeForCondo(condoId, typeId);
      checkIfStillIncidentType(condoId);
    },
    createNewDropdownDetail: function (condoId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      DeclarationDetails.insert({
        "isDropdownValue": true,
        "value-fr": "",
        "value-en": "",
        "condoId": condoId,
        "isNewEmpty": true
      })
    },
    deleteDropdownDetail: function (condoId, detailId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!ContactManagement.findOne({ condoId: condoId })) {
        let condoType = Condos.findOne(condoId).settings.condoType;
        defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });

        delete defaultContactManagement._id;
        delete defaultContactManagement.forCondoType;
        delete defaultContactManagement.condoId;
        defaultContactManagement.condoId = condoId;
        ContactManagement.insert(defaultContactManagement);
      }
      let thisManagementSet = ContactManagement.findOne({ condoId: condoId }).messengerSet;
      emptyIndex = [];
      _.each(thisManagementSet, function (set, index) {
        thisManagementSet[index].declarationDetailIds = _.filter(thisManagementSet[index].declarationDetailIds, function (id) {
          if (id != detailId)
            return true;
        });
        if (thisManagementSet[index].declarationDetailIds.length < 1)
          throw new Meteor.Error(405, "Not allowed to deleted last item");
      })
      ContactManagement.update({ condoId: condoId }, {
        $set: {
          messengerSet: thisManagementSet
        }
      });
      let thisDetail = DeclarationDetails.findOne(detailId);
      if (thisDetail && thisDetail.key == undefined && thisDetail.condoId == condoId) {
        DeclarationDetails.remove(detailId);
      }
    },
    saveDropdownDetailForCondo: function (condoId, dropdownDetail) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!ContactManagement.findOne({ condoId: condoId })) {
        let condoType = Condos.findOne(condoId).settings.condoType;
        defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });

        delete defaultContactManagement._id;
        delete defaultContactManagement.forCondoType;
        delete defaultContactManagement.condoId;
        defaultContactManagement.condoId = condoId;
        ContactManagement.insert(defaultContactManagement);
      }
      let error = "";
      if (!dropdownDetail.value_fr || dropdownDetail.value_fr == "" || dropdownDetail.value_fr == "-")
        error += "inputFr";
      if (!dropdownDetail.value_en || dropdownDetail.value_en == "" || dropdownDetail.value_en == "-")
        error += (error == "" ? "" : "|") + "inputEn";
      if (error != "")
        throw new Meteor.Error(531, error);
      let detailToChange = DeclarationDetails.findOne(dropdownDetail.detailsId);
      if (detailToChange && detailToChange.key == undefined && detailToChange.condoId == condoId) {
        DeclarationDetails.update({ _id: dropdownDetail.detailsId }, {
          $set: {
            "value-fr": dropdownDetail.value_fr,
            "value-en": dropdownDetail.value_en,
            "condoId": condoId,
            "isDropdownValue": true,
          },
          $unset: {
            "isNewEmpty": 1
          }
        });
        let thisManagementSet = ContactManagement.findOne({ condoId: condoId }).messengerSet;
        _.each(thisManagementSet, function (set, index) {
          if (set.id == dropdownDetail.typeId) {
            if (!_.includes(set.declarationDetailIds, dropdownDetail.detailsId))
              thisManagementSet[index].declarationDetailIds.push(dropdownDetail.detailsId);
          }
        })
        ContactManagement.update({ condoId: condoId }, {
          $set: {
            messengerSet: thisManagementSet
          }
        });

      }
      else if (detailToChange && detailToChange.key) {
        let newId = DeclarationDetails.insert({
          "value-fr": dropdownDetail.value_fr,
          "value-en": dropdownDetail.value_en,
          "condoId": condoId,
          "isDropdownValue": true,
        });
        let thisManagementSet = ContactManagement.findOne({ condoId: condoId }).messengerSet;
        _.each(thisManagementSet, function (set, indexSet) {
          if (set.id == dropdownDetail.typeId) {
            _.each(set.declarationDetailIds, function (id, indexId) {
              if (id == dropdownDetail.detailsId)
                thisManagementSet[indexSet].declarationDetailIds[indexId] = newId;
            })
          }
        })
        ContactManagement.update({ condoId: condoId }, {
          $set: {
            messengerSet: thisManagementSet
          }
        });

      }


    },
    saveIncidentDetailsForCondo: function (condoId, incidentDetail) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let error = "";
      if (!incidentDetail.pictureLink)
        error += "spanPicto";
      if (!incidentDetail.value_fr || incidentDetail.value_fr == "" || incidentDetail.value_fr == "-")
        error += (error == "" ? "" : "|") + "inputFr";
      if (!incidentDetail.value_en || incidentDetail.value_en == "" || incidentDetail.value_en == "-")
        error += (error == "" ? "" : "|") + "inputEn";
      if (error != "")
        throw new Meteor.Error(531, error);
      let thisDetail = IncidentDetails.findOne(incidentDetail.detailsId);
      if (thisDetail && thisDetail.condoId == "default") {
        delete thisDetail.condoId;
        delete thisDetail._id;
        delete thisDetail.defaultDetail;

        thisDetail.condoId = condoId;
        thisDetail["value-fr"] = incidentDetail.value_fr;
        thisDetail["value-en"] = incidentDetail.value_en;
        thisDetail.picto = incidentDetail.pictureLink;
        thisDetail.defaultDetail = null;
        let newId = IncidentDetails.insert(thisDetail);
      }
      else {
        let newId = IncidentDetails.update({ _id: incidentDetail.detailsId }, {
          $set: {
            "value-fr": incidentDetail.value_fr,
            "value-en": incidentDetail.value_en,
            "picto": incidentDetail.pictureLink
          },
          $unset: {
            "isNewEmpty": 1
          }
        });
      }
    },
    createNewIncidentDetails: function (condoId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!IncidentDetails.findOne({ condoId: condoId })) {
        setIncidentDetailForCondoFromDefault(condoId);
      }
      let incidentDetailId = createNewEmptyIncidentDetails(condoId);
    },
    deleteIncidentDetails: function (condoId, typeId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (IncidentDetails.findOne({ _id: typeId, condoId: "default" })) {
        setIncidentDetailForCondoFromDefaultWithoutOne(condoId, typeId);
      }
      else
        deleteIncidentDetailForCondo(condoId, typeId);
    },
    saveIncidentAreaForCondo: function (condoId, incidentArea) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let error = "";
      if (!incidentArea.pictureLink)
        error += "spanPicto";
      if (!incidentArea.value_fr || incidentArea.value_fr == "" || incidentArea.value_fr == "-")
        error += (error == "" ? "" : "|") + "inputFr";
      if (!incidentArea.value_en || incidentArea.value_en == "" || incidentArea.value_en == "-")
        error += (error == "" ? "" : "|") + "inputEn";
      if (error != "")
        throw new Meteor.Error(531, error);
      let thisArea = IncidentArea.findOne(incidentArea.areaId);
      if (thisArea && thisArea.condoId == "default") {
        delete thisArea.condoId;
        delete thisArea._id;
        delete thisArea.defaultArea;

        thisArea.condoId = condoId;
        thisArea["value-fr"] = incidentArea.value_fr;
        thisArea["value-en"] = incidentArea.value_en;
        thisArea.picto = incidentArea.pictureLink;
        thisArea.defaultArea = null;
        let newId = IncidentArea.insert(thisArea);
      }
      else {
        let newId = IncidentArea.update({ _id: incidentArea.areaId }, {
          $set: {
            "value-fr": incidentArea.value_fr,
            "value-en": incidentArea.value_en,
            "picto": incidentArea.pictureLink
          },
          $unset: {
            "isNewEmpty": 1
          }
        });
      }
    },
    createNewIncidentArea: function (condoId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!IncidentArea.findOne({ condoId: condoId })) {
        setIncidentAreaForCondoFromDefault(condoId);
      }
      let incidentAreaId = createNewEmptyIncidentArea(condoId);
    },
    deleteIncidentArea: function (condoId, typeId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (IncidentArea.findOne({ _id: typeId, condoId: "default" })) {
        setIncidentAreaForCondoFromDefaultWithoutOne(condoId, typeId);
      }
      else
        deleteIncidentAreaForCondo(condoId, typeId);
    },
    saveIncidentPriorityForCondo: function (condoId, incidentPriority) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let error = "";
      if (!incidentPriority.pictureLink)
        error += "spanPicto";
      if (!incidentPriority.value_fr || incidentPriority.value_fr == "" || incidentPriority.value_fr == "-")
        error += (error == "" ? "" : "|") + "inputFr";
      if (!incidentPriority.value_en || incidentPriority.value_en == "" || incidentPriority.value_en == "-")
        error += (error == "" ? "" : "|") + "inputEn";
      if (error != "")
        throw new Meteor.Error(531, error);
      let thisPriority = IncidentPriority.findOne(incidentPriority.priorityId);
      if (thisPriority && thisPriority.condoId == "default") {
        delete thisPriority.condoId;
        delete thisPriority._id;
        delete thisPriority.defaultDetail;

        thisPriority.condoId = condoId;
        thisPriority["value-fr"] = incidentPriority.value_fr;
        thisPriority["value-en"] = incidentPriority.value_en;
        thisPriority.picto = incidentPriority.pictureLink;
        thisPriority.defaultPriority = incidentPriority.showUrgent == true ? "urgent" : "no-urgent";
        let newId = IncidentPriority.insert(thisPriority);
        CondoContact.update({
          condoId: condoId,
          "contactReminder.priorityId": incidentPriority.priorityId
        }, {
            $set: {
              "contactReminder.$.priorityId": newId
            }
          });
      }
      else {
        let newId = IncidentPriority.update({ _id: incidentPriority.priorityId }, {
          $set: {
            "value-fr": incidentPriority.value_fr,
            "value-en": incidentPriority.value_en,
            "picto": incidentPriority.pictureLink,
            "defaultPriority": incidentPriority.showUrgent == true ? "urgent" : "no-urgent"
          },
          $unset: {
            "isNewEmpty": 1
          }
        });
      }
    },
    createNewIncidentPriority: function (condoId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (!IncidentPriority.findOne({ condoId: condoId })) {
        setIncidentPriorityForCondoFromDefault(condoId);
      }
      let incidentPriorityId = createNewEmptyIncidentPriority(condoId);
      updateReminderWithNewIncidentPriority(condoId, incidentPriorityId);
    },
    deleteIncidentPriority: function (condoId, typeId) {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      if (IncidentPriority.findOne({ _id: typeId, condoId: "default" })) {
        setIncidentPriorityForCondoFromDefaultWithoutOne(condoId, typeId);
      }
      else
        deleteIncidentPriorityForCondo(condoId, typeId);
      checkIfStillIncidentPriority(condoId)
    },
    ServerScriptSetOldContactToNewContactForCondo: function () {
      let condos = Condos.find().fetch();
      _.each(condos, function (condo) {
        let findElem = CondoContact.findOne({ condoId: condo._id });
        if (!findElem) {
          let condoEnterprise = Enterprises.findOne({ condos: condo._id });
          if (condoEnterprise) {
            let type = condo.settings.condoType;
            console.log(condo.info.address);
            let contactCondo = condo.contacts;
            let newDefaultContact = {
              condoId: condo._id,
              defaultContact: {
                declarationDetailId: DeclarationDetails.findOne({ "key": "defaultContact" })._id,
                userId: contactCondo.Defaut,
              },
              contactSet: [],
              contactReminder: [],
            };
            let contactSetFromOldContact = [];
            let messageTypeId = IncidentType.findOne({ defaultType: "billing" })._id;
            _.each(contactCondo["Facturation / Comptabilité"], function (elem, index) {

              let declarationDetailId = "";
              if (index != "Autre" && index != "Autre2" && index != "Trombi" && index != "validEntry")
                declarationDetailId = DeclarationDetails.findOne({ "value-fr": index })._id;
              else if (index == "Autre") {
                declarationDetailId = DeclarationDetails.findOne({ "key": "other_billing" })._id;
              }
              else if (index == "Autre2") {
                declarationDetailId = DeclarationDetails.findOne({ "key": "other" })._id;
              }
              else if (index == "Trombi") {
                declarationDetailId = DeclarationDetails.findOne({ "key": "managerWAccount" })._id;
              }
              else if (index == "validEntry") {
                declarationDetailId = DeclarationDetails.findOne({ "key": "validEntry" })._id;
              }

              if (elem == null)
                elem = [];

              contactSetFromOldContact.push({
                messageTypeId: messageTypeId,
                declarationDetailId: declarationDetailId,
                userIds: elem
              })
            });
            messageTypeId = IncidentType.findOne({ defaultType: "other" })._id;
            _.each(contactCondo["Autre"], function (elem, index) {
              let typeId = messageTypeId;
              let declarationDetailId = "";
              if (index != "Autre" && index != "Autre2" && index != "Trombi" && index != "validEntry")
                declarationDetailId = DeclarationDetails.findOne({ "value-fr": index })._id;
              else if (index == "Autre") {
                typeId = IncidentType.findOne({ defaultType: "billing" })._id;
                declarationDetailId = DeclarationDetails.findOne({ "key": "other_billing" })._id;
              }
              else if (index == "Autre2") {
                declarationDetailId = DeclarationDetails.findOne({ "key": "other" })._id;
              }
              else if (index == "Trombi") {
                typeId = null;
                declarationDetailId = DeclarationDetails.findOne({ "key": "managerWAccount" })._id;
              }
              else if (index == "validEntry") {
                typeId = null;
                declarationDetailId = DeclarationDetails.findOne({ "key": "validEntry" })._id;
              }
              if (index == "Incidents") {
                typeId = IncidentType.findOne({ defaultType: "incident" })._id;;
              }

              if (elem == null)
                elem = [];

              contactSetFromOldContact.push({
                messageTypeId: typeId,
                declarationDetailId: declarationDetailId,
                userIds: elem
              })
            });
            newDefaultContact.contactSet = contactSetFromOldContact;

            actualReminders = {
              'nonurgent': condoEnterprise.settings.incident_reminderTime || 24,
              'urgent': condoEnterprise.settings.incident_urgent_reminderTime || 48
            };

            let incidentPriorityUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "urgent" })._id;
            let incidentPriorityNonUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "no-urgent" })._id;

            newDefaultContact.contactReminder = [
              {
                priorityId: incidentPriorityUrgent,
                timeValue: actualReminders.urgent
              },
              {
                priorityId: incidentPriorityNonUrgent,
                timeValue: actualReminders.nonurgent
              }
            ]

            let defaultContactCase = ContactManagement.findOne({ condoId: "default", forCondoType: type });

            let allDeclarationIds = [];
            _.each(defaultContactCase.messengerSet, function (list) {
              allDeclarationIds = allDeclarationIds.concat(list.declarationDetailIds);
            });

            let allNotDropdownDeclarationIds = [];
            _.each(DeclarationDetails.find({ isDropdownValue: false }).fetch(), function (list) {
              allNotDropdownDeclarationIds.push(list._id);
            });

            usedDeclarationIds = [];

            newDefaultContact.contactSet = _.filter(newDefaultContact.contactSet, function (contact) {
              if (_.includes(allDeclarationIds, contact.declarationDetailId)) {
                usedDeclarationIds.push(contact.declarationDetailId);
                if (_.includes(allNotDropdownDeclarationIds, DeclarationDetails.findOne({ _id: contact.declarationDetailId })._id))
                  delete allNotDropdownDeclarationIds[allNotDropdownDeclarationIds.indexOf(DeclarationDetails.findOne({ _id: contact.declarationDetailId })._id)]
                return true;
              }
              else {
                if (DeclarationDetails.findOne({ _id: contact.declarationDetailId }).isDropdownValue == false) {
                  if (_.includes(allNotDropdownDeclarationIds, DeclarationDetails.findOne({ _id: contact.declarationDetailId })._id))
                    delete allNotDropdownDeclarationIds[allNotDropdownDeclarationIds.indexOf(DeclarationDetails.findOne({ _id: contact.declarationDetailId })._id)]
                  return true;
                }
                return false;
              }
            })

            let notPresentIds = _.filter(allDeclarationIds, function (elem) {
              if (!_.includes(usedDeclarationIds, elem))
                return true;
            });
            _.each(notPresentIds, function (elem) {
              let messageTypeId = null;
              _.each(defaultContactCase.messengerSet, function (list) {
                _.each(list.declarationDetailIds, function (declarationId) {
                  if (declarationId == elem)
                    messageTypeId = list.id;
                })
              });

              newDefaultContact.contactSet.push({
                messageTypeId: messageTypeId,
                declarationDetailId: elem,
                userIds: []
              });
            })
            _.each(allNotDropdownDeclarationIds, function (elem) {
              let messageTypeId = null;
              _.each(defaultContactCase.messengerSet, function (list) {
                _.each(list.declarationDetailIds, function (declarationId) {
                  if (declarationId == elem)
                    messageTypeId = list.id;
                })
              });

              if ((DeclarationDetails.findOne({ _id: elem }) || {key: ''}).key != "defaultContact") {
                newDefaultContact.contactSet.push({
                  messageTypeId: messageTypeId,
                  declarationDetailId: elem,
                  userIds: []
                });
              }
            })

            CondoContact.upsert({ condoId: condo._id }, newDefaultContact);
            // Condos.update({_id: condo._id}, {$unset: {contacts: true}});
          }
        }
        else {
          console.log("NOPE");
        }
      });
    },

    getServerScripts: function () {
      let ret = [];
      let method = _.clone(Meteor.server.method_handlers);
      let displayNameRE = /^ServerScript(.*)$/ig;
      _.each(method, function (value, key) {
        let reResult = displayNameRE.exec(key);
        if (!reResult)
          reResult = key.match(displayNameRE);
        if (reResult) {
          if (key.startsWith("ServerScript")) {
            ret.push({
              displayName: reResult[reResult.length - 1].replace(/([A-Z])/g, ' $1'),
              methodName: key
            });
          }
        }
      });
      return ret;
    },

    ServerScriptSendEmailGoblinUserNotConfirmConciergerie: function () {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let gobelins = Condos.findOne({ name: "38 à 40 av. des Gobelins 75013 Paris" });
      if (!gobelins)
        return false;
      let condoId = gobelins._id;
      let residents = Residents.find({ "pendings.condoId": condoId }, {
        fields: {
          userId: true
        }
      }).fetch();
      let userIds = _.map(residents, function (res) {
        return res.userId;
      })
      let users = Meteor.users.find({
        $and:
          [
            { _id: { $in: userIds } },
            { "services.password": { $exists: false } }
          ]
      }).fetch();
      _.each(users, function (user) {
        const lang = Meteor.users.findOne({ _id: user._id }).profile.lang || "fr"
        Meteor.call(
          'sendEmailSync',
          user.emails[0].address,
          condoId,
          "MonBuilding <> Allianz | Application de votre immeuble", /* NE PAS TRADUIRE !! */
          'goblin-user-not-confirm-conciergerie',
          {
            firstname: user.profile.firstname,
            url: Meteor.absoluteUrl(lang + '/services/set-password/' + user._id, condoId),
          }
        );
      });
      Meteor.call(
        'sendEmailSync',
        'monbuilding75@gmail.com',
        condoId,
        "MonBuilding <> Allianz | Application de votre immeuble", /* NE PAS TRADUIRE !! */
        'goblin-user-not-confirm-conciergerie',
        {
          firstname: 'Admin',
          url: Meteor.absoluteUrl('fr/services/set-password/', condoId),
        }
      );

    },
    ServerScriptSendEmailGoblinUserAllreadyConfirmConciergerie: function () {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let gobelins = Condos.findOne({ name: "38 à 40 av. des Gobelins 75013 Paris" });
      if (!gobelins)
        return false;
      let condoId = gobelins._id;
      let residents = Residents.find({ "condos.condoId": condoId }, {
        fields: {
          userId: true
        }
      }).fetch();
      let userIds = _.map(residents, function (res) {
        return res.userId;
      })
      console.log(userIds);
      let users = Meteor.users.find({
        $and:
          [
            { _id: { $in: userIds } },
            { "services.password": { $exists: true } }
          ]
      }).fetch();
      console.log(users);
      _.each(users, function (user) {
        const lang = Meteor.users.findOne({ _id: user._id }).profile.lang || "fr"
        Meteor.call(
          'sendEmailSync',
          user.emails[0].address,
          condoId,
          "Conciergerie du 38-40 avenue des Gobelins, 75013 Paris", /* NE PAS TRADUIRE !! */
          'goblin-user-allready-confirm-conciergerie',
          {
            firstname: user.profile.firstname,
            url: Meteor.absoluteUrl(lang + '/resident/' + condoId + '/concierge', condoId),
          }
        );
      });
      Meteor.call(
        'sendEmailSync',
        'monbuilding75@gmail.com',
        condoId,
        "Conciergerie du 38-40 avenue des Gobelins, 75013 Paris", /* NE PAS TRADUIRE !! */
        'goblin-user-allready-confirm-conciergerie',
        {
          firstname: "Admin",
          url: Meteor.absoluteUrl('fr/resident/' + condoId + '/concierge', condoId),
        }
      );

    },
    ServerScriptUpdateTranslationRight: function () {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      //  console.log(DefaultRoles.find({}).fetch());
      let migration = [
        {
          oldr: {
            module: 'map',
            right: 'see'
          },
          newr: {
            'value-fr': 'Voir la colonne « Plan »',
            'value-en': 'See the « Map » column'
          }
        },
        {
          oldr: {
            module: 'map',
            right: 'write'
          },
          newr: {
            'value-fr': 'Modifier le plan',
            'value-en': 'Edit the map'
          }
        },
        {
          oldr: {
            module: 'map',
            right: 'delete'
          },
          newr: {
            'value-fr': 'Effacer le plan',
            'value-en': 'Delete the map'
          }
        },
        {
          oldr: {
            module: 'setContact',
            right: 'seeWhosWho'
          },
          newr: {
            'value-fr': 'Voir la sous rubrique « Trombinoscope »',
            'value-en': 'See the « Building Directory » sub-section'
          }
        },
        {
          oldr: {
            module: 'trombi',
            right: 'sendMessage'
          },
          newr: {
            'value-fr': 'Envoyer message depuis le trombinoscope',
            'value-en': 'Send a message from the building directory'
          }
        },
        {
          oldr: {
            module: 'setContact',
            right: 'writeWhosWho'
          },
          newr: {
            'value-fr': 'Modifier les contacts de la sous rubrique « Trombinoscope »',
            'value-en': 'Edit contacts in the « Building Directory » sub-section'
          }
        },
        {
          oldr: {
            module: 'setContact',
            right: 'seeValidation'
          },
          newr: {
            'value-fr': 'Voir la sous rubrique « Validation entrées & réservations »',
            'value-en': 'See the « Entries and bookings validation » sub-section'
          }
        },
        {
          oldr: {
            module: 'view',
            right: 'stats'
          },
          newr: {
            'value-fr': 'Voir la rubrique « Statistiques »',
            'value-en': 'See the « Statistics » section'
          }
        },
        {
          board: 'manager',
          oldr: {
            module: 'actuality',
            right: 'see'
          },
          newr: {
            'value-fr': 'Voir la rubrique « Informations & Evénements »',
            'value-en': 'See the « Informations & Events » section'
          }
        },
        {
          board: 'occupant',
          oldr: {
            module: 'actuality',
            right: 'see'
          },
          newr: {
            'value-fr': 'Voir la rubrique « Informations »',
            'value-en': 'See the « Informations » section'
          }
        },
        {
          oldr: {
            module: 'trombi',
            right: 'see'
          },
          newr: {
            'value-fr': 'Voir la rubrique « Trombinoscope »',
            'value-en': 'See the « Building Directory » section'
          }
        }
      ];
      DefaultRoles.find({}).map((role) => {
        migration.forEach(rm => {
          let t = role.rights.findIndex((el) => {
            if (rm.board && rm.board !== role.for) {
              return false
            }
            return (el.module === rm.oldr.module && el.right === rm.oldr.right)
          });
          if (t !== -1 && role.rights[t]) {
            const r_vf = 'rights.' + t + '.value-fr'
            const r_ve = 'rights.' + t + '.value-en'
            role.rights[t]['value-fr'] = rm.newr['value-fr']
            role.rights[t]['value-en'] = rm.newr['value-en']
            DefaultRoles.update({ _id: role._id }, role)
          }
        })
      })
    },

    formatFilesIdInDBScript: function () {
      let users = Meteor.users.find({
        "profile.avatarId": {
          $exists: true,
          $ne: ""
        },
      }, {
          fields: {
            "profile.avatarId": true
          }
        }).fetch();

      _.each(users, function (user) {
        Residents.update({
          userId: user._id
        }, {
            $set: {
              fileId: user.profile.avatarId
            }
          });
      });
    },

    setOldConvToNewConv: function () {
      let conversation = Messages.find().fetch();
      _.each(conversation, function (conv) {
        let convId = conv._id;
        let thisFeed = conv.feed;
        let allUsers = [];
        let allUserId = []
        if (thisFeed && thisFeed.length > 0) {
          _.each(thisFeed, function (eachMessage) {
            let newFeed = {
              "messageId": convId,
              "date": eachMessage.date,
              "userId": eachMessage.userId,
              "text": eachMessage.text,
              "files": eachMessage.files
            };
            MessagesFeed.insert(newFeed);
            if (!_.includes(allUserId, eachMessage.userId)) {
              allUserId.push(eachMessage.userId);
              if (conv.origin == eachMessage.userId)
                allUsers.push({ userId: eachMessage.userId, createdBy: true, lastVisit: Date.now() });
              else
                allUsers.push({ userId: eachMessage.userId });
            }
          });
          if (conv.users.length == 0)
            Messages.update({ _id: convId }, { $unset: { feed: "" }, $set: { users: allUsers } });
          else
            Messages.update({ _id: convId }, { $unset: { feed: "" } });
        }
        let lastMsgFeed = MessagesFeed.findOne({ messageId: convId }, { sort: { date: -1 } });
        if (lastMsgFeed) {
          Messages.update({ _id: convId }, { $unset: { lastMsgUser: "" } })
          Messages.update({ _id: convId }, { $set: { lastMsg: lastMsgFeed.date, lastMsgString: lastMsgFeed.text, lastMsgUserId: lastMsgFeed.userId } });
        }
      })
    },

    sendSurvey: function (userid) {
      let archive = Archives.findOne({ userId: userid });
      Meteor.call(
        'sendEmailSync',
        archive.email,
        "Les Estudines Paris Rosny: ton avis nous intéresse !",
        'estudines-satisfaction-fr',
        {
          firstname: archive.firstname,
          url: Meteor.absoluteUrl('fr/satisfaction/' + archive._id, null, userid),
        }
      );

      let remindDate = new Date();
      remindDate.setDate(remindDate.getDate() + 2);
      Meteor.call(
        'scheduleAnEmail',
        {
          "templateName": 'estudines-satisfaction-fr',
          "to": [archive.email],
          "condoId": null,
          "subject": "Les Estudines Paris Rosny: ton avis nous intéresse !",
          "data": {
            firstname: archive.firstname,
            url: Meteor.absoluteUrl('fr/satisfaction/' + archive._id, null, userid),
          },
          "dueTime": remindDate
        }
      );
    },

    SendEmailRosny: function () {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      let rosny = Condos.findOne({ name: "Les Estudines Paris Rosny" });
      if (!rosny)
        return false;
      let condoId = rosny._id;
      let residents = Residents.find({ "pendings.condoId": condoId }, {
        fields: {
          userId: true
        }
      }).fetch();
      let userIds = _.map(residents, function (res) {
        return res.userId;
      })
      let users = Meteor.users.find({
        $and:
          [
            { _id: { $in: userIds } },
            { "services.password.bcrypt": { $exists: false } }
          ]
      }).fetch();
      _.each(users, function (user) {
        const lang = Meteor.users.findOne({ _id: user._id }).profile.lang || "fr"
        Meteor.call(
          'sendEmailSync',
          user.emails[0].address,
          condoId,
          "Tu n'as pas encore validé ton compte, voilà ce que tu rates", // NE PAS TRADUIRE !!
          'user-no-password',
          {
            firstname: user.profile.firstname,
            url: Meteor.absoluteUrl(lang + 'services/set-password/' + user._id, condoId),
          }
        );
      })
      Meteor.call(
        'sendEmailSync',
        'monbuilding75@gmail.com',
        condoId,
        `[Emails envoyés: ${users.length}] Tu n'as pas encore validé ton compte, voilà ce que tu rates`, // NE PAS TRADUIRE !!
        'user-no-password',
        {
          firstname: 'Admin',
          url: Meteor.absoluteUrl(user.profile.lang || "fr" + '/services/set-password/' + user._id, condoId),
        }
      );
      return users.length || 0
    },

    ServerScriptSendEmailGobelins38: function () {
      if (!Meteor.call("isAdmin", this.userId))
        throw new Meteor.Error(300, "Not Authenticated");
      console.log("Starting \"Gobelins\" scripts...")
      let gobelins = Condos.findOne({ name: "38 à 40 av. des Gobelins 75013 Paris" });
      if (!gobelins)
        return false;
      let condoId = gobelins._id;
      let residents = Residents.find({ "pendings.condoId": condoId }, {
        fields: {
          userId: true
        }
      }).fetch();
      let userIds = _.map(residents, function (res) {
        return res.userId;
      })
      let users = Meteor.users.find({
        $and:
          [
            { _id: { $in: userIds } },
            { "services.password": { $exists: false } }
          ]
      }).fetch();
      _.each(users, function (user) {
        console.log("Sending email \"Gobelins\" to : " + user.emails[0].address);
        const lang = Meteor.users.findOne({ _id: user._id }).profile.lang || "fr"
        Meteor.call(
          'sendEmailSync',
          user.emails[0].address,
          condoId,
          "Vous avez été invité(e) à rejoindre l'espace de l'immeuble", // NE PAS TRADUIRE !!
          'invite-new-user-to-condo-fr',
          {
            firstname: user.profile.firstname,
            setPasswordLink: Meteor.absoluteUrl(lang + '/services/set-password/' + user._id, condoId),
            gestionnaireName: "Allianz Real Estate",
            condoName: gobelins.name,
          }
        );
      })

      Meteor.call(
        'sendEmailSync',
        'monbuilding75@gmail.com',
        condoId,
        "Vous avez été invité(e) à rejoindre l'espace de l'immeuble", // NE PAS TRADUIRE !!
        'invite-new-user-to-condo-fr',
        {
          firstname: "Admin",
          setPasswordLink: Meteor.absoluteUrl('fr/services/set-password/' + 'admin', condoId),
          gestionnaireName: "Allianz Real Estate",
          condoName: gobelins.name,
        }
      );
      console.log("End of \"Gobelins\" script");
    },

    removeConcierge: function (conciergeId, type) {
      if (type == "archive") {
        ConciergeMessage.update({ _id: conciergeId }, { $set: { isArchive: true } });
        return true;
      }
      else if (type == "delete") {
        ConciergeMessage.remove({ _id: conciergeId });
        return true;
      }

    },

    updateBoardManagerForAdmin: function (enterpriseId, condosIds) {
      if (Meteor.call('isAdmin', this.userId)) {
        let gestionnaire = Enterprises.findOne(enterpriseId);

        let thisUserId = this.userId;

        var alreadyHere = false;
        var users = gestionnaire.users;
        _.each(users, function (user, index) {
          if (user.userId != thisUserId)
            users[index].isAdmin = false;
          else
            alreadyHere = true;
        })
        Enterprises.update({ "_id": gestionnaire._id }, { $set: { users: users } }, { multi: true });
        gestionnaire = Enterprises.findOne(enterpriseId);

        let adminUser = new SwitchBoard().adminUserTemplate;

        adminUser.userId = thisUserId;

        if (alreadyHere == false) {
          Enterprises.update({ "_id": gestionnaire._id }, { $push: { users: adminUser } }, { multi: true }, function (error) {
            console.log(error);
          });
        }

        var condosInCharge = [];
        _.each(condosIds, function (id) {
          let condoInCharge = new SwitchBoard().condoInChargeTemplate;
          condoInCharge.condoId = id;
          condosInCharge.push(condoInCharge);
        });
        gestionnaire = Enterprises.findOne(enterpriseId);
        _.each(gestionnaire.users, function (user, index) {
          if (user.userId == thisUserId && user.isAdmin == true)
            gestionnaire.users[index].condosInCharge = condosInCharge;
        })

        Enterprises.update({ _id: enterpriseId }, { $set: { users: gestionnaire.users } }, { multi: true });

        gestionnaire = Enterprises.findOne(enterpriseId);

        Meteor.users.update(this.userId, { $set: { "identities.gestionnaireId": gestionnaire._id } });
      }
    },

    updateBoardOccupantForAdmin: function (condosIds, type) {
      if (Meteor.call('isAdmin', this.userId)) {
        const thisUserId = this.userId;
        let resident = Residents.findOne({ "userId": thisUserId });
        if (resident) {
          var newResident = new SwitchBoard().residentTemplate;
          _.each(condosIds, function (elem, index) {
            let condo = new SwitchBoard().condoResidentTemplate;
            condo.condoId = elem;
            let thisCondo = Condos.findOne(elem);
            if (type == "keeper")
              condo.isKeeper = true;
            else if (type == "cs" && thisCondo.settings.condoType == "copro")
              condo.isSyndical = true;
            if (thisCondo.settings.condoType == "copro" && condo.isKeeper == false)
              condo.buildings[0].type = "owner";
            newResident.condos.push(condo);
          })
          Residents.update({ "userId": thisUserId }, { $set: { condos: newResident.condos } });
          return true;

        }
        else {
          var newResident = new SwitchBoard().residentTemplate;
          newResident.userId = thisUserId;
          _.each(condosIds, function (elem, index) {
            let condo = new SwitchBoard().condoResidentTemplate;
            condo.condoId = elem;
            let thisCondo = Condos.findOne(elem);
            if (type == "keeper")
              condo.isKeeper = true;
            else if (type == "cs")
              condo.isSyndical = true;
            if (thisCondo.settings.condoType == "copro")
              condo.buildings[0].type = "owner";
            newResident.condos.push(condo);
          })
          let residentId = Residents.insert(newResident);
          Meteor.users.update(thisUserId, { $set: { "identities.residentId": residentId } });
          return true;
        }
      }
    },

    updateAdmin: function (adminInfos) {
      if (Meteor.call("isAdmin", this.userId)) {
        if (adminInfos.email && adminInfos.email != undefined && adminInfos.email != "") {
          const oldEmail = Meteor.users.findOne(this.userId).emails[0].address;
          if (oldEmail != adminInfos.email) {
            Accounts.addEmail(this.userId, adminInfos.email);
            Accounts.removeEmail(this.userId, oldEmail);
          }
        }

        Meteor.users.update({ _id: this.userId }, {
          $set: {
            "profile.firstname": adminInfos.firstname,
            "profile.lastname": adminInfos.lastname,
          }
        });
        return true;
      }
      else
        throw new Meteor.Error(404, "Invalid user", "Not logged as admin");
    },

    changeCondoFromBackoffice: function (userId, oldCondoId, newCondoId) {
      if (Meteor.call("isAdmin", this.userId)) {
        const user = Meteor.users.findOne(userId)
        if (!user) return
        const condosUser = Residents.findOne({ "userId": userId }).condos;
        const pendingsUser = Residents.findOne({ "userId": userId }).pendings;

        let newCondosUser = condosUser;
        let newPendingsUser = pendingsUser;

        let defaultBuilding = [
          {
            "buildingId": "",
            "type": "tenant"
          }
        ]
        _.each(condosUser, function (condo, index) {
          if (condo.condoId == oldCondoId) {
            newCondosUser[index].condoId = newCondoId;
            newCondosUser[index].buildings = defaultBuilding;
          }
        });

        Residents.update({ "userId": userId }, { $set: { condos: newCondosUser } });
        _.each(pendingsUser, function (condo, index) {
          if (condo.condoId == oldCondoId) {
            newPendingsUser[index].condoId = newCondoId;
            newPendingsUser[index].buildings = defaultBuilding;
          }
        });
        Residents.update({ "userId": userId }, { $set: { pendings: newPendingsUser } });
        sendNotifRemoveCondo(userId, oldCondoId);
        sendCondoInvitationEmailToExistingUser(user.emails[0].address, newCondoId, user.profile.lang)
        sendNotifForNewCondo(userId, newCondoId);
      }
      else
        throw new Meteor.Error(404, "Invalid user", "Not logged as admin");
    },


    setNewParamsToGestionnaire: function () {
      let gestionnaires = Enterprises.find().fetch();
      let thisUserId = this.userId;
      var alreadyHere = false;
      _.each(gestionnaires, function (gest) {
        var users = gest.users;
        _.each(users, function (user, index) {
          if (user.userId != thisUserId)
            users[index].isAdmin = false;
          else
            alreadyHere = true;
        })
        Enterprises.update({ "_id": gest._id }, { $set: { users: users } }, { multi: true });

        let adminUser = {
          "userId": thisUserId,
          "firstname": "Admin",
          "lastname": "ADMIN",
          "tel": "",
          "level": 5,
          "type": "directionSiege",
          "condosInCharge": [
          ],
          "profile": {
          },
          "isAdmin": true
        }
        if (alreadyHere == false)
          Enterprises.update({ "_id": gest._id }, { $push: { users: adminUser } }, { multi: true });
      })
    },

    /*
      @summary change the access (0: see, 1: edit, 2: delete) of the condo (newRightsInCondo.condo) of the user (newRightsInCondo.user)
      @params newRightsInCondo {type: 0 || 1 || 2, condo: id, user: id}
      @return Do nothing if the connected user is not an admin
      */
    saveAccessEdit: function (newRightsInCondo) {
      if (Meteor.call('isAdmin', this.userId)) {
        const user = Meteor.users.findOne({ _id: newRightsInCondo.user });
        let condoInCharge = _.filter(_.map(Enterprises.findOne(user.identities.gestionnaireId), function (value, key) {
          if (key === "users")
            return value;
        }), function (elem) {
          return elem !== undefined;
        })[0];
        condoInCharge = _.find(condoInCharge, function (elem) {
          return elem.userId === newRightsInCondo.user;
        }).condosInCharge;
        _.each(condoInCharge, function (elem) {
          if (elem.condoId === newRightsInCondo.condo) {
            if (_.find(elem.access, function (type) {
              return type === newRightsInCondo.type;
            }) !== undefined) {
              elem.access = _.reject(elem.access, function (elem) {
                return elem === newRightsInCondo.type;
              });
            }
            else {
              if (!newRightsInCondo.type) {
                elem.access = [0];
              }
              else {
                elem.access = _.reject(elem.access, function (elem) {
                  return !elem;
                });
                elem.access[elem.access.length] = newRightsInCondo.type;
              }
            }
          }
        });
        Enterprises.update({
          _id: user.identities.gestionnaireId,
          'users.userId': user._id,
        },
          {
            $set: { 'users.$.condosInCharge': condoInCharge },
          });
      }
    },

    /*
    @summary function to add managers to an enterprise. You can add many managers in a row.
    @params managers array Object (Enterprises.findOne().users[i]), emails: array String, gestionnaireId: id, oldFiles: files array
    @return Do nothing if the connected user is not an admin
    */
    addManager: function (managers, emails, gestionnaireId, oldFiles) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        _.each(emails, function (elem, idx) { // for each manager in the array managers passed in parameter
          Accounts.createUser({ email: elem });
          const user = Accounts.findUserByEmail(elem);
          let civilite = managers[idx].civilite;
          delete managers[idx].civilite;
          managers[idx].userId = user._id;
          managers[idx].createdAt = Date.now();
          Enterprises.update(gestionnaireId, { // add the manager account to the enterprise (gestionnaireId)
            $push: { users: managers[idx] },
          });
          Meteor.users.update({ // update Meteor.users collection
            _id: user._id,
          }, {
              $set: {
                profile: { civilite: civilite, lastname: managers[idx].lastname, firstname: managers[idx].firstname, tel: managers[idx].tel, telCode: managers[idx].telCode, telCodeCountry: managers[idx].telCodeCountry, tel2: managers[idx].tel2, role: "Gestionnaire", lang: "fr" },
                'identities.gestionnaireId': gestionnaireId,
              },
            });
          const lang = Meteor.users.findOne(user._id).profile.lang || "fr"
          var translation = new Email(lang); // send email manager account creation
          Meteor.call('sendEmailSync',
            emails[idx],
            null,
            translation.email["invit_join"],
            'invite-new-manager-to-enterprise',
            {
              enterpriseName: Enterprises.findOne(gestionnaireId).name,
              gestionnaireName: Meteor.users.findOne(user),
              setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, null, null, gestionnaireId),
            });
        });
        _.each(oldFiles, function (file) {
          UserFiles.remove(file); // to avoid files leaks
        });
      }
    },

    removeCondoContactReferences: function (userId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        let toRemove = [];
        let condoContact = CondoContact.find().fetch();
        _.each(condoContact, function (elem) {
          _.each(elem.contactSet, function (contacts) {
            if (_.includes(contacts.userIds, userId)) {
              delete contacts.userIds[contacts.userIds.indexOf(userId)];
              contacts.userIds = _.without(contacts.userIds, null, undefined);
              toRemove.push({
                condoContactId: elem._id,
                declarationDetailId: contacts.declarationDetailId,
                newVal: contacts.userIds
              });
            }
          })
        })
        _.each(toRemove, function (element) {
          CondoContact.update({
            _id: element.condoContactId,
            'contactSet.declarationDetailId': element.declarationDetailId
          },
            {
              $set: {
                'contactSet.$': {
                  declarationDetailId: element.declarationDetailId,
                  userIds: element.newVal
                }
              }
            }
          );
        })
      }
    },
    /*
    @summary function to remove a manager (userId, _id Meteor.users) of a specific enterprise (gestionnaireId)
    @params gestionnaireId: id, userId: id
    @return Do nothing if the connected user is not an admin
    */
    removeManagerEnterprise: function (gestionnaireId, userId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        const thisUser = Meteor.users.findOne(userId)
        const lang = thisUser.profile.lang || "fr"
        var translation = new Email(lang);
        const gestionnaire = Enterprises.findOne(gestionnaireId);
        if (gestionnaire) {
          let findResult = _.find(gestionnaire.users, function (user) { // remove profile picture of the removed manager
            return user.userId == userId;
          })
          if (findResult) {
            UserFiles.remove(findResult.profile.fileId);
          }
        }
        Enterprises.update(gestionnaireId, { // update Gestionnaire collection
          $pull: {
            'users': { userId: userId },
          }
        });

        if (!Enterprises.findOne({ 'users.userId': userId })) {
          Meteor.users.remove({ _id: userId })
        }

        Meteor.call('sendEmailSync',
          thisUser.emails[0].address, // send email
          null,
          translation.email['desactivation_account'],
          "remove-access-" + lang,
          {
            userId: userId,
            condoId: undefined,
            formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, null, null, gestionnaireId)
          });

        Meteor.call('addToGestionnaireHistory',
          'managerList',
          gestionnaireId,
          'deletedManager',
          null,
          `${thisUser.profile.firstname} ${thisUser.profile.lastname}`,
          thisUser._id
        )
      }
    },

    /*
    @summary function to remove a specific condo (condoId) of a specific manager (userId) of a specific enterprise (gestionnaireId)
    @params gestionnaireId: id, userId: id, condoId: id
    @return Do nothing if the connected user is not an admin
    */
    removeCondoInCharge: function (gestionnaireId, userId, condoId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        let condo = Condos.findOne({ _id: condoId })
        if (!condo) return
        let condoInCharge = Enterprises.findOne(gestionnaireId);
        condoInCharge = _.find(condoInCharge.users, function (elem) {
          return elem.userId == userId;
        });
        condoInCharge = _.filter(condoInCharge.condosInCharge, function (elem) { // remove condo (condoId)
          return elem.condoId != condoId;
        });
        Enterprises.update({ _id: gestionnaireId, 'users.userId': userId }, { // update Gestionnaire collection
          $set: {
            'users.$.condosInCharge': condoInCharge,
          }
        });
        sendNotifRemoveCondo(userId, condoId);
        let user = Meteor.users.findOne(userId)
        let lang = user.profile.lang || 'fr'
        let translation = new Email(lang)
        Meteor.call('sendEmailSync',
          user.emails[0].address,
          condoId,
          translation.email['delete_condo'] + condo.name,
          "remove-access-condo-" + lang,
          {
            userId: userId,
            condoId: condoId,
            building: condo.name,
            formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, condoId)
          }
        );
        Meteor.removeUserRights(userId, condoId);
      }
    },

    /*
      @summary add a condo in charge (condoInCharge) to a manager (userId)
      @params gestionnaireId: id, userId: id, condoInCharge Object {
                                    access: Array,
                                    condoId: id,
                                    notifications: Object {
                                        actualites: bool,
                                        classifieds: bool,
                                        edl: bool,
                                        ...
                                      }
                                    }
      @return Do nothing if the connected user is not an admin
      */
    addCondoInCharge: function (gestionnaireId, userId, condoInCharge, defaultRoleId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        const user = Meteor.users.findOne({ _id: userId })
        condoInCharge.joined = Date.now();
        if (!user) return
        Enterprises.update({ _id: gestionnaireId, 'users.userId': userId }, { // update Gestionnaire collection
          $push: {
            'users.$.condosInCharge': condoInCharge,
          }
        });
        Enterprises.update({ _id: gestionnaireId }, {
          $set: { inactive: false }
        })
        if (defaultRoleId == undefined || defaultRoleId == null)
          defaultRoleId = DefaultRoles.findOne({ name: "Asset Manager", for: "manager" })._id;
        sendCondoInvitationEmailToExistingUser(user.emails[0].address, condoInCharge.condoId, user.profile.lang)
        sendNotifForNewCondo(userId, condoInCharge.condoId);
        Meteor.createDefaultRightsForUserWithCondo(userId, condoInCharge.condoId, defaultRoleId);
      }
    },

    /*
    @summary function to add an empty enterprise (without manager & condos)
    @params enterprise Object (Enterprises.findOne() with empty users array and empty condos array), oldFiles files array
    @return Do nothing if the connected user is not an admin
    */
    addEnterprise: function (enterprise, oldFiles) {
      if (Meteor.call('isAdmin', this.userId)) {
        Enterprises.insert(enterprise); // add the new empty enterprise
        _.each(oldFiles, function (file) {
          UserFiles.remove(file); // avoid files leaks
        });
      }
    },

    /*
    @summary function to edit an enterprise with the informations of the enterprise passed in parameter
    @params entreprise Object (Enterprises.findOne() with empty users & condos array), oldFiles files array, gestionnaireId : id
    @return Do nothing if the connected user is not an admin
    */
    changeLogoEnterprise: function (fileSet, gestionnaireId) {
      if (Meteor.call('isAdmin', this.userId)) {
        if (fileSet.isMobile == true) {
          let logo = Enterprises.findOne(gestionnaireId).logo;
          delete fileSet['isMobile'];
          if (logo && logo.fileMobileId)
            UserFiles.remove(logo.fileMobileId);
          if (logo && logo.fileId)
            fileSet.fileId = logo.fileId;
          Enterprises.update(gestionnaireId, { // update Gestionnaire collection
            $set: {
              logo: fileSet,
            }
          });
        }
        else {
          let logo = Enterprises.findOne(gestionnaireId).logo;
          delete fileSet['isMobile'];
          if (logo && logo.fileId)
            UserFiles.remove(logo.fileId);
          if (logo && logo.fileMobileId)
            fileSet.fileMobileId = logo.fileMobileId;
          Enterprises.update(gestionnaireId, { // update Gestionnaire collection
            $set: {
              logo: fileSet,
            }
          });
        }
      }
    },

    /*
    @summary function to edit an enterprise with the informations of the enterprise passed in parameter
    @params entreprise Object (Enterprises.findOne() with empty users & condos array), oldFiles files array, gestionnaireId : id
    @return Do nothing if the connected user is not an admin
    */
    editEnterprise: function (entreprise, oldFiles, gestionnaireId) {
      if (Meteor.call('isAdmin', this.userId)) {
        Enterprises.update(gestionnaireId, { // update Gestionnaire collection
          $set: {
            info: entreprise.info,
            name: entreprise.name
          }
        });
      }
    },

    /*
    @summary disable/enable all the manager account of the enterprise (enterpriseId) passed in parameter
    @params enterpriseId: id
    @return Do nothing if the connected user is not an admin
    */
    disableEnterprise: function (enterpriseId) {
      if (Meteor.call('isAdmin', this.userId)) {
        let gestionnaire = Enterprises.findOne(enterpriseId);
        if (gestionnaire) {
          if (!gestionnaire.inactive) {
            Enterprises.update(enterpriseId, {
              $set: {
                inactive: true,
              }
            });
          }
          else {
            Enterprises.update(enterpriseId, {
              $set: {
                inactive: false,
              }
            });
          }
        }
      }
    },

    /*
    @summary remove the enterprise (enterpriseId) passed in parameter, remove all the linked manager account too
    @params enterpriseId : id
    @return Do nothing if the connected user is not an admin
    */
    removeEnterprise: function (entityId) {
      if (Meteor.call('isAdmin', this.userId)) {
        const entity = Enterprises.findOne(entityId);
        if (entity) {
          _.each(entity.users, function (elem) {
            const user = Meteor.users.findOne(elem.userId)
            if (!user.identities.adminId) {
              const lang = user.profile.lang || "fr"
              var translation = new Email(lang);
              Meteor.call('sendEmailSync', user.emails[0].address, // send email to each manager
                translation.email['desactivation_account'],
                "remove-access-" + lang,
                {
                  userId: elem.userId,
                  condoId: undefined,
                  formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, null, null, entityId)
                });
              if (elem.profile.fileId) {
                UserFiles.remove({ _id: elem.profile.fileId }) // remove all the linked files
              }
              Meteor.users.remove({ _id: elem.userId }) // remove manager
            }
          })
          if (entity.logo.fileId)
            UserFiles.remove({ _id: entity.logo.fileId }) // remove the enterprise logo
          Enterprises.remove({ _id: entityId }) // remove the enterprise
        }
      }
    },

    /*
    @summary remove a specific condo (condoId) in the enterprise (gestionnaireId)
    @params gestionnaireId: id, condoId: id
    @return Do nothing if the connected user is not an admin
    */
    removeCondoEnterprise: function (gestionnaireId, condoId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Enterprises.update(gestionnaireId, {
          $pull: {
            condos: condoId,
          }
        });
      }
    },

    /*
    @summary function to display the enterprise's logo instead of the MonBuilding logo
    @params enterpriseId: id, value: bool
    @return Do nothing if the connected user is not an admin
    */
    setGrayMarkStatus: function (enterpriseId, value) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Enterprises.update(enterpriseId, { // update Gestionnaire collection
          $set: {
            'logo.showLogo': value, // display the logo: true || false
            'logo.showMonBuilding': false, // always false
          }
        });
      }
    },

    /*
    @summary function to display the MonBuilding logo instead of the enterprise logo or if the enterprise didn't enter any logo
    @params enterpriseId: id, value: bool
    @return Do nothing if the connected user is not an admin
    */
    setLogoMonBuildingStatus: function (enterpriseId, value) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Enterprises.update(enterpriseId, { // update Gestionnaire collection
          $set: {
            'logo.showLogo': false, // always false
            'logo.showMonBuilding': value, // display the MonBuilding logo : true || false
          }
        });
      }
    },

    /*
    @summary function to allow or not the validation of user registration according to the condo type (mono, copro, office, etudiant)
    @params gestionnaireId: id, option: String (copro, mono, etudiante, office), value: bool
    @return Do nothing if the connected user is not an admin
    */
    changeValidUsers: function (gestionnaireId, option, value) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        let gestionnaire = Enterprises.findOne(gestionnaireId);
        gestionnaire = gestionnaire.settings.validUsers;
        gestionnaire[option] = value;
        Enterprises.update(gestionnaireId, {
          $set: {
            'settings.validUsers': gestionnaire,
          }
        });
      }
    },


    /*
    @summary change the option (data) of a condo (condoId)
    @params condoId: id, data {field: value}
    @return Do nothing if the connected user is not an admin
    */
    saveCondoOptions: function (condoId, data) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        let options = Condos.findOne(condoId);
        options = options.settings.options; // retrieve options of the condo (condoId)
        options[Object.keys(data)[0]] = Object.values(data)[0]; // change only the value of the option in the data passed in parameter
        if ((Object.keys(data)[0] === 'reservations') && !Object.values(data)[0]) {
          options['reservations'] = false;
          options['EDL'] = false;
        }
        else if ((Object.keys(data)[0] === 'EDL' || Object.keys(data)[0] === 'reservations'))
          options['reservations'] = true;
        else if ((Object.keys(data)[0] === 'trombiKeeper' || Object.keys(data)[0] === 'trombiCS'))
          options['trombiAll'] = false;
        else if (Object.keys(data)[0] === 'trombiAll') {
          options['trombiKeeper'] = false;
          options['trombiCS'] = false;
        }
        options['messenger'] = options['messengerResident'] || options['messengerGestionnaire'] || options['messengerGardien'] || options["messengerSyndic"];
        if (options["trombiAll"] == false && options['trombiKeeper'] == false && options['trombiCS'] == false)
          options["trombi"] = false;
        else
          options["trombi"] = true;

        const existing = MarketServiceTypes.findOne({ condoId }, { $group: { _id: "$condoId" } })
        if (!existing) {
          const services = [
            { key: 'housekeeping' },
            { key: 'laundry' },
            { key: 'ironing' },
            { key: 'food' },
            { key: 'saleofgoods' },
            { key: 'offerofservices' }
          ]
          services.forEach(service => {
            MarketServiceTypes.insert({
              condoId,
              ...service
            })
          })
        }

        Condos.update({ _id: condoId }, // update options into the condo (condoId)
          { $set: { 'settings.options': options } });
        if (_.has(data, 'EDL') && data.EDL === true && Resources.findOne({ condoId: condoId, type: "Etat des lieux" }) === undefined) {
          let horaires = [
            {
              "dow": [0],
              "start": "00:00",
              "end": "00:01",
              "open": false,
              "full": false
            },
            {
              "dow": [1],
              "start": "08:00",
              "end": "20:00",
              "open": true,
              "full": false
            },
            {
              "dow": [2],
              "start": "08:00",
              "end": "20:00",
              "open": true,
              "full": false
            },
            {
              "dow": [3],
              "start": "08:00",
              "end": "12:00",
              "open": true,
              "full": false
            },
            {
              "dow": [4],
              "start": "08:00",
              "end": "20:00",
              "open": true,
              "full": false
            },
            {
              "dow": [5],
              "start": "08:00",
              "end": "20:00",
              "open": true,
              "full": false
            },
            {
              "dow": [6],
              "start": "00:00",
              "end": "00:01",
              "open": false,
              "full": false
            }
          ];
          Resources.insert({ name: "Etat des lieux", type: "Etat des lieux", etage: "-", condoId: condoId, events: [], horaires: horaires, edlDuration: 45 });
        }
        else
          Resources.remove({ condoId: condoId, type: "Etat des lieux" });
      }
    },

    /*
    @summary change the type (mono, copro, etudiante, office) of the condo (condoId)
    @params newType: String (copro, etudiante, mono, office), condoId: id
    @return Do nothing if the connected user is not an admin
    */
    changeCondoTypeOption: function (newType, condoId) {
      if (Meteor.call('isAdmin', this.userId)) {
        Condos.update({ _id: condoId }, {
          $set: {
            'settings.condoType': newType,
            'settings.options.couronne': newType === 'copro',
            'settings.options.forumSyndic': newType === 'copro',
            'settings.options.messengerSyndic': newType === 'copro'
          }
        });
      }
    },

    /*
    @summary function to remove a condo
    @params condoId: id
    @return Do nothing if the connected user is not an admin
    */
    removeCondo: function (condoId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        condo = Condos.findOne(condoId);
        let enterprise = Enterprises.find({ condos: condoId })
        let enterpriseId = null
        enterprise.forEach(enterprise => {
          enterpriseId = enterprise._id
          for (const user of enterprise.users) {
            Enterprises.update({ 'condos': condoId, 'users.userId': user.userId }, { $pull: { 'users.$.condosInCharge': { 'condoId': condoId } } })
          }
        })
        Enterprises.update({ condos: condoId }, { // update Gestionnaire collection
          $pull: {
            condos: condoId,
          }
        }, { multi: true });
        Residents.update({ 'condos.condoId': condoId }, { $pull: { 'condos': { 'condoId': condoId } } }, { multi: true })
        Residents.update({ 'pendings.condoId': condoId }, { $pull: { 'pendings': { 'condoId': condoId } } }, { multi: true })
        Residents.remove({
          $and: [
            { 'condos.0': { $exists: false } },
            { 'pendings.0': { $exists: false } }
          ]
        })
        UsersRights.remove({ condoId: condoId })
        CondoRole.remove({ condoId: condoId })

        ActuPosts.remove({ condoId: condoId })
        Analytics.remove({ condoId: condoId })
        ClassifiedsAds.remove({ condoId: condoId })
        // CondoDocuments.remove({ 'meta.condoId': condoId })
        EDL.remove({ condoId: condoId })
        EmailReminder.remove({ condoId: condoId })
        if (!!EnterpriseHistoryTab[enterpriseId]) {
          EnterpriseHistoryTab[enterpriseId].remove({ condoId: condoId })
        }

        ClassifiedsFeed.remove({ condoId: condoId })

        ForumCommentPosts.remove({ condoId: condoId })
        ForumPosts.remove({ condoId: condoId })
        Reservations.remove()
        Resources.remove({ condoId: condoId })
        ViewOfReservation.remove({ condoId: condoId })
        CondosModulesOptions.remove({ condoId: condoId })
        GestionnaireUnregistered.remove({ condoId: condoId })
        Messages.remove({ condoId: condoId })
        MessagesFeed.remove({ condoId: condoId })
        UnFollowForumPosts.remove({ condoId: condoId })
        ViewCondoDocument.remove({ condoId: condoId })

        Incidents.remove({ condoId: condoId })
        IncidentType.remove({ condoId: condoId })
        IncidentArea.remove({ condoId: condoId })
        IncidentDetails.remove({ condoId: condoId })
        IncidentPriority.remove({ condoId: condoId })

        CondoContact.remove({ condoId: condoId });
        ContactManagement.remove({ condoId: condoId });

        Condos.remove({ _id: condoId });
        let buildings = Buildings.find().forEach(building => {
          let testBuilding = Condos.findOne({ "buildings": building._id });
          if (!testBuilding || testBuilding == undefined) {
            Buildings.remove(building._id);
          }
        })
      }
    },

    /*
    @summary function to edit informations (name, info) of a specific condo (condoId)
    @params condoId: id, name: String, info {
                        address: String,
                        city: String,
                        code: String,
                        id: String,
                        }
    @return Do nothing if the connected user is not an admin
    */
    editCondoInfo: function (condoId, name, info) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Condos.update(condoId, { // update Condos collection
          $set: {
            name: name,
            'info.address': info.address,
            'info.city': info.city,
            'info.code': info.code,
            'info.id': info.id,
          }
        })
      }
    },

    /*
    @summary function to change the default contact (userId) of a specific condo (condoId)
    @params condoId: id, userId: id
    @return Do nothing if the connected user is not an admin
    */
    changeDefaultContact: function (condoContactId, userId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        CondoContact.update({ _id: condoContactId }, {
          $set: {
            "defaultContact.userId": userId
          }
        })
      }
    },

    /*
    @summary function to create a new Occupant. Update Meteor.users collection and fill Residents collection with the infos passed in parameter
              the new user creation has to be confirm or deny
    @params newUser {
                      firstname: String,
                      lastname: String,
                      email: String,
                      condo: Object {
                        address: String,
                        city: String,
                        code: String,
                        name: String,
                        type: String (mono, copro, office, etudiante),
                        _id: id,
                        placeId: id,
                        id: String
                    }
                      statut: String (tenant, owner),
                      validation: String,
                      justifFile: String,
                      password: String,
                    }
    @return
    */
    newResidentUser: function (newUser) {
      checkNewUserObject(newUser);

      try {
        Accounts.createUser({ email: newUser.email, password: newUser.password });
      } catch (e) {
        throw new Meteor.Error(e.error, e.reason, e.message);
      }

      const user = Accounts.findUserByEmail(newUser.email); // find the new created user
      let condoId;
      if (newUser.condo._id == "") {
        condoId = newCondoFromResident(newUser.condo);
        // now we are not automatically add EDL for new condo
        // createNewEdlForNewCondo(condoId);
      }
      let userCondos = [];
      let roleId = DefaultRoles.findOne({ name: "Occupant", for: "occupant" })._id;
      userCondos.push({ // set the condos Object
        condoId: (condoId ? condoId : newUser.condo._id),
        invited: new Date(),
        roleId: roleId,
        invitByGestionnaire: false,
        notifications: {
          "actualite": true,
          "incident": true,
          "forum_forum": true,
          "forum_syndic": true,
          "forum_reco": true,
          "forum_boite": true,
          "classifieds": true,
          "resa_new": true,
          "resa_rappel": true,
          "msg_resident": true,
          "msg_conseil": true,
          "msg_gardien": true,
          "msg_gestion": true,
          "edl": true
        },
        preferences: {
          messagerie: true
        },
        buildings: [
          {
            buildingId: "",
            type: newUser.statut
          }
        ],
        userInfo: {
          "company": newUser.company || null,
          "office": null,
          "diploma": null,
          "school": null,
          "studies": null,
          "porte": null,
          "etage": null,
          "wifi": null
        }
      });
      const residentId = Residents.insert({ // fill the new occupant in the Residents collection
        userId: user._id,
        condos: [], // userCondos will go here when this user will be validate
        registeredFrom: (newUser.from ? newUser.from : "web"),
        validation: {
          type: newUser.validation,
          justifFileId: newUser.justifFile,
          isValidate: false // not validate yet
        },
        pendings: userCondos, // not validate yet
      });

      Meteor.users.update({ // update the Meteor.users collection with the new user informations
        _id: user._id,
      }, {
          $set: {
            profile: { civilite: '', lastname: newUser.lastname, firstname: newUser.firstname, tel: '', role: "Resident", lang: newUser.lang },
            'identities.residentId': residentId,
          },
        });

      const userValidation = Accounts.findUserByEmail(newUser.email);
      const condo = Condos.findOne((condoId ? condoId : newUser.condo._id));
      let emailDest = [];
      if (newUser.validation == "backoffice")
        emailDest.push("bonjour@monbuilding.com");
      else {
        const condoContact = CondoContact.findOne({ condoId: condo._id });
        let validEntryDetailId = DeclarationDetails.findOne({ key: "validEntry" });
        let validEntryCondoContact = _.find(condoContact.contactSet, function (contact) {
          return contact.declarationDetailId == validEntryDetailId._id;
        })

        let gestDefaultEntry;
        const gestDefault = condoContact.defaultContact.userId;
        if (validEntryCondoContact && validEntryCondoContact.userIds.length > 0)
          gestDefaultEntry = _.without(validEntryCondoContact.userIds, null, undefined);
        if (gestDefaultEntry && gestDefaultEntry.length > 0) {
          let emails = [];
          let gest;
          _.each(gestDefaultEntry, function (elem) {
            gest = Meteor.users.findOne({ _id: elem }).emails[0].address;
            if (gest) {
              emails.push(gest);
              sendNotifForOccupantValidation(user._id, (condoId ? condoId : newUser.condo._id))
            }
          })
          if (emails.length > 0)
            emailDest = emails;
          else
            emailDest.push("bonjour@monbuilding.com");
        }
        else if (gestDefault) {
          let gest = Meteor.users.findOne({ _id: gestDefault }).emails[0].address;
          if (gest) {
            emailDest.push(gest);
            sendNotifForOccupantValidation(user._id, (condoId ? condoId : newUser.condo._id))
          }
          else
            emailDest.push("bonjour@monbuilding.com");
        }
        else
          emailDest.push("bonjour@monbuilding.com");
      }
      const condoAddress = condo.info.address + " " + condo.info.code + " " + condo.info.city;
      _.each(emailDest, function (emailAddress) {
        Meteor.call(
          'sendEmailSync',
          emailAddress,
          condo._id,
          'Demande de validation d’un nouvel occupant', // NE PAS TRADUIRE !!
          'signin-new-user',
          {
            condoAddress: condoAddress,
            url: Meteor.absoluteUrl(`fr/gestionnaire/occupantList/pending`, condo._id)
          }
        );
      });
    },

    /*
    @summary function to confirm the creation and registration of a new user
    @params userId: id , condoId: id
    @return
    */
    newResidentUserConfirm (userId, condoId) {
      let user = Meteor.users.findOne(userId);
      if (!user) {
        throw new Meteor.Error(601, "Invalid id", "Cannot find user");
      }
      Residents.update({ // the new user is now validate
        _id: user.identities.residentId,
      }, {
          $set: {
            "validation.isValidate": true,
            "validation.validatedBy": Meteor.userId()
          },
        })
      let email = user.emails[0].address;
      const userValidation = Accounts.findUserByEmail(email);
      console.log('condoId', condoId)
      let condo = Condos.findOne(condoId);
      let residentId = user.identities.residentId;
      let pendings = Residents.findOne({ _id: residentId }).pendings;
      _.each(pendings, function (elem, index) {
        pendings[index].joined = new Date().getTime();
      })
      Residents.update({ _id: residentId }, { $set: { pendings: [], condos: pendings } }); // move the condo from pendings to condos array (validation process)
      _.each(pendings, function (elem, index) {
        Meteor.call("createDefaultRightsForNewOccupantWithCondo", userId, elem.condoId);
      })

      const lang = user.profile.lang || "fr"
      var translation = new Email(lang);
      const condoName = (condo.name ? condo.name : (condo.info.addres + ", " + condo.info.code + " " + condo.info.city))
      Meteor.call(
        'sendEmailSync',
        email,
        condoId,
        translation.email['thanks_registering'],
        'accept-new-user-to-condo-' + lang,
        {
          userFirstname: user.profile.firstname,
          homeLink: Meteor.absoluteUrl(lang, condo._id),
          condoName: condoName,
        }
      );

      Meteor.call('addToGestionnaireHistory',
        'trombi',
        userId,
        'acceptedOccupant',
        condoId,
        `${user.profile.firstname} ${user.profile.lastname}`,
        Meteor.userId()
      )

      sendNotifForNewCondo(userId, condoId);
    },

    /*
    @summary function to deny the new user creation and registration
    @params userId: id, condoId: id
    @return
    */
    newResidentUserDeny (userId, condoId) {
      let user = Meteor.users.findOne(userId);
      if (!user) {
        throw new Meteor.Error(601, "Invalid id", "Cannot find user");
      }
      const lang = user.profile.lang || "fr";
      Residents.remove({ _id: user.identities.residentId }); // remove the informations of the new user in Residents collection
      Meteor.users.remove(userId); // remove the informations of the new user in Meteor.users collection
      let email = user.emails[0].address;
      const condo = Condos.findOne(condoId);

      Meteor.call('addToGestionnaireHistory',
        'trombi',
        userId,
        'rejectedOccupant',
        condoId,
        `${user.profile.firstname} ${user.profile.lastname}`,
        Meteor.userId()
      )

      var translation = new Email(lang);
      Meteor.call( // send an email
        'sendEmailSync',
        email,
        condoId,
        translation.email['registering'],
        'deny-new-user-to-condo-' + lang,
        {
          condoName: condo.getName()
        }
      );
    },

    /*
    @summary
    @params newUser {
                      civilite: String,
                      firstname: String,
                      lastname: String,
                      email: String,
                      phone: String,
                      condo: array Object {
                        condo: Object (Condos.findOne() only id),
                        buildings: Object {
                          buildingId: id,
                          etage: String,
                          porte: String,
                          type: String (tenant, owner)
                      },
                      role: String (Gardien, Conseil Syndical, Occupant),
                    },
                      senderEmail: String (Gestionnaire, Admin),
                    }
    @return Do nothing if the connected user is not an admin
    */
    addResidentUser: function (newUser) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Accounts.createUser({ email: newUser.email });

        const user = Accounts.findUserByEmail(newUser.email);
        let userCondos = [];
        _.each(newUser.condo, function (elem) {
          let roleId = DefaultRoles.findOne({ name: elem.role, for: "occupant" })._id;
          userCondos.push({
            condoId: elem.condo._id,
            type: "Invitation",
            invited: new Date(),
            roleId: roleId,
            invitByGestionnaire: ((newUser.senderEmail == "Gestionnaire") ? true : false),
            invitByAdmin: ((newUser.senderEmail == "Gestionnaire") ? false : true),
            notifications: {
              "actualite": true,
              "incident": true,
              "forum_forum": true,
              "forum_syndic": true,
              "forum_reco": true,
              "forum_boite": true,
              "classifieds": true,
              "resa_new": true,
              "resa_rappel": true,
              "msg_resident": true,
              "msg_conseil": true,
              "msg_gardien": true,
              "msg_gestion": true,
              "edl": true
            },
            preferences: {
              messagerie: true
            },
            buildings: elem.buildings
          });
        });
        const residentId = Residents.insert({
          userId: user._id,
          condos: [],
          registeredFrom: (newUser.from ? newUser.from : "web"),
          pendings: userCondos,
        });

        Meteor.users.update({
          _id: user._id,
        }, {
            $set: {
              profile: {
                civilite: newUser.civilite,
                lastname: newUser.lastname,
                firstname: newUser.firstname,
                tel: newUser.mobile,
                telCode: newUser.mobileCode,
                telCodeCountry: newUser.mobileCountry,
                tel2: newUser.phone,
                role: "Resident",
                lang: "fr",
                isFromV1: newUser.isFromV1
              },
              'identities.residentId': residentId,
            },
          });

        const userValidation = Accounts.findUserByEmail(newUser.email);
        const condo = Condos.findOne(newUser.condo[0].condo._id);
        const gestionnaire = Enterprises.findOne({ "condos": newUser.condo[0].condo._id });

        Incidents.update({ "state.status": 1 }, { $push: { 'view': { "lastView": Date.now(), "userId": user._id, "lastViewHistoric": Date.now() } } }, { multi: true });
        // ClassifiedsAds.update({}, {$push: {'views': {userId: user._id, date: Date.now()}}}, {multi: true});
        ActuPosts.update({}, { $push: { 'views': user._id } }, { multi: true });

        const lang = userValidation.profile.lang || "fr"
        var translation = new Email(lang);
        if (!newUser.isFromV1 || newUser.isFromV1 == false) {
          Meteor.call(
            'sendEmailSync',
            newUser.email,
            condo._id,
            translation.email['invite_to_condo'],
            'invite-new-user-to-condo-' + lang,
            {
              condoName: condo.getName(),
              gestionnaireName: ((newUser.senderEmail == "Gestionnaire") ? gestionnaire.name : undefined),
              setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${userValidation._id}`, condo._id),
            }
          );
        } else if (newUser.isFromV1 == true) {
          Meteor.call(
            'sendEmailSync',
            newUser.email,
            condo._id,
            "MonBuilding fait un ravalement de façade !",
            'newUserFromV1-' + lang,
            {
              condoName: condo.getName(),
              firstname: userValidation.profile.firstname,
              setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${userValidation._id}`, condo._id),
            }
          );
        }
      }
    },

    userSetPending: function () {
      let userId = Meteor.userId();
      let user = Meteor.users.findOne(userId);
      if (user && user != undefined && user.services && user.services.password && user.services.password.bcrypt) {
        let pending = Residents.findOne({ userId: userId, "pendings.0": { $exists: true } });
        if (pending !== undefined && !pending.validation) {
          pending2 = pending.pendings[0];
          delete pending2.type;
          Residents.update({ userId: userId }, { $push: { condos: pending2 } });
          Residents.update({ userId: userId }, {
            $pull: {
              pendings: { condoId: pending2.condoId }
            }
          });
          return true;
        }
        return false;
      }
      return false;
    },

    /*
    @summary remove the building (with the id passed in parameter) of the user (userId)
    @params userId: id (_id Meteor.users), buildingId: id
    @return Do nothing if the connected user is not an admin
    */
    removeBuildingOfUser: function (userId, buildingId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Residents.update({ userId: userId }, {
          $pull: {
            buildings: { buildingId: buildingId }
          }
        });
      }
    },

    /*
    @summary change the rights (field, value in parameter) of an occupant status (occupant, syndical member, keeper)
    @params status = 0 (default) | 1 (syndical member) | 2 (keeper), field String, value Number
    @return Do nothing if the connected user is not an admin
    */
    removeBuilding: function (buildingId) {
      if (Meteor.call('isAdmin', this.userId)) {
        let building = Buildings.findOne(buildingId);
        let condo = Condos.findOne({ "buildings": buildingId });
        if (building && condo) {
          Condos.update({ _id: condo._id }, { $pull: { buildings: buildingId } });
          Buildings.remove({ _id: buildingId });

          //TO DO modifier les users ::
          let defaultBuilding = [{
            "buildingId": "",
            "type": ""
          }];
          let users = Residents.find({ "condos.condoId": condo._id }).fetch();
          _.each(users, function (user) {
            var hasBeenModified = false;
            let condos = user.condos;
            _.each(user.condos, function (condo, indexCondo) {
              _.each(condo.buildings, function (building, indexBuilding) {
                if (building.buildingId == buildingId) {
                  hasBeenModified = true;
                  defaultBuilding[0].type = condos[indexCondo].buildings[indexBuilding].type;
                  condos[indexCondo].buildings.splice(indexBuilding, 1);
                }
              });
              if (condos[indexCondo].buildings.length == 0)
                condos[indexCondo].buildings = defaultBuilding;
            });
            if (hasBeenModified == true)
              Residents.update({ _id: user._id }, { $set: { "condos": condos } });
          })
        }
      }
    },

    /*
    @summary remove the occupant (userId) of the condo (condoId)
    @params userId: id, condoId: id
    @return Do nothing if the connected user is an occupant
    */
    removeResidentOfCondo: function (userId, condoId) {
      if (Meteor.call('isAdmin', this.userId) || Meteor.call('isGestionnaire', this.userId)) {
        let condo = Condos.findOne(condoId);
        if (!condo)
          throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
        ClassifiedsAds.remove({ userId: userId, condoId: condoId }); // remove all the classifieds made by the user (userId)

        // BOOKING
        let resources = Resources.find({ "events.origin": userId, condoId: condoId }).fetch(); // remove all the reservations made by the user (userId)
        _.each(resources, (r) => {
          let events = _.filter(r.events, (e) => { return e.origin === userId });
          _.each(events, (e) => {
            if (e.end > Date.now()) {
              Resources.update({ _id: r._id }, {
                $pull: {
                  events: { eventId: e.eventId }
                }
              });
            }
          });
        });

        //EDL
        EDL.remove({ origin: userId, condoId: condoId });

        let resident = Residents.findOne({ userId: userId });

        let archive = Archives.findOne({ userId: userId });
        let id = condo._id;
        let user = Meteor.users.findOne(userId);
        if (archive) {
          Archives.update({ userId: userId, }, {
            $push: {
              condos: {
                type: condo.settings.condoType,
                id: id
              }
            }
          });
        }
        else {
          let data = {
            lastname: user.profile.lastname,
            userId: user._id,
            firstname: user.profile.firstname,
            email: user.emails[0].address,
            createdAt: user.createdAt,
            deletedAt: new Date(),
            condos: [{ type: condo.settings.condoType, id: id }]
          };
          Archives.insert(data); // add the removed user in the Archives collection
        }

        Meteor.call('addToGestionnaireHistory',
          'trombi',
          userId,
          'deletedOccupant',
          condoId,
          `${user.profile.firstname} ${user.profile.lastname}`,
          Meteor.userId()
        );

        const lang = Meteor.users.findOne(userId).profile.lang || "fr"
        var translation = new Email(lang);
        if (resident.condos.length < 2) { // if the removed condo (condoId) was the last condo of the user (userId), the user account is deleted
          Meteor.call('sendEmailSync',
            Meteor.users.findOne(userId).emails[0].address,
            id,
            translation.email['desactivation_account'],
            "remove-access-" + lang,
            {
              userId: userId,
              condoId: id,
              formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, id)
            }
          );
          sendNotifRemoveCondo(userId, condoId);
          // delete account
          Residents.remove({ userId: userId });
          Meteor.users.remove({ _id: userId });
        } else { // only the condo (condoId) of the user (userId) is removed
          Meteor.call('sendEmailSync',
            Meteor.users.findOne(userId).emails[0].address,
            id,
            translation.email['delete_condo'] + condo.name,
            "remove-access-condo-" + lang,
            {
              userId: userId,
              condoId: id,
              building: condo.name,
              formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, id)
            }
          );
          Residents.update({ userId: userId }, { // remove the condo (condoId)
            $pull: {
              condos: { condoId: condoId }
            }
          });
          if (Meteor.user().lastCondoId === condoId) {
            Meteor.users.update({ _id: userId }, {
              $set: { lastCondoId: '' }
            })
          }
          sendNotifRemoveCondo(userId, condoId)
        }
        Meteor.removeUserRights(userId, condoId);
        return true;
      }
    },
    /*
    @summary function to know if the connected user is an admin
    @params
    @return true if the connected user is an admin
            false else (throw error)
    */
    user_allow_backoffice: function () {
      /* Authentication tests */
      if (this.userId === undefined)
        throw new Meteor.Error(300, "Not Authenticated");
      user = Meteor.users.findOne(this.userId, { fields: { "identities": true } });
      if (!user || user.identities.adminId === undefined)
        throw new Meteor.Error(301, "Access denied", "User doesnt have 'admin' identity");
      admin = Admin.findOne({ userId: this.userId });
      if (admin === undefined)
        throw new Meteor.Error(601, "Invalid id", "Cannot find admin identity for this user");

      /* Role && actif test */
      /* ... */
      return true;
    },

    /*
      @summary function to know if the user in parameter is an admin
      @params id: user id. _id in Meteor.users table
      @return true if the user (found with the id in parameter) is an admin
              false else
    */
    isAdmin: function (id) {
      if (id === undefined)
        return false;
      user = Meteor.users.findOne(id, { fields: { "identities": true } });
      if (!user || (user.identities && user.identities.adminId === undefined))
        return false;
      admin = Admin.findOne({ userId: id });
      if (admin === undefined)
        return false;
      return (true);
    },

    isAdminLogged: function () {
      user = Meteor.users.findOne(this.userId(), { fields: { "identities": true } });
      if (!user || (user.identities && user.identities.adminId === undefined))
        return false;
      admin = Admin.findOne({ userId: id });
      if (admin === undefined)
        return false;
      return (true);
    },

    /*
    @summary Change informations of a user. If this user is a manager, change informations in the Gestionnaire table too. If the oldFiles is not empty, remove files to avoid leaks.
    @params id : the user (_id in Meteor.users), data {
        firstname: String,
        lastname: String,
        email: String,
        access: String,
        tel: String,
        picture: String,
        fileId: id,
        removePicture: bool
      }, oldFiles: (optionnal) file id array
    @return Do nothing if the connected user is not an admin
    */
    updateUserFromBackoffice: function (id, data, oldFiles) {
      console.log("______________");
      console.log(oldFiles);
      console.log("______________");
      if (Meteor.call('isAdmin', this.userId) === true) {
        const user = Meteor.users.findOne(id); // change informations of the user
        Meteor.users.update({ _id: id }, {
          $set: {
            'profile.lastname': data.lastname,
            'profile.firstname': data.firstname,
            'emails.0.address': data.email,
            'profile.tel': data.tel,
            'profile.tel2': data.tel2,
            'profile.telCode': data.telCode,
            'profile.telCodeCountry': data.telCodeCountry,
            'profile.civilite': data.civilite == false ? "" : data.civilite
          }
        });
        if (_.has(user.identities, "gestionnaireId")) { // change informations of the manager if the user is a manager
          Enterprises.update({ _id: user.identities.gestionnaireId, 'users.userId': id }, {
            $set: {
              'users.$.firstname': data.firstname,
              'users.$.lastname': data.lastname,
              'users.$.tel': data.tel,
              'users.$.tel2': data.tel,
              'profile.telCode': data.telCode,
              'profile.telCodeCountry': data.telCodeCountry,
              'users.$.civilite': data.civilite == false ? "" : data.civilite
            }
          });
          if (!data.fileId) { // remove profile picture of the linked manager of the UserFiles collection
            const gestionnaire = Enterprises.findOne({ _id: user.identities.gestionnaireId });
            UserFiles.remove(_.find(gestionnaire.users, function (user) {
              return user.userId == id;
            }).profile.fileId);
          }
          if (data.fileId || data.removePicture) { // if there is a new profile picture or the profile picture was removed
            let avatar = null
            if (data.fileId) {
              const fileData = UserFiles.findOne({ _id: fileId });
              if (fileData) {
                avatar = {
                  fileId,
                  mime: fileData.type,
                  ext: fileData.extension
                }
                Meteor.users.update({ _id: id }, {
                  $set: {
                    avatar
                  }
                })
              }
            } else {
              Meteor.users.update({ _id: id }, {
                $set: {
                  avatar: null
                }
              })
            }
            Enterprises.update({ _id: user.identities.gestionnaireId, 'users.userId': id }, {
              $set: {
                'users.$.profile.picture': data.picture,
                'users.$.profile.fileId': data.fileId,
              }
            }); // update the profile picture in the Gestionnaire collection
          }
          _.each(oldFiles, function (file) { // avoid files leaks
            UserFiles.remove(file);
          });
        }
      }
    },

    /*
    @summary Remove the user thanks to the id passed in parameter (_id in Meteor.users)
    @params userId: id
    @return Do nothing if the connected user is not an admin
    */
    removeUserFromBackoffice: function (userId) {
      if (Meteor.call('isAdmin', this.userId) === true) {
        Meteor.users.remove({ _id: userId });
      }
    },

    /*
    @summary add a comment (comment in parameter) to the archive found with userId in parameter
    @params userId: id, condoId: id, comment: String
    @return true if the comment added
    false else
    */
    updateUserCommentArchive: function (userId, condoId, comment) {
      let archive = Archives.findOne({ userId: userId });
      if (!archive)
        return false;
      _.each(archive.condos, (c) => {
        if (c.condoId === condoId && !c.comment)
          c.comment = comment;
      });
      Archives.update({ userId: userId }, {
        $set: { condos: archive.condos }
      });
      return true;
    },
    ServerScriptUpdateOldPostToNew: function () {
      let oldForumPosts = []
      let newPosts = []

      oldForumPosts = ForumPosts.find().fetch()
      newPosts = adaptToNewPost(oldForumPosts)
    },

    ServerScriptMergePerCondoCollections: function () {
      const condoIds = Condos.find({}, { fields: { _id: 1 } }).map(c => c._id);
      mergeCondoCollection('ForumPosts', ForumPosts, condoIds);
      mergeCondoCollection('ForumCommentPosts', ForumCommentPosts, condoIds);
      mergeCondoCollection('ClassifiedsFeed', ClassifiedsFeed, condoIds);
      mergeCondoCollection('Reservations', Reservations, condoIds);
      mergeCondoCollection('Resources', Resources, condoIds);
    }
  })
});

function mergeCondoCollection (from, TargetCollection, condoIds) {
  for (const condoId of condoIds) {
    const SourceCollection = new Mongo.Collection(`${from}${condoId}`);
    const docs = SourceCollection.find({}).fetch();
    if (docs.length > 0) {
      const bulk = TargetCollection.rawCollection().initializeUnorderedBulkOp();
      docs.forEach(doc => {
        bulk.insert({ ...doc, condoId })
      });
      bulk.execute();
      SourceCollection.rawCollection().drop();
    }
  }
}

function adaptToNewPost (ofp) {
  let item = {}
  let items = []
  let forumCondo = []

  forumCondo = getCondoIdWithForumId()

  for (var i = 0; i < ofp.length; i++) {
    let condoId = getCondoId(ofp[i].forumId, forumCondo)

    item['subject'] = removeHtmlTags(ofp[i].description)
    item['condoId'] = condoId[0]
    item['filesId'] = ofp[i].files
    item['date'] = ofp[i].createdAt
    item['filesId'] = ofp[i].files
    item['updatedAt'] = ofp[i].updatedAt
    item['views'] = getPostsViews(ofp[i].views)
    item['userId'] = ofp[i].userId

    if (item.subject !== undefined && item.subject !== null && item.subject !== '') {
      addCommentsToCollection(condoId, ofp[i]._id, ofp[i].message)
      addUnfollowersToCollection(condoId, ofp[i]._id, ofp[i].unfollowers)

      if (ofp[i].poll) {
        item['type'] = 'poll'
        item['inputs'] = insertInputs(ofp[i].poll.choices)
        item['endDate'] = parseInt(moment(ofp[i].poll.endDate + ' ' + ofp[i].poll.endHour, 'DD/MM/YY HH:mm').format('x'))
        item['votes'] = getPollvotes(ofp[i].poll.choices)
      }
      else
        item['type'] = 'post'
      ForumPosts.insert(item)

    }

    item = {}
  }
};

function removeHtmlTags (str) {
  if ((str === null) || (str === ''))
    return false;
  else
    str = str.toString();
  return str.replace(/<[^>]*>/g, '');
}

function addUnfollowersToCollection (condoId, postId, tab) {
  let unfollow = {}

  unfollow['condoId'] = condoId[0]
  unfollow['postId'] = postId

  for (var i = 0; i < tab.length; i++) {
    unfollow['userId'] = tab[i]
    UnFollowForumPosts.upsert({
      'postId': unfollow.postId,
      'condoId': unfollow.condoId,
    }, {
        $addToSet: {
          'unfollowList': unfollow.userId
        }
      }
    );
    unfollow['userId'] = ''
  }
}

function addCommentsToCollection (condoId, postId, tab) {
  let comment = {}

  comment['condoId'] = condoId[0]
  comment['postId'] = postId

  for (var i = 0; i < tab.length; i++) {
    comment['userId'] = tab[i].userId
    comment['filesId'] = tab[i].files
    comment['comment'] = removeHtmlTags(tab[i].description)
    comment['commentDate'] = tab[i].createdAt
    ForumCommentPosts.insert(
      comment
    );

    comment['userId'] = ''
    comment['filesId'] = []
    comment['comment'] = ''
    comment['commentDate'] = ''
  }
}

function getPollvotes (tab) {
  let votes = {}

  for (var i = 0; i < tab.length; i++) {
    for (var j = 0; j < tab[i].votes.length; j++) {
      votes[tab[i].votes[j]] = i + 1
    }
  }

  return votes
}

function getPostsViews (tab) {
  let viewsList = {}

  for (var i = 0; i < tab.length; i++)
    viewsList[tab[i].userId] = tab[i].date

  return viewsList
}

function insertInputs (tab) {
  let inputs = {}

  for (var i = 0; i < tab.length; i++) {
    let j = i + 1
    inputs[j] = tab[i].name
  }

  return inputs
}

function getCondoId (item, forumCondo) {
  let condoId = null

  condoId = _.find(forumCondo, (elem) => {
    if (Object.values(elem) == item)
      return Object.keys(elem)
  })

  return Object.keys(condoId)
}

function getCondoIdWithForumId () {
  let condos = []
  let forumCondo = []
  let item = {}

  let condosPromise = new Promise((resolve, reject) => {
    condos = Condos.find().fetch()
  })

  for (var i = 0; i < condos.length; i++) {
    let forumId = _.find(condos[i].modules, (elem) => { return elem.name == 'forum' })
    item[condos[i]._id] = forumId.data.forumId
    forumCondo.push(item)
    item = {}
  }

  return forumCondo
};

function getDefaultCondoModules () {
  return [{ "name": "incident", "title": "Incident", "slug": "incident", "defaultDiv": true, "boards": ["resident", "gestionnaire"], "data": {} }, { "name": "forum", "title": "Forum", "slug": "forum", "defaultDiv": false, "boards": ["resident"], "data": { "forumId": "" } }, { "name": "classifieds", "title": "Petitesannonces", "slug": "annonces", "defaultDiv": false, "boards": ["resident"], "data": { "classifiedsId": "" } }, { "name": "forumcs", "title": "Forumconseilsyndical", "slug": "forumcs", "defaultDiv": false, "boards": ["resident"], "data": { "forumcsId": "" } }, { "name": "actuality", "title": "Information", "slug": "information", "defaultDiv": false, "boards": ["resident", "gestionnaire"], "data": { "actualityId": "" } }, { "name": "messagerie", "title": "Messagerie", "slug": "messagerie", "defaultDiv": true, "boards": ["resident", "gestionnaire"], "data": { "messagerieId": "" } }, { "name": "trombinoscope", "title": "Trombinoscope", "slug": "trombinoscope", "defaultDiv": false, "boards": ["resident"], "data": {} }, { "name": "reservation", "title": "Réservation", "slug": "reservation", "defaultDiv": true, "boards": ["resident"], "data": {} }]
}

function updateDataModules (modules) {
  const forumId = Forums.insert({ unfollowers: [] });
  const classifiedsId = Classifieds.insert({ unfollowers: [] });
  const actualityId = Actus.insert({ unfollowers: [] });
  const messagerieId = Messageries.insert({});
  _.each(modules, function (elem) {
    if (elem.slug == 'forum')
      elem.data.forumId = forumId;
    if (elem.slug == 'annonces')
      elem.data.classifiedsId = classifiedsId;
    if (elem.slug == 'information')
      elem.data.actualityId = actualityId;
    if (elem.slug == 'messagerie')
      elem.data.messagerieId = messagerieId;
  });
};

function newCondoFromResident (condo) {
  let newCondo = {
    'buildings': [],
    'createdAt': new Date(),
    'info': {
      'address': condo.address,
      'city': condo.city,
      'code': condo.code,
      'placeId': condo.placeId,
      'id': condo.id,
    },
    'modules': [],
    'name': condo.address,
    'settings': {
      'condoType': condo.type,
      'conseilSyndical': (condo.type == "copro" ? true : false),
      'options': {
        'classifieds': true,
        'conciergerie': true,
        'forum': true,
        'incidents': false,
        'informations': true,
        'manual': true,
        'map': true,
        'messenger': true,
        'messengerGardien': true,
        'messengerGestionnaire': false,
        'messengerResident': true,
        'reservations': false,
        'trombi': true,
        'trombiAll': true,
        'trombiCS': false,
        'trombiKeeper': false,
        'couronne': (condo.type == "copro" ? true : false),
        'forumSyndic': (condo.type == "copro" ? true : false),
        'messengerSyndic': (condo.type == "copro" ? true : false),
        "wifi": false
      }
    },
  }
  // creatCondoContactForNewCondo

  const fixtureCondo = DefaultCondo.findOne();
  newCondo.modules = fixtureCondo.default;
  updateDataModules(newCondo.modules);
  const condoId = Condos.insert(newCondo);

  let condoType = condo.type;
  if (condoType == "etudiante")
    condoType = "student";
  var query = "forCondoType." + condoType;
  let rolesId = _.map(DefaultRoles.find({ [query]: true, for: "occupant", name: { $ne: "Occupant" } }).fetch(), "_id");
  CondosModulesOptions.insert({
    condoId: condoId,
    incident: {
      managerActionVisibleBy: rolesId,
    },
    profile: {
      school: condoType === 'student',
      diploma: condoType === 'student',
      studies: condoType === 'student',
      floor: true,
      door: condoType !== 'office',
      company: condoType === 'office',
      office: condoType === 'office',
      documents: false,
      payments: false,
      wifi: false
    }
  })

  return condoId
};

function createNewEdlForNewCondo (condoId) {
  Resources.insert(
    {
      "name": "Etat des lieux",
      "type": "edl",
      "etage": "-",
      "condoId": condoId,
      "horaires": [
        {
          "dow": [
            0
          ],
          "start": "00:00",
          "end": "00:01",
          "open": false,
          "full": false
        },
        {
          "dow": [
            1
          ],
          "start": "08:00",
          "end": "20:00",
          "open": true,
          "full": false
        },
        {
          "dow": [
            2
          ],
          "start": "08:00",
          "end": "20:00",
          "open": true,
          "full": false
        },
        {
          "dow": [
            3
          ],
          "start": "08:00",
          "end": "12:00",
          "open": true,
          "full": false
        },
        {
          "dow": [
            4
          ],
          "start": "08:00",
          "end": "20:00",
          "open": true,
          "full": false
        },
        {
          "dow": [
            5
          ],
          "start": "08:00",
          "end": "20:00",
          "open": true,
          "full": false
        },
        {
          "dow": [
            6
          ],
          "start": "00:00",
          "end": "00:01",
          "open": false,
          "full": false
        }
      ],
      "events": [],
      "edlDuration": 45
    }
  )
};

function checkNewUserObject (newUser) {
  if (!newUser.firstname || newUser.firstname == "")
    throw new Meteor.Error(404, "Invalid firstname", "Cannot find firstname");
  if (!newUser.lastname || newUser.lastname == "")
    throw new Meteor.Error(404, "Invalid lastname", "Cannot find lastname");
  if (!newUser.email || newUser.email == "")
    throw new Meteor.Error(404, "Invalid email", "Cannot find email");
  if (!newUser.statut || newUser.statut == "")
    throw new Meteor.Error(404, "Invalid statut", "Cannot find statut");
  if (!newUser.validation || newUser.validation == "")
    throw new Meteor.Error(404, "Invalid validation", "Cannot find validation");
  if (!newUser.password || newUser.password == "")
    throw new Meteor.Error(404, "Invalid password", "Cannot find password");

  if (!newUser.condo)
    throw new Meteor.Error(404, "Invalid condo", "Cannot find condo");
  if (!newUser.condo.address || newUser.condo.address == "")
    throw new Meteor.Error(404, "Invalid condo.address", "Cannot find condo.address");
  if (!newUser.condo.city || newUser.condo.city == "")
    throw new Meteor.Error(404, "Invalid condo.city", "Cannot find condo.city");
  if (!newUser.condo.code || newUser.condo.code == "")
    throw new Meteor.Error(404, "Invalid condo.code", "Cannot find condo.code");
  if (!newUser.condo.type || newUser.condo.type == "")
    throw new Meteor.Error(404, "Invalid condo.type", "Cannot find condo.type");
  if (newUser.condo._id == undefined)
    throw new Meteor.Error(404, "Invalid condo._id", "Cannot find condo._id");
  if (newUser.condo.name == undefined)
    throw new Meteor.Error(404, "Invalid condo._id", "Cannot find condo._id");
};

function createNewContactSet (condoId, defaultContact) {
  let newDefaultContact = {
    condoId: condoId,
    defaultContact: {
      declarationDetailId: DeclarationDetails.findOne({ "key": "defaultContact" })._id,
      userId: defaultContact,
    },
    contactSet: [],
    contactReminder: [],
  };

  let incidentPriorityUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "urgent" })._id;
  let incidentPriorityNonUrgent = IncidentPriority.findOne({ condoId: "default", defaultPriority: "no-urgent" })._id;

  newDefaultContact.contactReminder = [
    {
      priorityId: incidentPriorityUrgent,
      timeValue: 24
    },
    {
      priorityId: incidentPriorityNonUrgent,
      timeValue: 48
    }
  ];

  let condoType = Condos.findOne(condoId).settings.condoType;
  let defaultContactCase = ContactManagement.findOne({ condoId: "default", forCondoType: condoType });
  let allDeclarationIds = [];
  _.each(defaultContactCase.messengerSet, function (list) {
    allDeclarationIds = allDeclarationIds.concat(list.declarationDetailIds);
  });

  _.each(allDeclarationIds, function (elem) {
    newDefaultContact.contactSet.push({
      declarationDetailId: elem,
      userIds: []
    });
  });

  CondoContact.upsert({ condoId: condoId }, newDefaultContact);

};


/* INCIDENT TYPE */


function setIncidentTypeForCondoFromDefault (condoId) {
  let defaultIncidentType = IncidentType.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentType, function (incidentType) {
    typeId = incidentType._id;
    delete incidentType.condoId;
    delete incidentType._id;
    incidentType.condoId = condoId;
    let newTypeId = IncidentType.insert(incidentType);

    if (!ContactManagement.findOne({ condoId: condoId })) {
      let condoType = Condos.findOne(condoId).settings.condoType;
      defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });

      delete defaultContactManagement._id;
      delete defaultContactManagement.forCondoType;
      delete defaultContactManagement.condoId;
      defaultContactManagement.condoId = condoId;
      ContactManagement.insert(defaultContactManagement);
    }

    ContactManagement.update(
      {
        condoId: condoId,
        "messengerSet.id": typeId
      }, {
        $set: {
          "messengerSet.$.id": newTypeId
        }
      }
    );
    let condoContact = CondoContact.findOne({ condoId: condoId });
    _.each(condoContact.contactSet, function (elem, index) {
      if (elem.messageTypeId == typeId)
        condoContact.contactSet[index].messageTypeId = newTypeId;
    })
    CondoContact.update(condoContact._id, {
      $set: {
        contactSet: _.without(condoContact.contactSet, null, undefined)
      }
    });
  })
};

function setIncidentTypeForCondoFromDefaultWithoutOne (condoId, withoutTypeId) {
  let defaultIncidentType = IncidentType.find({ condoId: "default" }).fetch();
  let typeToDelete = null;
  _.each(defaultIncidentType, function (incidentType) {
    typeId = incidentType._id;
    delete incidentType.condoId;
    delete incidentType._id;
    incidentType.condoId = condoId;
    let newTypeId = IncidentType.insert(incidentType);
    if (typeId == withoutTypeId) {
      typeToDelete = newTypeId;
    }
    if (!ContactManagement.findOne({ condoId: condoId })) {
      let condoType = Condos.findOne(condoId).settings.condoType;
      defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });

      delete defaultContactManagement._id;
      delete defaultContactManagement.forCondoType;
      delete defaultContactManagement.condoId;
      defaultContactManagement.condoId = condoId;
      ContactManagement.insert(defaultContactManagement);
    }

    ContactManagement.update(
      {
        condoId: condoId,
        "messengerSet.id": typeId
      }, {
        $set: {
          "messengerSet.$.id": newTypeId
        }
      }
    );
    let condoContact = CondoContact.findOne({ condoId: condoId });
    _.each(condoContact.contactSet, function (elem, index) {
      if (elem.messageTypeId == typeId)
        condoContact.contactSet[index].messageTypeId = newTypeId;
    })
    CondoContact.update(condoContact._id, {
      $set: {
        contactSet: _.without(condoContact.contactSet, null, undefined)
      }
    });
  });
  if (typeToDelete != null) {
    deleteIncidentTypeForCondo(condoId, typeToDelete);
  }
};

function deleteIncidentTypeForCondo (condoId, typeId) {
  IncidentType.remove(typeId);
  ContactManagement.update(
    {
      condoId: condoId,
      "messengerSet.id": typeId
    },
    {
      $pull: {
        "messengerSet": { id: typeId }
      }
    }
  )
  let condoContact = CondoContact.findOne({ condoId: condoId });
  _.each(condoContact.contactSet, function (elem, index) {
    if (elem.messageTypeId == typeId)
      delete condoContact.contactSet[index];
  })
  CondoContact.update(condoContact._id, {
    $set: {
      contactSet: _.without(condoContact.contactSet, null, undefined)
    }
  });
};

function createNewEmptyIncidentType (condoId) {
  return IncidentType.insert({
    "value-fr": "",
    "value-en": "",
    "picto": "",
    "condoId": condoId,
    "isNewEmpty": true
  })
};

function checkIfStillIncidentType (condoId) {
  if (!IncidentType.findOne({ condoId: condoId })) {
    ContactManagement.remove({ condoId: condoId });

    let thisCondo = Condos.findOne(condoId);

    let condoContact = CondoContact.findOne({ condoId: condoId });

    _.each(condoContact.contactSet, function (elem, index) {
      if (elem.messageTypeId != null)
        delete condoContact.contactSet[index];
    })

    let defaultMessengerSet = ContactManagement.findOne({ condoId: "default", forCondoType: thisCondo.settings.condoType }).messengerSet;

    _.each(defaultMessengerSet, function (elem) {
      _.each(elem.declarationDetailIds, function (detailId) {
        condoContact.contactSet.push({
          messageTypeId: elem.id,
          declarationDetailId: detailId,
          userIds: []
        })
      })
    });

    CondoContact.update(condoContact._id, {
      $set: {
        contactSet: _.without(condoContact.contactSet, null, undefined)
      }
    });
  }
};

function updateManagementWithNewIncidentType (condoId, incidentTypeId) {
  let condoType = Condos.findOne(condoId).settings.condoType;
  defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });
  otherIncidentTypeId = IncidentType.findOne({ condoId: "default", defaultType: "other" })._id
  let defaultContactSetOther = _.find(defaultContactManagement.messengerSet, function (messengerSet) {
    if (messengerSet.id == otherIncidentTypeId)
      return true;
  });
  if (defaultContactSetOther) {
    ContactManagement.update({ condoId: condoId }, {
      $push: {
        messengerSet: {
          id: incidentTypeId,
          declarationDetailIds: defaultContactSetOther.declarationDetailIds
        }
      }
    });
    let condoContact = CondoContact.findOne({ condoId: condoId });
    _.each(defaultContactSetOther.declarationDetailIds, function (declarationId) {
      condoContact.contactSet.push({
        messageTypeId: incidentTypeId,
        declarationDetailId: declarationId,
        userIds: []
      })
    })
    CondoContact.update(condoContact._id, {
      $set: {
        contactSet: _.without(condoContact.contactSet, null, undefined)
      }
    });
  }
};

function setContactManagementFromOneToOtherOne (condoId, incidentTypeId, typeDefault) {
  let condoType = Condos.findOne(condoId).settings.condoType;
  defaultContactManagement = ContactManagement.findOne({ forCondoType: condoType });
  otherIncidentTypeId = IncidentType.findOne({ condoId: "default", defaultType: typeDefault })._id
  let defaultContactSetOther = _.find(defaultContactManagement.messengerSet, function (messengerSet) {
    if (messengerSet.id == otherIncidentTypeId)
      return true;
  });
  if (defaultContactSetOther) {
    let previousIds = _.find(ContactManagement.findOne({ condoId: condoId }).messengerSet, function (elem) {
      if (elem.id == incidentTypeId)
        return true;
    }).declarationDetailIds;
    ContactManagement.update(
      {
        condoId: condoId,
        "messengerSet.id": incidentTypeId
      },
      {
        $set: {
          "messengerSet.$": {
            id: incidentTypeId,
            declarationDetailIds: defaultContactSetOther.declarationDetailIds
          }
        }
      });
    let condoContact = CondoContact.findOne({ condoId: condoId });
    _.each(defaultContactSetOther.declarationDetailIds, function (declarationId) {
      condoContact.contactSet.push({
        messageTypeId: incidentTypeId,
        declarationDetailId: declarationId,
        userIds: []
      })
    })
    _.each(previousIds, function (declarationId) {
      _.each(condoContact.contactSet, function (elem, index) {
        if (elem.declarationDetailId == declarationId)
          delete condoContact.contactSet[index];
      })
    })
    CondoContact.update(condoContact._id, {
      $set: {
        contactSet: _.without(condoContact.contactSet, null, undefined)
      }
    });
  }
};



/* INCIDENT DETAILS */

function setIncidentDetailForCondoFromDefault (condoId) {
  let defaultIncidentDetails = IncidentDetails.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentDetails, function (incidentDetail) {
    typeId = incidentDetail._id;
    delete incidentDetail.condoId;
    delete incidentDetail._id;
    incidentDetail.condoId = condoId;
    let newTypeId = IncidentDetails.insert(incidentDetail);
  })
};

function setIncidentDetailForCondoFromDefaultWithoutOne (condoId, withoutTypeId) {
  let defaultIncidentDetails = IncidentDetails.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentDetails, function (incidentDetail) {
    typeId = incidentDetail._id;
    delete incidentDetail.condoId;
    delete incidentDetail._id;
    incidentDetail.condoId = condoId;
    if (typeId != withoutTypeId) {
      let newTypeId = IncidentDetails.insert(incidentDetail);
    }
  });
};

function deleteIncidentDetailForCondo (condoId, typeId) {
  IncidentDetails.remove(typeId);
};

function createNewEmptyIncidentDetails (condoId) {
  return IncidentDetails.insert({
    "value-fr": "",
    "value-en": "",
    "picto": "",
    "condoId": condoId,
    "isNewEmpty": true
  })
};


/* INCIDENT AREA */

function setIncidentAreaForCondoFromDefault (condoId) {
  let defaultIncidentArea = IncidentArea.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentArea, function (incidentArea) {
    typeId = incidentArea._id;
    delete incidentArea.condoId;
    delete incidentArea._id;
    incidentArea.condoId = condoId;
    let newTypeId = IncidentArea.insert(incidentArea);
  })
};

function setIncidentAreaForCondoFromDefaultWithoutOne (condoId, withoutTypeId) {
  let defaultIncidentArea = IncidentArea.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentArea, function (incidentArea) {
    typeId = incidentArea._id;
    delete incidentArea.condoId;
    delete incidentArea._id;
    incidentArea.condoId = condoId;
    if (typeId != withoutTypeId) {
      let newTypeId = IncidentArea.insert(incidentArea);
    }
  });
};

function deleteIncidentAreaForCondo (condoId, typeId) {
  IncidentArea.remove(typeId);
};

function createNewEmptyIncidentArea (condoId) {
  return IncidentArea.insert({
    "value-fr": "",
    "value-en": "",
    "picto": "",
    "condoId": condoId,
    "isNewEmpty": true
  })
};


/* INCIDENT PRIORITY */

function setIncidentPriorityForCondoFromDefault (condoId) {
  let defaultIncidentPriority = IncidentPriority.find({ condoId: "default" }).fetch();
  _.each(defaultIncidentPriority, function (incidentPriority) {
    typeId = incidentPriority._id;
    delete incidentPriority.condoId;
    delete incidentPriority._id;
    incidentPriority.condoId = condoId;
    let newTypeId = IncidentPriority.insert(incidentPriority);
    CondoContact.update({
      condoId: condoId,
      "contactReminder.priorityId": typeId
    }, {
        $set: {
          "contactReminder.$.priorityId": newTypeId
        }
      });
  })
};

function setIncidentPriorityForCondoFromDefaultWithoutOne (condoId, withoutTypeId) {
  let defaultIncidentPriority = IncidentPriority.find({ condoId: "default" }).fetch();
  let typeToDelete = null;
  _.each(defaultIncidentPriority, function (incidentType) {
    typeId = incidentType._id;
    delete incidentType.condoId;
    delete incidentType._id;
    incidentType.condoId = condoId;
    let newTypeId = IncidentPriority.insert(incidentType);
    if (typeId == withoutTypeId) {
      typeToDelete = newTypeId;
    }

    let condoContact = CondoContact.findOne({ condoId: condoId });
    _.each(condoContact.contactReminder, function (elem, index) {
      if (elem.priorityId == typeId)
        condoContact.contactReminder[index].priorityId = newTypeId;
    })
    CondoContact.update(condoContact._id, {
      $set: {
        contactReminder: _.without(condoContact.contactReminder, null, undefined)
      }
    });
  });
  if (typeToDelete != null) {
    deleteIncidentPriorityForCondo(condoId, typeToDelete);
  }
};

function deleteIncidentPriorityForCondo (condoId, typeId) {
  IncidentPriority.remove(typeId);

  let condoContact = CondoContact.findOne({ condoId: condoId });
  _.each(condoContact.contactReminder, function (elem, index) {
    if (elem.priorityId == typeId)
      delete condoContact.contactReminder[index];
  })
  CondoContact.update(condoContact._id, {
    $set: {
      contactReminder: _.without(condoContact.contactReminder, null, undefined)
    }
  });
};

function checkIfStillIncidentPriority (condoId) {
  if (!IncidentPriority.findOne({ condoId: condoId })) {
    let thisCondo = Condos.findOne(condoId);
    let defaultIncidentPriority = IncidentPriority.find({ condoId: "default" }).fetch();
    let contactReminder = [];
    _.each(defaultIncidentPriority, function (incidentType) {
      contactReminder.push({
        "priorityId": incidentType._id,
        "timeValue": incidentType.defaultPriority == "urgent" ? 24 : 48
      })
    });
    CondoContact.update({ condoId: condoId }, {
      $set: {
        contactReminder: _.without(contactReminder, null, undefined)
      }
    });
  }
};

function createNewEmptyIncidentPriority (condoId) {
  return IncidentPriority.insert({
    "value-fr": "",
    "value-en": "",
    "picto": "",
    "condoId": condoId,
    "isNewEmpty": true
  })
};

function updateReminderWithNewIncidentPriority (condoId, incidentPriorityId) {
  CondoContact.update({ condoId: condoId }, {
    $push: {
      contactReminder: {
        priorityId: incidentPriorityId,
        timeValue: 24
      }
    }
  });
}

function sendCondoInvitationEmailToExistingUser (email, condoId, lang = 'fr') {
  const condo = Condos.findOne(condoId);
  Meteor.call(
    'sendEmailSync',
    email,
    condoId,
    lang == "en" ? 'You are invited to condo ' + condo.name : 'Vous avez été invité à rejoindre l\'immeuble ' + condo.name,
    'invite-existing-user-to-condo-' + lang,
    {
      condoName2: condo.name,
    }
  );
}

function createNewBuilding (condo) {
  let condoModules = getDefaultCondoModules()
  updateDataModules(condoModules)
  const newCondoId = Condos.insert({
    createdAt: new Date(),
    info: {
      address: condo.adressInfos.street_number + ' ' + condo.adressInfos.road,
      city: condo.adressInfos.city,
      code: condo.adressInfos.code,
      id: condo.formBuilding.buildingId || '-1'
    },
    name: condo.formBuilding.buildingName || (condo.adressInfos.street_number + ' ' + condo.adressInfos.road),
    modules: condoModules,
    settings: {
      condoType: condo.formBuilding.type,
      conseilSyndical: true,
      options: {
        couronne: condo.formBuilding.type === 'copro',
        EDL: true,
        classifieds: true,
        conciergerie: true,
        forum: true,
        forumSyndic: false,
        incidents: true,
        informations: true,
        manual: true,
        map: true,
        messenger: true,
        messengerGardien: true,
        messengerGestionnaire: true,
        messengerResident: true,
        messengerSyndic: false,
        reservations: true,
        trombi: true,
        trombiAll: true,
        trombiCS: false,
        trombiKeeper: false,
        buildingsList: true,
        digitalWallet: false,
        marketPlace: false,
        emergencyContact: false,
        print: false,
        wifi: false
      }
    }

  })

  let subBuildingsId = []

  if (condo.formBuilding.subBuildings) {
    condo.formBuilding.subBuildings.forEach(subBuilding => {
      subBuildingsId.push(Buildings.insert({
        createdAt: new Date(),
        condoId: newCondoId,
        name: subBuilding.name,
        info: {
          address: subBuilding.adressInfos.street_number + ' ' + subBuilding.adressInfos.road,
          city: subBuilding.adressInfos.city,
          code: subBuilding.adressInfos.code,
          placeId: subBuilding.adressInfos.id
        },
      }))
    })
  }
  Condos.update(newCondoId, {
    $set: {
      buildings: subBuildingsId,
    }
  });

  let condoType = condo.formBuilding.type;
  if (condoType == "etudiante")
    condoType = "student";
  const query = "forCondoType." + condoType;
  const rolesId = _.map(DefaultRoles.find({ [query]: true, for: "occupant", name: { $ne: "Occupant" } }).fetch(), "_id");
  CondosModulesOptions.insert({
    condoId: newCondoId,
    incident: {
      managerActionVisibleBy: rolesId,
    },
    profile: {
      school: condoType === 'student',
      diploma: condoType === 'student',
      studies: condoType === 'student',
      floor: true,
      door: condoType !== 'office',
      company: condoType === 'office',
      office: condoType === 'office',
      documents: false,
      payments: false,
      wifi: false
    },
    forum: {
      type: 'normal'
    }
  })


  createNewContactSet(newCondoId, '')
  return newCondoId
}
