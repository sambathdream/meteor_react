import { Meteor } from 'meteor/meteor'

Meteor.startup(function () {
  Meteor.publish('admin_userFiles', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return UserFiles.find().cursor
    }
  })
  Meteor.publish('admin_userFiles_messenger_condo_caracteristic', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return UserFiles.find({
        $or: [
          {'meta.messengerArea': true},
          {'meta.messengerDetails': true},
          {'meta.messengerType': true}
        ]
      }).cursor
    }
  })
  Meteor.publish('admin_buildings', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return Condos.find()
    }
  })
  Meteor.publish('admin_entities', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return Enterprises.find()
    }
  })
  Meteor.publish('admin_condosModulesOptions', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return CondosModulesOptions.find()
    }
  })
  Meteor.publish('admin_residents', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return Residents.find()
    }
  })
  Meteor.publish('admin_UsersJustifFiles', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return UsersJustifFiles.find().cursor
    }
    else this.stop()
  })
  Meteor.publish('admin_condosCharacteristics', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      return [ IncidentDetails.find(),
        IncidentArea.find(),
        IncidentType.find(),
        IncidentPriority.find(),
        ContactManagement.find(),
        DeclarationDetails.find()
      ]
    }
    else this.stop()
  })
  Meteor.publish('backoffice', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      // this.unblock()
      return [
        Meteor.users.find(),
        Residents.find(),
        Condos.find(),
        Enterprises.find(),
        UsersRights.find(),
        DefaultRoles.find(),
        CondoRole.find(),
        DefaultRights.find(),
        CondosModulesOptions.find(),

        UsersJustifFiles.find().cursor,
        IncidentDetails.find(),
        IncidentArea.find(),
        IncidentType.find(),
        IncidentPriority.find(),
        ContactManagement.find(),
        DeclarationDetails.find(),

        PhoneCode.find(),
        CompanyName.find(),
        ConciergeMessage.find(),
        Archives.find(),

        Buildings.find(),
        // ActuPosts.find(),
        // Incidents.find(),
        // Forums.find(),
        // ClassifiedsAds.find(),
        // Classifieds.find(),
        // Messages.find(),
        // MessagesFeed.find(),
        // Analytics.find(),
        CondoContact.find(),
        BuildingStatus.find(),
        // MessengerFiles.find().cursor,
      ]
    }
  })
  Meteor.publish('backoffice_selectBoard', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      let condosIds = []
      /*Gestionnaire = Mongo.Collection('gestionnaires')*/
      let enterpriseList = _.map(Enterprises.find().fetch(), function (elem) {
        _.each(elem.condos, function (elem2) {
          condosIds.push(elem2)
        })
        return { name: elem.name, _id: elem._id, address: elem.info.address, code: elem.info.code, city: elem.info.city }
      })
      let condosListBackoffice = _.map(Condos.find({ _id: { $in: condosIds } }).fetch(), function (elem) {
        return { name: elem.name, _id: elem._id, enterpriseId: Enterprises.findOne({ "condos": elem._id })._id, address: elem.info.address, code: elem.info.code, city: elem.info.city }
      })
      let self = this
      for (elem of enterpriseList) {
        self.added('EnterpriseList', elem._id, elem)
      }
      for (elem of condosListBackoffice) {
        self.added('CondosListBackoffice', elem._id, elem)
      }
      this.ready()
      return []
    }
  })
  Meteor.publish('UserFiles', () => {
    if (!this.user)
      throw new Meteor.Error(300, "Not Authenticated")
    if (Meteor.call('isAdmin', this.user._id)) {
      return UserFiles.find().cursor
    }
  })
})

