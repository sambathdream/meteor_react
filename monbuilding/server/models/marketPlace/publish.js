import { Meteor } from 'meteor/meteor'
import { MarketServices } from '/common/collections/marketPlace'
import { MarketPlaceFiles } from '/common/userFiles'

Meteor.startup(function () {
  Meteor.publish('getMarketPlaceServicesManager', function () {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }
    if (Meteor.call('isGestionnaire', this.userId)) {
      const condoIds = Meteor.listCondoUserHasRight('marketPlace', 'see') || []
      return [
        MarketServices.find({ condoId: { $in: condoIds } }, { sort: { updatedAt: -1 } }),
        MarketPlaceFiles.find({ 'meta.condoId': { $in: condoIds } }).cursor
      ]
    } else {
      throw new Meteor.Error(300, 'Not a manager')
    }
  })
  Meteor.publish('getMarketPlaceServicesOccupant', function (condoId) {
    if (!this.userId || !condoId) {
      throw this.ready() && new Meteor.Error(300, 'Not Authenticated')
    }
    if (Meteor.call('isResidentOfCondo', this.userId, condoId)) {
      return [MarketServices.find({ condoId }, { sort: { updatedAt: -1 } }), MarketPlaceFiles.find({ 'meta.condoId': condoId }).cursor]
    } else {
      throw this.ready() && new Meteor.Error(300, 'Not an occupant')
    }
  })
})
