import { Meteor } from 'meteor/meteor'
import { check, Match } from 'meteor/check'
import { MarketServices, MarketBasket, MarketReservations, TermsOfServices, MarketServiceTypes, ReservedSlots } from '/common/collections/marketPlace'
import { SendEmailMarketPlace, GetEmailReceivers } from './send_emails';
import { UserTransactions } from '/common/collections/Payments'
import { MarketPlaceFiles, BasketFiles } from '/common/userFiles'
import _ from 'lodash'
import { Session } from '../../models_new/imports/session';

export function validatePaidBasket (basketId, transactionId) {
  const existingBasket = MarketBasket.findOne({ _id: basketId, status: 'in_progress' })
  const date = Date.now()
  if (existingBasket) {
    let hasOtherThanPaid = false
    let services = existingBasket.services
    let differentServices = {}
    services.forEach(s => {
      if (!s.pay_outside_platform && !s.validatedAt) {
        let initialVal = (differentServices[s.serviceId] ? differentServices[s.serviceId] : 0)
        differentServices[s.serviceId] = initialVal + parseInt(s.quantity)
      }
    })
    _.each(differentServices, (quantity, id) => {
      const thisService = MarketServices.findOne({ _id: id })
      if (!thisService.unlimited_quantity && thisService.quantity_available < quantity) {
        throw new Meteor.Error(301, 'not_enough_item')
      }
    })
    const thisTransaction = UserTransactions.findOne({ _id: transactionId })
    const cardType = (thisTransaction && thisTransaction.cardType) || ''
    const receivers = GetEmailReceivers('manager', existingBasket.condoId, 'marketPlace.see')
    const receiversEdit = GetEmailReceivers('manager', existingBasket.condoId, 'marketPlace.editItem')
    services.forEach((s, index) => {
      if (!s.pay_outside_platform && !s.validatedAt) {
        const thisService = MarketServices.findOne({ _id: s.serviceId })
        if (!thisService.unlimited_quantity) {
          MarketServices.update({ _id: s.serviceId }, {
            $set: {
              quantity_available: (parseInt(thisService.quantity_available) - parseInt(s.quantity)).toString()
            }
          })
          if ((parseInt(thisService.quantity_available) - parseInt(s.quantity)) <= 2) {
            SendEmailMarketPlace('runningOut', receiversEdit, existingBasket.userId, thisService, s.marketReservationsId, reservedDate)
          }
        }
        let reservedDate = null
        if (thisService.use_time_slot) {
          reservedDate = s.day_selected + ' ' + s.time_slot
          const actualReserved = ReservedSlots.findOne({ serviceId: s.serviceId, day: s.day_selected })
          if (!actualReserved) {
            ReservedSlots.insert({
              serviceId: s.serviceId,
              day: s.day_selected,
              slots: {
                [s.time_slot]: 1
              }
            })
          } else {
            let slots = actualReserved.slots
            slots[s.time_slot] = (slots[s.time_slot] || 0) + 1
            ReservedSlots.update({ _id: actualReserved._id }, {
              $set: {
                slots
              }
            })
          }
        }

        MarketReservations.update({ _id: s.marketReservationsId }, {
          $set: {
            validatedAt: date,
            cardType,
            transactionId
          }
        })
        services[index].validatedAt = date
        SendEmailMarketPlace('newOrder', receivers, existingBasket.userId, thisService, s.marketReservationsId, reservedDate)
      } else if (!s.pay_outside_platform) {
        hasOtherThanPaid = true
      }
    })
    MarketBasket.update({ _id: existingBasket._id }, {
      $set: {
        services,
        status: hasOtherThanPaid ? 'in_progress' : 'finished',
        finishedAt: hasOtherThanPaid ? null : Date.now()
      }
    })
    return existingBasket._id
  }
}

Meteor.startup(function () {
  Meteor.methods({
    saveCondoAccountingValue: (condoId, options) => {
      if (!Meteor.call("isAdmin", Meteor.userId()) && !Meteor.call('isGestionnaireOfCondo', Meteor.userId(), condoId)) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      check(condoId, String)
      check(options, Array)

      options.forEach(o => {
        const query = 'settings.accounting.' + o.key
        Condos.update({ _id: condoId }, {
          $set: {
            [query]: {
              isActive: true,
              value: o.value
            }
          }
        })
      })
    },
    saveCondoAccountingOptions: (condoId, option, value, isBuildingOption) => {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      check(condoId, String)
      check(option, String)
      check(value, Boolean)

      const query = 'settings.accounting.' + option
      let newValue = value
      if (isBuildingOption) {
        newValue = {
          isActive: value,
          value: ''
        }
      }
      Condos.update({ _id: condoId }, {
        $set: {
          [query]: newValue
        }
      })
    },
    getReservedSlots: function(serviceId, day) {
      return ReservedSlots.findOne({ serviceId, day }) || []
    },
    getStatsMarketPlace: function(condoId, start, end) {
      const session = new Session();
      session.validateLogged();

      const condosWithPermission = []
      if (condoId === 'all') {
        const condos = session.getValue('condos')
        condos.forEach(c => {
          if (session.userHasPermission(c, 'marketPlace.see')) {
            condosWithPermission.push(c)
          }
        })
      } else {
        if (session.userHasPermission(condoId, 'marketPlace.see')) {
          condosWithPermission.push(condoId)
        }
      }

      return MarketReservations.find({ condoId: { $in: condosWithPermission }, validatedAt: { $ne: null, $gte: start, $lte: end }}, { fields: { _id: 1, validatedAt: 1 } }).fetch()
    },
    getStatsGraphMarketPlace: function(condoId, start, end) {
      const session = new Session();
      session.validateLogged();

      const condosWithPermission = []
      if (condoId === 'all') {
        const condos = session.getValue('condos')
        condos.forEach(c => {
          if (session.userHasPermission(c, 'marketPlace.see')) {
            condosWithPermission.push(c)
          }
        })
      } else {
        if (session.userHasPermission(condoId, 'marketPlace.see')) {
          condosWithPermission.push(condoId)
        }
      }

      const datas = MarketReservations.aggregate([
        { $match: { condoId: { $in: condosWithPermission }, validatedAt: { $ne: null, $gte: start, $lte: end } } },
        {
          $group: {
            _id: {
              "year": {
                "$year": {
                  "$add": [
                    new Date(0),
                    "$validatedAt"
                  ]
                }
              },
              "month": {
                "$month": {
                  "$add": [
                    new Date(0),
                    "$validatedAt"
                  ]
                }
              },
              "day": {
                "$dayOfMonth": {
                  "$add": [
                    new Date(0),
                    "$validatedAt"
                  ]
                }
              }
            },
            reservations: { $push: { 'serviceTypeKey': '$serviceTypeKey', price: '$price', quantity: '$quantity'} },
            count: { $sum: 1 }
          }
        },
        { $sort: { _id: 1 } }
      ])

      const servicesType = MarketServiceTypes.aggregate([
        { $match: { condoId: { $in: condosWithPermission } } },
        {
          $group: {
            _id: '$key'
          }
        }
      ])

      return {servicesType, datas}
    },
    'update-terms-of-uses': function (condoId, value) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        check(condoId, String)
        check(value, String)
        TermsOfServices.upsert({ condoId }, {
          $set: {
            value
          }
        })
      }
    },
    validateBookBakset: function (condoId) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        const existingBasket = MarketBasket.findOne({ userId: Meteor.userId(), condoId, status: 'in_progress' })
        const date = Date.now()
        if (existingBasket) {
          let hasOtherThanBook = false
          let services = existingBasket.services
          let differentServices = {}
          services.forEach(s => {
            if (s.pay_outside_platform === true && !s.validatedAt) {
              let initialVal = (differentServices[s.serviceId] ? differentServices[s.serviceId] : 0)
              differentServices[s.serviceId] = initialVal + parseInt(s.quantity)
            }
          })
          _.each(differentServices, (quantity, id) => {
            const thisService = MarketServices.findOne({ _id: id })
            if (!thisService.unlimited_quantity && thisService.quantity_available < quantity) {
              throw new Meteor.Error(301, 'not_enough_item')
            }
          })
          const receivers = GetEmailReceivers('manager', existingBasket.condoId, 'marketPlace.see')
          const receiversEdit = GetEmailReceivers('manager', existingBasket.condoId, 'marketPlace.editItem')
          services.forEach((s, index) => {
            if (s.pay_outside_platform === true && !s.validatedAt) {
              const thisService = MarketServices.findOne({ _id: s.serviceId })
              if (!thisService.unlimited_quantity) {
                MarketServices.update({ _id: s.serviceId }, {
                  $set: {
                    quantity_available: (parseInt(thisService.quantity_available) - parseInt(s.quantity)).toString()
                  }
                })
                if ((parseInt(thisService.quantity_available) - parseInt(s.quantity)) <= 2) {
                  SendEmailMarketPlace('runningOut', receiversEdit, existingBasket.userId, thisService, s.marketReservationsId, null)
                }
              }
              if (thisService.use_time_slot) {
                const actualReserved = ReservedSlots.findOne({ serviceId: s.serviceId, day: s.day_selected })
                if (!actualReserved) {
                  ReservedSlots.insert({
                    serviceId: s.serviceId,
                    day: s.day_selected,
                    slots: {
                      [s.time_slot]: 1
                    }
                  })
                }
              }

              MarketReservations.update({ _id: s.marketReservationsId }, {
                $set: {
                  validatedAt: date
                }
              })
              services[index].validatedAt = date
              SendEmailMarketPlace('newOrder', receivers, existingBasket.userId, thisService, s.marketReservationsId, null)
            } else if (!s.pay_outside_platform) {
              hasOtherThanBook = true
            }
          })
          MarketBasket.update({ _id: existingBasket._id }, {
            $set: {
              services,
              status: hasOtherThanBook ? 'in_progress' : 'finished',
              finishedAt: hasOtherThanBook ? null : Date.now()
            }
          })
          return existingBasket._id
        } else {
          throw new Meteor.Error(300, 'Basket not found')
        }
      }
    },
    checkPaidBasketQuantity: function(condoId) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        const existingBasket = MarketBasket.findOne({ userId: Meteor.userId(), condoId, status: 'in_progress' })
        if (existingBasket) {
          let out_of_stock = {}
          let services = existingBasket.services
          let differentServices = {}
          services.forEach(s => {
            if (!s.pay_outside_platform && !s.validatedAt) {
              let initialVal = (differentServices[s.serviceId] ? differentServices[s.serviceId] : 0)
              differentServices[s.serviceId] = initialVal + parseInt(s.quantity)
            }
          })
          _.each(differentServices, (quantity, id) => {
            const thisService = MarketServices.findOne({ _id: id })
            if (!thisService.unlimited_quantity && thisService.quantity_available < quantity) {
              out_of_stock[id] = thisService.quantity_available
            }
          })
          return (!!Object.keys(out_of_stock).length && out_of_stock) || false
        }
      }
      return true
    },
    checkTimeSlotUnavailable: function(condoId) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        const existingBasket = MarketBasket.findOne({ userId: Meteor.userId(), condoId, status: 'in_progress' })
        if (existingBasket) {
          let timeSlotUnavailable = []
          let services = existingBasket.services
          services.forEach(s => {
            if (!s.pay_outside_platform && !s.validatedAt && !!s.use_time_slot) {
              const reservedSlots = ReservedSlots.findOne({ serviceId: s.serviceId, day: s.day_selected })
              if (reservedSlots) {
                const thisSlot = reservedSlots.slots[s.time_slot] || null
                if (thisSlot !== null) {
                  const thisService = MarketServices.findOne({ _id: s.serviceId })
                  const slots = thisService.openedDays
                  const dayName = moment(s.day_selected, 'DD/MM/YYYY').locale('en').format('dddd').toLowerCase()
                  const daySlots = slots[dayName]
                  const selectedSlot = [parseInt(s.time_slot.split(':')[0]), parseInt(s.time_slot.split(':')[1])]
                  const thisDaySlot = daySlots.find(ds => {
                    const start = [parseInt(ds.start.split(':')[0]), parseInt(ds.start.split(':')[1])]
                    const end = [parseInt(ds.end.split(':')[0]), parseInt(ds.end.split(':')[1])]
                    if (selectedSlot[0] >= start[0] && selectedSlot[0] <= end[0] && (selectedSlot[1] >= start[1] || (selectedSlot[1] === start[1] && selectedSlot[1] < end[1]))) {
                      return true
                    }
                  })
                  if (!!thisDaySlot && parseInt(thisSlot) >= parseInt(thisDaySlot.simultaneous_time_slot)) {
                    timeSlotUnavailable.push(s.marketReservationsId)
                  }
                }
              }
            }
          })
          return (!!timeSlotUnavailable.length && timeSlotUnavailable) || false
        }
      }
      return true
    },
    deleteServiceMarketPlace: function (serviceId) {
      check(serviceId, String)
      const thisService = MarketServices.findOne({ _id: serviceId })
      if (!Meteor.userId() || !thisService || !Meteor.userHasRight('marketPlace', 'deleteItem', thisService.condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        return MarketServices.remove({ _id: serviceId })
      }
    },
    archiveMarketReservation: function (marketReservationsId, condoId) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        check(marketReservationsId, String)
        return MarketReservations.update({ _id: marketReservationsId }, {
          $set: {
            archivedAt: Date.now(),
            archivedBy: Meteor.userId(),
          }
        })
      }
    },
    deleteBasketItem: function (reservationId) {
      check(reservationId, String)
      let thisReservation = MarketReservations.findOne({ _id: reservationId })
      if (!Meteor.userId() || !thisReservation || thisReservation.userId !== Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', thisReservation.condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Wrong params')
      } else {
        const basketId = thisReservation.marketBaskedId
        MarketReservations.remove({ _id: reservationId })
        MarketBasket.update({ _id: basketId }, {
          $pull: {
            services: { marketReservationsId: reservationId }
          }
        })
      }
      return true
    },
    saveItemToBasket: function (serviceId, condoId, params, marketReservationsId = null, isOffered = false, actualFiles = []) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'see', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        if (!params.day_selected) {
          params.day_selected = ''
          params.time_slot = ''
        }
        check(serviceId, String)
        check(condoId, String)
        check(params, {
          pick_up: Boolean,
          drop_off: Boolean,
          delivered_by_staff: Boolean,
          picked_up_by_staff: Boolean,
          special_request: String,
          filesId: Array,
          price: String,
          vat: String,
          day_selected: String,
          time_slot: String,
          quantity: String
        })
        check(marketReservationsId, Match.OneOf(null, String))
        check(isOffered, Boolean)

        const existingBasket = MarketBasket.findOne({ userId: Meteor.userId(), condoId, status: 'in_progress'}, { fields: { _id: true } })
        let basketId = null
        if (existingBasket) {
          basketId = existingBasket._id
        } else if (!isOffered) {
          basketId = MarketBasket.insert({
            userId: Meteor.userId(),
            condoId,
            services: [],
            status: 'in_progress',
            createdAt: Date.now()
          })
        }

        const thisService = MarketServices.findOne({ _id: serviceId })

        if (!thisService) {
          throw new Meteor.Error(404, 'item_not_found')
        } else if (!thisService.unlimited_quantity && parseInt(thisService.quantity_available) < parseInt(params.quantity)) {
          throw new Meteor.Error(301, 'not_enough_item')
        }

        if (isOffered) {
          const receivers = GetEmailReceivers('manager', condoId, 'marketPlace.see')
          let reservedDate = null
          if (!thisService.unlimited_quantity) {
            MarketServices.update({ _id: serviceId }, {
              $set: {
                quantity_available: (parseInt(thisService.quantity_available) - 1).toString()
              }
            })
            if ((parseInt(thisService.quantity_available) - 1) <= 2) {
              const receiversEdit = GetEmailReceivers('manager', condoId, 'marketPlace.editItem')
              SendEmailMarketPlace('runningOut', receiversEdit, Meteor.userId(), thisService, marketReservationsId, null)
            }
          }
          if (thisService.use_time_slot) {
            reservedDate = params.day_selected + ' ' + params.time_slot
            const actualReserved = ReservedSlots.findOne({ serviceId, day: params.day_selected })
            if (!actualReserved) {
              ReservedSlots.insert({
                serviceId,
                day: params.day_selected,
                slots: {
                  [params.time_slot]: 1
                }
              })
            } else {
              let slots = actualReserved.slots
              slots[params.time_slot] = (slots[params.time_slot] || 0) + 1
              ReservedSlots.update({ _id: actualReserved._id }, {
                $set: {
                  slots
                }
              })
            }
          }
          SendEmailMarketPlace('newOrder', receivers, Meteor.userId(), thisService, marketReservationsId, reservedDate)
        }

        let filesMapped = []
        params.filesId.forEach(fileId => {
          const thisFile = BasketFiles.findOne({ _id: fileId }, { fields: { _id: 1, extensionWithDot: 1, name: 1, type: 1 } })
          if (thisFile) {
            const link = '/download/BasketFiles/' + thisFile._id + '/original/' + thisFile._id + (thisFile.extensionWithDot || '')
            filesMapped.push({
              id: thisFile._id,
              name: thisFile.name,
              type: thisFile.type,
              link
            })
          }
        })

        if (marketReservationsId !== null) {
          const thisReservation = MarketReservations.findOne({ _id: marketReservationsId })
          const filesDb = thisReservation.special_request_files
          const removedFiles = _.filter(filesDb, file => {
            return !_.find(actualFiles, actualFile => actualFile.id === file.id)
          })
          removedFiles.forEach(f => MarketPlaceFiles.remove({ _id: f.id }))
          filesMapped = [...filesMapped, ...actualFiles]
        // const fileIds = [...actualFiles, ...files.map(file => file._id)]



          MarketReservations.update({ _id: marketReservationsId } , {
            $set: {
              ...params,
              use_time_slot: thisService.use_time_slot || false,
              pick_up_location: thisService.pick_up_location,
              drop_off_location: thisService.drop_off_location,
              payment_location: thisService.payment_location,
              special_request_files: filesMapped,
              updatedAt: Date.now()
            }
          })
          const existingBasket = MarketBasket.findOne({ _id: basketId })
          const thisBasketService = existingBasket.services.find(s => s.marketReservationsId === marketReservationsId)
          return MarketBasket.update({ _id: basketId, 'services.marketReservationsId': marketReservationsId }, {
            $set: {
              'services.$': {
                serviceId: thisBasketService.serviceId,
                marketReservationsId: thisBasketService.marketReservationsId,
                title: thisService.title,
                description: thisService.description,
                quantity: params.quantity,
                price: params.price,
                vat: params.vat,
                day_selected: !!params.day_selected ? params.day_selected : null,
                time_slot: params.time_slot,
                use_time_slot: thisService.use_time_slot || false,
                pay_outside_platform: thisService.pay_outside_platform,
                payment_location: thisService.payment_location,
                createdAt: thisBasketService.createdAt,
                updatedAt: Date.now(),
                validatedAt: null
              }
            }
          })
        } else {
          marketReservationsId = MarketReservations.insert({
            userId: Meteor.userId(),
            condoId,
            serviceId,
            serviceTypeKey: thisService.type,
            marketBaskedId: basketId,
            title: thisService.title,
            description: thisService.description,
            pick_up_location: thisService.pick_up_location,
            drop_off_location: thisService.drop_off_location,
            payment_location: thisService.payment_location,
            use_time_slot: thisService.use_time_slot || false,
            time_slot_scale: thisService.time_slot_scale || null,
            time_slot_duration: thisService.time_slot_duration || null,
            ...params,
            special_request_files: filesMapped,
            createdAt: Date.now(),
            updatedAt: Date.now(),
            validatedAt: isOffered ? Date.now() : null,
            archivedAt: null,
            archivedBy: null,
          })

          return isOffered ? marketReservationsId : MarketBasket.update({ _id: basketId }, {
            $push: {
              services: {
                serviceId,
                marketReservationsId,
                title: thisService.title,
                description: thisService.description,
                quantity: params.quantity,
                price: params.price,
                day_selected: !!params.day_selected ? params.day_selected : null,
                time_slot: params.time_slot,
                vat: params.vat,
                use_time_slot: thisService.use_time_slot || false,
                pay_outside_platform: thisService.pay_outside_platform,
                payment_location: thisService.payment_location,
                createdAt: Date.now(),
                updatedAt: Date.now(),
                validatedAt: null
              }
            }
          })
        }
      }
    },
    createNewMarketService_classic: function(condoId, form, timeSlots = {}, files) {
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'addItem', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        check(form, {
          type: String,
          title: String,
          description: String,
          price: String,
          quantity_available: String,
          unlimited_quantity: Boolean,
          vat: String,
          pick_up: Boolean,
          drop_off: Boolean,
          delivered_by_staff: Boolean,
          picked_up_by_staff: Boolean,
          pick_up_location: String,
          drop_off_location: String,
          pay_on_platform: Boolean,
          pay_outside_platform: Boolean,
          payment_location: String,
          use_time_slot: Boolean,
          same_opened_hours: Boolean,
          allow_recurring_service: Boolean,
          time_slot_scale: Match.OneOf('minutes', 'hours', 'days'),
          time_slot_duration: String,
          product_code: String
        })
        check(timeSlots, {
          globalHours: Array,
          openedDays: {
            monday: Array,
            tuesday: Array,
            wednesday: Array,
            thursday: Array,
            friday: Array,
            saturday: Array,
            sunday: Array,
          }
        })
        check(condoId, String)
        check(files, Array)

        const filesMapped = []
        files.forEach(file => {
          const thisFile = MarketPlaceFiles.findOne({ _id: file._id }, { fields: { _id: 1, extensionWithDot: 1, name: 1, type: 1 } })
          const link = '/download/MarketPlaceFiles/' + thisFile._id + '/original/' + thisFile._id + thisFile.extensionWithDot
          filesMapped.push({
            id: thisFile._id,
            name: thisFile.name,
            type: thisFile.type,
            link
          })
        })
        let openedDays = timeSlots.openedDays
        if (!!form.use_time_slot && form.same_opened_hours) {
          openedDays.monday = !!openedDays.monday.length ? timeSlots.globalHours : []
          openedDays.tuesday = !!openedDays.tuesday.length ? timeSlots.globalHours : []
          openedDays.wednesday = !!openedDays.wednesday.length ? timeSlots.globalHours : []
          openedDays.thursday = !!openedDays.thursday.length ? timeSlots.globalHours : []
          openedDays.friday = !!openedDays.friday.length ? timeSlots.globalHours : []
          openedDays.saturday = !!openedDays.saturday.length ? timeSlots.globalHours : []
          openedDays.sunday = !!openedDays.sunday.length ? timeSlots.globalHours : []
        }
        const serviceId = MarketServices.insert({
          condoId,
          type: form.type,
          title: form.title,
          description: form.description,
          isThirdParty: false,
          link_third_party: null,
          price: form.price,
          quantity_available: form.quantity_available,
          unlimited_quantity: form.unlimited_quantity,
          vat: form.vat,
          pick_up: form.pick_up,
          drop_off: form.drop_off,
          delivered_by_staff: form.delivered_by_staff,
          picked_up_by_staff: form.picked_up_by_staff,
          pick_up_location: form.pick_up_location,
          drop_off_location: form.drop_off_location,
          pay_on_platform: form.pay_on_platform,
          pay_outside_platform: form.pay_outside_platform,
          payment_location: form.payment_location,
          use_time_slot: form.use_time_slot,
          same_opened_hours: form.same_opened_hours,
          allow_recurring_service: form.allow_recurring_service,
          time_slot_scale: form.time_slot_scale,
          time_slot_duration: form.time_slot_duration,
          product_code: form.product_code,
          // simultaneous_time_slot: form.simultaneous_time_slot,
          openedDays: openedDays,
          files: filesMapped,
          createdAt: Date.now(),
          updatedAt: Date.now(),
        })
        const receivers = GetEmailReceivers('resident', condoId, 'marketPlace.see')
        const thisService = MarketServices.findOne({ _id: serviceId })
        SendEmailMarketPlace('serviceAdded', receivers, null, thisService, null, null)
      }
    },
    createNewMarketService_thirdParty: function (condoId, form, timeSlot = null, files) { // TimeSlot to match non third party
      if (!Meteor.userId() || !Meteor.userHasRight('marketPlace', 'addItem', condoId, Meteor.userId())) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        check(form, {
          title: String,
          type: String,
          description: String,
          link_third_party: String,
          price: String,
          product_code: String
        })
        check(condoId, String)
        check(files, Array)

        const filesMapped = []
        files.forEach(file => {
          const thisFile = MarketPlaceFiles.findOne({ _id: file._id }, { fields: { _id: 1, extensionWithDot: 1, name: 1, type: 1 } })
          const link = '/download/MarketPlaceFiles/' + thisFile._id + '/original/' + thisFile._id + thisFile.extensionWithDot
          filesMapped.push({
            id: thisFile._id,
            name: thisFile.name,
            type: thisFile.type,
            link
          })
        })

        // const fileIds = files.map(file => file._id)
        const linkThirdParty = (form.link_third_party.startsWith('http') ? form.link_third_party : 'http://' + form.link_third_party)
        // console.log('form', form)
        const serviceId = MarketServices.insert({
          condoId,
          title: form.title,
          type: form.type,
          description: form.description,
          isThirdParty: true,
          link_third_party: linkThirdParty,
          price: form.price,
          product_code: form.product_code,
          files: filesMapped,
          createdAt: Date.now(),
          updatedAt: Date.now(),
        })
        const receivers = GetEmailReceivers('resident', condoId, 'marketPlace.see')
        const thisService = MarketServices.findOne({ _id: serviceId })
        SendEmailMarketPlace('serviceAdded', receivers, null, thisService, null, null)
      }
    },
    editMarketService_classic: function (serviceId, form, timeSlots = {}, files, actualFiles) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        check(form, {
          type: String,
          title: String,
          description: String,
          price: String,
          quantity_available: String,
          unlimited_quantity: Boolean,
          vat: String,
          pick_up: Boolean,
          drop_off: Boolean,
          delivered_by_staff: Boolean,
          picked_up_by_staff: Boolean,
          pick_up_location: String,
          drop_off_location: String,
          pay_on_platform: Boolean,
          pay_outside_platform: Boolean,
          payment_location: String,
          payment_location: String,
          use_time_slot: Boolean,
          same_opened_hours: Boolean,
          allow_recurring_service: Boolean,
          time_slot_scale: Match.OneOf('minutes', 'hours', 'days'),
          time_slot_duration: String,
          product_code: String,
          // simultaneous_time_slot: String
        })
        check(timeSlots, {
          globalHours: Array,
          openedDays: {
            monday: Array,
            tuesday: Array,
            wednesday: Array,
            thursday: Array,
            friday: Array,
            saturday: Array,
            sunday: Array,
          }
        })
        check(serviceId, String)
        check(files, Array)
        check(actualFiles, Array)
        const thisService = MarketServices.findOne({ _id: serviceId })

        const filesDb = thisService.files
        const removedFiles = _.filter(filesDb, file => {
          return !_.find(actualFiles, actualFile => actualFile.id === file.id)
        })
        removedFiles.forEach(f => MarketPlaceFiles.remove({ _id: f.id }))

        // const fileIds = [...actualFiles, ...files.map(file => file._id)]

        let filesMapped = [...actualFiles]
        files.forEach(file => {
          const thisFile = MarketPlaceFiles.findOne({ _id: file._id }, { fields: { _id: 1, extensionWithDot: 1, name: 1, type: 1 } })
          const link = '/download/MarketPlaceFiles/' + thisFile._id + '/original/' + thisFile._id + thisFile.extensionWithDot
          filesMapped.push({
            id: thisFile._id,
            name: thisFile.name,
            type: thisFile.type,
            link
          })
        })

        let openedDays = timeSlots.openedDays
        if (!!form.use_time_slot && form.same_opened_hours) {
          openedDays.monday = !!openedDays.monday.length ? timeSlots.globalHours : []
          openedDays.tuesday = !!openedDays.tuesday.length ? timeSlots.globalHours : []
          openedDays.wednesday = !!openedDays.wednesday.length ? timeSlots.globalHours : []
          openedDays.thursday = !!openedDays.thursday.length ? timeSlots.globalHours : []
          openedDays.friday = !!openedDays.friday.length ? timeSlots.globalHours : []
          openedDays.saturday = !!openedDays.saturday.length ? timeSlots.globalHours : []
          openedDays.sunday = !!openedDays.sunday.length ? timeSlots.globalHours : []
        }
        MarketServices.update({ _id: serviceId }, {
          $set: {
            type: form.type,
            title: form.title,
            description: form.description,
            price: form.price,
            quantity_available: form.quantity_available,
            unlimited_quantity: form.unlimited_quantity,
            isThirdParty: false,
            link_third_party: null,
            vat: form.vat,
            pick_up: form.pick_up,
            drop_off: form.drop_off,
            delivered_by_staff: form.delivered_by_staff,
            picked_up_by_staff: form.picked_up_by_staff,
            pick_up_location: form.pick_up_location,
            drop_off_location: form.drop_off_location,
            pay_on_platform: form.pay_on_platform,
            pay_outside_platform: form.pay_outside_platform,
            payment_location: form.payment_location,
            use_time_slot: form.use_time_slot,
            same_opened_hours: form.same_opened_hours,
            allow_recurring_service: form.allow_recurring_service,
            time_slot_scale: form.time_slot_scale,
            time_slot_duration: form.time_slot_duration,
            product_code: form.product_code,
            // simultaneous_time_slot: form.simultaneous_time_slot,
            openedDays: openedDays,
            files: filesMapped,
            createdAt: thisService.date,
            updatedAt: Date.now(),
          }
        })
        if (!thisService.unlimited_quantity && thisService.quantity_available === '0') {
          const receivers = GetEmailReceivers('resident', thisService.condoId, 'marketPlace.see')
          SendEmailMarketPlace('serviceRestock', receivers, null, thisService, null, null)
        }
      }
    },
    editMarketService_thirdParty: function (serviceId, form, timeSlots = null, files, actualFiles) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, 'Not Authenticated')
      } else {
        check(form, {
          title: String,
          type: String,
          description: String,
          link_third_party: String,
          price: String,
          product_code: String
        })
        check(serviceId, String)
        check(files, Array)
        check(actualFiles, Array)
        const thisService = MarketServices.findOne({ _id: serviceId })

        const filesDb = thisService.files
        const removedFiles = _.filter(filesDb, file => {
          return !_.find(actualFiles, actualFile => actualFile.id === file.id)
        })
        removedFiles.forEach(f => MarketPlaceFiles.remove({ _id: f.id }))

        // const fileIds = [...actualFiles, ...files.map(file => file._id)]

        let filesMapped = [...actualFiles]
        files.forEach(file => {
          const thisFile = MarketPlaceFiles.findOne({ _id: file._id }, { fields: { _id: 1, extensionWithDot: 1, name: 1, type: 1 } })
          const link = '/download/MarketPlaceFiles/' + thisFile._id + '/original/' + thisFile._id + thisFile.extensionWithDot
          filesMapped.push({
            id: thisFile._id,
            name: thisFile.name,
            type: thisFile.type,
            link
          })
        })

        const linkThirdParty = (form.link_third_party.startsWith('http') ? form.link_third_party : 'http://' + form.link_third_party)
        MarketServices.update({ _id: serviceId }, {
          $set: {
            title: form.title,
            type: form.type,
            description: form.description,
            isThirdParty: true,
            link_third_party: linkThirdParty,
            price: form.price,
            product_code: form.product_code,
            files: filesMapped,
            createdAt: thisService.date,
            updatedAt: Date.now()
          }
        })
      }
    }
  })
})
