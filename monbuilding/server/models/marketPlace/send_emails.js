import { CommonTranslation } from '/common/lang/lang'
import { MarketServices } from '/common/collections/marketPlace'

const typeEmailToName = {
  newOrder: 'market-place-new-order-',
  runningOut: 'market-place-running-out-',
  serviceAdded: 'market-place-service-added-',
  serviceRestock: 'market-place-service-restock-',
}

const typeEmailToSubject = {
  newOrder: 'market_place_new_order_subject',
  runningOut: 'market_place_running_out_subject',
  serviceAdded: 'market_place_service_added_subject',
  serviceRestock: 'market_place_service_restock_subject',
}

function getServiceLinkByType (emailType, serviceId, reservationId, condoId, lang) {
  switch (emailType) {
    case 'newOrder':
      return Meteor.absoluteUrl(`${lang}/gestionnaire/marketPlace/upcoming/${reservationId}`, condoId)
    case 'runningOut':
      return Meteor.absoluteUrl(`${lang}/gestionnaire/marketPlace/editService/${serviceId}`, condoId)
    case 'serviceAdded':
      return Meteor.absoluteUrl(`${lang}/resident/${condoId}/marketPlace/serviceDetails/${serviceId}`, condoId)
    case 'serviceRestock':
      return Meteor.absoluteUrl(`${lang}/resident/${condoId}/marketPlace/serviceDetails/${serviceId}`, condoId)
    default:
      return ''
  }
}

export async function SendEmailMarketPlace (emailType, emailReceiver, bookerId, thisService, reservationId, reservedDate = null) {
  let emails = emailReceiver
  if (typeof emailReceiver === 'string') {
    const thisUser = Meteor.users.findOne({ _id: emailReceiver }, { fields: { emails: 1 } })
    if (thisUser && thisUser.emails && thisUser.emails[0]) {
      emails = [thisUser.emails[0].emailReceiver]
    }
  }

  const condoName = Condos.findOne({ _id: thisService.condoId }).getName()

  const booker = Meteor.users.findOne({ _id: bookerId }, { fields: { profile: 1 } })
  let bookerName = ''
  if (booker && booker.profile) {
    bookerName = booker.profile.firstname + ' ' + booker.profile.lastname
  }

  translations = {
    en: new CommonTranslation('en'),
    fr: new CommonTranslation('fr')
  }
  emails.forEach(userInfo => {
    const serviceLink = getServiceLinkByType(emailType, thisService._id, reservationId, thisService.condoId, userInfo.lang)
    Meteor.call('sendEmailSync',
      userInfo.email,
      thisService.condoId,
      translations[userInfo.lang].commonTranslation[typeEmailToSubject[emailType]],
      typeEmailToName[emailType] + userInfo.lang,
      {
        condo_name: condoName,
        type: translations[userInfo.lang].commonTranslation[`mp_service_${thisService.type}`],
        serviceName: thisService.title,
        bookerName,
        reservedDate,
        serviceLink,
      }
    )
  })
}

export function GetEmailReceivers (usersType, condoId, right) {
  const moduleRight = right.split('.')[0]
  const typeRight = right.split('.')[1]

  let emails = []
  let usersList = []
  if (usersType === 'manager') {
    const managers = (Enterprises.findOne({ 'condos': condoId }, { fields: { users: 1 } }) || {}).users
    usersList = managers.filter(m => {
      return !!m.condosInCharge.find(c => c.condoId === condoId)
    }).map(m => m.userId)
  } else {
    usersList = Residents.find({ 'condos.condoId': condoId }, { fields: { userId: 1 } }).fetch().map(r => r.userId)
  }

  usersList.forEach(userId => {
    if (Meteor.userHasRight(moduleRight, typeRight, condoId, userId)) {
      const thisUser = Meteor.users.findOne({ _id: userId }, { fields: { emails: 1, profile: 1 } })
      if (thisUser && thisUser.emails && thisUser.emails[0]) {
        emails.push({
          lang: thisUser.profile.lang || 'fr',
          email: thisUser.emails[0].address
        })
      }
    }
  })
  return emails
}
