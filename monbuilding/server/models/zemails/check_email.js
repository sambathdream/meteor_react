/*
 THESE FUNCTIONS ARE CALLED BEFORE SENDING DELAYED EMAILS FROM CRON_EMAILS TABLE
 THEY CHECK IF THE USER HAS ALREADY SEEN THE SUBJECT OF THE EMAIL
 AND IF HE HAS UNCHECKED THE NOTIFICATION OPTION IN ITS PROFILE OPTIONS
 */

Meteor.methods({
    /*  CHECK BEFORE SENDING A NEW INFORMATION (Table "actus")
     data
     {
         actuId          (String) ID OF THE NEW INFORMATION
         userId          (String) ID OF THE RECIPIENT OF THE EMAIL
         condoId         (String) ID OF THE CONDO RELATED TO THE NEW INFORMATION
         createdAt       (Date) DATE OF CREATION OF DELAYED EMAIL
     }
     */
    checkActuBeforeSend({actuId, userId, condoId, createdAt}) {
        let actu = Actus.findOne(actuId);
        if (actu) {
            let user = _.find(actu.unfollowers, (e) => {return e === userId});
            if (user)
                return false;
            let resident = Residents.findOne({userId: userId});
            if (!resident)
                return false;
            let condo = _.find(resident.condos, (e) => {return e.condoId === condoId});
            if (condo && condo.notifications["actualite"] === false)
                return false;
            let view = _.find(actu.views, (e) => {return e.userId === userId});
            if (view && view.date > createdAt)
                return false;
        }
        return true;
    },
    /*  CHECK BEFORE SENDING EMAIL ABOUT A NEW CLASSIFIED (Table "classifieds")
     data              (Object) CONTAINS DATA FOR THE CHECKING
     {
         classifiedId    (String) ID OF THE NEW CLASSIFIED
         userId          (String) ID OF THE RECIPIENT OF THE EMAIL
         condoId         (String) ID OF THE CONDO RELATED TO THE NEW INFORMATION
         createdAt       (Date) DATE OF CREATION OF DELAYED EMAIL
     }
     */
    checkClassifiedBeforeSend({classifiedId, userId, condoId, createdAt}) {
        let classified = Classifieds.findOne(classifiedId);
        if (classified) {
            let user = _.find(classified.unfollowers, (e) => {return e === userId});
            if (user)
                return false;
            let resident = Residents.findOne({userId: userId});
            let condo = _.find(resident.condos, (e) => {return e.condoId === condoId});
            if (condo && condo.notifications["classifieds"] === false)
                return false;
            let view = _.find(classified.views, (e) => {return e.userId === userId});
            if (view && view.date > createdAt)
                return false;
        }
        return true;
    },
    /*  CHECK BEFORE SENDING EMAIL ABOUT A NEW MESSAGE (Table "messages")
     data              (Object) CONTAINS DATA FOR THE CHECKING
     {
         msgId           (String) ID OF THE NEW MESSAGE
         userId          (String) ID OF THE RECIPIENT OF THE EMAIL
         condoId         (String) ID OF THE CONDO RELATED TO THE NEW INFORMATION
         createdAt       (Date) DATE OF CREATION OF DELAYED EMAIL
     }
     */
    checkMessageBeforeSend({msgId, userId, condoId, createdAt, isCS}) {
        let msg = Messages.findOne(msgId);
        if (msg) {
            let user = _.find(msg.users, (e) => {return e.userId === userId});
            if (!user || (user.lastVisit && user.lastVisit > createdAt))
                return false;
            return true;
        }
        return false;
    },
    /*  CHECK BEFORE SENDING EMAIL ABOUT A NEW FORUM POST (Table "ForumPosts")
     data              (Object) CONTAINS DATA FOR THE CHECKING
     {
         postId          (String) ID OF THE NEW FORUM POST
         userId          (String) ID OF THE RECIPIENT OF THE EMAIL
         condoId         (String) ID OF THE CONDO RELATED TO THE NEW INFORMATION
         createdAt       (Date) DATE OF CREATION OF DELAYED EMAIL
     }
     */
    checkForumBeforeSend({postId, userId, condoId, createdAt}) {
        let post = ForumPosts.findOne({_id: postId});
        if (post) {
            let unfollowers = UnFollowForumPosts.findOne({ postId: postId })
            if (_.find(unfollowers, (e) => {return e === userId}))
                return false;
            let v = _.find(post.views, (date, viewUserId) => {
                if (viewUserId === userId) {
                    return true
                }
            })
            if (!!v && v > createdAt)
                return false;
        }
        return true;
    }
});
