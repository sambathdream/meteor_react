import { SyncedCron } from 'meteor/percolate:synced-cron';
import {setupKleyCron} from '../../external_services/kley/imports/cron'
const MAX_RETRY_TIMES = 5;
const CLEANING_EMAILS_INTERVAL = 'every 5 seconds';
const SENDING_EMAILS_INTERVAL = 'every 5 seconds';

SyncedCron.config({
  log: false,
  collectionTTL: 10
});

setupKleyCron()

SyncedCron.add({
  persist: false,
  name: 'Cleaning emails cron job',
  schedule(parser) {
    return parser.text(CLEANING_EMAILS_INTERVAL);
  },
  job() {
    // console.log('Clean cron')
    CronEmails.remove({
      retry: {
        $gt: MAX_RETRY_TIMES,
      },
    });
    SyncedCron._collection.remove({})
  }
});

SyncedCron.add({
  persist: false,
  name: 'Sending emails',
  schedule(parser) {
    return parser.text(SENDING_EMAILS_INTERVAL);
  },
  job() {
    // console.log('Send cron')
    CronEmails.find({
      dueTime: {
        $lt: new Date(),
      },
      status: 'waiting',
    }, {
      sort: {
        dueTime: 1,
      },
    }).forEach((cronEmail) => {
      const bindVarHandler = handleOneSendingMailJob.bind(undefined, cronEmail);
      const withMeteorEnvHandler = Meteor.bindEnvironment(bindVarHandler);

      Meteor.defer(withMeteorEnvHandler);
    });
    SyncedCron._collection.remove({})
  },
});

function handleOneSendingMailJob(cronEmail) {
  try {
    checkConditionAndSendEmail(cronEmail);
  } catch (e) {
    console.error(e, 'Cron email id: ' + cronEmail._id);
    increaseMailJobRetryTime(cronEmail, 1);
  }
}

function checkConditionAndSendEmail(cronEmail) {
  if (!canSendEmail(cronEmail)) {
    deleteOneMailJob(cronEmail);
    return;
  }

  sendCronEmail(cronEmail);
}

function increaseMailJobRetryTime(cronEmail, times) {
  CronEmails.update({
    _id: cronEmail._id,
  }, {
    $inc: {
      retry: times,
    },
  });
}

function sendCronEmail(cronEmail) {
  setMailJobStatus(cronEmail, 'processing');
  try {
    sendMailThanDeleteMailJob(cronEmail);
  } catch (e) {
    console.error(e, 'Cron email id: ' + cronEmail._id);
    setMailJobStatus(cronEmail, 'waiting');
    increaseMailJobRetryTime(cronEmail, 1);
  }
}

function sendMailThanDeleteMailJob(cronEmail) {

  if (!!cronEmail.isEvent) {
    Meteor.call('sendEmailEvent', {
      to: cronEmail.to,
      condoId: cronEmail.condoId,
      eventName: cronEmail.data && cronEmail.data.sujet,
      subject: cronEmail.subject,
      template_name: cronEmail.templateName,
      data: cronEmail.data,
      start: cronEmail.data.start,
      end: cronEmail.data.end,
      location: cronEmail.data && cronEmail.data.condo && (cronEmail.data.condo + ' ' + cronEmail.data.locate)
    })
  } else {
    Meteor.call(
      'sendEmailSync',
      cronEmail.to,
      cronEmail.condoId,
      cronEmail.subject,
      cronEmail.templateName,
      cronEmail.data,
    );
  }

  deleteOneMailJob(cronEmail);
}

function deleteOneMailJob(cronEmail) {
  CronEmails.remove({
    _id: cronEmail._id,
  });
}

function setMailJobStatus(cronEmail, status) {
  CronEmails.update({
    _id: cronEmail._id,
  }, {
    $set: {
      status,
    },
  });
}

function canSendEmail(cronEmail) {
  const condition = cronEmail.condition;

  return !doesConditionExist(condition) || isConditionPassed(condition);
}

function doesConditionExist(condition) {
  return !!condition;
}

function isConditionPassed(condition) {
  if (!condition || !condition.name) return true
  return Meteor.call(condition.name, condition.data);
}

SyncedCron.start();

const rightsToEmailTemplate = {
  'new-reply-forum-en': ['forum', 'seeSubject', 'seePoll'],
  'new-reply-forum-fr': ['forum', 'seeSubject', 'seePoll'],
  'new-classified-en': ['ads', 'see'],
  'new-classified-fr': ['ads', 'see'],
  'reminder_edl-en': ['reservation', 'seeAll'],
  'reminder_edl-fr': ['reservation', 'seeAll'],
  'approve-reservation-fr': ['reservation', 'seeAll'],
  'approve-reservation-en': ['reservation', 'seeAll'],
  'invited-reservation-fr': ['reservation', 'seeAll'],
  'invited-reservation-en': ['reservation', 'seeAll'],
  'end_edl': ['reservation', 'seeAll'],
  'new-post-forum-en': ['forum', 'seeSubject', 'seePoll'],
  'new-post-forum-fr': ['forum', 'seeSubject', 'seePoll'],
  'new-actu-en': ['actuality', 'see'],
  'new-actu-fr': ['actuality', 'see'],
  'rappel-actu-en': ['actuality', 'see'],
  'rappel-actu-fr': ['actuality', 'see'],
  'newIncidentDeclaredByBM-en': ['incident', 'see'],
  'newIncidentDeclaredByBM-fr': ['incident', 'see'],
  'newIncidentDeclared-en': ['incident', 'see'],
  'newIncidentDeclared-fr': ['incident', 'see'],
  'incidentCanceled-en': ['incident', 'see'],
  'incidentCanceled-fr': ['incident', 'see'],
  'newIncidentValid-en': ['incident', 'see'],
  'newIncidentValid-fr': ['incident', 'see'],
  'incidentMessage-en': ['incident', 'see'],
  'incidentMessage-fr': ['incident', 'see'],
  'incidentSolved-en': ['incident', 'see'],
  'incidentSolved-fr': ['incident', 'see'],
  'incidentReopen-en': ['incident', 'see'],
  'incidentReopen-fr': ['incident', 'see'],
  'incidentMessageToManager-en': ['incident', 'see'],
  'incidentMessageToManager-fr': ['incident', 'see'],
  'new-message-en': ['messenger', 'seeAllMsgEnterprise'],
  'new-message-fr': ['messenger', 'seeAllMsgEnterprise'],
}

const noRightsTemplate = [
  'invite-new-user-to-condo-en',
  'invite-new-user-to-condo-fr',
  'invite-new-manager-to-enterprise',
  'new-report'
]

Meteor.methods({
  /* CREATE A DELAYED EMAIL, INSERT INTO CRON_EMAILS TABLE
   data
   {
     templateName                                          (String) NAME OF EMAIL TEMPLATE
     to                                                    (String) EMAIL ADDRESS OF RECIPIENT
     subject                                               (String) SUBJECT OF EMAIL
     data                                                  (Object) DATA TO CUSTOMIZE EMAIL TEMPLATE (CF. TEMPLATE FILE WITH BLAZE)
     dueTime                                               (ISODate) DATE OF SENDING (DELAYED EMAIL)
     condition                                             (Object) DATA TO CALL FUNCTION BEFORE SENDING EMAIL
   }
   */
  async scheduleAnEmail(data) {
    check(data, {
      templateName: String,
      to: String,
      condoId: String,
      subject: String,
      isEvent: Match.Maybe(Boolean),
      data: Match.Maybe(Object),
      dueTime: Date,
      condition: Match.Maybe({
        name: String,
        data: Match.Maybe(Object),
      }),
    });

    // console.log('data.templateName', data.templateName)
    const emailTemplate = rightsToEmailTemplate[data.templateName]
    const user = Accounts.findUserByEmail(data.to)

    if (!noRightsTemplate.includes(data.templateName)) {
      if (!data.condition) data.condition = {}

      data.condition.recipientHasRight = data.emailTemplate === 'new-reply-forum-en'
                                          || data.emailTemplate === 'new-reply-forum-fr'
                                          || data.emailTemplate === 'new-post-forum-en'
                                          || data.emailTemplate === 'new-post-forum-fr'
                                          ? Meteor.userHasRight(emailTemplate[0], emailTemplate[1], data.condoId, user._id)
                                          && Meteor.userHasRight(emailTemplate[0], emailTemplate[2], data.condoId, user._id)
                                          : Meteor.userHasRight(emailTemplate[0], emailTemplate[1], data.condoId, user._id)
    }

    this.unblock()
    var id = CronEmails.insert({
      ...data,
      status: 'waiting',
      retry: 0,
    });
    if (id)
      return { id, 'sendOn': data.dueTime, 'sendTo': data.to };
    else
      return 'ERROR'
  },
    //  CANCEL A DELAYED EMAIL
    // cronEmailId                              (String) ID OF CRON EMAIL TO CANCEL
  cancelAnEmail(cronEmailId) {
    CronEmails.remove(cronEmailId);
  },
    //  CANCEL ALL DELAYED MAIL RELATED TO CURRENT USER OF SPECIFIC MODULE
    // module_name                               (String) NAME OF THE SPECIFIC MODULE
  cancelAllMyEmailsFromModule(module_name) {
    if (this.connection)
      throw new Meteor.Error(304, "Server Side Only");
    CronEmails.remove({to: Meteor.user().emails[0].address, "data.module": module_name});
  }
});
