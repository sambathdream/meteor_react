
const Mailgun = require('mailgun-js');

const mailgun = new Mailgun({ apiKey: 'key-2ffce503552fa46f85898d5d6c45982a', domain: 'monbuilding.com' });

const __TESTLIKEPROD__ = false

Meteor.startup(
  function () {

    if (__TESTLIKEPROD__ || process.env.ROOT_URL == "https://www.monbuilding.com/") {
      process.env.MAIL_URL = "smtp://" +
        encodeURIComponent("postmaster@monbuilding.com") + ":" +
        encodeURIComponent("b0ddd4f1c63ea0c829f55c0a26017666") + '@' +
        encodeURIComponent("smtp.mailgun.org") + ":" + 587;
    } else {
        process.env.MAIL_URL = "smtp://" +
        encodeURIComponent("dev.monbuilding@gmail.com") + ":" +
        encodeURIComponent("MonBuilding75!!") + '@' +
        encodeURIComponent("smtp.gmail.com") + ":" + 587;
    }
    //for debug purpose
    // process.env.MAIL_URL = 'smtp://34511670225418881:bbaa39a24ffe3f@smtp.mailtrap.io:2525';
    SSR.compileTemplate('email', Assets.getText('email/body.html'));
    SSR.compileTemplate('footer', Assets.getText('email/component/footer.html'));
    SSR.compileTemplate('slogan-fr', Assets.getText('email/component/slogan-fr.html'));
    SSR.compileTemplate('slogan-en', Assets.getText('email/component/slogan-en.html'));
    SSR.compileTemplate('generated-message-fr', Assets.getText('email/component/generated-message-fr.html'));
    SSR.compileTemplate('generated-message-en', Assets.getText('email/component/generated-message-en.html'));
    SSR.compileTemplate('header-logo', Assets.getText('email/component/header-logo.html'));
    SSR.compileTemplate('style', Assets.getText('email/component/style.html'));
    SSR.compileTemplate('forget-password-en', Assets.getText('email/forget-password-en.html'));
    SSR.compileTemplate('forget-password-fr', Assets.getText('email/forget-password-fr.html'));
    SSR.compileTemplate('invite-existing-user-to-condo-en', Assets.getText('email/invite-existing-user-to-condo-en.html'));
    SSR.compileTemplate('invite-existing-user-to-condo-fr', Assets.getText('email/invite-existing-user-to-condo-fr.html'));
    SSR.compileTemplate('newUserFromV1-fr', Assets.getText('email/newUserFromV1-fr.html'));
    SSR.compileTemplate('invite-new-user-to-condo-en', Assets.getText('email/invite-new-user-to-condo-en.html'));
    SSR.compileTemplate('invite-new-user-to-condo-fr', Assets.getText('email/invite-new-user-to-condo-fr.html'));
    SSR.compileTemplate('accept-new-user-to-condo-en', Assets.getText('email/accept-new-user-to-condo-en.html'));
    SSR.compileTemplate('accept-new-user-to-condo-fr', Assets.getText('email/accept-new-user-to-condo-fr.html'));
    SSR.compileTemplate('deny-new-user-to-condo-en', Assets.getText('email/deny-new-user-to-condo-en.html'));
    SSR.compileTemplate('deny-new-user-to-condo-fr', Assets.getText('email/deny-new-user-to-condo-fr.html'));
    SSR.compileTemplate('invite-external-user', Assets.getText('email/invite-external-user.html'));
    SSR.compileTemplate('signin-new-user', Assets.getText('email/signin-new-user.html'));
    SSR.compileTemplate('delete-post-en', Assets.getText('email/delete-post-en.html'));
    SSR.compileTemplate('delete-post-fr', Assets.getText('email/delete-post-fr.html'));
    SSR.compileTemplate('user-already-password', Assets.getText('email/user-already-password.html'));
    SSR.compileTemplate('user-no-password', Assets.getText('email/user-no-password.html'));
    SSR.compileTemplate('goblin-user-allready-confirm-conciergerie', Assets.getText('email/goblin_user_allready_confirm_conciergerie.html'));
    SSR.compileTemplate('goblin-user-not-confirm-conciergerie', Assets.getText('email/goblin_user_not_confirm_conciergerie.html'));
    SSR.compileTemplate('app-dl', Assets.getText('email/component/app-dl.html'));
    SSR.compileTemplate('unsubscribe-fr', Assets.getText('email/component/unsubscribe-fr.html'));
    SSR.compileTemplate('unsubscribe-en', Assets.getText('email/component/unsubscribe-en.html'));

    /* HOME */
    SSR.compileTemplate('contact-from-home', Assets.getText('email/contact-from-home.html'));


    /* Messagerie */
    SSR.compileTemplate('new-message-en', Assets.getText('email/modules/messagerie/new-message-en.html'));
    SSR.compileTemplate('new-message-fr', Assets.getText('email/modules/messagerie/new-message-fr.html'));
    SSR.compileTemplate('reminder-message-en', Assets.getText('email/modules/messagerie/reminder-en.html'));
    SSR.compileTemplate('reminder-message-fr', Assets.getText('email/modules/messagerie/reminder-fr.html'));
    SSR.compileTemplate('new-post-forum-en', Assets.getText('email/modules/forum/post-create-en.html'));
    SSR.compileTemplate('new-post-forum-fr', Assets.getText('email/modules/forum/post-create-fr.html'));
    SSR.compileTemplate('new-reply-forum-en', Assets.getText('email/modules/forum/post-reply-en.html'));
    SSR.compileTemplate('new-reply-forum-fr', Assets.getText('email/modules/forum/post-reply-fr.html'));
    SSR.compileTemplate('email-concierge', Assets.getText('email/email-concierge.html'));

    /* Actu */
    SSR.compileTemplate('new-classified-en', Assets.getText('email/modules/classifieds/new-classified-en.html'));
    SSR.compileTemplate('new-classified-fr', Assets.getText('email/modules/classifieds/new-classified-fr.html'));
    SSR.compileTemplate('rappel-actu-en', Assets.getText('email/modules/actu/rappel-actu-en.html'));
    SSR.compileTemplate('rappel-actu-fr', Assets.getText('email/modules/actu/rappel-actu-fr.html'));
    SSR.compileTemplate('new-actu-en', Assets.getText('email/modules/actu/new-actu-en.html'));
    SSR.compileTemplate('new-actu-fr', Assets.getText('email/modules/actu/new-actu-fr.html'));
    SSR.compileTemplate('delete-event-en', Assets.getText('email/modules/actu/delete-event-en.html'));
    SSR.compileTemplate('delete-event-fr', Assets.getText('email/modules/actu/delete-event-fr.html'));

    /* Incidents */
    SSR.compileTemplate('reminderDeclare', Assets.getText('email/modules/incident/reminderDeclare.html'));
    SSR.compileTemplate('reminderValid', Assets.getText('email/modules/incident/reminderValid.html'));
    SSR.compileTemplate('newIncidentDeclared-en', Assets.getText('email/modules/incident/newIncidentDeclared-en.html'));
    SSR.compileTemplate('newIncidentDeclared-fr', Assets.getText('email/modules/incident/newIncidentDeclared-fr.html'));
    SSR.compileTemplate('newIncidentDeclaredByBM-en', Assets.getText('email/modules/incident/newIncidentDeclaredByBM-en.html'));
    SSR.compileTemplate('newIncidentDeclaredByBM-fr', Assets.getText('email/modules/incident/newIncidentDeclaredByBM-fr.html'));
    SSR.compileTemplate('newIncidentValid-en', Assets.getText('email/modules/incident/newIncidentValid-en.html'));
    SSR.compileTemplate('newIncidentValid-fr', Assets.getText('email/modules/incident/newIncidentValid-fr.html'));
    SSR.compileTemplate('incidentSolved-en', Assets.getText('email/modules/incident/incidentSolved-en.html'));
    SSR.compileTemplate('incidentSolved-fr', Assets.getText('email/modules/incident/incidentSolved-fr.html'));
    SSR.compileTemplate('incidentReopen-en', Assets.getText('email/modules/incident/incidentReopen-en.html'));
    SSR.compileTemplate('incidentReopen-fr', Assets.getText('email/modules/incident/incidentReopen-fr.html'));
    SSR.compileTemplate('incidentMessage-en', Assets.getText('email/modules/incident/incidentMessage-en.html'));
    SSR.compileTemplate('incidentMessage-fr', Assets.getText('email/modules/incident/incidentMessage-fr.html'));
    SSR.compileTemplate('incidentCanceled-en', Assets.getText('email/modules/incident/incidentCanceled-en.html'));
    SSR.compileTemplate('incidentCanceled-fr', Assets.getText('email/modules/incident/incidentCanceled-fr.html'));

    SSR.compileTemplate('incidentMessageToManager-en', Assets.getText('email/modules/incident/incidentMessageToManager-en.html'));
    SSR.compileTemplate('incidentMessageToManager-fr', Assets.getText('email/modules/incident/incidentMessageToManager-fr.html'));

    /* Reservation */
    SSR.compileTemplate('reservation-en', Assets.getText('email/modules/reservation/reservation-en.html'));
    SSR.compileTemplate('reservation-fr', Assets.getText('email/modules/reservation/reservation-fr.html'));
    SSR.compileTemplate('approve-reservation-en', Assets.getText('email/modules/reservation/approve-reservation-en.html'));
    SSR.compileTemplate('approve-reservation-fr', Assets.getText('email/modules/reservation/approve-reservation-fr.html'));

    SSR.compileTemplate('new-reservation-to-validate-en', Assets.getText('email/modules/reservation/new-reservation-to-validate-en.html'));
    SSR.compileTemplate('new-reservation-to-validate-fr', Assets.getText('email/modules/reservation/new-reservation-to-validate-fr.html'));
    SSR.compileTemplate('invited-reservation-en', Assets.getText('email/modules/reservation/invited-reservation-en.html'));
    SSR.compileTemplate('invited-reservation-fr', Assets.getText('email/modules/reservation/invited-reservation-fr.html'));
    SSR.compileTemplate('invited-reservation-external-en', Assets.getText('email/modules/reservation/invited-reservation-external-en.html'));
    SSR.compileTemplate('invited-reservation-external-fr', Assets.getText('email/modules/reservation/invited-reservation-external-fr.html'));
    SSR.compileTemplate('edl-en', Assets.getText('email/modules/edl/edl-en.html'));
    SSR.compileTemplate('edl-fr', Assets.getText('email/modules/edl/edl-fr.html'));
    SSR.compileTemplate('edlManager', Assets.getText('email/modules/edl/edlManager.html'));
    SSR.compileTemplate('cancel_edl-en', Assets.getText('email/modules/edl/edl_cancel-en.html'));
    SSR.compileTemplate('cancel_edl-fr', Assets.getText('email/modules/edl/edl_cancel-fr.html'));
    SSR.compileTemplate('reminder_edl-en', Assets.getText('email/modules/edl/edl_reminder-en.html'));
    SSR.compileTemplate('reminder_edl-fr', Assets.getText('email/modules/edl/edl_reminder-fr.html'));
    SSR.compileTemplate('confirm_edl-en', Assets.getText('email/modules/edl/edl_confirm-en.html'));
    SSR.compileTemplate('confirm_edl-fr', Assets.getText('email/modules/edl/edl_confirm-fr.html'));
    SSR.compileTemplate('alert_edl', Assets.getText('email/modules/edl/edl_alert.html'));
    SSR.compileTemplate('end_edl', Assets.getText('email/modules/edl/edl_end.html'));

    /* Backoffice */
    SSR.compileTemplate('remove-access-en', Assets.getText('email/modules/backoffice/remove_access-en.html'));
    SSR.compileTemplate('remove-access-fr', Assets.getText('email/modules/backoffice/remove_access-fr.html'));
    SSR.compileTemplate('remove-access-condo-en', Assets.getText('email/modules/backoffice/remove_access_condo-en.html'));
    SSR.compileTemplate('remove-access-condo-fr', Assets.getText('email/modules/backoffice/remove_access_condo-fr.html'));
    SSR.compileTemplate('invite-new-manager-to-enterprise', Assets.getText('email/modules/backoffice/invite-new-manager-to-enterprise.html'));
    SSR.compileTemplate('estudines-satisfaction-fr', Assets.getText('email/estudines-satisfaction-fr.html'));
    /* Report */
    SSR.compileTemplate('new-report', Assets.getText('email/report.html'));

    /* Market Place */
    SSR.compileTemplate('market-place-new-order-fr', Assets.getText('email/modules/marketPlace/market-place-new-order-fr.html'));
    SSR.compileTemplate('market-place-new-order-en', Assets.getText('email/modules/marketPlace/market-place-new-order-en.html'));
    SSR.compileTemplate('market-place-running-out-fr', Assets.getText('email/modules/marketPlace/market-place-running-out-fr.html'));
    SSR.compileTemplate('market-place-running-out-en', Assets.getText('email/modules/marketPlace/market-place-running-out-en.html'));
    SSR.compileTemplate('market-place-service-added-fr', Assets.getText('email/modules/marketPlace/market-place-service-added-fr.html'));
    SSR.compileTemplate('market-place-service-added-en', Assets.getText('email/modules/marketPlace/market-place-service-added-en.html'));
    SSR.compileTemplate('market-place-service-restock-fr', Assets.getText('email/modules/marketPlace/market-place-service-restock-fr.html'));
    SSR.compileTemplate('market-place-service-restock-en', Assets.getText('email/modules/marketPlace/market-place-service-restock-en.html'));

    Accounts.emailTemplates.siteName = "monbuilding.com";
    Accounts.emailTemplates.from = "MonBuilding <bonjour@monbuilding.com>";
    Accounts.emailTemplates.resetPassword.subject = function (user) {
      if (!user.profile.lang || user.profile.lang == "fr")
        return "Oubli de mot de passe";
      else
        return "Forgot password";
    };
    Accounts.emailTemplates.resetPassword.html = function (user, url) {

      const userId = user._id

      let condoId
      const thisUser = Meteor.users.findOne({ _id: userId }, { fields: { identities: true } })
      if (thisUser && thisUser.identities.residentId) {
        const thisResident = Residents.findOne({ _id: thisUser.identities.residentId })
        if (thisResident) {
          condoId = thisResident.condos[0] ? thisResident.condos[0].condoId : thisResident.pendings[0].condoId
        }
      } else if (thisUser && thisUser.identities.gestionnaireId) {
        const enterprise = Enterprises.findOne({ 'users.userId': userId })
        const thisManager = enterprise.users.find(u => u.userId === userId)
        condoId = thisManager.condosInCharge[0].condoId
      }


      url = Meteor.absoluteUrl('#/' + url.split('/#/')[1], condoId, user._id)

      return getEmailHtml("forget-password-" + (user.profile.lang || "fr"), { "url": url, condoId })
    };
    Accounts.emailTemplates.resetPassword.from = function (user) {
      const userId = user._id

      let condoId
      const thisUser = Meteor.users.findOne({ _id: userId }, { fields: { identities: true } })
      if (thisUser && thisUser.identities.residentId) {
        const thisResident = Residents.findOne({ _id: thisUser.identities.residentId })
        if (thisResident) {
          condoId = thisResident.condos[0] ? thisResident.condos[0].condoId : thisResident.pendings[0].condoId
        }
      } else if (thisUser && thisUser.identities.gestionnaireId) {
        const enterprise = Enterprises.findOne({ 'users.userId': userId })
        const thisManager = enterprise.users.find(u => u.userId === userId)
        condoId = thisManager.condosInCharge[0].condoId
      }

      let from = 'MonBuilding <bonjour@monbuilding.com>'
      let custom = Condos.findOne({ _id: condoId })
      if (custom && custom.settings && custom.settings.options && custom.settings.options.customEmail === true && custom.settings.customEmail !== "") {
        from = custom.settings.customEmail + ' <bonjour@monbuilding.com>'
      }

      // Overrides value set in Accounts.emailTemplates.from when resetting passwords
      return from
    }
  })

function getEmailHtml(template_name, data) {
  let lang = template_name.slice(-2);
  let footer = {
    label: "Suivez-nous sur :"
  };
  let unsub = {
    notifUrl: data.notifUrl
  };
  let download = {
    label: "Vous pouvez télécharger l'app sur :"
  };
  if (lang === 'en') footer.label = 'Connect with us';
  if (lang === 'en') download.label = 'You can download the app from:';

  // get custom condo settings
  const logo = CondoLogos.findOne({ condoId: data.condoId })
  const header = {
    logoUrl: (!!data.condoId && !!logo && !!logo.avatar && !!logo.avatar.preview) ?
      logo.avatar.preview :
      'https://www.monbuilding.com/img/logo/logoMail.png'
  }

  if (lang !== 'fr' && lang !== 'en') {
    lang = 'fr'
  }

  const custom = Condos.findOne({ _id: data.condoId });
  data.condoName = (!!logo && !!logo.avatar && !!logo.avatar.preview && !!custom) ? custom.getName() : 'MonBuilding'
  const primary = (!!custom && !!custom.settings && !!custom.settings.colorPalette && !!custom.settings.colorPalette.primary) ?
    custom.settings.colorPalette.primary :
    '#69D2E7'
  const color = {
    primaryColor: primary
  }
  data.primaryColor = primary

  // get custom email signature
  data.signature = 'MonBuilding'
  if (custom && custom.settings && custom.settings.options && custom.settings.options.customEmail === true && !!custom.settings.customEmail) {
    data.signature = custom.settings.customEmail
  }
  data.signatureLink = 'https://www.monbuilding.com/' + lang
  if (custom && custom.settings && custom.settings.options && custom.settings.options.customEmail === true && !!custom.settings.signatureLink) {
    data.signatureLink = custom.settings.signatureLink
  }

  download.ios = "https://itunes.apple.com/fr/app/monbuilding/id1274455987?mt=8";
  download.android = "https://play.google.com/store/apps/details?id=com.monbuilding.app&hl=fr";
  footer.haveLink = false
  footer.address = (!!logo && !!logo.avatar && !!logo.avatar.preview && !!custom) ? custom.getNameWithAddress() : '19 Boulevard Poissonnière, 75002 Paris, France';
  if (!!custom && !!custom.settings) {
    if (!!custom.settings.appDownloadUrl && !!custom.settings.appDownloadUrl.ios && !!custom.settings.appDownloadUrl.android) {
      download.ios = custom.settings.appDownloadUrl.ios
      download.android = custom.settings.appDownloadUrl.android
    }
    if (!!custom.settings.socialMediaUrl) {
      if (!!custom.settings.socialMediaUrl.facebook) {
        footer.facebook = custom.settings.socialMediaUrl.facebook
        footer.haveLink = true
      }
      if (!!custom.settings.socialMediaUrl.twitter) {
        footer.twitter = custom.settings.socialMediaUrl.twitter
        footer.haveLink = true
      }
      if (!!custom.settings.socialMediaUrl.instagram) {
        footer.instagram = custom.settings.socialMediaUrl.instagram
        footer.haveLink = true
      }
    }
  }

  data.slogan = SSR.render('slogan-' + lang);
  data.generatedMessage = SSR.render('generated-message-' + lang);
  data.emailFooter = SSR.render('footer', footer);
  data.style = SSR.render('style', color);
  data.emailHeaderLogo = SSR.render('header-logo', header);
  data.appDl = SSR.render('app-dl', download);
  data.unsubscribeFr = SSR.render('unsubscribe-fr', unsub);
  data.unsubscribeEn = SSR.render('unsubscribe-en', unsub);
  return SSR.render(template_name, data);
}

Meteor.methods({
  /*  SEND INSTANTLY NEW EMAIL
  to                                     (String) EMAIL ADDRESS OF RECIPIENT
  subject                                (String) SUBJECT OF EMAIL
  templateName                           (String) NAME OF EMAIL TEMPLATE
  data                                   (Object) DATA TO CUSTOMIZE EMAIL TEMPLATE (CF. TEMPLATE FILE WITH BLAZE)
  */
  sendEmailSync(to, condoId, subject, template_name, data) {
    // console.log('to', to)
    // console.log('condoId', condoId)
    // console.log('subject', subject)
    // console.log('template_name', template_name)
    // console.log('data', data)
    // console.log('to, condoId, subject, template_name', to, condoId, subject, template_name)
    check([to, subject, template_name], [String]);
    this.unblock();

    let adminUser = Meteor.users.findOne({ "identities.adminId": { $exists: true } });

    if (adminUser && adminUser != undefined && adminUser.emails[0].address == to)
      return;

    if (Isemail.validate(to) !== true) {
      to = Meteor.users.findOne(to)
      if (to) {
        to = to.emails[0].address
      }
      if (!to || Isemail.validate(to) !== true) {
        console.log('wrong email address ---- ', to, subject, template_name, data)
        return;
      }
    }

    let extraData = {}
    // special block for reservation mail
    if (data.isReservation) {
      const start = moment(data.start).utc();
      const end = moment(data.end).utc();
      const eventName = data.eventName;
      const location = data.location;

      // create a new moment based on the original one
      let startFix = start && (typeof start.clone === 'function') && start.clone();
      let endFix = end && (typeof end.clone === 'function') && end.clone();

      // change the time zone of the new moment
      startFix.tz('Europe/Paris'); // or whatever time zone you desire
      endFix.tz('Europe/Paris'); // or whatever time zone you desire

      // shift the moment by the difference in offsets
      startFix.add(start.utcOffset() - startFix.utcOffset(), 'minutes');
      endFix.add(end.utcOffset() - endFix.utcOffset(), 'minutes');

      let cal = ical({});
      cal.events([
        {
          summary: eventName,
          organizer: {
            name: "MonBuilding",
            email: 'bonjour@monbuilding.com'
          },
          location: location,
          start: startFix.toDate(),
          end: endFix.toDate()
        }
      ]);

      // generate ics file
      let domain = Meteor.absoluteUrl('', condoId);
      let path = 'assets/app/uploads/';
      let filename = moment().format('x') + '_' + startFix.format('x') + '.ics';
      cal.save(path + filename);

      data.outlookLink = domain + 'download-ics/' + filename;
      data.icalLink = domain + 'download-ics/' + filename;

      let generateUrl = require('generate-google-calendar-url');
      data.googleLink = generateUrl({
        start: startFix.toDate(),
        end: endFix.toDate(),
        title: eventName,
        location: location,
        details: ''
      });
      data.domain = domain;

      extraData = {
        icalEvent: cal.toString(),
        attachments: [{
          fileName: "reservation.ics",
          contentType: "text/calendar",
          contents: cal.toString()
        }]
      }
    }

    let from = 'MonBuilding <bonjour@monbuilding.com>'
    let custom = Condos.findOne({ _id: condoId })
    if (custom && custom.settings && custom.settings.options && custom.settings.options.customEmail === true && !!custom.settings.customEmail) {
      from = custom.settings.customEmail + ' <bonjour@monbuilding.com>'
    }

    data.condoId = condoId;

    const html = getEmailHtml(template_name, data);
    // console.log('html', html)

    if (!__TESTLIKEPROD__ && (Meteor.isDevelopment || process.env.ROOT_URL != "https://www.monbuilding.com/")) {
    	// console.log(this.connection, this.connection.httpHeaders.host);
        subject = "[DEV] - [@"+to+"] " + subject;
        to = "dev.monbuilding@gmail.com";
    }
    Meteor.defer(function () {
      var data = {
        from: from,
        to: to,
        subject: subject,
        html: html,
        ...extraData
      }

      if (!__TESTLIKEPROD__ && (Meteor.isDevelopment || process.env.ROOT_URL != "https://www.monbuilding.com/")) {
        try {
          // console.log('to', to)
          // console.log('template_name', template_name)
          Email.send(data);
        } catch (e) {
          if (!!e && !!e.response) {
            console.log(e.response);
          } else {
            console.log("Fail sending Email")
          }
        }
      } else {
        mailgun.messages().send(data, function (err, body) {
          if (err) {
            console.log('to', to)
            console.log("got an error: ", err);
          } else {
            console.log('to', to)
            console.log('template_name', template_name)

            console.log(body);
          }
        });
      }
    })
  },
  /*  SEND INSTANTLY NEW EMAIL WITH EVENT ATTACHED AS ICS
  data
  {
  to                                    (String) EMAIL ADDRESS OF RECIPIENT
  subject                               (String) SUBJECT OF EMAIL
  template_name                         (String) NAME OF EMAIL TEMPLATE
  data                                  (Object) DATA TO CUSTOMIZE EMAIL TEMPLATE (CF. TEMPLATE FILE WITH BLAZE)
  eventName                             (String) NAME OF THE EVENT ATTACHED
  start                                 (Date) START OF THE EVENT ATTACHED
  end                                   (Date) START OF THE EVENT ATTACHED
  location                              (String) LOCATION OF THE EVENT ATTACHED
  }
  */
  sendEmailEvent({ to, condoId, eventName, subject, template_name, data, start, end, location }) {
    check([to, subject, template_name], [String]);
    this.unblock();

    if (Isemail.validate(to) !== true) {
      console.log('wrong email address ---- ', to)
      return;
    }

    if (_.isNumber(start)) {
      start = moment(start)
    }

    if (_.isNumber(end)) {
      end = moment(end)
    }

    // create a new moment based on the original one
    let startFix = start.clone();
    let endFix = end.clone();

    // change the time zone of the new moment
    startFix.tz('Europe/Paris'); // or whatever time zone you desire
    endFix.tz('Europe/Paris'); // or whatever time zone you desire

    // shift the moment by the difference in offsets
    startFix.add(start.utcOffset() - startFix.utcOffset(), 'minutes');
    endFix.add(end.utcOffset() - endFix.utcOffset(), 'minutes');

    let cal = ical({});
    cal.events([
      {
        summary: eventName,
        organizer: {
          name: "MonBuilding",
          email: 'bonjour@monbuilding.com'
        },
        location: location,
        start: startFix.toDate(),
        end: endFix.toDate()
      }
    ]);

    // generate ics file
    let domain = Meteor.absoluteUrl('', condoId);
    let path = 'assets/app/uploads/';
    let filename = moment().format('x') + '_' + startFix.format('x') + '.ics';
    cal.save(path + filename);

    data.outlookLink = domain + 'download-ics/' + filename;
    data.icalLink = domain + 'download-ics/' + filename;

    let generateUrl = require('generate-google-calendar-url');
    data.googleLink = generateUrl({
      start: startFix.toDate(),
      end: endFix.toDate(),
      title: eventName,
      location: location,
      details: ''
    });
    data.domain = domain;

    let from = 'MonBuilding <bonjour@monbuilding.com>'
    let custom = Condos.findOne({ _id: condoId })
    if (custom && custom.settings && custom.settings.options && custom.settings.options.customEmail === true && !!custom.settings.customEmail) {
      from = custom.settings.customEmail + ' <bonjour@monbuilding.com>'
    }

    data.condoId = condoId;

    // send email
    const html = getEmailHtml(template_name, data);
    if (!__TESTLIKEPROD__ && (Meteor.isDevelopment || process.env.ROOT_URL != "https://www.monbuilding.com/")) {
        subject = "[DEV] - [@"+to+"] " + subject;
        to = "dev.monbuilding@gmail.com";
    }

    Meteor.defer(function () {
      var data = {
        from: from,
        to: to,
        subject: subject,
        html: html,
        icalEvent: cal.toString(),
        attachments: [{
          fileName: "reservation.ics",
          contentType: "text/calendar",
          contents: cal.toString()
        }]
      }
      if (!__TESTLIKEPROD__ && (Meteor.isDevelopment || process.env.ROOT_URL != "https://www.monbuilding.com/")) {
        try {
          console.log('to', to)
          console.log('template_name', template_name)
          Email.send(data)
        } catch (e) {
          if (!!e && !!e.response) {
            console.log(e.response)
          } else {
            console.log("Fail sending Email")
          }
        }
      } else {
        mailgun.messages().send(data, function (err, body) {
          if (err) {
            console.log("got an error: ", err);
          } else {
            console.log('to', to)
            console.log('template_name', template_name)

            console.log(body);
          }
        });
      }
    })
  }
});
