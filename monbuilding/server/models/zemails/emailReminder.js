import { SyncedCron } from 'meteor/percolate:synced-cron';

EmailReminder = new Mongo.Collection("EmailReminder");

SyncedCron.config({
  log: false,
  collectionTTL: 10
});

const SENDING_REMIND_EMAILS_INTERVAL = 'every 1 minute';

function getNextDate(date, hours) {
  // ADD HOURS TO LAST DATE
  let d = new Date(date);
  d.setTime(d.getTime() + hours * 3600000); // 3600000

  // CHECK FOR WEEKEND
  let day = d.getDay();
  if (day === 6)
    return getNextDate(d, 48);
  if (day === 0)
    return getNextDate(d, 24);
  return d;
}

const rightsToEmailTemplate = {
  reminderDeclare: ['incident', 'see'],
  reminderValid: ['incident', 'see'],
  'reminder-message-fr': ['messenger', "seeAllMsgEnterprise"],
  'reminder-message-en': ['messenger', "seeAllMsgEnterprise"]
}

Meteor.startup(function() {
    Meteor.methods({
        /* CREATE A NEW EMAIL REMINDER
          data
           {
             to                 (Table of String) EMAIL ADDRESSES OF USERS TO SEND EMAIL
             emailTemplate      (String) NAME OF EMAIL TEMPLATE TO SEND
             emailData: {       (Object) DATA THAT WILL BE DISPLAYED IN EMAIL
               message          (String) MAIN MESSAGE IN EMAIL BODY
               module           (String) NAME OF MODULE RELATED TO THE EMAIL (WILL BE USED TO CHECK IF EMAIL IS NEEDED)
               from             (String) NAME OF ORIGIN
               setReplyLink     (String) LINK ON BUTTON IN EMAIL
             },
             remindHours        (Integer) NUMBER OF HOURS BETWEEN 2 EMAILS
             subject            (String) SUBJECT OF EMAILS
           }
           start                (Date) START DATE OF REMINDER
        */
      programEmailReminder: function(data, start) {
      	if (!data.remindHours || data.remindHours == undefined || data.remindHours == NaN)
          data.remindHours = 24;
        data.remindHours = +data.remindHours
        if (!Match.test(data, {
            "to": [String],
            "condoId": String,
            "emailTemplate": String,
            "emailData": Object,
            "remindHours": Number,
            "subject": String }) ||
          !Match.test(start, Match.Maybe(Date))) {
          throw new Meteor.Error(401, "Invalid parameters zemail programEmailReminder");
        }
        let d = start || new Date();
        let d2 = getNextDate(d, data.remindHours);
        let dest = _.filter(_.map(data.to, function(userId) {
        	let userEmail = Meteor.users.findOne(userId);
          const emailTemplate = rightsToEmailTemplate[data.emailTemplate]
        	if (userEmail && userEmail.emails && userEmail.emails[0].address && Meteor.userHasRight(emailTemplate[0], emailTemplate[1], data.condoId, userId))
        		return userEmail.emails[0].address
        	return "";
        }), function(userEmailList) {
        	if (userEmailList != "")
        		return true;
        });
        return EmailReminder.insert({
          "start": d,
          "subject": data.subject,
          "emailTemplate": data.emailTemplate,
          "emailData": data.emailData,
          "to": dest,
          "condoId": data.condoId,
          "remindHours": data.remindHours,
          "nextSend": d2,
          "nbSend": 0,
        });
      },
        // REMOVE AN EXISTING EMAIL REMINDER
        // id                 (String) ID OF EMAIL REMINDER TO REMOVE
      removeEmailReminder: function(id) {
        if (!Match.test(id, String))
          throw new Meteor.Error(401, "Invalid parameters email removeEmailReminder");
        EmailReminder.remove(id);
      },
        /* EDIT AN EXISTING EMAIL REMINDER
           id                   (String) ID OF EMAIL REMINDER TO EDIT
           data                 (Object) DATA TO EDIT EXISTING EMAIL REMINDER
           {
             to                 (Table of String) EMAIL ADDRESSES OF USERS TO SEND EMAIL
             emailTemplate      (String) NAME OF EMAIL TEMPLATE TO SEND
             emailData: {       (Object) DATA THAT WILL BE DISPLAYED IN EMAIL
               message          (String) MAIN MESSAGE IN EMAIL BODY
               module           (String) NAME OF MODULE RELATED TO THE EMAIL (WILL BE USED TO CHECK IF EMAIL IS NEEDED)
               from             (String) NAME OF ORIGIN
               setReplyLink     (String) LINK ON BUTTON IN EMAIL
             },
             remindHours        (Integer) NUMBER OF HOURS BETWEEN 2 EMAILS
             subject            (String) SUBJECT OF EMAILS
           }
           start                (Date) START DATE OF REMINDER
       */
      updateEmailReminder: function(id, data, start) {
        if (!Match.test(id, String) ||
            !Match.test(data, {
              "to": Match.Maybe([String]),
              "condoId": Match.Maybe(String),
              "emailTemplate": Match.Maybe(String),
              "emailData": Match.Maybe(Object),
              "remindHours": Match.Maybe(Number),
              "subject": Match.Maybe(String),
            }) ||
            !Match.test(start, Match.Maybe(Date)))
          throw new Meteor.Error(401, "Invalid parameters email updateEmailReminder");
        let mail = EmailReminder.findOne(id);
        if (!mail)
          throw new Meteor.Error(601, "Invalid id", "Cannot find email reminder");
        let up = {$set: {}};
        if (data.to)
          up.$set.to = data.to;
        if (data.condoId)
          up.$set.condoId = data.condoId;
        if (data.emailTemplate)
          up.$set.emailTemplate = data.emailTemplate;
        if (data.emailData)
          up.$set.emailData = data.emailData;
        if (data.remindHours)
          up.$set.remindHours = data.remindHours;
        if (data.subject)
          up.$set.subject = data.subject
        let nextSend = mail.nextSend;
        if (start)
          nextSend = start;
        if (data.remindHours || start) {
          up.$set.nextSend = getNextDate(nextSend, data.remindHours);
        }
        EmailReminder.update(id, up);
      }
    });
});

// CRON ROBOT WHICH CHECK IF ANY EMAIL HAS TO BE SENT, DEPENDING ON SENDING_REMIND_EMAILS_INTERVAL VALUE
SyncedCron.add({
  persist: false,
  name: 'Sending remind emails',
  schedule(parser) {
    return parser.text(SENDING_REMIND_EMAILS_INTERVAL);
  },
  job() {
    // console.log('send remind')
    SyncedCron._collection.remove({})
    EmailReminder.find({
      nextSend: {$lt: new Date()}
    },
    {sort: {nextSend: 1}}).forEach((email) => {
      const bindHandler = handleSendOneMail.bind(undefined, email);
      const wMeteorHandler = Meteor.bindEnvironment(bindHandler);
      Meteor.defer(wMeteorHandler);

      let nbSend = email.nbSend + 1;
      let nextSend = getNextDate(email.nextSend, email.remindHours);
      EmailReminder.update(email._id,
        {$set: {nbSend: nbSend, nextSend: nextSend}});
    });
  }
});

/* SEND EMAIL FROM CRONTASK
  email
  {
    to                        (Table of String) EMAIL ADDRESSES OF RECIPIENTS
    subject                   (String) SUBJECT OF EMAIL
    emailTemplate             (String) NAME OF EMAIL TEMPLATE (DECLARED IN SSR)
    emailData                 (Object) DATA TO FILL THE TEMPLATE
  }
 */
function handleSendOneMail(email) {
  for (dest of email.to) {
    const userDest = Accounts.findUserByEmail(dest)
    const emailTemplate = rightsToEmailTemplate[email.emailTemplate]
    if (userDest && Meteor.userHasRight(emailTemplate[0], emailTemplate[1], email.condoId, userDest._id)) {
      Meteor.call('sendEmailSync',
      dest,
      email.condoId,
      email.subject,
      email.emailTemplate,
      email.emailData)
    } else {
      // remove reminder
    }
  }
}

SyncedCron.start();
