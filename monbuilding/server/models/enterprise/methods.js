Meteor.startup(function () {
  Meteor.methods({
    ServerScriptUpdateEnterpriseHistory: () => {
      EnterpriseHistoryAction.upsert({
        "key": "privateIncident",
      },
        {
          "key": "privateIncident",
          "value-fr": "Incident privé",
          "value-en": "Private incident",
        }
      );
      console.log("EnterpriseHistoryAction => privateIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "solvedIncident",
      },
        {
          "key": "solvedIncident",
          "value-fr": "Incident résolu",
          "value-en": "Solved incident",
        }
      );
      console.log("EnterpriseHistoryAction => solvedIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "reopenedIncident",
      },
        {
          "key": "reopenedIncident",
          "value-fr": "Incident réouvert",
          "value-en": "Reopened incident",
        }
      );
      console.log("EnterpriseHistoryAction => reopenedIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "commentIncident",
      },
        {
          "key": "commentIncident",
          "value-fr": "Incident commentaire ajouté",
          "value-en": "Incident new comment",
        }
      );
      console.log("EnterpriseHistoryAction => commentIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "quoteIncident",
      },
        {
          "key": "quoteIncident",
          "value-fr": "Incident devis ajouté",
          "value-en": "Incident new quotation",
        }
      );
      console.log("EnterpriseHistoryAction => quoteIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "osIncident",
      },
        {
          "key": "osIncident",
          "value-fr": "Incident OS envoyé",
          "value-en": "Incident new SO sent",
        }
      );
      console.log("EnterpriseHistoryAction => osIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "programmInterventionIncident",
      },
        {
          "key": "programmInterventionIncident",
          "value-fr": "Incident intervention programmée",
          "value-en": "Incident new programmed intervention",
        }
      );
      console.log("EnterpriseHistoryAction => programmInterventionIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "canceledInterventionIncident",
      },
        {
          "key": "canceledInterventionIncident",
          "value-fr": "Incident intervention annulé",
          "value-en": "Incident canceled intervention",
        }
      );
      console.log("EnterpriseHistoryAction => canceledInterventionIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "declaredIncident",
      },
        {
          "key": "declaredIncident",
          "value-fr": "Incident déclaré",
          "value-en": "Declared incident",
        }
      );
      console.log("EnterpriseHistoryAction => declaredIncident inserted")
      EnterpriseHistoryAction.upsert({
        "key": "sentMessage",
      },
        {
          "key": "sentMessage",
          "value-fr": "Message envoyé",
          "value-en": "Sent message",
        }
      );
      console.log("EnterpriseHistoryAction => sentMessage inserted")
      EnterpriseHistoryAction.upsert({
        "key": "addEvent",
      },
        {
          "key": "addEvent",
          "value-fr": "Ajout d'un événement",
          "value-en": "Event added",
        }
      );
      console.log("EnterpriseHistoryAction => addEvent inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editEvent",
      },
        {
          "key": "editEvent",
          "value-fr": "Édition d'un événement",
          "value-en": "Event edited",
        }
      );
      console.log("EnterpriseHistoryAction => editEvent inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deleteEvent",
      },
        {
          "key": "deleteEvent",
          "value-fr": "Suppression d'un événement",
          "value-en": "Event deleted",
        }
      );
      console.log("EnterpriseHistoryAction => deleteEvent inserted")
      EnterpriseHistoryAction.upsert({
        "key": "addInfo",
      },
        {
          "key": "addInfo",
          "value-fr": "Ajout d'une information",
          "value-en": "Information added",
        }
      );
      console.log("EnterpriseHistoryAction => addInfo inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editInfo",
      },
        {
          "key": "editInfo",
          "value-fr": "Édition d'une information",
          "value-en": "Information edited",
        }
      );
      console.log("EnterpriseHistoryAction => editInfo inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deleteInfo",
      },
        {
          "key": "deleteInfo",
          "value-fr": "Suppression d'une information",
          "value-en": "Information deleted",
        }
      );
      console.log("EnterpriseHistoryAction => deleteInfo inserted")
      EnterpriseHistoryAction.upsert({
        "key": "newManual",
      },
        {
          "key": "newManual",
          "value-fr": "Nouveau manuel",
          "value-en": "New manual",
        }
      );
      console.log("EnterpriseHistoryAction => newManual inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editManual",
      },
        {
          "key": "editManual",
          "value-fr": "Manuel édité",
          "value-en": "Manual edited",
        }
      );
      console.log("EnterpriseHistoryAction => editManual inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deleteManual",
      },
          {
              "key": "deleteManual",
              "value-fr": "Manuel supprimé",
              "value-en": "Manual deleted",
          }
      );
      console.log("EnterpriseHistoryAction => deleteManual inserted")
      EnterpriseHistoryAction.upsert({
        "key": "newMap",
      },
        {
          "key": "newMap",
          "value-fr": "Nouveau plan",
          "value-en": "New plan",
        }
      );
      console.log("EnterpriseHistoryAction => newMap inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editMap",
      },
        {
          "key": "editMap",
          "value-fr": "Plan édité",
          "value-en": "Plan edited",
        }
      );
      console.log("EnterpriseHistoryAction => editMap inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deleteMap",
      },
          {
              "key": "deleteMap",
              "value-fr": "Plan supprimé",
              "value-en": "Plan deleted",
          }
      );
      console.log("EnterpriseHistoryAction => deleteMap inserted")
      EnterpriseHistoryAction.upsert({
        "key": "adCreated",
      },
        {
            "key": "adCreated",
            "value-fr": "Petite annonce publiée",
            "value-en": "Ad published",
        }
      );
      console.log("EnterpriseHistoryAction => adCreated inserted")
      EnterpriseHistoryAction.upsert({
        "key": "forumPost",
      },
        {
            "key": "forumPost",
            "value-fr": "Post/Sondage publié",
            "value-en": "Post/Poll published",
        }
      );
      console.log("EnterpriseHistoryAction => forumPost inserted")
      EnterpriseHistoryAction.upsert({
        "key": "newResa",
      },
        {
            "key": "newResa",
            "value-fr": "Nouvelle réservation soumise à validation",
            "value-en": "New reservation subject to validation",
        }
      );
      console.log("EnterpriseHistoryAction => newResa inserted")
      EnterpriseHistoryAction.upsert({
        "key": "acceptedResa",
      },
        {
            "key": "acceptedResa",
            "value-fr": "Réservation soumise à validation acceptée",
            "value-en": "Reservation subject to validation accepted",
        }
      );
      console.log("EnterpriseHistoryAction => acceptedResa inserted")
      EnterpriseHistoryAction.upsert({
        "key": "rejectedResa",
      },
        {
            "key": "rejectedResa",
            "value-fr": "Réservation soumise à validation rejetée",
            "value-en": "Reservation subject to validation rejected",
        }
      );
      console.log("EnterpriseHistoryAction => rejectedResa inserted")
      EnterpriseHistoryAction.upsert({
        "key": "newResource",
      },
        {
            "key": "newResource",
            "value-fr": "Ressource ajoutée",
            "value-en": "Resource added",
        }
      );
      console.log("EnterpriseHistoryAction => newResource inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deletedResource",
      },
        {
            "key": "deletedResource",
            "value-fr": "Ressource supprimée",
            "value-en": "Resource deleted",
        }
      );
      console.log("EnterpriseHistoryAction => deletedResource inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editResource",
      },
        {
            "key": "editResource",
            "value-fr": "Ressource modifiée",
            "value-en": "Resource modified",
        }
      );
      console.log("EnterpriseHistoryAction => editResource inserted")
      EnterpriseHistoryAction.upsert({
        "key": "editResource",
      },
        {
            "key": "editResource",
            "value-fr": "Ressource modifiée",
            "value-en": "Resource modified",
        }
      );
      console.log("EnterpriseHistoryAction => editResource inserted")
      // OCCUPANT - MANAGER
      EnterpriseHistoryAction.upsert({
        "key": "invitedOccupant",
      },
        {
            "key": "invitedOccupant",
            "value-fr": "Occupant invité",
            "value-en": "Occupant added",
        }
      );
      console.log("EnterpriseHistoryAction => invitedOccupant inserted")
      EnterpriseHistoryAction.upsert({
        "key": "acceptedOccupant",
      },
        {
            "key": "acceptedOccupant",
            "value-fr": "Occupant accepté",
            "value-en": "Occupant accepted",
        }
      );
      console.log("EnterpriseHistoryAction => acceptedOccupant inserted")
      EnterpriseHistoryAction.upsert({
        "key": "rejectedOccupant",
      },
        {
            "key": "rejectedOccupant",
            "value-fr": "Occupant rejeté",
            "value-en": "Occupant rejected",
        }
      );
      console.log("EnterpriseHistoryAction => rejectedOccupant inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deletedOccupant",
      },
        {
            "key": "deletedOccupant",
            "value-fr": "Occupant supprimé",
            "value-en": "Occupant deleted",
        }
      );
      console.log("EnterpriseHistoryAction => rejectedOccupant inserted")
      EnterpriseHistoryAction.upsert({
        "key": "invitedManager",
      },
        {
            "key": "invitedManager",
            "value-fr": "Gestionnaire invité",
            "value-en": "Manager invited",
        }
      );
      console.log("EnterpriseHistoryAction => invitedManager inserted")
      EnterpriseHistoryAction.upsert({
        "key": "deletedManager",
      },
        {
            "key": "deletedManager",
            "value-fr": "Gestionnaire supprimé",
            "value-en": "Manager deleted",
        }
      );
      console.log("EnterpriseHistoryAction => deletedManager inserted")
      // CONTACT
      EnterpriseHistoryAction.upsert({
        "key": "addedContact",
      },
        {
            "key": "addedContact",
            "value-fr": "a été ajouté(e) en tant que gestionnaire en charge de",
            "value-en": "has been added as the manager in charge of",
        }
      );
      console.log("EnterpriseHistoryAction => addedContact inserted")
      EnterpriseHistoryAction.upsert({
        "key": "removedContact",
      },
        {
            "key": "removedContact",
            "value-fr": "a été retiré(e) en tant que gestionnaire en charge de",
            "value-en": "has been removed as the manager in charge of",
        }
      );
      console.log("EnterpriseHistoryAction => removedContact inserted")
      EnterpriseHistoryAction.upsert({
        "key": "addedEmergencyContact",
      },
        {
            "key": "addedEmergencyContact",
            "value-fr": "a été ajouté(e) en tant que contact d'urgence",
            "value-en": "has been added as an emergency contact",
        }
      );
      console.log("EnterpriseHistoryAction => addedEmergencyContact inserted")
      EnterpriseHistoryAction.upsert({
        "key": "removedEmergencyContact",
      },
        {
            "key": "removedEmergencyContact",
            "value-fr": "a été retiré(e) en tant que contact d'urgence",
            "value-en": "has been removed as an emergency contact",
        }
      );
      console.log("EnterpriseHistoryAction => removedEmergencyContact inserted")
    }
  })
})