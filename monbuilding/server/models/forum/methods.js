import moment from 'moment';
import './emails.js'
import { check, Match } from 'meteor/check'

Meteor.startup(function () {
	Meteor.methods({
		// item contain: condoId / subject / date / filesId / type / views / categoriesId
		// if poll: inputs / endDate
		forumNewPost: function(item) {
			if (!Meteor.userId())
      throw new Meteor.Error(300, "Not Authenticated");
			if (item.type === 'post' && Meteor.userHasRight("forum", "writeSubject", item.condoId, Meteor.userId()) !== true) {
				throw new Meteor.Error(403, "Forbidden do delete post");
			}
			if (item.type === 'poll' && Meteor.userHasRight("forum", "writePoll", item.condoId, Meteor.userId()) !== true) {
				throw new Meteor.Error(403, "Forbidden do delete post");
      }
      const userId = Meteor.userId();

      let attachments = []
      if (Array.isArray(item.filesId) && item.filesId.length > 0) {
        const newFilesId = item.filesId.filter(file => typeof file !== 'string');
        const filesId = item.filesId.filter(file => typeof file === 'string');
        const fileDatas = ForumFiles.find({_id: {$in: filesId}});
        attachments = [...newFilesId, ...fileDatas.map(fileData => ({
          fileId: fileData._id,
          mime: fileData.type,
          ext: fileData.extension
        }))]
      }

			let postId = ForumPosts.insert({
        // NEW MESSAGE DATA
        createdBy: userId,
        participants: [userId],
        userUpdateView: [userId],
        userViews: [userId],
        createdAt: Date.now(),
        attachments,
        // ////////////////
        inputs: item.inputs,
        endDate: item.endDate,
        userId: userId,
        updatedAt: Date.now(),
        editedAt: null,
        condoId: item.condoId,
        subject: item.subject,
        date: Date.now(),
        filesId: item.filesId,
        type: item.type,
        views: item.views,
        categoriesId: item.categoriesId,
        isEditable: !!item.isEditable
      });
			Meteor.call('addView', {condoId: item.condoId, postId: postId})
			sendForumEmail(item.condoId, postId, item.userId, 'new-post-forum-', 'new_mess_forum', 'new_subject_published', item.subject);
			let rappelDate = null;
			if (item.endDate) {
				let timediff = moment(item.endDate).diff(moment(), 'days');
				if (timediff > 1) {
					let rplDate = moment(item.endDate);
					rplDate.add(-1, 'days');
					rplDate.add(1, 'hours');
					rappelDate = new Date(parseInt(rplDate.format('x')));
				}
			}
			if (rappelDate !== null) {
				let users = [...Meteor.call('getResidentsOfCondo', item.condoId), ...Meteor.call('getGestionnaireOfCondoInCharge', item.condoId)]
				OneSignal.MbNotification.sendToUserIds(users, {
					type: 'forum',
					key: 'poll_expire',
					dataId: postId,
					condoId: item.condoId,
					data: {
						typePost: 'poll'
					}
				}, rappelDate, (result, msg) => {
					if (!result) {
						console.log('Error: ', msg);
						return false;
					}
					if (result) {
						ReminderNotificationsId.insert({
							postId: postId,
							pushId: result
						})
					}
				})
      }
      Meteor.call('addToGestionnaireHistory',
        'forum',
        postId,
        'forumPost',
        item.condoId,
        item.subject,
        Meteor.userId()
      );
			return postId
		},
		// item contain : condoId / postId
		addView: function(item) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");

			let userId = Meteor.userId();
      let elem = "views." + userId;

      ForumPosts.update({
        _id : item.postId,
        userViews: {$not: { $elemMatch: { $eq: userId } } }
      },{
        $addToSet: { userViews: userId },
        $inc: {viewsCount: 1}
      });
      ForumPosts.update({_id : item.postId},{
          $set: { [elem] : Date.now()},
          $addToSet: { userUpdateView: userId }
      });
    },
    markForumPostsAsRead: function (condoIds) {
      check(condoIds, [String])
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");

      let elem = "views." + userId;
      ForumPosts.update({condoId: {$in: condoIds}},
        {
          $set: {[elem]: Date.now()},
          $addToSet: {
            userUpdateView: userId,
            userViews: userId
          }
        },
        {
          multi: true
        }
      )
    },
		// item contain : condoId / postId / newEdit / isEditable (modified post) / categoriesId / filesId
		modifPost: function(item) {
      check(item, Match.ObjectIncluding({
        condoId: String,
        postId: String,
        newEdit: String,
        isEditable: Match.Maybe(Boolean),
        filesId: Array,
        categoriesId: Match.Maybe([String])
      }))
			if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");

      if (!item.isEditable) {
        item.isEditable = false
      }
      const userId = Meteor.userId();

      let attachments = []
      if (Array.isArray(item.filesId) && item.filesId.length > 0) {
        const newFilesId = item.filesId.filter(file => typeof file !== 'string');
        const filesId = item.filesId.filter(file => typeof file === 'string');
        const fileDatas = ForumFiles.find({_id: {$in: filesId}});
        attachments = [...newFilesId, ...fileDatas.map(fileData => ({
          fileId: fileData._id,
          mime: fileData.type,
          ext: fileData.extension
        }))]
      }
			if (item.filesId !== undefined) {
				ForumPosts.update(
					{ '_id': item.postId },
          {
            $set: {
              'subject': item.newEdit && item.newEdit.trim(),
              'editedAt': Date.now(),
              filesId: item.filesId,
              attachments,
              'isEditable': item.isEditable,
              categoriesId: item.categoriesId
            },

          }
				);
			}
			else {
				ForumPosts.update(
					{ '_id' : item.postId },
          {
            $set: {
              'subject': item.newEdit && item.newEdit.trim(),
              'editedAt': Date.now(),
              'isEditable': item.isEditable,
              categoriesId: item.categoriesId
            }
          }
				);
			}
			Meteor.call('addView', { condoId: item.condoId, postId: item.postId })
		},
		addVote: function(condoId, postId, idxVote) {
			if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }

			let userId = Meteor.userId()

      let elem = "votes." + userId
      const userHasAlreadyVote = !!ForumPosts.findOne({ _id: postId, [elem]: idxVote })
      if (userHasAlreadyVote) {
        ForumPosts.update(
          {_id: postId},
          {
            $unset: {[elem]: null}
          }
        )
      } else {
        ForumPosts.update({_id: postId},
          {
            $set: { [elem] : idxVote },
            $addToSet: { participants: userId}
          }
        )
      }
			Meteor.call('addView', { condoId: condoId, postId: postId })
		},
		// item contain : condoId / postId / filesId / comment / commentDate  / userId for the update forum script
		forumCommentPost: function(item) {
			// console.log(item)
			if (!Meteor.userId())
      throw new Meteor.Error(300, "Not Authenticated");
      const userId = Meteor.userId();
			item.userId = userId;
			let postId = ForumCommentPosts.insert(
				item
      );

			ForumPosts.update(
        { _id: item.postId },
        {
          $set: {
            'updatedAt': Date.now(),
            userUpdateView: [userId]
          },
          $addToSet: { participants: userId },
          $inc: {commentCount: 1}
        }
      );
			sendForumEmail(item.condoId, item.postId, item.userId, 'new-reply-forum-', 'new_mess_forum', 'new_com_published', item.comment);
			Meteor.call('addView', { condoId: item.condoId, postId: item.postId })
			return postId
		},
		// item contain : condoId / commentId / newEdit (modified comment)
		modifComment: function(item) {
			if (!Meteor.userId())
			throw new Meteor.Error(300, "Not Authenticated");

			let post = ForumCommentPosts.findOne({_id: item.commentId})
			if (post.userId === Meteor.userId()) {
				ForumCommentPosts.update(
					{ '_id' : item.commentId },
					{ $set: { 'comment': item.newEdit && item.newEdit.trim(), 'updatedAt' : Date.now(), 'filesId': item.filesId } }
				);
				let postId = post.postId
				Meteor.call('addView', { condoId: item.condoId, postId: postId })
			}
		},
		// item contain : condoId / postId
		followPost: function(item) {
			if (!Meteor.userId())
			throw new Meteor.Error(300, "Not Authenticated");

			item.userId = Meteor.userId();
			UnFollowForumPosts.upsert({
				'postId' : item.postId,
				'condoId' : item.condoId,
			}, {
				$unset: {
					'unfollowList' : item.userId
				}
			});
			Meteor.call('addView', { condoId: item.condoId, postId: item.postId })
		},
		// item contain: condoId / postId / userId for the update forum script
		unfollowPost: function(item) {
			if (!Meteor.userId())
			throw new Meteor.Error(300, "Not Authenticated");

			item.userId = Meteor.userId();
			UnFollowForumPosts.upsert({
				'postId' : item.postId,
				'condoId' : item.condoId,
			}, {
				$addToSet: {
					'unfollowList' : item.userId
				}
			});
			Meteor.call('addView', { condoId: item.condoId, postId: item.postId })
		},
		// item contain : condoId / postId
		toggleFollowPost: function (condoId, postId) {
			if (!Meteor.userId())
			throw new Meteor.Error(300, "Not Authenticated");

			let userId = Meteor.userId();
			let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId })
			if (!(!!follower && follower.unfollowList.find((f) => f == userId))) {
				UnFollowForumPosts.upsert({
					'postId': postId,
					'condoId': condoId,
				}, {
					$unset: {
						'unfollowList': userId
					}
				});
			} else {
				UnFollowForumPosts.upsert({
					'postId': postId,
					'condoId': condoId,
				}, {
					$addToSet: {
						'unfollowList': userId
					}
				});
			}
			Meteor.call('addView', { condoId: condoId, postId: postId })
		},
		//item contain: condoId / postId
		removePostPoll: function(item) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let post = ForumPosts.findOne({_id: item.postId});
			if (post.type === 'post' && Meteor.userId() !== post.userId && Meteor.userHasRight("forum", "deleteSubject", item.condoId, Meteor.userId()) !== true) {
				throw new Meteor.Error(403, "Forbidden do delete post");
			}
			if (post.type === 'poll' && Meteor.userId() !== post.userId && Meteor.userHasRight("forum", "deletePoll", item.condoId, Meteor.userId()) !== true) {
				throw new Meteor.Error(403, "Forbidden do delete poll");
			}
			let filesId = post.filesId

			ForumPosts.remove({_id: item.postId});
			ForumCommentPosts.remove({
				'postId': item.postId
			});
			ForumFiles.remove({
				'_id': {$in: filesId}
			})

			let reminderPush = ReminderNotificationsId.findOne({ postId: item.postId });
			if (reminderPush) {
				OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
					if (err) {
						console.log('Error: ', err);
						return false;
					}
				})
				ReminderNotificationsId.remove({ postId: item.postId })
			}
			Meteor.call('addView', { condoId: item.condoId, postId: item.postId })
			if (post.userId !== this.userId) {
				let right = (post.type === 'poll' ? 'seePoll' : 'seeSubject')
				sendForumEmailDelete(item.condoId, post.userId, right, post.subject);
			}
		},
		//item contain: condoId / commentId
		removeComment: function(item) {
			if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");

			let post = ForumCommentPosts.findOne(item.commentId)
			let postId = post.postId
			if (post.userId === Meteor.userId() || Meteor.userHasRight('forum', (post.type === 'post' ? 'deleteSubject' : 'deletePoll'), item.condoId, Meteor.userId())) {
				ForumCommentPosts.remove({
					'_id': item.commentId,
					'userId': post.userId
				});
				Meteor.call('addView', { condoId: item.condoId, postId: postId })
      }

      ForumPosts.update(
        { _id: postId },
        { $inc: {commentCount: -1} }
      );
		},
		getForumCommentCount(condoId, postId) {
			return ForumCommentPosts.find({postId: postId}).count()
    },
    addOptionInForumPoll: function (postId, condoId, text) {
      check(postId, String);
      check(condoId, String);
      check(text, String);

      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");

      let post = ForumPosts.findOne({ _id: postId })
      if (post.type !== 'poll') {
        throw new Meteor.Error(403, "This forum post is not a poll");
      }
      const userId = Meteor.userId();
      ForumPosts.update(
        { _id: postId },
        {
          $push: { 'inputs': text },
          $set: { ['votes.' + this.userId]: post.inputs.length },
          $addToSet: {participants: userId}
        }
      )
      Meteor.call('addView', { condoId: condoId, postId: postId })
    },
		toggleAllForumNotification: function(condoId) {
			if (!Meteor.userId()) {
				throw new Meteor.Error(300, "Not Authenticated");
			}
			let userId = Meteor.userId()
			let resident = Residents.findOne({userId: userId});
			if (resident) {
				let condo = _.find(resident.condos, (condo) => {return condo.condoId === condoId});
				if (!condo || !condo.notifications) {
					throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
				}
				if (condo.notifications.forum_forum == true) {
					let postIds = _.map(ForumPosts.find({condoId}).fetch(), '_id');
					if (postIds && postIds.length > 0) {
						_.each(postIds, (postId) => {
							let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId });
							if (!(follower && follower.unfollowList && follower.unfollowList.find((f) => f == userId))) {
								UnFollowForumPosts.upsert({
									'postId': postId,
									'condoId': condoId,
								}, {
									$addToSet: {
										'unfollowList': userId
									}
								});
							}
						})
					}
					condo.notifications.forum_forum = false;
				} else {
					let postIds = _.map(ForumPosts.find({condoId}).fetch(), '_id');
					if (postIds && postIds.length > 0) {
						_.each(postIds, (postId) => {
							let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId });
							if (follower && follower.unfollowList && follower.unfollowList.find((f) => f == userId)) {
								UnFollowForumPosts.upsert({
									'postId': postId,
									'condoId': condoId,
								}, {
									$unset: {
										'unfollowList': userId
									}
								});
							}
						})
					}
					condo.notifications.forum_forum = true;
				}
				Residents.update({
					userId: userId,
					'condos.condoId': condoId
				},
				{
					$set: {
						'condos.$.notifications.forum_forum': condo.notifications.forum_forum
					}
				});
			} else {
				let gestionnaire = Enterprises.findOne({"users.userId": userId});
				if (!gestionnaire) {
					throw new Meteor.Error(300, "Enterprise not found");
				}
				let user = gestionnaire.users.find((u) => {return u.userId == userId});
				if (!user) {
					throw new Meteor.Error(300, "User not found");
				}
				let condoInCharge = user.condosInCharge.find((c) => { return c.condoId == condoId });
				if (!user) {
					throw new Meteor.Error(300, "Condos In Charge not found");
				}
				if (condoInCharge.notifications.forum_forum == undefined) {
					condoInCharge.notifications.forum_forum = false;
				}
				if (condoInCharge.notifications.forum_forum == true) {
					let postIds = _.map(ForumPosts.find({condoId}).fetch(), '_id');
					if (postIds && postIds.length > 0) {
						_.each(postIds, (postId) => {
							let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId });
							if (!(follower && follower.unfollowList && follower.unfollowList.find((f) => f == userId))) {
								UnFollowForumPosts.upsert({
									'postId': postId,
									'condoId': condoId,
								}, {
									$addToSet: {
										'unfollowList': userId
									}
								});
							}
						})
					}
					condoInCharge.notifications.forum_forum = false;
				} else {
					let postIds = _.map(ForumPosts.find({condoId}).fetch(), '_id');
					if (postIds && postIds.length > 0) {
						_.each(postIds, (postId) => {
							let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId });
							if (follower && follower.unfollowList && follower.unfollowList.find((f) => f == userId)) {
								UnFollowForumPosts.upsert({
									'postId': postId,
									'condoId': condoId,
								}, {
									$unset: {
										'unfollowList': userId
									}
								});
							}
						})
					}
					condoInCharge.notifications.forum_forum = true;
				}
				_.each(user.condosInCharge, function(elem) {
					if (elem.condoId === condoId) {
						elem = condoInCharge;
					}
				});
				Enterprises.update({
					'_id': gestionnaire._id,
					'users.userId': userId
				},
				{
					$set: {
						'users.$.condosInCharge': user.condosInCharge
					}
				});
			}
		}
	});
});
