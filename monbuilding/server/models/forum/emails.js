import { Email, DeletePost } from './../../../common/lang/lang';
import moment from 'moment';

if (Meteor.isServer) {
    sendForumEmailDelete = async function(condoId, userId, right, content) {
        content = (content.length > 50 ? (content.substr(0, 50) + '...') : content)
        let user = Meteor.users.findOne({_id: userId});
        let building = Condos.findOne({_id: condoId}).getNameWithId()
        if (user) {
            let lang = user.profile.lang || 'fr';
            let translation = new DeletePost(lang);
            if (Meteor.userHasRight("forum", right, condoId, userId)) {
                Meteor.call('sendEmailSync',
                    user.emails[0].address,
                    condoId,
                    translation.deletePost['subject'],
                    'delete-post-' + lang,
                    {
                      building: building,
                        category: translation.deletePost["forum"],
                        title: content,
                    }
                );
            }
        }
    }

    sendForumEmail = async function(condoId, postId, userId, emailTemplatePrefix, subjectTranslation, titleTranslation, content) {
        content = (content.length > 50 ? (content.substr(0, 50) + '...') : content)
        let tmpCondo = Condos.findOne(condoId);
        let condoName = tmpCondo.getName()
        let date = new Date();
        let follower = UnFollowForumPosts.findOne({ postId: postId, condoId: condoId })
        date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
        let post = ForumPosts.findOne({_id: postId});
        if (!post) return false
        let pushNotificationUserIds = []

        function getListUsersBeforeSend(post, condoId) {
            let users = [...Meteor.call('getResidentsOfCondo', condoId), ...Meteor.call('getGestionnaireOfCondoInCharge', condoId)]
            users = users.filter((userId) => {
                if (userId === Meteor.userId()) {
                    return false
                }
                let thisUserView = _.find(post.views, (date, viewUserId) => {
                    return viewUserId === userId
                })
                let commentCounter = ForumCommentPosts.find({
                    postId: post._id
                }).count();
                if (post.userId === userId) {
                    return true
                } else if (!!thisUserView === false && commentCounter == 0) {
                    return true
                } else {
                    let replyOfUser = ForumCommentPosts.findOne({
                        postId: post._id,
                        userId: userId
                    }, { fields: { _id: true } })
                    if (!!replyOfUser) {
                        return true
                    }
                }
                return false
            })
            return users
        }

        let userIds = getListUsersBeforeSend(post, condoId);
        userIds.forEach((id) => {
            if (id !== Meteor.userId()) {
                let user = Meteor.users.findOne({ _id: id })
                if (!user) return
                var lang = user.profile.lang || 'fr';
                var translation = new Email(lang);
                if (!(!!follower && !!follower.unfollowList && follower.unfollowList.find((f) => f == id))) {
                    if (Meteor.userHasRight("forum", 'seeSubject', condoId, id) && Meteor.userHasRight("forum", post.type === 'post' ? "seeSubject" : 'seePoll', condoId, id)) {
                        pushNotificationUserIds.push(id);
                        Meteor.call('checkNotifResident', id, condoId, 'forum_forum', function (error, result) {
                            if (result) {
                                Meteor.call('scheduleAnEmail', {
                                    templateName: emailTemplatePrefix + lang,
                                    to: user.emails[0].address,
                                    condoId: condoId,
                                    subject: translation.email[subjectTranslation],
                                    data: {
                                        module: "forum",
                                        title: translation.email[titleTranslation],
                                        condo: condoName,
                                        condoId: condoId,
                                        notifUrl: Meteor.absoluteUrl(`${lang}/resident/${condoId}/profile/notifications`, condoId),
                                        sujet: content,
                                        setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/forum/${postId}`, condoId),
                                    },
                                    dueTime: date,
                                    condition: {
                                        name: "checkForumBeforeSend",
                                        data: { postId: postId, userId: id, condoId: condoId, createdAt: Date.now() }
                                    }
                                });
                            }
                        });
                        Meteor.call('checkModuleGestionnaire', id, condoId, function (error, result) {
                            if (result) {
                                Meteor.call('checkNotifGestionnaire', id, condoId, 'forum_forum', function (error, result) {
                                    if (result) {
                                        Meteor.call('scheduleAnEmail', {
                                            templateName: emailTemplatePrefix + lang,
                                            to: user.emails[0].address,
                                            condoId: condoId,
                                            subject: translation.email[subjectTranslation],
                                            data: {
                                                module: "forum",
                                                title: translation.email[titleTranslation],
                                                condo: condoName,
                                                condoId: condoId,
                                                notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condoId),
                                                sujet: content,
                                                setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/forum/${condoId}/${postId}`, condoId)
                                            },
                                            dueTime: date,
                                            condition: {
                                                name: "checkForumBeforeSend",
                                                data: { postId: postId, userId: id, condoId: condoId, createdAt: Date.now() }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            }
        })
        OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
            type: 'forum',
            key: emailTemplatePrefix === 'new-post-forum-' ? 'new_post' : 'new_comment',
            dataId: postId,
            condoId: condoId,
            data: {
                typePost: post.type === 'post' ? 'post' : 'poll'
            }
        })
    }
}
