import { Meteor } from 'meteor/meteor'

Meteor.startup(function () {
  Meteor.publish('managerStatsForumPosts', function () {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }
    if (Meteor.call('isAdmin', this.userId) !== true && Meteor.call('isGestionnaire', this.userId) !== true) {
      throw new Meteor.Error(300, 'Not Authenticated as admin')
    }
    let self = this
    let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId')
    let handlers = []
    if (condoIds) {
      let feed = ForumPosts.find({condoId: { $in: condoIds}})
      handlers.push(feed.observe({
        added: function (document) {
          self.added('forumPosts', document._id, document)
        },
        changed: function (newDocument, oldDocument) {
          self.changed('forumPosts', oldDocument._id, newDocument)
        },
        removed: function (oldDocument) {
          self.removed('forumPosts', oldDocument._id)
        }
      }))
      let feedComment = ForumCommentPosts.find({condoId: {$in: condoIds}})
      handlers.push(feedComment.observe({
        added: function (document) {
          self.added('forumCommentPosts', document._id, document)
        },
        changed: function (newDocument, oldDocument) {
          self.changed('forumCommentPosts', oldDocument._id, newDocument)
        },
        removed: function (oldDocument) {
          self.removed('forumCommentPosts', oldDocument._id)
        }
      }))
      this.onStop(() => {
        handlers.forEach((handler) => {
          handler.stop()
        })
        return true
      })
      self.ready()
    }
  })

  Meteor.publish('fetchForumPostsAdmin', function () {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }
    if (Meteor.call('isAdmin', this.userId) !== true) {
      throw new Meteor.Error(300, 'Not Authenticated as admin')
    }
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let condoIds = _.map(Condos.find().fetch(), '_id')
    let self = this

    // let handlers = []
    let feed = ForumPosts.find({condoId: {$in: condoIds}})
    // handlers.push(feed.observe({
    //   added: function (document) {
    //     self.added('ForumPosts', document._id, document)
    //   },
    //   changed: function (newDocument, oldDocument) {
    //     self.changed('ForumPosts', oldDocument._id, newDocument)
    //   },
    //   removed: function (oldDocument) {
    //     self.removed('ForumPosts', oldDocument._id)
    //   }
    // }))
    let feedComments = ForumCommentPosts.find({condoId: {$in: condoIds}})

    // handlers.push(feedComments.observe({
    //   added: function (document) {
    //     self.added('forumCommentPosts', document._id, document)
    //   },
    //   changed: function (newDocument, oldDocument) {
    //     self.changed('forumCommentPosts', oldDocument._id, newDocument)
    //   },
    //   removed: function (oldDocument) {
    //     self.removed('forumCommentPosts', oldDocument._id)
    //   }
    // }));
    // this.onStop(() => {
    //   handlers.forEach(handler => {
    //     handler.stop()
    //   })
    //   return true
    // })
    self.ready()
    return [feed, feedComments, UserFiles.find({isForumFiles: true, condoId: {$in: condoIds}}).cursor]
  })

  Meteor.publish('fetchForumPosts', function (condoIds) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let self = this
    let handlers = []
    let feed = ForumPosts.find({condoId: {$in: condoIds}})
    // handlers.push(feed.observe({
    //   added: function (document) {
    //     self.added('ForumPosts', document._id, document)
    //   },
    //   changed: function (newDocument, oldDocument) {
    //     self.changed('ForumPosts', oldDocument._id, newDocument)
    //   },
    //   removed: function (oldDocument) {
    //     self.removed('ForumPosts', oldDocument._id)
    //   }
    // }));

    // this.onStop(() => {
    //   handlers.forEach(handler => {
    //     handler.stop()
    //   })
    // })
    return [feed, UserFiles.find({isForumFiles: true, condoId: {$in: condoIds}}).cursor]
  })

  Meteor.publish('forumFiles', function (condoIds) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }
    if (condoIds === undefined || condoIds === null || condoIds === 'all' || condoIds === '') {
      this.stop()
      return
    }
    if (!_.isArray(condoIds)) {
      condoIds = [condoIds]
    }
    return ForumFiles.find({'meta.condoId': {$in: condoIds}}).cursor
  })

  Meteor.publish('forumCommentPosts', function (condoId, postId) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let self = this
    if (!postId || postId === undefined) {
      self.stop()
      throw new Meteor.Error(404, 'Post not found')
    }
    let handler = {}
    let feed = ForumCommentPosts.find({postId: postId})

    handler = feed.observe({
      added: function (document) {
        self.added('forumCommentPosts', document._id, document)
      },
      changed: function (newDocument, oldDocument) {
        self.changed('forumCommentPosts', oldDocument._id, newDocument)
      },
      removed: function (oldDocument) {
        self.removed('forumCommentPosts', oldDocument._id)
      }
    })
    this.onStop(() => {
      return handler.stop()
    })
    return self.ready()
    // return UserFiles.find({'meta.forumPosts': forumPosts}).cursor
  })

  Meteor.publish('forumFeedAnswerCounter', function (condoIds) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let result = []
    let self = this
    let handlers = []
    let feed = ForumCommentPosts.find({condoId: {$in: condoIds}}, { fields: { postId: true } })
    handlers.push(feed.observe({
      added: function (document) {
        if (!result[document.postId]) {
          result[document.postId] = 1
          self.added('forumFeedCounter', document.postId, { _id: document.postId, counter: 1 })
        } else {
          result[document.postId] += 1
          self.changed('forumFeedCounter', document.postId, { _id: document.postId, counter: result[document.postId] })
        }
      },
      removed: function (oldDocument) {
        if (result[oldDocument.postId]) {
          result[oldDocument.postId] -= 1
          if (result[oldDocument.postId] < 1) {
            delete result[oldDocument.postId]
            self.removed('forumFeedCounter', oldDocument.postId)
          } else {
            self.changed('forumFeedCounter', oldDocument.postId, { _id: oldDocument.postId, counter: result[oldDocument.postId] })
          }
        }
      }
    }));
    this.onStop(() => {
      handlers.forEach(handler => {
        handler.stop()
      })
      return true
    })
    self.ready()
  })

  Meteor.publish('newForumPost', function (condoIds) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let self = this
    let userId = this.userId
    let handlers = []
    // let feed = ForumPosts.find({condoId: {$in: condoIds}}, { fields: { views: true, updatedAt: true } })
    let feed = ForumPosts.find({
      condoId: {$in: condoIds},
      $or: [
        {userViews: {$not: { $elemMatch: { $eq: userId } } }},
        {
          userUpdateView: {$not: { $elemMatch: { $eq: userId } } },
          participants: userId
        }
      ]
    }, {fields: {_id: 1}});

    handlers.push(feed.observe({
      added: function (document) {
        const postId = document._id
        self.added('forumNewPost', postId)
      },
      changed: function (newDocument) {
        const postId = newDocument._id
        self.added('forumNewPost', postId)
      },
      removed: function (oldDocument) {
        try {
          self.removed('forumNewPost', oldDocument._id)
        } catch (e) {
        }
      }
    }))
    this.onStop(() => {
      handlers.forEach(handler => {
        handler.stop()
      })
      return true
    })

    self.ready()
  })

  Meteor.publish('unFollowForumPosts', function () {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }

    let self = this
    let feed = UnFollowForumPosts.find({unfollowList: this.userId})
    return feed
  })

  Meteor.publish('forumCategories', function (condoId) {
    if (!this.userId) {
      throw new Meteor.Error(300, 'Not Authenticated')
    }
    let doc = ForumCategories.find(
      { condoId },
      { fields: {
        condoId: false
      } })
    if (!doc.count()) {
      doc = ForumCategories.find(
        { condoId: 'default' },
        { fields: {
          condoId: false
        } })
    }
    return doc
  })
})
