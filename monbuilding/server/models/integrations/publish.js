import { SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION } from "constants";
const integrations = [
  {
    _id: 'payZenIntegration',
    image: "/img/integrations/payzen.png",
    provider: "Payzen",
    description: "Allow online payment using credit card / bank account / SEPA.",
    service: "payment",
    type: "Full Integration",
    entity: "PayZen",
    section: "Service",
    brandDisplay: "Full",
    company: "PayZen",
    requiredFields: ['wsdl_url', 'siteId', 'testCert', 'prodCert']
  },
  {
    _id: 'assaAbloyIntegration',
    image: "/img/integrations/assa_abloy.jpg",
    brandDisplay: "Full",
    company: "Assa Abloy",
    description: "Access control for the building",
    entity: "Assa Abloy",
    provider: "Assa Abloy",
    section: "Service",
    service: "access control",
    type: "Full Integration"
  },
  {
    _id: 'activeDirectoryIntegration',
    image: "/img/integrations/active_directory.png",
    brandDisplay: "Full",
    company: "Microsoft",
    description: "Lightweight Directory Access Protocol",
    entity: "Microsoft",
    provider: "Microsoft",
    section: "Users management",
    service: "Active Directory",
    type: "Full Integration",
    requiredFields: ['url', 'baseDN', 'username', 'password']
  },
  {
    _id: 'wiLineIntegration',
    image: '/img/integrations/wiline.png',
    brandDisplay: 'Full',
    company: 'ec2e',
    description: 'Connect to your laundry and be alerted when cycle end',
    entity: 'ec2e',
    provider: 'ec2e',
    section: 'Service',
    service: 'Laundry',
    type: 'Full Integration',
    requiredFields: ['user', 'api_pass', 'central_code']
  },
  {
    _id: 'hfsqlKleyIntegration',
    image: '/img/integrations/kley.png',
    brandDisplay: 'Full',
    company: 'Kley',
    description: 'Import users from Kley HFSQL database',
    entity: 'Kley',
    provider: 'Kley',
    section: 'Users management',
    service: 'Import users from HFSQL',
    type: 'Full Integration',
    requiredFields: []
  },
  {
    _id: 'kyoceraIntegration',
    image: '/img/integrations/kyocera.png',
    brandDisplay: 'Full',
    company: 'Kyocera',
    description: 'Manage printers inside the building',
    entity: 'Kyocera',
    provider: 'Kyocera',
    section: 'Service',
    service: 'Printers',
    type: 'Full Integration',
    requiredFields: []
  },
  {
    _id: 'adIntegration',
    image: '',
    brandDisplay: 'Full',
    company: 'AD',
    description: 'Information handler from Active Directory',
    entity: 'AD',
    provider: 'AD',
    section: 'Service',
    service: 'Data',
    type: 'Full Integration',
    requiredFields: ['apiToken']
  }
]

Meteor.startup(function () {
  /* GLOBAL PUBLISH FOR RESIDENT BOARD (CONDOS, BUILDINGS, GESTIONNAIRE, RESIDENTUSERS FAKE TABLE)*/
  Meteor.publish('integrations', function () {
    if (Meteor.call('isAdmin', this.userId) === true) {
      // Generated virtual document with the list of integrations
      // const condos = Condos.find().fetch()
      for (const doc of integrations) {
        this.added('integrations', doc._id, {
          ...doc
        })
      }
      return [
        CondoIntegrationConfig.find(),
        IntegrationConfig.find()
      ]
    } else if (Meteor.call('isGestionnaire', this.userId) === true) {
      const allowedCondoIds = Meteor.listCondoUserHasRight('integrations', 'see')
      const config = CondoIntegrationConfig.find({ condoId: { $in: allowedCondoIds }})
      const map = config.fetch().reduce((map, c) => {
        map[c.integrationId] = true
        return map
      }, {})

      const filteredIntegrations = integrations.filter(i => {
        return map[i._id]
      })
      for (const doc of filteredIntegrations) {
        this.added('integrations', doc._id, {
          ...doc
        })
      }

      return config

    } else {
      throw new Meteor.Error(300, "Not Authenticated");
    }

  })

  Meteor.publish('integrationConfig', function (condoId, integrationId) {
    if (Meteor.call('isGestionnaire', this.userId) || Meteor.call('isAdmin', this.userId)) {
      return IntegrationConfig.find({ integrationId: integrationId, condoId: condoId })
    } else {
      return this.stop()
    }
  })








  Meteor.publish('terms_and_conditions', function() {
    return this.ready()
  })
});
