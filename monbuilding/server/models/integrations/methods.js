
import { Meteor } from 'meteor/meteor'
import {
  verifyActiveDirectoryIntegration,
  getActiveDirectoryData,
  prepareUsersData,
  createUsers
} from './active-directory/active-directory.service'

// import { mockData } from './active-directory/active-directory.mock'

Meteor.startup(function () {
  Meteor.methods({
    'integrations.verifyAD': ({ condoId }) => {
      return verifyActiveDirectoryIntegration({ condoId })
    },
    'integrations.refreshUsersFromAD': async ({ condoId, dateToSendEmail }) => {
      try {
        const usersDataFromAD = await getActiveDirectoryData({ condoId })
        // const usersDataFromAD = mockData
        const userData = prepareUsersData({
          condoId,
          users: usersDataFromAD
        })
        if (userData.length > 0) {
          userData.forEach(user => {
            createUsers({ user, dateToSendEmail })
          })
        }
      } catch (error) {
        throw new Meteor.Error(400, 'All the requirements to connect to Active Directory aren\'t met')
      }
    },
    'updateIntegration-config': function(configId, datas) {
      if (Meteor.call('isGestionnaire', this.userId) || Meteor.call('isAdmin', this.userId)) {
        check(configId, String)
        check(datas, [Match.ObjectIncluding({
          key: Match.OneOf(String, null),
          value: Match.OneOf(String, null)
        })])
        const query = {}
        datas.forEach(data => {
          query[data.key] = data.value
        })
        IntegrationConfig.update({ _id: configId }, {
          $set: query
        })
      }
    },
    'integration-set-active': function(condoId, integrationId, active) {
      if (Meteor.call('isAdmin', this.userId) !== true) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      const condoExist = Condos.find({_id: condoId}).limit(1).size() > 0;
      if (!condoExist) {
        throw new Meteor.Error(404, "Condo doesn't exist");
      }
      const enabled = CondoIntegrationConfig.update({integrationId, condoId}, {$set: {active}});
      if (enabled === 0)  {
        throw new Meteor.Error(404, "Integration doesn't exist");
      }
    },
    'integration-setup': function(condoId, integrationId, config) {
      if (Meteor.call('isAdmin', this.userId) !== true) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      const condoExist = Condos.find({_id: condoId}).count() > 0;
      if (!condoExist) {
        throw new Meteor.Error(404, "Condo doesn't exist");
      }
      check(config, {
        order: Match.Optional(Number),
        section: Match.Optional(String),
        config: Match.Optional(Object),
        active: Match.Optional(Boolean),
        comments: Match.Optional(String),
        contacts: Match.Optional([{
          name: String,
          role: String,
          email: String,
          phone: String
        }]),
      })
      const document = CondoIntegrationConfig.update({integrationId, condoId}, {
        $setOnInsert: {
          condoId,
          integrationId,
          activatedOn: new Date().getTime()
        },
        $set: {
          order: config.order,
          section: config.section,
          config: config.config,
          active: config.active,
          comments: config.comments,
          contacts: config.contacts
        }
      }, {upsert: true});

      Condos
        .update({ _id: condoId }, {
          $set: {
            [`integrations.${integrationId}`]: config.active
          }
        })

      if (!IntegrationConfig.findOne({ condoId: condoId, integrationId: integrationId })) {
        IntegrationConfig.insert({
          condoId, integrationId
        })
      }
      return document
    }
  })
})
