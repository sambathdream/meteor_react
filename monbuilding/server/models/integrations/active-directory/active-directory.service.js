import { Meteor } from 'meteor/meteor'
import ActiveDirectory from 'activedirectory'

const integrationId = 'activeDirectoryIntegration'

const verifyActiveDirectoryIntegration = ({ condoId }) => {
  const ic = IntegrationConfig.findOne({
    condoId,
    integrationId
  })
  return !!ic
}

const getActiveDirectoryData = ({ condoId }) => {
  return new Promise((resolve, reject) => {
    const it = IntegrationConfig.findOne({ condoId, integrationId })
    if (it) {
      const ad = new ActiveDirectory({
        url: 'ldap://' + it.url,
        baseDN: it.baseDN,
        username: it.username,
        password: it.password
      })
      const query = 'cn=*'

      ad.find(query, (err, results) => {
        if (err || !results) {
          reject(new Error(JSON.stringify(err)))
        } else {
          if (results.users) {
            resolve(results.users)
          } else {
            reject(new Error('Users not found in AD'))
          }
        }
      })
    } else {
      reject(new Error('Integration not found in MB'))
    }
  })
}

const prepareUsersData = ({ condoId, users }) => {
  let preparedData = []

  if (Array.isArray(users) && users.length > 0) {
    preparedData = users
      .filter(user => user.dn &&
        user.dn.search('OU=USERS') &&
        user.mail
      )
      .map(user => ({
        firstName: user.givenName,
        lastName: user.sn,
        email: user.mail,
        condoId
      }))
  }

  return preparedData
}

const createUsers = ({ user, dateToSendEmail = '', avoidEmail = false }) => {
  return new Promise((resolve, reject) => {
    let deferredRegistrationDate = new Date(dateToSendEmail)
    deferredRegistrationDate = isNaN(deferredRegistrationDate.getTime()) ? new Date() : deferredRegistrationDate
    const baseData = {
      civilite: null,
      phone: null,
      phoneCode: '33',
      phoneCodeLandline: '33',
      landline: null,
      company: [ null ],
      diploma: [ null ],
      school: [ null ],
      studies: [ null ],
      door: [ null ],
      floor: [ null ],
      office: [ null ],
      deferredRegistrationDate: deferredRegistrationDate.getTime(),
      roles: [ 'Kg9vGJrrnHWQJYGE2' ],
      building: [ '-1' ],
      status: [ 'user' ]
    }
    const dataFromAD = {
      firstname: user.firstName,
      lastname: user.lastName,
      email: user.email,
      condos: [ user.condoId ],
      fromAD: true
    }

    console.log({
      ...dataFromAD,
      ...baseData
    })

    Meteor.call('createNewOccupantFromManagerSide', {
      ...dataFromAD,
      ...baseData
    }, [ null ], 'web', avoidEmail, (error, result) => {
      console.log('error, result::: ', error, result)
      resolve(result)
    })
  })
}

export {
  verifyActiveDirectoryIntegration,
  getActiveDirectoryData,
  prepareUsersData,
  createUsers
}
