Meteor.startup(function () {
    Meteor.methods({
        /*  SET FILEID OF LOGO FOR SPECIFIC SCHOOL
         schoolName                (String) NAME OF SCHOOL TO UPDATE
         fileId                    (String) UD OF FILE FROM "SchoolPhotos" MONGO TABLE
         */
        setSchoolId(schoolName, fileId) {
            Schools.update({"schools.name": schoolName},
                {$set: {"schools.$.fileId": fileId}});
        }
    })
});