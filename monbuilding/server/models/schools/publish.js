// PUBLISH ALL SCHOOLS FROM DATABASE (INCLUDING LOGO)
Meteor.publish('schools', function () {
	return [SchoolPhotos.find().cursor, Schools.find()];
});

Meteor.publish('infoTrombiGestionnaire', function(current_condo) {
	const condoContact = CondoContact.findOne({condoId: current_condo});
	let managerDetailId = DeclarationDetails.findOne({key: "managerWAccount"});
	if (!!condoContact) {
		let managerCondoContact = _.find(condoContact.contactSet, function(contact) {
			return contact.declarationDetailId == managerDetailId._id;
		})

		if (managerCondoContact && managerCondoContact.length > 0) {
			managerCondoContact = _.without(managerCondoContact.userIds, null, undefined);

			if (managerCondoContact && managerCondoContact.length > 0) {
				let profilePictures = UserFiles.find({ $and:
					[
					{"meta.userId": {$exists: true}},
					{"meta.userId": {$in: managerCondoContact}},
					{"meta.publicPicture": {$exists: true}},
					{"meta.publicPicture": true}
					]
				}).cursor;
				return profilePictures;
			}
		}
	}
	this.stop()
});
