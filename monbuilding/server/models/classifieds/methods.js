import {Email, DeletePost} from './../../../common/lang/lang';

let index = 0
Meteor.startup(function () {
	Meteor.methods({
		replyClassified: function(condoId, classifiedId, message, files) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let date = Date.now();
			ClassifiedsFeed.insert({
				classifiedId: classifiedId,
				message: message,
				files: files,
        date: date,
        createdAt: Date.now(),
        updatedAt: Date.now(),
				isEdited: false,
        userId: Meteor.userId(),
        condoId
			});
			let elem = "views." + this.userId;
			ClassifiedsAds.update({ _id: classifiedId }, {
        $set: {
          updatedAt: date,
          [elem]: (date + 10),
          userUpdateView: [this.userId]
        },
        $addToSet: {participants: this.userId },
        $inc: {commentCount: 1}
      });

			_.each(files, function(fileId) {
				UserFiles.update({_id: fileId}, {$set: {"meta.classifiedId": classifiedId}});
			})

			date = new Date()
			date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
			let condo = Condos.findOne(condoId)
			let condoName = 'Condo not found.'
			if (condo)
				condoName = condo.getName();
			let usersId = _.map(ClassifiedsFeed.find({ classifiedId: classifiedId }).fetch(), (c) => {
				return c.userId;
			});
      usersId = _.uniq([...usersId, ClassifiedsAds.findOne({ _id: classifiedId }).userId]);
      let pushNotificationUserIds = [];
      for (let index = usersId.length - 1; index >= 0; index--) {
        let id = usersId[index];
				if (id !== Meteor.userId()) {
					let user = Meteor.users.findOne({ _id: id })
					if (!!user) {
            const lang = user.profile.lang || 'fr';
            const translation = new Email(lang);
            message = (message.length > 50 ? (message.substr(0, 50) + '...') : message);
            if (Meteor.userHasRight("ads", "see", condoId, id)) {
              pushNotificationUserIds.push(id);
              if (!user.identities.gestionnaireId) {
                Meteor.call('checkNotifResident', id, condoId, 'classifieds', function (error, result) {
                  if (result) {
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-reply-forum-' + lang,
                      to: user.emails[0].address,
                      condoId: condoId,
                      subject: translation.email['new_offers_answer'],
                      data: {
                        module: "classifieds",
                        title: translation.email['publish_new_offers_answer'],
                        condo: condoName,
                        condoId: condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/resident/${condoId}/profile/notifications`, condoId),
                        sujet: message,
                        setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/annonces?ad=${classifiedId}`, condoId),
                      },
                      dueTime: date,
                      condition: {
                        name: "checkClassifiedBeforeSend",
                        data: { classifiedsId: classifiedId, userId: id, condoId: condoId, createdAt: Date.now() }
                      }
                    });
                  }
                });
              } else {
                Meteor.call('checkNotifGestionnaire', id, condoId, 'classifieds', function (error, result) {
                  if (result) {
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-reply-forum-' + lang,
                      to: user.emails[0].address,
                      condoId: condoId,
                      subject: translation.email['new_offers_answer'],
                      data: {
                        module: "classifieds",
                        title: translation.email['publish_new_offers_answer'],
                        condo: condoName,
                        condoId: condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condoId),
                        sujet: message,
                        setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/classifieds/${condoId}/?ad=${classifiedId}`, condoId),
                      },
                      dueTime: date,
                      condition: {
                        name: "checkClassifiedBeforeSend",
                        data: { classifiedsId: classifiedId, userId: id, condoId: condoId, createdAt: Date.now() }
                      }
                    });
                  }
                });
              }
            }
					}
				}
			}
      OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
				type: 'ads',
				key: 'new_comment',
				dataId: classifiedId,
				condoId: condoId
			})
		},

		editClassifiedAnswer: function(condoId, classifiedFeedId, message, files) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let date = Date.now();
			let thisFeed = ClassifiedsFeed.findOne({_id: classifiedFeedId});
			if (thisFeed) {
				ClassifiedsFeed.update({
					_id: classifiedFeedId
				}, {
					$set: {
						message: message,
						files: files,
						isEdited: date
					}
				});

				let removedFiles = _.difference(thisFeed.files, files);

				_.each(removedFiles, function(fileId) {
					UserFiles.remove({_id: fileId});
				})

				ClassifiedsAds.update({_id: thisFeed.classifiedId}, {
          $set: {updatedAt: date},
          $addToSet: {participants: this.userId}
        });
				_.each(files, function(fileId) {
					UserFiles.update({_id: fileId}, {$set: {"meta.classifiedId": thisFeed.classifiedId}});
				})
			}
		},

		deleteClassifiedAnswer: function(condoId, classifiedFeedId) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let date = Date.now();
			let thisFeed = ClassifiedsFeed.findOne({_id: classifiedFeedId});
			if (thisFeed) {
				_.each(thisFeed.files, function(fileId) {
					UserFiles.remove({_id: fileId});
				})
				ClassifiedsFeed.remove({_id: classifiedFeedId});
				ClassifiedsAds.update(
          {_id: thisFeed.classifiedId},
          {$set: {updatedAt: date}},
          {$inc: {commentCount: -1}}
        );
			}
		},

		// UPDATE THE DATE VIEW OF SPECIFIC AD OF CURRENT USER
		// id                                    (String) ID OF THE MODULE TO UPDATE
		'module-classifieds-view-ad': function(id) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
      let elem = "views." + this.userId;
      ClassifiedsAds.update({
        _id: id,
        userViews: {$not: { $elemMatch: { $eq: this.userId } } }
      },
				{
          $addToSet: { userViews: this.userId },
          $inc: {viewsCount: 1}
				}
			);
			ClassifiedsAds.update({ _id: id },
				{
					$set: { [elem]: Date.now() },
          $addToSet: { userUpdateView: this.userId }
				}
			);
		},

		// UPDATE THE DATE VIEW OF SPECIFIC AD OF CURRENT USER
		// id                                    (String) ID OF THE MODULE TO UPDATE
		'module-classifieds-view-ads': function(ids) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let elem = "views." + this.userId;
			ClassifiedsAds.update({_id: {$in: ids}},
				{
					$set: {
						[elem]: Date.now()
          },
          $addToSet: {
            userUpdateView: this.userId,
            userViews: this.userId
          }
				}, {
					multi: true
				}
			);
		},
		// DELETE AN EXISTING AD
		// id                                    (String) ID OF AD ("ClassifiedsAds" TABLE)
		'module-classifieds-ad-delete': function(id) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			const classified = ClassifiedsAds.findOne(id);
			if (!classified)
				throw new Meteor.Error(601, "Invalid id", "Cannot find post");
			let postfiles = UserFiles.find({$and:
				[
					{'meta.classifiedsId': {$exists: true} },
					{'meta.classifiedsId': {$eq: classified.classifiedsId} },
					{_id: {$in: classified.files} },
				]
			}).fetch();
			_.each(postfiles, function(elem) {
				UserFiles.remove(elem._id);
			});
      ClassifiedsFeed.remove({classifiedId: id}, {multi: true});
			ClassifiedsAds.remove(id);
			if (classified.userId != Meteor.userId()) {
				let user = Meteor.users.findOne(classified.userId);
				const condo = Condos.findOne(classified.condoId);
				if (user) {
					let lang = user.profile.lang || 'fr';
					let translation = new DeletePost(lang);
					Meteor.call('sendEmailSync',
						user.emails[0].address,
						classified.condoId,
						translation.deletePost['subject'],
						'delete-post-' + lang,
						{
              building: !!condo ? condo.getName() : '',
							category : translation.deletePost["ads"],
							title : classified.title,
							description : classified.description
						}
					);
				}
			}
		},
		/* EDIT AN EXISTING AD
		id                                    (String) ID OF AD ("ClassifiedsAds" TABLE)
		modifier                              (Object) CONTAINS DATA FOR THE EDITION
		{
		title                               (String) TITLE
		price                               (String) PRICE
		description                         (String) DESCRIPTION
		classifiedsId                       (String) ID OF MODULE ("Classifieds" TABLE)
		userId                              (String) USERID OF AUTHOR
		createdAt                           (Date) DATE OF CREATION
		updatedAt                           (Date) DATE OF EDTIT
		views                               (Table of Object) TABLE TO TRACK VIEWS OF AD
		condoId                             (String) CONDOID OF CONDO RELATED TO THE AD
		history                             (Table of Object) HISTORY OF EDITS
		files                               (Table of String) FILEIDs FROM "UserFiles" TABLE
		}
		*/
		'module-classifieds-ad-edit': function(id, modifier) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			const post = ClassifiedsAds.findOne(id);
			if (!post)
				throw new Meteor.Error(601, "Invalid id", "Cannot find post");
			check(modifier.title, String);
			check(modifier.description, String);
			check(modifier.classifiedsId, String);
			check(modifier.history, [String]);
			check(modifier.userId, String);
			modifier.editedAt = Date.now()
			if (modifier.price)
				check(modifier.price, String);
			modifier.views = post.views;
			if (post.createdAt)
				modifier.createdAt = post.createdAt;
			let postfiles = UserFiles.find({$and:
				[
				{'meta.classifiedsId': {$exists: true} },
				{'meta.classifiedsId': {$eq: post.classifiedsId} },
				{_id: {$in: post.files} },
				{_id: {$nin: modifier.files} },
				]
			}).fetch();
			_.each(postfiles, function(elem) {
				UserFiles.remove(elem._id);
      });

      let attachments = []
      if (Array.isArray(modifier.files) && modifier.files.length > 0) {
        const newFilesId = modifier.files.filter(file => typeof file !== 'string');
        const filesId = modifier.files.filter(file => typeof file === 'string');
        const fileDatas = UserFiles.find({_id: {$in: filesId}});
        attachments = [...newFilesId, ...fileDatas.map(fileData => ({
          fileId: fileData._id,
          mime: fileData.type,
          ext: fileData.extension
        }))]
      }
      modifier.attachments = attachments;

      modifier.userUpdateView = [this.userId]
			ClassifiedsAds.update(id, {
        $set: modifier,
        $addToSet: {participants: this.userId}
			});
		},
		/* CREATE A NEW AD
		data                                  (Object) CONTAINS DATA FOR THE CREATION
		{
		title                               (String) TITLE
		price                               (String) PRICE
		description                         (String) DESCRIPTION
		classifiedsId                       (String) ID OF MODULE ("Classifieds" TABLE)
		userId                              (String) USERID OF AUTHOR
		createdAt                           (Date) DATE OF CREATION
		views                               (Table of Object) TABLE TO TRACK VIEWS OF AD
		condoId                             (String) CONDOID OF CONDO RELATED TO THE AD
		history                             (Table of Object) HISTORY OF EDITS
		files                               (Table of String) FILEIDs FROM "UserFiles" TABLE
		}
		*/
		'module-classifieds-create-ad': function(data) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			check(data.title, String);
			check(data.description, String);
			check(data.classifiedsId, String);
			check(data.history, [String]);
			check(data.userId, String);
			if (data.price)
				check(data.price, String);
			let CondoId = data.condoId;
			if (!data.views) {
				data.views = {}
			}
			data.views[Meteor.userId()] = (data.createdAt + 10) /* To prevent badges error */
      data.editedAt = null

      let attachments = []
      if (Array.isArray(data.files) && data.files.length > 0) {
        const newFilesId = data.files.filter(file => typeof file !== 'string');
        const filesId = data.files.filter(file => typeof file === 'string');
        const fileDatas = UserFiles.find({_id: {$in: filesId}});
        attachments = [...newFilesId, ...fileDatas.map(fileData => ({
          fileId: fileData._id,
          mime: fileData.type,
          ext: fileData.extension
        }))]
      }

			let adId = ClassifiedsAds.insert({
        // NEW MESSAGE DATA
        createdBy: this.userId,
        participants: [this.userId],
        userUpdateView: [this.userId],
        userViews: [this.userId],
        viewsCount: 0,
        commentCount: 0,
        attachments,
        // ////////////////
        title: data.title,
        description: data.description,
        classifiedsId: data.classifiedsId,
        history: data.history,
        userId: data.userId,
        price: data.price,
        createdAt: Date.now(),
        updatedAt: Date.now(),
        views: data.views,
        condoId: data.condoId,
        files: data.files
      });
			// ENVOI DE L EMAIL
			let date = new Date();
			date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
			let userId = Meteor.userId();
			let tmpCondo = Condos.findOne(data.condoId);
			let condoName = tmpCondo.name ? tmpCondo.name : tmpCondo.info.address + ", " + tmpCondo.info.code + " " + tmpCondo.info.city;
			let pushNotificationUserIds = []
			let residentsIds = Meteor.call('getResidentsOfCondo', data.condoId);
			residentsIds.forEach((id) => {
				if (id !== userId) {
          const user = Meteor.users.findOne({ _id: id })
          if (!user) return
          var lang = user.profile.lang || 'fr';
					var translation = new Email(lang);
					data.title = (data.title.length > 50 ? (data.title.substr(0, 50) + '...') : data.title);
					if (Meteor.userHasRight("ads", "see", data.condoId, id)) {
            pushNotificationUserIds.push(id);
            Meteor.call('checkNotifResident', id, data.condoId, 'classifieds', function(error, result) {
							if (result) {
								Meteor.call('scheduleAnEmail', {
									templateName: 'new-classified-' + lang,
									to: user.emails[0].address,
									condoId: data.condoId,
									subject: translation.email['new_offers'],
									data: {
										module: "classifieds",
										condo: condoName,
										condoId: data.condoId,
										notifUrl: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/profile/notifications`, data.condoId),
										title: translation.email['publish_new_offers'],
										sujet: data.title,
										setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/annonces?ad=${adId}`, data.condoId)
									},
									dueTime: date,
									condition: {
										name: "checkClassifiedBeforeSend",
										data: {classifiedsId: data.classifiedsId, userId: id, condoId: CondoId, createdAt: Date.now()}
									}
								});
							}
						});
					}
				}
			});
			if (gestionnaire = Enterprises.findOne({"condos": data.condoId})) {
				gestionnaire.users.forEach((Guser) => {
					if (Guser.userId !== userId) {
            if (Meteor.userHasRight("ads", "see", data.condoId, Guser.userId)) {
              pushNotificationUserIds.push(Guser.userId);
              Meteor.call('checkNotifGestionnaire', Guser.userId, data.condoId, 'classifieds', function(error, result) {
                if (result) {
                  var lang = Meteor.users.findOne({_id: Guser.userId}).profile.lang || 'fr';
                  var translation = new Email(lang);
                  data.title = (data.title.length > 50 ? (data.title.substr(0, 50) + '...') : data.title);
                  Meteor.call('scheduleAnEmail', {
                    templateName: 'new-classified-' + lang,
                    to: Meteor.users.findOne({_id: Guser.userId}).emails[0].address,
                    condoId: data.condoId,
                    subject: translation.email['new_offers'],
                    data: {
                      module: "classifieds",
                      condo: condoName,
                      title: translation.email['publish_new_offers'],
                      sujet: data.title,
                      condoId: data.condoId,
                      notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, data.condoId),
                      setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/classifieds/${data.condoId}/?ad=${adId}&back=`, data.condoId)
                    },
                    dueTime: date,
                    condition: {
                      name: "checkClassifiedBeforeSend",
                      data: {classifiedsId: data.classifiedsId, userId: Guser.userId, condoId: CondoId, createdAt: Date.now()}
                    }
                  });
                }
              });
            }
					}
				})
      }
      Meteor.call('addToGestionnaireHistory',
        'ads',
        adId,
        'adCreated',
        data.condoId,
        data.title,
        Meteor.userId()
      );
			OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
				type: 'ads',
				key: 'new_ad',
				dataId: adId,
				condoId: data.condoId
			})
			return adId
		},
		// FOLLOW / UNFOLLOW A CLASSIFIED MODULE
		// condoId                         (String) ID OF CONDO
		"module-classifieds-follow-toggle": function(condoId) {
			if (!Match.test(condoId, String))
				throw new Meteor.Error(401, "Invalid parameters classifieds module-classifieds-follow-toggle");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let resident = Residents.findOne({userId: Meteor.userId()});
			if (resident) {
				let condo = _.find(resident.condos, (condo) => {return condo.condoId === condoId});
				if (!condo)
					throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
				let value = condo.notifications.classifieds;
				Residents.update({userId: Meteor.userId(),
					'condos.condoId': condoId},
					{$set:
						{'condos.$.notifications.classifieds': !value}
					});
			}
			else {
				let gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()});
				let user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()});
				let condoInCharge = user.condosInCharge.find((c) => {return c.condoId == condoId});
				if (condoInCharge.notifications.classifieds == undefined)
					condoInCharge.notifications.classifieds = true;
				else
					condoInCharge.notifications.classifieds = !condoInCharge.notifications.classifieds;
				_.each(user.condosInCharge, function(elem) {
					if (elem.condoId === condoId) {
						elem = condoInCharge;
					}
				});
				Enterprises.update({
					'_id': gestionnaire._id,
					'users.userId': Meteor.userId()
				},
				{
					$set: {'users.$.condosInCharge': user.condosInCharge},
				});
			}
		},
		"isReplyAds": function(classifiedId) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let classifieds = ClassifiedsAds.findOne(classifiedId);
			if (!classifieds)
				throw new Meteor.Error(601, "Ads not found");
			let view = classifieds.views[Meteor.userId()]
			if (!view)
				throw new Meteor.Error(601, "Ads pas vu, comment tu as repondu ?");
			return true;
		},
		'isNewAds': function (adId, condoId) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			const ad = ClassifiedsAds.findOne({_id: adId})
			if (!ad)
				throw new Meteor.Error(601, "Ads not found");
			let userId = Meteor.userId()
			let view = ad.views[userId]
			let resident = Residents.findOne({userId: userId})
			if (resident) {
				let joined = resident.condos.find(c => c.condoId === condoId).joined
				if (ad.updatedAt < joined) {
					return false
				}
			}
			if (!view || (view < ad.updatedAt)) {
				if (ad.userId === userId) {
					return true
				} else {
					if (!view) {
						return true
					} else {
						let replyOfUser = ClassifiedsFeed.find({
							classifiedId: ad._id,
							userId: userId
						}).fetch()
						if (replyOfUser && replyOfUser.length > 0) {
							return true
						}
					}
				}
			}
			return false
		}
	});
});
