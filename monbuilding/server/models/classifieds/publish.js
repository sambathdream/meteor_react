Meteor.startup(function () {
  Meteor.publish('classifieds', function (id) {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");
    let resident = Residents.findOne({ userId: this.userId });
    if (!resident)
      throw new Meteor.Error(301, "Access denied", "User dont have 'resident' identity");
    return Classifieds.find(id);
  });

  Meteor.publish('classifiedsOccupantFeed', function (condoId) {
    if (!this.userId) {
      throw new Meteor.Error(300, "Not Authenticated");
    }
    return ClassifiedsAds.find({ condoId: condoId }, { sort: { updatedAt: -1 }, limit: 5 })
  })
  Meteor.publish('ClassifiedsAds', function (selector) {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");
    let resident = Residents.findOne({ userId: this.userId });
    let enterprise = Enterprises.findOne({ 'users.userId': this.userId });
    if (!resident && !enterprise)
      throw new Meteor.Error(301, "Access denied", "User dont have 'resident' identity");
    return ClassifiedsAds.find(selector);
  });

  Meteor.publish('classifiedsFiles', function (classId) {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");
    return UserFiles.find({
      $and:
        [
          { 'meta.classifiedsId': { $exists: true } },
          { 'meta.classifiedsId': { $eq: classId } },
        ]
    }).cursor;
  });

  Meteor.publish('classifiedsGestionnaireView', function () {
    let userId = this.userId
    if (!userId)
      throw new Meteor.Error(300, "Not Authenticated");
    let enterprise = Enterprises.findOne({ 'users.userId': userId });
    if (!enterprise)
      throw new Meteor.Error(301, "Acces denied", "User don't have any 'gestionnaire' identity")
    let condoIds = _.map(_.find(enterprise.users, function (user) {
      return user.userId == userId
    }).condosInCharge, function (condo) {
      return condo.condoId
    });
    let ClassifiedsIds = [];
    _.each(condoIds, function (condoId) {
      let condo = Condos.findOne(condoId)
      if (condo) {
        let modules = _.find(condo.modules, function (mod) {
          return mod.name == "classifieds"
        })
        if (modules && modules.data)
          ClassifiedsIds.push(modules.data.classifiedsId)
      }
    })
    return [Classifieds.find({ '_id': { $in: ClassifiedsIds } }), ClassifiedsAds.find({ 'classifiedsId': { $in: ClassifiedsIds } })];
  });

  Meteor.publish('classifiedsFeedAnswerCounter', function (condoIds) {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");

    let self = this;
    let handlers = []
    condoIds.forEach((condoId) => {
      let feed = ClassifiedsFeed.find({condoId});
      let result = [];

      handlers.push(feed.observe({
        added: function (document) {
          if (!result[document.classifiedId]) {
            result[document.classifiedId] = 1;
            self.added("classifiedsFeedCounter", document.classifiedId, { _id: document.classifiedId, counter: 1 });
          }
          else {
            result[document.classifiedId] += 1;
            self.changed("classifiedsFeedCounter", document.classifiedId, { _id: document.classifiedId, counter: result[document.classifiedId] });
          }
        },
        removed: function (oldDocument) {
          if (result[oldDocument.classifiedId]) {
            result[oldDocument.classifiedId] -= 1;
            if (result[oldDocument.classifiedId] < 1) {
              delete result[oldDocument.classifiedId];
              self.removed("classifiedsFeedCounter", oldDocument.classifiedId);
            }
            else
              self.changed("classifiedsFeedCounter", oldDocument.classifiedId, { _id: oldDocument.classifiedId, counter: result[oldDocument.classifiedId] });
          }
        },
      }))
    })
    this.onStop(() => {
      handlers.forEach(handler => {
        handler.stop()
      })
      return true
    })
    self.ready();
  })

  Meteor.publish('classifiedsFeed', function (condoId, classifiedId) {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");

    let self = this;
    let feed = ClassifiedsFeed.find({ classifiedId: classifiedId });

    const handler = feed.observe({
      added: function (document) {
        self.added("classifiedsFeed", document._id, document);
      },
      changed: function (newDocument, oldDocument) {
        self.changed("classifiedsFeed", oldDocument._id, newDocument);
      },
      removed: function (oldDocument) {
        self.removed("classifiedsFeed", oldDocument._id);
      },
    });
    this.onStop(() => {
      return handler.stop()
    })
    self.ready();
    return UserFiles.find({ "meta.classifiedId": classifiedId }).cursor;
  })

  Meteor.publish('classifiedsFeedStat', function () {
    if (!this.userId)
      throw new Meteor.Error(300, "Not Authenticated");

    let condoIds = Meteor.listCondoUserHasRight("ads", "see")

    let self = this;
    let handlers = []
    condoIds.forEach((condoId) => {
      let classifiedIds = _.map(ClassifiedsAds.find({ condoId: condoId }).fetch(), (c) => {
        return c._id
      })
      let feed = ClassifiedsFeed.find({ classifiedId: { $in: classifiedIds } })

      handlers.push(feed.observe({
        added: function (document) {
          self.added("classifiedsFeed", document._id, {
            _id: document._id,
            classifiedId: document.classifiedId,
            date: document.date
          });
        },
        changed: function (newDocument, oldDocument) {
          self.changed("classifiedsFeed", oldDocument._id, {
            _id: newDocument._id,
            classifiedId: newDocument.classifiedId,
            date: newDocument.date
          })
        },
        removed: function (oldDocument) {
          self.removed("classifiedsFeed", oldDocument._id);
        },
      }))
    })
    this.onStop(() => {
      handlers.forEach(handler => {
        handler.stop()
      })
      return true
    })
    self.ready();
    // return UserFiles.find({"meta.classifiedId": classifiedId}).cursor;
  })
});
