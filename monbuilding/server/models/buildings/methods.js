Meteor.startup(function () {
	Meteor.methods({
	  BuildingJoinRequest: function (place_id) {
			if (!Meteor.userId)
				return;
			let building = Meteor.Buildings.findOne({"info.place_id": place_id});
	  },
    updateCondoPhoto: function (condoId, fileId, uploaderId, isLogo) {
      check(condoId, String)
      check(fileId, String)
      check(uploaderId, Match.Maybe(String))

      const file = !isLogo ? CondoPhotoFiles.findOne({_id: fileId}) : CondoLogoFiles.findOne({_id: fileId})
      const isExist = !isLogo ? CondoPhotos.findOne({condoId: condoId}) : CondoLogos.findOne({condoId: condoId})
      const data = {
        condoId,
        fileId,
        avatar: {
          original: file.link(),
          large: file.link("large"),
          thumbnail40: file.link("thumbnail40"),
          thumb100: file.link("thumb100"),
          avatar: file.link("avatar"),
          preview: file.link("preview")
        },
        ext: file.ext || null,
        type: file.type || null,
        uploaderId,
        date: Date.now()
      }
      if (isLogo) {
        Condos.update({_id: condoId}, { $set: {
          logo: {
            fileId,
            mime: file.type,
            ext: file.ext || file.extension
          }
        }})
      } else {
        Condos.update({_id: condoId}, { $set: {
          photo: {
            fileId,
            mime: file.type,
            ext: file.ext || file.extension
          }
        }})
      }
      if (!isExist) {
        if (!isLogo) {
          CondoPhotos.insert(data)
        } else {
          CondoLogos.insert(data)
        }
      } else {
        if (!isLogo) {
          CondoPhotos.update({ _id: isExist._id }, data)
        } else {
          CondoLogos.update({ _id: isExist._id }, data)
        }
      }
    },
    updateCondoSettings: function (condoId, data) {
      check(condoId, String)
      check(data, Object)

      const condo = Condos.findOne(condoId);
      if (!!condo) {
        Condos.update({ _id: condoId }, {
          ...condo,
          settings: {
            ...condo.settings,
            ...data
          }
        })
      }
    },
    removeCondoPhoto: function (condoId, isLogo) {
      const isExist = !isLogo ? CondoPhotos.findOne({condoId: condoId}) : CondoLogos.findOne({condoId: condoId})

      if (isLogo) {
        Condos.update({_id: condoId}, { $set: { logo: null } } )
      } else {
        Condos.update({_id: condoId}, { $set: { photo: null }})
      }
      if (!!isExist) {
        if (!isLogo) {
          CondoPhotoFiles.remove({_id: isExist.fileId})
          CondoPhotos.remove({condoId: condoId})
          Condos.update({ _id: condoId }, {
            $set: {
              'settings.false': false
            }
          })
        } else {
          CondoLogoFiles.remove({_id: isExist.fileId})
          CondoLogos.remove({condoId: condoId})
        }
      }
    },

    ServerScriptResetAllCondoColor: function () {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
        throw new Meteor.Error(300, "Not Authenticated");
      let condos = Condos.find().fetch();
      _.each(condos, function(condo) {
        if (!!condo.settings && !!condo.settings.colorPalette) {
          Condos.update({ _id: condo._id }, {
            ...condo,
            settings: {
              ...condo.settings,
              colorPalette: false
            }
          })
        }
      });
    },
	});
});
