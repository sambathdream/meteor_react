import { Meteor } from 'meteor/meteor'
// import { UserRightsHandler } from '/server/userInformationHandler'

Meteor.startup(() => {
  Meteor.publish('fillUserPermissions', function (condoId) {
    let userRightsCursor
    let condoRoleCursor
    let defaultRolesCursor
    let loaded = false

    userRightsCursor = UsersRights.find({ condoId: condoId, userId: this.userId })
    const userRightDoc = userRightsCursor.fetch()

    defaultRolesCursor = DefaultRoles.find({ _id: userRightDoc[0].defaultRoleId })
    const defaultRoleDoc = defaultRolesCursor.fetch()

    condoRoleCursor = CondoRole.find({ name: defaultRoleDoc[0].name, condoId: condoId })

    const updateUserPermissions = (collection) => {
      return (document) => {
        const rightsFromDoc = collection === 'user' ? document.module : document.rights

        if (rightsFromDoc !== undefined) {
          UserRightsHandler.cleanUpTrackers()

          switch (collection) {
            case 'user':
              UserRightsHandler.fillCacheFromUserModules(rightsFromDoc)
              break
            case 'default':
              UserRightsHandler.fillCacheFromDefaultRoles(rightsFromDoc)
              break
            case 'condo':
              UserRightsHandler.fillCacheFromCondoRole(rightsFromDoc)
              break
          }

          const rightsMerged = UserRightsHandler.getRightsMerged()

          for (let urModule in rightsMerged) {
            if (UserRightsHandler.lastInsertedModuleRights.has(urModule)) {
              this.added('UserPermissions', urModule, rightsMerged[urModule])
            } else if (
              UserRightsHandler.lastUpdatedModuleRights.has(urModule) &&
              !UserRightsHandler.lastInsertedModuleRights.has(urModule)
            ) {
              this.changed('UserPermissions', urModule, rightsMerged[urModule])
            } else if (!loaded) {
              this.added('UserPermissions', urModule, rightsMerged[urModule])
            }
          }
          loaded = true
        }
      }
    }

    const prepareCallbacks = (collection) => {
      return {
        added: updateUserPermissions(collection),
        changed: updateUserPermissions(collection)
      }
    }

    const handlerDefaultRolesCursor = defaultRolesCursor.observe(prepareCallbacks('default'))
    const handlerCondoRoleCursor = condoRoleCursor.observe(prepareCallbacks('condo'))
    const handlerUserRightsCursor = userRightsCursor.observe(prepareCallbacks('user'))

    this.onStop(() => {
      return handlerDefaultRolesCursor.stop() && handlerCondoRoleCursor.stop() && handlerUserRightsCursor.stop()
    })
    this.ready()
  })
})
