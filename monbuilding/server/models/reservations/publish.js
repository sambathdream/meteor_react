import moment from 'moment'

const RESA_MAX_DATA = 1000;

Meteor.startup(
  function () {
    /*
     PUBLISH PRESENT AND FUTURE RESERVATIONS OF SPECIFIC CONDO
     */
    Meteor.publish('presentReservations', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;
      const yesterday = +moment().add(-1, 'd').format('x');

      let handlers = []
      if (condoId) {

        let feed = Reservations.find({ condoId, start: { $gte: yesterday } }, { limit: RESA_MAX_DATA });

        handlers.push(feed.observe({
          added: function (document) {
            self.added("reservations", document._id, document);
          },
          changed: function (newDocument, oldDocument) {
            self.changed("reservations", oldDocument._id, newDocument);
          },
          removed: function (oldDocument) {
            self.removed("reservations", oldDocument._id);
          },
        }))

        self.ready();

      } else if (Meteor.call('isGestionnaire', self.userId)) {
        let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId');
        if (!!condoIds) {
          let feed = Reservations.find({ condoId: {$in: condoIds}, start: { $gte: yesterday } });
          handlers.push(feed.observe({
            added: function (document) {
              self.added("reservations", document._id, document);
            },
            changed: function (newDocument, oldDocument) {
              self.changed("reservations", oldDocument._id, newDocument);
            },
            removed: function (oldDocument) {
              self.removed("reservations", oldDocument._id);
            },
          }))
          self.ready()
        } else {
          self.stop();
        }
      }
      this.onStop(() => {
        handlers.forEach(handler => {
          handler.stop()
        })
        return true
      })
    });
    Meteor.publish('allReservationsByMonth', function (condoId, date) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;

      const from = +moment(date).startOf('month').add(-1, 'week').format('x');
      const to = +moment(date).endOf('month').add(1, 'week').format('x');
      let handlers = []
      if (condoId) {
        let feed = Reservations.find({
          condoId,
          $and: [
            { start: { $gte: from } },
            { start: { $lte: to } }
          ]
        });

        handlers.push(feed.observe({
          added: function (document) {
            self.added("reservations", document._id, document);
          },
          changed: function (newDocument, oldDocument) {
            self.changed("reservations", oldDocument._id, newDocument);
          },
          removed: function (oldDocument) {
            self.removed("reservations", oldDocument._id);
          },
        }))
        self.ready();

      } else if (Meteor.call('isGestionnaire', self.userId)) {
        let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId');
        if (!!condoIds) {
          let feed = Reservations.find({
            condoId: {$in: condoIds},
            $and: [
              { start: { $gte: from } },
              { start: { $lte: to } }
            ]
          });

          handlers.push(feed.observe({
            added: function (document) {
              self.added("reservations", document._id, document);
            },
            changed: function (newDocument, oldDocument) {
              self.changed("reservations", oldDocument._id, newDocument);
            },
            removed: function (oldDocument) {
              self.removed("reservations", oldDocument._id);
            },
          }))

          self.ready()
        } else {
          self.stop();
        }
      }
      this.onStop(() => {
        handlers.forEach(handler => {
          handler.stop()
        })
        return true
      })
    });
    /*
     PUBLISH SPECIFIC BOOK
     */
    Meteor.publish('specificReservations', function (condoId, bookId, isEventId = false) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;
      let handlers = []
      if (condoId) {
        let feed = Reservations.find(!isEventId? { _id: bookId }: {$or: [
          {eventId: bookId},
          {eventId: new Meteor.Collection.ObjectID(bookId._str)}
        ]});

        handlers.push(feed.observe({
          added: function (document) {
            self.added("reservations", document._id, document);
          },
          changed: function (newDocument, oldDocument) {
            self.changed("reservations", oldDocument._id, newDocument);
          },
          removed: function (oldDocument) {
            self.removed("reservations", oldDocument._id);
          },
        }))
        self.ready();

      } else if (Meteor.call('isGestionnaire', self.userId)) {
        let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId');
        if (!!condoIds) {
          let feed = Reservations.find(!isEventId? { _id: bookId }: {$or: [
            {eventId: bookId},
            {eventId: new Meteor.Collection.ObjectID(bookId._str)}
          ]});

          handlers.push(feed.observe({
            added: function (document) {
              self.added("reservations", document._id, document);
            },
            changed: function (newDocument, oldDocument) {
              self.changed("reservations", oldDocument._id, newDocument);
            },
            removed: function (oldDocument) {
              self.removed("reservations", oldDocument._id);
            },
          }))
          self.ready()
        } else {
          self.stop();
        }
      }
      this.onStop(() => {
        handlers.forEach(handler => {
          handler.stop()
        })
        return true
      })
    });
    /*
     PUBLISH ALL RESERVATIONS OF SPECIFIC CONDO
     */
    Meteor.publish('reservations', function (condoId, limit, type) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;

      const isManager = Meteor.call('isGestionnaire', self.userId);
      const options = {
        sort: { start: (type === 'confirmed' || type === 'pending') ? 1 : -1 },
        limit: Math.min(limit, RESA_MAX_DATA)
      };

      const getQuery = (condoId, type, isManager) => {
        let query;
        const yesterday = +moment().add(-1, 'd').format('x');
        const extraCheck = isManager ? {} : {
          $or: [
            { origin: self.userId },
            { participants: { $all: [self.userId] } }
          ]
        };
        if (type === 'history') {
          query = {
            $and: [
              {
                $or: [
                  {start: {$lte: +moment().add(1, 'd').format('x')}},
                  {rejected: true}
                ]
              },
              extraCheck
            ]
          };
        } else if (type === 'confirmed') {
          query = {
            start: { $gte: yesterday },
            $and: [
              {
                $or: [
                  { pending: { $exists: false } },
                  { pending: false }
                ]
              },
              {
                $or: [
                  { rejected: { $exists: false } },
                  { rejected: false }
                ]
              },
              extraCheck
            ]
          };
        } else if (type === 'pending') {
          query = {
            start: { $gte: yesterday },
            pending: true,
            ...extraCheck
          };
        } else {
          query = { ...extraCheck };
        }

        return query;
      }

      let handlers = []
      if (condoId) {
        const query = getQuery(condoId, type, isManager);
        let feed = Reservations.find({...query, condoId}, options);

        handlers.push(feed.observe({
          added: function (document) {
            self.added("reservations", document._id, document);
          },
          changed: function (newDocument, oldDocument) {
            self.changed("reservations", oldDocument._id, newDocument);
          },
          removed: function (oldDocument) {
            self.removed("reservations", oldDocument._id);
          },
        }))
        // Counts.publish(self, 'book-length', feed, { noReady: true });
        self.ready();

      } else if (isManager) {
        let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId');
        if (!!condoIds) {
          _.each(condoIds, function (condoId) {
            const query = getQuery(condoId, type, isManager);
            let feed = Reservations.find({...query, condoId}, options);

            handlers.push(feed.observe({
              added: function (document) {
                self.added("reservations", document._id, document);
              },
              changed: function (newDocument, oldDocument) {
                self.changed("reservations", oldDocument._id, newDocument);
              },
              removed: function (oldDocument) {
                self.removed("reservations", oldDocument._id);
              },
            }))
            // Counts.publish(self, 'book-length', feed, { noReady: true });
          });
          self.ready()
        } else {
          self.stop();
        }
      }
      this.onStop(() => {
        handlers.forEach(handler => {
          handler.stop()
        })
        return true
      })
    });
  }
);
