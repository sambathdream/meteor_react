import { ModuleReservationError, Email, ModuleReservationIndex } from '/common/lang/lang.js'
import { Session } from '../../models_new/imports/session'
import { Meteor } from 'meteor/meteor'

const tzOffset = 2
const TYPE = {
  edl: 'Etat des lieux'
}

export function validatePaidBook(bookId, transactionId) {
  return Reservations.update({_id: bookId},
    {$set: {pending: false, transactionId: transactionId}})
}

/**
 * set start date
 *
 * @param event
 * @param lang
 * @returns {*}
 */
function setStartDate (event, lang) {
  const translation_ = new ModuleReservationError(lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang)
  let Sdate
  if (!event.startTime.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$')) {
    throw new Meteor.Error(401, translation_.moduleReservationError['bad_start_time'], 'startDate')
  }
  let enStartDate = event.startDate.split('/')
  if (enStartDate.length === 1) {
    throw new Meteor.Error(401, translation_.moduleReservationError['bad_start_date'], 'startDate')
  }
  Sdate = moment(event.startDate, 'DD/MM/YYYY')
  Sdate.set({ hour: parseInt(event.startTime.split(':')[0], 10), minute: parseInt(event.startTime.split(':')[1], 10) })
  if (Sdate.isBefore(moment().add(tzOffset, 'h'))) {
    throw new Meteor.Error(401, translation_.moduleReservationError['invalid_start_time'], 'startDate')
  }
  return Sdate
}

/**
 * set end date to moment
 *
 * @param event
 * @param Sdate
 * @param lang
 * @returns {*}
 */
function setEndDate (event, Sdate, lang) {
  const translation_ = new ModuleReservationError(lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang)
  let Edate
  if (!event.startDate) {
    throw new Meteor.Error(401, translation_.moduleReservationError['no_start_time'], 'endDate')
  }
  if (!event.endTime.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$')) {
    throw new Meteor.Error(401, translation_.moduleReservationError['bad_end_time'], 'endDate')
  }
  let enEndDate = event.endDate.split('/')
  if (enEndDate.length === 1) {
    throw new Meteor.Error(401, translation_.moduleReservationError['bad_end_date'], 'endDate')
  }
  Edate = moment(event.endDate, 'DD/MM/YYYY')
  Edate.set({ hour: parseInt(event.endTime.split(':')[0], 10), minute: parseInt(event.endTime.split(':')[1], 10) })
  if (Edate.isBefore(moment().add(tzOffset, 'h')) || Edate.isBefore(Sdate) || Edate.isSame(Sdate)) {
    throw new Meteor.Error(401, translation_.moduleReservationError['invalid_end_time'], 'bothDates')
  }
  return Edate
}

/**
 * Validate event
 *
 * @param Sdate
 * @param Edate
 * @param event
 * @param r
 * @param lang
 * @param onValidate
 * @param isEdl
 */
function validateEvent (Sdate, Edate, event, r, lang, onValidate, isEdl = false) {
  if (!Meteor.userId())
    throw new Meteor.Error(300, "Not Authenticated");
  const translation_ = new ModuleReservationError(lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang)
  if (Sdate.isSame(Edate, 'day')) {
    if (Meteor.call('resourceIsOpened', event.resourceId, Sdate, Edate, event.condoId) === false) {
      throw new Meteor.Error(401, translation_.moduleReservationError['unopened'], 'busy')
    }
  } else {
    if (Meteor.call('resourceIsOpenedWithStart', event.resourceId, Sdate, event.condoId) === false) {
      throw new Meteor.Error(401, translation_.moduleReservationError['unopened'], 'busy')
    }
    if (Meteor.call('resourceIsOpenedWithEnd', event.resourceId, Edate, event.condoId) === false) {
      throw new Meteor.Error(401, translation_.moduleReservationError['unopened'], 'busy')
    }
  }
  if (Meteor.call('resourceIsFree', event.resourceId, Sdate, Edate, event.condoId, event.number, isEdl, event.eventId) === false) {
    throw new Meteor.Error(412, translation_.moduleReservationError[!!onValidate ? 'already_taken_on_validate': 'allready_taken'], 'busy')
  }
  if (Meteor.call('resourceLimit', r.type, Meteor.userId(), +Sdate.format('x'), event.condoId) === true) {
    throw new Meteor.Error(401, translation_.moduleReservationError['limit'])
  }
  if (event.title && event.title.length > 140) {
      throw new Meteor.Error(401, translation_.moduleReservationError['title_too_long'])
  }
}

/**
 *
 * @param date
 * @param lang
 * @returns {string}
 */
function change_date (date, lang) {
  let day = new Date(date).getDay()
  let month = new Date(date).getMonth()
  if (lang == 'en') {
    let dayList = [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday'
    ]
    let monthList = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December'
    ]
    return dayList[day] + ', ' + date.format('DD') + ' ' + monthList[month]
  } else {
    let dayList = [
      'Dimanche',
      'Lundi',
      'Mardi',
      'Mercredi',
      'Jeudi',
      'Vendredi',
      'Samedi'
    ]
    let monthList = [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Aout',
      'Septembre',
      'Octobre',
      'Novembre',
      'Décembre'
    ]
    return dayList[day] + ' ' + date.format('DD') + ' ' + monthList[month]
  }
}

Meteor.startup(function () {
  Meteor.methods({
    /*  ADD A BOOKING TO SPECIFIC RESOURCE WITH CHECKING IF AVAILABLE, AND SEND EMAIL WITH EVENT ATTACHED IF NEEDED
         event               (Object) CONTAINS DATA FOR THE CREATION
         {
             resourceId      (String) ID OF THE RESOURCE TO BOOK
             origin          (String) USERID OF THE CREATOR
             startDate       (String) START DATE OF THE BOOKING (DD/MM/YYYY)
             startTime       (String) START TIME OF THE BOOKING (HH:MM)
             endDate         (String) END DATE OF THE BOOKING
             endTime         (String) END TIME OF THE BOOKING
             title           (String) TITLE OF THE BOOKING DISPLAYED ON THE TIMELINE
             emailEvent      (Boolean) INDICATE IF USER WANTS TO RECEIVE AN EMAIL WITH EVENT ATTACHED
         }
         */
    'addEventToResource': function (event, lang) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let debut, fin, dates, Sdate, Edate
      const l = lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang
      const translation_ = new ModuleReservationError(l)
      const r = Resources.findOne({_id: event.resourceId})
      if (!r) {
        throw new Meteor.Error(401, translation_.moduleReservationError['unknowed'])
      }
      if (event.startDate) {
        Sdate = setStartDate(event, l)
      }
      if (event.endDate) {
        Edate = setEndDate(event, Sdate, l)
      }

      validateEvent(Sdate, Edate, event, r, l)
      const condoId = event.condoId;
      const pending = r.needValidation && r.needValidation.status && !(r.needValidation.managerIds.includes(event.origin) || r.needValidation.managerIds.includes(event.bookedBy));
      let newEvent = {
        // new fields
        createdBy: this.userId,
        createdAt: Date.now(),
        updatedAt: Date.now(),
        userViews: [this.userId],
        needValidation: r.needValidation ? r.needValidation.status : false,
        validators: r.needValidation ? r.needValidation.managerIds : [],
        // /////////
        creation: Date.now(),
        resourceId: event.resourceId,
        condoId: event.condoId,
        title: event.title,
        eventId: new Meteor.Collection.ObjectID(),
        origin: event.origin,
        bookedBy: event.bookedBy,
        participants: event.participants,
        externalParticipants: event.externalParticipants,
        participantNumber: event.participantNumber,
        start: +Sdate.format('x'),
        end: +Edate.format('x'),
        pending: pending
      }
      if (event.isAllDay) {
        newEvent.isAllDay = event.isAllDay
      }
      if (event.isRecurring) {
        newEvent.isRecurring = event.isRecurring
        newEvent.recurringType = event.recurringType
        newEvent.recurringEnd = event.recurringEnd
      }
      if (event.services) {
        newEvent.services = event.services
      }
      if (event.message) {
        newEvent.message = event.message
      }
      if (isInt(event.number)) {
        newEvent.number = event.number
      }
      if (event.paid) {
        newEvent.paid = event.paid
        newEvent.price = event.price
        newEvent.vat = event.vat
        newEvent.paidDuration = event.paidDuration
        newEvent.paidAmount = event.paidAmount
        newEvent.paidAmount = event.paidAmount
        newEvent.qty = event.qty
        newEvent.pending = true
      }

      const id = Reservations.insert(newEvent)
      if (!pending) {

        if (event.emailEvent) {
          let user = Meteor.users.findOne(event.origin)
          const lang = user.profile.lang || 'fr'
          const translationEmail = new Email(lang)
          if (Sdate.isSame(Edate, 'day')) {
            dates = change_date(Sdate, lang) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
          } else {
            debut = change_date(Sdate, lang) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
            dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
          }
          try {

            Meteor.call('checkNotifResident', user._id, event.condoId, 'reservation', function(error, result) {
              if (result) {
                Meteor.call('scheduleAnEmail', {
                  to: user.emails[0].address,
                  condoId: event.condoId,
                  subject: translationEmail.email['confirmation_reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name) + ', ' + dates,
                  templateName: 'reservation-' + lang,
                  data: {
                    type: r.type,
                    name: (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
                    dates: dates,
                    cancelLink: Meteor.absoluteUrl(`${user.profile.lang}/resident/${condoId}/reservation`, condoId),
                    start: +Sdate.format('x'),
                    end: +Edate.format('x'),
                    location: (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
                    eventName: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
                    isReservation: true
                  },
                  dueTime: new Date(),
                })
              }
            })
          }
          catch(err) {
              console.log(err)
          }
        }

        Meteor.call('sendInviteMail', Sdate, Edate, condoId, newEvent.eventId, l)
      }

      if (newEvent.participants) {
        pushNotificationUserIds = [newEvent.origin, ...newEvent.participants]
        // Push Notification invitation :
        OneSignal.MbNotification.sendToUserIds(event.participants, {
          type: 'reservation',
          key: 'invit_booking',
          dataId: newEvent.eventId,
          condoId: event.condoId,
          data: {
            reservation: {
              _id: id,
              ...newEvent
            }
          }
        })
      } else {
        pushNotificationUserIds = [newEvent.origin]
      }

      // Push Notification confirmation (new_booking) :
      OneSignal.MbNotification.sendToOneUser(newEvent.origin, {
        type: 'reservation',
        key: 'new_booking',
        dataId: newEvent.eventId,
        condoId: event.condoId,
        data: {
          reservation: {
            _id: id,
            ...newEvent
          }
        }
      })

      // Push Notification add reminder (reminder_booking) :
      let rplDate = moment(Sdate);
      rplDate.add(-15, 'minutes');
      rplDate.add(-1, 'hour');
      let rappelDate = new Date(parseInt(rplDate.format('x')));
      if (rappelDate !== null) {
        OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
          type: 'reservation',
          key: 'reminder_booking',
          dataId: newEvent.eventId,
          condoId: event.condoId,
          data: {
            reservation: {
              _id: id,
              ...newEvent
            }
          }
        }, rappelDate, (result, msg) => {
          if (result) {
            ReminderNotificationsId.insert({
              postId: newEvent.eventId,
              pushId: result
            })
          }
        })
      }
      if (pending && r.needValidation && r.needValidation.status && r.needValidation.managerIds && r.needValidation.managerIds.length > 0) {
        OneSignal.MbNotification.sendToUserIds(r.needValidation.managerIds, {
          type: 'reservation',
          key: 'need_validation_booking',
          dataId: newEvent.eventId,
          condoId: event.condoId,
          data: {
            reservation: {
              _id: id,
              ...newEvent
            }
          }
        })
        let userName = Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname
        let managerIds = r.needValidation.managerIds
        for (const managerId of managerIds) {
          let user = Meteor.users.findOne(managerId);
          if (user) {
            Meteor.call('checkNotifGestionnaire', user._id, condoId, 'reservations', function(error, result) {
              if (result) {
                const condoId = event.condoId
                const lang = user.profile.lang || 'fr'
                const translation = new Email(lang)
                if (Sdate.isSame(Edate, 'day')) {
                  dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
                } else {
                  debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
                  fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
                  dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
                }
                Meteor.call('sendEmailEvent', {
                  to: user.emails[0].address,
                  condoId: condoId,
                  eventName: translation.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name),
                  subject: translation.email['req_reservation'] + (r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name) + ', ' + dates,
                  template_name: 'new-reservation-to-validate-' + lang,
                  data: {
                    condoId,
                    name: (r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name),
                    type: getResourceTypeByKey(r.type, lang),
                    dates,
                    userName,
                    comment: event.message,
                    cancelLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}`, condoId)
                  },
                  start: Sdate,
                  end: Edate,
                  location: (r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name)
                })
              }
            })
          }
        }
      }

      if (!!pending) {
        Meteor.call('addToGestionnaireHistory',
          'reservation',
          newEvent.eventId._str,
          'newResa',
          event.condoId,
          newEvent.title || '',
          event.origin
        );
      }

      return {
        id,
        eventId: newEvent.eventId
      }
    },
    /*  ADD A SCHEDULE OF FIXTURES IN SPECIFIC RESOURCES, BY RESIDENT
         event               (Object) CONTAINS DATA FOR THE CREATION
         {
             resourceId      (String) ID OF THE RESOURCE TO BOOK
             origin          (String) USERID OF THE RESIDENT RELATED TO THE SCHEDULE OF FIXTURES
             startDate       (String) START DATE OF THE SCHEDULE OF FIXTURES (DD/MM/YYYY)
             startTime       (String) START TIME OF THE SCHEDULE OF FIXTURES (HH:MM)
             title           (String) TITLE OF THE SCHEDULE OF FIXTURES DISLAYED ON THE TIMELINE
         }
         */
    'addEDLToResource': function (event, lang) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let debut, fin, dates, Sdate, Edate
      const l = lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang
      const translation_ = new ModuleReservationError(l)
      const resource = Resources.findOne({_id: event.resourceId})
      if (!resource) { throw new Meteor.Error(401, translation_.moduleReservationError['unknowed']) }

      if (event.startDate) {
        Sdate = setStartDate(event, l)
      }
      const duration = +resource.edlDuration || 45
      Edate = moment(Sdate).add(duration, 'm')
      validateEvent(Sdate, Edate, event, resource, l)

      let translationResa = new ModuleReservationIndex(l)
      let direction = translationResa.moduleReservationIndex[!!event.edlDirection ? event.edlDirection: 'out'].toLowerCase()

      const bookEdl = Meteor.call('canBookEdl', event);
      if ( bookEdl.status === false) {
        const message = translation_.moduleReservationError['already_have_edl'].replace('$date', bookEdl.date).replace('$time', bookEdl.time).replace('$direction', direction);
        throw new Meteor.Error(401, message, 'busy')
      }

      const condo = Condos.findOne({_id: resource.condoId})
      const pending = resource.needValidation && resource.needValidation.status && resource.needValidation.managerIds && !(resource.needValidation.managerIds.includes(event.origin) || resource.needValidation.managerIds.includes(event.bookedBy))
      let newEvent = {
        // new fields
        createdBy: this.userId,
        createdAt: Date.now(),
        updatedAt: Date.now(),
        userViews: [this.userId],
        needValidation: resource.needValidation ? resource.needValidation.status : false,
        validators: resource.needValidation ? resource.needValidation.managerIds : [],
        // /////////
        creation: Date.now(),
        resourceId: event.resourceId,
        condoId: event.condoId,
        title: event.title,
        eventId: new Meteor.Collection.ObjectID(),
        origin: event.origin,
        bookedBy: event.bookedBy,
        participants: event.participants,
        participantNumber: event.participantNumber,
        edlDirection: !!event.edlDirection ? event.edlDirection : 'out',
        start: +Sdate.format('x'),
        end: +Edate.format('x'),
        pending: pending
      }

      if (isInt(event.number)) {
        newEvent.number = event.number
      }

      if (event.paid) {
        newEvent.paid = event.paid
        newEvent.price = event.price
        newEvent.vat = event.vat
        newEvent.paidDuration = event.paidDuration
        newEvent.paidAmount = event.paidAmount
        newEvent.paidAmount = event.paidAmount
        newEvent.qty = event.qty
        newEvent.pending = true
      }

      let edlDate = moment(event.startDate, 'DD/MM/YYYY')
      edlDate.set({ hour: parseInt(event.startTime.split(':')[0], 10), minute: parseInt(event.startTime.split(':')[1], 10) })
      if (Sdate.isBefore(moment().add(tzOffset, 'h'))) {
        throw new Meteor.Error(401, translation_.moduleReservationError['invalid_start_time'], 'startDate')
      }

      newEvent.edlId = EDL.insert({
        condoId: resource.condoId,
        creation: Date.now(),
        date: +edlDate.format('x'),
        origin: event.origin,
        status: 'En attente',
        resourceId: event.resourceId,
        fromGestionnaire: false
      })
      const condoContact = CondoContact.findOne({condoId: condo._id})
      let emailList = []
      if (condoContact) {
        let edlDetailId = DeclarationDetails.findOne({key: 'edl'})
        let edlCondoContact = _.find(condoContact.contactSet, function (contact) {
          return contact.declarationDetailId == edlDetailId._id
        })

        emailList = [condoContact.defaultContact.userId]
        if (edlCondoContact && edlCondoContact.userIds && edlCondoContact.userIds.length > 0) { emailList = _.without(edlCondoContact.userIds, null, undefined) }
      }

      const user = Meteor.users.findOne(event.origin)

      _.each(emailList, (e) => {
        if (Meteor.call('checkNotifGestionnaire', Meteor.users.findOne(e)._id, condo._id, 'reservations')) {
          let lang = 'fr' // Meteor.users.findOne(e).profile.lang || 'fr' // for now hard coded to FR because only FR template available
          let translation = new Email(lang)
          // FOR NOW EMAIL IS ONLY IN FR REMOVE BELLOW TRANSLATION IF EN EMAIL READY
          // translationResa = new ModuleReservationIndex(lang) // <<---- HARD CODED FR
          // direction = translationResa.moduleReservationIndex[!!event.edlDirection ? event.edlDirection: 'out'].toLowerCase()
          // if (Sdate.isSame(Edate, 'day')) {
          //   debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
          //   fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
          //   dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
          // } else {
          //   debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
          //   fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
          //   dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
          // }
          // Meteor.call('sendEmailSync',
          //   Meteor.users.findOne(e).emails[0].address,
          //   condo._id,
          //   "Demande d'un " + direction + ", " + dates,
          //   'alert_edl',
          //   {
          //     residentName: user.profile.firstname + ' ' + user.profile.lastname,
          //     dates: dates,
          //     building: condo.name,
          //     link: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}`, condo._id),
          //     direction: direction
          //   }
          // )

          lang = Meteor.users.findOne(e).profile.lang || 'fr'
          translation = new Email(lang)
          translationResa = new ModuleReservationIndex(lang)
          direction = translationResa.moduleReservationIndex[!!event.edlDirection ? event.edlDirection: 'out'].toLowerCase()
          if (Sdate.isSame(Edate, 'day')) {
            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
            dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
          } else {
            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
            dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
          }

          Meteor.call('sendEmailEvent', {
            to: Meteor.users.findOne(e).emails[0].address,
            condoId: condo._id,
            eventName: translation.email['reservation'] + (resource.name === 'edl' ? translation.email['edl'] : resource.name),
            subject: translation.email['req_reservation'] + (resource.name === 'edl' ? translation.email['edl'] : resource.name) + ', ' + dates,
            template_name: 'new-reservation-to-validate-' + lang,
            data: {
              condoId: condo._id,
              name: resource.name === 'edl' ? translation.email['edl'] : resource.name,
              type: direction,
              dates,
              userName: user.profile.firstname + ' ' + user.profile.lastname,
              cancelLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}`, condo._id)
            },
            start: Sdate,
            end: Edate,
            location: resource.name === 'edl' ? translation.email['edl'] : resource.name
          })
        }
      })
      const id = Reservations.insert(newEvent)

      if (event.bookedBy !== event.origin) {
        Meteor.call('sendEdlMailToResident', condo._id, user, Sdate, event.edlDirection)
      }

      if (!pending && (!event.edlDirection || (!!event.edlDirection && event.edlDirection === 'out'))) {
        Meteor.call('sendEdlMail', condo._id, user, newEvent.edlId, id, Edate, Sdate)
      }

      // Push Notification confirmation (new_booking) :
      OneSignal.MbNotification.sendToOneUser(newEvent.origin, {
        type: 'reservation',
        key: 'new_booking',
        dataId: newEvent.eventId,
        condoId: newEvent.condoId,
        data: {
          reservation: {
            '_id': id,
            ...newEvent
          }
        }
      })

      // Push Notification add reminder (reminder_booking) :
      let rplDate = moment(Sdate);
      rplDate.add(-15, 'minutes');
      rplDate.add(-1, 'hour');
      let rappelDate = new Date(parseInt(rplDate.format('x')));
      if (rappelDate !== null) {
        OneSignal.MbNotification.sendToOneUser(newEvent.origin, {
          type: 'reservation',
          key: 'reminder_booking',
          dataId: newEvent.eventId,
          condoId: newEvent.condoId,
          data: {
            reservation: {
              '_id': id,
              ...newEvent
            }
          }
        }, rappelDate, (result, msg) => {
          if (result) {
            ReminderNotificationsId.insert({
              postId: newEvent.eventId,
              pushId: result
            })
          }
        })
      }

      if (!!pending) {
        Meteor.call('addToGestionnaireHistory',
          'reservation',
          newEvent.eventId._str,
          'newResa',
          event.condoId,
          newEvent.title || '',
          event.origin
        );
      }

      return {
          id,
          eventId: newEvent.eventId
      }
    },
    /*  EDIT AN EXISTING BOOKING
         event               (Object) CONTAINS DATA FOR THE EDITION
         {
             resourceId        (String) ID OF THE RESOURCE OF THE EVENT
             startDate         (String) NEW START DATE OF THE BOOKING (DD/MM/YYYY)
             startTime         (String) NEW START TIME OF THE BOOKING (HH:MM)
             endDate           (String) NEW END DATE OF THE BOOKING
             endTime           (String) NEW END TIME OF THE BOOKING
             eventId           (String) ID OF SPECIFIC EVENT TO EDIT
         }
         */
    'editEvent': function (event, lang) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let debut, fin, dates, Sdate, Edate
      const l = lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang
      const translation_ = new ModuleReservationError(l)
      if (event.startDate) {
        Sdate = setStartDate(event, l)
      }
      if (event.endDate) {
        Edate = setEndDate(event, Sdate, l)
      }
      let resource = Resources.findOne({_id: event.resourceId})
      if (!resource) {
        throw new Meteor.Error(401, translation_.moduleReservationError['no_booking'], 'busy')
      }
      validateEvent(Sdate, Edate, event, resource, l, resource.type === 'edl')
      const oldEvent = Reservations.findOne({ 'eventId': event.eventId })
      const pending = resource.needValidation && resource.needValidation.status && !(resource.needValidation.managerIds.includes(event.origin) || resource.needValidation.managerIds.includes(event.bookedBy));
      let editEvent = {
        updatedAt: Date.now(),
        title: event.title,
        origin: event.origin,
        bookedBy: event.bookedBy,
        participants: event.participants,
        participantNumber: event.participantNumber,
        isAllDay: event.isAllDay,
        isRecurring: event.isRecurring,
        recurringType: event.recurringType,
        recurringEnd: event.recurringEnd,
        resourceId: event.resourceId,
        externalParticipants: event.externalParticipants,
        start: +Sdate.format('x'),
        end: +Edate.format('x'),
        services: event.services,
        pending: oldEvent.start !== +Sdate.format('x') || oldEvent.end !== +Edate.format('x') ? pending: !!oldEvent.pending,
        message: event.message
      }

      if (isInt(event.number)) {
        editEvent.number = event.number
      } else if (!!oldEvent.number) {
        editEvent.number = null
      }

      if (!oldEvent.pending) {
        if (event.emailEvent) {
          const condoId = event.condoId
          const lang = Meteor.user().profile.lang || 'fr'
          const translation = new Email(lang)
          if (Sdate.isSame(Edate, 'day')) {
            dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
          } else {
            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
            dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
          }
          const user = Meteor.users.findOne(event.origin)
          Meteor.call('sendEmailEvent', {
            to: user.emails[0].address,
            condoId: condoId,
            eventName: translation.email['reservation'] + (resource.type === 'edl' && resource.name === 'edl' ? translation.email['edl'] : resource.name),
            subject: translation.email['confirmation_reservation'] + (resource.type === 'edl' && resource.name === 'edl' ? translation.email['edl'] : resource.name) + ', ' + dates,
            template_name: 'reservation-' + Meteor.user().profile.lang,
            data: {
              type: resource.type,
              name: (resource.type === 'edl' && resource.name === 'edl' ? translation.email['edl'] : resource.name),
              dates: dates,
              cancelLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/reservation`, condoId)},
            start: Sdate,
            end: Edate,
            location: (resource.type === 'edl' && resource.name === 'edl' ? translation.email['edl'] : resource.name)
          })
        }
      }
      // Push Notification invit new user (invit_booking) :
      let newUser = _.difference(event.participants, oldEvent.participants)
      if (newUser && newUser.length > 0) {
        OneSignal.MbNotification.sendToUserIds(newUser, {
          type: 'reservation',
          key: 'invit_booking',
          dataId: event.eventId,
          condoId: event.condoId,
          data: {
            reservation: {
              ...oldEvent
            }
          }
        })
        for (const userId of newUser) {
          let user = Meteor.users.findOne({_id: userId})
          if (user) {
            let lang = user.profile.lang || 'fr'
            let Sdate = moment(oldEvent.start)
            let Edate = moment(oldEvent.end)
            let translationEmail = new Email(lang);
            let debut, fin, dates;
            if (Sdate.isSame(Edate, 'day')) {
              dates = change_date(Sdate, lang) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
            } else {
              debut = change_date(Sdate, lang) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
              fin = change_date(Edate, lang) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
              dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
            }
            Meteor.call('checkNotifResident', user._id, event.condoId, 'reservation', function(error, result) {
              if (result) {
                Meteor.call('scheduleAnEmail', {
                  to: user.emails[0].address,
                  condoId: event.condoId,
                  subject: translationEmail.email['reservation'] + (resource.type === 'edl' && resource.name === 'edl' ? translationEmail.email['edl'] : resource.name) + ', ' + dates,
                  templateName: 'invited-reservation-' + lang,
                  data: {
                    title: !!resource.title ? resource.title : '',
                    type: resource.type,
                    name: resource.type === 'edl' && resource.name === 'edl' ? translationEmail.email['edl'] : resource.name,
                    dates: dates,
                    participantNumber: isInt(editEvent.participantNumber) ? editEvent.participantNumber : '-',
                    message: !!editEvent.message ? editEvent.message : '-',
                    start: +Sdate.format('x'),
                    end: +Edate.format('x'),
                    location: resource.type === 'edl' && resource.name === 'edl' ? translationEmail.email['edl'] : resource.name,
                    eventName: translationEmail.email['reservation'] + (resource.type === 'edl' && resource.name === 'edl' ? translationEmail.email['edl'] : resource.name),
                    isReservation: true
                  },
                  dueTime: new Date(),
                })
              }
            })
          }
        }
      }
      // Push Notification add reminder (reminder_booking) :
      let pushNotificationUserIds = []
      if (event.participants && event.participants.length > 0) {
        pushNotificationUserIds = [event.origin, ...event.participants]
      } else {
        pushNotificationUserIds = [event.origin]
      }
      let reminderPush = ReminderNotificationsId.findOne({ postId: event.eventId });
      if (reminderPush) {
        OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
          if (err) {
            // console.log('Error: ', err);
            return false;
          }
        })
        ReminderNotificationsId.remove({ postId: event.eventId }, { multi: true })
      }
      let rplDate = moment(Sdate)
      rplDate.add(-15, 'minutes')
      rplDate.add(-1, 'hour')
      let rappelDate = new Date(parseInt(rplDate.format('x')))
      if (rappelDate !== null) {
        pushNotificationUserIds
        OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
          type: 'reservation',
          key: 'reminder_booking',
          dataId: event.eventId,
          condoId: event.condoId,
          data: {
            reservation: {
              ...oldEvent
            }
          }
        }, rappelDate, (result, msg) => {
          if (result) {
            ReminderNotificationsId.insert({
              postId: event.eventId,
              pushId: result
            })
          }
        })
      }
      editEvent.userViews = [this.userId]
      return Reservations.update({'eventId': event.eventId},
        {$set: editEvent})
    },
    /*  EDIT AN EXISTING BOOKING (POTENTIALLY CHANGE RESOURCE)
         event               (Object) CONTAINS DATA FOR THE EDITION
         {
             resourceId        (String) ID OF THE RESOURCE OF THE EVENT
             startDate         (String) NEW START DATE OF THE BOOKING (DD/MM/YYYY)
             startTime         (String) NEW START TIME OF THE BOOKING (HH:MM)
             endDate           (String) NEW END DATE OF THE BOOKING
             endTime           (String) NEW END TIME OF THE BOOKING
             eventId           (String) ID OF SPECIFIC EVENT TO EDIT
             title             (String) TITLE OF THE BOOKING DISPLAYED ON THE TIMELINE
             origin            (String) USERID OF THE RESIDENT RELATED TO THE EVENT
         }
         */
    'moveEvent': function (event, lang) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let debut, fin, dates, Edate, Sdate
      const l = lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang
      const translation_ = new ModuleReservationError(l)
      if (event.startDate) {
        Sdate = setStartDate(event, l)
      }
      if (event.endDate) {
        Edate = setEndDate(event, Sdate, l)
      }
      let resource = Resources.findOne({_id: event.resourceId})
      if (!resource) {
        throw new Meteor.Error(401, translation_.moduleReservationError['no_original'], 'busy')
      }
      if (Meteor.call('resourceIsOpened', event.resourceId, Sdate, Edate, event.condoId) === false) {
        throw new Meteor.Error(401, translation_.moduleReservationError['unopened'], 'busy')
      }
      if (Meteor.call('resourceIsEditable', event.resourceId, event.eventId, Sdate, Edate, event.condoid) === false) {
        throw new Meteor.Error(401, translation_.moduleReservationError['no_slot'], 'busy')
      }
      const condoId = event.condoId
      let newEvent = {
        resourceId: event.resourceId,
        condoId: condoId,
        title: event.title,
        origin: event.origin,
        start: +Sdate.format('x'),
        end: +Edate.format('x')
      }
      if (event.emailEvent) {
        let lang = Meteor.user().profile.lang || 'fr'
        const translation = new Email(lang)
        if (Sdate.isSame(Edate, 'day')) {
          dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
        } else {
          debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
          fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
          dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
        }

        const user = Meteor.users.findOne(event.origin)
        Meteor.call('checkNotifResident', user._id, condoId, 'reservation', function(error, result) {
          if (result) {
            Meteor.call('sendEmailEvent', {
              to: user.emails[0].address,
              condoId: condoId,
              eventName: translation.email['reservation'] + resource.name,
              subject: translation.email['confirmation_reservation'] + resource.name + ', ' + dates,
              template_name: 'reservation-' + Meteor.user().profile.lang,
              data: {type: resource.type,
                name: resource.name,
                dates: dates,
                cancelLink: Meteor.absoluteUrl(`${Meteor.user().profile.lang}/resident/${condoId}/reservation`, condoId)},
              start: Sdate,
              end: Edate,
              location: resource.name
            })
          }
        })
      }
      return Reservations.update({'eventId': event.eventId}, {$set: {...newEvent}})
    },
    /*  REMOVE AN EXISTING BOOKING IN SPECIFIC RESOURCE
         resourceId          (String) ID OF THE RESOURCE OF THE BOOKING TO REMOVE
         eventId             (String) ID OF SPECIFIC BOOKING TO REMOVE
         */
    'removeEvent': function (condoId, eventId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      // remove pushNotification reminder
      let reminderPush = ReminderNotificationsId.findOne({ postId: eventId });
      console.log('REMINDER PUSH', reminderPush);
      if (reminderPush) {
        OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
          if (err) {
            // console.log('Error: ', err);
            return false;
          }
        })
        ReminderNotificationsId.remove({ postId: eventId }, { multi: true })
      }
      let c = Condos.findOne({ _id: condoId })
      let event = Reservations.findOne({ eventId: eventId })
      const r = event && Resources.findOne({_id: event.resourceId})
      if (event && r) {

        let participants = null
        if (event.participants && event.participants.length > 0) {
          participants = [...event.participants, event.origin]
        } else {
          participants = [event.origin]
        }
        let Sdate = moment(event.start)
        let Edate = moment(event.end)
        _.each(participants, (userId) => {
          let user = Meteor.users.findOne(userId)
          const lang = user.profile.lang || 'fr'
          const translation = new Email(lang)
          const translationResa = new ModuleReservationIndex(lang)
          if (Sdate.isSame(Edate, 'day')) {
            dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
          } else {
            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
            dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
          }
          Meteor.call('checkNotifResident', user._id, condoId, 'reservation', function(error, result) {
            if (result) {
              Meteor.call('sendEmailSync',
                user.emails[0].address,
                condoId,
                translation.email['cancel_booking'] + dates,
                "cancel_edl-" + lang,
                {
                  title: translation.email['canceled_booking'],
                  dates: dates,
                  type: r.type === 'edl' ? translationResa.moduleReservationIndex[!!event.edlDirection ? event.edlDirection: 'out'].toLowerCase() : getResourceTypeByKey(r.type, lang),
                  name: r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name,
                  buildingName: c.info.address == c.name ? c.info.address + ', ' + c.info.code + ' ' + c.info.city : c.name,
                  buttonText: translation.email['access_plan'],
                  cancelLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/reservation`, condoId)
                }
              );
            }
          })
        })
        OneSignal.MbNotification.sendToUserIds(participants, {
          type: 'reservation',
          key: 'cancel_booking',
          dataId: eventId,
          condoId: condoId,
          data: {
            reservation: {
              ...event
            }
          }
        })
        if (event.pending === true) {
          let resource = Resources.findOne({_id: event.resourceId})
          if (resource.needValidation && resource.needValidation.status && resource.needValidation.managerIds && resource.needValidation.managerIds.length > 0) {
            OneSignal.MbNotification.sendToUserIds(resource.needValidation.managerIds, {
              type: 'reservation',
              key: 'cancel_pending_booking',
              dataId: eventId,
              condoId: condoId,
              data: {
                reservation: {
                  ...event
                }
              }
            })
            for (const managerId of resource.needValidation.managerIds) {
              let user = Meteor.users.findOne(managerId)
              if (user) {
                const lang = user.profile.lang || 'fr'
                const translation = new Email(lang)
                const translationResa = new ModuleReservationIndex(lang);
                if (Sdate.isSame(Edate, 'day')) {
                  dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format('HH[h]mm') + translation.email['_to_'] + Edate.format('HH[h]mm')
                } else {
                  debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')
                  fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format('HH[h]mm')
                  dates = translation.email['from2_'] + debut + translation.email['_to'] + fin
                }
                Meteor.call('checkNotifGestionnaire', user._id, condoId, 'reservations', function(error, result) {
                  if (result) {
                    Meteor.call('sendEmailSync',
                      user.emails[0].address,
                      condoId,
                      translation.email['cancel_confirm_booking'] + ' ' + dates,
                      "cancel_edl-" + lang,
                      {
                        title: translation.email['canceled_confirm_booking'],
                        dates: dates,
                        type: r.type === 'edl' ? translationResa.moduleReservationIndex[!!event.edlDirection ? event.edlDirection: 'out'].toLowerCase() : getResourceTypeByKey(r.type, lang),
                        name: r.type === 'edl' && r.name === 'edl' ? translation.email['edl'] : r.name,
                        buildingName: c.info.address == c.name ? c.info.address + ', ' + c.info.code + ' ' + c.info.city : c.name,
                        buttonText: translation.email['access_plan'],
                        cancelLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condoId}`, condoId)
                      }
                    );
                  }
                })
              }
            }
          }
        }
      }
      return Reservations.remove({eventId: eventId})
    },
    /*  CHECK IF SPECIFIC RESOURCE IS OPENED AT SPECIFIC SLOT
         resourceId          (String) ID OF THE RESOURCE TO CHECK
         start               (Date) START OF THE SLOT TO CHECK
         end                 (Date) END OF THE SLOT TO CHECK
         */
    'resourceIsOpened': function (resourceId, start, end, condoId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let resource = Resources.findOne({_id: resourceId})
      if (!resource) { return false }

      let openDate = moment(start)
      let closeDate = moment(end)

      if (start.isSame(end, 'day')) {
        let horaire = _.find(resource.horaires, (h) => { return h.dow[0] === start.day() })
        if (!horaire.open) { return false }
        if (horaire.start === horaire.end === '00:00') { return true }

        if (horaire.openHours && horaire.openHours.length > 1) {
          // new multiple open schedule check
          let isOpen = false
          _.each(horaire.openHours, (h) => {
            openDate.set({ hour: parseInt(h.start.split(':')[0], 10), minute: parseInt(h.start.split(':')[1], 10) })
            closeDate.set({ hour: parseInt(h.end.split(':')[0], 10), minute: parseInt(h.end.split(':')[1], 10) })

            if (start.isSameOrAfter(openDate) && start.isBefore(closeDate) && end.isAfter(openDate) && end.isSameOrBefore(closeDate)) {
              isOpen = true
            }
          })

          if (!isOpen) {
            return false
          }
        } else {
          openDate.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
          closeDate.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })

          if (start.isBefore(openDate) || end.isAfter(closeDate)) {
            return false
          }
        }
      } else {
        /* BEGIN */
        let horaire = _.find(resource.horaires, (h) => { return h.dow[0] === start.day() })
        if (!horaire.open) { return false }
        let openDate = moment(start)
        let closeDate = moment(start)
        openDate.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
        closeDate.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })
        if (start.isBefore(openDate) || start.isAfter(closeDate) || horaire.end !== '23:59') { return false }
        /* END */
        horaire = _.find(resource.horaires, (h) => { return h.dow[0] === end.day() })
        if (!horaire.open) { return false }
        openDate = moment(end)
        closeDate = moment(end)
        openDate.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
        closeDate.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })
        if (end.isBefore(openDate) || end.isAfter(closeDate) || horaire.start !== '00:00') { return false }
        /* BETWEEN */
        start.add(1, 'd')
        while (!start.isSame(end)) {
          let horaire = _.find(resource.horaires, (h) => { return h.dow[0] === start.day() })
          if (!(horaire.open === horaire.full === true)) { return false }
          start.add(1, 'd')
        }
      }
      return true
    },
    /*  CHECK IF SPECIFIC RESOURCE IS OPENED WITH START TIME EVENT
         resourceId          (String) ID OF THE RESOURCE OF THE BOOKING TO REMOVE
         end                 (Date) END OF THE SLOT TO CHECK
         */
    'resourceIsOpenedWithStart': function (resourceId, start, condoId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      const resource = Resources.findOne({_id: resourceId})

      if (!resource) { return false }

      const horaire = _.find(resource.horaires, (h) => { return h.dow[0] === start.day() })

      if (!horaire || !horaire.open) { return false }

      let openDate = moment(start)
      let closeDate = moment(start)

      if (horaire.openHours && horaire.openHours.length > 1) {
        // new multiple open schedule check
        let isOpen = false
        _.each(horaire.openHours, (h) => {
          openDate.set({ hour: parseInt(h.start.split(':')[0], 10), minute: parseInt(h.start.split(':')[1], 10) })
          closeDate.set({ hour: parseInt(h.end.split(':')[0], 10), minute: parseInt(h.end.split(':')[1], 10) })

          if (start.isSameOrAfter(openDate) && start.isBefore(closeDate)) {
            isOpen = true
          }
        })

        if (!isOpen) {
          return false
        }
      } else {
        openDate.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
        closeDate.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })

        if (start.isBefore(openDate) || start.isSameOrAfter(closeDate)) { return false }
      }

      return true
    },
    /*  CHECK IF SPECIFIC RESOURCE IS OPENED WITH START TIME EVENT
         resourceId          (String) ID OF THE RESOURCE OF THE BOOKING TO REMOVE
         start               (Date) START OF THE SLOT TO CHECK
         */
    'resourceIsOpenedWithEnd': function (resourceId, end, condoId) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
      const resource = Resources.findOne({_id: resourceId})

      if (!resource) { return false }

      const horaire = _.find(resource.horaires, (h) => { return h.dow[0] === end.add(-1, 'm').day() })

      if (!horaire || !horaire.open) { return false }

      let openDate = moment(+end - 1)
      let closeDate = moment(+end - 1)

      if (horaire.openHours && horaire.openHours.length > 1) {
        // new multiple open schedule check
        let isOpen = false
        _.each(horaire.openHours, (h) => {
          openDate.set({ hour: parseInt(h.start.split(':')[0], 10), minute: parseInt(h.start.split(':')[1], 10) })
          closeDate.set({ hour: parseInt(h.end.split(':')[0], 10), minute: parseInt(h.end.split(':')[1], 10) })

          if (end.isAfter(openDate) && end.isSameOrBefore(closeDate)) {
            isOpen = true
          }
        })

        if (!isOpen) {
          return false
        }
      } else {
        openDate.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
        closeDate.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })

        if (parseInt(horaire.end.split(':')[0], 10) !== 24 && (end.isSameOrBefore(openDate) || end.isAfter(closeDate))) { return false }
      }

      return true
    },
    /*  CHECK IF SPECIFIC RESOURCE IS FREE AT SPECIFIC SLOT
         resourceId          (String) ID OF THE RESOURCE TO CHECK
         start               (Date) START OF THE SLOT TO CHECK
         end                 (Date) END OF THE SLOT TO CHECK
         */
    'resourceIsFree': function (resourceId, start, end, condoId, number = null, isEdl = false, eventId = false) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let resource = Resources.findOne({_id: resourceId})
      if (!resource) { return false }
      let ret = true
      let param = {
        resourceId: resourceId,
        end: {$gte: +moment().format('x')},
        ...!eventId? {}: {eventId: {$ne: eventId}},
        $and: [
          {$or: [
            {pending: false},
            {pending: null},
            {pending: { $exists: false }},
            {pending: isEdl}, // if it's an EDL don't suggest already pending booked
            // un comment if user should not book another pending resource at the same time
            {$and: [
              {pending: true},
              {$or: [
                {origin: Meteor.userId()},
                {participants: Meteor.userId()}
              ]}
            ]}
          ]},
          {$or: [
            {rejected: false },
            {rejected: { $exists: false }}
          ]}
        ]
      }
      if (number) {
        param.number = number
      }
      const events = Reservations.find(param).fetch()
      _.each(events, (e) => {
        let elemStart = moment(e.start)
        let elemEnd = moment(e.end)

        if (((start.isSameOrAfter(elemStart) && start.isBefore(elemEnd)) || (end.isAfter(elemStart) && end.isSameOrBefore(elemEnd))) ||
                ((elemStart.isSameOrAfter(start) && elemStart.isBefore(end)) || (elemEnd.isAfter(start) && elemEnd.isSameOrBefore(end)))) { ret = false }
      })
      return ret
    },
    /*  CHECK IF SPECIFIC EVENT DATE CAN BE CHANGE TO NEW SPECIFIC SLOT
         resourceId          (String) ID OF THE RESOURCE TO CHECK
         eventId             (String) ID OF SPECIFIC BOOKING TO CHANGE
         start               (Date) START OF THE SLOT TO CHECK
         end                 (Date) END OF THE SLOT TO CHECK
         */
    'resourceIsEditable': function (resourceId, eventId, start, end, condoId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let resource = Resources.findOne({_id: resourceId})
      if (!resource) { return false }
      let ret = true
      const events = Reservations.find({resourceId: resourceId}).fetch()
      _.each(events, (e) => {
        let elemStart = moment(e.start)
        let elemEnd = moment(e.end)
        if (e.eventId._str !== eventId._str && !e.pending && (((start.isSameOrAfter(elemStart) && start.isBefore(elemEnd) || (end.isAfter(elemStart) && end.isSameOrBefore(elemEnd)))) ||
                ((elemStart.isSameOrAfter(start) && elemStart.isBefore(end)) || (elemEnd.isAfter(start) && elemEnd.isSameOrBefore(end))))) { ret = false }
      })
      return ret
    },
    /*  GET IDS OF RESOURCES OPENED AND FREE AT SPECIFIC SLOT
         data
         {
             condoId           (String) ID OF THE CONDO OF THE RESOURCES TO GET
             type              (String) TYPE OF THE RESOURCES TO GET
             startDate         (String) START DATE OF THE SLOT (DD/MM/YYYY)
             startTime         (String) START TIME OF THE SLOT (HH:MM)
             endDate           (String) END DATE OF THE SLOT
             endTime           (String) END TIME OF THE SLOT
         }
         */
    'getFreeResources': function (data) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      if (!data.startDate || !data.startTime || !data.endDate || !data.endTime) { return }
      let dateStart = moment(data.startDate + ' ' + data.startTime, 'DD-MM-YYYY HH:mm')
      let dateEnd = moment(data.endDate + ' ' + data.endTime, 'DD-MM-YYYY HH:mm')
      if (dateStart < +moment().format('x') || dateEnd < +moment().format('x') || dateEnd <= dateStart) {
        throw new Meteor.Error(401, 'Dates invalides')
      }
      let resources = Resources.find({condoId: data.condoId, type: data.type}).fetch()
      let freeIds = []
      const dow = moment(data.startDate, 'DD-MM-YYYY').day();
      _.each(resources, (r) => {
        if (data.type === 'edl') {
          const duration = +r.edlDuration || 45
          dateEnd = moment(dateStart).add(duration, 'm')
        }
        if (!!data.isAllDay) {
          const horaire = _.find(r.horaires, (h) => { return h.dow[0] === dow });
          dateStart = moment(`${data.startDate} ${horaire.start.split(':')[0]}:${horaire.start.split(':')[1]}`, 'DD-MM-YYYY HH:mm')
          let horEndHour = parseInt(horaire.end.split(':')[0], 10)
          let horEndMinute = parseInt(horaire.end.split(':')[1], 10)
          if (horEndHour === 24 && horEndMinute === 0) {
            // horEndHour = 23
            // horEndMinute = 59
            dateEnd = moment(`${data.startDate} 00:00`, 'DD-MM-YYYY HH:mm').add(24, 'h')
          } else {
            dateEnd = moment(`${data.startDate} ${horaire.end.split(':')[0]}:${horaire.end.split(':')[1]}`, 'DD-MM-YYYY HH:mm')
          }
        }
        // check the resource that have multiple space
        if (
          isInt(r.number) &&
          Meteor.call('resourceIsOpenedWithStart', r._id, dateStart, r.condoId) &&
          Meteor.call('resourceIsOpenedWithEnd', r._id, dateEnd, r.condoId)
        ) {
          let keys = {
            id: r._id,
            number: []
          }
          for (let i = 1; i <= r.number; i++) {
            if (Meteor.call('resourceIsFree', r._id, dateStart, dateEnd, r.condoId, i, data.type === 'edl')) {
              keys.number.push(i)
            }
          }
          freeIds.push(keys)
        } else {
          if (
            Meteor.call('resourceIsFree', r._id, dateStart, dateEnd, r.condoId, null, data.type === 'edl') &&
            Meteor.call('resourceIsOpenedWithStart', r._id, dateStart, r.condoId) &&
            Meteor.call('resourceIsOpenedWithEnd', r._id, dateEnd, r.condoId)
          ) {
            freeIds.push(r._id)
          }
        }
      })
      return freeIds
    },
    /**
       * VALIDATE BOOKING APPROVE/REJECT
       * make pending to false, and rejected to true/false depending on approval or rejection
       *
       * @param data
       * @param lang
       */
    'rejectBooking': function (data, lang) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let update = { 'validateAt': Date.now(), 'pending': false, 'rejected': data.rejected, 'resourceId': data.resourceId, 'number': isInt(data.number) ? data.number: null };
      let participants = []
      const book = Reservations.findOne({ 'eventId': data.eventId });
      const condo = Condos.findOne(data.condoId);
      if (book.participants) {
        participants = [book.origin, ...book.participants]
      } else {
        participants = [book.origin]
      }

      const r = Resources.findOne({_id: data.resourceId})
      if (!r) {
        throw new Meteor.Error(401, translation_.moduleReservationError['unknowed'])
      }

      let l = lang || Meteor.users.findOne({_id: Meteor.userId()}).profile.lang
      const translation_ = new ModuleReservationError(l)
      let Sdate, Edate
      if (data.startDate) {
        Sdate = setStartDate(data, l)
      }
      if (data.endDate) {
        Edate = setEndDate(data, Sdate, l)
      }

      if (!!data.startMoment && !!data.endMoment) {
        Sdate = data.startMoment
        Edate = data.endMoment
      }

      if (!!data.rejectionMessage) {
        update.rejectionMessage = data.rejectionMessage
      }

      // validate if approved
      if (!data.rejected) {

        validateEvent(Sdate, Edate, book, r, l, true)

        Meteor.call('sendInviteMail', Sdate, Edate, data.condoId, data.eventId, l)

        try {
          let receipents = null
          if (book.participants) {
            receipents = Meteor.users.find({_id: {$in: [book.origin, ...book.participants]}}).fetch();
          } else {
            receipents = Meteor.users.find({ _id: { $in: [ book.origin ] } }).fetch();
          }

          // action only for move out edl
          if (r.type === 'edl' && (!book.edlDirection || (!!book.edlDirection && book.edlDirection === 'out'))) {
            const originUser = Meteor.users.findOne(book.origin)
            Meteor.call('sendEdlMail', book.condoId, originUser, book.edlId, book._id, Edate, Sdate)
          }

          if (receipents !== null) {

            _.each(receipents, (rec) => {
              l = rec.profile.lang
              let translationEmail = new Email(l);
              let translationResa = new ModuleReservationIndex(l);
              let debut, fin, dates;
              if (Sdate.isSame(Edate, 'day')) {
                dates = change_date(Sdate, l) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
              } else {
                debut = change_date(Sdate, l) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
                fin = change_date(Edate, l) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
                dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
              }

              Meteor.call('checkNotifResident', rec._id, book.condoId, 'reservation', function(error, result) {
                if (result) {
                  Meteor.call('scheduleAnEmail', {
                    to: rec.emails[0].address,
                    condoId: book.condoId,
                    subject: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name) + ', ' + dates,
                    templateName: 'approve-reservation-' + l,
                    data: {
                      type: r.type === 'edl' ? translationResa.moduleReservationIndex[!!book.edlDirection ? book.edlDirection: 'out'].toLowerCase() : getResourceTypeByKey(r.type, l),
                      name: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
                      dates: dates,
                      participantNumber: isInt(book.participantNumber) ? book.participantNumber : false,
                      message: !!book.message ? book.message : false,
                      cancelLink: Meteor.absoluteUrl(`${rec.profile.lang}/resident/${book.condoId}/reservation`, book.condoId),
                      start: +Sdate.format('x'),
                      end: +Edate.format('x'),
                      location: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
                      eventName: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
                      isReservation: true
                    },
                    dueTime: new Date(),
                  })
                }
              })
            })
          }

          Meteor.call('addToGestionnaireHistory',
            'reservation',
            data.eventId._str,
            'acceptedResa',
            data.condoId,
            !!data.title ? data.title : '',
            Meteor.userId()
          );
        }
        catch(err) {
          console.log(err)
        }
      } else {
        // remove pushNotification reminder
        let reminderPush = ReminderNotificationsId.findOne({ postId: data.eventId });
        if (reminderPush) {
          OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
            if (err) {
              // console.log('Error: ', err);
              return false;
            }
          })
          ReminderNotificationsId.remove({ postId: data.eventId }, { multi: true })
        }

        _.each(participants, (userId) => {
          let user = Meteor.users.findOne(userId)
          const lang = user.profile.lang || 'fr'
          const translationEmail = new Email(lang)
          const translationResa = new ModuleReservationIndex(lang);
          let dates, debut, fin

          if (Sdate.isSame(Edate, 'day')) {
            dates = change_date(Sdate, lang) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
          } else {
            debut = change_date(Sdate, lang) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
            fin = change_date(Edate, lang) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
            dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
          }

          Meteor.call('checkNotifResident', user._id, data.condoId, 'reservation', function(error, result) {
            if (result) {
              Meteor.call('sendEmailSync',
                user.emails[0].address,
                data.condoId,
                (!book.pending? translationEmail.email['cancel_booking'] : translationEmail.email['reject_booking']) + dates,
                "cancel_edl-" + lang,
                {
                  title: !book.pending? translationEmail.email['canceled_booking'] : translationEmail.email['rejected_booking'],
                  dates: dates,
                  type: r.type === 'edl' ? translationResa.moduleReservationIndex[!!book.edlDirection ? book.edlDirection: 'out'].toLowerCase() : getResourceTypeByKey(r.type, lang),
                  name: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
                  message: !!data.rejectionMessage ? data.rejectionMessage : '-',
                  buildingName: condo.info.address == condo.name ? condo.info.address + ', ' + condo.info.code + ' ' + condo.info.city : condo.name,
                  buttonText: translationEmail.email['access_plan'],
                  cancelLink: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/reservation`, data.condoId)
                }
              );
            }
          })

          Meteor.call('addToGestionnaireHistory',
            'reservation',
            data.eventId._str,
            'rejectedResa',
            data.condoId,
            !!data.title ? data.title : '',
            Meteor.userId()
          );
        })
      }
      let ret = Reservations.update(
        { 'eventId': data.eventId },
        { $set: {
            ...update,
            userViews: [this.userId]
          }
        }
      )
      OneSignal.MbNotification.sendToUserIds(participants, {
        type: 'reservation',
        key: 'manager_reply',
        dataId: data.eventId,
        condoId: data.condoId,
        data: {
          reservation: {
            ...book
          }
        }
      })

      const eventId = data.eventId
      const viewOfResa = ViewOfReservation.findOne({ _id: eventId })
      const viewToSave = {
        [Meteor.userId()]: Date.now()
      }

      if (viewOfResa) {
        ViewOfReservation.update({
          _id: eventId
        }, {
          $set: viewToSave
        })
      } else {
        ViewOfReservation.insert({
          _id: eventId,
          condoId: data.condoId,
          view: viewToSave
        })
      }
      return ret
    },
    /**
       * GET SUGGESTION TIME SLOT
       *
       * @param data
       * @returns {Array}
       */
    'getSuggestResources': function (data) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      if (!data.startDate || !data.startTime || !data.endDate || !data.endTime) { return }
      const dateStart = moment(data.startDate + ' ' + data.startTime, 'DD-MM-YYYY HH:mm')
      let dateEnd = moment(data.endDate + ' ' + data.endTime, 'DD-MM-YYYY HH:mm')
      let passedDay = false
      if (+dateStart.format('x') < +moment().add(tzOffset, 'h').format('x') || +dateEnd.format('x') < +moment().add(tzOffset, 'h').format('x') || +dateEnd.format('x') <= +dateStart.format('x')) {
        passedDay = true
      }
      const lang = Meteor.users.findOne({_id: Meteor.userId()}).profile.lang || 'fr'

      // range from start request and end request in minutes
      let range = Math.abs(dateEnd.diff(dateStart, 'minutes'))
      let suggestions = []
      let resources = Resources.find({condoId: data.condoId, type: data.type}).fetch()
      _.each(resources, (r) => {
        if (data.type === 'edl') {
          range = +r.edlDuration || 45
          dateEnd = +moment(dateStart).add(range, 'm').format('x')
        }

        let multiSpace = false
        let loop = 1
        if (isInt(r.number)) {
          multiSpace = true
          loop = r.number
        }

        for (let i = 1; i <= loop; i++) {
          const number = multiSpace ? i : null

          // if selected date is before today
          if (passedDay) {
            let found = false
            // dateCursor is to mark the date in the loop
            let dateCursor = moment().startOf('day')
            while (!found) {
              if (r.horaires.length === 7) {
                const ds = moment(dateStart)
                if (dateCursor.isAfter(ds.add(7, 'd'))) {
                    found = true
                }
                const horaire = _.find(r.horaires, (h) => {
                  const dow = moment(dateCursor).add(1, 'm').day()
                  return h.dow[0] === dow
                })
                let openHour = moment(dateCursor)
                openHour.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
                let closeHour = moment(dateCursor)
                closeHour.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })

                if (!!data.isAllDay) {
                  if (dateCursor.format('DDMMYY') === moment().add(tzOffset, 'h').format('DDMMYY')) {
                    range = Math.abs(closeHour.diff(moment().add(tzOffset, 'h'), 'minutes'))
                    range = range - (range % 15)
                  } else {
                    range = Math.abs(closeHour.diff(openHour, 'minutes'))
                  }
                  if (range === 1) {
                    range = 0
                  }
                }

                if (dateCursor.isBefore(openHour)) {
                    dateCursor = openHour
                }
                if (dateCursor.isAfter(closeHour) || !horaire.open) {
                    dateCursor = closeHour.startOf('day').add(1, 'd').add(-15, 'm')
                }
                let testEnd = moment(dateCursor)
                testEnd.add(range, 'm')
                if (
                  range > 0 &&
                  Meteor.call('resourceIsFree', r._id, dateCursor, testEnd, r.condoId, number, r.type === 'edl') &&
                  Meteor.call('resourceIsOpenedWithStart', r._id, dateCursor, r.condoId) &&
                  Meteor.call('resourceIsOpenedWithEnd', r._id, testEnd, r.condoId) &&
                  +dateCursor.format('x') > +moment().add(tzOffset, 'h').format('x')
                ) {
                  const n = Math.floor(Math.random() * 11)
                  const k = Math.floor(Math.random() * 1000000)
                  const m = String.fromCharCode(n) + k
                  let sug = {
                      ...r,
                      name: r.type === 'edl' && r.name === 'edl' ? getResourceTypeByKey('edl', lang) : r.name,
                      start: +dateCursor.format('x'),
                      end: +testEnd.format('x'),
                      key: m
                  }
                  if (multiSpace) {
                    sug.number = number
                  }
                  suggestions.push(sug)
                  found = true
                } else {
                    dateCursor.add(15, 'm')
                }
              } else {
                found = true
              }
            }
          } else {

            // if (!data.onlyNextDay) {
            //   let foundPrev = false
            //   // check for timeslot before selected date
            //   // dateCursor is to mark the date in the loop
            //   let prevDateCursor = moment(dateStart)
            //   while (!foundPrev) {
            //     if (r.horaires.length === 7) {
            //       const ds = moment(dateStart)
            //       if (prevDateCursor.isBefore(ds.add(-7, 'd'))) {
            //         foundPrev = true
            //       }
            //       const horaire = _.find(r.horaires, (h) => {
            //         return h.dow[0] === prevDateCursor.day()
            //       })
            //       let openHour = moment(prevDateCursor)
            //       openHour.set({
            //         hour: parseInt(horaire.start.split(':')[0], 10),
            //         minute: parseInt(horaire.start.split(':')[1], 10)
            //       })
            //       let closeHour = moment(prevDateCursor)
            //       closeHour.set({
            //         hour: parseInt(horaire.end.split(':')[0], 10),
            //         minute: parseInt(horaire.end.split(':')[1], 10)
            //       })
            //
            //       if (!!data.isAllDay) {
            //         if (prevDateCursor.format('DDMMYY') === moment().add(tzOffset, 'h').format('DDMMYY')) {
            //           range = Math.abs(closeHour.diff(moment().add(tzOffset, 'h'), 'minutes'))
            //           range = range - (range % 15)
            //         } else {
            //           range = Math.abs(closeHour.diff(openHour, 'minutes'))
            //         }
            //       }
            //
            //       if (prevDateCursor.isBefore(openHour) || !horaire.open) {
            //         prevDateCursor = openHour.endOf('day').add(-1, 'd')
            //       }
            //       if (prevDateCursor.isAfter(closeHour)) {
            //         prevDateCursor = closeHour
            //       }
            //       let testEnd = moment(prevDateCursor)
            //       testEnd.add(range, 'm')
            //       if (
            //         Meteor.call('resourceIsFree', r._id, prevDateCursor, testEnd, r.condoId, number, r.type === 'edl') &&
            //         Meteor.call('resourceIsOpenedWithStart', r._id, prevDateCursor, r.condoId) &&
            //         Meteor.call('resourceIsOpenedWithEnd', r._id, testEnd, r.condoId) &&
            //         +prevDateCursor.format('x') > +moment().add(tzOffset, 'h').format('x')
            //       ) {
            //         const n = Math.floor(Math.random() * 11)
            //         const k = Math.floor(Math.random() * 1000000)
            //         const m = String.fromCharCode(n) + k
            //         let sug = {
            //           ...r,
            //           name: r.type === 'edl' && r.name === 'edl' ? getResourceTypeByKey('edl', lang) : r.name,
            //           start: +prevDateCursor.format('x'),
            //           end: +testEnd.format('x'),
            //           key: m
            //         }
            //         if (multiSpace) {
            //           sug.number = number
            //         }
            //         suggestions.push(sug)
            //         foundPrev = true
            //       } else {
            //         if (+prevDateCursor.format('x') < +moment().add(tzOffset, 'h').format('x')) {
            //           foundPrev = true
            //         } else {
            //           prevDateCursor.add(-15, 'm')
            //         }
            //       }
            //     } else {
            //       foundPrev = true
            //     }
            //   }
            // }
            // check for timeslot after selected date
            let foundNext = false
            let nextDateCursor = moment(dateStart)
            while (!foundNext) {
              if (r.horaires.length === 7) {
                const ds = moment(dateStart)
                if (nextDateCursor.isAfter(ds.add(7, 'd'))) {
                    foundNext = true
                }
                const horaire = _.find(r.horaires, (h) => { return h.dow[0] === nextDateCursor.day() })
                let openHour = moment(nextDateCursor)
                openHour.set({ hour: parseInt(horaire.start.split(':')[0], 10), minute: parseInt(horaire.start.split(':')[1], 10) })
                let closeHour = moment(nextDateCursor)
                closeHour.set({ hour: parseInt(horaire.end.split(':')[0], 10), minute: parseInt(horaire.end.split(':')[1], 10) })

                if (!!data.isAllDay) {
                  if (nextDateCursor.format('DDMMYY') === moment().add(tzOffset, 'h').format('DDMMYY')) {
                    range = Math.abs(closeHour.diff(moment().add(tzOffset, 'h'), 'minutes'))
                    range = range - (range % 15)
                  } else {
                    range = Math.abs(closeHour.diff(openHour, 'minutes'))
                  }
                  if (range === 1) {
                    range = 0
                  }
                }

                if (nextDateCursor.isBefore(openHour)) {
                    nextDateCursor = openHour
                }
                if (nextDateCursor.isAfter(closeHour) || !horaire.open) {
                    nextDateCursor = closeHour.startOf('day').add(1, 'd').add(-15, 'm')
                }
                let testEnd = moment(nextDateCursor)
                testEnd.add(range, 'm')
                if (
                  range > 0 &&
                  Meteor.call('resourceIsFree', r._id, nextDateCursor, testEnd, r.condoId, number, data.type === 'edl') &&
                  Meteor.call('resourceIsOpenedWithStart', r._id, nextDateCursor, r.condoId) &&
                  Meteor.call('resourceIsOpenedWithEnd', r._id, testEnd, r.condoId) &&
                  +nextDateCursor.format('x') > +moment().add(tzOffset, 'h').format('x')
                ) {
                  const n = Math.floor(Math.random() * 11)
                  const k = Math.floor(Math.random() * 1000000)
                  const m = String.fromCharCode(n) + k
                  let sug = {
                      ...r,
                      name: r.type === 'edl' && r.name === 'edl' ? getResourceTypeByKey('edl', lang) : r.name,
                      start: +nextDateCursor.format('x'),
                      end: +testEnd.format('x'),
                      key: m
                  }
                  if (multiSpace) {
                      sug.number = number
                  }
                  suggestions.push(sug)
                  foundNext = true
                } else {
                  nextDateCursor.add(15, 'm')
                }
              } else {
                foundNext = true
              }
            }
          }
        }
      })

      return suggestions.length > 0 ? _.sortBy(suggestions, 'start') : []
    },
    /*  CHECK IF USER HAS REACH LIMIT OF SIMULTANEOUS BOOKING OF SAME TYPE
         data
         {
             type              (String) TYPE OF RESOURCE TO CHECK
             userId            (String) ID OF THE USER TO CHECK
             start             (String) DATE TO CHECK
         }
         */
    'resourceLimit': function (type, userId, start) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let condosIds
      let resources = []
      let user = Residents.findOne({userId: userId})
      if (!user) {
        let gestionnaire = Enterprises.findOne({'users.userId': userId})
        user = _.find(gestionnaire.users, (u) => { return u.userId === userId })
        if (!user) { return false }
        condosIds = _.map(user.condosInCharge, (c) => { return c.condoId })
      } else {
        condosIds = _.map(user.condos, function (c) { return c.condoId })
      }
      resources = Resources.find({condoId: {$in: condosIds}, type: type}).fetch()

      let nb = 0
      _.each(resources, (r) => {
        const events = Reservations.find({resourceId: r._id}).fetch()
        _.each(events, (e) => {
          if (e.origin === userId && e.start === start) { nb++ }
        })
      })
      return nb >= 6
    },
    'canBookEdl': function (event) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let extraCheck = {}
      if (!!event.edlDirection) {
        extraCheck = {
          edlDirection: event.edlDirection
        }
      }
      const userBook = Reservations.find({
        edlId: { $exists: true },
        condoId: event.condoId,
        origin: event.origin,
        end: {$gte: +moment().format('x')},
        $or: [
          {rejected: false },
          {rejected: { $exists: false }}
        ],
        ...extraCheck
      }).fetch();

      return {
        status: !!userBook && userBook.length === 0,
        date: !!userBook && userBook.length > 0 ? moment(userBook[0].start).format('DD/MM/YYYY') : '',
        time: !!userBook && userBook.length > 0 ? moment(userBook[0].start).format('HH:mm') : ''
      }
    },
    checkReservationBeforeSend({id}) {
      const book = Reservations.findOne(id);
      return (!!book && !book.rejected && !book.pending)
    },
    /**
     *
     * @param condoId
     * @param user
     * @param Sdate
     * @param edlDirection
     */
    'sendEdlMailToResident': function (condoId, user, Sdate, edlDirection) {
      const lang = user.profile.lang || 'fr'
      const condo = Condos.findOne(condoId)
      const translation = new Email(lang)
      const debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')

      const translationResa = new ModuleReservationIndex(lang)
      const direction = translationResa.moduleReservationIndex[!!edlDirection ? edlDirection: 'out'].toLowerCase()

      Meteor.call('sendEmailSync',
        user.emails[0].address,
        condo._id,
        translation.email['reservation'] + translation.email['inventorie'] + ', ' + debut,
        'confirm_edl-' + lang,
        {
          type: direction,
          name: translation.email['edl'],
          building: condo.getNameWithAddress(),
          dates: debut
        }
      )
    },
    /**
     * Send scheduled email to manager upon approved edl book
     *
     * @param condoId
     * @param user
     * @param edlId
     * @param eventId
     * @param Edate
     * @param Sdate
     */
    'sendEdlMail': function (condoId, user, edlId, eventId, Edate, Sdate) {
      const condoContact = CondoContact.findOne({condoId})
      const condo = Condos.findOne(condoId)
      let emailList = []
      if (condoContact) {
        let edlDetailId = DeclarationDetails.findOne({key: 'edl'})
        let edlCondoContact = _.find(condoContact.contactSet, function (contact) {
          return contact.declarationDetailId == edlDetailId._id
        })

        emailList = [condoContact.defaultContact.userId]
        if (edlCondoContact && edlCondoContact.userIds && edlCondoContact.userIds.length > 0) {
          emailList = _.without(edlCondoContact.userIds, null, undefined)
        }
      }

      const sendTime = moment(Edate).add(22, 'hours');

      _.each(emailList, (e) => {
        if (Meteor.call('checkNotifGestionnaire', Meteor.users.findOne(e)._id, condo._id, 'reservations')) {
          const manager = Meteor.users.findOne(e)
          const lang = 'fr' // manager.profile.lang || 'fr' // for now hard coded to FR because only FR template available
          const translation = new Email(lang)

          const debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format('HH[h]mm')

          Meteor.call('scheduleAnEmail', {
            templateName: 'end_edl',
            to: manager.emails[0].address,
            condoId: condo._id,
            subject: condo.getName() + translation.email['confirm_leaving'],
            data: {
              building: condo.getName(),
              date: debut,
              resident: user.profile.firstname + " " + user.profile.lastname,
              yesLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}?edl=${edlId}&answer=1`, condo._id),
              noLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}?edl=${edlId}&answer=0`, condo._id)
            },
            dueTime: new Date(sendTime),
            condition: {
              name: "checkReservationBeforeSend",
              data: {id: eventId}
            }
          });

          // DEBUG PURPOSE SKIP SCHEDULE
          // console.log('send debug email')
          // Meteor.call('sendEmailSync',
          //   manager.emails[0].address,
          //   condo._id,
          //   condo.getName() + translation.email['confirm_leaving'],
          //   'end_edl',
          //   {
          //     building: condo.getName(),
          //     date: debut,
          //     resident: user.profile.firstname + " " + user.profile.lastname,
          //     yesLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}?edl=${edlId}&answer=1`, condo._id),
          //     noLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condo._id}?edl=${edlId}&answer=0`, condo._id)
          //   }
          // )
        }
      })
    },
    /**
     *
     * @param Sdate
     * @param Edate
     * @param condoId
     * @param eventId
     * @param l
     */
    'sendInviteMail': function (Sdate, Edate, condoId, eventId, l) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      try {
        const book = Reservations.findOne({'eventId': eventId});
        const r = Resources.findOne({_id: book.resourceId});

        const booker = Meteor.users.findOne(book.origin)
        const receipents = Meteor.users.find({_id: {$in: book.participants}}).fetch();
        _.each(receipents, (rec) => {
          Meteor.call('checkNotifResident', rec._id, condoId, 'reservation', function(error, result) {
            if (result) {
              const lang = rec.profile.lang
              const translationEmail = new Email(lang);
              let debut, fin, dates;
              if (Sdate.isSame(Edate, 'day')) {
                dates = change_date(Sdate, lang) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
              } else {
                debut = change_date(Sdate, lang) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
                fin = change_date(Edate, lang) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
                dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
              }

              Meteor.call('scheduleAnEmail', {
                to: rec.emails[0].address,
                condoId: condoId,
                subject: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name) + ', ' + dates,
                templateName: 'invited-reservation-' + lang,
                data: {
                  title: !!r.title ? r.title : '',
                  type: r.type,
                  name: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
                  dates: dates,
                  participantNumber: isInt(book.participantNumber) ? book.participantNumber : '-',
                  message: !!book.message ? book.message : '-',
                  start: +Sdate.format('x'),
                  end: +Edate.format('x'),
                  location: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
                  eventName: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
                  isReservation: true
                },
                dueTime: new Date(),
              })
            }
          })
        })

        const translationEmail = new Email(l);
        let debut, fin, dates;
        if (Sdate.isSame(Edate, 'day')) {
          dates = change_date(Sdate, l) + translationEmail.email['_from_'] + Sdate.format('HH[h]mm') + translationEmail.email['_to_'] + Edate.format('HH[h]mm')
        } else {
          debut = change_date(Sdate, l) + translationEmail.email['_at_'] + Sdate.format('HH[h]mm')
          fin = change_date(Edate, l) + translationEmail.email['_at_'] + Edate.format('HH[h]mm')
          dates = translationEmail.email['from2_'] + debut + translationEmail.email['_to'] + fin
        }
        // send email to external participant if any
        _.each(book.externalParticipants, (rec) => {
          Meteor.call('scheduleAnEmail', {
            to: rec.email,
            condoId: condoId,
            subject: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name) + ', ' + dates,
            templateName: 'invited-reservation-external-' + l,
            data: {
              title: !!r.title ? r.title : '',
              type: r.type,
              name: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
              dates: dates,
              participantNumber: isInt(book.participantNumber) ? book.participantNumber : '-',
              message: !!book.message ? book.message : '-',
              start: +Sdate.format('x'),
              end: +Edate.format('x'),
              location: r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name,
              eventName: translationEmail.email['reservation'] + (r.type === 'edl' && r.name === 'edl' ? translationEmail.email['edl'] : r.name),
              isReservation: true,
              condoId: condoId,
              bookedBy: !!booker ? `${booker.profile.firstname} ${booker.profile.lastname}` : ''
            },
            dueTime: new Date(),
          })
        })
      }
      catch(err) {
        console.log(err)
      }
    },
    // commented as not used?
    // /**
    //  *
    //  * @param condoId
    //  */
    // setValidateReservationsHasView: function (condoId) {
    //   if (!Meteor.userId())
    //     throw new Meteor.Error(300, "Not Authenticated");
    //   let event = Reservations.find({
    //     condoId: condoId,
    //     pending: false,
    //     rejected: false
    //   }).fetch()
    //   if (event) {
    //     _.each(event, (e) => {
    //       let eventId = e.eventId
    //       let viewOfResa = ViewOfReservation.findOne({ _id: eventId })
    //       if (viewOfResa) {
    //         let view = viewOfResa.view || {}
    //         view[Meteor.userId()] = Date.now() + 10000
    //         ViewOfReservation.update({ _id: eventId }, {
    //           $set: {
    //             view
    //           }
    //         })
    //       } else {
    //         // Do Nothing
    //       }
    //     })
    //   }
    // },
    // /**
    //  *
    //  * @param evIdent
    //  */
    setValidateReservationHasView: function (eventId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      if (eventId) {
        // let viewOfResa = ViewOfReservation.findOne({ _id: eventId })
        // if (viewOfResa) {
          // let view = viewOfResa.view || {}
          // view[Meteor.userId()] = Date.now() + 10000
          // console.log('VIEW', this.userId, eventId)
          // ViewOfReservation.update({ _id: eventId }, {
          //   $set: {
          //     view
          //   }
          // })
          Reservations.update({_id: eventId}, {
            $addToSet: {userViews: this.userId}
          })
        // } else {
          // Do Nothing
        // }
      }
    },
    'module-reservation-follow-toggle': function (condoId) {
      if (!Match.test(condoId, String))
        throw new Meteor.Error(401, "Invalid parameters \'condoId\' module-edl-follow-toggle");
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let resident = Residents.findOne({ userId: Meteor.userId() });
      if (resident) {
        let condo = _.find(resident.condos, (condo) => { return condo.condoId === condoId });
        if (!condo)
          throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
        Residents.update({
          userId: Meteor.userId(),
          'condos.condoId': condoId
        },
        {
          $set:
            { 'condos.$.notifications.reservation': !condo.notifications.reservation }
        });
      }
      else {
        let gestionnaire = Enterprises.findOne({ "users.userId": Meteor.userId() }) || {}
        let user = gestionnaire.users.find((u) => { return u.userId == Meteor.userId() }) || {}
        let condo = user.condosInCharge.find((c) => { return c.condoId == condoId })
        if (!condo)
          throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
        if (condo.notifications.reservations == undefined) {
          condo.notifications.reservations = true;
        } else {
          condo.notifications.reservations = !condo.notifications.reservations;
        }
        _.each(user.condosInCharge, function (elem) {
          if (elem.condoId === condoId) {
            elem = condo;
          }
        });
        Enterprises.update({
          '_id': gestionnaire._id,
          'users.userId': Meteor.userId()
        },
          {
            $set: { 'users.$.condosInCharge': user.condosInCharge },
          });
      }
    },
    // chart data
    getStatsGraphBookedResources: function (condoId, start, end, lang) {
      const session = new Session();
      session.validateLogged();

      const condosWithPermission = []
      if (condoId === 'all') {
        const condos = session.getValue('condos')
        condos.forEach(c => {
          if (session.userHasPermission(c, 'reservation.see')) {
            condosWithPermission.push(c)
          }
        })
      } else {
        if (session.userHasPermission(condoId, 'reservation.see')) {
          condosWithPermission.push(condoId)
        }
      }

      const resourcesType = getResourceTypes(condoId, condosWithPermission, lang);
      const dataset = _.map(resourcesType, (type, i) => {
        const data = Reservations.find({
          $and: [
            {resourceId: {$in: type.ids}},
            {start: {$gte: start}},
            {start: {$lte: end}},
            condoId === 'all' ? {condoId: {$in: condosWithPermission}} : {condoId: condoId},
            {
              $or: [
                {pending: {$exists: false}},
                {pending: false}
              ]
            },
            {
              $or: [
                {rejected: {$exists: false}},
                {rejected: false}
              ]
            }
          ]
        }).fetch();

        const dataCount = _.reduce(data, (count, d) => {
          return count + moment(d.end).diff(moment(d.start), 'm');
        }, 0);

        return {
          label: type.name,
          value: dataCount,
          color: RESA_CHART_COLOR[i] || '#63fea9'
        }
      })

      console.log('getStatsGraphBookedResources')
      return dataset
    }
  })
})

const RESA_CHART_COLOR = {
  'restaurant': '#fe0900',
  'coworking': '#69d2e7',
  'meeting': '#cff1f8',
  'equipment': '#fdb813',
  'edl': '#8b999f',
  'other': '#e0e0e0'
}

function getResourceTypes (condoId, condosWithPermission, lang) {
  const resources = attachResourceType(Resources.find(condoId === 'all' ? {condoId: {$in: condosWithPermission}} : {condoId: condoId}).fetch(), lang);
  const data = {}

  _.each(resources, (res) => {
    if (!data[res.type]) {
      data[res.type] = {
        name: res.typeName,
        ids: [res._id]
      }
    } else {
      data[res.type].ids.push(res._id)
    }
  });

  return data;
}
