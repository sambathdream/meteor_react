import { Meteor } from 'meteor/meteor'
import { eventFilter } from '/server/sharedFunctions/events'

Meteor.startup(
  function () {
    // PUBLISH ALL NEWS FROM CONDOS IN CHARGE OF CURRENT USER
    Meteor.publish('actus_gestionnaire', function (count, search) {

      /* Checks */
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");
      if (Meteor.call('isGestionnaire', this.userId)) {
        const regexp = !!search ? new RegExp(search, "i") : null;
        // RETRIEVE IDs OF NEWS (module["actu"].data.actuId OF EACH CONDO)
        let self = this;
        let user = Meteor.users.findOne(this.userId);
        let condosInCharge = Meteor.listCondoUserHasRight("actuality", "see")
        let actuList = [];
        for (condo of condosInCharge) {
          let c = Condos.findOne(condo);
          if (c && c.settings && c.settings.options && c.settings.options.informations === true) {
            let m = _.find(c.modules, (e) => {return e.name == "actuality"});
            if (m)
              actuList.push(m.data.actualityId);
          }
        }
        // console.log(ActuPosts.find({
        //   $and: [
        //     !!search ? {
        //       $or: [
        //         { "title": regexp },
        //         { "locate": regexp },
        //         { "description": regexp },
        //       ]
        //     } : {},
        //     { actualityId: { $in: actuList } }
        //   ]
        // }, { sort: { createdAt: -1 }, limit: !!count && _.isNumber(count) ? count : 200 }).count())
        return ActuPosts.find({
            $and: [
              !!search ? {
                $or: [
                  { "title": regexp },
                  { "locate": regexp },
                  { "description": regexp },
                ]
              } : {},
              { actualityId: { $in: actuList } }
            ]
          }, { sort: { createdAt: -1 }, limit: !!count && _.isNumber(count) ? count : 200 })
      }
    });
    Meteor.publish('info_gestionnaire', function (count, search) {
      /* Checks */
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");
      if (Meteor.call('isGestionnaire', this.userId)) {
        const regexp = !!search ? new RegExp(search, "i") : null;
        // RETRIEVE IDs OF NEWS (module["actu"].data.actuId OF EACH CONDO)
        let self = this;
        let user = Meteor.users.findOne(this.userId);
        let condosInCharge = Meteor.listCondoUserHasRight("actuality", "see")
        let actuList = [];
        for (condo of condosInCharge) {
          let c = Condos.findOne(condo);
          if (c && c.settings && c.settings.options && c.settings.options.informations === true) {
            let m = _.find(c.modules, (e) => {return e.name == "actuality"});
            if (m)
              actuList.push(m.data.actualityId);
          }
        }
        return ActuPosts.find({
          $and: [
            !!search ? {
              $or: [
                { "title": regexp },
                { "locate": regexp },
                { "description": regexp },
              ]
            } : {},
            { actualityId: { $in: actuList } },
            { startDate: { $eq: '' } }
          ]
        }, { sort: { createdAt: -1 }, limit: !!count && _.isNumber(count) ? count : 200 })
      }
    });
    // PUBLISH ALL ACTUS THAT ARE EVENTS
    Meteor.publish('ActuPostsEvents', () => {
      /* Checks */
      if (!this.user)
          throw new Meteor.Error(300, "Not Authenticated");
      if (Meteor.call('isGestionnaire', this.user._id)) {
          const condoIds = _.map(_.find(Enterprises.findOne({'users.userId': this.user._id}, {fields: {users: true}}).users, (user) => { return user.userId === this.user._id}).condosInCharge, 'condoId')
          return ActuPosts.find({startDate: {$ne: ''}, condoId: {$in: condoIds}}, {sort: {createdAt: -1}});
      }
    });
    // PUBLISH ALL USER FILES WHEN USER IS MANAGER
    Meteor.publish('UserFilesAll', () => {
      /* Checks */
      if (!this.user)
          throw new Meteor.Error(300, "Not Authenticated");
      if (Meteor.call('isGestionnaire', this.user._id)) {
          return UserFiles.find().cursor;
      }
    });
    // PUBLISH ALL ACTUS RELATED TO ID RECEIVED AS PARAMETER
    Meteor.publish('actus', function (id) {
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");
        let resident = Residents.findOne({userId: this.userId});
      if (!resident)
          throw new Meteor.Error(301, "Access denied", "User dont have 'resident' identity");
      return Actus.find(id);
    });
    // PUBLISH ALL ACTUPOST RELATED TO POSTID RECEIVED AS PARAMETER
    Meteor.publish('infosOccupantFeed', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      return ActuPosts.find({ condoId: condoId, $or: [{startDate: {$exists: false}}, {startDate: ""}] }, { sort: { updatedAt: -1 }, limit: 5 })

    })
    Meteor.publish('eventsOccupantFeed', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      return ActuPosts.find({
        condoId: condoId,
        $and: [{startDate: {$exists: true}}, {startDate: {$ne: ""}}],
        ...Meteor.call('getNewEventFilter', this.userId)
      }, { sort: { updatedAt: -1 } })
    })

    Meteor.publish('ActuPosts', function (postId) {
      if (!this.userId) {
        throw new Meteor.Error(300, 'Not Authenticated')
      }
      let resident = Residents.findOne({ userId: this.userId })
      if (!resident) {
        throw new Meteor.Error(301, 'Access denied', "User dont have 'resident' identity")
      }
        
      return ActuPosts.find({
        ...postId,
        ...eventFilter(this.userId)
      }, { sort: { createdAt: -1 } })
    })
    // PUBLISH ALL FILES RELATED TO ACTUPOST RECEIVED AS PARAMETER
    Meteor.publish('actuFiles', function (actuIds) {
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");
      if (Array.isArray(actuIds) !== true) {
        actuIds = [actuIds];
      }
      return UserFiles.find({ $and:
        [
          {'meta.actualityId': {$exists: true} },
          {'meta.actualityId': {$in: actuIds} },
        ]
      }).cursor;
    });

    Meteor.publish('eventCommentsFeedCounter', function(condoIds) {
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");

      let self = this;
      let handlerFeed = {}
      condoIds.forEach((condoId) => {
        if (EventCommentsFeed[condoId] === undefined || !EventCommentsFeed[condoId]) {
          name = "EventCommentsFeed" + condoId;
          EventCommentsFeed[condoId] = new Mongo.Collection(name);
        }
        let feed = EventCommentsFeed[condoId].find();
        let result = [];

        handlerFeed[condoId] = feed.observe({
          added: function (document) {
            if (!result[document.eventId]) {
              result[document.eventId] = 1;
              self.added("eventCommentsFeedCounter", document.eventId, {_id: document.eventId, counter: 1});
            }
            else {
              result[document.eventId] += 1;
              self.changed("eventCommentsFeedCounter", document.eventId, {_id: document.eventId, counter: result[document.eventId]});
            }
          },
          removed: function (oldDocument) {
            if (result[oldDocument.eventId]) {
              result[oldDocument.eventId] -= 1;
              if (result[oldDocument.eventId] < 1) {
                delete result[oldDocument.eventId];
                self.removed("eventCommentsFeedCounter", oldDocument.eventId);
              }
              else
                self.changed("eventCommentsFeedCounter", oldDocument.eventId, {_id: oldDocument.eventId, counter: result[oldDocument.eventId]});
            }
          },
        });
      })
      this.onStop(() => {
        _.each(handlerFeed, (handler) => {
          handler.stop()
        })
        return true
      })
      self.ready();
    })

    Meteor.publish('eventCommentsFeed', function(condoId, eventId) {
      if (!this.userId)
        throw new Meteor.Error(300, "Not Authenticated");

      let self = this;
      if (EventCommentsFeed[condoId] === undefined || !EventCommentsFeed[condoId]) {
        name = "EventCommentsFeed" + condoId;
        EventCommentsFeed[condoId] = new Mongo.Collection(name);
      }
      let feed = EventCommentsFeed[condoId].find({eventId: eventId});

      const handlerFeed = feed.observe({
        added: function (document) {
          self.added("eventCommentsFeed", document._id, document);
        },
        changed: function (newDocument, oldDocument) {
          self.changed("eventCommentsFeed", oldDocument._id, newDocument);
        },
        removed: function (oldDocument) {
          self.removed("eventCommentsFeed", oldDocument._id);
        },
      });
      this.onStop(() => {
        return handlerFeed.stop()
      })
      self.ready();
      return UserFiles.find({"meta.eventId": eventId}).cursor;
    });
});
