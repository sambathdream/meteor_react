import { Email, DeletePost} from './../../../common/lang/lang';
import moment from 'moment';

import { eventFilter } from '/server/sharedFunctions/events'

Meteor.startup(function() {
	Meteor.methods({
    'getNewEventFilter': function (userId) {
      return eventFilter(userId)
    },
		// possible response is going, maybe, not
		'eventResponse': function (id, response) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      check(id, String);
      check(response, String);
      const event = ActuPosts.findOne(id);
      if (!event)
        throw new Meteor.Error(601, "Actu not found");

      if (response === 'going' && !!event.isLimited && !!event.participantLimit && event.participantLimit > 0 && !!event.participantResponse && !!event.participantResponse.going && event.participantLimit <= event.participantResponse.going.length) {
        throw new Meteor.Error(602, "Limit reached");
      }

      const participantResponse = !!event.participantResponse ? event.participantResponse: {}
      const responseOptions = ['going', 'maybe', 'not'];
      const newResponse = responseOptions.reduce((memo, res) => {
        memo[res] = !!participantResponse[res] ? (
          res === response ? _.uniq([...participantResponse[res], Meteor.userId()]) : participantResponse[res].filter(r => r !== Meteor.userId())
        ) : (
          res === response ? [Meteor.userId()] : []
        )

        return memo
      }, {})

      ActuPosts.update(id, {
        ...event,
        participantResponse: newResponse
      });
    },
		'eventResponseStatus': function (id) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      check(id, String);
      const event = ActuPosts.findOne(id);
      if (!event)
        throw new Meteor.Error(601, "Actu not found");

      let state = false;
      if (!!event.allowAction) {
        const isParticipant = !!event.participants && !!event.participants.find((p) => { return p === Meteor.userId()})
        if (event.visibility === 'public' || isParticipant) {

          const existInGoing = !!event.participantResponse && !!event.participantResponse.going && !!event.participantResponse.going.find((p) => { return p === Meteor.userId()})
          const existInMaybe = !!event.participantResponse && !!event.participantResponse.maybe && !!event.participantResponse.maybe.find((p) => { return p === Meteor.userId()})
          const existInNot = !!event.participantResponse && !!event.participantResponse.not && !!event.participantResponse.not.find((p) => { return p === Meteor.userId()})

          if (existInGoing) {
            state = 'going'
          } else if (existInMaybe) {
            state = 'maybe'
          } else if (existInNot) {
            state = 'not'
          }
        }
      }

      return state
    },
		'canResponseToEvent': function (id) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      check(id, String);
      const event = ActuPosts.findOne(id);
      if (!event)
        throw new Meteor.Error(601, "Actu not found");

      let state = false;
      if (!!event.allowAction) {
        const isParticipant = !!event.participants && !!event.participants.find((p) => { return p === Meteor.userId()})
        if (event.visibility === 'public' || isParticipant) {
          const existInGoing = !!event.participantResponse && !!event.participantResponse.going && !!event.participantResponse.going.find((p) => { return p === Meteor.userId()})
          const existInMaybe = !!event.participantResponse && !!event.participantResponse.maybe && !!event.participantResponse.maybe.find((p) => { return p === Meteor.userId()})
          const existInNot = !!event.participantResponse && !!event.participantResponse.not && !!event.participantResponse.not.find((p) => { return p === Meteor.userId()})

          if (!existInGoing && !existInMaybe && !existInNot) {
            state = true
          }
        }
      }

      return state
    },
    'postEventComment': function (condoId, eventId, message, files) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      check(eventId, String);
      check(message, String);
      check(condoId, String);
      const date = Date.now();

      EventCommentsFeed[condoId].insert({
        eventId: eventId,
        message: message,
        files: files,
        date: date,
        isEdited: false,
        userId: Meteor.userId()
      });
      const post = ActuPosts.findOne(eventId);
      ActuPosts.update({_id: eventId}, {
        $set: {
          userUpdateView: [this.userId]
        },
        $inc: { commentCount: 1}
      });
      let residentsIds = Meteor.call('getResidentsOfCondo', condoId).filter((id) => {
        return id !== Meteor.userId()
      });
      // filter only to participant if event is private
      if (!!post.visibility && post.visibility === 'private') {
        const temp = residentsIds;
        residentsIds = [];
        if (!!post.participants && post.participants.length > 0) {
          residentsIds = temp.filter((id) => {
            return post.participants.includes(id)
          })
        }
      }

      OneSignal.MbNotification.sendToUserIds(residentsIds, {
        type: 'actuality',
        key: 'new_comment',
        dataId: eventId,
        condoId: condoId,
        data: {
          post: null,
          event: post
        }
      })
    },
    'editEventComment': function (condoId, commentId, message, files) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      check(commentId, String);
      check(message, String);
      check(condoId, String);
      const date = Date.now();

      let thisFeed = EventCommentsFeed[condoId].findOne(commentId);
      if (thisFeed) {
        EventCommentsFeed[condoId].update({
          _id: commentId
        }, {
          $set: {
            message: message,
            files: files,
            isEdited: date
          }
        });

        let removedFiles = _.difference(thisFeed.files, files);

        _.each(removedFiles, function(fileId) {
          UserFiles.remove({_id: fileId});
        })

        _.each(files, function(fileId) {
          UserFiles.update({_id: fileId}, {$set: {"meta.eventId": thisFeed.eventId}});
        })
      }
    },
    'deleteEventComment': function(condoId, commentId) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      let thisFeed = EventCommentsFeed[condoId].findOne(commentId);
      if (thisFeed) {
        _.each(thisFeed.files, function(fileId) {
          UserFiles.remove({_id: fileId});
        })
        ActuPosts.update({_id: thisFeed.eventId}, {
          $inc: {commentCount: -1}
        })
        EventCommentsFeed[condoId].remove({_id: commentId});
      }
    },
		/*    CREATE A NEW POST
		data                                  (Object) CONTAINS DATA FOR THE CREATION
		{
		title                             (String) TITLE
		locate                            (String) LOCATION
		description                       (String) DESCRIPTION
		startDate                         (String) START DATE DD/MM/YYYY
		startHour                         (String) START TIME MM:HH
		endDate                           (String) END DATE DD/MM/YYYY
		endHour                           (String) END TIME MM:HH
		userId                            (String) USERID OF AUTHOR
		createdAt                         (Date) DATE OF CREATION
		views                             (Table of Object) TABLE TO TRACK VIEWS
		files                             (Table of String) FILEIDs FROM "UserFiles" TABLE
    condoId                           (String) CONDOID OF CONDO RELATED TO THE POST
    condoIds                          (Array) CONDOID OF CONDO RELATED TO THE POST
		}
		*/
		'module-actu-create-post': function(post) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			check(post.title, String);
			if (post.locate)
				check(post.locate, String);
			check(post.description, String);
			check(post.userId, String);
			let condoIds = [post.condoId]
      if (_.isArray(post.condoIds) && post.condoIds.length > 0) {
        condoIds = post.condoIds
      }

      let postId
      const loop = _.map(condoIds, (c) => {
        post.condoId = c;

        const condo = Condos.findOne({ _id: { $eq: c } });
        const actu = _.find(condo.modules, (m) => m.name === 'actuality')
        if (!!actu) {
          post.actualityId = actu.data.actualityId

          console.log(post, 'hehe')

          let pushNotificationUserIds = []
          post.type = 'information';
          if (post.startDate) {
            post.type = 'event';
            if (post.startHour == '-1') {

              let enStartDate = post.startDate.split("/");
              if (enStartDate.length === 1) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              var Sdate = new Date(`${enStartDate[1]}/${enStartDate[0]}/${enStartDate[2]}`);
              if (!Sdate.getDate()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              post.startUtcTimestamp = +moment.utc(post.startDate, 'DD/MM/YYYY').format('x');
            }
            else {

              if (!post.startHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$')) {
                throw new Meteor.Error(401, "Hours: Invalid format");
              }

              let enStartDate = post.startDate.split("/");
              if (enStartDate.length === 1) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              var Sdate = new Date(`${enStartDate[1]}/${enStartDate[0]}/${enStartDate[2]}`);
              if (!Sdate.getDate()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              Sdate.setHours(post.startHour.split(':')[0], post.startHour.split(':')[1]);
              if (Sdate.getTime() < new Date().getTime()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              post.startUtcTimestamp = +moment.utc(post.startDate, 'DD/MM/YYYY').set({
                hour: parseInt(post.startHour.split(':')[0], 10),
                minute: parseInt(post.startHour.split(':')[1], 10)
              }).format('x');
            }
          }
          if (post.endDate) {

            if (post.endHour == '-1') {

              let enEndDate = post.endDate.split("/");
              if (enEndDate.length === 1) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              var Edate = new Date(`${enEndDate[1]}/${enEndDate[0]}/${enEndDate[2]}`);
              if (!Edate.getDate()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              post.endUtcTimestamp = +moment.utc(post.endDate, 'DD/MM/YYYY').format('x');
            }
            else {
              if (!post.startDate) {
                throw new Meteor.Error(401, "Date: No start date");
              }
              if (!post.endHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$')) {
                throw new Meteor.Error(401, "Hours: Invalid format");
              }

              let enEndDate = post.endDate.split("/");
              if (enEndDate.length === 1) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              let Edate = new Date(`${enEndDate[1]}/${enEndDate[0]}/${enEndDate[2]}`);
              if (!Edate.getDate()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              Edate.setHours(post.endHour.split(':')[0], post.endHour.split(':')[1]);
              if (Edate.getTime() < new Date().getTime() || Edate.getTime() <= Sdate.getTime()) {
                throw new Meteor.Error(401, "Date: Invalid date");
              }

              post.endUtcTimestamp = +moment.utc(post.endDate, 'DD/MM/YYYY').set({
                hour: parseInt(post.endHour.split(':')[0], 10),
                minute: parseInt(post.endHour.split(':')[1], 10)
              }).format('x');
            }
          }
          post.title = post.title ? post.title.replace(new RegExp('\"', 'g'), '“') : '';
          post.locate = post.locate ? post.locate.replace(new RegExp('\"', 'g'), '“') : '';
          post.description = post.description ? post.description.replace(new RegExp('\"', 'g'), '“') : '';
          post.createdBy = this.userId;
          post.userUpdateView = [this.userId];
          post.userViews = [this.userId];
          post.createdAt = Date.now();
          post.updatedAt = Date.now();
          post.viewsCount = 1;
          post.commentCount = 0;
          if (!!post.visibility && post.visibility === 'private') {
            if (!!post.participants) {
              post.participants = _.uniq([...post.participants, Meteor.userId()])
            }
          }
          let attachments = []
          if (Array.isArray(post.files) && post.files.length > 0) {
            const newFilesId = post.files.filter(file => typeof file !== 'string');
            const filesId = post.files.filter(file => typeof file === 'string');
            const fileDatas = UserFiles.find({_id: {$in: filesId}});
            attachments = [...newFilesId, ...fileDatas.map(fileData => ({
              fileId: fileData._id,
              mime: fileData.type,
              ext: fileData.extension
            }))]
          }
          post.attachments = attachments;

          postId = ActuPosts.insert(post);
          Meteor.call('addToGestionnaireHistory',
            post.startDate ? 'event' : 'info',
            postId,
            post.startDate ? 'addEvent' : 'addInfo',
            post.condoId,
            post.title,
            Meteor.userId()
          );
          _.each(post.files, (f) => {
            UserFiles.update({_id: f},
              {
                $set: {
                  'meta.actualityId': post.actualityId
                }
              });
          });

          // ENVOI DE L EMAIL
          let date = new Date();
          date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
          let userId = this.userId;
          let residentsIds = Meteor.call('getResidentsOfCondo', post.condoId);
          let tmpCondo = Condos.findOne(post.condoId);
          let condoName = tmpCondo.name ? tmpCondo.name : tmpCondo.info.address + ", " + tmpCondo.info.code + " " + tmpCondo.info.city;
          let splitDate = post.startDate.split("/");
          let rappelDate = null;
          if (post.startDate) {
            let timediff = moment(Sdate).diff(moment(), 'days');
            if (timediff > 1) {
              let rplDate = moment(Sdate);
              rplDate.add(-1, 'days');
              rplDate.add(1, 'hours');
              rappelDate = new Date(parseInt(rplDate.format('x')));
            }
          }

          // filter only to participant if event is private
          if (!!post.visibility && post.visibility === 'private') {
            const temp = residentsIds;
            residentsIds = [];
            if (!!post.participants && post.participants.length > 0) {
              residentsIds = temp.filter((id) => {
                return post.participants.includes(id)
              })
            }
          }

          residentsIds.forEach((id) => {
            if (id !== userId) {
              let user = Meteor.users.findOne({_id: id});
              if (!user) return
              let lang = user.profile.lang || 'fr';
              var translation = new Email(lang);
              post.title = (post.title.length > 50 ? (post.title.substr(0, 50) + '...') : post.title);
              if (Meteor.userHasRight("actuality", "see", post.condoId, id)) {
                pushNotificationUserIds.push(id);
                Meteor.call('checkNotifResident', id, post.condoId, 'actualite', function (error, result) {
                  if (result) {
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-actu-' + lang,
                      to: user.emails[0].address,
                      condoId: post.condoId,
                      subject: translation.email['new_message'],
                      isEvent: !!post.startUtcTimestamp,
                      data: {
                        module: "actus",
                        postId: postId,
                        title: translation.email['publish_new_info'],
                        condo: condoName,
                        sujet: post.title,
                        locate: post.locate,
                        startDate: post.startDate,
                        endDate: post.endDate,
                        startHour: (post.startHour != '-1' ? post.startHour : ""),
                        endHour: (post.endHour != '-1' ? post.endHour : ""),
                        condoId: post.condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/resident/${post.condoId}/profile/notifications`, post.condoId),
                        isSameDate: (post.startDate === post.endDate),
                        checkStartHour: (post.startHour != '-1'),
                        checkEndHour: (post.endHour != '-1'),
                        setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${post.condoId}/information?date=${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`, post.condoId),
                        start: !!post.startUtcTimestamp ? post.startUtcTimestamp : '',
                        end: !!post.endUtcTimestamp ? post.endUtcTimestamp : (!!post.startUtcTimestamp ? post.startUtcTimestamp + 1 : '')
                      },
                      dueTime: date,
                      condition: {
                        name: "checkActuBeforeSend",
                        data: {actualityId: post.actualityId, userId: id, condoId: post.condoId, createdAt: Date.now()}
                      }
                    });
                    if (post.startDate && rappelDate !== null) {
                      Meteor.call('scheduleAnEmail', {
                        templateName: 'rappel-actu-' + lang,
                        to: user.emails[0].address,
                        condoId: post.condoId,
                        subject: translation.email['new_message_reminder'],
                        isEvent: !!post.startUtcTimestamp,
                        data: {
                          module: "actus",
                          postId: postId,
                          title: translation.email['reminder_event'],
                          condo: condoName,
                          sujet: post.title,
                          locate: post.locate,
                          startDate: post.startDate,
                          endDate: post.endDate,
                          startHour: (post.startHour != '-1' ? post.startHour : ""),
                          endHour: (post.endHour != '-1' ? post.endHour : ""),
                          condoId: post.condoId,
                          notifUrl: Meteor.absoluteUrl(`${lang}/resident/${post.condoId}/profile/notifications`, post.condoId),
                          isSameDate: (post.startDate === post.endDate),
                          checkStartHour: (post.startHour != '-1'),
                          checkEndHour: (post.endHour != '-1'),
                          setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${post.condoId}/information?date=${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`, post.condoId),
                          start: !!post.startUtcTimestamp ? post.startUtcTimestamp : '',
                          end: !!post.endUtcTimestamp ? post.endUtcTimestamp : (!!post.startUtcTimestamp ? post.startUtcTimestamp + 1 : '')
                        },
                        dueTime: rappelDate
                      });
                    }
                  }
                });
              }
            }
          });
          let result = Meteor.call('getGestionnaireOfCondoInCharge', post.condoId);
          result.forEach((id) => {
            if (id !== userId) {
              if (Meteor.userHasRight("actuality", "see", post.condoId, id)) {
                pushNotificationUserIds.push(id);
                Meteor.call('checkNotifGestionnaire', id, post.condoId, 'actualites', function (error, result) {
                  if (result) {
                    let lang = Meteor.users.findOne({_id: id}).profile.lang || 'fr';
                    var translation = new Email(lang);
                    post.title = (post.title.length > 50 ? (post.title.substr(0, 50) + '...') : post.title);
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-actu-' + lang,
                      to: Meteor.users.findOne({_id: id}).emails[0].address,
                      condoId: post.condoId,
                      subject: translation.email['new_message'],
                      isEvent: !!post.startUtcTimestamp,
                      data: {
                        module: "actus",
                        postId: postId,
                        title: translation.email['publish_new_info'],
                        condo: condoName,
                        sujet: post.title, // $(post.title).text()
                        locate: post.locate,
                        startDate: post.startDate,
                        endDate: post.endDate,
                        startHour: (post.startHour != '-1' ? post.startHour : ""),
                        endHour: (post.endHour != '-1' ? post.endHour : ""),
                        condoId: post.condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, post.condoId),
                        isSameDate: (post.startDate === post.endDate),
                        checkStartHour: (post.startHour != '-1'),
                        checkEndHour: (post.endHour != '-1'),
                        setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/${post.startDate ? 'evenements/' : 'innformations'}`, post.condoId),
                        start: !!post.startUtcTimestamp ? post.startUtcTimestamp : '',
                        end: !!post.endUtcTimestamp ? post.endUtcTimestamp : (!!post.startUtcTimestamp ? post.startUtcTimestamp + 1 : '')
                      },
                      dueTime: date,
                      condition: {
                        name: "checkActuBeforeSend",
                        data: {actualityId: post.actualityId, userId: id, condoId: post.condoId, createdAt: Date.now()}
                      }
                    });
                    if (post.startDate && rappelDate !== null) {
                      Meteor.call('scheduleAnEmail', {
                        templateName: 'rappel-actu-' + lang,
                        to: Meteor.users.findOne({_id: id}).emails[0].address,
                        condoId: post.condoId,
                        subject: translation.email['new_message_reminder'],
                        isEvent: !!post.startUtcTimestamp,
                        data: {
                          module: "actus",
                          postId: postId,
                          title: translation.email['reminder_event'],
                          condo: condoName,
                          sujet: post.title, // $(post.title).text()
                          locate: post.locate,
                          startDate: post.startDate,
                          endDate: post.endDate,
                          startHour: (post.startHour != '-1' ? post.startHour : ""),
                          endHour: (post.endHour != '-1' ? post.endHour : ""),
                          condoId: post.condoId,
                          notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, post.condoId),
                          isSameDate: (post.startDate === post.endDate),
                          checkStartHour: (post.startHour != '-1'),
                          checkEndHour: (post.endHour != '-1'),
                          setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/${post.startDate ? 'evenements/' : 'innformations'}`, post.condoId),
                          start: !!post.startUtcTimestamp ? post.startUtcTimestamp : '',
                          end: !!post.endUtcTimestamp ? post.endUtcTimestamp : (!!post.startUtcTimestamp ? post.startUtcTimestamp + 1 : '')
                        },
                        dueTime: rappelDate
                      });
                    }
                  }
                })
              }
            }
          })
          OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
            type: 'actuality',
            key: !post.startDate ? 'new_info' : 'new_event',
            dataId: postId,
            condoId: post.condoId,
            data: {
              post: !post.startDate ? {'_id': postId, ...post} : null,
              event: !!post.startDate ? {'_id': postId, ...post} : null
            }
          })
          if (rappelDate !== null) {
            OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
              type: 'actuality',
              key: 'reminder_event',
              dataId: postId,
              condoId: post.condoId,
              data: {
                post: !post.startDate ? {'_id': postId, ...post} : null,
                event: !!post.startDate ? {'_id': postId, ...post} : null
              }
            }, rappelDate, (result, msg) => {
              if (!result) {
                console.log('Error: ', msg);
                return false;
              }
              if (result) {
                ReminderNotificationsId.insert({
                  postId: postId,
                  pushId: result
                })
              }
            })
          }
        }

        return true
      }); // eof loop
      return postId
		},
		/* EDIT AN EXISTING POST
		postId                                (String) ID OF THE POST TO EDIT
		modifier                              (Object) CONTAINS DATA FOR THE EDITION
		{
		title                             (String) TITLE
		locate                            (String) LOCATION
		description                       (String) DESCRIPTION
		startDate                         (String) START DATE DD/MM/YYYY
		startHour                         (String) START TIME MM:HH
		endDate                           (String) END DATE DD/MM/YYYY
		endHour                           (String) END TIME MM:HH
		actualityId                       (String) ID OF MODULE ("actus" TABLE)
		userId                            (String) USERID OF AUTHOR
		createdAt                         (Date) DATE OF CREATION
		views                             (Table of Object) TABLE TO TRACK VIEWS
		files                             (Table of String) FILEIDs FROM "UserFiles" TABLE
		condoId                           (String) CONDOID OF CONDO RELATED TO THE POST
		}
		*/
		'module-actu-edit-post': function(postId, modifier) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let actu = ActuPosts.findOne(postId);
			if (!actu)
				throw new Meteor.Error(601, "Actu not found");
			check(modifier.title, String);
			if (modifier.locate)
				check(modifier.locate, String);
			check(modifier.description, String);
			check(modifier.actualityId, String);
			check(modifier.userId, String);
			modifier.createdAt = actu.createdAt;
			let rappelDate = null;
			if (modifier.startDate) {
				if (modifier.startHour == '-1') {
					let enStartDate = modifier.startDate.split("/");

					if (enStartDate.length == 1)
						throw new Meteor.Error(401, "Date: Invalid date");

					var Sdate = new Date(`${enStartDate[1]}/${enStartDate[0]}/${enStartDate[2]}`);
					if (!Sdate.getDate())
						throw new Meteor.Error(401, "Date: Invalid date");

					modifier.startUtcTimestamp = +moment(modifier.startDate, 'DD/MM/YYYY').format('x');

				}
				else {
					if (!modifier.startHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$'))
						throw new Meteor.Error(401, "Hours: Invalid format");

					let enStartDate = modifier.startDate.split("/");
					if (enStartDate.length == 1)
						throw new Meteor.Error(401, "Date: Invalid date");

					var Sdate = new Date(`${enStartDate[1]}/${enStartDate[0]}/${enStartDate[2]}`);
					if (!Sdate.getDate())
						throw new Meteor.Error(401, "Date: Invalid date");

					Sdate.setHours(modifier.startHour.split(':')[0], modifier.startHour.split(':')[1]);
					if (Sdate.valueOf() < new Date().valueOf())
						throw new Meteor.Error(401, "Date: Invalid date");

					modifier.startUtcTimestamp = +moment(modifier.startDate, 'DD/MM/YYYY').set({ hour: parseInt(modifier.startHour.split(':')[0], 10), minute: parseInt(modifier.startHour.split(':')[1], 10) }).format('x');
				}
				let timediff = moment(Sdate).diff(moment(), 'days');
				if (timediff > 1) {
					let rplDate = moment(Sdate);
					rplDate.add(-1, 'days');
					rplDate.add(1, 'hours');
					rappelDate = new Date(parseInt(rplDate.format('x')));
				}
			}
			if (modifier.endDate) {
				if (modifier.endHour == '-1') {
					if (!modifier.startDate)
						throw new Meteor.Error(401, "Date: No start date");

					let enEndDate = modifier.endDate.split("/");
					if (enEndDate.length == 1)
						throw new Meteor.Error(401, "Date: Invalid date");

					let Edate = new Date(`${enEndDate[1]}/${enEndDate[0]}/${enEndDate[2]}`);
					if (!Edate.getDate())
						throw new Meteor.Error(401, "Date: Invalid date");

					modifier.endUtcTimestamp = +moment(modifier.endDate, 'DD/MM/YYYY').format('x');
				}
				else {
					if (!modifier.startDate)
						throw new Meteor.Error(401, "Date: No start date");

					if (!modifier.endHour.match('^([0-1][0-9]|2[0-3]):([0-5][0-9])$'))
						throw new Meteor.Error(401, "Hours: Invalid format");

					let enEndDate = modifier.endDate.split("/");
					if (enEndDate.length == 1)
						throw new Meteor.Error(401, "Date: Invalid date");

					let Edate = new Date(`${enEndDate[1]}/${enEndDate[0]}/${enEndDate[2]}`);
					if (!Edate.getDate())
						throw new Meteor.Error(401, "Date: Invalid date");

					Edate.setHours(modifier.endHour.split(':')[0], modifier.endHour.split(':')[1]);
					if (Edate.valueOf() < new Date().valueOf() || Edate.valueOf() <= Sdate.valueOf())
						throw new Meteor.Error(401, "Date: Invalid date");

					modifier.endUtcTimestamp = +moment(modifier.endDate, 'DD/MM/YYYY').set({ hour: parseInt(modifier.endHour.split(':')[0], 10), minute: parseInt(modifier.endHour.split(':')[1], 10) }).format('x');
				}
			}
			modifier.title = modifier.title.replace(new RegExp('\"', 'g'), '“');
			modifier.locate = modifier.locate.replace(new RegExp('\"', 'g'), '“');
			modifier.description = modifier.description.replace(new RegExp('\"', 'g'), '“');
			let postfiles = UserFiles.find({$and:
				[
					{'meta.actualityId': {$exists: true} },
					{'meta.actualityId': {$eq: actu.actualityId} },
					{_id: {$in: actu.files} },
					{_id: {$nin: modifier.files} },
				]
			}).fetch();
			_.each(postfiles, function(elem) {
				UserFiles.remove(elem._id);
			});
			_.each(modifier.files, (f) => {
				UserFiles.update({_id: f},
					{$set:
						{
							'meta.actualityId': actu.actualityId
						}
					});
			});
			Meteor.call('addToGestionnaireHistory',
				actu.startDate ? 'event' : 'info',
				postId,
				actu.startDate ? 'editEvent' : 'editInfo',
				modifier.condoId,
				modifier.title,
				Meteor.userId()
			);
			modifier.views = actu.views;
      if (!!modifier.visibility && modifier.visibility === 'private') {
        if (!!modifier.participants) {
          modifier.participants = _.uniq([...modifier.participants, Meteor.userId()])
        }
      }
      if (modifier.endDate || modifier.startDate) {
        modifier.userUpdateView = [this.userId];
      }
      let attachments = []
      if (Array.isArray(modifier.files) && modifier.files.length > 0) {
        const newFilesId = modifier.files.filter(file => typeof file !== 'string');
        const filesId = modifier.files.filter(file => typeof file === 'string');
        const fileDatas = UserFiles.find({_id: {$in: filesId}});
        attachments = [...newFilesId, ...fileDatas.map(fileData => ({
          fileId: fileData._id,
          mime: fileData.type,
          ext: fileData.extension
        }))]
      }
      modifier.attachments = attachments;
      const newData = {...actu, ...modifier}

			ActuPosts.update(postId, newData);

			let pushNotificationUserIds = [];
			let residentsIds = Meteor.call('getResidentsOfCondo', modifier.condoId);

      // filter only to participant if event is private
      if (!!modifier.visibility && modifier.visibility === 'private') {
        const temp = residentsIds;
        residentsIds = [];
        if (!!modifier.participants && modifier.participants.length > 0) {
          residentsIds = temp.filter((id) => {
            return modifier.participants.includes(id)
          })
        }
      }

			residentsIds.forEach((id) => {
				if (id !== Meteor.userId()) {
					if (Meteor.userHasRight("actuality", "see", modifier.condoId, id)) {
						pushNotificationUserIds.push(id);
					}
				}
			});
			Meteor.call('getGestionnaireOfCondoInCharge', modifier.condoId, function (error, result) {
				result.forEach((id) => {
					if (id !== Meteor.userId()) {
						if (Meteor.userHasRight("actuality", "see", modifier.condoId, id)) {
							pushNotificationUserIds.push(id);
						}
					}
				})
			});
			if (rappelDate !== null && (actu.startUtcTimestamp !== modifier.startUtcTimestamp || actu.endUtcTimestamp !== modifier.endUtcTimestamp)) {
				CronEmails.remove({
					'data.postId': postId
				});

				let splitDate = newData.startDate.split("/");
				if (newData.startDate && rappelDate !== null) {
					for (const userId of pushNotificationUserIds) {
						let user = Meteor.users.findOne({ _id: userId });
						let lang = user.profile.lang || 'fr';
						var translation = new Email(lang);
						let tmpCondo = Condos.findOne(newData.condoId);
						let condoName = tmpCondo.name ? tmpCondo.name : tmpCondo.info.address + ", " + tmpCondo.info.code + " " + tmpCondo.info.city;
						Meteor.call('scheduleAnEmail', {
							templateName: 'rappel-actu-' + lang,
							to: user.emails[0].address,
							condoId: newData.condoId,
							subject: translation.email['new_message_reminder'],
              isEvent: !!newData.startUtcTimestamp,
							data: {
								module: "actus",
								postId: postId,
								title: translation.email['reminder_event'],
								condo: condoName,
                sujet: newData.title,
                locate: newData.locate,
								startDate: newData.startDate,
								endDate: newData.endDate,
								startHour: (newData.startHour != '-1' ? newData.startHour : ""),
								endHour: (newData.endHour != '-1' ? newData.endHour : ""),
								condoId: newData.condoId,
								notifUrl: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, newData.condoId) : Meteor.absoluteUrl(`${lang}/resident/${newData.condoId}/profile/notifications`, newData.condoId),
								isSameDate: (newData.startDate === newData.endDate),
								checkStartHour: (newData.startHour != '-1'),
								checkEndHour: (newData.endHour != '-1'),
								setReplyLink: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/${newData.startDate ? 'evenements/' : 'innformations'}`, newData.condoId) : Meteor.absoluteUrl(`${lang}/resident/${newData.condoId}/information?date=${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`, newData.condoId),
                start: !!newData.startUtcTimestamp ? newData.startUtcTimestamp: '',
                end: !!newData.endUtcTimestamp ? newData.endUtcTimestamp: (!!newData.startUtcTimestamp ? newData.startUtcTimestamp + 1: '')
							},
							dueTime: rappelDate
						});
					}
				}
				let reminderPush = ReminderNotificationsId.findOne({ postId: postId });
				if (reminderPush) {
					OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
						if (err) {
							console.log('Error: ', err);
							return false;
						}
					})
					ReminderNotificationsId.remove({ postId: postId })
					OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
						type: 'actuality',
						key: 'change_event',
						dataId: postId,
						condoId: modifier.condoId,
						data: {
							post: !modifier.startDate ? { '_id': postId, ...modifier } : null,
							event: !!modifier.startDate ? { '_id': postId, ...modifier } : null
						}
					})
					OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
							type: 'actuality',
							key: 'reminder_event',
							dataId: postId,
							condoId: modifier.condoId,
							data: {
								post: !modifier.startDate ? { '_id': postId, ...modifier } : null,
								event: !!modifier.startDate ? { '_id': postId, ...modifier } : null
							}
					}, rappelDate, (result, msg) => {
						if (!result) {
							console.log('Error: ', msg);
							return false;
						}
						if (result) {
							ReminderNotificationsId.insert({
								postId: postId,
								pushId: result
							})
						}
					})
				}
			}
		},
		// DELETE AN EXISTING POST
		// id                                    (String) ID OF ACTUPOST ("ActuPosts" TABLE)
		'module-actu-post-delete': function(id) {
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let actu = ActuPosts.findOne(id);
			if (!actu)
				throw new Meteor.Error(601, "Actu post not found");
			let postfiles = UserFiles.find({$and:
				[
					{'meta.actualityId': {$exists: true} },
					{'meta.actualityId': {$eq: actu.actualityId} },
					{_id: {$in: actu.files} },
				]
			}).fetch();
			_.each(postfiles, function(elem) {
				UserFiles.remove(elem._id);
			});
			Meteor.call('addToGestionnaireHistory',
				actu.startDate ? 'event' : 'info',
				id,
				actu.startDate ? 'deleteEvent' : 'deleteInfo',
				actu.condoId,
				actu.title,
				Meteor.userId()
			);
      let pushNotificationUserIds = [];

      // filter only to participant if event is private
      if (actu.visibility && actu.visibility === 'private') {
        console.log('PRIVATE EVENT')
        pushNotificationUserIds = [...(actu.participants || [])]
      } else {
        console.log('PUBLIC EVENT')
        let residentsIds = Meteor.call('getResidentsOfCondo', actu.condoId);
        residentsIds.forEach((id) => {
          if (id !== Meteor.userId()) {
            if (Meteor.userHasRight("actuality", "see", actu.condoId, id)) {
              pushNotificationUserIds.push(id);
            }
          }
        });
        Meteor.call('getGestionnaireOfCondoInCharge', actu.condoId, function (error, result) {
          result.forEach((id) => {
            if (id !== Meteor.userId()) {
              if (Meteor.userHasRight("actuality", "see", actu.condoId, id)) {
                pushNotificationUserIds.push(id);
              }
            }
          })
        });
      }
      console.log('PARticipants', pushNotificationUserIds)
			if (actu.startDate) {
				let reminderPush = ReminderNotificationsId.findOne({ postId: id });
				if (reminderPush) {
					OneSignal.cancelNotification(reminderPush.pushId, function (err, httpResponse, data) {
						if (err) {
							console.log('Error: ', err);
							return false;
						}
					})
          ReminderNotificationsId.remove({ postId: id })
        }
        OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
          type: 'actuality',
          key: 'cancel_event',
          dataId: id,
          condoId: actu.condoId
        })
			}
			CronEmails.remove({
				'data.postId': id
			});
			// Do the delete after PN !
			ActuPosts.remove(id);
			if (actu.startDate !== '') {
				let tmpCondo = Condos.findOne(actu.condoId);
				let splitDate = actu.startDate.split("/");
				for (const userId of pushNotificationUserIds) {
					let user = Meteor.users.findOne(userId);
					if (user) {
						let lang = user.profile.lang || 'fr';
						let translation = new DeletePost(lang);
						Meteor.call('sendEmailSync',
							user.emails[0].address,
							actu.condoId,
							translation.deletePost['event_delete'],
							'delete-event-' + lang,
							{
								condoId: actu.condoId,
								condo: tmpCondo.getName(),
								title: actu.title,
								startDate: actu.startDate,
								endDate: actu.endDate,
								startHour: (actu.startHour != '-1' ? actu.startHour : ""),
								endHour: (actu.endHour != '-1' ? actu.endHour : ""),
								notifUrl: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, actu.condoId) : Meteor.absoluteUrl(`${lang}/resident/${actu.condoId}/profile/notifications`, actu.condoId),
								isSameDate: (actu.startDate === actu.endDate),
								checkStartHour: (actu.startHour != '-1'),
								checkEndHour: (actu.endHour != '-1'),
								setReplyLink: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/${actu.startDate ? 'evenements/' : 'innformations'}`, actu.condoId) : Meteor.absoluteUrl(`${lang}/resident/${actu.condoId}/information?date=${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`, actu.condoId)
							}
						);
					}
				}
			}
			if (actu.userId != Meteor.userId()) {
				let user = Meteor.users.findOne(actu.userId);
				if (user) {
					let lang = user.profile.lang || 'fr';
					let translation = new DeletePost(lang);
          const condo = Condos.findOne(actu.condoId);
					Meteor.call('sendEmailSync',
						user.emails[0].address,
						actu.condoId,
						translation.deletePost['subject'],
						'delete-post-' + lang,
						{
              building: !!condo ? condo.getName() : '',
							category : actu.startDate == '' ? translation.deletePost["info"] : translation.deletePost["event"],
							title : actu.title,
							description : actu.description
						}
					);
				}
			}
		},
		// FOLLOW / UNFOLLOW AN ACTU MODULE
		// actualityId                           (String) ID OF THE MODULE TO FOLLOW / UNFOLLOW
		'module-follow-actu': function(condoId) {
			if (!Match.test(condoId, String))
				throw new Meteor.Error(401, "Invalid parameters actus module-follow-actu");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let resident = Residents.findOne({userId: Meteor.userId()});
			if (resident) {
				let condo = _.find(resident.condos, (condo) => {return condo.condoId === condoId});
				if (!condo)
					throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
				let value = condo.notifications.actualite;
				Residents.update({userId: Meteor.userId(),
					'condos.condoId': condoId},
					{$set:
						{'condos.$.notifications.actualite': !value}
					});
			}
			else {
				let gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()});
				let user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()});
				let condo = user.condosInCharge.find((c) => {return c.condoId == condoId});
				if (!condo)
					throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
				if (condo.notifications.actualites == undefined)
					condo.notifications.actualites = true;
				else
					condo.notifications.actualites = !condo.notifications.actualites;
				_.each(user.condosInCharge, function(elem) {
					if (elem.condoId === condoId) {
						elem = condo;
					}
				});
				Enterprises.update({
					'_id': gestionnaire._id,
					'users.userId': Meteor.userId()
				},
				{
					$set: {'users.$.condosInCharge': user.condosInCharge},
				});
			}
		},
    // UPDATE THE DATE VIEW OF MODULE OF CURRENT USER BY ID
    // actualityId                           (String) ID OF THE MODULE TO UPDATE
    'module-actu-view-by-id': function(actualityId, id) {
      if (!Meteor.userId())
        throw new Meteor.Error(300, "Not Authenticated");
      console.log('READING', actualityId, id)
      ActuPosts.update({
        _id: id,
        userViews: {$not: { $elemMatch: { $eq: Meteor.userId() } } }
      }, {
        $addToSet: {
          views: Meteor.userId(),
          userViews: Meteor.userId(),
        },
        $inc: {viewsCount: 1}
      });

      ActuPosts.update({
        _id: id
      }, {
        $addToSet: {
          userUpdateView: Meteor.userId(),
        }
      });

      if (!actualityId) { return }
      console.log('update info', actualityId)
      let post = Actus.findOne(actualityId);
      if (!post)
        throw new Meteor.Error(601, "Actu not found");
      let views = _.find(post.views, (e) => {return e.userId === Meteor.userId()});

      if (views) {
        Actus.update({_id: actualityId, "views.userId": Meteor.userId()},
          {$set: {"views.$.date": Date.now()}}
        );
      }
      else {
        Actus.update(actualityId, {
          $push: {
            views: {userId: Meteor.userId(), date: Date.now()}
          }
        });
      }
    },
	});
});
