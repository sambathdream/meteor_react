import {Email, ModuleReservationIndex} from './../../../common/lang/lang';

function change_date(date, lang) {
    let day = new Date(date).getDay();
    let month = new Date(date).getMonth();
    if (lang == 'en') {
        let dayList = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
        ];
        let monthList = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ]
        return dayList[day] + ', ' + date.format("DD") + ' ' + monthList[month];
    }
    else {
        let dayList = [
            'Dimanche',
            'Lundi',
            'Mardi',
            'Mercredi',
            'Jeudi',
            'Vendredi',
            'Samedi',
        ];
        let monthList = [
            "Janvier",
            "Février",
            "Mars",
            "Avril",
            "Mai",
            "Juin",
            "Juillet",
            "Aout",
            "Septembre",
            "Octobre",
            "Novembre",
            "Décembre"
        ]
        return dayList[day] + ' ' + date.format("DD") + ' ' + monthList[month];
    }
}

Meteor.startup(function() {
    Meteor.methods({
        /*  CONFIRM A REQUEST OF SCHEDULE OF FIXTURE FROM MANAGER SIDE
         edlId                                   (String) ID OF SCHEDULE
         message                                 (String) MESSAGE THAT WILL BE SENT TO RESIDENT
         */
        'confirmEDL': function(edlId, message) {
            EDL.update({_id: edlId},
                {
                    $set: {
                        status: "Confirmé"
                    }
                });
            let edl = EDL.findOne(edlId);
            Resources.update({_id: edl.resourceId, "events.edlId": edlId},
                {
                    $set: {"events.$.pending": false}
                });
            let r = Resources.findOne(edl.resourceId);
            let e = _.find(r.events, (e) => {return e.edlId === edlId});
            let Sdate = moment(edl.date);
            let Edate = moment(Sdate);
            Edate.minutes(Edate.minutes() + r.edlDuration);
            let debut = "", fin = "", dates = "";
            let lang = Meteor.users.findOne(edl.origin).profile.lang || 'fr';
            var translation = new Email(lang);
            if (Sdate.isSame(Edate, 'day')) {
                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
            }
            else {
                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
            }
            let condoId = Resources.findOne(edl.resourceId).condoId;
            let condo = Condos.findOne(condoId);
            message = (message.length > 50 ? (message.substr(0, 50) + '...') : message);
            Meteor.call('sendEmailEvent', {
                to: Meteor.users.findOne(edl.origin).emails[0].address,
                condoId: condoId,
                eventName: "Etat des lieux",
                subject: translation.email['confirmation_inventories'] + dates,
                template_name: "edl-" + lang,
                data: {
                    building: condo.name,
                    answerEdl: translation.email['accept'],
                    dates: dates,
                    message: message,
                    buttonText: translation.email['cancel'],
                    cancelLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/reservation?removee=${e.eventId}&remover=${edl.resourceId}`, condoId)
                },
                start: Sdate,
                end: Edate,
                location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code
            });

            // Mail 1h avant pour le resident
            var sendTime = moment(Sdate).subtract(180, 'minutes');
            message = (message.length > 50 ? (message.substr(0, 50) + '...') : message);
            if (Meteor.call('checkNotifResident', edl.origin, condoId, 'edl')) {
                Meteor.call('scheduleAnEmail', {
                    templateName: 'reminder_edl-' + lang,
                    to: Meteor.users.findOne(edl.origin).emails[0].address,
                    condoId: condoId,
                    subject: translation.email['remind_invent'] + dates,
                    data: {
                        building: condo.name,
                        date: dates,
                        gestionnaire: Meteor.users.findOne(this.userId).profile.firstname + ' ' + Meteor.users.findOne(this.userId).profile.lastname,
                        message : message
                    },
                    dueTime: new Date(sendTime),
                    condition: {
                        name: "checkEdlBeforeSend",
                        data: {edlId: edl._id, date: new Date(sendTime)}
                    }
                });
            }

			const condoContact = CondoContact.findOne({condoId: condo._id});
			let edlDetailId = DeclarationDetails.findOne({key: "edl"});
			let edlCondoContact = _.find(condoContact.contactSet, function(contact) {
				return contact.declarationDetailId == edlDetailId._id;
			})

			let gestDefaultEntry;
			let emailList = [condoContact.defaultContact.userId];
			if (edlCondoContact && edlCondoContact.userIds.length > 0)
				emailList = _.without(edlCondoContact.userIds, null, undefined);


            if (!emailList[this.userId] && emailList[this.userId] != undefined)
                emailList.push(this.userId);
            // let user = Meteor.users.findOne(this.userId);
            sendTime = moment(Edate).add(22, 'hours');
            _.each(emailList, (elem) => {
                let user = Meteor.users.findOne(elem);
                let lang = user.profile.lang || 'fr';
                var translation = new Email(lang);
                if (Sdate.isSame(Edate, 'day')) {
                    debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                    fin   = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                    dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                }
                else {
                    debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                    fin   = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                    dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                }
                let occupant = Meteor.users.findOne(edl.origin);
                occupant = occupant.profile.firstname + " " + occupant.profile.lastname;

                Meteor.call('scheduleAnEmail', {
                    templateName: 'end_edl',
                    to: user.emails[0].address,
                    condoId: condo._id,
                    subject: condo.name + translation.email['confirm_leaving'],
                    data: {
                        building: condo.name,
                        date: dates,
                        resident: occupant,
                        yesLink: Meteor.absoluteUrl(`${lang}/gestionnaire/edl?edl=${edl._id}&answer=1`, condo._id),
                        noLink: Meteor.absoluteUrl(`${lang}/gestionnaire/edl?edl=${edl._id}&answer=0`, condo._id)
                    },
                    dueTime: new Date(sendTime),
                    condition: {
                        name: "checkEdlBeforeSend",
                        data: {edlId: edl._id, date: new Date(sendTime)}
                    }
                });
                let event = _.find(r.events, (e) => {return e.edlId === edl._id});

                Meteor.call('sendEmailEvent', {
                    to: user.emails[0].address,
                    condoId: condo._id,
                    eventName: "Etat des lieux",
                    subject: "Confirmation d'état des lieux, " + debut,
                    template_name: "edlManager",
                    data: {
                        building: condo.name,
                        answerEdl: translation.email['accept'],
                        dates: dates,
                        message: message,
                        buttonText: translation.email['cancel'],
                        occupant: occupant,
                        cancelLink: Meteor.absoluteUrl(`${lang}/resident/${r.condoId}/reservation?removee=${event.eventId}&remover=${edl.resourceId}`, condo._id)
                    },
                    start: Sdate,
                    end: Edate,
                    location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code
                });
            });

            //ENVOIE EMAIL AVEC AJOUT CALENDRIER PERSO POUR GESTIONNAIRE (uniquement celui qui a valider)




        },
        /*  CANCEL AN EXISTING SCHEDULE OF FIXTURE, FROM RESIDENT OR MANAGER SIDE
         edlId                                   (String) ID OF SCHEDULE
         fromGestionnaire                        (Boolean) TRUE IF FROM MANAGER SIDE, FALSE FOR RESIDENT
         message                                 (String) MESSAGE THAT WILL BE SENT TO RESIDENT IF FROM MANAGER
         */
        'cancelEDL': function(edlId, fromGestionnaire, message) {
            let debut = "", fin = "", dates = "";
            EDL.update({_id: edlId}, {$set: {status: "Annulé"}});
            let edl = EDL.findOne(edlId);
            let c = Condos.findOne({_id: edl.condoId});
            Reservations.remove({edlId: edlId})
            let r = Resources.findOne({_id: edl.resourceId});
            let Sdate = moment(edl.date);
            let Edate = moment(Sdate);
            Edate.minutes(Edate.minutes() + r.edlDuration);
            let condoId = Resources.findOne({_id: edl.resourceId}).condoId;
            if (fromGestionnaire) {
                let user = Meteor.users.findOne(edl.origin);
                if (user) {
                    let lang = Meteor.users.findOne(edl.origin).profile.lang || 'fr';
                    var translation = new Email(lang);
                    console.log("address1:")
                    console.log(user.emails[0].address)
                    if (Sdate.isSame(Edate, 'day')) {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                    }
                    else {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                    }
                    Meteor.call('sendEmailSync',
                        user.emails[0].address,
                        condoId,
                        translation.email['cancel_inventorie'] + dates,
                        "cancel_edl-" + lang,
                        {
                            title: translation.email['canceled_inventories'],
                            dates: dates,
                            message: message,
                            buildingName: c.info.address == c.name ? c.info.address +', '+ c.info.code+' '+c.info.city : c.info.name,
                            buttonText: translation.email['access_plan'],
                            cancelLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/reservation`, condoId)}
                    );
                }
            } else {
				const condoContact = CondoContact.findOne({condoId: c._id});
				let edlDetailId = DeclarationDetails.findOne({key: "edl"});
				let edlCondoContact = _.find(condoContact.contactSet, function(contact) {
					return contact.declarationDetailId == edlDetailId._id;
				})

				let gestDefaultEntry;
				let emailList = [condoContact.defaultContact.userId];
				if (edlCondoContact && edlCondoContact.userIds.length > 0)
					emailList = _.without(edlCondoContact.userIds, null, undefined);

                _.each(emailList, (e) => {
                    let lang = Meteor.users.findOne(e).profile.lang || 'fr';
                    console.log('lang', lang)
                    var translation = new Email(lang);
                    console.log("address2:")
                    console.log(user.emails[0].address)
                    if (Sdate.isSame(Edate, 'day')) {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                    }
                    else {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                    }
                    Meteor.call('sendEmailSync',
                        Meteor.users.findOne(e).emails[0].address,
                        condoId,
                        translation.email['cancel_inventorie'] + dates,
                        "cancel_edl-" + lang,
                        {
                            title: translation.email['canceled_inventorie'],
                            dates: dates,
                            message: "",
                            buildingName: c.info.address == c.name ? c.info.address +', '+ c.info.code+' '+c.info.city : c.info.name,
                            buttonText: translation.email['access_plan'],
                            cancelLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${condoId}`, condoId)
                        }
                    );
                });
            }
        },
        /*  REFUSE A SCHEDULE OF FIXTURE FROM MANAGER SIDE
         edlId                                   (String) ID OF SCHEDULE
         message                                 (String) MESSAGE THAT WILL BE SENT TO RESIDENT THROUGH EMAIL
         */
        'refuseEDL': function(edlId, message) {
            EDL.update({_id: edlId},
                {
                    $set: {
                        status: "Refusé"
                    }
                });
            let edl = EDL.findOne(edlId);
            Resources.update({_id: edl.resourceId, "events.edlId": edlId},
                {
                    $pull: {events: {edlId: edlId}}
                });

            let r = Resources.findOne({_id: edl.resourceId});
            let Sdate = moment(edl.date);
            let Edate = moment(Sdate);
            Edate.minutes(Edate.minutes() + r.edlDuration);
            let debut = "", fin = "", dates = "";
            let lang = Meteor.users.findOne(edl.origin).profile.lang || 'fr';
            var translation = new Email(lang);
            if (Sdate.isSame(Edate, 'day')) {
                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
            }
            else {
                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
            }
            let condoId = Resources.findOne({_id: edl.resourceId}).condoId;
            let condo = Condos.findOne(condoId);
            Meteor.call('sendEmailSync',
                Meteor.users.findOne(edl.origin).emails[0].address,
                condoId,
                translation.email['refusal_invent'] + dates,
                "edl-" + Meteor.users.findOne(edl.origin).profile.lang,
                {
                    building: condo.name,
                    answerEdl: translation.email['refusal'],
                    dates: dates,
                    message: message,
                    buttonText: translation.email['propose_new_date'],
                    cancelLink: Meteor.absoluteUrl(`${Meteor.users.findOne(edl.origin).profile.lang}/resident/${condoId}/reservation`, condoId)
                }
            );
        },
        /*  ACCEPT / REFUSE A SCHEDULE OF FIXTURE FROM RESIDENT SIDE
         edlId                                   (String) ID OF SCHEDULE
         answer                                  (Char) "1" IF SCHEDULE IS ACCEPTED, "0" IF REFUSED
         */
        'acceptEDL': function(edlId, answer) {
            const translation1 = new ModuleReservationIndex((FlowRouter.getParam("lang") || "fr"));

            let edl = EDL.findOne(edlId);
            if (edl !== undefined && edl.origin === Meteor.userId() && edl.status === "En attente") {
                EDL.update({_id: edlId}, {$set: {status: (answer === "1" ? translation1.moduleReservationIndex["confirmed"] : translation1.moduleReservationIndex["rejected"])}});
                if (answer === "1") {
                    let gestionnaire = Enterprises.findOne({condos: edl.condoId});
                    let c = Condos.findOne(edl.condoId);
					const condoContact = CondoContact.findOne({condoId: c._id});
					let edlDetailId = DeclarationDetails.findOne({key: "edl"});
					let edlCondoContact = _.find(condoContact.contactSet, function(contact) {
						return contact.declarationDetailId == edlDetailId._id;
					})

					let gestDefaultEntry;
					let emailList = [condoContact.defaultContact.userId];
					if (edlCondoContact && edlCondoContact.userIds.length > 0)
						emailList = _.without(edlCondoContact.userIds, null, undefined);

                    let Sdate = moment(edl.date);
                    let r = Resources.findOne({_id: edl.resourceId});
                    let Edate = moment(Sdate);
                    Edate.minutes(Edate.minutes() + r.edlDuration);
                    var sendTime = moment(Edate).add(22, 'hours');
                    let condo = Condos.findOne(r.condoId);
                    let debut = "", fin = "", dates = "";
                    let occupant = Meteor.users.findOne(edl.origin);
                    occupant = occupant.profile.firstname + " " + occupant.profile.lastname;
                    let event = _.find(r.events, (e) => {return e.edlId === edl._id});
                    _.each(emailList, (e) => {
                        let user = Meteor.users.findOne(e);
                        let lang = user.profile.lang || 'fr';
                        var translation = new Email(lang);
                        if (Sdate.isSame(Edate, 'day')) {
                            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                            dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                        }
                        else {
                            debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                            fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                            dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                        }
                        Meteor.call('scheduleAnEmail', {
                            templateName: 'end_edl',
                            to: user.emails[0].address,
                            condoId: edl.condoId,
                            subject: c.name + translation.email['confirm_leaving'],
                            data: {
                                building: c.name,
                                date: debut,
                                resident: user.profile.firstname + " " + user.profile.lastname,
                                yesLink: Meteor.absoluteUrl(`${lang}/gestionnaire/edl?edl=${edlId}&answer=1`, edl.condoId),
                                noLink: Meteor.absoluteUrl(`${lang}/gestionnaire/edl?edl=${edlId}&answer=0`, edl.condoId)
                            },
                            dueTime: new Date(sendTime),
                            condition: {
                              name: "checkEdlBeforeSend",
                              data: {edlId: edl._id, date: new Date(sendTime)}
                            }
                        });
                        Meteor.call('sendEmailEvent', {
                            to: user.emails[0].address,
                            condoId: edl.condoId,
                            eventName: "Etat des lieux",
                            subject: "Confirmation d'état des lieux, " + debut,
                            template_name: "edlManager",
                            data: {
                                building: condo.name,
                                answerEdl: translation.email['accept'],
                                dates: debut,
                                message: "",
                                buttonText: translation.email['cancel'],
                                occupant: occupant,
                                cancelLink: Meteor.absoluteUrl(`${Meteor.users.findOne(edl.origin).profile.lang}/resident/${r.condoId}/reservation?removee=${event.eventId}&remover=${edl.resourceId}`, edl.condoId)
                            },
                            start: Sdate, end: Edate, location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code
                        });
                    });

                    // Mail 1h avant pour le resident
                    var sendTime = moment(Sdate).subtract(180, 'minutes');
                    let lang = Meteor.users.findOne(edl.origin).profile.lang || 'fr';
                    var translation = new Email(lang);
                    if (Sdate.isSame(Edate, 'day')) {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                    } else {
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                        dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                    }
                    if (Meteor.call('checkNotifResident', Meteor.userId(), condo._id, 'edl')) {
                        Meteor.call('scheduleAnEmail', {
                            templateName: 'reminder_edl-' + Meteor.users.findOne(edl.origin).profile.lang || "fr",
                            to: Meteor.user().emails[0].address,
                            condoId: edl.condoId,
                            subject: translation.email['remind_invent'] + " " + debut,
                            data: {
                                building: condo.name,
                                date: debut,
                                gestionnaire: Meteor.users.findOne(this.userId).profile.firstname + ' ' + Meteor.users.findOne(this.userId).profile.lastname,
                                message : ""
                            },
                            dueTime: new Date(sendTime),
                            condition: {
                              name: "checkEdlBeforeSend",
                              data: {edlId: edl._id, date: new Date(sendTime)}
                            }
                        });
                    }
                    event.commentaire = (event.commentaire.length > 50 ? (event.commentaire.substr(0, 50) + '...') : event.commentaire);
                    Meteor.call('sendEmailEvent', {
                        to: Meteor.users.findOne(edl.origin).emails[0].address,
                        condoId: edl.condoId,
                        eventName: translation.email['inventorie'],
                        subject: translation.email['confirmation_inventories'] + debut,
                        template_name: "edl-" + Meteor.users.findOne(edl.origin).profile.lang || "fr",
                        data: {
                            building: condo.name,
                            answerEdl: translation.email['accept'],
                            dates: debut,
                            message: event.commentaire,
                            buttonText: translation.email['cancel'],
                            cancelLink: Meteor.absoluteUrl(`${Meteor.users.findOne(edl.origin).profile.lang}/resident/${event.condoId}/reservation?removee=${event.eventId}&remover=${edl.resourceId}`, edl.condoId)
                        },
                        start: Sdate, end: Edate, location: condo.info.address + ', ' + condo.info.city + ' ' + condo.info.code
                    });

                }
                else if (answer === "0") {

                    let user = Meteor.users.findOne(edl.origin);
                    let Sdate = moment(edl.date);
                    let Edate = moment(Sdate);
                    let c = Condos.findOne({_id: edl.condoId});

                    if (user) {
                        let lang = user.profile.lang || 'fr';
                        var translation = new Email(lang);
                        console.log("address3:")
                        console.log(user.emails[0].address)
                        debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                        Meteor.call('sendEmailSync',
                            user.emails[0].address,
                            edl.condoId,
                            translation.email['cancel_inventorie'] + debut,
                            "cancel_edl-" + lang,
                            {
                                title: translation.email['canceled_inventories'],
                                dates: debut,
                                message: "",
                                buildingName: c.info.address == c.name ? c.info.address +', '+ c.info.code+' '+c.info.city : c.info.name,
                                buttonText: translation.email['access_plan'],
                                cancelLink: Meteor.absoluteUrl(`${lang}/resident/${edl.condoId}/reservation`, edl.condoId)
                            }
                        );
                    }
					const condoContact = CondoContact.findOne({condoId: c._id});
					let edlDetailId = DeclarationDetails.findOne({key: "edl"});
					let edlCondoContact = _.find(condoContact.contactSet, function(contact) {
						return contact.declarationDetailId == edlDetailId._id;
					})

					let gestDefaultEntry;
					let emailList = [condoContact.defaultContact.userId];
					if (edlCondoContact && edlCondoContact.userIds.length > 0)
						emailList = _.without(edlCondoContact.userIds, null, undefined);

                    _.each(emailList, (e) => {
                            let lang = Meteor.users.findOne(e).profile.lang || 'fr';
                            var translation = new Email(lang);
                            console.log("address4:")
                            console.log(Meteor.users.findOne(e).emails[0].address)
                            if (Sdate.isSame(Edate, 'day')) {
                                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                                dates = change_date(Sdate, lang) + translation.email['_from_'] + Sdate.format("HH[h]mm") + translation.email['_to_'] + Edate.format("HH[h]mm");
                            } else {
                                debut = change_date(Sdate, lang) + translation.email['_at_'] + Sdate.format("HH[h]mm");
                                fin = change_date(Edate, lang) + translation.email['_at_'] + Edate.format("HH[h]mm");
                                dates = translation.email['from2_'] + debut + translation.email['_to'] + fin;
                            }
                            Meteor.call('sendEmailSync',
                                Meteor.users.findOne(e).emails[0].address,
                                edl.condoId,
                                translation.email['cancel_inventorie'] + debut,
                                "cancel_edl-" + 'fr',
                                {
                                    title: translation.email['canceled_inventorie'],
                                    dates: debut,
                                    message: "",
                                    buildingName: c.info.address == c.name ? c.info.address +', '+ c.info.code+' '+c.info.city : c.info.name,
                                    buttonText: translation.email['access_plan'],
                                    cancelLink: Meteor.absoluteUrl(`${lang}/gestionnaire/planning/${edl.condoId}`, edl.condoId)
                                }
                            );
                    });


                    Resources.update({"events.edlId": edlId},
                        {
                            $pull: {events: {edlId: edlId}}
                        });
                }
                else
                    return (0);
                return (1);
            }
            return (0);
        },
        'module-edl-follow-toggle': function(condoId) {
            if (!Match.test(condoId, String))
                throw new Meteor.Error(401, "Invalid parameters \'condoId\' module-edl-follow-toggle");
            if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated");
            let resident = Residents.findOne({ userId: Meteor.userId() });
            if (resident) {
                let condo = _.find(resident.condos, (condo) => { return condo.condoId === condoId });
                if (!condo)
                    throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
                Residents.update({
                    userId: Meteor.userId(),
                    'condos.condoId': condoId
                },
                    {
                        $set:
                            { 'condos.$.notifications.edl': !condo.notifications.edl }
                    });
            }
            else {
                let gestionnaire = Enterprises.findOne({ "users.userId": Meteor.userId() });
                let user = gestionnaire.users.find((u) => { return u.userId == Meteor.userId() });
                let __condo = user.condosInCharge.find((c) => { return c.condoId == condoId });
                if (__condo.notifications.edl == undefined)
                    __condo.notifications.edl = true;
                else
                    __condo.notifications.edl = !__condo.notifications.edl;
                _.each(user.condosInCharge, function (elem) {
                    if (elem.condoId === condoId) {
                        elem = __condo;
                    }
                });
                Enterprises.update({
                    '_id': gestionnaire._id,
                    'users.userId': Meteor.userId()
                },
                    {
                        $set: { 'users.$.condosInCharge': user.condosInCharge },
                    });
            }
        },
        checkEdlBeforeSend({edlId, date}) {
            let edl = EDL.findOne(edlId);
            if (edl && edl.status == "Confirmé")
                return true;
            else
                return false
        }
    });
});
