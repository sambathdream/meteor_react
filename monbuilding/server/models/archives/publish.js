Meteor.startup(function () {
  Meteor.publish('archives', function () {
    if (Meteor.user())
      return [ Archives.find() ];
  });
});