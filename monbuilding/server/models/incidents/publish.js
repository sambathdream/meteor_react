/*PUBLISH ALL INCIDENTS AND FILES RELATED TO CURRENT RESIDENT USER, OF SPECIFIC CONDO*/
/*condoId                                     (String) ID OF CONDO*/
Meteor.publish('module_incidents_resident', function (condoId) {
	if (!this.userId)
		throw new Meteor.Error(300, "Not Authenticated");
	if (!Meteor.call('isResidentOfCondo', this.userId, condoId))
		throw new Meteor.Error(302, "Access Denied for id");

	let userId = this.userId;
	let resident = Residents.findOne({"userId": this.userId});
	let self = this;
	let residentCondo = _.find(resident.condos, (e) => { return e.condoId === condoId });
	let IncidentQuery = {
    condoId: condoId,
    $and: [
      {$or: [
        {public: {$ne: false}},
        {participants: userId}
      ]},
      {$or: [
        {'state.status': { $in: [1, 2] }},
        {$and: [
          { "state.status": { $in: [0, 3] } },
          { "declarer.userId": userId }
        ]}
      ]}
    ]
  }
	let residentRole = UsersRights.findOne({
		$and: [
			{ "userId": userId },
			{ "condoId": condoId }
		]
	})
	let residentRoleId = null
	if (!!residentRole) {
		let residentRoleId = residentRole.defaultRoleId;
		let isOccupant = !!DefaultRoles.findOne({ _id: residentRoleId, name: 'Occupant' })
		if (!isOccupant) {
			// don't touch ! Ask to me ! @vmariot
			let field = 'history.share.' + residentRoleId + '.state'
			IncidentQuery.$or[1].$or.push({
				$and: [
					{ "state.status": 3 },
					{ [field]: true }
				]
			})
		}
	}
	let query = Incidents.find(IncidentQuery);
	let incidentsIds = _.map(query.fetch(), (i) => {return i._id;});
	let incidentsFiles = IncidentFiles.find({ $and:
		[
		{"meta.incidentId": {$exists: true}},
		{"meta.incidentId": {$in: incidentsIds}}
		]
	}).cursor;

	const handler = query.observeChanges({
		changed: function(id) {
			let i = Incidents.findOne({_id: id});
			update_resident_incident("changed", self, i);
		},
		added: function(id) {
			let i = Incidents.findOne({_id: id});
			update_resident_incident("added", self, i);
		}
	});

	let incidentType = IncidentType.find({$or: [{condoId: condoId}, {condoId: "default"}]});
	if (incidentType.fetch() == undefined || incidentType.fetch()[0] == undefined) {
		incidentType = IncidentType.find({condoId: "default"});
	}

	let incidentDetails = IncidentDetails.find({$or: [{condoId: condoId}, {condoId: "default"}]});
	if (incidentDetails.fetch() == undefined || incidentDetails.fetch()[0] == undefined) {
		incidentDetails = IncidentDetails.find({condoId: "default"});
	}

	let incidentPriority = IncidentPriority.find({$or: [{condoId: condoId}, {condoId: "default"}]});
	if (incidentPriority.fetch() == undefined || incidentPriority.fetch()[0] == undefined) {
		incidentPriority = IncidentPriority.find({condoId: "default"});
	}

	let incidentArea = IncidentArea.find({$or: [{condoId: condoId}, {condoId: "default"}]});
	if (incidentArea.fetch() == undefined || incidentArea.fetch()[0] == undefined) {
		incidentArea = IncidentArea.find({condoId: "default"});
	}

	let thisCondo = Condos.findOne(condoId);
	let condoType = "";
	if (thisCondo)
		condoType = thisCondo.settings.condoType;
	let contactManagement = ContactManagement.find({$or: [{condoId: condoId}, {condoId: "default"}, {forCondoType: condoType}]});
	if (contactManagement.fetch() == undefined || contactManagement.fetch()[0] == undefined) {
		contactManagement = ContactManagement.find({condoId: "default", forCondoType: condoType});
	}

  this.onStop(() => {
    return handler.stop()
  })
	this.ready();

	return [incidentType, incidentDetails, incidentPriority, incidentArea, IncidentTitle.find(), incidentsFiles, contactManagement, DeclarationDetails.find()];

	/* ADD INCIDENT i TO THE CLIENT ONLY COLLECTION "residentIncidents" */
	/*DELETE PRIVATE FIELDS OF INCIDENT i*/
	function update_resident_incident(action, self, i) {
		/*INCIDENT TO ADD*/
		let rI = {
			_id : i._id,
			createdAt: i.createdAt,
			condoId: i.condoId,
			lastUpdate: i.lastUpdate,
			info: i.info,
			state: i.state,
			isNew: true,
			history: [],
			view: [],
			eval: []
		};

		/*ADD THE RATING OF THE CURRENT USER ONLY*/
		let myeval = _.find(i.eval, function(e) {return e.userId === userId});
		if (myeval)
			rI.eval.push(myeval);

		/*ADD THE LAST VIEW OF THE CURRENT USER ONLY*/
		let lastView = _.find(i.view, function(v) {return v.userId === userId});
		if (lastView)
			rI.view.push(lastView);

		/*ADD THE DECLARER ONLY THE CURRENT USER IS THE DECLARER*/
		if (i.declarer.userId === self.userId)
			rI.declarer = i.declarer;

		/*CHANGE ISNEW TO FALSE IF ALREADY SEEN BY OCCUPANT*/
		if (i.view && i.view.length > 0) {
			let userView = null;
			userView = _.find(i.view, (d) => d.userId === userId);
			rI.isNew = !userView || moment(i.lastUpdate).isAfter(moment(userView.lastView));
		}
		if (rI.isNew == true && rI.declarer &&  rI.declarer.userId == Meteor.userId() && rI.state.status == 0)
			rI.isNew = false;

		/*ADD ELEMENTS OF HISTORY RELATED TO THE CURRENT USER*/
		for (h of i.history) {
			if ((!!h && !!h.share && h.share.all.state) ||
				(residentRole && residentRoleId && !!h && !!h.share && h.share[residentRoleId] && h.share[residentRoleId].state) ||
				(i.declarer.userId === self.userId && !!h && !!h.share && !!h.share.declarer && h.share.declarer.state) ||
				(!!h && !!h.share && !!h.share.custom && _.find(h.share.custom, function(c) {return c.users.indexOf(userId) !== -1;})))
			{
				/*HISTORY OBJECT TO ADD*/
				let rH = {
					date: h.date,
					name: h.name,
					title: h.title,
					isNew: true,
					details: h.details,
					date_interv: h.date_interv,
          undefinedHour: h.undefinedHour,
          intervenantKeeper: h.intervenantKeeper,
          intervenantOther: h.intervenantOther,
          intervenantName: h.intervenantName,
          detailsIsDate: h.detailsIsDate,
					share: {custom: []}
				};
				if (rI.view && rI.view.length > 0) {
					let dViewHistoric = rI.view[0].lastViewHistoric;
					if (dViewHistoric && h.date < dViewHistoric) {
						rH.isNew = false;
					}
				}
				if (rH.isNew == true && rI.declarer && rI.declarer.userId == Meteor.userId() && rI.state.status == 0)
					rH.isNew = false;
				if (!!h && !!h.share && h.share.all.state)
					rH.share.all = h.share.all;
				if ((!!h && !!h.share && rH.share.all) && rI.history.length == 0 && i.declarer.autoDeclare == true) {
					rH.share.all.files = (h.share.declarer.files && h.share.declarer.files.length > 0) ? h.share.declarer.files : h.share.all.files;
				}
				if (residentRoleId && h.share[residentRoleId] && h.share[residentRoleId].state)
					rH.share[residentRoleId] = h.share[residentRoleId];
				if (i.declarer.userId === self.userId && h.share.declarer.state)
					rH.share.declarer = h.share.declarer;
				for (c of h.share.custom) {
					if (c.users.indexOf(self.userId) !== -1 && c.comment !== "")
						rH.share.custom.push(c);
				}
				rI.history.push(rH);
			}
		}
		rI.history.sort(reverseHistoryByDate);

		if (action === "added")
			self.added('residentIncidents', rI._id, rI);
		else if (action === "changed")
			self.changed('residentIncidents', rI._id, rI);
	}

	function reverseHistoryByDate(first, second) {
		if (first.date > second.date)
			return -1;
		if (first.date < second.date)
			return 1;
		return 0;
	}

});

/*PUBLISH ALL INCIDENTS AND FILES RELATED TO CURRENT MANAGER, OF ALL CONDOS HE IS IN CHARGE*/
Meteor.publish('module_incidents_gestionnaire', function () {
	if (!this.userId)
		throw new Meteor.Error(300, "Not Authenticated");
	if (Meteor.call('isGestionnaire', this.userId)) {
		let self = this;
		let user = Meteor.users.findOne(this.userId);
		let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
		let userGestionnaire = _.find(gestionnaire.users, function (u) {return u.userId === self.userId});
		let condosInCharge = _.map(userGestionnaire.condosInCharge, function(condo){return condo.condoId});
		let incidents = Incidents.find({"condoId": {$in: condosInCharge}});
		let incidentsIds = _.map(incidents.fetch(), (i) => {return i._id;});
		let incidentDetails = IncidentDetails.find({$or: [{condoId: {$in: condosInCharge}}, {condoId: "default"}]});
		if (incidentDetails.fetch() == undefined || incidentDetails.fetch()[0] == undefined) {
			incidentDetails = IncidentDetails.find({condoId: "default"});
		}
		let incidentsFiles = IncidentFiles.find({
			$or:
			[
			{ $and:
				[
				{"meta.incidentId": {$exists: true}},
				{"meta.incidentId": {$in: incidentsIds}}
				]
			},
			{ $and:
				[
				{"meta.userId": {$exists: true}},
				{"meta.userId": this.userId},
				{"meta.tmp": {$exists: true}},
				{"meta.tmp" : true}
				]
			}
			]
		}).cursor;
		return [incidents, incidentsFiles, incidentDetails];
	}
});

/*PUBLISH FILES RELATED TO THE INCIDENTID*/
Meteor.publish('incident_files', function (incidentId) {
	if (!this.userId)
		throw new Meteor.Error(300, "Not Authenticated");
	let incident = Incidents.findOne({ '_id': incidentId });
	if (!incident || incident == undefined)
		throw new Meteor.Error(404, "Incident not found");
	return IncidentFiles.find({ $and:
		[
		{"meta.incidentId": {$exists: true}},
		{"meta.incidentId": {$eq: incidentId}}
		]
	}).cursor;
});
