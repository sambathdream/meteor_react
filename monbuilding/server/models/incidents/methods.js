import moment from 'moment';
import {Email} from './../../../common/lang/lang';
import { IncidentResidentIndex, CommonTranslation } from './../../../common/lang/lang';
/*
EACH FUNCTION CALLED BY MANAGER SIDE ADDS A NEW ENTRY IN HISTORY (AN UPDATE VALUES SPECIFIC TO THE INCIDENT)
EACH ENTRY IN HISTORY CONTAINS "share" OBJECT
"share" OBJECT DESCRIBES WHICH RESIDENTS CAN SEE THE ENTRY, AND ALLOWS THE MANAGER TO ADD TEXT / FILES
"share.all" SHARE THE ENTRY WITH ALL RESIDENTS
"share.declarer" SHARE THE ENTRY ONLY WITH THE DECLARER
"share.intern" SHARE THE ENTRY ONLY WITH MANAGERS
"share.custom" IS A TABLE, EACH ELEMENT OF TABLE IS A CUSTOM SHARING (WHO, TEXT, FILES)
*/

/*
AS LONG AS AN INCIDENT IS NOT SOLVED, THE MANAGER RECEIVES AN EMAIL EVERY X HOURS
EVERY NEW ACTION DELAYS THE REMINDER X HOURS LATER
THE REMINDER IS SETUP THANKS TO "reminder" PARAMS
*/

/*
SOME ACTIONS FROM MANAGER (TO VALID, TO SOLVE, TO REFUSE...) TRIGGER THE SENDING OF AN EMAIL TO ALL RESIDENTS OF THE RELATED CONDO
THE CREATION OF A INCIDENT BY A RESIDENT TRIGGER THE SENDING OF AN EMAIL TO MANAGER IN CHARGE OF THE RELATED CONDO
AN EMAIL IS SENT X MINUTES AFTER THE ACTION. IT'S CANCELED IF THE TARGET OF THE EMAIL SEES THE INCIDENT BEFORE THE DEADLINE (LastView UPDATE)
EMAILS MANAGEMENT IS REFERENCED BY "notif" PARAMS
*/

/***********************************************************************************************************************/

/* CHECK PARAMETERS OF INCIDENTS METHODES, THROW ERROR IN CASE OF ERROR
incidentId                      (String) ID OF INCIDENT
data                            (Object) CONTAINS DATA ABOUT INCIDENT
datarequire                     (Boolean) TRUE IF "data" HAS TO BE CHECKED
remind                          (Integer) VALUE FOR THE REMINDER
remindrequire                   (Boolean) TRUE IF "remind" HAS TO BE CHECKED
*/
function checkIncidentAllow(incidentId, data, datarequire, remind, remindrequire) {
	if (!Meteor.userId())
		throw new Meteor.Error(300, "Not Authenticated");
	let incident = Incidents.findOne(incidentId);
	if (!incident)
		throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
	return incident;
}

Meteor.startup(function() {
  function getJoinDate(userId) {
    const resident = Residents.findOne({userId}, { fields: {'condos.condoId': 1, 'condos.joined': 1}});
    return resident.reduce((obj, c) => {
      obj[c.condoId] = c.joined && new Date(c.joined).getTime();
      return obj
    }, {});

  }
	Meteor.methods({
		// FOLLOW / UNFOLLOW INCIDENT NOTIFICATIONS OF SPECIFIC CONDO
		// condoId                           (String) ID OF CONDO
		"module-incident-follow-toggle": function(condoId) {
			if (!Match.test(condoId, String))
				throw new Meteor.Error(401, "Invalid parameters incident module-incident-follow-toggle");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let resident = Residents.findOne({userId: Meteor.userId()});
			if (resident) {
				let condo = _.find(resident.condos, (condo) => {return condo.condoId === condoId});
				if (!condo)
					throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
				Residents.update({userId: Meteor.userId(),
					'condos.condoId': condoId},
					{$set:
						{'condos.$.notifications.incident': !condo.notifications.incident}
					});
			}
			else {
				let gestionnaire = Enterprises.findOne({"users.userId": Meteor.userId()});
				let user = gestionnaire.users.find((u) => {return u.userId == Meteor.userId()});
				let __condo = user.condosInCharge.find((c) => {return c.condoId == condoId});
				if (__condo.notifications.incidents == undefined)
					__condo.notifications.incidents = true;
				else
					__condo.notifications.incidents = !__condo.notifications.incidents;
				_.each(user.condosInCharge, function(elem) {
					if (elem.condoId === condoId) {
						elem = __condo;
					}
				});
				Enterprises.update({
					'_id': gestionnaire._id,
					'users.userId': Meteor.userId()
				},
				{
					$set: {'users.$.condosInCharge': user.condosInCharge},
				});
			}
		},
		/*  UPDATE EVALUATION FROM RESIDENT
		incidentId                  (String) ID OF INCIDENT TO UPDATE
		evalValue                   (Integer) RATING FROM 1 TO 5 (5 BEST)
		comment                     (String) COMMENT TEXT WITH RATING
		*/
		updateEvalIncident: function (incidentId, evalValue, comment) {
			if (!Match.test(incidentId, String) ||
				!Match.test(evalValue, Number) ||
				evalValue <= 0 || evalValue > 5 ||
				!Match.test(comment, String))
				throw new Meteor.Error(401, "Invalid parameters incident updateEvalIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 2)
				throw new Meteor.Error(603, "Invalid state object", "Can't eval unsolved incident");
			let userId = Meteor.userId();
      let userEval = _.find(incident.eval, function (e) { return e.userId === userId });
      let d = new Date();
      if (userEval) {
        if (!userEval.open) { throw new Meteor.Error(603, "Invalid state object", "User already eval Incident"); }
        Incidents.update({ _id: incidentId, "eval.userId": userId },
				{
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"eval.$.value": evalValue,
						"eval.$.comment": comment,
						"eval.$.date": new Date(),
            "eval.$.open": false,
            userViews: [this.userId]
          },
          $addToSet: {
            participants: this.userId
          }
				});
      } else {
        const condosJoinDate = getJoinDate(this.userId)
        const joinDate = condosJoinDate[incident.condoId]
        if (solvedDate && joinDate < incident.solvedDate) {
          throw new Meteor.Error(604, "Id not found in document", "User can't eval Incident");
        }
        Incidents.update({ _id: incidentId, "eval.userId": userId },
				{
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            userViews: [this.userId]
          },
          $push: {
            eval: {
              value: evalValue,
              comment: comment,
              date: new Date(),
              open: false,
            }
          },
          $addToSet: {
            participants: this.userId
          }
				});
      }
		},
		updateEvalIncidentOnlyEvalValue: function (incidentId, evalValue, comment) {
      // TODO: @ropilz This is the same as updateEvalIncident, should check why this was ever created
			if (!Match.test(incidentId, String) ||
				!Match.test(evalValue, Number) ||
				evalValue <= 0 || evalValue > 5 ||
				!Match.test(comment, String))
				throw new Meteor.Error(401, "Invalid parameters incident updateEvalIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 2)
				throw new Meteor.Error(603, "Invalid state object", "Can't eval unsolved incident");
			let userId = Meteor.userId();
      let userEval = _.find(incident.eval, function (e) { return e.userId === userId });
      let d = new Date();
      if (userEval) {
        if (!userEval.open && userEval != '') { throw new Meteor.Error(603, "Invalid state object", "User already eval Incident"); }
        Incidents.update({ _id: incidentId, "eval.userId": userId },
				{
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"eval.$.value": evalValue,
						"eval.$.comment": comment,
						"eval.$.date": new Date(),
            "eval.$.open": false,
            userViews: [this.userId]
          },
          $addToSet: {
            participants: this.userId
          }
				});
      } else {
        const condosJoinDate = getJoinDate(this.userId)
        const joinDate = condosJoinDate[incident.condoId]
        if (solvedDate && joinDate < incident.solvedDate) {
          throw new Meteor.Error(604, "Id not found in document", "User can't eval Incident");
        }
        Incidents.update({ _id: incidentId, "eval.userId": userId },
				{
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            userViews: [this.userId]
          },
          $push: {
            eval: {
              value: evalValue,
              comment: comment,
              date: new Date(),
              open: false,
            }
          },
          $addToSet: {
            participants: this.userId
          }
				});
      }
		},

		/*  UPDATE LAST VIEW ON SPECIFIC INCIDENT, RESIDENT OR MANAGER
		incidentId                  (String) ID OF INCIDENT TO UPDATE
		historic                    (Boolean) TRUE : UPDATE LASTVIEW AND LASTVIEWHISTORIC, FALSE : UPDATE ONLY LASTVIEW
		*/
		updateLastViewIncident: function(incidentId, historic) {
			if (!Match.test(incidentId, String) ||
				!Match.test(historic, Boolean))
				throw new Meteor.Error(401, "Invalid parameters incident updateLastViewIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			let userId = Meteor.userId();
			let d = new Date();
			if (!_.find(incident.view, function(v) {return v.userId === userId})) {
				let view = {
					"lastView": d,
					"userId": userId
				};
				if (historic)
					view.lastViewHistoric = d;
				Incidents.update({_id: incidentId}, {
          $push: {"view": view},
          $addToSet: {userViews: this.userId}
				});
			}
			else {
				if (historic) {
					Incidents.update({_id: incidentId, "view.userId": userId}, {
            $set: {"view.$.lastViewHistoric": d, "view.$.lastView": d},
            $addToSet: {userViews: this.userId}
					});
				}
				else {
					Incidents.update({_id: incidentId, "view.userId": userId}, {
            $set: {"view.$.lastView": d},
            $addToSet: {userViews: this.userId}
					});
				}
			}
		},
		/*  VALIDATE AN INCIDENT FROM MANAGER (STATE GOES FROM 0 TO 1)
		incidentId                  (String) ID OF INCIDENT TO VALIDATE
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		title                     (String) TITLE OF INCIDENT
		priority                  (Integer) 1 IF URGENT, 0 IF NOT URGENT
		type                      (String) TYPE OF INCIDENT
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		validIncident: function(incidentId, data, remind) {
			if (!data || !Match.test(incidentId, String) || !Match.test(remind, Number) || remind < -1 ||
				data.title === "" || data.title.length > 70)
				throw new Meteor.Error(401, "Invalid parameters incident validIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 0)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 1,
					share: data.share
				};
				incident.history.push(newHistory);
        incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
        // visibility
        const newParticipants = []
        let isPublic = false
        if (data.share.all.state) { isPublic = true; }
        if (data.share.declarer.state) { newParticipants.push(this.userId); }
        // if (data.share.intern.state) { }
        if (data.share.custom.state) { newParticipants.push(...data.share.custom[0].users); }


				Incidents.update({_id: incidentId}, {
					$set: {
            public: isPublic,
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"state.status": 1,
						"info.title": data.title,
						"info.type": data.type,
						"info.priority": data.priority,
            "history": incident.history,
            userViews: newParticipants
          },
          $addToSet: { participants: this.userId }
				});
				setIncidentIdOnIncidentFiles(incidentId, data.share);

				// UPDATE THE REMINDER
				let reminderData = {
					emailTemplate: "reminderValid",
					subject: "Rappel - Un incident est en attente"
				};
				updateReminder(reminderData, incident, remind);

				// SEND AN EMAIL TO EVERY RESIDENT OF THE CONDO
				// AND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				incident = Incidents.findOne(incidentId);
				sendNotifToShared(incident);
				let condo = Condos.findOne(incident.condoId);
				let declarer = Meteor.users.findOne({ _id: incident.declarer.userId })
				var lang = declarer.profile.lang || 'fr';
        var translation = new Email(lang);
        let message = Messages.findOne({ 'incidentId': incident._id })
				let mail = {
					templateName: "newIncidentValid-" + lang,
					subject: translation.email["incident_valid"] + condo.getName(),
					data: {
						incident: incident,
						condoId: condo._id,
						notifUrl: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id),
						setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/incident/${incident._id}`, condo._id),
            setMessageLink: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/messenger/manager/${!!message === true ? message._id : ''}`, condo._id),
						share: data.share.declarer.comment
					}
				};

        if (Meteor.userHasRight('actuality', 'see', condo._id, incident.declarer.userId)) {
          sendNotifToDeclarer(incident, mail);
        }
			}
		},
		/*  CANCEL AN INCIDENT FROM MANAGER (STATE GOES FROM 0 TO 3)
		incidentId                  (String) ID OF INCIDENT TO CANCEL
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
			share                      (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		}
		*/
		cancelIncident: function (incidentId, data) {
      console.log('CANCEL!!!', incidentId, data)
			let incident = checkIncidentAllow(incidentId, undefined, false, 0, false);
			if (incident.state.status !== 0)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 9,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"state.status": 3,
						userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'privateIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				// CANCEL THE REMINDER
				if (incident.gestionnaireReminder) {
					Meteor.call("removeEmailReminder", incident.gestionnaireReminder);
					Incidents.update(incidentId, {$set: {gestionnaireReminder: null}});
				}
				incident = Incidents.findOne(incidentId);

				// SEND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				sendNotifToShared(incident, true);
				if (data && data.share && data.share.declarer && data.share.declarer.state !== true) {
					let condo = Condos.findOne(incident.condoId);
					var lang = Meteor.users.findOne({_id: incident.declarer.userId}).profile.lang || 'fr';
					var translation = new Email(lang);
					let mail = {
						templateName: "incidentCanceled-" + lang,
						subject: translation.email["incident_declared_unvalid"] + condo.getName(),
						data: {incident: incident, condo: condo,
							condoId: incident.condoId,
							notifUrl: Meteor.absoluteUrl(`${lang}/resident/${incident.condoId}/profile/notifications`, incident.condoId),
							setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/incident`, incident.condoId),
							setMessageLink: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/messagerie`, incident.condoId),
							share: data.share.declarer.comment
						}
					};
					sendNotifToDeclarer(incident, mail);
				}
			}
		},
		/*  UPDATE AN INCIDENT AS SOLVED FROM MANAGER (STATE GOES FROM 1 TO 2)
		incidentId                  (String) ID OF INCIDENT TO UPDATE
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                    (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		}
		*/
		solvedIncident: function (incidentId, data) {
			let incident = checkIncidentAllow(incidentId, data, true, 0, false);
			if (incident.state.status !== 1)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let userId = Meteor.userId();
			let condo = Condos.findOne(incident.condoId);
			let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
			let syndic = syndicalRole.getResidentsByCondo(incident.condoId);

			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {

				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 8,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );

				// OPEN EVALUATIONS (AVAILABLE ONLY IN STEP 2
				let eval = incident.eval || [];
				for (e of eval) {
					e.open = true;
				}
				// let residents = condo.residents();
				// for (r of residents) {
				// 	if (!_.find(eval, (e)=>{return e.userId === r.userId}))
				// 		eval.push({userId: r.userId, value: 0, comment: "", date: null, open: true});
				// }

				Incidents.update({_id: incidentId}, {
					$set: {
            solvedDate: Date.now(),
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"state.status": 2,
						"eval": eval,
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});
				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'resolvedIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				// REMOVE REMINDER
				if (incident.gestionnaireReminder) {
					Meteor.call("removeEmailReminder", incident.gestionnaireReminder);
					Incidents.update(incidentId, {$set: {gestionnaireReminder: null}});
				}

				// SEND AN EMAIL TO EVERY RESIDENT OF THE CONDO
				// AND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				incident = Incidents.findOne(incidentId);
				sendNotifToShared(incident);
			}
		},
		/*  REOPEN A SOLVED INCIDENT FROM MANAGER (STATE GOES FROM 2 TO 1)
		incidentId                  (String) ID OF INCIDENT TO UPDATE
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		reopenIncident: function(incidentId, data, remind) {
			let incident = checkIncidentAllow(incidentId, data, true, remind, true);
			if (incident.state.status !== 2 && incident.state.status !== 3)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 10,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
						"state.status": 1,
						"eval": [],
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});
				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'reopenedIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				// CREATE AGAIN THE REMINDER
				createReminder(incident, true, remind);

				// SEND AN EMAIL TO EVERY RESIDENT OF THE CONDO
				// AND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				incident = Incidents.findOne(incidentId);
				sendNotifToShared(incident);
			}
		},
		/*  ADD A COMMENT ON AN EXISTING INCIDENT FROM MANAGER, THE COMMENT IS ONLY VISIBLE BY USERS PART OF "share" OBJECT
		incidentId                  (String) ID OF INCIDENT TO UPDATE
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		}
		*/
		commentIncident: function(incidentId, data, remind) {
			let incident = checkIncidentAllow(incidentId, data, true, remind, false);
			if (incident.state.status !== 1)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 4,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'commentIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				incident = Incidents.findOne(incidentId);
				updateReminder({}, incident, remind);
				sendNotifToShared(incident, true);
			}
		},

		/*  ADD A QUOTATION TO AN EXISTING INCIDENT, THE QUOTATION ONLY VISIBLE BY USERS PART OF "share" OBJECT
		incidentId                  (String) ID OF INCIDENT
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		devisIncident: function(incidentId, data, remind) {
			let incident = checkIncidentAllow(incidentId, data, true, remind, true);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 1)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					details: "",
					title: 5,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'quoteIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				incident = Incidents.findOne(incidentId);
				// UPDATE THE REMINDER
				updateReminder({}, incident, remind);
				// SEND AN EMAIL TO EVERY RESIDENT OF THE CONDO
				// AND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				sendNotifToShared(incident, true);
			}
		},
		/*  ADD A CLASS SERVICE TO AN EXISTING INCIDENT
		incidentId                  (String) ID OF INCIDENT
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		keeper_intervention       (Boolean) ?
		extern_intervention       (Boolean) ?
		extern_details            (String) ?
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		osIncident: function(incidentId, data, remind, lang) {
			if (!Match.test(data,
			{
				"share": Object,
				"keeper_intervention": Boolean,
				"extern_intervention": Boolean,
				"extern_details": String
			}) ||
				!Match.test(incidentId, String) ||
				!Match.test(remind, Number) || remind < -1)
				throw new Meteor.Error(401, "Invalid parameters incident osIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 1)
        throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
      let translation = new CommonTranslation(lang);
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
      let detailsData = translation.commonTranslation['technician'] + ' : ';
      let intervenantKeeper = false
      let intervenantOther = false
      let intervenantName = null
			if (data.keeper_intervention) {
        intervenantKeeper = true
        detailsData += translation.commonTranslation['keeper'] + ' ' + (data.extern_intervention ? ", " : "");
      }
			if (data.extern_intervention) {
        intervenantOther = true
        intervenantName = data.extern_details
        detailsData += (data.extern_details !== "" ? "'" + data.extern_details + "'" : translation.commonTranslation['other_technician']);
      }
			let d = new Date();
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					title: 6,
					details: detailsData,
          share: data.share,
          intervenantKeeper,
          intervenantOther,
          intervenantName
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId }
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'osIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				incident = Incidents.findOne(incidentId);
				// UPDATE THE REMINDER
				updateReminder({}, incident, remind);
				// SEND AN EMAIL TO EVERY RESIDENT OF THE CONDO
				// AND A CUSTOM EMAIL TO USERS PART OF 'share' OBJECT
				sendNotifToShared(incident, true);
			}
		},
		/*  ADD AN INTERVENTION DATE TO AN EXISTING INCIDENT
		incidentId                  (String) ID OF INCIDENT
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		date                      (Date) DATE OF INTERVENTION
		undefinedHour             (Boolean) TRUE IS INTERVENTION TIME NOT FIXED
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		intervIncident: function(incidentId, data, remind) {
			let d = new Date();
			if (!Match.test(data,
			{
				"share": Object,
				"date": Date,
				"undefinedHour": Boolean,
				"keeper_intervention": Boolean,
				"extern_intervention": Boolean,
				"extern_details": String
			}) ||
				!Match.test(incidentId, String) ||
				!Match.test(remind, Number) || remind < -1)
				throw new Meteor.Error(401, "Invalid parameters incident intervIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 1)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
			let user = Meteor.user();
			let name = user.profile.firstname + " " + user.profile.lastname;
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					title: 7,
					date_interv: data.date,
					undefinedHour: data.undefinedHour,
					// details: "Programmée le " + (data.date.getDate() < 10 ? '0' + data.date.getDate() : data.date.getDate()) + '/' + (data.date.getMonth() < 10 ? '0' + data.date.getMonth() : data.date.getMonth()) + '/' + data.date.getFullYear() + (data.undefinedHour ? "" : " à " + (data.date.getHours() < 10 ? '0' + data.date.getHours() : data.date.getHours()) + ":" + (data.date.getMinutes() < 10 ? '0' + data.date.getMinutes() : data.date.getMinutes())),
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({_id: incidentId}, {
					$set: {
            "lastUpdate": d,
            updatedAt: d.getTime(),
            "history": incident.history,
            userViews: [this.userId]
          },
          $addToSet: { participants: this.userId },
					$push: {
						"state.intervention": {
							"date": data.date,
							"active": true,
							"undefinedHour": data.undefinedHour,
							"keeper_intervention": data.keeper_intervention,
							"extern_intervention": data.extern_intervention,
							"extern_details": data.extern_details,
						}
					}
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'programmInterventionIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				incident.state.intervention.push({date:data.date, active:true});
				incident = Incidents.findOne(incidentId);
				// UPDATE THE REMINDER X HOURS AFTER INTERVENTION DATE
				setupReminderToLastInterv(incident, remind);
				sendNotifToShared(incident, true);
			}
		},
		/*  CANCEL AN EXISTING INTERVENTION OF AN EXISTING INCIDENT
		incidentId                  (String) ID OF INCIDENT
		data                        (Object) CONTAINS DATA TO UPDATE INCIDENT IF NEEDED
		{
		share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
		date                      (Date) DATE OF INTERVENTION TO CANCEL
		undefinedHour             (Bool) is undefined hour
		}
		remind                      (Integer) INDICATES IF MANAGER WANTS TO RECEIVE AUTOMATIC REMINDER AND THE FREQUENCY
		*/
		cancelIntervIncident: function(incidentId, data, remind, lang) {
			if (!Match.test(data,
			{
				"share": Object,
				"date": Date,
				"undefinedHour": Boolean,
			}) || !Match.test(incidentId, String) || !Match.test(remind, Number) || remind < -1)
				throw new Meteor.Error(401, "Invalid parameters incident cancelIntervIncident");
			if (!Meteor.userId())
				throw new Meteor.Error(300, "Not Authenticated");
			let incident = Incidents.findOne(incidentId);
			if (!incident)
				throw new Meteor.Error(601, "Invalid id", "Cannot find incident");
			if (incident.state.status !== 1)
				throw new Meteor.Error(603, "Invalid state object", "Incident isn't in correct state");
      let user = Meteor.user();
      let translation = new CommonTranslation(lang);
			let name = user.profile.firstname + " " + user.profile.lastname;
      let d = new Date();
      console.log('data.date', data.date)
			let dateDetail = (data.undefinedHour == true ? moment(data.date).format("DD/MM/YYYY") : moment(data.date).format("DD/MM/YYYY") + ' ' + translation.commonTranslation['at'] + ' ' + moment(data.date).format('HH:mm'));
			if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId)) {
				let newHistory = {
					date: d,
					userId: Meteor.userId(),
					name: name,
					title: 11,
					// this data is generating issue because dateDetail is using server timezone when parsed instead of client timezone
          details: translation.commonTranslation['intervention_scheduled'] + dateDetail + translation.commonTranslation['was_canceled'],
          detailsDate: data.date,
          undefinedHour: data.undefinedHour,
          detailsIsDate: true,
					share: data.share
				};
				incident.history.push(newHistory);
				incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
				Incidents.update({
					_id: incidentId,
					"state.intervention" : {$elemMatch: {
						date: data.date,
						active: true,
					}}
				}, {
					$set: {
						"state.intervention.$.active": false,
            "lastUpdate": d,
            updatedAt: d.getTime(),
            "history": incident.history,
            userViews: [this.userId]
					}
				});

				setIncidentIdOnIncidentFiles(incidentId, data.share);
				Meteor.call('addToGestionnaireHistory',
					'incident',
					incidentId,
					'canceledInterventionIncident',
					incident.condoId,
					incident.info.title,
					Meteor.userId()
				);

				let interv = _.find(incident.state.intervention, (e)=>{return e.date === data.date;});
				if (interv)
					interv.active = false;
				incident = Incidents.findOne(incidentId);
				// UPDATE THE REMINDER X HOURS AFTER THE LAST INTERVENTION DATE
				setupReminderToLastInterv(incident, remind);
				sendNotifToShared(incident, true);
			}
		},
		addIncidentToMessageList(data, isMessenger = false) {
      let dateFeed = Date.now()
			let msgId = Messages.insert({
        // NEW MESSAGE DATA
        createdBy: this.userId,
        participants: [this.userId],
        views: [this.userId],
        destination: 'incident',
        createdAt: dateFeed,
        updatedAt: dateFeed,
        // ////////////////
        date: dateFeed,
				condoId: data.condoId,
				type: data.type,
				details: data.details,
				users: [{userId: this.userId, lastVisit: dateFeed + 10, createdBy: true, incidentMessage: true}],
				target: data.target,
				origin: this.userId,
				sujet: data.sujet,
        lastMsg: dateFeed,
        views: [this.userId],
				lastMsgString: data.message,
				lastMsgUserId: this.userId,
				incidentId: data.incidentId
			});
			if (!data.files) {
				data.files = []
			}
			let msgFeedId = null;
			if (isMessenger) {
				msgFeedId = MessagesFeed.insert({messageId: msgId, date: dateFeed, userId: this.userId, text: data.message, files: data.files});
			} else {
				msgFeedId = MessagesFeed.insert({messageId: msgId, date: dateFeed, userId: this.userId, text: data.message, incidentFiles: data.files});
      }
			const condoContact = CondoContact.findOne({ condoId: data.condoId });
			let emailList = getEmailList (condoContact, data.type, true)

      OneSignal.MbNotification.sendToUserIds(emailList, {
        type: 'messenger',
        key: 'new_message_incident',
        dataId: msgFeedId,
        condoId: data.condoId,
        data: {
          target: data.target
        }
      })
			return msgId;
		},
		/*  ADD A NEW INCIDENT (FROM RESIDENT OR MANAGER)
		condoId                     (String) ID OF CONDO RELATED TO THE INCIDENT
		incident                    (Object) DATA FOR THE CREATION
		{
		declarer                    (Object) DATA ABOUT THE DECLARER
		{
		phone                       (String) PHONE NUMBER OF DECLARER
		}
		info                        (Object) DATA ABOUT THE INCIDENT
		{
		priority
		type                        (String) TYPE OF INCIDENT
		zone
		title                       (String) TITLE OF INCIDENT
		comment                     (String) COMMENT OF INCIDENT
		}
		rappel                      (Boolean) INDICATES IF A REMINDER HAS TO BE CREATED
		}
		from                        (String) "gestionnaire" OR "resident"
		files                       (Table of String) IDs OF INCIDENTFILES
		*/
		publishIncident: function(condoId, incident, from, files, remindManager) {
			if (!Match.test(condoId, String) ||
				!Match.test(incident,
				{
					"declarer": {
						"phone": Match.OneOf(null, String)
					},
					"info": {
						"priority": String,
						"type": String,
						"zone": String,
						"title": String,
						"incidentTypeId": String,
						"comment": String
					},
					"rappel": Match.Maybe(Boolean)
				}))
				throw new Meteor.Error(401, "Invalid parameters incident publishIncident 1");
			if (from !== "resident" && from !== "gestionnaire")
				throw new Meteor.Error(401, "Invalid parameters incident publishIncident 2");
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			let user = Meteor.users.findOne(this.userId);
			let condo = Condos.findOne(condoId);
			if (!condo)
				throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
			if (incident.info.title === "" || incident.info.type === "" ||
				incident.info.title.length > 70)
				throw new Meteor.Error(401, "Invalid parameters incident publishIncident 3");
			if (from === "resident") {
				Meteor.call('isResidentOfCondo', this.userId, condoId);
				incident.declarer.autoDeclare = false;
			}
			else {
				Meteor.call('isGestionnaireOfCondo', this.userId, condoId);
				incident.declarer.autoDeclare = true;
			}
			incident.declarer.userId = this.userId;

			incident.state = {
				"status": (from === "resident" ? 0 : 1),
				"keeper_intervention": false,
				"extern_intervention": false,
				"extern_details": "",
				"intervention": []
			};
      const date = new Date();
			incident.condoId = condoId;
			incident.createdAt = date.getTime();
      incident.lastUpdate = date;
      incident.updatedAt = date.getTime();
      incident.eval = [];
      incident.createdBy = this.userId;
      incident.participants = [this.userId];

      incident.view = [];
      incident.userViews = [];
			if (incident.declarer.autoDeclare) {
        incident.view.push({lastView: new Date(), userId: this.userId, lastViewHistoric: new Date()});
        incident.userViews = [this.userId];
			}

			let declarerShare = !incident.declarer.autoDeclare;
			incident.history = [];
			const title = (from == "resident" ? 2 : 3); // refer to index in IncidentTitle colection
			incident.history.push({
				"date": incident.createdAt,
				"userId": this.userId,
				"title": title,
				"details": incident.info.title,
				"share": {
					"all": {"state": incident.declarer.autoDeclare, "comment": "", "files": []},
					"declarer": {"state": declarerShare, "comment": incident.info.comment, "files": files},
					"intern": {"state": false, "comment": "", "files": []},
					"custom": []
				}
			});

			let incidentId = Incidents.insert(incident);
			incident = Incidents.findOne(incidentId);
			incident.declarer.firstname = user.profile.firstname;
			incident.declarer.lastname = user.profile.lastname;
			incident.declarer.email = user.emails[0].address;

			_.each(files, (f) => {
				IncidentFiles.update({_id: f},
					{$set:
						{
							'meta.incidentId': incidentId
						}
					});
			});

			let d = moment().toDate();
			Meteor.call('addToGestionnaireHistory',
				'incident',
				incidentId,
				'declaredIncident',
				incident.condoId,
				incident.info.title,
				Meteor.userId()
			);

			// CREATE A REMINDER FOR MANAGER
			if (remindManager == true)
				createReminder(incident, incident.declarer.autoDeclare, -1);
			let priority = IncidentPriority.findOne(incident.info.priority);

			if (priority && priority != undefined) {
				priority = priority["value-fr"] + " - ";
			}
			else {
				priority = "";
			}
			if (incident.declarer.autoDeclare === false) {
				var lang = user.profile.lang || 'fr';
				let mail = {
					templateName: "newIncidentDeclared-fr",
					subject: priority + "INCIDENT - "+ condo.getName(),
					data: {showComment: true, incident: incident, condo: condo, setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/incidents/${incident._id}`, condo._id)},
				};
				// SEND AN EMAIL TO MANAGER IF DECLARED BY A RESIDENT
        let pushNotificationUserIds = sendNotifToGestionnaires(incident, mail);
				OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
					type: 'incident',
					key: 'incident_' + title,
					dataId: incident._id,
					condoId: incident.condoId,
					forManager: true,
					data: {
						incident
					}
				})
			}
			else {
				let pushNotificationUserIds = [];
        let residents = _.map(Residents.find({"condos.condoId": incident.condoId}, {fields: {userId: 1}}).fetch(), function (r) {return r.userId});
        let managers = ((Enterprises.findOne({condos: incident.condoId}, {"users.userId": 1}) || {}).users || []).map(u => u.userId);

				let users = Meteor.users.find({"_id": {$in: [...residents, ...managers]}}).fetch();
				for (user of users) {
					pushNotificationUserIds.push(user._id)
					Meteor.call('checkNotifResident', user._id, incident.condoId, 'incident', function(error, result) {
						if (result) {
							var lang = user.profile.lang || 'fr';
							var translation = new Email(lang);
							let mail = {
								templateName: "newIncidentDeclaredByBM-" + lang,
								subject: translation.email['incident_declared'] + condo.getName(),
                data: { showComment: false, incident: incident, condo: condo, setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condo._id}/incident`, condo._id)}
							};
							// SEND AN EMAIL TO ALL RESIDENTS IF DECLARED BY A MANAGER
							sendNotifToResidents(incident, mail, user);
						}
					});
        }
				OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
					type: 'incident',
					key: 'incident_' + title,
					dataId: incident._id,
					condoId: incident.condoId,
					forManager: false,
					data: {
						incident
					}
				})
				let mail = {
					templateName: 'newIncidentDeclaredByBM-fr',
					subject: priority + "INCIDENT - "+ condo.getName(),
					data: {
						showComment: true,
						incident: incident,
						condo: condo,
            setReplyLink: Meteor.absoluteUrl(`fr/gestionnaire/incidents/${incident._id}`, condo._id)},
				}
				sendNotifToGestionnaires(incident, mail)
			}
			return incidentId;
		},
		replyAtIncidentMessage: function(id) {
			let history = Incidents.findOne(id).history
			let d = new Date();
			_.each(history, function(h, index) {
				if (history[index].title == 12 && !history[index].isReply)
					history[index].isReply = Date.now();
			})
			Incidents.update({_id: id},
			{
				$set : {
          "lastUpdate": d,
          updatedAt: d.getTime(),
          history: history,
          userViews: [this.userId]
        },
        $addToSet: { participants: this.userId }
			})
		},
	});
});

/*  UPDATE ALL IN "share" OBJECT FILES TO SET INCIDENT ID
incidentId                (String) ID OF INCIDENT RELATED TO FILES
share                     (Object) CONTAINS MESSAGES AND FILES TO SEND TO ALL / COUNCIL / DECLARER / INTERN...
*/
function setIncidentIdOnIncidentFiles(incidentId, share) {
	_.each(share, (thisShare, name) => {
		if (name === 'custom') {
			_.each(thisShare, (customShare) => {
				_.each(customShare.files, (f) => {
					IncidentFiles.update({ _id: f },
						{
							$set:
								{
									'meta.incidentId': incidentId
								}
						});
				});
			})
		} else {
			_.each(thisShare.files, (f) => {
				IncidentFiles.update({_id: f},
					{$set:
						{
							'meta.incidentId': incidentId
						}
					});
			});
		}
	})
}

/*  CREATE A NEW REMINDER FOR MANAGER
incident                (Object) INCIDENT RELATED TO THE REMINDER TO CREATE
valid                   (Boolean) TRUE IF INCIDENT IS STATE 1 (IN PROGRESS), FALSE IF STATE 0 (DECLARED)
hours                   (Integer) NUMBER OF HOURS BEFORE STARTING REMINDER
d                       (Date) START DATE OF REMINDER
*/
function createReminder(incident, valid, hours, d) {
	let condo = Condos.findOne(incident.condoId);
	const condoContact = CondoContact.findOne({condoId: condo._id});
	let emailList = getEmailList(condoContact, incident.info.incidentTypeId)

	if (Meteor.call('isGestionnaireOfCondo', Meteor.userId(), incident.condoId) && (!emailList.includes(Meteor.userId()))) {
    emailList.push(Meteor.userId());
    console.log('emailList3', emailList)
  }
	if (!d) {
    d = new Date();
  }
	let priority = IncidentPriority.findOne(incident.info.priority)
	if (hours === -1) {
		hours = getReminderValue(incident.condoId, incident.info.priority);
	}

	let validationDate = _.find(incident.history, function(elem) {
		return (elem.title == 1 || elem.title == 3); /* 1 => incident validé ; 3 => incident creer par gestionnaire donc validé */
	})
	if (validationDate && validationDate != undefined && validationDate.date)
		validationDate = validationDate.date;
	else
		validationDate = null;
	emailList = _.without(emailList, undefined, null)
	if (emailList.length > 0) {
		let reminderData = {
			to: emailList,
			condoId: incident.condoId,
			emailTemplate: (valid) ? "reminderValid" : "reminderDeclare",
			emailData: {
				incidentId: incident._id,
				title: incident.info.title,
				date: (validationDate ? moment(new Date(validationDate)).format("DD/MM/YYYY [à] HH:mm") : "Non validé"),
				comment: incident.info.comment,
				condoName: condo.getName(),
				zone: incident.info.zone,
				priority: incident.info.priority,
        setReplyLink: Meteor.absoluteUrl(`fr/gestionnaire/incidents/${incident._id}`, condo._id)
			},
			remindHours: hours,
			subject: (priority ? priority["value-fr"] : 'URGENT') + " - INCIDENT RAPPEL - " + condo.getName()
		};
		let reminderId = Meteor.call("programEmailReminder", reminderData, d);
		Incidents.update(incident._id, {$set: {gestionnaireReminder: reminderId}});
	}
}

// UPDATE AN EXISTING REMINDER
function updateReminder(data, incident, hours) {
	if (incident.gestionnaireReminder) {
		Meteor.call('removeEmailReminder', incident.gestionnaireReminder);
	}
	if (hours !== -1) {
		createReminder(incident, true, hours);
	}
}

function setupReminderToLastInterv(incident, hours) {
	incident.state.intervention.reverse();
	let d = _.find(incident.state.intervention, (e)=>{return e.active});
	if (d)
		d = d.date;
	else
		d = new Date();
	if (incident.gestionnaireReminder) {
		if (hours === -1) {
			Meteor.call('removeEmailReminder', incident.gestionnaireReminder);
			Incidents.update(incident._id, {$set: {gestionnaireReminder: null}});
		}
		else {
			Meteor.call("updateEmailReminder", incident.gestionnaireReminder, {remindHours: hours}, d);
		}
	}
	else if (hours !== -1)
		createReminder(incident, true, hours, d);
}

function sendNotifToGestionnaires(incident, mail) {
	let d = new Date();
	mail.dueTime = new Date(d.getTime() + Meteor.global.email.notification_delay_minutes * 60 * 1000);
	mail.condition = {
		name: "checkNotifEmailIncident",
		data: {
			incidentId: incident._id,
			date: d
		}
	};
	mail.condoId = incident.condoId
	const condoContact = CondoContact.findOne({condoId: incident.condoId});
	let emailList = getEmailList(condoContact, incident.info.incidentTypeId)
	pushNotificationUserIds = []
	for (u of emailList) {
		let user = Meteor.users.findOne(u);
		if (!!user) {
			mail.to = user.emails[0].address;
			mail.condition.data.userId = user._id;
			pushNotificationUserIds.push(user._id);
			Meteor.call('checkNotifGestionnaire', user._id, incident.condoId, 'incidents', function(error, result) {
				if (result) {
					Meteor.call('scheduleAnEmail', mail);
				}
			})
		}
	}
	return pushNotificationUserIds
}

function sendNotifToDeclarer(incident, mail) {
	if (incident.declarer.autoDeclare)
		return ;
	let d = new Date();
	let declarer = Meteor.users.findOne(incident.declarer.userId);
	mail.dueTime = new Date(d.getTime() + Meteor.global.email.notification_delay_minutes * 60 * 1000);
	mail.condition = {
		name: "checkNotifEmailIncident",
		data: {incidentId: incident._id, date: d}
	};
	mail.condoId = incident.condoId
	mail.to = declarer.emails[0].address;
	mail.condition.data.userId = declarer._id;
	if (Meteor.call('checkNotifResident', declarer._id, incident.condoId, 'incident')) {
		Meteor.call('scheduleAnEmail', mail);
	}
}

function sendNotifToResidents(incident, mail, user) {
	let d = new Date();
	mail.dueTime = new Date(d.getTime() + Meteor.global.email.notification_delay_minutes * 60 * 1000);
	mail.condition = {
		name: "checkNotifEmailIncident",
		data: {incidentId: incident._id, date: d}
	};
	mail.condoId = incident.condoId
	mail.to = user.emails[0].address;
	mail.condition.data.userId = user._id;
	let pushNotificationUserIds = [];
	Meteor.call('checkNotifResident', user._id, incident.condoId, 'incident', function(error, result) {
		if (result) {
			Meteor.call('scheduleAnEmail', mail);
		}
	});
}

function sendNotifToShared(incident, allowAll) {
	let d = new Date();
  let lastHistory = incident.history[0];
	var condo = Condos.findOne(incident.condoId);
  let declarerId = incident.declarer.userId

  let user = Meteor.users.findOne(declarerId);
  incident.declarer.firstname = user.profile.firstname;
  incident.declarer.lastname = user.profile.lastname;
  incident.declarer.email = user.emails[0].address;

	let mail = {
		templateName: "",
		subject: "",
		data: {incident: incident, condo: condo, title: lastHistory.title, details: lastHistory.details, date: lastHistory.date,
			setReplyLink: ""
		},
		dueTime: new Date(d.getTime() + Meteor.global.email.notification_delay_minutes * 60 * 1000),
		condition: {
			name: "checkNotifEmailIncident",
			data: {incidentId: incident._id, date: d, historic: true}
		}
	};
	let pushNotificationUserIds = [];
	let pushNotificationInternIds = []
	mail.condoId = incident.condoId
	_.each(lastHistory.share, function(shared, index) {
    console.log('index', index)
		if (index == "custom") {
			_.each(shared, function(c) {
				mail.data.comment = c.comment || "---";
				mail.data.nbfiles = c.files.length;
				for (u of c.users) {
					let user = Meteor.users.findOne(u);
					pushNotificationUserIds.push(user._id);
					let lang_other = user.profile.lang || 'fr'
					var translation = new Email(lang_other);
					mail = selectTemplateAndSubject(lastHistory.title, mail, translation, lang_other, condo);
          mail.data.setReplyLink = Meteor.absoluteUrl(`${lang_other}/resident/${condo._id}/incident`, incident.condoId);
					mail.to = user.emails[0].address;
					mail.condition.data.userId = user._id;
					Meteor.call('scheduleAnEmail', mail);
				}
			});
		}
		else if (shared.state && shared.state == true) {
			if (index == "all") {
				mail.data.comment = shared.comment || "---";
				mail.data.nbfiles = shared.files.length;
				let residents = _.map(Residents.find({"condos.condoId": incident.condoId}).fetch(),	function (r) {return r.userId});
        let users = Meteor.users.find({"_id": {$in: residents}}).fetch();
        // console.log('users', users)
				for (user of users) {
					pushNotificationUserIds.push(user._id);
					if (lastHistory.title === 1 && user._id !== declarerId && Meteor.call('checkNotifResident', user._id, incident.condoId, 'incident') === true) {
						let lang_all = user.profile.lang || 'fr';
						var translation = new Email(lang_all);
						mail = selectTemplateAndSubject(lastHistory.title, mail, translation, lang_all, condo);
						mail.data.setReplyLink = Meteor.absoluteUrl(`${lang_all}/resident/${condo._id}/incident`, condo._id);
						mail.to = user.emails[0].address;
						mail.condition.data.userId = user._id;
						let emailId = Meteor.call('scheduleAnEmail', mail);
					}
				}
			}
			else if (index == "declarer" && incident.declarer.state) {
				let user = Meteor.users.findOne(incident.declarer.userId);
				mail.data.comment = shared.comment || "---";
				mail.data.nbfiles = shared.length;
				pushNotificationUserIds.push(user._id);
				if (Meteor.call('checkNotifResident', user._id, incident.condoId, 'incident')) {
					let lang_declarer = user.profile.lang || 'fr';
					var translation = new Email(lang_declarer);
					mail = selectTemplateAndSubject(lastHistory.title, mail, translation, lang_declarer, condo);
          mail.data.setReplyLink = Meteor.absoluteUrl(`${lang_declarer}/resident/${condo._id}/incident`, condo._id);
					mail.to = user.emails[0].address;
					mail.condition.data.userId = user._id;
					Meteor.call('scheduleAnEmail', mail);
				}
			}
			else if (index == "intern") {
				const condoContact = CondoContact.findOne({ condoId: incident.condoId });
				let emailList = getEmailList(condoContact, incident.info.incidentTypeId)
				pushNotificationInternIds = [
					...emailList
				];
				mail.data.comment = shared.comment || "---";
				mail.data.nbfiles = shared.length;
				pushNotificationUserIds.push(user._id);
				for (const userId of emailList) {
					let user = Meteor.users.findOne(userId);
					if (user._id !== Meteor.userId() && Meteor.call('checkNotifGestionnaire', user._id, incident.condoId, 'incidents')) {
						let lang_intern = user.profile.lang || 'fr';
						var translation = new Email(lang_intern);
						mail = selectTemplateAndSubject(lastHistory.title, mail, translation, lang_intern, condo, true);
						mail.data.setReplyLink = Meteor.absoluteUrl(`${lang_intern}/gestionnaire/incidents/${incident._id}`, condo._id);
						mail.to = user.emails[0].address;
						mail.condition.data.userId = user._id;
						Meteor.call('scheduleAnEmail', mail);
					}
				}
			}
			else {
				let destsByRole = condo.getResidentWithRoleId(index);
				mail.data.comment = shared.comment || "---";
				mail.data.nbfiles = shared.length;
				for (d of destsByRole) {
					let user = Meteor.users.findOne(d.userId);
					pushNotificationUserIds.push(user._id);
					if (Meteor.call('checkNotifResident', user._id, incident.condoId, 'incident')) {
						let lang_customRole = user.profile.lang || 'fr';
						var translation = new Email(lang_customRole);
						mail = selectTemplateAndSubject(lastHistory.title, mail, translation, lang_customRole, condo);
            mail.data.setReplyLink = Meteor.absoluteUrl(`${lang_customRole}/resident/${condo._id}/incident`, condo._id);
						mail.to = user.emails[0].address;
						mail.condition.data.userId = user._id;
						Meteor.call('scheduleAnEmail', mail);
					}
				}
			}
		}
	})
  pushNotificationUserIds = _.without(pushNotificationUserIds, declarerId)
	if (pushNotificationUserIds && pushNotificationUserIds.length > 0) {

		OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
			type: 'incident',
			key: 'incident_' + (lastHistory.title != 1 ? lastHistory.title : '2'),
			dataId: incident._id,
			condoId: incident.condoId,
			forManager: false,
			data: {
				incident
			}
		})
	}
	if (declarerId) {
		OneSignal.MbNotification.sendToOneUser(declarerId, {
			type: 'incident',
			key: 'incident_' + lastHistory.title,
			dataId: incident._id,
			condoId: incident.condoId,
			forManager: false,
			data: {
				incident
			}
		})
  }
	if (pushNotificationInternIds && pushNotificationInternIds.length > 0) {
		OneSignal.MbNotification.sendToUserIds(pushNotificationInternIds, {
			type: 'incident',
			key: 'incident_' + lastHistory.title,
			dataId: incident._id,
			condoId: incident.condoId,
			forManager: true,
			data: {
				incident
			}
		})
	}
}

function getEmailList (condoContact, incidentTypeId, sw) {
	const contactSet = condoContact ? condoContact.contactSet : []
	let incidentCondoContact
	let incidentDetailId = DeclarationDetails.findOne({ key: "incident" })

	if (incidentTypeId) {
		incidentCondoContact = _.find(contactSet, function (contact) {
			return contact.declarationDetailId == incidentDetailId._id && contact.messageTypeId == incidentTypeId
		})
	} else if(!sw) {
		incidentCondoContact = _.find(contactSet, function (contact) {
			return contact.declarationDetailId == incidentDetailId._id
		})
	}

	let emailList = condoContact ? [condoContact.defaultContact.userId] : []

  if (incidentCondoContact && incidentCondoContact.userIds && incidentCondoContact.userIds.length > 0) {
		emailList = _.without(incidentCondoContact.userIds, null, undefined)
	}

	return emailList
}

Meteor.startup(function() {
	Meteor.methods({
		pingpong: (str) => {
			return ("PONG : " + str);
		},
		checkNotifEmailIncident: (data) => {
			let i = Incidents.findOne(data.incidentId);
			if (!i)
				return false;
			let v = _.find(i.view, (e)=>{return e.userId === data.userId});
			if (!v)
				return true;
			if (data.historic)
				return (v.lastViewHistoric < data.date);
			return (v.lastView < data.date);
		}
	});
});

function selectTemplateAndSubject(title, mail, translation, lang, condo, isManager) {
	let translationTitle = new IncidentResidentIndex(lang);
	switch (title) {
		case 1: // === title 2 (it's normal!)
			mail.templateName = "newIncidentDeclared-" + lang;
			title = translationTitle.incidentResidentIndex["title2"];
			mail.subject = translation.email["incident_declared"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 2:
			mail.templateName = "newIncidentDeclared-" + lang;
			title = translationTitle.incidentResidentIndex["title2"];
			mail.subject  = translation.email["incident_declared"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 3:
			mail.templateName = "newIncidentDeclared-" + lang;
			title = translationTitle.incidentResidentIndex["title3"];
			mail.subject  = translation.email["incident_declared"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 4:
			mail.templateName = "incidentMessage-" + lang;
			title = translationTitle.incidentResidentIndex["title4"];
			mail.subject  = translation.email["not_read_incident"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 5:
			mail.templateName = "incidentMessage-" + lang;
			title = translationTitle.incidentResidentIndex["title5"];
			mail.subject  = translation.email["not_read_incident"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 6:
			mail.templateName = "incidentMessage-" + lang;
			title = translationTitle.incidentResidentIndex["title6"];
			mail.subject  = translation.email["not_read_incident"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 7:
			mail.templateName = "incidentMessage-" + lang;
			title = translationTitle.incidentResidentIndex["title7"];
			mail.subject  = translation.email["not_read_incident"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 8:
			mail.templateName = "incidentSolved-" + lang;
			title = translationTitle.incidentResidentIndex["title8"];
			mail.subject  = "Incident " + translation.email['resolved'] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 9:
			mail.templateName = "incidentCanceled-" + lang;
			title = translationTitle.incidentResidentIndex["title9"];
			mail.subject  = translation.email["incident_declared_unvalid"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 10:
			mail.templateName = "incidentReopen-" + lang;
			title = translationTitle.incidentResidentIndex["title10"];
			mail.subject = "Incident " + translation.email['re-open'] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
		case 11:
			mail.templateName = "incidentMessage-" + lang;
			title = translationTitle.incidentResidentIndex["title11"];
			mail.subject  = translation.email["not_read_incident"] + condo.getName();
			mail.data.title = title;
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
			break;
		default:
			mail.templateName = "incidentMessage-" + lang;
			mail.subject = translation.email["not_read_incident"] + condo.getName();
			mail.data.condoId = condo._id;
			mail.data.notifUrl = isManager ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, condo._id) : Meteor.absoluteUrl(`${lang}/resident/${condo._id}/profile/notifications`, condo._id);
		break ;
	}
	return mail;
}
