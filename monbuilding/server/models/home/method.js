Meteor.startup(function() {
	Meteor.methods({
		'signinHasGestionnaire': function(condoId) {
			if (condoId) {
				let gestionnaire = Enterprises.find({"condos": condoId}).fetch();
				if (gestionnaire.length > 0)
					return true;
			}
			return false;
		},
		'signinHasGestionnaireToValid': function(condoId, condoType) {
			if (condoId) {
				let gestionnaire = Enterprises.findOne({"condos": condoId});
				if (gestionnaire && gestionnaire.settings && gestionnaire.settings.validUsers) {
					let gestionnaireSettings = gestionnaire.settings.validUsers;
					if (condoType == "copro")
						return gestionnaireSettings.copro;
					if (condoType == "mono")
						return gestionnaireSettings.mono;
					if (condoType == "etudiante")
						return gestionnaireSettings.etudiante;
					if (condoType == "office")
						return gestionnaireSettings.office;
				}
				return false;
			}
			return false;
		},
		'pushJustifDom': function(File) {
			UsersJustifFiles.insert({file: File});
		},
		'getUserIdWithEmail': function(email) {
			let user = Meteor.users.findOne({$and: [{"emails.0.address": email}, {"services.password": {$exists: false}}]});
			if (user)
				return {userId: user._id};
			else
				return {userId: undefined};
		},
		'sendEmailContactHomePage': function(email, text) {
			Meteor.call('sendEmailSync',
				'bonjour@monbuilding.com',
				null,
				'HomePage - formulaire de contact',
				'contact-from-home',
				{
					email: email,
					text: text
				}
			);
		}
	});
});