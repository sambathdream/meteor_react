Meteor.publish("register_home_page", function() {
	let signinCondos = _.map(Condos.find().fetch(), function(elem) {
    let condoModule = CondosModulesOptions.findOne({ condoId: elem._id })
    let needCompany = false
    if (condoModule && condoModule.profile && condoModule.profile.company === true) {
      needCompany = true
    }
		return {name: elem.name, _id: elem._id, address: elem.info.address, code: elem.info.code, city: elem.info.city, type: elem.settings.condoType, needCompany};
	});
	let signinBuildings = _.map(Buildings.find().fetch(), function(elem) {
		return {name: elem.name, _id: elem._id, condoId: elem.condoId, address: elem.info.address, code: elem.info.code, city: elem.info.city};
	});
	let self = this;
	for (elem of signinCondos) {
		self.added('SigninCondos', elem._id, elem);
	}
	for (elem of signinBuildings) {
		self.added('SigninBuildings', elem._id, elem);
	}
	this.ready();
  return [BuildingStatus.find(), CompanyName.find(), UsersJustifFiles.find().cursor];
});
