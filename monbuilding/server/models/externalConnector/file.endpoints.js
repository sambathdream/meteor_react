import { Meteor } from 'meteor/meteor'
import { validateFileRequest } from './middlewares/validate-request'
import { jsonResponse, prefixEndpoint } from './helpers/endpoint-utils.helper'

if (Meteor.isServer) {
  const filesEndpoint = `${prefixEndpoint}/files`
  const bodyParser = require('body-parser')
  Picker.middleware(bodyParser.json())
  Picker.middleware(validateFileRequest)

  Picker.route('/custom-endpoint/upload', function (params, req, res, next) {
    res.end(`File information: ${req.file ? req.file.originalname : 'nothing'}`)
  })

  Picker
    .route(filesEndpoint, function (params, req, res, next) {
      jsonResponse({
        res,
        status: 200,
        body: { valid: true }
      })
    })

  Picker
    .route(`${filesEndpoint}/videos`, function (params, req, res, next) {
      const { fileName, fileUrl } = req.body
      const fileId = MbFiles.insert({ fileName, fileUrl })

      jsonResponse({
        res,
        status: 200,
        body: { fileId }
      })
    })
}
