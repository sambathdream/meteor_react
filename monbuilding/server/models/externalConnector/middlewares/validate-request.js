import { validateUserSession } from '../helpers/validate-user-session.helper'

const filePathRegex = /^\/api\/v1\/files/

const validateFileRequest = (req, res, next) => {
  if (filePathRegex.test(req.url)) {
    const { authorization, userid } = req.headers
    let validUserSession = false
    if (authorization && userid) {
      const token = authorization.replace('Bearer ', '')
      validUserSession = validateUserSession(userid, token)
    }

    if (!validUserSession) {
      res.writeHead(401, {
        'Content-Type': 'application/json'
      })

      res.end(JSON.stringify({
        valid: false
      }))
    }
  }
  next()
}

export {
  validateFileRequest
}
