import { Meteor } from 'meteor/meteor'
const jwt = require('jsonwebtoken')

const pathRegex = /^\/api\/v1\/(ad-users)/

const validateRequestJWT = (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Credentials', 'true')
  res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,DELETE,OPTIONS,POST,PUT')
  res.setHeader('Access-Control-Allow-Headers', 'Authorization, Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')

  if (req.method === 'OPTIONS') {
    res.end('')
  } else {
    if (pathRegex.test(req.url)) {
      try {
        const originRequest = req.headers.origin
        const authToken = req.headers['authorization']
        const jwtToken = authToken.replace('Bearer ', '').trim()
        const token = jwt.verify(jwtToken, Meteor.settings.jwtSecret)

        const integrationQuery = {
          integrationId: 'adIntegration',
          condoId: token.condoId,
          username: token.username
        }

        if (originRequest) {
          integrationQuery.originRequest = originRequest
        }

        const it = token ? IntegrationConfig.findOne(integrationQuery) : false

        if (it) {
          res.setHeader('Access-Control-Allow-Origin', originRequest || '*')
          next()
        } else {
          throw Error('Not found')
        }
      } catch (error) {
        res.writeHead(401, { 'Content-Type': 'application/json' })
        res.end(JSON.stringify({
          valid: false
        }))
      }
    } else {
      next()
    }
  }
}

export {
  validateRequestJWT
}
