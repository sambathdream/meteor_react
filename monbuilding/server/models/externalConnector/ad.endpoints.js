import { Meteor } from 'meteor/meteor'
import { validateRequestJWT } from './middlewares/validate-request-jwt'
import { jsonResponse, prefixEndpoint } from './helpers/endpoint-utils.helper'

import { createUsers } from '../integrations/active-directory/active-directory.service'
const DDPCommon = Package['ddp-common'].DDPCommon;

if (Meteor.isServer) {
  const externalEndpoint = `${prefixEndpoint}/external`
  const generateLoginUrl = `/generate-login-url`
  const autoLogin = `/fr/auto-login`
  const adEndpoint = `${prefixEndpoint}/users`
  const jwt = require('jsonwebtoken')
  const bodyParser = require('body-parser')
  Picker.middleware(bodyParser.json())
  Picker.middleware(bodyParser.urlencoded({ extended: false }))

  const postRoute = Picker.filter(function (req, res) {
    return req.method === 'POST'
  })

  const deleteRoute = Picker.filter(function (req, res) {
    return req.method === 'DELETE'
  })

  const getMethodInvocation = () => {
    return new DDPCommon.MethodInvocation({
      isSimulation: false,
      userId: 'EXagBySN2uRAjCBhk', // ADMIN ID
      // userId: 'sN4jSjLeFqvyz9pF5', // LOCAL ADMIN ID
      setUserId: () => {},
      unblock: () => {},
      connection: null,
      randomSeed: Math.random()
    })
  }

  const createUser = (firstName, lastName, email, condoId) => {
    return new Promise((resolve, reject) => {
      DDP._CurrentInvocation.withValue(getMethodInvocation(), async () => {
        try {
          const userId = await createUsers({
            user: {
              firstName,
              lastName,
              email,
              condoId
            },
            avoidEmail: true
          })
          Meteor.call('newResidentUserConfirm', userId, condoId)
          resolve(userId)
        } catch (error) {
          reject(error)
        }
      })
    })
  }

  const deleteUser = (userId, condoId) => {
    return new Promise((resolve, reject) => {
      DDP._CurrentInvocation.withValue(getMethodInvocation(), async () => {
        try {
          Meteor.call('removeResidentOfCondo', userId, condoId)
          resolve(userId)
        } catch (error) {
          reject(error)
        }
      })
    })
  }

  const validateToken = (token) => {
    return token ? (
      IntegrationConfig.findOne({
        integrationId: 'adIntegration',
        apiToken: token
      })
    ) : false
  }

  postRoute
    .route(generateLoginUrl, async function (params, req, res, next) {
      let authLoginUrl = ''
      const { email, token } = params.query
      const { firstName, lastName } = req.body
      const it = validateToken(token)

      if (it) {
        const condoId = it.condoId
        let user = email ? (
          Meteor.users.findOne({
            'emails.address': email
          })
        ) : false

        if (!user && email) {
          try {
            const newUserId = await createUser(firstName, lastName, email, condoId)
            user = Meteor.users.findOne({ _id: newUserId })
          } catch (error) {
            user = undefined
          }
        }

        if (user) {
          const { gestionnaireId, residentId } = user.identities
          const userId = user._id
          let currentUser
          
          if (residentId) {
            currentUser = Residents.findOne({
              userId,
              'condos.condoId': condoId
            })
          } else if (gestionnaireId) {
            currentUser = Enterprises.findOne({
              'users.userId': userId,
              condos: condoId
            })
            
            if (currentUser) {
              const _user = currentUser.users.filter(user => user.userId === userId)
              if (_user.length > 0) {
                const _condo = _user[0].condosInCharge.filter(condo => condo.condoId === condoId)
                if (_condo.length === 0) {
                  currentUser = false
                }
              } else {
                currentUser = false
              }
            }
          }

          if (currentUser) {
            const payload = {
              condoId: condoId
            }
            const secret = Meteor.settings.jwtSecret
            const options = { expiresIn: '60 years' }
            const token = jwt.sign(payload, secret, options)
  
            Meteor.users.update(userId,
              {
                $set: {
                  'autoLogin.token': token
                }
              }
            )
  
            authLoginUrl = `${process.env.ROOT_URL}${autoLogin.replace('/', '')}?token=${token}`
            jsonResponse({
              res,
              status: 200,
              body: { url: authLoginUrl }
            })
          }
        }
      }

      if (authLoginUrl.length === 0) {
        jsonResponse({
          res,
          status: 400,
          body: { message: `The request doesn't have necessary and correct data.` }
        })
      }
    })

  deleteRoute
    .route(`${adEndpoint}`, async function (params, req, res, next) {
      const { email, token } = params.query
      const it = validateToken(token)
      let sw = false

      if (it) {
        const condoId = it.condoId
        let user = email ? (
          Meteor.users.findOne({
            'emails.address': email,
            'profile.fromAD': true
          })
        ) : false

        if (user) {
          try {
            await deleteUser(user._id, condoId)
            sw = true
          } catch (error) { }
        }

        if (sw) {
          jsonResponse({
            res,
            status: 200,
            body: { message: 'User deleted.' }
          })
        }
      }
      if (!sw) {
        jsonResponse({
          res,
          status: 400,
          body: { message: `We can't delete this user.` }
        })
      }
    })
}
