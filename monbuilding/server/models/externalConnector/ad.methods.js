import { Meteor } from 'meteor/meteor'
const jwt = require('jsonwebtoken')

Meteor.startup(() => {

  Accounts.registerLoginHandler((loginRequest) => {
    const { ad, token } = loginRequest

    if (!ad || !token) {
      return undefined
    }

    let status = 200
    let message = ''
    let jwtToken

    try {
      jwtToken = jwt.verify(token, Meteor.settings.jwtSecret)
    } catch (error) {
      status = 400
      message = 'Invalid token'
    }

    if (status === 200) {
      const user = Meteor.users.findOne({
        'autoLogin.token': token
      })

      if (user) {
        const stampedToken = Accounts._generateStampedLoginToken()
        const hashStampedToken = Accounts._hashStampedToken(stampedToken)
        const userId = user._id
        Meteor.users.update(userId, {
          $push: {
            'services.resume.loginTokens': hashStampedToken
          }
        })
        message = 'Everything seems legit.'

        let currentUser = Meteor.users.findOne(userId)

        const { gestionnaireId, residentId } = currentUser.identities
        let condoId = jwtToken.condoId

        return {
          userId: userId,
          token: hashStampedToken,
          type: {
            gestionnaireId,
            residentId,
            condoId
          }
        }
      } else {
        status = 401
        message = 'User without access.'
      }
    }

    // TODO: this function comming from my first approach, that is why u will see status and message variables
    // we could use this for tracking
    console.log('message', message)

    return undefined
  })
})
