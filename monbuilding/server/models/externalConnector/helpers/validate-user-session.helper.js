import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

const validateUserSession = (userId = '', tokenToValidate = '') => {
  const hashedToken = Accounts._hashLoginToken(tokenToValidate)
  const user = Meteor.users.findOne({
    '_id': userId,
    'services.resume.loginTokens.hashedToken': hashedToken
  })

  if (user) {
    const currentToken = user.services.resume.loginTokens.find(token => token.hashedToken === hashedToken)

    const tokenExpires = Accounts._tokenExpiration(currentToken.when)
    if (new Date() < tokenExpires) {
      return true
    }
  }
  return false
}

export {
  validateUserSession
}
