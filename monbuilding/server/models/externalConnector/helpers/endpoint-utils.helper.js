const jsonResponse = ({ res, status, body }) => {
  res.writeHead(status, {
    'Content-Type': 'application/json'
  })

  res.end(JSON.stringify(body))
}

const prefixEndpoint = '/api/v1'

export {
  jsonResponse,
  prefixEndpoint
}
