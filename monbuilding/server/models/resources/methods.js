import { ModuleReservationError, Email } from "/common/lang/lang.js";
import safeGet from 'safe-get';

const tzOffset = 1;

Meteor.startup(function() {
    Meteor.methods({
        'updateEdlValidator': function (condoId, managerIds) {
            const resource = Resources.findOne({condoId, type: 'edl'});
            if (!!resource) {
                resource.needValidation.managerIds = managerIds;

                Resources.update({_id: resource._id}, {$set: resource});
                Reservations.update({resourceId: resource._id}, {$set: {validators: managerIds}})
            }
        },
        /*  ADD A NEW RESOURCE IN SPECIFIC CONDO FROM GESTIONNAIRE
         data                (Object) CONTAINS DATA FOR THE CREATION
         {
             condoId           (String) ID OF THE CONDO RELATED TO THE RESOURCE
             type              (String) TYPE OF THE RESOURCE
             horaires          (Table of Object) OPENING TIME OF THE RESOURCE (cf. SCHEMA)
             etage             (String) FLOOR OF THE RESOURCE IN CONDO
             name              (String) NAME OF THE RESOURCE
             seats             (Integer) NUMBER OF SEATS, ONLY IF "type" == "Salle de réunion"
             persons           (Integer) NUMBER OF STANDINGS, ONLY IF "type" == "Salle de réunion"
             number            (Integer) NUMBER OF THE PLACE, ONLY IF "type" == "Espace de coworking"
         }
         */
        'addResource': function(data) {
            const translation_ = new ModuleReservationError(!!data.lang ? data.lang : Meteor.user().profile.lang || "fr");
            let resource = {
                type: data.type,
                etage: data.etage,
                condoId: data.condoId,
                horaires: _.map(data.horaires, (hor) => {
                  return {
                    ...hor,
                    ...(hor.start === '00:00' && hor.end === '00:00' ? {end: '24:00'}: {}),
                    openHours: _.map(hor.openHours, (oh) => {
                      return {
                        ...oh,
                        ...(oh.start === '00:00' && oh.end === '00:00' ? {end: '24:00'}: {})
                      }
                    })
                  }
                }),
                note: !!data.note ? data.note : '',
                name: data.name,
                availableServices: data.availableServices,
                limitHoursBooked: data.limitHoursBooked,
                allowRecurringEvent: data.allowRecurringEvent,
                needValidation: data.needValidation
            };

            // check whether the condo alredy have EDL
            // if (data.type === "edl") {
            //   resource.number = data.number;
            //   const edl = Resources.find({
            //       type: 'edl',
            //       condoId: data.condoId
            //   }).fetch();
            //   if (edl.length > 0) {
            //       throw new Meteor.Error(401, translation_.moduleReservationError['edl_exist']);
            //   }
            //
            // }

            // TODO Fix this condition
            if (data.type === "meeting") { // Salle de réunion
              resource.seats = data.seats;
              resource.persons = data.persons;
              Resources.insert(resource);
            }
            else if ((data.type === "restaurant") || (data.type === "coworking")) { // Espace de coworking
              resource.number = data.number;
              Resources.insert(resource);
            }
            else if (data.type === "equipment") {
              resource.number = data.availability;
              Resources.insert(resource);
            }
            else {
              resource.number = data.number;
              resource.edlDuration = data.edlDuration;

              Resources.insert(_.omitBy(resource, function(value, key, object) {
                return _.isUndefined(value);
              }));
              // if (data.type === "edl") {
              //   // Update contacts
              //   const declaration = DeclarationDetails.findOne({key: 'edl'});
              //   const declarationId = declaration ? declaration._id : false;
              //   Meteor.call("updateCondoContact", data.condoId, declarationId, data.needValidation.managerIds, null, "Validation");
              // }
            }

            Meteor.call('addToGestionnaireHistory',
              'reservation',
              "",
              'newResource',
              resource.condoId,
              resource.name,
              Meteor.userId()
            )
        },
        /*  EDIT AN EXISTING RESOURCE FROM GESTIONNAIRE
         resourceId          (String) ID OF THE RESOURCE TO EDIT
         data                (Object) CONTAINS DATA FOR THE EDITION
         {
             horaires          (Table of Object) OPENING TIME OF THE RESOURCE (cf. SCHEMA)
             etage             (String) FLOOR OF THE RESOURCE IN CONDO
             name              (String) NAME OF THE RESOURCE
             seats             (Integer) NUMBER OF SEATS, ONLY IF "type" == "Salle de réunion"
             persons           (Integer) NUMBER OF STANDINGS, ONLY IF "type" == "Salle de réunion"
             number            (Integer) NUMBER OF THE PLACE, ONLY IF "type" == "Espace de coworking"
         }
         */
        'editResource': function(resourceId, data) {
            const oldResource = Resources.findOne(resourceId)
            if (!!oldResource) {
              const editData = {
                ...data,
                horaires: _.map(data.horaires, (hor) => {
                  return {
                    ...hor,
                    ...(hor.start === '00:00' && hor.end === '00:00' ? {end: '24:00'}: {}),
                    openHours: _.map(hor.openHours, (oh) => {
                      return {
                        ...oh,
                        ...(oh.start === '00:00' && oh.end === '00:00' ? {end: '24:00'}: {})
                      }
                    })
                  }
                })
              }

              Resources.update({_id: resourceId}, {$set: editData});

              if (!!safeGet(oldResource, 'needValidation.status') && !safeGet(editData, 'needValidation.status')) {
                // handle https://trello.com/c/qYey7sYr

                const pendingBook = Reservations.find({
                  resourceId,
                  pending: true,
                  start: {$gte: +moment().format('x')}
                }).fetch()
                if (pendingBook.length > 0) {
                  pendingBook.forEach((book) => {
                    const startMoment = moment(book.start)
                    const endMoment = moment(book.end)
                    Meteor.call('rejectBooking', {...book, rejected: false, startMoment, endMoment}, null, (err, res) => {
                      if (err) {
                        // const lang = Meteor.users.findOne({_id: book.origin}).profile.lang || 'fr'
                        // const translation = new ModuleReservationError(lang);
                        const fr = new ModuleReservationError('fr');
                        const en = new ModuleReservationError('en');
                        Meteor.call('rejectBooking', {...book, rejected: true, rejectionMessage: `${fr.moduleReservationError['auto_reject']}\n${en.moduleReservationError['auto_reject']}`, startMoment, endMoment}, null, (err2, res) => {
                          if (err2) {
                            // more error?
                          }
                        })
                      }
                    })
                  })
                }
                // Reservations.update({
                //   resourceId,
                //   pending: true,
                //   start: {$gte: +moment().format('x')}
                // }, {
                //   $set: {
                //     pending: false,
                //     needValidation: false
                //   },
                // }, {
                //   multi:true
                // })
              }
            }

            // const translation_ = new ModuleReservationError(Meteor.users.findOne({_id: Meteor.userId()}).profile.lang);
            // if (data.type !== "edl") {
            //     Resources.update({_id: resourceId}, {$set: data});
            // } else {
            //     const edl = Resources.find({
            //         type: 'edl',
            //         condoId: data.condoId,
            //         _id: {$ne: resourceId}
            //     }).fetch();
            //     if (edl.length > 0) {
            //         throw new Meteor.Error(401, translation_.moduleReservationError['edl_exist']);
            //     } else {
            //         Resources.update({_id: resourceId}, {$set: data});
            //     }
            //
            //     // Update contacts
            //     // const declaration = DeclarationDetails.findOne({key: 'edl'});
            //     // const declarationId = declaration ? declaration._id : false;
            //     // Meteor.call("updateCondoContact", data.condoId, declarationId, data.needValidation.managerIds, null, "Validation", (error, result) => {
            //     //   // console.log('error', error)
            //     // });
            // }

            Meteor.call('addToGestionnaireHistory',
              'reservation',
              resourceId,
              'editResource',
              data.condoId,
              data.name,
              Meteor.userId()
            )
        },
        // REMOVE AN EXISTING RESOURCE FROM GESTIONNAIRE
        // resourceId          (String) ID OF THE RESOURCE TO REMOVE
        'removeResource': function(resourceId, condoId) {
            const resource = Resources.findOne({_id: resourceId});
            Resources.remove({_id: resourceId});
            Reservations.remove((resourceId));
            Meteor.call('addToGestionnaireHistory',
              'reservation',
              resourceId,
              'deletedResource',
              condoId,
              !!resource && !!resource.name ? resource.name : "",
              Meteor.userId()
            )
        },
        'ServerScriptRemoveCoworkingAndEquipment': function () {
            if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
                throw new Meteor.Error(300, "Not Authenticated");
            let condos = Condos.find().fetch();
            _.each(condos, function(condo) {
                const resources = Resources.find({condoId: condo._id}).fetch();
                _.each(resources, function (res) {
                    if (res.type === 'coworking' || res.type === 'equipment') {
                        Resources.remove({_id: res._id});
                    }
                });
            });
        },
        'ServerScriptMigrateToNewReservation': function () {
            if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures)
                throw new Meteor.Error(300, "Not Authenticated");
            ResourceType.remove({});
            let resourceType = ResourceType.find().fetch();
            if (resourceType.length === 0) {
                const resourceTypeData = [{
                    'key': 'meeting',
                    'defaultLang': 'fr',
                    'value-fr': 'Salle de réunion',
                    'value-en': 'Meeting Room',
                    'options': {'seats': true, 'persons': true, 'number': false, 'availability': false, 'selectable': true}
                }, {
                    'key': 'coworking',
                    'defaultLang': 'fr',
                    'value-fr': 'Espace de CoWorking',
                    'value-en': 'Coworking Space',
                    'options': {'seats': false, 'persons': false, 'number': true, 'availability': false, 'selectable': true}
                }, {
                    'key': 'equipment',
                    'defaultLang': 'fr',
                    'value-fr': 'Equipement',
                    'value-en': 'Equipment',
                    'options': {'seats': false, 'persons': false, 'number': false, 'availability': true, 'selectable': true}
                }, {
                    'key': 'edl',
                    'defaultLang': 'fr',
                    'value-fr': 'État des lieux',
                    'value-en': 'Inventory of Fixtures',
                    'options': {'seats': false, 'persons': false, 'number': true, 'availability': false, 'selectable': true}
                }, {
                    'key': 'restaurant',
                    'defaultLang': 'fr',
                    'value-fr': 'Restaurant',
                    'value-en': 'Restaurant',
                    'options': {'seats': false, 'persons': false, 'number': true, 'availability': false, 'selectable': true}
                }, {
                    'key': 'other',
                    'defaultLang': 'fr',
                    'value-fr': 'Autre',
                    'value-en': 'Other',
                    'options': {'seats': false, 'persons': false, 'number': false, 'availability': false, 'selectable': true}
                }]
                _.each(resourceTypeData, function (rt) {
                    ResourceType.insert(rt)
                })
                console.log('done insert ResourceType')
            }
            let condos = Condos.find().fetch();
            _.each(condos, function(condo) {
                const resources = Resources.find({condoId: condo._id}).fetch();

                _.each(resources, function (res) {
                    let data = {
                        ...res,
                        type: newTypeKey(res.type),
                        etage: (typeof res.etage === 'string' ? +res.etage.replace(/\D+$/g, '') : res.etage)
                    };
                    _.each(res.events, function (e) {
                        Reservations.insert({
                            resourceId: res._id,
                            condoId: res.condoId,
                            ...e
                        })
                        console.log('insert resa: ' + e.eventId);
                    });
                    delete data.events;
                    Resources.update({_id: res._id}, {$set: data, $unset: {events: 1}});
                    console.log('update: ' + res.name);
                });
            });
        },
    });
});

function newTypeKey(type) {
    let newType = ''
    switch (type) {
        case 'Salle de réunion':
            newType = 'meeting';
            break ;
        case 'Espace de coworking':
            newType = 'coworking';
            break ;
        case 'Etat des lieux':
            newType = 'edl';
            break ;
        case 'Équipement':
            newType = 'equipment';
            break ;
        default:
            newType = type;
            break ;
    }

    return newType;
}
