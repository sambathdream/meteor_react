Meteor.startup(
  function () {
    /*
     PUBLISH ALL RESOURCES OF SPECIFIC CONDO
     */
    Meteor.publish('resources', function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let self = this;

      let handlers = []
      if (condoId) {
        let feed = Resources.find({condoId});
        handlers.push(feed.observe({
          added: function (document) {
            self.added("resources", document._id, document);
          },
          changed: function (newDocument, oldDocument) {
            self.changed("resources", oldDocument._id, newDocument);
          },
          removed: function (oldDocument) {
            self.removed("resources", oldDocument._id);
          },
        }))
        self.ready();

      } else if (Meteor.call('isGestionnaire', self.userId)) {
        let condoIds = _.map(_.find(Enterprises.findOne({ 'users.userId': self.userId }, { fields: { users: true } }).users, (user) => { return user.userId === self.userId }).condosInCharge, 'condoId');
        if (!!condoIds) {
          let feed;
          _.each(condoIds, function (condoId) {
            if (Meteor.userHasRight('reservation', 'seeResources', condoId)) {
              feed = Resources.find({condoId});
              handlers.push(feed.observe({
                added: function (document) {
                  self.added("resources", document._id, document);
                },
                changed: function (newDocument, oldDocument) {
                  self.changed("resources", oldDocument._id, newDocument);
                },
                removed: function (oldDocument) {
                  self.removed("resources", oldDocument._id);
                },
              }))
            }
          });
          self.ready();
        } else {
          self.stop();
        }
      }
      this.onStop(() => {
        handlers.forEach(handler => {
          handler.stop()
        })
        return true
      })

      return [
        ResourceType.find(),
        ResourceServices.find()
      ];
    });
  }
);
