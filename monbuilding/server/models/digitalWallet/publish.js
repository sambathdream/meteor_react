import { UsersCreditCard, UserTransactions } from '/common/collections/Payments'

Meteor.startup(function() {
  Meteor.publish('creditCard', function() {
    if (!Meteor.userId()) {
      throw new Meteor.Error(300, "Not Authenticated")
    }
    return UsersCreditCard.find({ userId: Meteor.userId() })
  });

  Meteor.publish('getUserTransactions', function(condoId) {
    if (!Meteor.userId()) {
      throw new Meteor.Error(300, "Not Authenticated")
    }
    return UserTransactions.find({ condoId: condoId, userId: Meteor.userId() }, { sort: { date: -1 }, fields: { vads_infos: 0, vads_return: 0 } })
  });
})
