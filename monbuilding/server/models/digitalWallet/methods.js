import { UsersCreditCard, UserTransactions } from '/common/collections/Payments'
import CryptoJS from 'crypto-js'
import uuid from "uuid"
import fetch from 'node-fetch'
import soap from 'soap'

Meteor.startup(function() {
  Meteor.methods({
    topUp: function(condoId, cardId, amount, vads_infos, isRecurring = false, frequency = null) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      check(condoId, String)
      check(cardId, String)
      check(amount, Match.Where(x => {
        return !isNaN(x) && x > 0
      }))
      check(vads_infos, Object)

      const transactionId = UserTransactions.insert({
        cardId,
        amount,
        type: 'TRANSACTION_top_up',
        condoId: condoId,
        isCredit: true,
        isRecurring: isRecurring,
        frequency: frequency,
        status: 'pending',
        date: Date.now(),
        userId: Meteor.userId(),
        vads_infos,
        vads_return: null
      })
      return transactionId
    },
    removeCreditCard: function(cardId) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      check(cardId, String)
      const cardData = UsersCreditCard.findOne({ _id: cardId })
      if (cardData.userId === Meteor.userId()) {
        UsersCreditCard.remove({ _id: cardId })
      } else {
        throw new Meteor.Error(403, "Not your card")
      }
    },
    saveCreditCard: function(formData, editCardId) {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      check(formData, Match.ObjectIncluding({
        name: String,
        number: String,
        cvc: String,
        expiration: String,
        type: Match.OneOf('visa', 'amex', 'mastercard', 'maestro'),
        sameBillingAddress: Boolean,
        street: String,
        code_city: String,
        country: String
      }))

      if (editCardId !== null) {
        const oldData = UsersCreditCard.findOne({ _id: editCardId })
        formData.userId = Meteor.userId()
        formData.createdAt = oldData.createdAt
        UsersCreditCard.update({ _id: editCardId }, {
          $set: formData
        })
      } else {
        formData.userId = Meteor.userId()
        formData.createdAt = Date.now()
        UsersCreditCard.insert(formData)
      }

    },
    addTestsCards: function() {
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      if (Meteor.isDevelopment || process.env.ROOT_URL !== "https://www.monbuilding.com/") {
        UsersCreditCard.update({ sameBillingAddress: { $exists: false } }, {
          $set: {
            "sameBillingAddress": true,
            "street": '',
            "code_city": '',
            "country": ''
          }
        })
        if (!UsersCreditCard.findOne({ number: CryptoJS.AES.encrypt("5000550000000029", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(), userId: Meteor.userId() })) {
          UsersCreditCard.insert({
              "name" : "PAYZEN TEST WORKING",
              "number" : CryptoJS.AES.encrypt("5000550000000029", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "displayableNumber": "**** 0029",
              "cvc" : CryptoJS.AES.encrypt("123", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "expiration" : CryptoJS.AES.encrypt("06/2019", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "type" : "maestro",
              "userId" : Meteor.userId(),
              "createdAt" : 1526845052700.0,
              "sameBillingAddress" : true,
              "street" : "",
              "code_city" : "",
              "country" : ""

          })
          UsersCreditCard.insert({
              "name" : "TEST PAYZEN 3D INTERACTIF",
              "number" : CryptoJS.AES.encrypt("4970100000000022", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "displayableNumber": "**** 0022",
              "cvc" : CryptoJS.AES.encrypt("123", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "expiration" : CryptoJS.AES.encrypt("06/2019", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "type" : "visa",
              "userId" : Meteor.userId(),
              "createdAt" : 1526850280549.0,
              "sameBillingAddress" : true,
              "street" : "",
              "code_city" : "",
              "country" : ""

          })
          UsersCreditCard.insert({
              "name" : "TEST PAYZEN ERROR CRYPTO",
              "number" : CryptoJS.AES.encrypt("4970100000000089", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "displayableNumber": "**** 0089",
              "cvc" : CryptoJS.AES.encrypt("123", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "expiration" : CryptoJS.AES.encrypt("06/2019", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "type" : "visa",
              "userId" : Meteor.userId(),
              "createdAt" : 1526850329978.0,
              "sameBillingAddress" : true,
              "street" : "",
              "code_city" : "",
              "country" : ""

          })
          UsersCreditCard.insert({
              "name" : "test 3d secure",
              "number" : CryptoJS.AES.encrypt("4970100000000014", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "displayableNumber": "**** 0014",
              "cvc" : CryptoJS.AES.encrypt("123", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "expiration" : CryptoJS.AES.encrypt("06/2019", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "type" : "visa",
              "userId" : Meteor.userId(),
              "createdAt" : 1526850394992.0,
              "sameBillingAddress" : true,
              "street" : "",
              "code_city" : "",
              "country" : ""

          })
          UsersCreditCard.insert({
              "name" : "TEST 3D SECURE ASKING CODE",
              "number" : CryptoJS.AES.encrypt("4970100000000009", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "displayableNumber": "**** 0009",
              "cvc" : CryptoJS.AES.encrypt("123", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "expiration" : CryptoJS.AES.encrypt("11/2030", '84816ff383c826e9c11e26fdbd8d66e4edd7cf1ec1a4b23f8d10d84aa56e0d8f').toString(),
              "type" : "visa",
              "userId" : Meteor.userId(),
              "createdAt" : 1527063388494.0,
              "sameBillingAddress" : true,
              "street" : "",
              "code_city" : "",
              "country" : ""

          })
        } else {
          throw new Meteor.Error(300, "Fake card already added")
        }
      } else {
        throw new Meteor.Error(300, "Can't add fake card on prod")
      }
    }
  })
})
