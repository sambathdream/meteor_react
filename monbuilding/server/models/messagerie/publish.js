Meteor.startup(
	function () {
		/* PUBLISH ALL MESSAGES OF RESIDENT ONLY CONCERNING CONNECTED USER (CONNECTED USER AS PARTICIPANT)*/

		Meteor.publish('isTyping', function(conversationId) {
			return IsTyping.find({conversationId: conversationId, userId: {$ne: this.userId}});
		});

		Meteor.publish('messengerFeed', function(convId, count) {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			return MessagesFeed.find({messageId: convId}, {sort: {date: -1}, limit: count});
		});

		Meteor.publish('statsMessengerFeed', function() {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			return MessagesFeed.find({}, {sort: {date: -1}});
		});

		Meteor.publish('messagerie', function() {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			return [Messages.find({"users.userId": this.userId})];
		});

		Meteor.publish('messagerieCS', function(condoId) {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
			if (syndicalRole.isUserMember(this.userId, condoId))
        return [Messages.find({ condoId: condoId, target: syndicalRole._id})];
			return [];
		});
		Meteor.publish('messagerieGardien', function(condoId) {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			let supervisorRole = DefaultRoles.findOne({for: "occupant", "name": "Gardien"});
			if (supervisorRole.isUserMember(this.userId, condoId))
        return [Messages.find({ condoId: condoId, target: supervisorRole._id })];
			return [];
		});
		/* PUBLISH ALL FILES OF RESIDENT IN "UserFiles" TABLE OF MESSAGES CONCERNING CONNECTED USER (CONNECTED USER AS PARTICIPANT)*/
		Meteor.publish('messagerieFiles', function (messageId) {
			if (!this.userId) {
				throw new Meteor.Error(300, "Not Authenticated");
			}

			return [
				IncidentFiles.find({
					$and: [
						{ 'meta.userId': {$exists: true} },
						{ 'meta.userId': this.userId }
					]
				}).cursor,
				MessengerFiles.find({
					'meta.msgId': messageId
				}).cursor
			]
		})

		Meteor.publish('messengerManagerIncidentFiles', function(incidentId) {
			return [IncidentFiles.find({
				$and: [
					{ "meta.incidentId": { $exists: true } },
					{ "meta.incidentId": incidentId }
				]
			}).cursor];
		});

		Meteor.publish('messagerieGestionnaireFiles', function() {
			if (!this.userId || !Meteor.call('isGestionnaire', this.userId))
				throw new Meteor.Error(300, "Not Authenticated");
			let self = this;

			let user = Meteor.user();
			let gestionnaire = Enterprises.findOne(user.identities.gestionnaireId);
			let userGestionnaire = _.find(gestionnaire.users, function (u) { return u.userId === self.userId });
			let condoIds = _.map(userGestionnaire.condosInCharge, function (c) { return c.condoId });

			let alreadyAdded = []

			const handler = MessengerFiles.find({ 'meta.condoId': { $in: condoIds } }).observe({
				added: function (document) {
					let thisConv = Messages.findOne({
						$and: [
							{ _id: document.meta.msgId },
							{$or : [{ "users.userId": self.userId }, {'incidentId': document.meta.incidentId}]}
						]
					});
					if (thisConv) {
						alreadyAdded.push(document._id)
						self.added('MessengerFiles', document._id, document);
					}
				},
				changed: function (document, oldDocument) {
					let thisConv = Messages.findOne({
						$and: [
							{ _id: document.meta.msgId },
							{ $or: [{ "users.userId": self.userId }, { 'meta.incidentId': document.meta.incidentId }] }
						]
					});
					if (thisConv) {
						if (_.includes(alreadyAdded, oldDocument._id)) {
							self.changed("MessengerFiles", oldDocument._id, document)
						} else {
							alreadyAdded.push(document._id)
							self.added('MessengerFiles', document._id, document);
						}
					}
				},
      });
      this.onStop(() => {
        return handler.stop()
      })
			return self.ready()
		});
	}
);
