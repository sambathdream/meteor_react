import {Email} from './../../../common/lang/lang';
import { Session } from '../../models_new/imports/session';

function updateGestionnaireReminder(userId, message, hasReminder, text) {
  let isGestionnaire = Meteor.users.findOne(userId).identities.gestionnaireId !== undefined;
	if (isGestionnaire || !hasReminder) {
		if (message.reminder)
    Meteor.call('removeEmailReminder', message.reminder);
		return null;
	}
	else if (!message.reminder) {
		const condoContact = CondoContact.findOne({condoId: message.condoId});
		let detailId = message.details;
		let typeMessageId = message.type;
		let condoContactList = _.find(condoContact.contactSet, function(contact) {
			return contact.declarationDetailId == detailId._id && contact.messageTypeId == typeMessageId;
		})

		let emailList = [condoContact.defaultContact.userId];

		if (!condoContactList || condoContactList.userIds.length == 0)
    condoContactList = _.find(condoContact.contactSet, function(contact) {
      return contact.declarationDetailId == detailId._id;
    })

		if (condoContactList && condoContactList.userIds.length > 0)
    emailList = _.without(condoContactList.userIds, null, undefined);

		let remindHours = getReminderValue(message.condoId, message.urgent);
		return Meteor.call('programEmailReminder', {
			to: emailList,
			condoId: message.condoId,
			emailTemplate: "reminder-message-fr",
			emailData: {
				msgId: message._id,
				message: text.length > 150 ? text.substr(0, 150) + "..." : text,
				module: "messagerie",
				condoId: message.condoId,
				from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
        setReplyLink: Meteor.absoluteUrl(`fr/gestionnaire/messagedetails/${message._id}`, message.condoId)
			},
			remindHours: remindHours,
			subject: "RAPPEL - Nouveau message : \"Messenger\""
		});
	}
	else
  return message.reminder;
}

let IsTypingTimers = {};

Meteor.startup(function() {
	Meteor.methods({

		userIsTyping(conversationId) {
			if (this.userId) {
        const userId = this.userId
        const userProfile = Meteor.user().profile
				IsTyping.upsert({userId: userId, conversationId: conversationId}, {
					conversationId: conversationId,
					userId: userId,
          firstname: `${userProfile.firstname}`,
          lastname: `${userProfile.lastname}`,
          fullname: `${userProfile.firstname} ${userProfile.lastname}`,
					date: Date.now()
        })
			}
		},

		userIsNotTyping(conversationId) {
			if (this.userId) {
				IsTyping.remove({userId: this.userId, conversationId: conversationId});
			}
		},
    getUserStatusInConversation (conversationId) {
      check(conversationId, String)
      if (!Match.test(conversationId, String)) {
        throw new Meteor.Error(401, "Invalid parameters \'conversationId\' getUserStatusInConversation")
      }
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      const conversation = Messages.findOne({ _id: conversationId })
      if (!conversation) {
        return []
      }
      let userIdInConv = _.map(conversation.users, user => user.userId)
      userIdInConv = userIdInConv.filter(userId => userId !== Meteor.userId() )

      let users = _.map(Meteor.users.find({
        _id: { $in: userIdInConv }
      }, {
        fields: {
          'status.online': true,
          'status.idle': true,
          'profile.avatarId': true,
          'profile.firstname': true,
          'profile.lastname': true
        }
      }).fetch(), function (user) {
        return {
          _id: user._id,
          status: user.status.online ? (user.status.idle ? 'idle' : 'online') : 'offline',
        }
      })
      return users;
    },

		removeConversation(convId) {
			Messages.remove(convId);
		},

		removeMessageFeed(messageId) {
			if (this.userId) {
				let message = MessagesFeed.findOne(messageId);
				if (!message || message == undefined)
        return -1;
				if (message.userId != this.userId)
        return -2;
				MessagesFeed.remove(messageId);
				let lastMsg = MessagesFeed.findOne({messageId: message.messageId}, {sort: {date: -1}});
				if (lastMsg) {
          Messages.update({_id: message.messageId}, {$set: {lastMsg: lastMsg.date, lastMsgString: lastMsg.text, lastMsgUserId : lastMsg.userId}});
				} else {
          Messages.remove(message.messageId);
				}
				return 0;
			}
		},

		updateMessageFeed(messageId, newText, filesId) {
			if (this.userId) {
				let message = MessagesFeed.findOne(messageId);
				newText = newText.trim();
				if (!message || message == undefined)
        return -1;
				if (message.userId != this.userId)
        return -2;
				// if (!newText || newText == undefined || newText == "")
        //   return -3;
        const conversation = Messages.findOne({ _id: message.messageId })
        _.each(filesId, (f) => {
          MessengerFiles.update({ _id: f },
            {
              $set:
              {
                userId: this.userId,
                'meta.userId': this.userId,
                'meta.condoId': message.condoId,
                'meta.msgId': conversation._id
              }
            });
          });
          MessagesFeed.update({ _id: message._id }, { $set: { text: newText, files: filesId, lastUpdated: Date.now() } });
          let checkMessage = MessagesFeed.findOne({messageId: message.messageId}, {sort: {date: -1}});
          if (checkMessage._id == message._id)
          Messages.update({ _id: message.messageId }, { $set: { lastMsg: Date.now(), lastMsgString: newText, lastMsgUserId: this.userId } });
          return 0;
        }
      },
    /**
     * BLOCK USER
     *
     * @param userId
     */
      blockUser(userId) {
        const blockedUsers = Meteor.users.findOne({_id: Meteor.userId()}).blockedUsers
        Meteor.users.update(
          {_id: Meteor.userId()},
          {$set:
            {
              "blockedUsers": !!blockedUsers? [...blockedUsers, userId]: [userId]
            }
          }
        );

        const blockedBy = Meteor.users.findOne({_id: userId}).blockedBy
        Meteor.users.update(
          {_id: userId},
          {$set:
            {
              "blockedBy": !!blockedBy? [...blockedBy, Meteor.userId()]: [Meteor.userId()]
            }
          }
        );
      },
    /**
     * UNBLOCK USER
     *
     * @param userId
     */
      unBlockUser(userId) {
        const blockedUsers = Meteor.users.findOne({_id: Meteor.userId()}).blockedUsers
        Meteor.users.update(
          {_id: Meteor.userId()},
          {$set:
            {
              "blockedUsers": !!blockedUsers? _.filter(blockedUsers, (b) => b !== userId): []
            }
          }
        );

        const blockedBy = Meteor.users.findOne({_id: userId}).blockedBy
        Meteor.users.update(
          {_id: userId},
          {$set:
            {
              "blockedBy": !!blockedBy? _.filter(blockedBy, (b) => b !== Meteor.userId()): []
            }
          }
        );
      },
      // DISABLE REMINDERS SENT TO GESTIONNAIRE
      // msgId                 (String) ID OF THE MESSAGE
      disableReminder(msgId) {
        let m = Messages.findOne(msgId);
        if (m.reminder) {
          Meteor.call('removeEmailReminder', m.reminder);
          Messages.update(msgId, {$set: {reminder: null}});
        }
      },
      /*  INVITE NEW USER(S) TO EXISTING CONVERSATION AND SEND EMAIL INVITES
      msgId               (String) ID OF THE MESSAGE
      users               (Table of String) IDS OF THE USERS TO INVITE
      */
      inviteUserToConversation(msgId, users) {
        let date = new Date();
        date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
        let message = Messages.findOne(msgId);
        let userAllreadyInConv = message.participants || [];
        users.forEach((id) => {
          Messages.update(msgId, {
            $push: {users: {userId: id}},
            $addToSet: {participants: id}
          });
          MessagesFeed.insert({ messageId: msgId, date: Date.now(), userId: id, text: null, files: null, isNewUserMessage: true });
          let replyLink = "";
          let lang = Meteor.users.findOne({_id: id}).profile.lang || 'fr';
          try {
            if (Meteor.call('isGestionnaireOfCondo', id, message.condoId) === true)
						replyLink = Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${message._id}`, message.condoId);
            else if (Meteor.call('isResidentOfCondo', id, message.condoId) === true)
            replyLink = Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/messenger/${message.target}/${message._id}`, message.condoId);
            else return;
          }
          catch (e) {}
          var translation = new Email(lang);
          const defaultRole = DefaultRoles.findOne({ _id: message.target })
          if (!!defaultRole === true && checkNotifToResident(defaultRole.name, id, message.condoId) == true)
					Meteor.call('scheduleAnEmail', {
						templateName: 'new-message-' + lang,
						to: Meteor.users.findOne({_id: id}).emails[0].address,
						condoId: message.condoId,
						subject: translation.email['new_mess_mess'],
						data: {
							title: translation.email['invit_conv'],
							module: "messagerie",
							message: "",
							condoId: message.condoId,
              notifUrl: Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/profile/notifications`, message.condoId),
							from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
							setReplyLink: replyLink
						},
						dueTime: date,
						condition: {
							name: "checkMessageBeforeSend",
							data: {msgId: msgId, userId: id, condoId: message.condoId, createdAt: Date.now()}
						}
					});
        });
        OneSignal.MbNotification.sendToUserIds(users, {
          type: 'messenger',
          key: 'add_occupant',
          dataId: message._id,
          condoId: message.condoId,
          userAllreadyInConv,
          data: {
            target: message.target
          }
        })
        for (const user of users) {
          OneSignal.MbNotification.sendToUserIds(userAllreadyInConv, {
            type: 'messenger',
            key: 'join_conversation',
            userToAdd: user,
            dataId: message._id,
            condoId: message.condoId,
            data: {
              target: message.target
            }
          })
        }
      },
      /*  ADD A MESSAGE IN EXISTING CONVERSATION'S FEED AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS, AND CREATE REMINDER IF GESTIONNAIRE AS PARTICIPANT
      msgId                 (String) ID OF THE MESSAGE
      text                  (String) TEXT OF THE MESSAGE
      files                 (Table of String) FILES ATTACHED TO THE MESSAGE
      */
      addComment: function (msgId, text, files, sendNotif) {
        const session = new Session();
        let message = Messages.findOne({ _id: msgId });
        const defaultRole = DefaultRoles.findOne({ _id: message.target })
        let userId = this.userId;

        _.each(files, (f) => {
          MessengerFiles.update({_id: f},
            {$set:
              {
                userId: this.userId,
                'meta.condoId': message.condoId,
                'meta.msgId': msgId
              }
            });
          });
          Messages.update(
            {_id: msgId},
            {
              $set: {
                lastMsg: Date.now(),
                userUpdateView: [this.userId],
                lastMsgString: text,
                lastMsgUserId: this.userId,
                isArchived: false,
                managerReplied: session.getValue('role') === 'manager'
              }
            }
          );

          let self = this

          const messageFeedData = {
            messageId: msgId,
            date: Date.now(),
            userId,
            text
          }

          if (files.length > 0) {
            messageFeedData.files = files
          }

          let msgFeedId = MessagesFeed.insert(messageFeedData)
          let fromOccupant = !!Meteor.user().identities.residentId
          if (message.target === 'manager' && fromOccupant && message.global !== true) {

            const condoContact = CondoContact.findOne({ condoId: message.condoId });
            let detailId = message.details;
            let typeMessageId = message.type;
            let condoContactList = _.find(condoContact.contactSet, function (contact) {
              return contact.declarationDetailId == detailId._id && contact.messageTypeId == typeMessageId;
            })

            let emailList = [condoContact.defaultContact.userId];

            if (!condoContactList || condoContactList.userIds.length == 0)
            condoContactList = _.find(condoContact.contactSet, function (contact) {
              return contact.declarationDetailId == detailId._id;
            })

            if (condoContactList && condoContactList.userIds.length > 0)
            emailList = _.without(condoContactList.userIds, null, undefined);

            OneSignal.MbNotification.sendToUserIds(emailList, {
              type: 'messenger',
              key: 'new_message_other',
              dataId: msgFeedId,
              condoId: message.condoId,
              data: {
                target: message.target
              }
            })

          } else {
            let pushNotificationUserIds = _.map(message.users, (u) => {
              return u.userId
            })
            OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
              type: 'messenger',
              key: 'new_message',
              dataId: msgFeedId,
              condoId: message.condoId,
              data: {
                target: message.target
              }
            })
          }

          if ((message.users.length == 0 || _.find(message.users, (u) => {return u.userId === userId}) == undefined)) {
            Messages.update(msgId, {$push: {users: {userId: userId}}});
          }
          if ((message.target === "manager") && Meteor.call("isGestionnaire", userId) && (!message.resp || message.resp.length == 0 || _.find(message.resp, (r) => {return r.userId === userId}) == undefined))
          {
            let resp = Messages.update(
              {_id: msgId, "users.userId": userId},
              {$push: {resp: {userId}}}
            );
          }
          Messages.update(
            {_id: msgId, "users.userId": userId},
            {$set: {"users.$.lastVisit": Date.now()}}
          );
          if ((message.target === "manager") && message.global !== true) {
            let reminder = updateGestionnaireReminder(userId, message, sendNotif, text);
            Messages.update(msgId, {$set: {reminder: reminder}});
          }
          if (message && message.users && !!defaultRole === true && defaultRole.name === "Occupant") {
            let date = new Date();
            date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
            message.users.forEach((user) => {
              if (user.userId !== userId && Meteor.users.findOne(user.userId)) {
                let lang = Meteor.users.findOne(user.userId).profile.lang || "fr";
                let replyLink = "";
                try {
                  if (Meteor.call('isGestionnaireOfCondo', user.userId, message.condoId) === true)
                  replyLink = Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${message._id}`, message.condoId);
                  else if (Meteor.call('isResidentOfCondo', user.userId, message.condoId) === true)
                  replyLink = Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/messenger/${defaultRole._id}/${message._id}`, message.condoId);
                  else return;
                }
                catch (e) {return;}
                var translation = new Email(lang);
                if (checkNotifToResident(defaultRole.name, user.userId, message.condoId) == true)
                Meteor.call('scheduleAnEmail', {
                  templateName: 'new-message-' + lang,
                  to: Meteor.users.findOne({_id: user.userId}).emails[0].address,
                  condoId: message.condoId,
                  subject: translation.email['new_mess_mess'],
                  data: {
                    title: translation.email['new_unread_mess'],
                    message: text.length > 150 ? text.substr(0, 150) + "..." : text,
                    module: "messagerie",
                    condoId: message.condoId,
                    notifUrl: Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/profile/notifications`, message.condoId),
                    from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                    setReplyLink: replyLink
                  },
                  dueTime: date,
                  condition: {
                    name: "checkMessageBeforeSend",
                    data: {msgId: msgId, userId: user.userId, condoId: message.condoId, createdAt: Date.now()}
                  }
                });
              }
            });
          }
          if (message && message.users && !!defaultRole === true && defaultRole.name === "Conseil Syndical") {
            let memberCS = Meteor.call('getCSOfCondo', message.condoId);
            memberCS = _.filter(memberCS, (elem) => {return elem !== self.userId});
            if (message.origin !== self.userId && _.find(memberCS, (elem) => {return elem === message.origin}) === undefined)
            memberCS.push(message.origin);
            let date = new Date();
            date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
            if (memberCS.length > 0) {
              _.each(memberCS, (memberId) => {
                let lang = Meteor.users.findOne(memberId).profile.lang || "fr";
                var translation = new Email(lang);
                if (checkNotifToResident(defaultRole.name, memberId, message.condoId) == true)
                Meteor.call('scheduleAnEmail', {
                  templateName: 'new-message-' + lang,
                  to: Meteor.users.findOne(memberId).emails[0].address,
                  condoId: message.condoId,
                  subject: translation.email['new_mess_uc_mess2'],
                  data: {
                    title: translation.email['new_unread_uc2'],
                    message: text.length > 150 ? text.substr(0, 150) + "..." : text,
                    module: "messagerie",
                    condoId: message.condoId,
                    notifUrl: Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/profile/notifications`, message.condoId),
                    from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                    setReplyLink: Meteor.absoluteUrl(`${Meteor.users.findOne(memberId).profile.lang}/resident/${message.condoId}/messenger/${defaultRole._id}/${message._id}`, message.condoId)
                  },
                  dueTime: date,
                  condition: {
                    name: "checkMessageBeforeSend",
                    data: {
                      msgId: msgId,
                      userId: memberId,
                      condoId: message.condoId,
                      createdAt: Date.now(),
                      isCS: true
                    }
                  }
                });
              });
            }
          }
          if (message && message.users && !!defaultRole === true && defaultRole.name === "Gardien") {
            let memberKeeper = Meteor.call('getKeeperOfCondo', message.condoId);
            memberKeeper = _.filter(memberKeeper, (elem) => {return elem !== self.userId});
            if (message.origin !== self.userId && _.find(memberKeeper, (elem) => {return elem === message.origin}) === undefined)
            memberKeeper.push(message.origin);
            let date = new Date();
            date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
            if (memberKeeper.length > 0) {
              _.each(memberKeeper, (memberId) => {
                let lang = Meteor.users.findOne(memberId).profile.lang || "fr";
                var translation = new Email(lang);
                if (checkNotifToResident(defaultRole.name, memberId, message.condoId) == true)
                Meteor.call('scheduleAnEmail', {
                  templateName: 'new-message-' + lang,
                  to: Meteor.users.findOne(memberId).emails[0].address,
                  condoId: message.condoId,
                  subject: translation.email['new_mess_keeper2'],
                  data: {
                    title: translation.email['mess_unread_keeper2'],
                    message: text.length > 150 ? text.substr(0, 150) + "..." : text,
                    module: "messagerie",
                    condoId: message.condoId,
                    notifUrl: Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/profile/notifications`, message.condoId),
                    from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                    setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/messenger/${defaultRole._id}/${message._id}`, message.condoId)
                  },
                  dueTime: date,
                  condition: {
                    name: "checkMessageBeforeSend",
                    data: {
                      msgId: msgId,
                      userId: memberId,
                      condoId: message.condoId,
                      createdAt: Date.now(),
                      isCS: true
                    }
                  }
                });
              });
            }
          }
          if (message && message.users && message.target === "manager" && message.global !== true) {
            let type = IncidentType.findOne(message.type)
            let declarationDetail = DeclarationDetails.findOne(message.details)
            let usersInConv = message.users;
            usersInConv = _.filter(usersInConv, (elem) => {return elem !== self.userId});
            let date = new Date();
            date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
            if (usersInConv.length > 0) {
              _.each(usersInConv, (userInConv) => {
                memberId = userInConv.userId
                if (memberId === Meteor.userId()) {
                  return
                }
                let user = Meteor.users.findOne(memberId) || {}
                let lang = user.profile.lang || "fr";
                var translation = new Email(lang);
                if ((checkNotifToGestionnaire(_.clone(memberId), message.condoId) == true) || (checkNotifFromGestionnaireToResident(_.clone(memberId), message.condoId) == true)) {
                  Meteor.call('scheduleAnEmail', {
                    templateName: 'new-message-' + lang,
                    to: user.emails[0].address,
                    condoId: message.condoId,
                    subject: user.profile.role === 'Gestionnaire' ? translation.email['new_mess_admin'] : translation.email['mess_from_admin'],
                    data: {
                      title: translation.email['new_unread_mess'],
                      type: type ? type["value-" + lang] : null,
                      details: declarationDetail ? declarationDetail["value-" + lang] : null,
                      message: text.length > 150 ? text.substr(0, 150) + "..." : text,
                      module: "messagerie",
                      condoId: message.condoId,
                      notifUrl: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, message.condoId) : Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/profile/notifications`, message.condoId),
                      from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                      setReplyLink: user.profile.role === 'Gestionnaire' ? Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${msgId}`, message.condoId) : Meteor.absoluteUrl(`${lang}/resident/${message.condoId}/messenger/manager/${message._id}`, message.condoId)
                    },
                    dueTime: date,
                    condition: {
                      name: "checkMessageBeforeSend",
                      data: {
                        msgId: msgId,
                        userId: memberId,
                        condoId: message.condoId,
                        createdAt: Date.now()
                      }
                    }
                  });
                }
              });
            }
          }
        },
        /*  CREATE A NEW MESSAGE FROM RESIDENT TO RESIDENT AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS
        data                  (Object) CONTAINS DATA FOR THE CREATION
        {
          target            (String) "Resident" (FROM RESIDENT TO RESIDENT)
          destinataires     (Table of String) CONTAINS USERIDS OF PARTICIPANTS (INCLUDING THE CREATOR'S)
          condoId           (Striing) ID OF THE CONDO RELATED TO THE CONVERSATION
          message           (String) TEXT OF MESSAGE
          files             (Table of String) FILES ATTACHED TO THE MESSAGE
          sujet             (String) MAIN TITLE OF THE REQUEST (NOT USED FROM RESIDENT TO RESIDENT)
        }
        */
        addMessage: function(data) {
          const userSet = new Set(data.destinataires);
          userSet.add(this.userId);
          let msgId = Messages.insert({
            // NEW MESSAGE DATA
            createdBy: this.userId,
            participants: Array.from(userSet),
            userUpdateView: [this.userId],
            destination: 'occupant',
            // ////////////////
            date: Date.now(),
            condoId: data.condoId,
            users: [],
            target: data.target,
            origin: this.userId,
            sujet: data.sujet,
            lastMsg: Date.now()
          });
          const defaultRole = DefaultRoles.findOne({ _id: data.target })
          let users = [];
          let date = new Date();
          date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
          let userId = this.userId;

          _.each(data.files, (f) => {
            MessengerFiles.update({_id: f},
              {$set:
                {
                  'meta.condoId': data.condoId,
                  'meta.msgId': msgId
                }
              });
            });
            let feedDate = Date.now(); //to avoid bug in badges//
            data.destinataires.forEach((id) => {
              if (id !== userId) {
                let currentUser = Meteor.users.findOne({_id: id})
                let lang = currentUser.profile.lang || "fr";
                var translation = new Email(lang);
                let replyLink = "";
                try {
                  if (Meteor.call('isGestionnaireOfCondo', id, data.condoId) === true)
                  replyLink = Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${msgId}`, data.condoId);
                  else if (Meteor.call('isResidentOfCondo', id, data.condoId) === true)
                  replyLink = Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/messenger/${data.target}/${msgId}`, data.condoId);
                  else return;
                }
                catch (e) {}
                if (checkNotifToResident(defaultRole.name, id, data.condoId) == true) {
                  Meteor.call('scheduleAnEmail', {
                    templateName: 'new-message-' + lang,
                    to: Meteor.users.findOne({_id: id}).emails[0].address,
                    condoId: data.condoId,
                    subject: translation.email['new_mess_mess'],
                    data: {
                      title: translation.email['new_unread_mess'],
                      message: data.message.length > 150 ? data.message.substr(0, 150) + "..." : data.message,
                      module: "messagerie",
                      condoId: data.condoId,
                      notifUrl: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/profile/notifications`, data.condoId),
                      from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                      setReplyLink: replyLink
                    },
                    dueTime: date,
                    condition: {
                      name: "checkMessageBeforeSend",
                      data: {msgId: msgId, userId: id, condoId: data.condoId, createdAt: Date.now()}
                    }
                  });
                }
                users.push({"userId": id});
              }
              else
              users.push({"userId": id, "lastVisit": Date.now(), "createdBy": true});
            });
            Messages.update(msgId, {$set: {userUpdateView: [this.userId], users: users, lastMsgString: data.message, lastMsgUserId: this.userId}});

            const messageFeedData = {
              messageId: msgId,
              date: feedDate,
              userId: this.userId,
              text: data.message,
              files: data.files
            }

            let msgFeedId = MessagesFeed.insert(messageFeedData);
            let pushNotificationUserIds = _.filter(_.map(users, (u) => {
              if (u.userId !== Meteor.userId())
              return u.userId
            }), (userId) => userId !== undefined)
            OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
              type: 'messenger',
              key: 'new_message',
              dataId: msgFeedId,
              condoId: data.condoId,
              data: {
                target: data.target
              }
            })
            return msgId;
          },
          /*  CREATE A NEW MESSAGE FROM RESIDENT TO CO-OWNCER COUNCIL AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS
          data                  (Object) CONTAINS DATA FOR THE CREATION
          {
            condoId           (Striing) ID OF THE CONDO RELATED TO THE CONVERSATION
            message           (String) TEXT OF MESSAGE
            files             (Table of String) FILES ATTACHED TO THE MESSAGE
          }
          */
          addMessageCS: function ({ condoId, message, files, otherMembers, target }) {
            let memberCS = Meteor.call('getCSOfCondo', condoId);
            const userSet = new Set(memberCS);
            userSet.add(this.userId);
            let msgId = Messages.insert({
              // NEW MESSAGE DATA
              createdBy: this.userId,
              participants: Array.from(userSet),
              userUpdateView: [this.userId],
              destination: 'council',
              // ////////////////
              date: Date.now(),
              condoId: condoId,
              origin: this.userId,
              users: [{userId: this.userId, lastVisit: Date.now()}],
              target: target,
              lastMsg: Date.now()
            });

            memberCS = _.filter(memberCS, (elem) => {return elem !== this.userId});
            memberCS = memberCS.concat(otherMembers);
            let users = [];
            let date = new Date();
            date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
            let userId = this.userId;

            files = files.filter(file => !file.video)
            _.each(files, (f) => {
              UserFiles.update({ _id: f },
                {
                  $set:
                  {
                    meta: { msgId: msgId }
                  }
                });
              });
              let feedDate = Date.now(); //to avoid bug in badges//
              users.push({"userId": userId, "lastVisit": Date.now(), "createdBy": true});
              if (memberCS.length > 0) {
                _.each(memberCS, (memberId) => {
                  users.push({"userId": memberId});
                  Messages.update({_id: msgId}, {
                    $push : {
                      users: {
                        userId: memberId
                      }
                    }
                  });
                  let lang = (Meteor.users.findOne({_id: memberId}) || {}).profile.lang || "fr";
                  var translation = new Email(lang);
                  if (checkNotifToResident("Conseil Syndical", memberId, condoId) == true) {
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-message-' + lang,
                      to: Meteor.users.findOne(memberId).emails[0].address,
                      condoId: condoId,
                      subject: translation.email['new_mess_uc_mess2'],
                      data: {
                        title: translation.email['new_unread_uc2'],
                        message: message.length > 150 ? message.substr(0, 150) + "..." : message,
                        module: "messagerie",
                        condoId: condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/resident/${condoId}/profile/notifications`, condoId),
                        from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                        setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/messenger/${target}/${msgId}`, condoId)
                      },
                      dueTime: date,
                      condition: {
                        name: "checkMessageBeforeSend",
                        data: {msgId: msgId, userId: memberId, condoId: condoId, createdAt: Date.now(), isCS: true}
                      }
                    });
                  }
                });
              }
              Messages.update(msgId, {$set: {userUpdateView: [this.userId], users: users, lastMsgString: message, lastMsgUserId: this.userId}});

              const messageFeedData = {
                messageId: msgId,
                date: feedDate,
                userId: this.userId,
                text: message,
                files: files
              }

              let msgFeedId = MessagesFeed.insert(messageFeedData)
              let pushNotificationUserIds = _.filter(_.map(users, (u) => {
                if (u.userId !== Meteor.userId())
                return u.userId
              }), (userId) => userId !== undefined)
              OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
                type: 'messenger',
                key: 'new_message',
                dataId: msgFeedId,
                condoId: condoId,
                data: {
                  target: target
                }
              })
              return msgId;
            },
            /*  CREATE A NEW MESSAGE FROM RESIDENT TO KEEPER AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS
            data                  (Object) CONTAINS DATA FOR THE CREATION
            {
              condoId           (Striing) ID OF THE CONDO RELATED TO THE CONVERSATION
              message           (String) TEXT OF MESSAGE
              files             (Table of String) FILES ATTACHED TO THE MESSAGE
            }
            */
            addMessageKeeper: function ({ condoId, message, files, otherMembers, target }) {
              let memberKeeper = Meteor.call('getKeeperOfCondo', condoId);

              memberKeeper = _.filter(memberKeeper, (elem) => {return elem !== this.userId});
              memberKeeper = memberKeeper.concat(otherMembers);
              const userSet = new Set(memberKeeper);
              userSet.add(this.userId);
              let msgId = Messages.insert({
                // NEW MESSAGE DATA
                createdBy: this.userId,
                participants: Array.from(userSet),
                userUpdateView: [this.userId],
                destination: 'keeper',
                // ////////////////
                date: Date.now(),
                condoId: condoId,
                origin: this.userId,
                users: [{userId: this.userId, lastVisit: Date.now()}],
                target: target,
                lastMsg: Date.now()
              });

              let date = new Date();
              let users = [];
              date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
              let userId = this.userId;

              _.each(files, (f) => {
                UserFiles.update({_id: f},
                  {$set:
                    {
                      meta: {msgId: msgId}
                    }
                  });
                });
                users.push({"userId": userId, "lastVisit": Date.now(), "createdBy": true});
                let feedDate = Date.now(); //to avoid bug in badges//
                if (memberKeeper.length > 0) {
                  _.each(memberKeeper, (memberId) => {
                    users.push({"userId": memberId});
                    Messages.update({_id: msgId}, {
                      $push : {
                        users: {
                          userId: memberId
                        }
                      }
                    });
                    console.log('memberId', memberId)
                    let lang = Meteor.users.findOne({_id: memberId}).profile.lang || "fr";
                    var translation = new Email(lang);
                    if (checkNotifToResident("Gardien", memberId, condoId) == true)
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-message-' + lang,
                      to: Meteor.users.findOne(memberId).emails[0].address,
                      condoId: condoId,
                      subject: translation.email['new_mess_keeper2'],
                      data: {
                        title: translation.email['mess_unread_keeper2'],
                        message: message.length > 150 ? message.substr(0, 150) + "..." : message,
                        module: "messagerie",
                        condoId: condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/resident/${condoId}/profile/notifications`, condoId),
                        from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                        setReplyLink: Meteor.absoluteUrl(`${lang}/resident/${condoId}/messenger/${target}/${msgId}`, condoId)
                      },
                      dueTime: date,
                      condition: {
                        name: "checkMessageBeforeSend",
                        data: {msgId: msgId, userId: memberId, condoId: condoId, createdAt: Date.now(), isKeeper: true}
                      }
                    });
                  });
                }
                Messages.update(msgId, {$set: {userUpdateView:[this.userId], users: users, lastMsgString: message, lastMsgUserId: this.userId}});

                const messageFeedData = {
                  messageId: msgId,
                  date: feedDate,
                  userId: this.userId,
                  text: message,
                  files: files
                }


                let msgFeedId = MessagesFeed.insert(messageFeedData)
                let pushNotificationUserIds = _.filter(_.map(users, (u) => {
                  if (u.userId !== Meteor.userId())
                  return u.userId
                }), (userId) => userId !== undefined)
                OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
                  type: 'messenger',
                  key: 'new_message',
                  dataId: msgFeedId,
                  condoId: condoId,
                  data: {
                    target: target
                  }
                })
                return msgId;
              },
              /*  CREATE A NEW MESSAGE FROM GESTIONNAIRE TO RESIDENT AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS
              data                  (Object) CONTAINS DATA FOR THE CREATION
              {
                destinataires     (Table of String) CONTAINS USERIDS OF PARTICIPANTS (INCLUDING THE CREATOR'S)
                condoId           (Striing) ID OF THE CONDO RELATED TO THE CONVERSATION
                message           (String) TEXT OF MESSAGE
                files             (Table of String) FILES ATTACHED TO THE MESSAGE
                details           (String) DETAILS ABOUT THE REQUEST
              }
              */
              addMessageAsGestionnaire: function(data) {
                console.log('addMessageAsGestionnaire')
                const userSet = new Set(data.destinataires);
                userSet.add(this.userId);

                let msgId = Messages.insert({
                  // NEW MESSAGE DATA
                  createdBy: this.userId,
                  participants: Array.from(userSet),
                  userUpdateView: [this.userId],
                  destination: 'occupant',
                  // ////////////////
                  date: Date.now(),
                  condoId: data.condoId,
                  type: data.typeMessage,
                  details: data.details,
                  users: [],
                  target: "manager",
                  sujet: "-",
                  origin: this.userId,
                  statut: "Locataire",
                  lot: "-",
                  urgent: false,
                  resp: [{userId: this.userId}],
                  lastMsg: Date.now()
                });
                console.log('yo')
                Meteor.call('addToGestionnaireHistory',
                'messenger',
                msgId,
                'sentMessage',
                data.condoId,
                data.details,
                Meteor.userId()
              );
              _.each(data.files, (f) => {
                MessengerFiles.update({_id: f},
                  {$set:
                    {
                      'meta.msgId': msgId,
                      'meta.condoId': data.condoId
                    }
                  });
                });
                let users = [];
                let date = new Date();
                let userId = this.userId;
                date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
                let feedDate = Date.now(); //to avoid bug in badges//
                let type = IncidentType.findOne(data.typeMessage)
                let declarationDetail = DeclarationDetails.findOne(data.details)
                let userIds = data.destinataires
                for (const userId of userIds) {
                  if (userId !== Meteor.userId()) {
                    let replyLink = "";
                    let lang = Meteor.users.findOne({_id: userId}).profile.lang || "fr";
                    if (Meteor.call('isGestionnaireOfCondo', userId, data.condoId) === true) {
                      replyLink = Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${msgId}`, data.condoId);
                    } else if (Meteor.call('isResidentOfCondo', userId, data.condoId) === true) {
                      replyLink = Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/messenger/manager/${msgId}`, data.condoId);
                    }
                    if (checkNotifToResident("Gestionnaire", userId, data.condoId) == true) {
                      let user = Meteor.users.findOne({ _id: userId })
                      let lang = user.profile.lang || "fr";
                      let translation = new Email(lang)
                      Meteor.call('scheduleAnEmail', {
                        templateName: 'new-message-' + lang,
                        to: user.emails[0].address,
                        condoId: data.condoId,
                        subject: translation.email['mess_from_admin'],
                        data: {
                          title: translation.email['new_unread_mess'],
                          type: type ? type["value-" + lang] : null,
                          details: declarationDetail ? declarationDetail["value-" + lang] : null,
                          message: data.message.length > 150 ? data.message.substr(0, 150) + "..." : data.message,
                          module: "messagerie",
                          condoId: data.condoId,
                          notifUrl: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/profile/notifications`, data.condoId),
                          from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                          setReplyLink: replyLink
                        },
                        dueTime: date,
                        condition: {
                          name: "checkMessageBeforeSend",
                          data: { msgId: msgId, userId: userId, condoId: data.condoId, createdAt: Date.now() }
                        }
                      });
                    }
                    users.push({ "userId": userId });
                  }
                  else {
                    users.push({ "userId": userId, "lastVisit": Date.now(), "createdBy": true });
                  }
                }
                Messages.update(msgId, {$set: {userUpdateView: [this.userId], users: users, lastMsgString: data.message, lastMsgUserId: this.userId}});
                let msgFeedId = MessagesFeed.insert({ messageId: msgId, date: feedDate, userId: this.userId, text: data.message, files: data.files });
                let pushNotificationUserIds = _.filter(_.map(users, (u) => {
                  if (u.userId !== Meteor.userId())
                  return u.userId
                }), (userId) => userId !== undefined)
                OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
                  type: 'messenger',
                  key: 'new_message',
                  dataId: msgFeedId,
                  condoId: data.condoId,
                  data: {
                    target: 'manager'
                  }
                })
                return msgId;
              },
              /*  CREATE A NEW MESSAGE FROM RESIDENT TO GESTIONNAIRE, CREATE REMINDER SENT TO GESTIONNAIRE IN CHARGE OF THIS TYPE OF REQUEST
              data                  (Object) CONTAINS DATA FOR THE CREATION
              {
                type              (String) "Incident" || "Facturation / Comptabilité" || "Autre"
                details           (String) DETAILS ABOUT THE REQUEST
                target            (String) "Gestionnaire" (FROM RESIDENT TO GESTIONNAIRE)
                priority          (Boolean) INDICATE IF THE REQUEST IS URGENT
                message           (String) TEXT OF MESSAGE
                condoId           (String) ID OF CONDO RELATED TO THE CONVERSATION
                sujet             (String) MAIN TITLE OF THE REQUEST
                statut            (String) "Locataire" || "Propriétaire" (STATUS OF THE USER IN THIS CONDO)
                lot               (String) INFO ABOUT THE BUILDING WHERE THE CREATOR LIVES
                files             (Table of String) FILES ATTACHED TO THE MESSAGE
                reminder          (Bool) gestionnaire reminder true / false
              }
              */
              addMessageGestionnaire: function(data) {
                let emailList = [];
                let condoContact = CondoContact.findOne({condoId: data.condoId});

                if (condoContact) {
                  let listOfIds = _.find(condoContact.contactSet, function(list) {
                    return list.declarationDetailId == data.details && list.messageTypeId == data.type;
                  })
                  if (listOfIds)
                  emailList = _.without(listOfIds.userIds, null, undefined);
                }

                if (!emailList || emailList.length === 0)
                emailList = [condoContact.defaultContact.userId];

                let users = _.map(emailList, (e) => {return {userId: e}});
                let feedDate = Date.now(); //to avoid bug in badges//
                users.push({userId: this.userId, lastVisit: feedDate + 10, "createdBy": true}); //+10 to avoid bug in badges//
                const userSet = new Set(emailList);
                userSet.add(this.userId);

                let msgId = Messages.insert({
                  // NEW MESSAGE DATA
                  createdBy: this.userId,
                  participants: Array.from(userSet),
                  userUpdateView: [this.userId],
                  destination: 'manager',
                  // ////////////////
                  date: Date.now(),
                  target: "manager",
                  condoId: data.condoId,
                  urgent: data.priority,
                  type: data.type,
                  details: data.details,
                  sujet: data.sujet,
                  origin: this.userId,
                  resp: [],
                  users: users,
                  lastMsgString: data.message,
                  lastMsg: Date.now(),
                  lastMsgUserId: this.userId
                });
                var userId = this.userId;
                _.each(data.files, (f) => {
                  MessengerFiles.update({_id: f},
                    {$set:
                      {
                        'meta.msgId': msgId
                      }
                    });
                  });
                  let type = IncidentType.findOne(data.type)
                  let declarationDetail = DeclarationDetails.findOne(data.details)
                  let msgFeedId = MessagesFeed.insert({ messageId: msgId, date: feedDate, userId: this.userId, text: data.message, files: data.files });
                  for (const userId of emailList) {
                    let user = Meteor.users.findOne(userId)
                    let lang = user.profile.lang || 'fr'
                    var translation = new Email(lang);
                    Meteor.call('scheduleAnEmail', {
                      templateName: 'new-message-' + lang,
                      to: user.emails[0].address,
                      condoId: data.condoId,
                      subject: translation.email['new_mess_admin'],
                      data: {
                        title: translation.email['new_unread_mess'],
                        type: type ? type["value-" + lang] : null,
                        details: declarationDetail ? declarationDetail["value-" + lang] : null,
                        message: data.message.length > 150 ? data.message.substr(0, 150) + "..." : data.message,
                        module: "messagerie",
                        condoId: data.condoId,
                        notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, data.condoId),
                        from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                        setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${msgId}`, data.condoId)
                      },
                      dueTime: new Date(Date.now() + Meteor.global.email.notification_delay_minutes * 60 * 1000),
                      condition: {
                        name: "checkMessageBeforeSend",
                        data: {
                          msgId: msgId,
                          userId: userId,
                          condoId: data.condoId,
                          createdAt: Date.now()
                        }
                      }
                    });

                  }
                  OneSignal.MbNotification.sendToUserIds(emailList, {
                    type: 'messenger',
                    key: 'new_message_other',
                    dataId: msgFeedId,
                    condoId: data.condoId,
                    data: {
                      target: 'manager'
                    }
                  })
                  let message = data.sujet.length > 150 ? data.sujet.substr(0, 150) + "..." : data.sujet;
                  users.forEach(function(user){
                    if (user.userId != Meteor.userId()) {
                      if (gUser = Meteor.users.findOne(user.userId)) {
                        let reminder = null;
                        let lang = gUser.profile.lang || "fr";
                        let declarationDetail = DeclarationDetails.findOne(data.sujet);
                        if (declarationDetail)
                        message = declarationDetail["value-" + lang];
                        if (Meteor.userHasRight("messenger", "seeAllMsgEnterprise", data.condoId, user.userId))
                        {
                          if (checkNotifToGestionnaire(_.clone(gUser._id), data.condoId) == true) {
                            Meteor.call('sendEmailSync',
                            gUser.emails[0].address,
                            data.condoId,
                            'Nouveau message : "Messenger"',
                            "new-message-fr",
                            {
                              title: 'Vous avez un nouveau message de ',
                              message: message,
                              module: "messagerie",
                              condoId: data.condoId,
                              notifUrl: Meteor.absoluteUrl(`${lang}/gestionnaire/profile`, data.condoId),
                              from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                              setReplyLink:  Meteor.absoluteUrl(`${lang}/gestionnaire/messagedetails/${msgId}`, data.condoId)
                            }
                          );
                        }
                      }
                    }
                  }
                });
                if (data.reminder) {
                  let usersEmailList = users &&  users.map((u) => {
                    if (u.userId && u.userId !== Meteor.userId()) {
                      return u.userId
                    }
                  })
                  usersEmailList = _.without(usersEmailList, undefined);
                  let remindHours = getReminderValue(data.condoId, data.priority);
                  reminder = Meteor.call('programEmailReminder', {
                    to: usersEmailList,
                    condoId: data.condoId,
                    emailTemplate: "reminder-message-fr",
                    emailData: {
                      msgId: msgId,
                      message: message,
                      module: "messagerie",
                      condoId: data.condoId,
                      from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                      setReplyLink: Meteor.absoluteUrl(`fr/gestionnaire/messagedetails/${msgId}`, data.condoId)
                    },
                    remindHours: remindHours,
                    subject: "RAPPEL - Nouveau message : \"Messenger\""
                  });
                  Messages.update(msgId, { $set: { reminder: reminder } });
                }
                return msgId;
              },

              newMessageIncidentFromMessenger: function(conversationId, message, files, sendNotif) {
                let conversation = Messages.findOne(conversationId);
                if (conversation) {
                  let incident = Incidents.findOne(conversation.incidentId);
                  if (incident) {
                    let details = incident.info.type;
                    let sujet = incident.info.title;
                    let data = {
                      conversationId: conversationId,
                      details: details,
                      target: "manager",
                      urgent: incident.info.priority,
                      message: message,
                      condoId: incident.condoId,
                      sujet: sujet,
                      statut: "Locataire",
                      lot: "nc",
                      files: files,
                      incidentId: incident._id,
                      sendNotif: sendNotif
                    };
                    Meteor.call('contactGestionnaireFromIncidents', data, function(error) {
                      if (!error) {
                        return;
                      }
                      else {
                        console.log('error', error)
                        throw new Meteor.Error(400, "Error");
                      }
                    });
                  }
                  else
                  throw new Meteor.Error(404, "Unknown incident");
                }
                else
                throw new Meteor.Error(404, "Unknown conversation");
              },
              /*  CREATE A NEW CONVERSATION / ADD A MESSAGE IN EXISTING CONVERSATION RELATED TO SPECIFIC INCIDENT, CREATE REMINDER SENT TO GESTIONNAIRE IN CHARGE OF THE INCIDENT
              data                  (Object) CONTAINS DATA FOR THE CREATION
              {
                type              (String) "Incident" || "Facturation / Comptabilité" || "Autre"
                details           (String) DETAILS ABOUT THE REQUEST
                target            (String) "Gestionnaire" (FROM RESIDENT TO GESTIONNAIRE)
                urgent            (Boolean) INDICATE IF THE REQUEST IS URGENT
                message           (String) TEXT OF MESSAGE
                condoId           (String) ID OF CONDO RELATED TO THE CONVERSATION
                sujet             (String) MAIN TITLE OF THE REQUEST
                statut            (String) "Locataire" || "Propriétaire" (STATUS OF THE USER IN THIS CONDO)
                lot               (String) INFO ABOUT THE BUILDING WHERE THE CREATOR LIVES
                files             (Table of String) FILES ATTACHED TO THE MESSAGE
                incidentId        (String) ID OF THE INCIDENT RELATED TO THE MESSAGE
                sendNotif         (String) should create a reminder :)
              }
              */
              contactGestionnaireFromIncidents: function(data) {
                let gestionnaire = Enterprises.findOne({condos: data.condoId}, {fields: {users: true}});

                const condoContact = CondoContact.findOne({condoId: data.condoId});
                let incidentDetailId = DeclarationDetails.findOne({key: "incident"});
                // console.log('condoContact', condoContact)
                // console.log('incidentDetailId ', incidentDetailId )
                // console.log('data.type', data)
                const incident = Incidents.findOne({ _id: data.incidentId }, { fields: { info: true } })
                let incidentCondoContact = _.find(condoContact.contactSet, function(contact) {
                  return contact.declarationDetailId == incidentDetailId._id && contact.messageTypeId == incident.info.incidentTypeId;
                })

                let emailList = [condoContact.defaultContact.userId];
                if (incidentCondoContact && incidentCondoContact.userIds && incidentCondoContact.userIds.length > 0) {
                  emailList = _.without(incidentCondoContact.userIds, null, undefined);
                }
                let users = _.map(emailList, (e) => {return {userId: e}});
                let actualDate = Date.now();
                users.push({userId: this.userId, lastVisit: actualDate, "createdBy": true});
                if (gestionnaire) {
                  let msgId = undefined;
                  if (data.conversationId !== undefined)
                    msgId = Messages.findOne({_id: data.conversationId});
                  if (msgId === undefined)
                    msgId = Messages.findOne({incidentId: data.incidentId, 'users.userId': Meteor.userId()});
                  if (msgId === undefined) {
                    let convOrigin = Messages.findOne({ incidentId: data.incidentId })
                    let newConv = {}
                    if (convOrigin !== undefined) {
                      newConv = {
                        target: convOrigin.target,
                        destinataires: convOrigin.destinataires,
                        condoId: data.condoId,
                        message: data.message,
                        sujet: convOrigin.sujet,
                        type: convOrigin.type,
                        details: convOrigin.details,
                        incidentId: convOrigin.incidentId,
                        files: data.files
                      }
                    } else {
                      let incident = Incidents.findOne(data.incidentId)
                      newConv = {
                        target: data.target,
                        destinataires: ["Gestionnaire", incident.declarer.userId],
                        condoId: data.condoId,
                        message: data.message,
                        sujet: data.sujet,
                        type: incident.info.incidentTypeId,
                        details: data.details,
                        incidentId: data.incidentId,
                        files: data.files
                      }
                    }
                    msgId = Meteor.call('addIncidentToMessageList', newConv, true)
                  } else {
                    let message = msgId;
                    msgId = msgId._id;
                    _.each(message.users, function(u) {
                      if (u.userId == Meteor.userId()) {
                        u.lastVisit = actualDate;
                      }
                    })
                    Messages.update(msgId, {
                      $set: {
                        lastMsg: actualDate,
                        users:   message.users,
                        userUpdateView: [this.userId],
                        lastMsgString: data.message,
                        lastMsgUserId: this.userId,
                        isArchived: false
                      }
                    })

                    const messageFeedData = {
                      messageId: msgId,
                      date: actualDate,
                      userId: this.userId,
                      text: data.message,
                      files: data.files
                    }

                    let msgFeedId = MessagesFeed.insert(messageFeedData);
                    OneSignal.MbNotification.sendToUserIds(emailList, {
                      type: 'messenger',
                      key: 'new_message_incident',
                      dataId: msgFeedId,
                      condoId: message.condoId,
                      data: {
                        target: message.target
                      }
                    })
                    let usersEmailList = []
                    for (const userId of emailList) {
                      let user = Meteor.users.findOne(userId);
                      let lang = user.profile.lang || 'fr'
                      var translation = new Email(lang);
                      let incident = Incidents.findOne(data.incidentId)
                      if (Meteor.userHasRight("incident", "see", message.condoId, user._id)) {
                        if (Meteor.call('checkNotifGestionnaire', user._id, message.condoId, 'incidents')) {
                          usersEmailList.push(userId)
                          Meteor.call('scheduleAnEmail', {
                            templateName: 'incidentMessageToManager-' + lang,
                            to: user.emails[0].address,
                            condoId: message.condoId,
                            subject: translation.email['new_mess_incident'],
                            data: {
                              condo: Condos.findOne({ _id: message.condoId }),
                              incident: incident,
                              message: data.message,
                              setReplyLink: Meteor.absoluteUrl(`${lang}/gestionnaire/incidents/${incident._id}`, message.condoId)
                            },
                            dueTime: new Date(Date.now() + Meteor.global.email.notification_delay_minutes * 60 * 1000),
                            condition: {
                              name: "checkMessageBeforeSend",
                              data: { msgId: msgId, userId: userId, condoId: message.condoId, createdAt: Date.now() }
                            }
                          });
                        }
                      }
                    }
                    if (message.reminder) {
                      Meteor.call('removeEmailReminder', message.reminder)
                    }
                    if (data.sendNotif && usersEmailList.length > 0) {
                      let remindHours = getReminderValue(data.condoId, data.urgent);
                      reminder = Meteor.call('programEmailReminder', {
                        to: usersEmailList,
                        condoId: data.condoId,
                        emailTemplate: "reminder-message-fr",
                        emailData: {
                          msgId: msgId,
                          message: message,
                          module: "messagerie",
                          condoId: data.condoId,
                          from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                          setReplyLink: Meteor.absoluteUrl(`fr/gestionnaire/messagedetails/${msgId}`, data.condoId)
                        },
                        remindHours: remindHours,
                        subject: "RAPPEL - Nouveau message : \"Messenger\""
                      });
                      Messages.update(msgId, { $set: { reminder: reminder } });
                    }
                  }
                  data.files.forEach((file) => {
                    MessengerFiles.update(file, {
                      $set: {
                        'meta.msgId': msgId,
                        'meta.incidentId': data.incidentId
                      }
                    })
                  })

                  let user = Meteor.users.findOne(this.userId);
                  let newHistory = {
                    date: actualDate - 10,
                    userId: this.userId,
                    name: user.profile.firstname + " " + user.profile.lastname,
                    details: "",
                    title: 12,
                    files: data.files,
                    msgId: msgId,
                    message: data.message,
                    share: {
                      "all": {"state": false, "comment": "", "files": []},
                      "syndic": {"state": false, "comment": "", "files": []},
                      "declarer": {"state": false, "comment": "", "files": []},
                      "intern": {"state": false, "comment": "", "files": []},
                      "custom": []
                    }
                  };
                  let incident = Incidents.findOne(data.incidentId);
                  let d = new Date();
                  incident.history.push(newHistory);
                  incident.history.sort(function(a,b) {return (a.date < b.date) ? 1 : ((b.date < a.date) ? -1 : 0);} );
                  Incidents.update(data.incidentId, {
                    $set: {
                      "lastUpdate": d,
                      "history": incident.history
                    }
                  });
                  return msgId
                }
                return null
              },
              // UPDATE THE DATABASE WITH MY LAST VISIT ON THE MESSAGE, AND CANCEL EMAIL IF NEEDED
              // msgId                 (String) ID OF THE MESSAGE
              updateLastVisit: function(msgId) {
                let t = Date.now();
                // FIXME: Delete/update users array
                Messages.update(
                  {_id: msgId, "users.userId": this.userId},
                  {
                    $set: {"users.$.lastVisit": t}
                  },
                );
                Messages.update(
                  {_id: msgId},
                  {
                    $addToSet: {
                      'views': this.userId,
                      userUpdateView: this.userId,
                    }
                  },
                );
                let msg = Messages.findOne({_id: msgId, "users.userId": this.userId});
                if (msg) {
                  for (let i = 0; i < msg.users.length; i++) {
                    if (msg.users[i].userId === this.userId) {
                      if (msg.users[i].emailId) {
                        Meteor.call('cancelAnEmail', msg.users[i].emailId);
                        Messages.update(
                          {_id: msgId, "users.userId": this.userId},
                          {$unset: {"users.$.emailId": 1}}
                        );
                      }
                    }
                  }
                }
              },
              'module-messenger-follow-toggle': function (condoId) {
                if (!Match.test(condoId, String))
                throw new Meteor.Error(401, "Invalid parameters \'condoId\' module-messenger-follow-toggle");
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated");
                let resident = Residents.findOne({ userId: Meteor.userId() });
                if (resident) {
                  let condo = _.find(resident.condos, (condo) => { return condo.condoId === condoId });
                  if (!condo)
                  throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
                  Residents.update({
                    userId: Meteor.userId(),
                    'condos.condoId': condoId
                  },
                  {
                    $set:
                    {
                      'condos.$.notifications.msg_resident': !condo.notifications.msg_resident,
                      'condos.$.notifications.msg_gardien': !condo.notifications.msg_gardien,
                      'condos.$.notifications.msg_conseil': !condo.notifications.msg_conseil,
                      'condos.$.notifications.msg_gestion': !condo.notifications.msg_gestion
                    }
                  });
                }
                else {
                  let gestionnaire = Enterprises.findOne({ "users.userId": Meteor.userId() });
                  let user = gestionnaire.users.find((u) => { return u.userId == Meteor.userId() });
                  let condo = user.condosInCharge.find((c) => { return c.condoId == condoId });
                  if (condo.notifications.messenger == undefined) {
                    condo.notifications.messenger = true
                  } else {
                    condo.notifications.messenger = !condo.notifications.messenger;
                  }
                  _.each(user.condosInCharge, function (elem) {
                    if (elem.condoId === condoId) {
                      elem = condo;
                    }
                  });
                  Enterprises.update({
                    '_id': gestionnaire._id,
                    'users.userId': Meteor.userId()
                  },
                  {
                    $set: { 'users.$.condosInCharge': user.condosInCharge },
                  });
                }
              },
              'module-messenger-one-follow-toggle': function (condoId, key) {
                if (!Match.test(condoId, String))
                throw new Meteor.Error(401, "Invalid parameters \'condoId\' module-messenger-follow-toggle");
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated");
                let resident = Residents.findOne({ userId: Meteor.userId() });
                if (resident) {
                  let condo = _.find(resident.condos, (condo) => { return condo.condoId === condoId });
                  if (!condo)
                  throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
                  let field = null
                  if (!!key && (key === 'msg_resident' || key === 'msg_gardien' || key === 'msg_conseil' || key === 'msg_gestion')) {
                    field = 'condos.$.notifications.' + key
                  } else {
                    throw new Meteor.Error(601, "Invalid key", "Cannot find this key in messenger notification");
                  }
                  let toto = Residents.update({
                    userId: Meteor.userId(),
                    'condos.condoId': condoId
                  },
                  {
                    $set:
                    {
                      [field]: !condo.notifications[key],
                    }
                  });
                }
                else {
                  throw new Meteor.Error(601, "Invalid Role", "This function is only for occupant");
                }
              },
              // UPDATE THE DATABASE WITH MY LAST VISIT ON ALL MESSAGES RELATED TO SPECIFIC CONDO
              // condoId               (String) ID OF THE CONDO
              updateVisitModuleMessagerie: function(condoId) {
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated");
                Messages.update({condoId: condoId, "users.userId": this.userId},
                {$set: {"users.$.lastVisit": Date.now()}});
              },

              // CHECK IF ROOM ALREADY EXIST
              // users			(Array) array of sting of user id participant
              checkResidentRoomExist: function (users, condoId) {
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated");
                let exist = false
                users.push(Meteor.userId())
                const defaultRole = DefaultRoles.findOne({ name: 'Occupant' }, { fields: { _id: true } })
                console.log({
                  condoId: condoId,
                  "users.userId": { $all: users },
                  users: { $size: users.length },
                  target: defaultRole._id
                })
                let msg = Messages.findOne({
                  condoId: condoId,
                  "users.userId": { $all: users },
                  users: { $size: users.length },
                  target: defaultRole._id
                })
                if (!!msg === true) {
                  exist = msg._id
                }

                return exist;
              },
              checkManagerRoomExist: function (users, condoId) {
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated")
                let exist = false
                users.push(Meteor.userId())
                let msg = Messages.findOne({
                  condoId: condoId,
                  "users.userId": { $all : users},
                  users: {$size: users.length},
                  target: 'manager',
                  global: true
                })
                if (!!msg === true) {
                  exist = msg._id;
                }

                return exist;
              },
              setConversationPicture: function (conversationId, fileId) {
                check(conversationId, String);
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated")
                const checkMessage = Messages.findOne({ _id: conversationId, "users.userId": this.userId })
                if (!!checkMessage === false)
                throw new Meteor.Error(404, "Conversation not found or you can't do that")
                Messages.update({
                  _id: conversationId,
                  "users.userId": this.userId
                }, {
                  $set: {
                    picture: fileId
                  }
                })
              },
              setConversationName: function (conversationId, name) {
                check(conversationId, String);
                if (!Meteor.userId())
                throw new Meteor.Error(300, "Not Authenticated")
                const checkMessage = Messages.findOne({ _id: conversationId, "users.userId": this.userId })
                if (!!checkMessage === false)
                throw new Meteor.Error(404, "Conversation not found or you can't do that")
                Messages.update({
                  _id: conversationId,
                  "users.userId": this.userId
                }, {
                  $set: {
                    name: name
                  }
                })
              },
              addMessageFromManagerMessengerGlobal: function (data) {
                console.log('addMessageFromManagerMessengerGlobal')
                check(data, Match.ObjectIncluding({
                  condoId: String,
                  file: Match.Maybe([String]),
                  users: [String],
                  message: Match.Maybe(String)
                }))
                if (!Meteor.userId()) {
                  throw new Meteor.Error(300, "Not Authenticated")
                }

                const userSet = new Set(data.users);
                userSet.add(this.userId);
                let msgId = Messages.insert({
                  // NEW MESSAGE DATA
                  createdBy: this.userId,
                  participants: Array.from(userSet),
                  userUpdateView: [this.userId],
                  destination: 'global',
                  // ////////////////
                  date: Date.now(),
                  condoId: data.condoId,
                  users: [],
                  target: 'manager',
                  global: true,
                  origin: Meteor.userId(),
                  lastMsg: Date.now()
                });
                let users = [];
                let date = new Date();
                date.setMinutes(date.getMinutes() + Meteor.global.email.notification_delay_minutes);
                _.each(data.files, (f) => {
                  MessengerFiles.update({ _id: f },
                    {
                      $set:
                      {
                        'meta.condoId': data.condoId,
                        'meta.msgId': msgId
                      }
                    });
                  });
                  let feedDate = Date.now(); //to avoid bug in badges//
                  data.users.forEach((userId) => {
                    if (userId !== Meteor.userId()) {
                      let currentUser = Meteor.users.findOne({ _id: userId })
                      let lang = currentUser.profile.lang || "fr";
                      var translation = new Email(lang);
                      let replyLink = "";
                      try {
                        let roleInBuilding = UsersRights.findOne({userId: userId, condoId: condoId})
                        if (!!roleInBuilding === true) {
                          let isManagerRole = DefaultRoles.findOne({ _id: roleInBuilding.defaultRoleId, for: 'manager' })
                          if (!!isManagerRole === true) {
                            replyLink = Meteor.absoluteUrl(`${lang}/gestionnaire/messenger/perso/${msgId}`, condoId);
                          } else {
                            replyLink = Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/messenger/manager/${msgId}`, condoId)
                          }
                        } else {
                          return
                        }
                      }
                      catch (e) { }
                      // if (checkNotifToResident(defaultRole.name, id, data.condoId) == true) {
                      // Meteor.call('scheduleAnEmail', {
                      //   templateName: 'new-message-' + lang,
                      //   to: Meteor.users.findOne({ _id: id }).emails[0].address,
                      //   condoId: data.condoId,
                      //   subject: translation.email['new_mess_mess'],
                      //   data: {
                      //     title: translation.email['new_unread_mess'],
                      //     message: data.message.length > 150 ? data.message.substr(0, 150) + "..." : data.message,
                      //     module: "messagerie",
                      //     condoId: data.condoId,
                      //     notifUrl: Meteor.absoluteUrl(`${lang}/resident/${data.condoId}/profile/notifications`, data.condoId),
                      //     from: Meteor.user().profile.firstname + ' ' + Meteor.user().profile.lastname,
                      //     setReplyLink: replyLink
                      //   },
                      //   dueTime: date,
                      //   condition: {
                      //     name: "checkMessageBeforeSend",
                      //     data: { msgId: msgId, userId: id, condoId: data.condoId, createdAt: Date.now() }
                      //   }
                      // });
                      // }
                      users.push({ "userId": userId });
                    }
                    else
                    users.push({ "userId": userId, "lastVisit": Date.now(), "createdBy": true });
                  });
                  Messages.update(msgId, { $set: {userUpdateView:[this.userId],  users: users, lastMsgString: data.message, lastMsgUserId: this.userId } });
                  let msgFeedId = MessagesFeed.insert({ messageId: msgId, date: feedDate, userId: this.userId, text: data.message, files: data.files });
                  let pushNotificationUserIds = _.filter(_.map(users, (u) => {
                    if (u.userId !== Meteor.userId())
                    return u.userId
                  }), (userId) => userId !== undefined)
                  OneSignal.MbNotification.sendToUserIds(pushNotificationUserIds, {
                    type: 'messenger',
                    key: 'new_message',
                    dataId: msgFeedId,
                    condoId: data.condoId,
                    data: {
                      target: 'manager'
                    }
                  })
                  return msgId;
                },
                ServerScriptREVERT___MessengerChangeTargetWithDefaultRoleId: function () {
                  let defaultRolesOccupant = DefaultRoles.findOne({ 'name': 'Occupant' })
                  let defaultRolesConseilSyndical = DefaultRoles.findOne({ 'name': 'Conseil Syndical' })
                  let defaultRolesGardien = DefaultRoles.findOne({ 'name': 'Gardien' })
                  if (!!defaultRolesOccupant && !!defaultRolesOccupant._id) {
                    Messages.update({
                      target: defaultRolesOccupant._id,
                    }, {
                      $set: { target: 'Resident' }
                    }, { multi: true })
                  }
                  if (!!defaultRolesConseilSyndical && !!defaultRolesConseilSyndical._id) {
                    Messages.update({
                      target: defaultRolesConseilSyndical._id
                    }, {
                      $set: { target: 'Conseil Syndical' }
                    }, { multi: true })
                  }
                  if (!!defaultRolesGardien && !!defaultRolesGardien._id) {
                    Messages.update({
                      target: defaultRolesGardien._id
                    }, {
                      $set: { target: 'Gardien' }
                    }, { multi: true })
                  }
                  Messages.update({
                    $and: [
                      { target: 'manager' },
                      { incidentId: { $exists: true } }
                    ]
                  }, {
                    $set: { target: 'Incident' }
                  }, { multi: true })
                  Messages.update({
                    $and: [
                      { target: 'manager' },
                      {
                        $and: [
                          { type: { $exists: true } },
                          { details: { $exists: true } },
                        ]
                      }
                    ]
                  }, {
                    $set: { target: 'Gestionnaire' }
                  }, { multi: true })
                },
                ServerScriptMessengerChangeTargetWithDefaultRoleId: function () {
                  let defaultRolesOccupant = DefaultRoles.findOne({ 'name': 'Occupant' })
                  let defaultRolesConseilSyndical = DefaultRoles.findOne({ 'name': 'Conseil Syndical' })
                  let defaultRolesGardien = DefaultRoles.findOne({ 'name': 'Gardien' })
                  if (!!defaultRolesOccupant && !!defaultRolesOccupant._id) {
                    Messages.update({
                      target: 'Resident'
                    }, {
                      $set: { target: defaultRolesOccupant._id }
                    }, {multi: true})
                  }
                  if (!!defaultRolesConseilSyndical && !!defaultRolesConseilSyndical._id) {
                    Messages.update({
                      target: 'Conseil Syndical'
                    }, {
                      $set: { target: defaultRolesConseilSyndical._id }
                    }, {multi: true})
                  }
                  if (!!defaultRolesGardien && !!defaultRolesGardien._id) {
                    Messages.update({
                      target: 'Gardien'
                    }, {
                      $set: { target: defaultRolesGardien._id }
                    }, {multi: true})
                  }
                  Messages.update({
                    $or: [
                      { target: 'Gestionnaire' },
                      { target: 'Incident' },
                      { incidentId: { $exists: true } },
                      { $and: [
                        { type: { $exists: true } },
                        { details: { $exists: true } },
                      ]
                    }
                  ]
                }, {
                  $set: { target: 'manager' }
                }, {multi: true})
              }
            });
          });

          function isManager(userId, condoId) {
            const user = Meteor.users.findOne(userId, { fields: { "identities": true } })
            const enterprise = Enterprises.findOne({"condos": condoId})
            return user &&
              user.identities &&
              user.identities.gestionnaireId &&
              enterprise.users.filter( u => u.userId === userId ).length > 0
          }

          function checkNotifToResident(target, residentId, condoId) {
            let notif = false
            if (target == "Occupant" && Meteor.call('isResidentOfCondo', residentId, condoId)) {
              notif = checkNotifFromResidentToResident(residentId, condoId);
            } else if (target == "Conseil Syndical" && Meteor.call('isInCsOfCondo', residentId, condoId)) {
              notif = checkNotifFromCsToResident(residentId, condoId);
            } else if (target == "Gardien" && Meteor.call('isKeeperOfCondo', residentId, condoId)) {
              notif = checkNotifFromKeeperToResident(residentId, condoId);
            } else if (target == "Gestionnaire" && isManager(residentId, condoId)) {
              notif = Meteor.call('checkNotifGestionnaire', residentId, condoId, 'messagerie')
            } else if (target == "Gestionnaire" && Meteor.call('isResidentOfCondo', residentId, condoId)) {
              notif = checkNotifFromGestionnaireToResident(residentId, condoId);
            }
            return notif
          }

          function checkNotifFromResidentToResident(residentId, condoId) {
            let resident = Residents.findOne({userId: residentId});
            if (resident) {
              let resCondo = _.find(resident.condos, (condo) => {return condo.condoId == condoId});
              if (resCondo) {
                if (resCondo.notifications.msg_resident != false && residentId != Meteor.userId())
                return true;
              }
            }
            return false;
          }

          function checkNotifFromCsToResident(residentId, condoId) {
            let resident = Residents.findOne({userId: residentId});
            if (resident) {
              let resCondo = _.find(resident.condos, (condo) => {return condo.condoId == condoId});
              if (resCondo) {
                if (resCondo.notifications.msg_conseil != false && residentId != Meteor.userId())
                return true;
              }
            }
            return false;
          }

          function checkNotifFromKeeperToResident(residentId, condoId) {
            let resident = Residents.findOne({userId: residentId});
            if (resident) {
              let resCondo = _.find(resident.condos, (condo) => {return condo.condoId == condoId});
              if (resCondo) {
                if (resCondo.notifications.msg_gardien != false && residentId != Meteor.userId())
                return true;
              }
            }
            return false;
          }

          function checkNotifFromGestionnaireToResident(residentId, condoId) {
            let resident = Residents.findOne({userId: residentId});
            if (resident) {
              let resCondo = _.find(resident.condos, (condo) => {return condo.condoId == condoId});
              if (resCondo) {
                if (resCondo.notifications.msg_gestion != false && residentId != Meteor.userId())
                return true;
              }
            }
            return false;
          }

          function checkNotifToGestionnaire(residentId, condoId) {
            let gestionnaire = Enterprises.findOne({"condos": condoId});
            if (gestionnaire) {
              let gUser = gestionnaire.users.find((u) => {return u.userId == residentId});
              if (gUser) {
                let condo = gUser.condosInCharge.find((condo)=>{return condo.condoId == condoId});
                if (condo) {
                  if (condo.notifications.messenger != false && residentId != Meteor.userId())
                  return true;
                }
              }
            }
            return false;
          }
