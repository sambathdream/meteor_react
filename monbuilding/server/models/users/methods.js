import { Accounts } from 'meteor/accounts-base';
import { check, Match } from 'meteor/check';
import { HTTP } from 'meteor/http';

import { Email } from '/common/lang/lang';

function getBuildingsWithCondo(condoId) {
	const condo = Condos.findOne(condoId);
	return Buildings.find({
		_id: {
			$in: condo.buildings,
		}
	});
}

function addUserToCondo(user, condoId) {
	const buildings = getBuildingsWithCondo(condoId);
	const residentId = user.identities.residentId;

	Residents.update({
		_id: residentId,
	}, {
		$addToSet: {
			buildings: {
				$each: buildings.map(obj => ({buildingId: obj._id})),
				invited: Date.now()
			}
		}
	});
}

function sendCondoInvitationEmailToExistingUser(email, condoId, lang = 'fr') {
  const condo = Condos.findOne(condoId);
	Meteor.call(
		'sendEmailSync',
		email,
		lang == "en" ? 'You are invited to condo ' + condo.name : 'Vous avez été invité à rejoinndre l\'immeuble ' + condo.name,
		'invite-existing-user-to-condo-' + lang,
		{
			condoName: condo.name,
		}
	);
}

function sendCondoInvitationEmailToNewUser(email, condoId) {
	const user = Accounts.findUserByEmail(email);
	const condo = Condos.findOne(condoId);
  const gestionnaire = Enterprises.findOne({"condos": condoId});
  const lang = Meteor.users.findOne({_id: user._id}).profile.lang || "fr"

  console.log('email', email)
  console.log('condoId', condoId)
  console.log('user', user)
  console.log('${lang}/services/set-password/${user._id}, condoId', `${lang}/services/set-password/${user._id}`, condoId)
  const passwordLink = Meteor.absoluteUrl(`${lang}/services/set-password/${user._id}`, condoId)
  console.log('passwordLink', passwordLink)
	Meteor.call(
		'sendEmailSync',
		email,
		condoId,
		'Vous avez été invité(e) à rejoindre l\'espace de votre immeuble',
		'invite-new-user-to-condo-' + lang,
		{
			condoName: condo.getName(),
			gestionnaireName: gestionnaire.name,
      setPasswordLink: passwordLink,
		}
	);
}

function inviteExistingUserToCondo(user, condoId) {
	addUserToCondo(user, condoId);
	sendCondoInvitationEmailToExistingUser(user.emails[0].address, condoId, user.profile.lang);
}

function inviteNewUserToCondo(email, civilite, firstname, lastname, condoId, from, floor, number) {
	setUpNewUserWithResident(email, civilite, firstname, lastname, condoId, from, floor, number);
	sendCondoInvitationEmailToNewUser(email, condoId);
}

function inviteNewUserToResidence(email, civilite, firstname, lastname, school, matiere, condoId, from, floor, number, company) {
	setUpNewStudentWithResident(email, civilite, firstname, lastname, school, matiere, condoId, from, floor, number, company);
	sendCondoInvitationEmailToNewUser(email, condoId);
}

function setUpNewUserWithResident(email, civilite, firstname, lastname, condoId, from, floor, number) {
	Accounts.createUser({email});

	const user = Accounts.findUserByEmail(email);

	let roleId = DefaultRoles.findOne({ name: "Occupant", for: "occupant" })._id;
	const residentId = Residents.insert({
		userId: user._id,
		condos: [],
		registeredFrom: (from ? from : "web"),
		pendings: [{
				condoId: condoId,
				roleId: roleId,
				invited: new Date(),
				invitByGestionnaire: true,
				notifications: {
					"actualite" : true,
					"incident" : true,
					"forum_forum" : true,
					"forum_syndic" : true,
					"forum_reco" : true,
					"forum_boite" : true,
					"classifieds" : true,
					"resa_new" : true,
					"resa_rappel" : true,
					"msg_resident" : true,
					"msg_conseil" : true,
					"msg_gardien" : true,
					"msg_gestion" : true,
					"edl" : true
				},
				preferences: {
					messagerie: true
				},
				buildings: [
					{
						buildingId: "",
						type: "tenant"
					}
				]
			}],
	});

	Meteor.users.update({
		_id: user._id,
	}, {
		$set: {
			profile: {civilite: civilite, lastname: lastname, firstname: firstname, floor: floor, number: number},
			'identities.residentId': residentId,
		},
	});
}

function setUpNewStudentWithResident(email, civilite, firstname, lastname, school, matiere, condoId, from, floor, number, company) {
	Accounts.createUser({email});

	const user = Accounts.findUserByEmail(email);

	let roleId = DefaultRoles.findOne({ name: "Occupant", for: "occupant" })._id;
	const residentId = Residents.insert({
		userId: user._id,
		condos: [],
		registeredFrom: (from ? from : "web"),
		pendings: [{
				condoId: condoId,
				roleId: roleId,
				invited: new Date(),
				invitByGestionnaire: true,
				notifications: {
					"actualite" : true,
					"incident" : true,
					"forum_forum" : true,
					"forum_syndic" : true,
					"forum_reco" : true,
					"forum_boite" : true,
					"classifieds" : true,
					"resa_new" : true,
					"resa_rappel" : true,
					"msg_resident" : true,
					"msg_conseil" : true,
					"msg_gardien" : true,
					"msg_gestion" : true,
					"edl" : true
				},
				preferences: {
					messagerie: true
				},
				buildings: [
					{
						buildingId: "",
						type: "tenant"
					}
				]
			}],
	});

	Meteor.users.update({
		_id: user._id,
	}, {
		$set: {
			profile: {civilite: civilite, lastname: lastname, firstname: firstname,
				school: school, matiere: matiere, floor: floor, number: number, company: company, role: 'Resident'},
			'identities.residentId': residentId,
		},
	});
}

Meteor.startup(function () {
	Meteor.methods({
    resendValidationToUser (userId, condoId) {
      check(userId, String)
      check(condoId, String)
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      const thisUser = Meteor.users.findOne({ _id: userId })
      if (thisUser && thisUser.identities.residentId) {
        sendCondoInvitationEmailToNewUser(thisUser.emails[0].address, condoId)
      } else if (thisUser && thisUser.identities.gestionnaireId) {
        const lang = thisUser.profile.lang || 'fr'
        var translation = new Email(lang)
        Meteor.call('sendEmailSync',
          thisUser.emails[0].address,
          condoId,
          translation.email["invit_join"],
          'invite-new-manager-to-enterprise',
          {
            setPasswordLink: Meteor.absoluteUrl(`${lang}/services/set-password/${userId}`, null, null, thisUser.identities.gestionnaireId),
          }
        );
      }
    },
    checkPassword (digest) {
      check(digest, String)

      if (this.userId) {
        const user = Meteor.user()
        const password = { digest: digest, algorithm: 'sha-256' }
        const result = Accounts._checkPassword(user, password)
        return !result.error
      } else {
        return false
      }
    },
    switchReceiptsEmailCondoValue (condoId) {
      check(condoId, String)
      if (!Meteor.userId()) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      const resident = Residents.findOne({ userId: Meteor.userId() })
      let ret = false
      if (resident && condoId) {
        const residentCondo = resident.condos.find((condo) => condo.condoId === condoId)
        if (residentCondo) {
          ret = residentCondo.preferences.emailPaymentReceipts || false
        }
      }
      ret = !ret // switch value
      return Residents.update(
        { userId: Meteor.userId(), 'condos.condoId': condoId },
        {
          $set: {
            'condos.$.preferences.emailPaymentReceipts': ret
          }
        }
      )
    },
    removeOccupantFromCondo(userId, condoId) {
      check(userId, String)
      check(condoId, String)
      if (Meteor.userHasRight('trombi', 'delete', condoId)) {
        let condo = Condos.findOne(condoId);
        if (!condo)
          throw new Meteor.Error(601, "Invalid id", "Cannot find condo");
        ClassifiedsAds.remove({ userId: userId, condoId: condoId }); // remove all the classifieds made by the user (userId)

        // BOOKING
        let resources = Resources.find({ "events.origin": userId, condoId: condoId }).fetch(); // remove all the reservations made by the user (userId)
        _.each(resources, (r) => {
          let events = _.filter(r.events, (e) => { return e.origin === userId });
          _.each(events, (e) => {
            if (e.end > Date.now()) {
              Resources.update({ _id: r._id }, {
                $pull: {
                  events: { eventId: e.eventId }
                }
              });
            }
          });
        });

        //EDL
        EDL.remove({ origin: userId, condoId: condoId });

        let resident = Residents.findOne({ userId: userId });

        let archive = Archives.findOne({ userId: userId });
        let id = condo._id;
        if (archive) {
          Archives.update({ userId: userId, }, {
            $push: {
              condos: {
                type: condo.settings.condoType,
                id: id
              }
            }
          });
        }
        else {
          let user = Meteor.users.findOne(userId);
          let data = {
            lastname: user.profile.lastname,
            userId: user._id,
            firstname: user.profile.firstname,
            email: user.emails[0].address,
            createdAt: user.createdAt,
            deletedAt: new Date(),
            condos: [{ type: condo.settings.condoType, id: id }]
          };
          Archives.insert(data); // add the removed user in the Archives collection
        }
        const lang = Meteor.users.findOne(userId).profile.lang || "fr"
        var translation = new Email(lang);
        if (resident.condos.length < 2) { // if the removed condo (condoId) was the last condo of the user (userId), the user account is deleted
          Meteor.call('sendEmailSync',
            Meteor.users.findOne(userId).emails[0].address,
            id,
            translation.email['desactivation_account'],
            "remove-access-" + lang,
            {
              userId: userId,
              condoId: id,
              formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, id)
            }
          );
          sendNotifRemoveCondo(userId, condoId);
          // delete account
          Residents.remove({ userId: userId });
          Meteor.users.remove({ _id: userId });
        } else { // only the condo (condoId) of the user (userId) is removed
          Meteor.call('sendEmailSync',
            Meteor.users.findOne(userId).emails[0].address,
            id,
            translation.email['delete_condo'] + condo.name,
            "remove-access-condo-" + lang,
            {
              userId: userId,
              condoId: id,
              building: condo.name,
              formLink: Meteor.absoluteUrl(`${lang}/services/feedback`, id)
            }
          );
          Residents.update({ userId: userId }, { // remove the condo (condoId)
            $pull: {
              condos: { condoId: condoId }
            }
          });
          sendNotifRemoveCondo(userId, condoId)
        }
        Meteor.removeUserRights(userId, condoId);
        return true;
      }
    },
    deleteOccupantDocument(documentId, userId = null) {
      check(documentId, String)
      if (userId === null) userId = Meteor.userId()
      const document = UserDocuments.findOne({userId: userId, _id: documentId})
      if (document) {
        UserDocuments.remove({_id: documentId})
        UserDocumentFiles.remove({_id: document.fileId})
      }
    },
    uploadNewOccupantDocument(condoId, title, fileId, userId, uploaderId) {
      check(condoId, String)
      check(title, String)
      check(fileId, String)
      check(userId, Match.Maybe(String))
      check(uploaderId, Match.Maybe(String))

      if (!userId) {
        userId = Meteor.userId()
        uploaderId = Meteor.userId()
      }

      let file = UserDocumentFiles.findOne({_id: fileId})
      let fileLink = file.link()
      UserDocuments.insert({
        condoId,
        title,
        fileId,
        userId,
        fileLink,
        ext: file.ext || null,
        type: file.type || null,
        uploaderId,
        date: Date.now()
      })
    },
    editOccupantDocument(condoId, title, fileId, userId, uploaderId, documentId) {
      check(condoId, String)
      check(title, String)
      check(fileId, Match.Maybe(String))
      check(userId, Match.Maybe(String))
      check(uploaderId, Match.Maybe(String))
      check(documentId, String)

      if (!userId) {
        userId = Meteor.userId()
        uploaderId = Meteor.userId()
      }

      const existingDocument = UserDocuments.findOne({ _id: documentId })

      if (existingDocument) {
        let file = UserDocumentFiles.findOne({ _id: existingDocument.fileId })
        if (!!fileId) {
          UserDocumentFiles.remove({ _id: existingDocument.fileId })
          file = UserDocumentFiles.findOne({ _id: fileId })
        } else {
          fileId = existingDocument.fileId
        }
        let fileLink = file ? file.link() : existingDocument.fileLink

        UserDocuments.update({ _id: documentId }, {
          condoId,
          title,
          fileId,
          userId,
          fileLink,
          ext: file.ext || null,
          type: file.type || null,
          uploaderId,
          date: Date.now()
        })
      }
    },

    saveOccupantBuildingInfo(condoId, form, userId = null) {
      check(condoId, String)
      check(form, Match.ObjectIncluding({
        company: Match.OneOf(null, String),
        diploma: Match.OneOf(null, String),
        school: Match.OneOf(null, String),
        studies: Match.OneOf(null, String),
        door: Match.OneOf(null, String),
        floor: Match.OneOf(null, String),
        office: Match.OneOf(null, String)
      }))
      check(userId, Match.OneOf(null, String))

      if (userId === null) {
        userId = Meteor.userId()
      }

      Residents.update({
        userId: userId,
        'condos.condoId': condoId
      }, {
        $set: {
          'condos.$.userInfo.company': form.company,
          'condos.$.userInfo.diploma': form.diploma,
          'condos.$.userInfo.school': form.school,
          'condos.$.userInfo.studies': form.studies,
          'condos.$.userInfo.porte': form.door,
          'condos.$.userInfo.etage': form.floor,
          'condos.$.userInfo.office': form.office,
          'condos.$.userInfo.wifi': form.wifi,
        }
      })
      Residents.update({
        userId: userId,
        'pendings.condoId': condoId
      }, {
        $set: {
          'pendings.$.userInfo.company': form.company,
          'pendings.$.userInfo.diploma': form.diploma,
          'pendings.$.userInfo.school': form.school,
          'pendings.$.userInfo.studies': form.studies,
          'pendings.$.userInfo.porte': form.door,
          'pendings.$.userInfo.etage': form.floor,
          'pendings.$.userInfo.office': form.office,
          'pendings.$.userInfo.wifi': form.wifi,
        }
      })

    },

    updateInterests(interests, userId = null) {
      check(interests, Match.Maybe(String))
      check(userId, Match.OneOf(null, String))

      if (userId === null) {
        userId = Meteor.userId()
      }
      Meteor.users.update(
        { _id: userId },
        {
          $set: {
            "profile.interests": interests
          }
        }
      )
    },

    updateUserProfile(email, phone, phoneCode, landline, landlineCode, userId = null, firstname = null, lastname = null, statusToOccupant = null) {
      check(email, Match.Maybe(String))
      check(phone, Match.Maybe(String))
      check(phoneCode, Match.Maybe(String))
      check(landline, Match.Maybe(String))
      check(landlineCode, Match.Maybe(String))
      check(userId, Match.OneOf(null, String))
      check(statusToOccupant, Match.OneOf(null, String))
      if (userId === null) {
        userId = Meteor.userId()
      }
      let objectToSet = {
        "profile.tel": phone,
        "profile.telCode": phoneCode,
        "profile.tel2": landline,
        "profile.tel2Code": landlineCode
      }

      if (!!statusToOccupant) objectToSet['profile.statusToOccupant'] = statusToOccupant

      if (Meteor.call('isAdmin', Meteor.userId())) {
        if (firstname) objectToSet['profile.firstname'] = firstname
        if (lastname) objectToSet['profile.lastname'] = lastname
      }
      Meteor.users.update(
        { _id: userId },
        {
          $set: objectToSet
        }
      )
      let thisUser = Meteor.users.findOne(userId)
      if (thisUser.identities.gestionnaireId) {
        let enterpriseId = thisUser.identities.gestionnaireId
        let objectToSet = {
          "users.$.tel": phone,
          "users.$.telCode": phoneCode,
          "users.$.tel2": landline,
          "users.$.tel2Code": landlineCode
        }

        if (!!statusToOccupant) objectToSet['profile.$.statusToOccupant'] = statusToOccupant

        if (Meteor.call('isAdmin', Meteor.userId())) {
          if (firstname) objectToSet['users.$.firstname'] = firstname
          if (lastname) objectToSet['users.$.lastname'] = lastname
        }
        Enterprises.update({
          _id: enterpriseId,
          "users.userId": thisUser._id
        },
          {
            $set: objectToSet
          }
        )
      }
      try {
        prevEmail = thisUser.emails[0].address
        if (prevEmail !== email) {
          Accounts.addEmail(userId, email, true)
          Accounts.removeEmail(userId, prevEmail)
        }
      } catch (e) {
        throw e
      }
    },
    updateUserNames (firstname, lastname, userId) {
      check(firstname, Match.Maybe(String))
      check(lastname, Match.Maybe(String))

      if (userId === null) {
        userId = Meteor.userId()
      }
      let objectToSet = {
        "profile.firstname": firstname,
        "profile.lastname": lastname
      }

      Meteor.users.update(
        { _id: userId },
        {
          $set: objectToSet
        }
      )
      let thisUser = Meteor.users.findOne(userId)
      if (thisUser.identities.gestionnaireId) {
        let enterpriseId = thisUser.identities.gestionnaireId
        let objectToSet = {
          "users.$.firstname": firstname,
          "users.$.lastname": lastname
        }

        Enterprises.update({
          _id: enterpriseId,
          "users.userId": thisUser._id
        },
          {
            $set: objectToSet
          }
        )
      }
    },
    saveNotificationsOccupant: function (condoId, name, value) {
      const actualNotif = Residents.findOne({ userId: Meteor.userId() }).condos.find(condo => { return condo.condoId === condoId }).notifications

      actualNotif[name] = value
      Residents.update({
        userId: this.userId,
        'condos.condoId': condoId
      },
        {
          $set:
            { 'condos.$.notifications': actualNotif }
        });
    },

    updateLangProfile (lang) {
      check(lang, String);
      if (!this.userId) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      Meteor.users.update(Meteor.userId(), { $set: { "profile.lang": lang } });
    },

		updateLastKnownCondo(condoId) {
			Meteor.users.update({
				_id: Meteor.userId()
			}, {
				$set: {
					lastCondoId: condoId
				}
			})
		},

		setApartment(archiveId, number, condoId) {
			let condos = _.flattenDeep(_.map(Archives.find(archiveId).fetch(), function(archive) {
				return archive.condos;
			}))
			_.each(condos, function(condo, index) {
				if (condo.id == condoId) {
					condos[index].apartment = number;
				}
			})
			Archives.update({_id: archiveId}, {
				$set: {
					condos: condos,
				}
			});
		},

		updateMovedIn(archiveId, date) {
			Archives.update({_id: archiveId}, {
				$set: {
					movedInAt: new Date(date),
				}
			});
		},

		updateDeletedAt(archiveId, date) {
			Archives.update({_id: archiveId}, {
				$set: {
					deletedAt: new Date(date),
				}
			});
		},

		submitSatisfaction(archiveId, satisfactionObj) {
			Archives.update({_id: archiveId}, {
				$push: {
					satisfaction: satisfactionObj
				}
			});
		},

		updateFileName(id, name) {
			UserFiles.update({_id: id}, {$set: {"name": name}});
		},

		updateSEO(params) {
			SEO.routes.remove({routeName: params.routeName});
			// SEO.routes.upsert({routeName: params.routeName}, {$set: {
			// 	title: params.routeName + params.lang,
			// 	description: 'A lovely place to be',
			// 	meta: {
			// 		'name="author"': 'monBuilding & Co',
			// 		'property="og:locale"': "en_FR",
			// 		'property="og:locale:alternate"': "en_US",
			// 	}
			// }});
		},
		/*	ADD UNREGISTERED GESTIONNAIRE TO SPECIFIC BASIC CONDO
		 condoId                   (String) ID OF CONDO
		 firstname                 (String) FIRSTNAME OF THE USER
		 lastname                  (String) LASTNAME OF THE USER
		 function                  (String) FUNCTION IN THE CONDO
		 phone 					   (String) PHONE NUMBER
		 pictureId 				   (String) ID OF USER'S PICTURE
		 */
		saveNewUnregisteredManager: function(condoId, newManager) {
			check(condoId, String);
			if (Meteor.call('user_isGestionnaire')) {
				if (Meteor.userHasRight("setContact", "writeWhosWho", condoId) != true)
					throw new Meteor.Error(403, "Not allowed");
				_.each(newManager, function(elem) {
					let id = GestionnaireUnregistered.insert({
						condoId: condoId,
						added: new Date(),
						firstname: elem.firstname,
						lastname: elem.lastname,
						function: elem.function,
						phone: elem.phone,
						pictureId: elem.pictureId
					});
					UserFiles.update({_id: elem.pictureId}, {$set: {"meta.userId": id, "userId": id}});
				})
			}
		},
		removeUneregisteredManager: function(unregisteredManagerId) {
			check(unregisteredManagerId, String);
			if (Meteor.call('user_isGestionnaire')) {
				let manager = GestionnaireUnregistered.findOne(unregisteredManagerId);
				if (Meteor.userHasRight("setContact", "writeWhosWho", manager.condoId) != true)
					throw new Meteor.Error(403, "Not allowed");
				if (manager) {
					if (manager.pictureId)
						UserFiles.remove(manager.pictureId);
					GestionnaireUnregistered.remove(unregisteredManagerId)
				}
			}
		},
		/*	INVITE USER TO SPECIFIC BASIC CONDO
		condoId				   (String) ID OF CONDO
		civilite				  (String) TITLE OF THE USER ("Monsieur" || "Madame")
		firstname				 (String) FIRSTNAME OF THE USER
		lastname				  (String) LASTNAME OF THE USER
		email					 (String) EMAIL ADDRESS OF USER
		*/
		inviteUserToCondo({ condoId, civilite, firstname, lastname, email, from, floor, number}) {
			check(condoId, String);
			check(email, Match.Where((val) => {
				check(val, String);
				return Isemail.validate(val);
			}));
			inviteNewUserToCondo(email, civilite, firstname, lastname, condoId, from, floor, number);
		},
		/*	INVITE USER TO SPECIFIC STUDENT CONDO
		condoId				   (String) ID OF CONDO
		civilite				  (String) TITLE OF THE USER ("Monsieur" || "Madame")
		firstname				 (String) FIRSTNAME OF THE USER
		lastname				  (String) LASTNAME OF THE USER
		email					 (String) EMAIL ADDRESS OF USER
		school					(String) SCHOOL OF THE USER
		matiere				   (String) DISCIPLINE OF THE USER
		*/
		inviteUserToResidence({ condoId, civilite, firstname, lastname, email, school, matiere, from, floor, number, company}) {
			check(condoId, String);
			check(email, Match.Where((val) => {
				check(val, String);
				return Isemail.validate(val);
			}));
			inviteNewUserToResidence(email, civilite, firstname, lastname, school, matiere, condoId, from, floor, number, company);

		},
		/*	CHECK IF SPECIFIC USER HAS PASSWORD
		userId					  (String) USERID OF THE USER TO CHECK
		*/
		hasPassword(userId) {
			const user = Meteor.users.findOne(userId);
			if (!user)
				throw new Meteor.Error("Utilisateur inconnu");
      return (user && user.services && user.services.password && !!user.services.password.bcrypt);
		},
		/*	UPDATE PASSWORD, FIRSTNAME AND LASTNAME OF SPECIFIC USER
		 userId                    (String) USERID OF THE USER
		 pwd                       (String) NEW PASSWORD OF THE USER
		 */
		setPasswordForOccupant({ userId, pwd }) {
			check(userId, String);
			check(pwd, String);

			Accounts.setPassword(userId, pwd);
			let user = Meteor.users.findOne({_id: userId});
      if (!user.deferredRegistrationDate) {
        let residentId = user.identities.residentId;
        let pendings = Residents.findOne({_id: residentId}).pendings;
        pendings.forEach((pending, index) => {
          pendings[index].joined = new Date().getTime();
        });
        Residents.update({_id: residentId}, {$set: {pendings: [], condos: pendings}});
        Meteor.call("createDefaultRightsForNewOccupant", userId);
        sendNotifForNewCondo(userId, pendings[0].condoId)
      }
      return {email: user.emails[0].address, pwd};
		},
		setManagerPassword(userId, password) {
			check(userId, String);
			check(password, String);

			Accounts.setPassword(userId, password);
			let user = Meteor.users.findOne({ _id: userId });
			return { email: user.emails[0].address, password };
    },
    updateUserPending(userId) {
			check(userId, String);

      let user = Meteor.users.findOne({_id: userId});
      const residentId = user.identities.residentId;
      const resident = Residents.findOne({_id: residentId})
      let pendings = resident ? resident.pendings : []
      if (pendings.length > 0) {
        pendings.forEach((pending, index) => {
          pendings[index].joined = new Date().getTime();
        });
        Residents.update({_id: residentId}, {$set: {pendings: [], condos: pendings}});
        Meteor.call("createDefaultRightsForNewOccupant", userId);
        sendNotifForNewCondo(userId, pendings[0].condoId)
      }
    },
		/*	UPDATE EMAIL, PHONE, SCHOOL, DISCIPLINE AND DIPLOMA OF SPECIFIC USER
		email					 (String) NEW EMAIL ADDRESS OF USER
		telephone				 (String) NEW PHONE OF USER
		school					(String) NEW SCHOOL OF USER
		matiere				   (String) NEW DISCIPLINE OF USER
		diplome				   (String) NEW DIPLOMA OF USER
		*/
		changeMyResidentProfile: function (data) {
			try {
				Meteor.users.update(
					{_id: Meteor.userId()},
					{$set:
						{
							"profile.tel": data.telephone,
							"profile.telCode": data.telCode,
							"profile.telCodeCountry": data.telCodeCountry,
							"profile.tel2": data.tel2,
							"profile.school": data.school,
							"profile.matiere": data.matiere,
							"profile.diplome": data.diplome,
							"profile.company": data.company,
							"profile.wifi": data.wifi
						}
					}
				);
				prevEmail = Meteor.user().emails[0].address
				if (prevEmail !== data.email) {
					Accounts.addEmail(Meteor.userId(), data.email, true)
					Accounts.removeEmail(Meteor.userId(), prevEmail)
				}
			} catch (e) {
				throw e
			}
		},
		/*	UPDATE RESIDENT PREFERENCES ON SPECIFIC CONDO
		condoId					 (String) ID OF CONDO
		preferences				 (Object) INDICATE IF THE USER WANTS TO ACTIVATE SPECIFIC MODULES IN SPECIFIC CONDO
		*/
		changeMyResidentPreferences: function(condoId, preferences) {
      const actualPreferences = Residents.findOne({ userId: Meteor.userId() }).condos.find(condo => { return condo.condoId === condoId }).preferences

      const updateData = {...actualPreferences, ...preferences}
			Residents.update(
			{userId: Meteor.userId(), "condos.condoId": condoId},
			{$set:
				{
					"condos.$.preferences": updateData
				}
			}
			);
		},
		/*	UPDATE GESTIONNAIRE JOB, PHONE AND EMAIL
		data
		{
			job					   (String) NEW JOB OF GESTIONNAIRE
			telephone				 (String) NEW PHONE OF GESTIONNAIRE
			email					 (String) NEW EMAIL ADDRESS OF GESTIONNAIRE
		}
		*/
		changeMyBasicsGestionnaire: function(data) {
			check(data.email, Match.Where((val) => {
				check(val, String);
				return Isemail.validate(val);
			}));
			try {
				Meteor.users.update(
					{_id: Meteor.userId()},
					{$set:
						{
							"profile.poste": data.poste,
							'profile.tel': data.telephone,
              'profile.telCode': data.telCode,
							'profile.telCodeCountry': data.telCodeCountry,
							'profile.tel2': data.tel2,
              ...(!!data.statusToOccupant? {'profile.statusToOccupant': data.statusToOccupant}: {})
						}
					}
				);
				prevEmail = Meteor.user().emails[0].address
				if (prevEmail !== data.email) {
					Accounts.addEmail(Meteor.userId(), data.email, true)
					Accounts.removeEmail(Meteor.userId(), prevEmail)
				}
			} catch (e) {
				throw e
			}
		},
    /* Check if a user waiting his password */
    isPasswordPending: function(userId) {
      if (this.userId === undefined)
        throw new Meteor.Error(300, "Not Authenticated")
      if (Meteor.call('user_isGestionnaire')) {
        const user = Meteor.users.findOne(userId)
        if (user === undefined)
          throw new Meteor.Error(601, "Invalid id", "Cannot find resident identity for this user")
        return !(user !== undefined
          && user !== null
          && user.services
          && user.services[0]
          && user.services[0].password)
      }
      else
        throw new Meteor.Error(301, "Access denied")
    },
		// CHECK IF CURRENT USER HAS RESIDENT IDENTITY
		user_isResident: function() {
			if (this.userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let user = Meteor.users.findOne(this.userId, {fields: {"identities": true}});
			if (!user || !user.identities || !user.identities.residentId)
				throw new Meteor.Error(301, "Access denied", "User doesnt have 'resident' identity");
			resident = Residents.findOne({userId: this.userId});
			if (resident === undefined)
				throw new Meteor.Error(601, "Invalid id", "Cannot find resident identity for this user");
			return true;
		},
		/*	CHECK IF SPECIFIC USER LIVES IN SPECIFIC CONDO
		userId					  (String) ID OF USER TO CHECK
		condoId					 (String) ID OF CONDO TO CHECK
		*/
		isResidentOfCondo: function(userId, condoId) {
			if (userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let user = Meteor.users.findOne(userId, {fields: {"identities": true}});
			if (!user || !user.identities || !user.identities.residentId)
				throw new Meteor.Error(301, "Access denied", "User doesnt have 'resident' identity");
			let resident = Residents.findOne({userId: userId});
			if (resident === undefined)
				throw new Meteor.Error(601, "Invalid id", "Cannot find resident identity for this user");
			let condo = Condos.findOne(condoId);
			if (condo === undefined)
				throw new Meteor.Error(601, "Invalid id", "Cannot find condo for this id");
			for (b of resident.condos) {
				if (b.condoId === condoId)
					return true;
			}
			return false;
		},
		/*	CHECK IF SPECIFIC USER IS IN UNION COUNCIL IN SPECIFIC CONDO
		userId					  (String) ID OF USER TO CHECK
		condoId					 (String) ID OF CONDO TO CHECK
		*/
		isInCsOfCondo: function(userId, condoId) {
			if (userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
			return syndicalRole.isUserMember(userId, condoId);
		},
		/*	CHECK IF SPECIFIC USER IS KEEPER IN SPECIFIC CONDO
		userId					  (String) ID OF USER TO CHECK
		condoId					 (String) ID OF CONDO TO CHECK
		*/
		isKeeperOfCondo: function(userId, condoId) {
			if (userId === undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			let supervisorRole = DefaultRoles.findOne({for: "occupant", "name": "Gardien"});
			return supervisorRole.isUserMember(userId, condoId);
		},
		/*	UPDATE CURRENT USER NOTIFICATIONS OF SPECIFIC CONDO
		condoId					 (String) ID OF CONDO TO UPDATE IN RESIDENTS TABLE
		data						(Object) NEW NOTIFICATIONS TO SAVE
		*/
		saveNotificationsUser: function(condoId, data) {
			Residents.update({userId: this.userId,
				'condos.condoId': condoId},
				{$set:
					{'condos.$.notifications': data}
				});
		},
		/* CHECK NOTIFICATION OF RESIDENT USER WITH CONDO ID AND MODULE
			residentId			   (string) ID OF RESIDENT USER
			condoId				  (string) ID OF CONDO
			name					 (string) NAME OF MODULE
		*/
		checkNotifResident(residentId, condoId, name) {
			if (resident = Residents.findOne({userId: residentId})) {
				if (resCondo = _.find(resident.condos, (condo) => {return condo.condoId == condoId})) {
					if (resCondo.notifications[name] && resCondo.notifications[name] != false) {
						return true;
					}
				}
			}
			return false;
		},
		/* CHECK IF MODULE CAN BE SEE BY THE RESIDENT USER WITH CONDO ID AND MODULE
			residentId			   (string) ID OF RESIDENT USER
			condoId				  (string) ID OF CONDO
			name					 (string) NAME OF MODULE ( == displayActuality || displayExchangeSpaces || displayForumCS )
		*/
		/* CHECK NOTIFICATION OF GESTIONNAIRE USER WITH CONDO ID AND MODULE
			gestionnaireId		   (string) ID OF GESTIONNAIRE USER
			condoId				  (string) ID OF CONDO
			name					 (string) NAME OF MODULE
		*/
		checkNotifGestionnaire(gestionnaireId, condoId, name) {
			if (gestionnaire = Enterprises.findOne({"condos": condoId})) {
				if (gUser = gestionnaire.users.find((u) => {return u.userId == gestionnaireId})) {
					if (condo = gUser.condosInCharge.find((condo)=>{return condo.condoId == condoId})) {
						if (condo.notifications[name] && condo.notifications[name] != false) {
							return true;
						}
					}
				}
			}
			return false;
		},
		/* CHECK IF MODULE CAN BE SEE OF GESTIONNAIRE USER WITH CONDO ID AND MODULE
			gestionnaireId			   (string) ID OF RESIDENT USER
			condoId				  (string) ID OF CONDO
		*/
		checkModuleGestionnaire(gestionnaireId, condoId) {
			if (gestionnaire = Enterprises.findOne({"condos": condoId})) {
				if (gUser = gestionnaire.users.find((u) => {return u.userId == gestionnaireId})) {
					if (condo = gUser.condosInCharge.find((condo)=>{return condo.condoId == condoId})) {
						if (condo.access.length == 2) { // All right
							return true;
						}
						else if (condo.access[0] == 0 || condo.access[0] == 1) { // See right or write right
							return true;
						}
					}
				}
			}
			return false;
		},
		// UPDATE AVATAR OF CURRENT USER
		// fileId					  (String) ID OF NEW AVATAR IN USERFILES TABLES
		setAvatarId: function(fileId, userId = null) {
      check(fileId, Match.OneOf(null, String))
      check(userId, Match.OneOf(null, String))
      if (userId === null) {
        userId = Meteor.userId()
      }
      const user = Meteor.users.findOne({ _id: userId })
			if (user.profile.avatarId && user.profile.avatarId != "") {
				let uf = UserFiles.findOne(user.profile.avatarId);
				if (uf && uf != undefined)
					uf.remove();
			}
			Meteor.users.update(
				{_id : userId},
				{$set:
					{'profile.avatarId': fileId}
				});
      const fileData = UserFiles.findOne({_id: fileId});
      if (fileData) {
        avatar = {
          fileId,
          mime: fileData.type,
          ext: fileData.extension
        }
        Meteor.users.update({_id: userId}, {$set: {
          avatar
        }})
      }
			if (resident = Residents.findOne({userId: userId})) {
				Residents.update({userId: userId}, {$set:
					{'fileId': fileId}
				});
			}
			if (gestionnaire = Enterprises.findOne({"users.userId": userId})) {
				Enterprises.update({_id: gestionnaire._id, 'users.userId': userId}, {
					$set: {
						'users.$.profile.fileId': fileId,
					}
				});
			}

		},
		// UPDATE LOGO OF GESTIONNAIRE CURRENTLY CONNECTED
		// fileId					  (String) ID OF NEW LOGO IN USERFILES TABLES
		setLogoId: function(fileId) {
			let user = Meteor.users.findOne(userId, {fields: {"identities": true}});
			Enterprises.update({_id: user.identities.gestionnaireId},
				{$set:
					{'logoId': fileId}
				});
		},
		// GET USERS MATCHING WITH SPECIFIC USERID
		// userId					  (String) USERID OF THE USER TO GET
		usernameById: function(userId) {
			return Meteor.users.findOne(userId);
		},
		// GET USERIDs OF RESIDENTS OF SPECIFIC CONDO
		// condoId					 (String) ID OF THE CONDO TO GET THE RESIDENTS
		getResidentsOfCondo: function(condoId) {
			let residents = Residents.find({"condos.condoId": condoId}).fetch();
			return (_.map(residents, (resident) => {return resident.userId}));
		},
		getResidentsOfCondoWithRight: function(condoId, module, right) {
      let residents = Residents.find({ "condos.condoId": condoId }, { fields: { userId: true } }).fetch()
      return residents.map(resident => {
        if (Meteor.userHasRight(module, right, condoId, resident.userId) && !Meteor.call('isAdmin', resident.userId)) {
          return resident.userId
        }
      })
		},
    getGestionnaireOfCondoInChargeWithRight: function(condoId, module, right) {
      let gestionnaire = Enterprises.findOne({"condos": condoId});
      let ret = [];
      // console.log("users gestionnaire: ", gestionnaire)
      if (!!gestionnaire && !!gestionnaire.users) {
        _.each(gestionnaire.users, (gUser, index) => {
          if (_.find(gUser.condosInCharge, (c) => {return c.condoId === condoId}) && Meteor.userHasRight(module, right, condoId, gUser.userId) && !gUser.isAdmin)
            ret.push(gUser.userId);
        })
      }
      return ret;
    },
		getCSOfCondo: function(condoId) {
			let syndicalRole = DefaultRoles.findOne({for: "occupant", "name": "Conseil Syndical"});
			residents = syndicalRole.getUserIdsByCondo(condoId);
			return residents;
		},
		getKeeperOfCondo: function(condoId) {
			let supervisorRole = DefaultRoles.findOne({for: "occupant", "name": "Gardien"});
			residents = supervisorRole.getUserIdsByCondo(condoId);
			return residents;
		},
		getGestionnaireOfCondo: function(condoId) {
			let gestionnaire = Enterprises.findOne({"condos": condoId})
			return (_.map(gestionnaire.users, (gUser) => {return gUser.userId}))
		},
		getGestionnaireOfCondoInCharge: function(condoId) {
			let gestionnaire = Enterprises.findOne({"condos": condoId});
			let ret = [];
			// console.log("users gestionnaire: ", gestionnaire)
      if (!!gestionnaire && !!gestionnaire.users) {
        _.each(gestionnaire.users, (gUser, index) => {
          if (_.find(gUser.condosInCharge, (c) => {return c.condoId === condoId}) && !gUser.isAdmin)
            ret.push(gUser.userId);
        })
      }
			return ret;
		},
		connectWithFacebook: function(tokenId, tokenExpires) {
			check(tokenId, String);
			var result = '';
			try {
					result = HTTP.call('GET', `https://graph.facebook.com/me?fields=email,name,picture.type(large)&access_token=${tokenId}`, {
					params: { token: tokenId }
				});
			} catch (e) {
				return null;
			}
			if (result && _.has(result, "data")) {
				var picture = '';
				// if (result.data.picture.data.url) {
					//  try {
					// 		picture = HTTP.call('GET', result.data.picture.data.url);
					//  } catch (e) {
					// 	return null;
					//  }
				// }
				const user = Meteor.users.findOne({'emails.address': result.data.email});
				if (user) {
						this.setUserId(user._id);
						if (user.profile && (user.profile.avatarId == undefined || user.profile.avatarId == "") && result.data.picture.data.url) {
							UserFiles.load(result.data.picture.data.url, {
								meta: {
									userId: user._id,
									publicPicture: true
								}
							}, function(error, fileRef) {
								if (!error) {
									if (/png|jpe?g/i.test(fileRef.extension || '')) {
										Meteor.setTimeout( () => {
											createThumbnails(UserFiles, fileRef, (error) => {
												if (error) {
													console.error(error);
												}
											});
										}, 1024);
									}
									Meteor.users.update({_id: user._id}, );
									if (resident = Residents.findOne({userId: user._id})) {
										Residents.update({userId: user._id}, {$set:
											{'fileId': fileRef._id}
										});
									}
									if (gestionnaire = Enterprises.findOne({"users.userId": user._id})) {
										Enterprises.update({_id: gestionnaire._id, 'users.userId': user._id}, {
											$set: {
												'users.$.profile.fileId': fileRef._id,
											}
										});
									}
								}
								else
									throw new Meteor.Error(301, "Error", "Error with file upload");
							})
						}
						const stampedToken = Accounts._generateStampedLoginToken()
						const hashStampedToken = Accounts._hashStampedToken(stampedToken)
						const userId = user._id
						Meteor.users.update(userId, {
							$push: {
								'services.resume.loginTokens': hashStampedToken
							}
						})
						return {
							id: userId,
							type: 'facebook',
							token: hashStampedToken, // Accounts._storedLoginToken(),
							rememberMe: true,
							tokenExpires: new Date(tokenExpires)
						}
						//return user;
				} else {
					 throw new Meteor.Error(301, "Email is not registered", "Email not registered.");
				}
			}
		},
		// CHECK IF RESET PASSWORD TOKEN IS STILL ACTIVE AND USABLE
		isResetTokenActive: function(token) {
			let user = Meteor.users.findOne(
			{
				$and : [
					{"services.password.reset.token": {$exists: true}},
					{"services.password.reset.token": token}
				]
			});
			if (user)
				return token;
			return false;
		},
		desactivateHelper: function(module) {
			if (Meteor.userId() == undefined)
				throw new Meteor.Error(300, "Not Authenticated");
			if (Helpers.findOne({_id: Meteor.userId()}) == undefined) {
				Helpers.insert({
					"_id": Meteor.userId(),
					"information": false,
					"trombi": false,
					"forum": false,
					"forumcs": false,
					"ad": false,
				})
			}
			let field = {};
			field[module] = true;
			Helpers.update(
				{ _id: Meteor.userId() },
				{
					$set: field
				},
			)
		}
	});
});
