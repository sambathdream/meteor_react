/*
 SOME DATA ARE PUBLISHED BY DEFAULT (DOESN'T REQUIRE ANY SUBSCRIPTION FROM CLIENT SIDE) IF A USER IS CONNECTED

	 - CURRENT USER IN COLLECTION "users"
	 - CURRENT USER RESIDENT ENTRY (IF IS RESIDENT) IN COLLECTION "Residents"
	 - CURRENT USER GESTIONNAIRE ENTRY (IF IS GESTIONNAIRE) IN COLLECTION "Gestionnaire"
 */

Meteor.publish('UserDocumentsPublish', function() {
  let userId = Meteor.userId()

  return UserDocuments.find({userId: userId});
})

Meteor.publish('UserDocumentsPublishManagerSide', function(userId) {
  if (Meteor.call('isAdmin', Meteor.userId()) || Meteor.call('user_isGestionnaire')) {
    return UserDocuments.find({userId: userId});
  }
  else {
    this.stop()
  }

})

Meteor.publish('usersProfile', function (condoIds) {
  if (!Meteor.user()) {
    return this.stop()
  }
	if (Meteor.call('isAdmin', Meteor.userId())) {
		condoIds = _.map(Condos.find({}, {fields: {_id: true}}).fetch(), '_id')
  }

  if (!condoIds) {
    if (Meteor.user().identities.residentId) {
      const thisResident = Residents.findOne({ _id: Meteor.user().identities.residentId }, { fields: { condos: true } })
      if (thisResident && thisResident.condos) {
        condoIds = _.map(thisResident.condos, 'condoId')
      }
    } else if (Meteor.user().identities.gestionnaireId) {
      const thisManager = Enterprises.findOne({ _id: Meteor.user().identities.gestionnaireId }, { fields: { users: true } })
      if (thisManager && thisManager.users) {
        const thisUser = thisManager.users.find(((u) => u.userId === Meteor.userId()))
        if (thisUser) {
          condoIds = _.map(thisUser.condosInCharge, 'condoId')
        }
      }
    }
  }

  if (typeof condoIds === 'string') {
    condoIds = [condoIds]
  }

  let resident = Residents.find({ $or: [{ "condos.condoId": { $in: condoIds } }, { "pendings.condoId": { $in: condoIds } } ] }, {fields: {userId: true}});

  let self = this;
  const handleMeteorUser = Meteor.users.find({}, { fields: { profile: true, emails: true, _id: true } }).observe({
    changed: function (user, oldDocument) {
      if (user._id) {
        try {
          self.changed('usersProfile', user._id, {
            firstname: user.profile.firstname,
            lastname: user.profile.lastname,
            tel: user.profile.tel || '',
            telCode: user.profile.telCode || '',
            tel2: user.profile.tel2 || '',
            tel2Code: user.profile.tel2Code || '',
            email: user.emails[0].address,
            statusToOccupant: user.profile.statusToOccupant,
            interests: user.profile.interests,
            role: "occupant"
          });
        } catch (e) {
          // just here to prevent error :)
        }
      }

    },
  })

  const handleResident = resident.observe({
		added: function (document) {
			if (document.userId) {
        let user = Meteor.users.findOne(document.userId, {fields: {profile: true, emails: true}})
        if (user && user.profile) {
          self.added('usersProfile', document.userId, {
            firstname: user.profile.firstname,
            lastname: user.profile.lastname,
            tel: user.profile.tel || '',
            telCode: user.profile.telCode || '',
            tel2: user.profile.tel2 || '',
            tel2Code: user.profile.tel2Code || '',
            email: user.emails[0].address,
            statusToOccupant: user.profile.statusToOccupant,
            interests: user.profile.interests,
            role: "occupant"
          });
        }
			}
		},
		changed: function (newDocument, oldDocument) {
			if (newDocument.userId) {
        let user = Meteor.users.findOne(newDocument.userId, { fields: { profile: true, emails: true } })
        if (user && user.profile) {
          self.changed('usersProfile', newDocument.userId, {
            firstname: user.profile.firstname,
            lastname: user.profile.lastname,
            tel: user.profile.tel || '',
            telCode: user.profile.telCode || '',
            tel2: user.profile.tel2 || '',
            tel2Code: user.profile.tel2Code || '',
            email: user.emails[0].address,
            statusToOccupant: user.profile.statusToOccupant,
            interests: user.profile.interests,
            role: "occupant"
          });
        }
			}

		},
		removed: function (oldDocument) {
			if (oldDocument.userId)
				self.removed('usersProfile', oldDocument.userId);
		}
	})

	let enterprises = Enterprises.find({condos: {$in: condoIds}}, {fields: {users: true}})
	const handleEnterprise = enterprises.observe({
		added: function (document) {
			_.each(document.users, (user) => {
        let hasCondo = false;
				// if ((user.isAdmin == undefined || user.isAdmin == false) && _.find(user.condosInCharge, (condo) => { if (_.includes(condoIds, condo.condoId)) return true })) {
					let userMeteor = Meteor.users.findOne(user.userId, { fields: { profile: true, emails: true, services: true } })
					self.added('usersProfile', user.userId, {
						firstname: userMeteor.profile.firstname,
						lastname: userMeteor.profile.lastname,
            tel: userMeteor.profile.tel || '',
            telCode: userMeteor.profile.telCode || '',
            tel2: userMeteor.profile.tel2 || '',
            tel2Code: userMeteor.profile.tel2Code || '',
						email: userMeteor.emails[0].address,
            interests: userMeteor.profile.interests,
            statusToOccupant: userMeteor.profile.statusToOccupant,
            isPending: !userMeteor.services.password,
						role: "manager"
					});
				// }
			});
		},
		changed: function (newDocument, oldDocument) {
			_.each(newDocument.users, (user) => {
				let hasCondo = false;
				// if ((user.isAdmin == undefined || user.isAdmin == false) && _.find(user.condosInCharge, (condo) => { if (_.includes(condoIds, condo.condoId)) return true })) {
          let userMeteor = Meteor.users.findOne(user.userId, { fields: { profile: true, emails: true, services: true } })
          if (userMeteor) {
            try {
              self.changed('usersProfile', user.userId, {
                firstname: userMeteor.profile.firstname,
                lastname: userMeteor.profile.lastname,
                  tel: userMeteor.profile.tel || '',
                  telCode: userMeteor.profile.telCode || '',
                tel2: userMeteor.profile.tel2 || '',
                tel2Code: userMeteor.profile.tel2Code || '',
                email: userMeteor.emails[0].address,
                interests: userMeteor.profile.interests,
                statusToOccupant: userMeteor.profile.statusToOccupant,
                isPending: !userMeteor.services.password,
                role: "manager"
              });
            } catch(e) {
              try {
                self.added('usersProfile', user.userId, {
                  firstname: userMeteor.profile.firstname,
                  lastname: userMeteor.profile.lastname,
                  tel: userMeteor.profile.tel || '',
                  telCode: userMeteor.profile.telCode || '',
                  tel2: userMeteor.profile.tel2 || '',
                  tel2Code: userMeteor.profile.tel2Code || '',
                  email: userMeteor.emails[0].address,
                  interests: userMeteor.profile.interests,
                  statusToOccupant: userMeteor.profile.statusToOccupant,
                  isPending: !userMeteor.services.password,
                  role: "manager"
                });
              } catch (e){ console.log(e) }
            }
          } else {
            try {
              self.removed('usersProfile', user.userId);
            } catch (e) {}
          }
				// }
			});
		},
		removed: function (oldDocument) {
			_.each(oldDocument.users, (user) => {
				let hasCondo = false;
				// if ((user.isAdmin == undefined || user.isAdmin == false) && _.find(user.condosInCharge, (condo) => { if (_.includes(condoIds, condo.condoId)) return true })) {
        self.removed('usersProfile', user.userId);
				// }
			});
		}
	})

  this.onStop(() => {
    return handleMeteorUser.stop() && handleResident.stop() && handleEnterprise.stop()
  })
	self.ready()
});

// PUBLISH CURRENT USER ENTRY OF COLLECTIONS "users" and "Residents" (REQUIRED TO SET PASSWORD IF NEW USER HAS BEEN INVITED)
// userId              (String) ID OF THE USER TO GET INFORMATION

Meteor.publish('userWithJoinedDate', function(condoId) {
	let residents = Residents.find({"condos.condoId": condoId}, {fields: {"condos.joined": true, userId: true, "condos.condoId": true}});

	let self = this;
	const handler = residents.observe({
		added: function (document) {
			self.added('usersJoinedDate', document.userId, {joined: _.find(document.condos, (condo) => { return condo.condoId == condoId }).joined});
		},
		changed: function (newDocument, oldDocument) {
			self.changed('usersJoinedDate', newDocument.userId, {joined: _.find(newDocument.condos, (condo) => { return condo.condoId == condoId }).joined});
		},
		removed: function (oldDocument) {
			self.removed('usersJoinedDate', oldDocument.userId);
		},
  });
  this.onStop(() => {
    return handler.stop()
  })
	this.ready();
})

Meteor.publish('newUserForPassword', function(userId) {
	return [Meteor.users.find(userId), Residents.find({userId: userId})];
});

Meteor.publish('GestionnaireUnregistered', function(userId) {
	return GestionnaireUnregistered.find();
});

Meteor.publish("userStatus", function() {
	if (this.userId) {
		let users = Meteor.users.find({$and: [
			{"status.online": true},
			{"identities.adminId": {$exists: false}},
		]}, {fields: {status: true}});
		this.ready();
		return users;
	}
	else
		this.stop();
});
Meteor.publish("helpers", function() {
	if (Meteor.userId())
		return Helpers.find({"_id": Meteor.userId()});
});

process.env.HTTP_FORWARDED_COUNT = 1;

