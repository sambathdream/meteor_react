let counter = 0
Meteor.publish('resident_badges', function () {
  counter = 0
  this.unblock()
  if (!this.userId) {
    this.stop()
    throw new Meteor.Error(300, "Not Authenticated");
  }

  let self = this;
  let userId = this.userId;
  let condoIds = []
  let resident = Residents.findOne({ 'userId': userId })
  if (!resident) {
    this.stop()
    throw new Meteor.Error(300, "Resident not found");
  }
  const badgesModules = {
    manual: 0,
    map: 0,
    messagerieResident: 0,
    messagerieGestionnaire: 0,
    messagerieConseilSyndical: 0,
    messagerieGardien: 0,
    forum: 0,
    myforum: 0,
    ads: 0,
    myads: 0,
    event: 0,
    information: 0,
    incidentResident: 0,
    reservation: 0,
    reservation_reject: 0,
    newUser: 0
  };
  const badgesNames = {
    messagerie: true,
    forum: true,
    actuality: true,
    classifieds: true,
    incident: true,
    reservation: true
  };

  condoIds = _.map(resident.condos, (elem) => {
    self.added('residentBadges', elem.condoId, badgesModules);
    return { condoId: elem.condoId, joined: elem.joined }
  })
  // return this.ready() // TO REMOVE HERE FOR TESTING !!

  let handlers = []
  let mapSelector = module_map_getSelector(condoIds);
  handlers.push(mapSelector.observeChanges({
    changed: function (documentId, document) {
      module_map_updateBadges(document.condoId, userId, "changed", self);
    },
    added: function (documentId, document) {
      module_map_updateBadges(document.condoId, userId, "changed", self);
    }
  }))

  // V-- It's for Manual badges

  let manualSelector = module_manual_getSelector(condoIds);
  handlers.push(manualSelector.observeChanges({
    changed: function (documentId, document) {
      module_manual_updateBadges(document.condoId, userId, "changed", self);
    },
    added: function (documentId, document) {
      module_manual_updateBadges(document.condoId, userId, "changed", self);
    }
  }))

  _.each(condoIds, (elem) => {
    let condoId = elem.condoId
    let joined = moment(elem.joined).format('x')

    let condo = Condos.findOne(condoId, {fields: {modules: 1}});

    // V-- It's for messagerie, forum, event, information, classifieds, incident badges
    _.each(condo.modules, (m) => {
      const moduleName = m.name
      if (badgesNames[moduleName]) {
        let select = null
        let funcNameBadges = null
        let moduleId = m.data[m.name + "Id"];
        if (moduleName === 'messagerie') {
          selector = module_messagerie_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_messagerie_updateBadges
        } else if (moduleName === 'forum') {
          selector = module_forum_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_forum_updateBadges
        } else if (moduleName === 'classifieds') {
          selector = module_classifieds_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_classifieds_updateBadges
        } else if (moduleName === 'actuality') {
          selector = module_actuality_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_actuality_updateBadges
        } else if (moduleName === 'incident') {
          selector = module_incident_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_incident_updateBadges
        } else if (moduleName === 'reservation') {
          selector = module_reservation_getSelector(condoId, userId, moduleId, false)
          funcNameBadges = module_reservation_updateBadges
        }

        if (!selector) {
          return ;
        }
        handlers.push(selector.observeChanges({
          changed: function(id, document) {
            funcNameBadges(condoId, userId, moduleId, "changed", self, joined)
          },
          added: function (id, document) {
            // console.log(id)
            funcNameBadges(condoId, userId, moduleId, "changed", self, joined)
          }
        }))
      }
      let pendingUserSelector = module_pending_user_getSelector(condoId)
      handlers.push(pendingUserSelector.observeChanges({
        changed: function (documentId, document) {
          module_pending_user_updateBadges(condoId, self)
        },
        added: function (documentId, document) {
          module_pending_user_updateBadges(condoId, self)
        }
      }))
    })
  })
  this.onStop(() => {
    handlers.forEach(handler => {
      handler.stop()
    })
    return true
  })
  return this.ready();
});

function module_messagerie_getSelector(condoId, userId, moduleId, test = false) {
  if (test === true) {
    return Messages.find({ "users.userId": userId, condoId: condoId }, { limit: 1 });
  }
  return Messages.find({ "users.userId": userId, condoId: condoId });
}
function module_messagerie_updateBadges(condoId, userId, moduleId, action, self) {
  let writeToResident = Meteor.userHasRight("messenger", "writeToResident", condoId, userId);
  let writeToManager = Meteor.userHasRight("messenger", "writeToManager", condoId, userId);
  let writeToUnionCouncil = Meteor.userHasRight("messenger", "writeToUnionCouncil", condoId, userId);
  let writeToSupervisors = Meteor.userHasRight("messenger", "writeToSupervisors", condoId, userId);

  if (!writeToResident && !writeToManager && !writeToUnionCouncil && !writeToSupervisors)
  return false
  let messages = module_messagerie_getSelector(condoId, userId, moduleId).fetch();
  let nb = 0;
  let nb2 = 0;
  let nb3 = 0;
  let nb4 = 0;
  const occupantRole = DefaultRoles.findOne({ name: 'Occupant' })
  const unionCouncilRole = DefaultRoles.findOne({ name: 'Conseil Syndical' })
  const supervisorRole = DefaultRoles.findOne({ name: 'Gardien' })
  for (let i = messages.length - 1; i >= 0; i--) {
    // console.log('counter++', counter++)
    let user = _.find(messages[i].users, (user) => {return user.userId === self.userId});
    let lastMsgDate = messages[i].lastMsg
    if (writeToResident && !!occupantRole === true && messages[i].target === occupantRole._id && user && (!user.lastVisit || user.lastVisit < lastMsgDate)) {
      nb++;
    } else if (writeToManager && messages[i].target === "manager" && user && (!user.lastVisit || user.lastVisit < lastMsgDate)) {
      nb2++;
    } else if (writeToUnionCouncil && !!unionCouncilRole === true && messages[i].target === unionCouncilRole._id && user && (!user.lastVisit || user.lastVisit < lastMsgDate)) {
      nb3++;
    } else if (writeToSupervisors && !!supervisorRole === true && messages[i].target === supervisorRole._id && user && (!user.lastVisit || user.lastVisit < lastMsgDate)) {
      nb4++;
    }
  }
  self.changed('residentBadges', condoId, {
    messagerieResident: nb,
    messagerieGestionnaire: nb2,
    messagerieConseilSyndical: nb3,
    messagerieGardien: nb4
  });
}

function module_forum_getSelector(condoId, userId, moduleId, test = false) {
  if (test === true) {
    return ForumPosts.find({condoId}, { limit: 1 });

  }
  return ForumPosts.find({condoId});
}

function module_forum_updateBadges(condoId, userId, moduleId, action, self, joined) {
  let seePoll = Meteor.userHasRight("forum", "seePoll", condoId, userId);
  let seePost = Meteor.userHasRight("forum", "seeSubject", condoId, userId);

  if (!seePoll && !seePost) {
    return false
  }
  let posts = module_forum_getSelector(condoId, userId, moduleId).fetch();
  let nb = 0;
  let mynb = 0;
  for (let i = posts.length - 1; i >= 0; i--) {
    // console.log('counter++', counter++)
    let thisUserView = _.find(posts[i].views, (date, viewUserId) => {
      if (viewUserId === userId) {
        return true
      }
    })
    if (((!!thisUserView === false && joined <= posts[i].updatedAt) || (thisUserView < posts[i].updatedAt))) {
      if ((posts[i].type === 'poll' && seePoll) || (posts[i].type === 'post' && seePost)) {
        if (posts[i].userId === userId)
        mynb++
        else if (!!thisUserView === false) {
          nb++
        } else {
          if (posts[i].userId !== userId) {
            let replyOfUser = ForumCommentPosts.findOne({
              postId: posts[i]._id,
              userId: userId
            }, {fields: {_id: true}})
            if (replyOfUser && thisUserView < posts[i].updatedAt) {
              nb++
            }
          }
        }
      }
    }
  }
  self.changed('residentBadges', condoId, { forum: nb, myforum: mynb });
}

function module_classifieds_getSelector(condoId, userId, moduleId, test = false) {
  if (test === true) {
    return ClassifiedsAds.find({ classifiedsId: moduleId }, { limit: 1 });
  }
  return ClassifiedsAds.find({ classifiedsId: moduleId });
}

function module_classifieds_updateBadges(condoId, userId, moduleId, action, self, joined) {
  let seeAds = Meteor.userHasRight("ads", "see", condoId, userId);

  if (!seeAds) {
    return false
  }
  let posts = module_classifieds_getSelector(condoId, userId, moduleId).fetch();
  let nb = 0;
  let mynb = 0;
  for (let i = posts.length - 1; i >= 0; i--) {
    // console.log('counter++', counter++)
    let view = posts[i].views[userId]
    if ((!!view === false && joined <= posts[i].updatedAt) || (view < posts[i].updatedAt)) {
      if (posts[i].userId === userId) {
        mynb++
      } else {
        if (!view) {
          nb++
        } else {
          if (view < posts[i].updatedAt) {
            let replyOfUser = ClassifiedsFeed.findOne({
              classifiedId: posts[i]._id,
              userId: userId
            }, { fields: { _id: true } })
            if (replyOfUser) {
              nb++
            }
          }
        }
      }
    }
  }
  self.changed('residentBadges', condoId, { ads: nb, myads: mynb });
}

function module_actuality_getSelector(condoId, userId, moduleId, test = false){
  if (test === true) {
    return ActuPosts.find({ actualityId: moduleId }, { limit: 1 });
  }
  return ActuPosts.find({ actualityId: moduleId });
}

function module_actuality_updateBadges(condoId, userId, moduleId, action, self, joined) {
  let seeActu = Meteor.userHasRight("actuality", "see", condoId, userId);

  if (!seeActu) {
    return false
  }
  let posts = module_actuality_getSelector(condoId, userId, moduleId).fetch();
  let information = 0;
  let event = 0;
  for (let i = posts.length - 1; i >= 0; i--) {
    // console.log('counter++', counter++)
    let view = _.find(posts[i].views, (e) => {
      return e === Meteor.userId();
    });
    if (!!view === false && joined <= posts[i].createdAt) {
      if (posts[i].startDate === '') {
        information++;
      } else {
        event++;
      }
    }
  }
  self.changed('residentBadges', condoId, { information, event });
}

function module_incident_getSelector(condoId, userId, moduleId, test = false) {
  let residentRoleId = UsersRights.findOne({
    $and: [
      { "userId": userId },
      { "condoId": condoId }
    ]
  })
  if (!residentRoleId) return
  residentRoleId = residentRoleId.defaultRoleId;
  let isOccupant = !!DefaultRoles.findOne({ _id: residentRoleId, name: 'Occupant' })
  let IncidentQuery = {
    condoId: condoId,
    $or: [
      {
        "state.status": {
          $in: [1, 2]
        }
      },
      {
        $or: [
          {
            $and: [
              { "state.status": { $in: [0, 3] } },
              { "declarer.userId": userId }
            ]
          }
        ]
      }
    ]
  }
  if (!isOccupant) {
    // don't touch ! Ask to me ! @vmariot
    let field = 'history.share.' + residentRoleId + '.state'
    IncidentQuery.$or[1].$or.push({
      $and: [
        { "state.status": 3 },
        { [field]: true }
      ]
    })
  }
  if (test === true) {
    return Incidents.find(IncidentQuery, { limit: 1 });
  }
  return Incidents.find(IncidentQuery);
}

function module_incident_updateBadges(condoId, userId, moduleId, action, self, joined) {
  let seeIncident = Meteor.userHasRight("incident", "see", condoId, userId);

  if (!seeIncident) {
    return false
  }
  let incidents = module_incident_getSelector(condoId, userId, moduleId).fetch();
  let nb = 0;
  let dateNow = new Date();
  for (let i = incidents.length - 1; i >= 0; i--) {
    // console.log('counter++', counter++)
    let view = _.find(incidents[i].view, function (view) {
      return view.userId === userId
    });
    if ((!view && joined <= incidents[i].lastUpdate)  || (view && view.lastView < dateNow && !view.lastViewHistoric) || (view && incidents[i].lastUpdate > view.lastViewHistoric)) {
      nb++;
    }
  }
  self.changed('residentBadges', condoId, { incidentResident: nb });
}

function module_manual_getSelector(condoIds, test = false) {
  if (test === true) {
    return ViewCondoDocument.find({ 'condoId': { $in: condoIds }, 'type': 'manual' }, { limit: 1 });
  }
  return ViewCondoDocument.find({ 'condoId': { $in: condoIds }, 'type': 'manual' });
}
function module_manual_updateBadges(condoId, userId, action, self) {
  let hasManualNotif = 0

  if (!condoId) {
    return false
  }

  let seeManual = Meteor.userHasRight("manual", "see", condoId, userId);
  if (!seeManual) {
    return false
  }
  let condoManual = ViewCondoDocument.findOne({ 'condoId': condoId, 'type': 'manual' })

  if (!condoManual) {
    return
  }
  if (condoManual && seeManual) {
    let view = condoManual.views
    if (view && view[userId]) {
      let condo = Condos.findOne({ '_id': condoId })
      if (condo && condo.manual) {
        let manualOfCondo = condo.manual
        hasManualNotif = view[userId] < manualOfCondo.manualUpdate ? 1 : 0
      }
    } else {
      hasManualNotif = 1
    }
  } else {
    hasManualNotif = 0
  }

  self.changed('residentBadges', condoId, { manual: hasManualNotif });
}

function module_map_getSelector(condoIds, test = false) {
  if (test === true) {
    return ViewCondoDocument.find({ 'condoId': { $in: condoIds }, 'type': 'map' }, { limit: 1 });
  }
  return ViewCondoDocument.find({ 'condoId': { $in: condoIds }, 'type': 'map' });
}
function module_map_updateBadges(condoId, userId, action, self) {
  let hasMapNotif = 0

  if (!condoId) {
    return false
  }

  let seeMap = Meteor.userHasRight("map", "see", condoId, userId);
  if (!seeMap) {
    return false
  }
  let condoMap = ViewCondoDocument.findOne({ 'condoId': condoId, 'type': 'map' })
  if (!condoMap) {
    return
  }
  if (condoMap && seeMap) {
    let view = condoMap.views
    if (view && view[userId]) {
      let condo = Condos.findOne({ '_id': condoId })
      if (condo && condo.map) {
        let mapOfCondo = condo.map
        hasMapNotif = view[userId] < mapOfCondo.mapUpdate ? 1 : 0
      }
    } else {
      hasMapNotif = 1
    }
  } else {
    hasMapNotif = 0
  }

  self.changed('residentBadges', condoId, { map: hasMapNotif });
}

function module_reservation_getSelector(condoId, userId, moduleId, test = false) {
  if (test === true) {
    return ViewOfReservation.find({ condoId: condoId }, { limit: 1 })
  }
  return ViewOfReservation.find({ condoId: condoId })
}

function module_reservation_updateBadges(condoId, userId, moduleId, action, self) {
  let nb_validated = 0;
  let nb_reject = 0;
  let reservations = Reservations.find({
    $or: [
      { origin: userId },
      { participants: userId },
    ],
    start: { $gte: +moment().format('x') },
    condoId: condoId
  }).fetch();
  if (!reservations) {
    return false
  }
  let resourceIds = _.map(reservations, (r) => r.resourceId);
  let resources = Resources.find({ _id: { $in: resourceIds } }).fetch();
  for (let i = 0; i < reservations.length; i++) {
    // console.log('counter++', counter++)
    const reservation = reservations[i];
    let resourceOfResa = resources.find((r) => r._id === reservation.resourceId);
    if (resourceOfResa) {
      if (resourceOfResa.needValidation) {
        if (reservation.pending === false && reservation.rejected === false) {
          let viewOfResa = ViewOfReservation.findOne({ _id: reservation.eventId })
          if (viewOfResa) {
            if (viewOfResa.view && viewOfResa.view[Meteor.userId()]) {
              if (viewOfResa.view[Meteor.userId()] < reservation.validateAt) {
                nb_validated++
              } else {
                // Do Nothing
              }
            } else {
              nb_validated++
            }
          }
        } else if (reservation.pending === false && reservation.rejected === true) {
          let viewOfResa = ViewOfReservation.findOne({ _id: reservation.eventId })
          if (viewOfResa) {
            if (viewOfResa.view && viewOfResa.view[Meteor.userId()]) {
              if (viewOfResa.view[Meteor.userId()] < reservation.validateAt) {
                nb_reject++
              } else {
                // Do Nothing
              }
            } else {
              nb_reject++
            }
          }
        }
      }
    }
  }
  self.changed('residentBadges', condoId, { reservation: nb_validated, reservation_reject: nb_reject });
}

function module_pending_user_getSelector(condoId, test = false) {
  let occupants = null
  const condosOptions = CondosModulesOptions.findOne({ condoId: condoId })
  const thisResident = Residents.findOne({ userId: Meteor.userId() })
  const thisResidentCondo = thisResident.condos.find(condo => {
    return condo.condoId === condoId
  })

  if (condosOptions && condosOptions.profile && condosOptions.profile.company === true) {
    const companyId = thisResidentCondo.userInfo ? (thisResidentCondo.userInfo.company || null) : null
    occupants = Residents.find({
      '_id': { $ne: Meteor.userId() },
      'pendings': {
        $elemMatch: {
          'condoId': condoId,
          'userInfo.company': companyId
        }
      },
      'validation.type': 'gestionnaire'
    })
  } else {
    occupants = Residents.find({
      '_id': { $ne: Meteor.userId() },
      'pendings.condoId': condoId,
      'validation.type': 'gestionnaire'
    })
  }

  return occupants

  // return Residents.find(({ 'pendings.0': { $exists: true }, 'pendings.condoId': condoId, 'validation.type': 'gestionnaire' }))
}

function module_pending_user_updateBadges(condoId, self) {
  const canSeeOccupantList = Meteor.userHasRight('trombi', 'seeOccupantList', condoId, self.userId)

  if (!canSeeOccupantList) {
    return false
  }

  let nb_pending = module_pending_user_getSelector(condoId).count()
  self.changed('residentBadges', condoId, { newUser: nb_pending });
}
