Meteor.publish('gestionnaire_badges', function () {
  this.unblock()
  if (!this.userId) {
    this.stop()
    throw new Meteor.Error(300, "Not Authenticated");
  }
  let gestionnnaire = Enterprises.findOne({'users.userId': Meteor.userId()});
  if (!gestionnaire) {
    this.stop()
    throw new Meteor.Error(302, "Access Denied for this condo");
  }
  let gUser = !!gestionnnaire && _.find(gestionnnaire.users, function(gU) { return gU.userId == Meteor.userId()});

  if (!gUser) {
    this.stop()
    throw new Meteor.Error(300, "Gestionnaire not found");
  }
  let condoIds = _.map(gUser.condosInCharge, function(c) {return c.condoId})

  let self = this;
  let userId = this.userId;
  let badgesModules = {
    manual: 0,
    map: 0,
    messenger: 0,
    messenger_enterprise: 0,
    messenger_personnal: 0,
    newUser: 0,
    incident: 0,
    forum: 0,
    myforum: 0,
    ads: 0,
    myads: 0,
    reservation_pending: 0,
    reservation_validated: 0
  };

  const handlers = []
  _.each(condoIds, function(condoId) {
    let condo = Condos.findOne(condoId);
    if (!condo) return false
    self.added('gestionnaireBadges', condoId, badgesModules);
    // return // TO REMOVE HERE FOR TESTING !!

    // V-- It's for Map badges

    let mapSelector = module_map_getSelector(condoId);
    handlers.push(mapSelector.observeChanges({
      changed: function () {
        module_map_updateBadges(condoId, userId, "changed", self);
      },
      added: function () {
        module_map_updateBadges(condoId, userId, "changed", self);
      }
    }))

    // V-- It's for Manual badges

    let manualSelector = module_manual_getSelector(condoId);
    handlers.push(manualSelector.observeChanges({
      changed: function () {
        module_manual_updateBadges(condoId, userId, "changed", self);
      },
      added: function () {
        module_manual_updateBadges(condoId, userId, "changed", self);
      }
    }))

    let badgesNames = ["newUser"];
    if (condo.settings.options.EDL)
    badgesNames.push('edl');
    if (condo.settings.options.classifieds)
    badgesNames.push('classifieds');
    if (condo.settings.options.forum)
    badgesNames.push('forum');
    if (condo.settings.options.incidents)
    badgesNames.push('incident');
    if (condo.settings.options.informations)
    badgesNames.push('actuality');

    _.each(condo.modules, (m) => {

      if (_.find(badgesNames, (b) => {return b === m.name;}) === undefined)
      return
      let funcNameSelector = "module_" + m.name + "_getSelector";
      let funcNameBadges = "module_" + m.name + "_updateBadges";
      let moduleId = m.data[m.name + "Id"];
      let selector = eval(funcNameSelector)(condoId, userId, moduleId);
      handlers.push(selector.observeChanges({
        removed: function() {
          eval(funcNameBadges)(condoId, userId, moduleId, "changed", self);
        },
        changed: function() {
          eval(funcNameBadges)(condoId, userId, moduleId, "changed", self);
        },
        added: function() {
          eval(funcNameBadges)(condoId, userId, moduleId, "changed", self);
        }
      }))
    });

    let funcNameSelectorMessenger = "module_messenger_getSelector";
    let funcNameBadgesMessenger = "module_messenger_updateBadges";
    let selectorMessenger = eval(funcNameSelectorMessenger)(condoId, userId, null);
    handlers.push(selectorMessenger.observeChanges({
      removed: function () {
        eval(funcNameBadgesMessenger)(condoId, userId, null, "changed", self);
      },
      changed: function () {
        eval(funcNameBadgesMessenger)(condoId, userId, null, "changed", self);
      },
      added: function () {
        eval(funcNameBadgesMessenger)(condoId, userId, null, "changed", self);
      }
    }))

    let funcNameSelectorNewUser = "module_newUser_getSelector";
    let funcNameBadgesNewUser = "module_newUser_updateBadges";
    let selectorNewUser = eval(funcNameSelectorNewUser)(condoId, userId, null);
    handlers.push(selectorNewUser.observeChanges({
      removed: function () {
        eval(funcNameBadgesNewUser)(condoId, userId, null, "changed", self);
      },
      changed: function () {
        eval(funcNameBadgesNewUser)(condoId, userId, null, "changed", self);
      },
      added: function () {
        eval(funcNameBadgesNewUser)(condoId, userId, null, "changed", self);
      }
    }))

    let funcNameSelectorResa = "module_reservation_getSelector";
    let funcNameBadgesResa = "module_reservation_updateBadges";
    let selectorResa = eval(funcNameSelectorResa)(condoId, userId, null);
    handlers.push(selectorResa.observeChanges({
      removed: function () {
        eval(funcNameBadgesResa)(condoId, userId, null, "changed", self);
      },
      changed: function () {
        eval(funcNameBadgesResa)(condoId, userId, null, "changed", self);
      },
      added: function () {
        eval(funcNameBadgesResa)(condoId, userId, null, "changed", self);
      }
    }))
  });
  this.onStop(() => {
    handlers.forEach(handler => {
      handler.stop()
    })
    return true
  })
  this.ready();

  function module_messenger_getSelector(condoId, userId, moduleId){
    return Messages.find({
      "target": "manager",
      "isArchived": { $ne: true },
      "condoId": condoId
    });
  }

  function module_messenger_updateBadges(condoId, userId, moduleId, action, self) {
    let seeMessenger = Meteor.userHasRight("messenger", "seeAllMsgEnterprise", condoId, this.userId);

    if (!seeMessenger) {
      return false
    }
    let nbPersonnal = 0;
    let nbEnterprise = 0;
    let nb = 0;
    let messages = module_messenger_getSelector(condoId, userId, moduleId).fetch();

    for (const message of messages) {
      if (message && message.lastMsgUserId) {
        if (message.global === true) {
          let user = _.find(message.users, (user) => { return user.userId === self.userId })
          let lastMsgDate = message.lastMsg
          if (user && (!user.lastVisit || user.lastVisit < lastMsgDate)) {
            nbPersonnal++
          }
        } else {
          let thisUser = Meteor.users.findOne(message.lastMsgUserId)
          if (thisUser && !!thisUser.identities.residentId) {
            nbEnterprise++
          }
        }
      }
    }
    nb = nbPersonnal + nbEnterprise
    if (action === "added") {
      self.added('gestionnaireBadges', condoId, { messenger: nb })
      self.added('gestionnaireBadges', condoId, { messenger_personnal: nbPersonnal })
      self.added('gestionnaireBadges', condoId, { messenger_enterprise: nbEnterprise })
    } else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { messenger: nb })
      self.changed('gestionnaireBadges', condoId, { messenger_personnal: nbPersonnal })
      self.changed('gestionnaireBadges', condoId, { messenger_enterprise: nbEnterprise })
    }
  }

  function module_incident_getSelector(condoId, userId, moduleId){
    let query = {$and: [
      {"condoId": condoId},
      {"state.status": {$in: [0, 1]}}
    ]};
    return Incidents.find(query);
  }

  function module_incident_updateBadges(condoId, userId, moduleId, action, self) {
    let seeIncident = Meteor.userHasRight("incident", "see", condoId, this.userId);

    if (!seeIncident) {
      return false
    }
    let incidents = module_incident_getSelector(condoId, userId, moduleId).fetch();
    let nb = incidents.length;
    if (action === "added")
    self.added('gestionnaireBadges', condoId, {incident: nb});
    else if (action === "changed")
    self.changed('gestionnaireBadges', condoId, {incident: nb});
  }

  function module_newUser_getSelector(condoId, userId, moduleId){
    return Residents.find({"pendings.condoId": condoId});
  }

  function module_newUser_updateBadges(condoId, userId, moduleId, action, self){
    let nb = 0;
    let users = module_newUser_getSelector(condoId, userId, moduleId).fetch();

    _.each(users, function(elem) {
      if (elem && elem.validation && elem.validation.type == "gestionnaire")
      nb++;
    });

    if (action === "added")
    self.added('gestionnaireBadges', condoId, {newUser: nb});
    else if (action === "changed")
    self.changed('gestionnaireBadges', condoId, {newUser: nb});
  }

  function module_forum_getSelector(condoId, userId, moduleId){
    return ForumPosts.find({condoId});
  }

  function module_forum_updateBadges(condoId, userId, moduleId, action, self) {
    let seePoll = Meteor.userHasRight("forum", "seePoll", condoId, this.userId);
    let seePost = Meteor.userHasRight("forum", "seeSubject", condoId, this.userId);

    if (!seePoll && !seePost) {
      return false
    }
    let posts = module_forum_getSelector(condoId, userId, moduleId).fetch();
    let nb = 0;
    let mynb = 0;
    for (let i = 0; i < posts.length; i++) {
      let thisUserView = _.find(posts[i].views, (date, viewUserId) => {
        if (viewUserId === self.userId) {
          return true
        }
      })
      if ((!!thisUserView === false || (thisUserView < posts[i].updatedAt))) {
        if ((posts[i].type === 'poll' && seePoll) || (posts[i].type === 'post' && seePost)) {
          if (posts[i].userId === userId) {
            mynb++
          } else if (!!thisUserView === false) {
            nb++
          } else {
            if (posts[i].userId !== userId) {
              let response = ForumCommentPosts.find({
                postId: posts[i]._id,
                userId: userId
              }).fetch()
              if (response && response.length > 0 && thisUserView < posts[i].updatedAt) {
                nb++
              }
            }
          }
        }
      }
    }
    if (action === "added")
    return { forum: nb, myforum: mynb };
    else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { forum: nb });
      self.changed('gestionnaireBadges', condoId, { myforum: mynb });
    }
  }

  function module_classifieds_getSelector(condoId, userId, moduleId){
    return ClassifiedsAds.find({ 'condoId': condoId });
  }

  function module_classifieds_updateBadges(condoId, userId, moduleId, action, self) {
    let seeAds = Meteor.userHasRight("ads", "see", condoId, this.userId);

    if (!seeAds) {
      return false
    }
    let posts = module_classifieds_getSelector(condoId, userId, moduleId).fetch();
    let nb = 0;
    let mynb = 0;
    for (let i = 0; i < posts.length; i++) {
      let view = posts[i].views[self.userId]
      if (!view || (view < posts[i].updatedAt)) {
        if (posts[i].userId === userId) {
          mynb++
        } else {
          if (!view) {
            nb++
          } else {
            let replyOfUser = ClassifiedsFeed.find({
              classifiedId: posts[i]._id,
              userId: userId
            }).fetch()
            if (replyOfUser && replyOfUser.length > 0) {
              nb++
            }
          }
        }
      }
    }
    if (action === "added")
    return { ads: nb, myads: mynb }
    else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { ads: nb });
      self.changed('gestionnaireBadges', condoId, { myads: mynb });
    }
  }

  function module_actuality_getSelector(condoId, userId, moduleId) {
    return ActuPosts.find({
      actualityId: moduleId
    });
  }

  function module_actuality_updateBadges(condoId, userId, moduleId, action, self) {
    let seeActu = Meteor.userHasRight("actuality", "see", condoId, this.userId);

    if (!seeActu) {
      return false
    }
    let posts = module_actuality_getSelector(condoId, userId, moduleId).fetch();
    let information = 0;
    let event = 0;
    for (let i = 0; i < posts.length; i++) {
      let view = _.find(posts[i].views, (e) => {
        return e === Meteor.userId();
      });
      if (view === undefined) {
        if (posts[i].startDate === '') {
          information++;
        } else {
          event++;
        }
      }
    }
    if (action === "added") {
      self.added('gestionnaireBadges', condoId, { information });
      self.added('gestionnaireBadges', condoId, { event });
    }
    else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { information });
      self.changed('gestionnaireBadges', condoId, { event });
    }
  }

  function module_manual_getSelector(condoId) {
    return ViewCondoDocument.find({ 'condoId': condoId, 'type': 'manual' });
  }
  function module_manual_updateBadges(condoId, userId, action, self) {
    let hasManualNotif = 0

    let seeManual = Meteor.userHasRight("manual", "see", condoId, userId);
    if (!seeManual) {
      return false
    }
    let condoManual = module_manual_getSelector(condoId).fetch();

    if (condoManual) {
      condoManual = condoManual[0]
    } else {
      return
    }
    if (condoManual && seeManual) {
      let view = condoManual.views
      if (view && view[userId]) {
        let condo = Condos.findOne({ '_id': condoId })
        if (condo && condo.manual) {
          let manualOfCondo = condo.manual
          hasManualNotif = view[userId] < manualOfCondo.manualUpdate ? 1 : 0
        }
      } else {
        hasManualNotif = 1
      }
    } else {
      hasManualNotif = 0
    }

    if (action === "added") {
      return hasManualNotif
    } else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { manual: hasManualNotif });
    }
  }

  function module_map_getSelector(condoId) {
    return ViewCondoDocument.find({ 'condoId': condoId, 'type': 'map' });
  }
  function module_map_updateBadges(condoId, userId, action, self) {
    let hasMapNotif = 0

    let seeMap = Meteor.userHasRight("map", "see", condoId, userId);
    if (!seeMap) {
      return false
    }
    let condoMap = module_map_getSelector(condoId).fetch();
    if (condoMap) {
      condoMap = condoMap[0]
    } else {
      return
    }
    if (condoMap && seeMap) {
      let view = condoMap.views
      if (view && view[userId]) {
        let condo = Condos.findOne({ '_id': condoId })
        if (condo && condo.map) {
          let mapOfCondo = condo.map
          hasMapNotif = view[userId] < mapOfCondo.mapUpdate ? 1 : 0
        }
      } else {
        hasMapNotif = 1
      }
    } else {
      hasMapNotif = 0
    }

    if (action === "added") {
      return hasMapNotif;
    } else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { map: hasMapNotif });
    }
  }

  function module_reservation_getSelector(condoId, userId, moduleId) {
    return Reservations.find({
      start: { $gte: +moment().format('x') },
      condoId: condoId
    })
  }

  function module_reservation_updateBadges(condoId, userId, moduleId, action, self) {
    let nb_pending = 0;
    let nb_validated = 0;
    let reservations = module_reservation_getSelector(condoId, userId, moduleId).fetch();
    let resourceIds = _.map(reservations, (r) => r.resourceId);
    let resources = Resources.find({ _id: { $in: resourceIds } }).fetch();
    for (let i = 0; i < reservations.length; i++) {
      const reservation = reservations[i];
      let resourceOfResa = resources.find((r) => r._id === reservation.resourceId);
      if (!!resourceOfResa) {
        if (resourceOfResa.needValidation) {
          if (reservation.pending === true) {
            if (resourceOfResa.needValidation.status === true) {
              let { managerIds } = resourceOfResa.needValidation
              if (managerIds && managerIds.find((managerId) => managerId === Meteor.userId())) {
                nb_pending++
              } else {
                // Do Nothing
              }
            } else {
              nb_pending++
            }
          } else if (reservation.pending === false && reservation.rejected === false) {
            let viewOfResa = ViewOfReservation.findOne({ _id: reservation.eventId })
            if (viewOfResa) {
              if (viewOfResa.view && viewOfResa.view[Meteor.userId()]) {
                if (viewOfResa.view[Meteor.userId()] < reservation.validateAt) {
                  nb_validated++
                } else {
                  // Do Nothing
                }
              } else {
                nb_validated++
              }
            }
          }
        }
      }
    }
    if (action === "added") {
      self.added('gestionnaireBadges', condoId, { reservation_pending: nb_pending, reservation_validated: nb_validated });
    }
    else if (action === "changed") {
      self.changed('gestionnaireBadges', condoId, { reservation_pending: nb_pending, reservation_validated: nb_validated });
    }
  }
});
