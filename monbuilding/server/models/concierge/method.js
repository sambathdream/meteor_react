import {Email} from './../../../common/lang/lang';

Meteor.startup(function() {
	Meteor.methods({
		/*  CREATE A NEW MESSAGE FROM RESIDENT TO KEEPER AND TRIGGER EMAIL SEND TO OTHER PARTICIPANTS
		data                  (Object) CONTAINS DATA FOR THE CREATION
		{
		condoId           (Striing) ID OF THE CONDO RELATED TO THE CONVERSATION
		message           (String) TEXT OF MESSAGE
		files             (Table of String) FILES ATTACHED TO THE MESSAGE
		param1            (String) value of first dropdown
		param2            (String) value of second dropdown
		param3            (String) value of third dropdown
		}
		*/
		addMessageConcierge: function(data) {
			if (Meteor.userId()) {
				let msgId = ConciergeMessage.insert({
					date: Date.now(),
					condoId: data.condoId,
					origin: this.userId,
					param1: data.param1,
					param2: data.param2,
					param3: data.param3,
					category: data.category,
					content: [{text: data.message, files: data.files}],
				});
				var translation = new Email(Meteor.user().profile.lang);

				let condoAddress = Condos.findOne(data.condoId).info;
				condoAddress = condoAddress.address + ", " + condoAddress.code + " " + condoAddress.city;

				let user = Meteor.user();
				user = user.profile.firstname + " " + user.profile.lastname;

				let params = data.param1 + ", " + data.param2;
				if (data.param3 && data.param3 != "")
					params += ", " + data.param3;

				Meteor.call('sendEmailSync',
					'bonjour@monbuilding.com',
					data.condoId,
					'Demande depuis la conciergerie', // NE PAS TRADUIRE !!
					'email-concierge',
					{
						condoAddress: condoAddress,
						message: data.message,
						category: data.category,
						params: params,
						userName: user
					}
				);
			}
			else
				throw new Meteor.Error(404, "Invalid user", "Not logged");
		},


	});
});
