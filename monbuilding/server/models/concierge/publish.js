Meteor.startup(
	function () {
		// PUBLISH ALL MESSAGES OF RESIDENT ONLY CONCERNING CONNECTED USER (CONNECTED USER AS PARTICIPANT)
		Meteor.publish('conciergeMessage', function() {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			return [ConciergeMessage.find({
				origin: this.userId
			})];
		});

		Meteor.publish('conciergeMessageGestionnnaire', function(condoId) {
			if (!this.userId)
				throw new Meteor.Error(300, "Not Authenticated");
			let condoIds = Meteor.call('gestionnaireCondosInCharge', Meteor.user());
			return [ConciergeMessage.find({condoId: {$in: condoIds}})];
		});

	}
);