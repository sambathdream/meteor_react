Meteor.startup(
    function () {
        /*
         PUBLISH ALL PUSH NOTIFICATION OF SPECIFIC USER
         */
        Meteor.publish('pushNotificationSub', function () {
            if (!this.userId) {
                throw new Meteor.Error(300, "Not Authenticated");
            }
            return PushNotificationsId.find({'_id': this.userId})
        });
    }
);
