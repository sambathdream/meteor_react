function userHasPlayerId (user, playerId) {
  return !!user.playerIds.find(did => did.playerId === playerId);
}

function deviceNeedsUpdate(user, playerId) {
  return !!user.playerIds.find(did => did === playerId);
}

function unregisterDevideFromUserId (userId, playerId) {
  const user = PushNotificationsId.findOne({_id: userId});
  unregisterDeviceFromUser(user, playerId);
}

function unregisterDeviceFromUser (user, playerId) {
  const didList = user.playerIds.filter(did => did !== playerId && did.playerId !== playerId);
  PushNotificationsId.update({ _id: user._id }, {
    $set: {
      playerIds: didList
    }
  });
}

function unregisterDevice (playerId) {
  const user = PushNotificationsId.findOne({
    $or: [
      {'playerIds.playerId': playerId},
      {'playerIds': playerId},
    ]
  })
  if (user) {
    unregisterDeviceFromUser(user, playerId);
  }
  return;
}

Meteor.startup(function () {
  Meteor.methods({
    registerUserPushNotification: function (playerId, client) {
      const userId = Meteor.userId();
      if (!userId) {
        unregisterDevice(playerId);
        return;
      }

      const user = PushNotificationsId.findOne({_id: userId});

      if (!user) {
        unregisterDevice(playerId);
        PushNotificationsId.insert({ _id: userId, playerIds: [{playerId, client}] });
        return;
      }

      if (userHasPlayerId(user, playerId)) {
        return;
      }

      if (deviceNeedsUpdate(user, playerId)) {
        user.playerIds = user.playerIds.filter(d => d !== playerId);
      } else {
        unregisterDevice(playerId);
      }

      user.playerIds.push({playerId, client});

      PushNotificationsId.update({ _id: userId }, {
        $set: {
          playerIds: user.playerIds
        }
      });

      function getArrrayIdsOf(collection, query = {}, params) {
        return _.map(collection.find(query).fetch(), (data = null) => {
          return data && !!data[params] ? data[params] : undefined
        })
      }

      let condoIds = []
      let defaultRoleIds = []
      let enterpriseIds = []
      let resident = Residents.findOne({ 'userId': Meteor.userId() })

      if (resident) {
        condoIds = _.map(resident.condos, (c) => {
            return c.condoId
        })
        defaultRoleIds = getArrrayIdsOf(UsersRights, { 'userId': Meteor.userId() }, 'defaultRoleId')
        enterpriseIds = getArrrayIdsOf(Enterprises, { 'condos': { $in: condoIds } }, '_id')
        condoIds = _.uniq(condoIds)
        defaultRoleIds = _.uniq(defaultRoleIds)
        enterpriseIds = _.uniq(enterpriseIds)
      } else {
        let enterprise = Enterprises.findOne({ 'users.userId': Meteor.userId() })

        if (enterprise) {
          let user = enterprise.users.find((u) => {
              return u.userId === Meteor.userId()
          })
          condoIds = _.map(user.condosInCharge, (c) => {
              return c.condoId
          })
          defaultRoleIds = getArrrayIdsOf(UsersRights, { 'userId': Meteor.userId() }, 'defaultRoleId')
          enterpriseIds = [enterprise._id]
          condoIds = _.uniq(condoIds)
          defaultRoleIds = _.uniq(defaultRoleIds)
          enterpriseIds = _.uniq(enterpriseIds)
          }
      }

      let existingTags = {}
      let newTags = _.filter([...condoIds, ...defaultRoleIds, ...enterpriseIds], (tag) => {
        return tag !== undefined
      })
      // This is done from mobile
      //   OneSignal.viewDevice(playerId, function (err, httpResponse, data) {
      //     try {
      //       let deviceBody = {}
      //       let existingTags = !!data ? JSON.parse(data).tags : {}
      //       let allTags = {}

      //       if (existingTags) {
      //         for (var i = 0; i < Object.keys(existingTags).length; i++) {
      //           var oldTag = Object.keys(existingTags)[i]
      //           allTags[oldTag] = ''
      //         }
      //       }

      //       for (var i = 0; i < newTags.length; i++) {
      //         var tag = newTags[i]
      //         allTags[tag] = true
      //       }

      //       deviceBody.tags = allTags

      //       OneSignal.editDevice(playerId, deviceBody, function (err, httpResponse, data) {
      //         if (data.success === true) {
      //           OneSignal.viewDevice(playerId, function (err, httpResponse, data) {
      //             console.log('Success edit PlayerId: ', playerId)
      //             return true
      //           })
      //         } else {
      //           console.log('Fail to editDevice.', err)
      //           return false
      //         }
      //       })
      //   } catch (error) {
      //     console.log('Fail to viewDevice.', err)
      //     return false
      //   }
      // })
  },
  unregisterUserPushNotification: function (playerId) {
    unregisterDevice(playerId);
    // This is done from mobile
    // if (this.userId) {
    //   OneSignal.viewDevice(playerId, function (err, httpResponse, data) {
    //     try {
    //       let deviceBody = {}
    //       let existingTags = !!data ? JSON.parse(data).tags : {}
    //       let tmpTags = {}

    //       if (existingTags) {
    //         for (var i = 0; i < Object.keys(existingTags).length; i++) {
    //           var oldTag = Object.keys(existingTags)[i]
    //           tmpTags[oldTag] = ''
    //         }
    //       }
    //       deviceBody.tags = tmpTags

    //       OneSignal.editDevice(playerId, deviceBody, function (err, httpResponse, data) {
    //         if (data.success === true) {
    //           OneSignal.viewDevice(playerId, function (err, httpResponse, data) {
    //             console.log('Success edit PlayerId: ', playerId)
    //             return true
    //           })
    //         } else {
    //           console.log('Fail to editDevice.', data)
    //           return false
    //         }
    //       })
    //     } catch (error) {
    //       console.log('Fail to viewDevice.', error)
    //       return false
    //     }
    //   })
    // }
  },
  togglePushInformation: function (condoId) {
    togleNotification('actuality', condoId, this.userId);
  },
  togglePushIncident: function (condoId) {
    togleNotification('incident', condoId, this.userId);
  },
  togglePushForum: function (condoId) {
    togleNotification('forum', condoId, this.userId);
  },
  togglePushAds: function (condoId) {
    togleNotification('ads', condoId, this.userId);
  },
  togglePushReservation: function (condoId) {
    togleNotification('reservation', condoId, this.userId);
  },
  togglePushMessenger: function (condoId) {
    togleNotification('messenger', condoId, this.userId);
  },
})

function togleNotification(notification, condoId, userId) {
  if (!userId) {
    throw new Meteor.Error(300, 'Not Authenticated');
  }
  let pushNotification = PushNotificationsId.findOne({'_id': userId});
  const field = `notification.${condoId}.${notification}`
  if (pushNotification) {
    let notif = pushNotification.notification
    if (notif && notif[condoId]) {
      PushNotificationsId.update({
        '_id': userId,
      }, {
        $set: {
          [field]: !notif[condoId][notification]
        }
      });
    } else {
      PushNotificationsId.update({
        '_id': userId,
      }, {
        $set: {
          [field]: false
        }
      })
    }
  } else {
    return false
  }
}


})
