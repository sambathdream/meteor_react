import {Meteor} from 'meteor/meteor';
import {FilesCollection} from 'meteor/ostrio:files';
import { MarketPlaceFiles, BasketFiles } from '/common/userFiles'

if (Meteor.isServer) {
	const _multer = require('multer');
	const _fs = require('fs');
	const _multerInstanceConfig = {dest: '/tmp'}; // Temp dir for multer
	const _multerInstance = _multer(_multerInstanceConfig);

	Picker.middleware(_multerInstance.single('photo'));

	Picker.route('/api/v1/upload', function (params, req, res, next) {
		if (!!req.file && !!params.query.userId) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			const user = Meteor.users.findOne({'_id': params.query.userId});
			let collection = UserFiles
			if (user) {
        console.log('User exists')
        console.log('req.file', req.file)
				var _addFileMeta = {};
				_fs.stat(req.file.path, function (_statError, _statData) {
					if (params.query.forumId) {
						console.log("Uploading forum ", params.query.forumId)
						_addFileMeta = {
							fileName: req.file.originalname,
							type: req.file.mimetype,
							size: req.file.size,
							meta: {
								forumId: params.query.forumId,
								userId: user._id
							}
						};
					}
					else if (params.query.actualityId) {
						console.log("Uploading actuality ", params.query.actualityId)
						_addFileMeta = {
							fileName: req.file.originalname,
							type: req.file.mimetype,
							size: req.file.size,
							meta: {
								actualityId: params.query.actualityId,
								userId: user._id
							}
						};
					}
          else if (params.query.classifiedsId) {
            console.log("Uploading classifieds ", params.query.classifiedsId)
            _addFileMeta = {
              fileName: req.file.originalname,
              type: req.file.mimetype,
              size: req.file.size,
              meta: {
                classifiedsId: params.query.classifiedsId,
                userId: user._id
              }
            };
          }
          else if (params.query.eventId) {
            console.log("Uploading event comment ", params.query.eventId)
            _addFileMeta = {
              fileName: req.file.originalname,
              type: req.file.mimetype,
              size: req.file.size,
              meta: {
                eventId: params.query.eventId,
                userId: user._id
              }
            };
          }
					else if (params.query.msgId) {
						console.log("Uploading msg ", params.query.msgId)
						let condoId = params.query.condoId
						collection = MessengerFiles
						if (!condoId) {
							condoId = Messages.findOne({ _id: params.query.msgId }).condoId
						}
						_addFileMeta = {
							fileName: req.file.originalname,
							type: req.file.mimetype,
							size: req.file.size,
							meta: {
								msgId: params.query.msgId,
								condoId: condoId,
								userId: user._id
							}
						};
					}
					else if (params.query.contactId) {
            console.log("Uploading emergency contact ", params.query.contactId)
            collection = EmergencyContactPhotoFiles
            _addFileMeta = {
              fileName: req.file.originalname,
              type: req.file.mimetype,
              size: req.file.size,
              meta: {
                uploader: user._id,
                contactId: params.query.contactId
              }
            };
          }
					else {
						console.log("Uploading else ")
						_addFileMeta = {
							fileName: req.file.originalname,
							type: req.file.mimetype,
							size: req.file.size,
							meta: {
								userId: user._id,
								publicPicture: true
							}
						};
					}

					console.log('_addFileMeta', _addFileMeta);
					_fs.readFile(req.file.path, function (_readError, _readData) {
						if (_readError) {
							console.log(_readError);
							res.writeHead(400, {"Content-Type": "application/json"});
							var json = JSON.stringify(_readError);
							res.end(json);
						} else {
							collection.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
								if (_uploadError) {
									console.log(_uploadError);
									res.writeHead(400, {"Content-Type": "application/json"});
									var json = JSON.stringify(_uploadError);
									res.end(json);
								} else {
									console.log('upload data=', _uploadData);
									_fs.unlink(req.file.path); // remove temp upload
									res.writeHead(200, {"Content-Type": "application/json"});
									var json = JSON.stringify(_uploadData);
									res.end(json);
								}
							}, true);
						}
					});
				});
			}
			else {
				res.writeHead(400, {"Content-Type": "text/plain"});
				res.end('User not found');
			}
		}
		else {
			res.writeHead(400, {"Content-Type": "text/plain"});
			res.end('File not found');
		}
	});

	Picker.route('/api/v1/uploadIncident', function (params, req, res, next) {
		if (!!req.file && !!params.query.userId) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			const user = Meteor.users.findOne({'_id': params.query.userId});
			if (user) {
				var _addFileMeta = {};
				_fs.stat(req.file.path, function (_statError, _statData) {
					_addFileMeta = {
						fileName: req.file.originalname,
						type: req.file.mimetype,
						size: req.file.size,
						meta: {
							userId: user._id
						}
					};

					_fs.readFile(req.file.path, function (_readError, _readData) {
						if (_readError) {
							console.log(_readError);
							res.writeHead(400, {"Content-Type": "application/json"});
							var json = JSON.stringify(_readError);
							res.end(json);
						} else {
							IncidentFiles.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
								if (_uploadError) {
									console.log(_uploadError);
									res.writeHead(400, {"Content-Type": "application/json"});
									var json = JSON.stringify(_uploadError);
									res.end(json);
								} else {
									console.log('upload data=', _uploadData);
									_fs.unlink(req.file.path); // remove temp upload
									res.writeHead(200, {"Content-Type": "application/json"});
									var json = JSON.stringify(_uploadData);
									res.end(json);
								}
							}, true);
						}
					});
				});
			}
			else {
				res.writeHead(400, {"Content-Type": "text/plain"});
				res.end('User not found');
			}
		}
		else {
			res.writeHead(400, {"Content-Type": "text/plain"});
			res.end('File not found');
		}
	});

	Picker.route('/api/v1/uploadJustif', function (params, req, res, next) {
		if (!!req.file) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			var _addFileMeta = {};
			_fs.stat(req.file.path, function (_statError, _statData) {
				_addFileMeta = {
					fileName: req.file.originalname,
					type: req.file.mimetype,
					size: req.file.size,
					meta: {
					}
				};

				_fs.readFile(req.file.path, function (_readError, _readData) {
					if (_readError) {
						console.log(_readError);
						res.writeHead(400, {"Content-Type": "application/json"});
						var json = JSON.stringify(_readError);
						res.end(json);
					} else {
						UsersJustifFiles.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
							if (_uploadError) {
								console.log(_uploadError);
								res.writeHead(400, {"Content-Type": "application/json"});
								var json = JSON.stringify(_uploadError);
								res.end(json);
							} else {
								console.log('upload data=', _uploadData);
								_fs.unlink(req.file.path); // remove temp upload
								res.writeHead(200, {"Content-Type": "application/json"});
								var json = JSON.stringify(_uploadData);
								res.end(json);
							}
						}, true);
					}
				});
			});
		}
		else {
			res.writeHead(400, {"Content-Type": "text/plain"});
			res.end('File not found');
		}
	});

	Picker.route('/api/v1/uploadForum', function (params, req, res, next) {
		if (!!req.file && !!params.query.userId) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			const user = Meteor.users.findOne({ '_id': params.query.userId });
			if (user) {
				var _addFileMeta = {};
				_fs.stat(req.file.path, function (_statError, _statData) {
					_addFileMeta = {
						fileName: req.file.originalname,
						type: req.file.mimetype,
						size: req.file.size,
						meta: {
							userId: user._id,
							condoId: params.query.condoId
						}
					};

					_fs.readFile(req.file.path, function (_readError, _readData) {
						if (_readError) {
							console.log(_readError);
							res.writeHead(400, { "Content-Type": "application/json" });
							var json = JSON.stringify(_readError);
							res.end(json);
						} else {
							ForumFiles.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
								if (_uploadError) {
									console.log(_uploadError);
									res.writeHead(400, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadError);
									res.end(json);
								} else {
									console.log('upload data=', _uploadData);
									_fs.unlink(req.file.path); // remove temp upload
									res.writeHead(200, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadData);
									res.end(json);
								}
							}, true);
						}
					});
				});
			}
			else {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('User not found');
			}
		}
		else {
			res.writeHead(400, { "Content-Type": "text/plain" });
			res.end('File not found');
		}
	});

  Picker.route('/api/v1/uploadBasket', function (params, req, res, next) {
		if (!!req.file && !!params.query.userId) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			const user = Meteor.users.findOne({ '_id': params.query.userId });
			if (user) {
				var _addFileMeta = {};
				_fs.stat(req.file.path, function (_statError, _statData) {
					_addFileMeta = {
						fileName: req.file.originalname,
						type: req.file.mimetype,
						size: req.file.size,
						meta: {
							userId: user._id,
							condoId: params.query.condoId
						}
					};

					_fs.readFile(req.file.path, function (_readError, _readData) {
						if (_readError) {
							console.log(_readError);
							res.writeHead(400, { "Content-Type": "application/json" });
							var json = JSON.stringify(_readError);
							res.end(json);
						} else {
              BasketFiles.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
								if (_uploadError) {
									console.log(_uploadError);
									res.writeHead(400, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadError);
									res.end(json);
								} else {
									console.log('upload data=', _uploadData);
									_fs.unlink(req.file.path); // remove temp upload
									res.writeHead(200, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadData);
									res.end(json);
								}
							}, true);
						}
					});
				});
			}
			else {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('User not found');
			}
		}
		else {
			res.writeHead(400, { "Content-Type": "text/plain" });
			res.end('File not found');
		}
	});

	Picker.route('/api/v1/uploadMessenger', function (params, req, res, next) {
		if (!!req.file && !!params.query.userId) {
			if (req.file.mimetype === 'image/svg+xml') {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('SVG_Files_not_alowed');
				return 'SVG_Files_not_alowed'
			}
			const user = Meteor.users.findOne({ '_id': params.query.userId });
			if (user) {
				var _addFileMeta = {};
				_fs.stat(req.file.path, function (_statError, _statData) {
					_addFileMeta = {
						fileName: req.file.originalname,
						type: req.file.mimetype,
						size: req.file.size,
						meta: {
							userId: user._id,
							msgId: params.query.msgId || null,
							condoId: params.query.condoId || null
						}
					};

					_fs.readFile(req.file.path, function (_readError, _readData) {
						if (_readError) {
							console.log(_readError);
							res.writeHead(400, { "Content-Type": "application/json" });
							var json = JSON.stringify(_readError);
							res.end(json);
						} else {
							MessengerFiles.write(_readData, _addFileMeta, function (_uploadError, _uploadData) {
								if (_uploadError) {
									console.log(_uploadError);
									res.writeHead(400, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadError);
									res.end(json);
								} else {
									console.log('upload data=', _uploadData);
									_fs.unlink(req.file.path); // remove temp upload
									res.writeHead(200, { "Content-Type": "application/json" });
									var json = JSON.stringify(_uploadData);
									res.end(json);
								}
							}, true);
						}
					});
				});
			}
			else {
				res.writeHead(400, { "Content-Type": "text/plain" });
				res.end('User not found');
			}
		}
		else {
			res.writeHead(400, { "Content-Type": "text/plain" });
			res.end('File not found');
		}
	});
}
