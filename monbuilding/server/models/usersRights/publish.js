Meteor.publish('getDefaultRights', function() {
	if (Meteor.userId()) {
		return DefaultRights.find();
	}
});

Meteor.publish('getDefaultRoles', function() {
	if (Meteor.userId()) {
		return DefaultRoles.find();
	}
});

Meteor.publish('getDefaultRolesFor', function(type) {
	if (Meteor.userId()) {
		return DefaultRoles.find({"for": type});
	}
});

Meteor.publish('getCondoRole', function(condoId) {
	if (Meteor.userId()) {
		return CondoRole.find({
			condoId: condoId
		});
	}
});

Meteor.publish('getCondosRole', function() {
	if (Meteor.userId()) {
		let condoIds = [];
		if (Meteor.user()) {
			if (Meteor.user().identities.gestionnaireId) {
				const enterprise = Enterprises.findOne(Meteor.user().identities.gestionnaireId);
        const users = enterprise.users;
        const user = users.find(u => u.userId === Meteor.userId());
        condoIds = user.condosInCharge.map(c => c.condoId);
			} else {
				let resident = Residents.findOne({ '_id': Meteor.user().identities.residentId })
				if (resident) {
					condoIds = _.map(resident.condos, function (condo) {
						return condo.condoId
					})
				}
			}
		}
		return CondoRole.find({
			condoId: {$in: condoIds}
		});
	}
});
