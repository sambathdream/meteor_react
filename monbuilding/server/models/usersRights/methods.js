import { Occupant, ConseilSyndical, ServicesGeneraux, ServicesCourrier, OfficeManager, Gardien } from '/common/export/defaultRolesOccupant.js'
import { AssetManager, PropertyManager, Mainteneur, OperateursRestauration, PCSecurite} from '/common/export/defaultRolesManager.js';

Meteor.startup(function() {
	Meteor.methods({
    createNewDefaultRole: function (rights) {
      if (!Meteor.call("isAdmin", Meteor.userId())) {
        throw new Meteor.Error(300, "Not Authenticated")
      }
      check(rights, Match.ObjectIncluding({
        for: Match.OneOf('manager', 'occupant'),
        name: String,
        rights: [Object]
      }))

      DefaultRoles.insert(rights)
    },
    'ServerScriptUpdateRightsValues': function() {
      let roles = [Occupant, ConseilSyndical, ServicesGeneraux, ServicesCourrier, OfficeManager, Gardien, AssetManager, PropertyManager, Mainteneur, OperateursRestauration, PCSecurite];

      _.each(roles, function (role, index) {
        let thisRole = DefaultRoles.findOne({ for: role.for, name: role.name })
        role.rights.forEach((right, indexRight) => {
          if (!!right.toRemove) {
            delete role.rights[indexRight]
          } else if (!!right) {
            let actualValue = _.find(thisRole.rights, { module: right.module, right: right.right })
            if (actualValue) {
              actualValue = !!actualValue.default
            } else {
              actualValue = !!right.default
            }
            role.rights[indexRight].default = !!(actualValue | false)
          }
        })

        role.rights = _.without(role.rights, undefined, null)
        // thisRole.rights
        UsersRights.update({ 'module.incident.sendMessage': { $exists: true } }, { $unset: { 'module.incident.sendMessage': { sendMessage: true } } }, { multi: true })
        console.log(DefaultRoles.update({ for: role.for, name: role.name }, {
          $set: {
            'rights': role.rights
          }
        }));
      });
    },
    'ServerScriptAddNewRightManagerManagement': function() {
      if (!Meteor.call("isAdmin", this.userId) && !Meteor.fixtures) {
        throw new Meteor.Error(300, "Not Authenticated");
      }
      let defaultRolesManager = DefaultRoles.find({for: 'manager'}).fetch()
      _.each(defaultRolesManager, (defaultRole) => {
        let active = defaultRole.name === "Asset Manager" || defaultRole.name === "Property Manager"
        DefaultRoles.update(defaultRole._id, {
          $push: {
            rights: {
              module: 'managerList',
              right: 'seeManager',
              default: active,
              'value-fr': 'Voir le module de gestion des gestionnaires',
              'value-en': 'Voir le module de gestion des gestionnaires'
            }
          }
        })
        DefaultRoles.update(defaultRole._id, {
          $push: {
            rights: {
              module: 'managerList',
              right: 'addManager',
              default: active,
              'value-fr': 'Inviter un gestionnaire',
              'value-en': 'Inviter un gestionnaire'
            }
          }
        })
        DefaultRoles.update(defaultRole._id, {
          $push: {
            rights: {
              module: 'managerList',
              right: 'deleteCondoManager',
              default: active,
              'value-fr': 'Supprimer l\'immeuble d\'un gestionnaire',
              'value-en': 'Supprimer l\'immeuble d\'un gestionnaire'
            }
          }
        })
      })
    },
		'ServerScriptApplyDefaultRolesToUsersWithoutRole': function () {
			let managers = Meteor.users.find({
				"identities.gestionnaireId": {
					$exists: true
				},
			}, {fields: {_id: true}}).fetch();

			let occupants = Meteor.users.find({
				"identities.residentId": {
					$exists: true
				}
			}, {fields: {_id: true, 'identities.residentId': true}}).fetch();

			let occupantDefaultRole = DefaultRoles.findOne({'for': 'occupant', 'name' : 'Occupant'}, {fields: {_id: true}});
			let managerDefaultRole = DefaultRoles.findOne({'for': 'manager', 'name' : 'Asset Manager'}, {fields: {_id: true}});
			let CSDefaultRole = DefaultRoles.findOne({'for': 'occupant', 'name' : 'Conseil Syndical'}, {fields: {_id: true}});
			let KeeperDefaultRole = DefaultRoles.findOne({'for': 'occupant', 'name' : 'Gardien'}, {fields: {_id: true}});

			_.each(managers, function(manager){
				Meteor.createDefaultRightsForManager(manager._id, managerDefaultRole._id);
			});
			_.each(occupants, function(occupant){
				let resident = Residents.findOne(occupant.identities.residentId);
				_.each(resident.condos, function(condo){
					if (condo.isSyndical) {
						Meteor.createDefaultRightsForUserWithCondo(occupant._id, condo.condoId, CSDefaultRole._id);
					}
					else if (condo.isKeeper) {
						Meteor.createDefaultRightsForUserWithCondo(occupant._id, condo.condoId, KeeperDefaultRole._id);
					}
					else {
						Meteor.createDefaultRightsForUserWithCondo(occupant._id, condo.condoId, occupantDefaultRole._id);
					}
				});
			});
		},
		'ServerScriptCreateDefaultRoles': function() {
			let roles = [Occupant, ConseilSyndical, ServicesGeneraux, ServicesCourrier, OfficeManager, Gardien, AssetManager, PropertyManager, Mainteneur, OperateursRestauration, PCSecurite];

			_.each(roles, function(role){
				console.log(DefaultRoles.upsert({for: role.for, name: role.name}, role));
			});
		},
		'ServerScriptCreateUsersRightsForAllUsers': function() {
			let alreadyExistsRightsUsersId = _.map(UsersRights.find().fetch(), function(rights) {
				return rights.userId;
			});
			let allUsersIds = _.map(Meteor.users.find({"identities.adminId": {$exists: false}}).fetch(), function(user) {
				if (!_.includes(alreadyExistsRightsUsersId, user._id))
					return user._id;
			});
			let newUsersOnly = _.without(allUsersIds, undefined);
			_.each(newUsersOnly, function(userId) {
				let condosToSetAsOccupant = [];
				let condosToSetAsManager = [];
				let resident = Residents.findOne({userId: userId});
				if (resident) {
					_.each(resident.condos, function(condo) {
						condosToSetAsOccupant.push(condo.condoId);
					})
				}
				else {
					let enterprise = Enterprises.findOne({"users.userId": userId});
					if (enterprise) {
						_.each(enterprise.users, function(manager) {
							if (manager.userId == userId) {
								_.each(manager.condosInCharge, function(condo) {
									condosToSetAsManager.push(condo.condoId);
								})
							}
						})
					}
				}
				_.each(condosToSetAsOccupant, function(condoId) {
					Meteor.call("createUsersRightsForOccupant", userId, condoId);
				})
				_.each(condosToSetAsManager, function(condoId) {
					Meteor.call("createUsersRightsForManager", userId, condoId);
				})
			});
    },
    changeRoleCondoTypeOption: function (roleId, forCondoTypeKey, value) {
      check(roleId, String)
      check(forCondoTypeKey, String)

      if (!Meteor.call("isAdmin", this.userId)) {
        throw new Meteor.Error(300, "Not Authenticated")
      }

      return DefaultRoles.update({ _id: roleId }, {
        $set: {
          ['forCondoType.' + forCondoTypeKey]: value
        }
      })
    },
		'createUsersRightsForOccupant': function(userId, condoId) {
			let resources = Resources.find({condoId: condoId}, {fields: {_id: true, name: true}}).fetch();
			let ret = [];
			_.each(resources, function (resource) {
				ret.push({
					resourceId: resource._id,
					name: resource.name,
					book: true
				});
			});
			let module = {
				reservation: {
					resources: ret,
					invit: true,
					seeAll: false,
					reserveOther: true,
					askAdjustment: true,
				},
				incident: {
					see: true,
					sendMessage: true,
					vote: true,
					declare: true,
				},
				messenger: {
					askBadges: true,
					askInvit: true,
					askOther: true,
					writeToResident: true,
					writeToUnionCouncil: true,
					writeToSupervisors: true,
					writeToManager: true,
					edit: true,
					delete: true,
				},
				actuality: {
					see: true,
					write: true,
					delete: true,
				},
				manual: {
					see: true,
					write: true,
					delete: true,
				},
				map: {
					see: true,
					write: true,
					delete: true,
				},
				restaurant: {
					see: true,
					write: true,
					delete: true,
				},
				trombi: {
					see: true,
					sendMessage: true,
				},
				forum: {
					deletePoll: true,
					deleteSubject: true,
					seePoll: true,
					seeSubject: true,
					writePoll: true,
					writeSubject: true
				},
				forumCS: {
					deletePoll: true,
					deleteSubject: true,
					seePoll: true,
					seeSubject: true,
					writePoll: true,
					writeSubject: true
				},
				ads: {
					see: true,
					write: true,
					delete: true,
				},
				view: {
					concierge: true,
				}
			};
			if (UsersRights.findOne({$and: [ {"userId": userId}, {"condoId": condoId} ]}) != undefined) {
				return -1;
			}
			else {
				UsersRights.insert({
					"condoId": condoId,
					"userId": userId,
					"module": module,
				})
			}
		},
		'createUsersRightsForManager': function(userId, condoId) {
			let resources = Resources.find({condoId: condoId}, {fields: {_id: true, name: true}}).fetch();
			let ret = [];
			_.each(resources, function (resource) {
				ret.push({
					resourceId: resource._id,
					name: resource.name,
					valid: true,
					book: true //[NEXT_STEP] TO UPDATE WITH ONLY THE RESOURCES THAT HAVE TO BE VALIDATED
				});
			});
			let module = {
				reservation: {
					seeAll: true,
					resources: ret,
					invit: true,
					reserveOther: true,
					seeResources: true,
					addResources: true,
					editLimit: true,
					seeReservation: []
				},
				incident: {
					see: true,
					valid: true,
					sendMessage: true,
					autodeclare: true,
					insertQuote: true,
					insertNote: true,
					scheduleIntervention: true,
					resolve: true,
					setPrivate: true,
				},
				messenger: {
					seeAllMsgEnterprise: true,
					writeAllMsgEnterprise: true,
					edit: true,
					delete: true,
				},
				actuality: {
					see: true,
					write: true,
					delete: true,
				},
				manual: {
					see: true,
					write: true,
					delete: true,
				},
				map: {
					see: true,
					write: true,
					delete: true,
				},
				restaurant: {
					see: true,
					write: true,
					delete: true,
				},
				trombi: {
					see: true,
					addOccupant: true,
					delete: true,
          seeManager: false,
          addManager: false,
          deleteCondoManager: false
				},
				setContact: {
					seeSetContact: true,
					seeMessenger: true,
					writeMessenger: true,
					seeWhosWho: true,
					writeWhosWho: true,
					seeValidation: true,
					writeValidation: true,
				},
				forum: {
					deletePoll: true,
					deleteSubject: true,
					seePoll: true,
					seeSubject: true,
					writePoll: true,
					writeSubject: true
				},
				forumCS: {
					deletePoll: true,
					deleteSubject: true,
					seePoll: true,
					seeSubject: true,
					writePoll: true,
					writeSubject: true
				},
				ads: {
					see: true,
					write: true,
					delete: true,
				},
				view: {
					concierge: true,
					buildingView: true,
					buildingList: true,
					stats: true,
				}
			};
			if (UsersRights.findOne({$and: [ {"userId": userId}, {"condoId": condoId} ]}) != undefined) {
				return -1;
			}
			else {
				UsersRights.insert({
					"condoId": condoId,
					"userId": userId,
					"module": module,
				})
			}
		},
		'modifyDefaultRoleIdForUser': function(userId, condoId, defaultRoleId){
      check(userId, String);
      check(condoId, String);
      check(defaultRoleId, String);

			UsersRights.upsert({
				"condoId": condoId,
				"userId": userId,
			},
			{
				$set: {
					"defaultRoleId": defaultRoleId
				}
			});
		},
		'modifyOneRight': function(userId, condoId, defaultRoleId, moduleName, moduleType, value) {
			let userRights = UsersRights.findOne({$and: [ {"userId": userId}, {"condoId": condoId} ]});

			if (!userRights)
				return false;

			if (!userRights.module)
				userRights.module = {};
			if (!userRights.module[moduleName])
				userRights.module[moduleName] = {};
			userRights.module[moduleName][moduleType] = value;

			UsersRights.upsert({
				"condoId": condoId,
				"userId": userId,
			},
			{
				$set: {
					"module": userRights.module,
					"defaultRoleId": defaultRoleId
				}
			});
		},
		'modifyResourcesRight': function(userId, condoId, resources) {
			UsersRights.update({
				"condoId": condoId,
				"userId": userId
			},
			{
				$set: {
					"module.reservation.resources": resources,
				}
			});
		},
		'createDefaultRightsForOccupant': function(userId, defaultRoleId) {
			Meteor.createDefaultRightsForOccupant(userId, defaultRoleId);
		},
		'createDefaultRightsForNewOccupant': function(userId) {
			Meteor.createDefaultRightsForNewOccupant(userId);
		},
		'createDefaultRightsForNewOccupantWithCondo': function(userId, condoId) {
			Meteor.createDefaultRightsForNewOccupantWithCondo(userId, condoId);
		},
		'createDefaultRightsForManager': function(userId, defaultRoleId) {
			Meteor.createDefaultRightsForManager(userId, defaultRoleId);
		},
		'createDefaultRightsForUserWithCondo': function(userId, condoId, defaultRoleId) {
			Meteor.createDefaultRightsForUserWithCondo(userId, condoId, defaultRoleId);
		},
		'updateDefaultRole': function(defaultRoleId, module, right, value) {
			let oldRoles = DefaultRoles.findOne(defaultRoleId);

			_.each(oldRoles.rights, function(Right, key){
				if (Right.module == module && Right.right == right) {
					oldRoles.rights[key].default = value;
					return false;
				}
			});

			DefaultRoles.update({
				"_id": defaultRoleId,
			}, {
				$set : {rights: oldRoles.rights}
			});
		},
		'deleteDefaultRole': function(defaultRoleId, module, right) {
			let oldRoles = DefaultRoles.findOne(defaultRoleId);

			_.each(oldRoles.rights, function(Right, key){
				if (Right.module == module && Right.right == right) {
					oldRoles.rights.splice(key, 1);
					return false;
				}
			});

			DefaultRoles.update({
				"_id": defaultRoleId,
			}, {
				$set : {rights: oldRoles.rights}
			});
		},
		'addRightToDefaultRole': function(defaultRoleId, module, right, rightDisplay) {
			DefaultRoles.update(defaultRoleId, {
				$push : {rights: {
					module: module,
					right: right,
					default: null,
					'value-fr': rightDisplay,
					'value-en': rightDisplay
				}}
			})
		},
		'updateCondoRole': function(condoId, For, name, module, right, value) {
			let oldRoles = CondoRole.findOne({
				condoId: condoId,
				for: For,
				name: name,
			});

			let edited = false;
      const defaultRole = DefaultRoles.findOne({ for: For, name: name })
			if (!oldRoles)
				oldRoles = {rights: []};
			_.each(oldRoles.rights, function(Right, key){
				if (Right.module == module && Right.right == right) {
					oldRoles.rights[key].default = value;
					edited = true;
					return false;
				}
			});

			if (!edited) {
				oldRoles.rights.push({
					module: module,
					right: right,
					default: value
				});
			}

			CondoRole.upsert({
				condoId: condoId,
				for: For,
        name: name,
        defaultRoleId: defaultRole._id
			}, {
				$set : {rights: oldRoles.rights}
			});
		},
		'findUserForCondoAndRole': function(condoId, defaultRoleId, module, right) {
			let userRights = UsersRights.find({condoId: condoId, defaultRoleId: defaultRoleId}).fetch();
			let result = [];
			_.each(userRights, function(userRight){
				if (userRight && userRight.module && userRight.module[module] && _.isBoolean(userRight.module[module][right])) {
					result.push(Meteor.users.findOne(userRight.userId));
				}
			});
			return result;
		},
		'removeUserRight': function(userId, condoId, module, right) {
			let userright = UsersRights.findOne({
				condoId: condoId,
				userId: userId
			});

			let modules = userright.module;

			delete modules[module][right];
			_.without(modules[module], null, undefined);

			UsersRights.update({
				condoId: condoId,
				userId: userId
			},
			{
				$set: {
					module: modules
				}
			});
		},
	});
});
