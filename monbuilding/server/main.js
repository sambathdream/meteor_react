import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { WebApp } from 'meteor/webapp';
import Spiderable from 'meteor/ostrio:spiderable-middleware';
// import prerenderio from 'prerender-node';

// var db = require('odbc')()
// const cn = "Driver={HFSQL};" +
//   "Server=46.16.202.39;" +
//   "Port=4900;" +
//   "DataBase=residence_DOMETUDE;" +
//   "Uid=Gestion;" +
//   "Pwd=Gestion@6;"

_ = require('lodash');

Meteor.startup(function() {
  // db.open(cn, function (err) {
  //   if (err) return console.log(err);
  //   db.query('select * from user where user_id = ?', [42], function (err, data) {
  //     if (err) console.log(err);

  //     console.log(data);

  //     db.close(function () {
  //       console.log('done');
  //     });
  //   });
  // });

  // BrowserPolicy.content.allowStyleOrigin('*.bootstrapcdn.com')
	// BrowserPolicy.content.allowStyleOrigin('*.googleapis.com')
	// BrowserPolicy.content.allowStyleOrigin('*.quilljs.com')
	// BrowserPolicy.content.allowScriptOrigin('*.quilljs.com')
	// BrowserPolicy.content.allowScriptOrigin('*.googleapis.com')
	// BrowserPolicy.content.allowScriptOrigin('*.googletagmanager.com')
	// BrowserPolicy.content.allowScriptOrigin('*.google-analytics.com')
	// BrowserPolicy.content.allowImageOrigin('*.google-analytics.com')
	// BrowserPolicy.content.allowImageOrigin('*.doubleclick.net')

	// BrowserPolicy.content.allowImageOrigin('*.tumblr.com')

	// BrowserPolicy.content.allowFontOrigin('*.gstatic.com')
	// BrowserPolicy.content.allowFontOrigin('*.bootstrapcdn.com')

	// BrowserPolicy.content.allowOriginForAll('*.monbuilding.com')
	// BrowserPolicy.content.allowSameOriginForAll('*.monbuilding.com')
	// BrowserPolicy.content.allowConnectOrigin('*.monbuilding.com')
	// BrowserPolicy.content.allowConnectSameOrigin('*.monbuilding.com')
	let users = Meteor.users.find({$and: [
		{"status.online": true},
		{"identities.adminId": {$exists: false}},
		{"identities.gestionnaireId": {$exists: false}},
	]}, {fields: {status: true}});
	thisHandler = users.observe({
		added: function (document) {
			let condoId = Meteor.users.findOne(document._id).lastCondoId;
			if (!condoId || condoId == undefined || Condos.findOne(condoId) === undefined)
				condoId = Residents.findOne({userId: document._id}).condos[0].condoId;
      // console.log("********* added *********");
			Meteor.call('setNewAnalytics', {type: "login", accessType: "web", condoId: condoId, userId: document._id});
		},
		changed: function (newDocument, oldDocument) {
			let status = newDocument.status.online ? (newDocument.status.idle ? 'idle' : 'active') : 'logout';
			// console.log("********* changed *********");
			Meteor.call('updateAnalytics', {type: "login", status: status, accessType: "web", condoId: Meteor.users.findOne(newDocument._id).lastCondoId, userId: newDocument._id});
		},
		removed: function (oldDocument) {
			// console.log("********* removed *********");
			Meteor.call('updateAnalytics', {type: "login", status: "logout", accessType: "web", condoId: Meteor.users.findOne(oldDocument._id).lastCondoId, userId: oldDocument._id});
		},
	});

	Meteor.setInterval(function() {
		IsTyping.remove({date: {$lte: Date.now() - 3000}});
	}, 86400000);


	WebApp.rawConnectHandlers.use(function (req, res, next) {
		res.setHeader('X-XSS-Protection', '1; mode=block');
		return next();
	});

	WebApp.connectHandlers.use(new Spiderable({
		rootURL: 'https://www.monbuilding.com',
		serviceURL: 'https://render.ostr.io',
    auth: 'afw2XokCWTwXFe6Qx:bt_tAwa_U1RAGDEHY3kOBn6dzth3jI0qTC-SLVFfoyk'
	}));


	try {
		process.on('unhandledRejection', r => console.log(r));
	} catch (err) {
		console.log(err)
	}

	const allowedLocales = [
		"fr",
		"en",
		"backoffice",
		"gestionnaire",
		"services",
		"download",
		"error",
		"_oauth",
		"api",
	];
	const locale = allowedLocales[0];
	const localeRe = new RegExp(/^[\w?\.?\:]*\/(\w*)/i);
  const isStaticFileRe = new RegExp(/(.*\..*)$/i);

	WebApp.rawConnectHandlers.use(function(req, res, next) {
    let matchLocale = req.url.match(localeRe);
		let urlSplitted = req.url.split("/");
		let matchSF = urlSplitted[urlSplitted.length - 1].match(isStaticFileRe);
		if (!(matchLocale && matchLocale[1] && _.includes(allowedLocales, matchLocale[1])) && !(matchSF && matchSF[1])) {
			if (req.url == "/")
			{
				res.writeHead(301, {
					'Location': "/" + locale
				});
				res.end();
				return
			}
			else
			{
				res.writeHead(404);
				res.end(`404 File not found!`);
				return
			}
		}

		if (Meteor.isProduction)
		{
			let re = new RegExp(/(.*):(\d*)$/i);
			let match = req.headers["host"].match(re);
			let bannedPorts = [
				"443",  /*Master HTTPS*/
				"3000"  /*localhost*/
			];
			let bannedHosts = [
				"galaxy.monbuilding.com",
				"prod.monbuilding.com",
				"sandbox.monbuilding.com",
				"localhost:3000"
			];

      // console.log('BannedHost: ' + _.includes(bannedHosts, req.headers["host"]) + " | " + req.headers["host"]);
      // console.log('req.headers["x-forwarded-proto"]', req.headers["x-forwarded-proto"])
      // console.log('match', match)

			if ((!req.headers["x-forwarded-proto"] || req.headers["x-forwarded-proto"] != "https")
				&& (!match || (match[2] && !_.includes(bannedPorts, match[2]))) && !_.includes(bannedHosts, req.headers["host"]))
			{
        if (req.headers["host"] === 'develop.monbuilding.com') {
          httpsUrl = "https://" + "develop.monbuilding.com" + req.url;
        } else {
          httpsUrl = "https://" + "www.monbuilding.com" + req.url;
        }
				res.writeHead(301, {
					'Location': httpsUrl
				});
				res.end();
				return
			}
		}
		return next();
	})

	moment.tz.setDefault("UTC");

	ServiceConfiguration.configurations.remove({
		service: "facebook"
	});
	ServiceConfiguration.configurations.insert({
		service: "facebook",
		appId: '1412712598751286',
		secret: 'bc4ebd9f6e14d05cda99999eefff4e44'
	});

	Accounts.onCreateUser(function (options, user) {
		if (!user.services.facebook) {
			return user;
		}
		user.username = user.services.facebook.name;
		user.emails = [{address: user.services.facebook.email, verified: true}];
		user.profile = {
			civilite: (user.services.facebook.gender === "male" ? "Monsieur" : "Madame"),
			lastname: user.services.facebook.last_name,
			firstname: user.services.facebook.first_name,
			state: true,
			role: 'Resident'
		};
		user.identities = {};

		let oldUser = Meteor.users.findOne({"emails.address": { $all : [user.services.facebook.email]}});

		if (oldUser) {
			oldUser.services.facebook = user.services.facebook;
			user = oldUser;
			Meteor.users.remove(oldUser._id);
		}
		return user;
  });

  // Check whether or not user can log in
  // The chack is based on the account activation date 'deferredRegistrationDate'
  // The chack is based on the account activation date 'delayedDate'
  // The chack is based on the account activation date 'deferredRegistrationDate'
  Accounts.validateLoginAttempt(info => {
    if (info.type === 'password') {
      const user = info && info.user

      if (user && user.profile && user.deferredRegistrationDate > parseInt(moment().format('x'))) {
        throw new Meteor.Error(403, 'Your account is not activated yet');
      } else if (user && user.deferredRegistrationDate <= parseInt(moment().format('x'))) {
        Meteor.call('updateUserPending', user._id)
      }
    }

    return true;
  })

	WebApp.connectHandlers.use('/download-ics', (req, res, next) => {
		let http = require('http'),
		fileSystem = require('fs'),
		path = require('path');

		try {
			let filePath = path.join('assets/app/uploads', req.url);
			let stat = fileSystem.statSync(filePath);

			res.writeHead(200, {
				'Content-Type': 'text/calendar',
				'Content-Length': stat.size
			});

			let readStream = fileSystem.createReadStream(filePath);
			readStream.pipe(res);
		} catch (e) {
			res.writeHead(404);
			res.end(`404 File not found!`);
		}
	});
});
