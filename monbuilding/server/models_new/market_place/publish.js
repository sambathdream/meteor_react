// import { IndexLoader } from './imports/load_indexes';
// import { DataCollector } from './imports/collect_extra_data';
import { Session } from '../imports/session';
import { map, tap } from 'rxjs/operators';
import { RxCollection, pipeToSubscription } from '../imports/collection-observer';
import { MarketServices, MarketServiceTypes, MarketBasket, MarketReservations, TermsOfServices } from '/common/collections/marketPlace'
import { payzenApiId } from '/common/collections/API'
import { Meteor } from 'meteor/meteor'
import _ from 'lodash'
// import { MarketPlaceFiles } from '/common/userFiles'

// const FEED_PAGE_SIZE = 40;
Meteor.startup(() => {
  const MarketServices$ = new RxCollection(MarketServices);
  const MarketServiceTypes$ = new RxCollection(MarketServiceTypes);
  const MarketBasket$ = new RxCollection(MarketBasket);
  const MarketReservations$ = new RxCollection(MarketReservations);
  const TermsOfServices$ = new RxCollection(TermsOfServices);
  const CondoIntegrationConfig$ = new RxCollection(CondoIntegrationConfig);

  Meteor.publish('get-terms-of-services_manager', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const condosWatcher = session.watchValue('condos').pipe(
        tap(condosEvent => {
          let condosWithRight = []
          condosEvent && condosEvent.forEach(condoId => {
            if (session.userHasPermission(condoId, 'marketPlace.see')) {
              condosWithRight.push(condoId)
            }
          });
          const termsOfServicesCursor = TermsOfServices$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, value: 1 } })
          pipeToSubscription(this,
            termsOfServicesCursor
          );
        })
      )
      pipeToSubscription(this,
        condosWatcher
      );
    })();
  });

  Meteor.publish('get-terms-of-services', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const termsOfServicesCursor = TermsOfServices$.find({ condoId }, { fields: { _id: 1, condoId: 1, value: 1 } })
      pipeToSubscription(this,
        termsOfServicesCursor
      );
    })();
  });

  Meteor.publish('market_place_reservation_details', function (reservationId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketReservationsCursor = MarketReservations$.find({ _id: reservationId }, { fields: { _id: 1, userId: 1, condoId: 1, marketBaskedId: 1, pick_up: 1, drop_off: 1, delivered_by_staff: 1, picked_up_by_staff: 1, special_request: 1, special_request_files: 1, price: 1, vat: 1, quantity: 1, createdAt: 1, updatedAt: 1, validatedAt: 1, time_slot: 1, day_selected: 1, use_time_slot: 1 } });
      pipeToSubscription(this,
        marketReservationsCursor
      );
    })();
  });

  Meteor.publish('market_place_reservations_details', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketServicesCursor = MarketServiceTypes$.find({ condoId: condoId }, { fields: { _id: 1, condoId: 1, key: 1 } })
      const marketReservationsCursor = MarketReservations$.find({ condoId, userId: Meteor.userId(), validatedAt: { $ne: null } }, { fields: { _id: 1, userId: 1, condoId: 1, marketBaskedId: 1, title: 1, decription: 1, pick_up: 1, drop_off: 1, delivered_by_staff: 1, picked_up_by_staff: 1, special_request: 1, special_request_files: 1, price: 1, vat: 1, quantity: 1, createdAt: 1, updatedAt: 1, validatedAt: 1, archivedAt: 1, archivedBy: 1, serviceId: 1, serviceTypeKey: 1, pick_up_location: 1, drop_off_location: 1, payment_location: 1, cardType: 1, time_slot: 1, day_selected: 1, use_time_slot: 1, time_slot_scale: 1, time_slot_duration: 1 } });
      pipeToSubscription(this,
        marketReservationsCursor,
        marketServicesCursor
      );
    })();
  });

  Meteor.publish('market_place_services_stats', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketServicesCursor = MarketServices$.find({ condoId: condoId }, { fields: { _id: 1, condoId: 1, type: 1 } })
      pipeToSubscription(this,
        marketServicesCursor
      );
    })();
  });

  Meteor.publish('market_place_reservations_details_manager', function () {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const condosWatcher = session.watchValue('condos').pipe(
        tap(condosEvent => {
          let condosWithRight = []
          condosEvent && condosEvent.forEach(condoId => {
            if (session.userHasPermission(condoId, 'marketPlace.see')) {
              condosWithRight.push(condoId)
            }
          });
          const marketReservationsCursor = MarketReservations$.find({ condoId: { $in: condosWithRight }, validatedAt: { $ne: null } }, { fields: { _id: 1, userId: 1, condoId: 1, marketBaskedId: 1, title: 1, decription: 1, pick_up: 1, drop_off: 1, delivered_by_staff: 1, picked_up_by_staff: 1, special_request: 1, special_request_files: 1, price: 1, vat: 1, quantity: 1, createdAt: 1, updatedAt: 1, validatedAt: 1, archivedAt: 1, archivedBy: 1, serviceId: 1, serviceTypeKey: 1, pick_up_location: 1, drop_off_location: 1, payment_location: 1, cardType: 1, time_slot: 1, day_selected: 1, use_time_slot: 1, time_slot_scale: 1, time_slot_duration: 1 } });
          const marketServicesTypesCursor = MarketServiceTypes$.find({}, { fields: { _id: 1, condoId: 1, key: 1 } })
          const marketServicesCursor = MarketServices$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, key: 1 } })
          pipeToSubscription(this,
            marketReservationsCursor,
            marketServicesTypesCursor,
            marketServicesCursor
          );
        })
      )
      pipeToSubscription(this,
        condosWatcher
      );
    })();
  });

  Meteor.publish('market_place_basket', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketBasketCursor = MarketBasket$.find({ userId: Meteor.userId(), condoId: condoId }, { fields: { _id: 1, userId: 1, condoId: 1, services: 1, status: 1, createdAt: 1, updatedAt: 1 }, sort: { status: -1, finishedAt: -1 }, limit: 2 });
      pipeToSubscription(this,
        marketBasketCursor
      );
    })();
  });

  Meteor.publish('market_place_all_basket', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketBasketCursor = MarketBasket$.find({ userId: Meteor.userId(), condoId: condoId }, { fields: { _id: 1, userId: 1, condoId: 1, services: 1, status: 1, createdAt: 1, updatedAt: 1 }, sort: { status: -1, finishedAt: -1 }, limit: 100 });
      pipeToSubscription(this,
        marketBasketCursor
      );
    })();
  });

  Meteor.publish('market_place_basket_with_reservations', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketBasketCursor = MarketBasket$.find({ userId: Meteor.userId(), condoId: condoId }, { fields: { _id: 1, userId: 1, condoId: 1, services: 1, status: 1, createdAt: 1, updatedAt: 1 }, sort: { status: -1, finishedAt: -1 }, limit: 2 });
      const marketServicesCursor = MarketServices$.find({ condoId }, { fields: { _id: 1, condoId: 1, key: 1, files: 1 } })
      const marketReservationsCursor = MarketReservations$.find({ condoId, userId: Meteor.userId(), validatedAt: { $eq: null } }, { fields: { _id: 1, userId: 1, condoId: 1, marketBaskedId: 1, title: 1, decription: 1, pick_up: 1, drop_off: 1, delivered_by_staff: 1, picked_up_by_staff: 1, special_request: 1, special_request_files: 1, price: 1, vat: 1, quantity: 1, createdAt: 1, updatedAt: 1, validatedAt: 1, archivedAt: 1, archivedBy: 1, serviceId: 1, serviceTypeKey: 1, time_slot: 1, day_selected: 1, use_time_slot: 1, time_slot_scale: 1, time_slot_duration: 1 } });
      pipeToSubscription(this,
        marketBasketCursor,
        marketServicesCursor,
        marketReservationsCursor
      );
    })();
  });

  Meteor.publish('market_place_basket_count', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const marketBasketCursor = MarketBasket$.find({ userId: Meteor.userId(), condoId: condoId, status: 'in_progress' }, { fields: { _id: 1, services: 1, status: 1, condoId: 1 } });
      pipeToSubscription(this,
        marketBasketCursor
      );
    })();
  });

  Meteor.publish('market_place_create_service', function () {
    const session = new Session();
    // validate user
    session.validateLogged();

    (async () => {
      // const role = session.getValue('role');
      // console.log('role', role)

      const condosWatcher = session.watchValue('condos').pipe(
        tap(condosEvent => {
          let condosWithRight = []
          condosEvent && condosEvent.forEach(condoId => {
            if (session.userHasPermission(condoId, 'marketPlace.addItem')) {
              condosWithRight.push(condoId)
            }
          });

          const marketServicesCursor = MarketServiceTypes$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, key: 1 } });
          const integrationConfigCursor = CondoIntegrationConfig$.find({ integrationId: payzenApiId, condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, integrationId: 1, active: 1, activatedOn: 1 } });
          pipeToSubscription(this,
            marketServicesCursor,
            integrationConfigCursor
          );
        })
      )
      pipeToSubscription(this,
        condosWatcher
      );
    })();
  });

  Meteor.publish('market_place_service_details', function (serviceId) {
    const session = new Session();
    // validate user
    session.validateLogged();

    (async () => {
      // const role = session.getValue('role');
      // console.log('role', role)
      const condosWatcher = session.watchValue('condos').pipe(
        tap(condosEvent => {
          let condosWithRight = []
          condosEvent && condosEvent.forEach(condoId => {
            if (session.userHasPermission(condoId, 'marketPlace.editItem')) {
              condosWithRight.push(condoId)
            }
          });

          const marketServicesCursor = MarketServiceTypes$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, key: 1 } });
          const integrationConfigCursor = CondoIntegrationConfig$.find({ integrationId: payzenApiId, condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, integrationId: 1, active: 1, activatedOn: 1 } });

          const marketCursor = MarketServices$.find({ _id: serviceId },
            {
              fields: {
                _id: 1,
                condoId: 1,
                type: 1,
                title: 1,
                description: 1,
                isThirdParty: 1,
                link_third_party: 1,
                price: 1,
                quantity_available: 1,
                unlimited_quantity: 1,
                vat: 1,
                pick_up: 1,
                drop_off: 1,
                delivered_by_staff: 1,
                picked_up_by_staff: 1,
                pick_up_location: 1,
                drop_off_location: 1,
                pay_on_platform: 1,
                pay_outside_platform: 1,
                payment_location: 1,
                files: 1,
                createdAd: 1,
                updatedAt: 1,
                use_time_slot: 1,
                same_opened_hours: 1,
                allow_recurring_service: 1,
                time_slot_scale: 1,
                time_slot_duration: 1,
                simultaneous_time_slot: 1,
                openedDays: 1,
                product_code: 1
              },
              limit: 1
            }
          );
          pipeToSubscription(this,
            marketCursor,
            marketServicesCursor,
            integrationConfigCursor
          );
        })
      )
      pipeToSubscription(this,
        condosWatcher
      );
    })();
  });

  Meteor.publish('market_place_feed', function (condoId) {
    const session = new Session();
    session.validateLogged();

    (async () => {
      const condosWatcher = session.watchValue('condos').pipe(
        tap(condosEvent => {
          let condosWithRight = []
          condosEvent && condosEvent.forEach(condoId => {
            if (session.userHasPermission(condoId, 'marketPlace.see')) {
              condosWithRight.push(condoId)
            }
          });
          const marketCursor = MarketServices$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, title: 1, type: 1, description: 1, isThirdParty: 1, price: 1, files: 1, createdAt: 1, updatedAt: 1, quantity_available: 1, unlimited_quantity: 1 } })
          const marketServicesCursor = MarketServiceTypes$.find({ condoId: { $in: condosWithRight } }, { fields: { _id: 1, condoId: 1, key: 1 } })
          pipeToSubscription(this,
            marketCursor,
            marketServicesCursor
          );
        })
      )
      pipeToSubscription(this,
        condosWatcher
      );
    })();
  });
});
