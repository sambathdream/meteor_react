Meteor.startup(
  function () {
      Meteor.publish('pushNotification', function () {
          if (!this.userId) {
              throw new Meteor.Error(300, "Not Authenticated");
          }
          return PushNotificationsId.find({'_id': this.userId}, {fields: {notification: 1}})
      });
  }
);
