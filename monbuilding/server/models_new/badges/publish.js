import {feedOccupantBadges} from './imports/resident_badges';
import {feedManagerBadges} from './imports/manager_badges';
import { Session } from '../imports/session';


Meteor.startup(() => {
  Meteor.publish('badges', function() {
    const session = new Session();
    // validate user
    session.validateLogged();
    const role = session.getValue('role');
    if (role === 'occupant') {
      feedOccupantBadges(this, session);
    } else if (role === 'manager') {
      feedManagerBadges(this, session);
    }
  });
  Meteor.publish('userLastReadAll', function() {
    return UserLastReadAll.find({_id: this.userId})
  })
});
