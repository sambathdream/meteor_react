import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount, getJoinedDate} from './helpers';

export function watchInformation (context, session, condoIds) {
  const Information$ = new RxCollection(ActuPosts);
  const userId = context.userId;

  // Get an object with dates of when user joined a condo
  const joined = getJoinedDate(context, session);

  // Whatch permissions
  const permissions = session.watchMultiplePermission(condoIds, [
    'actuality.see'
  ]);

  // Watch posts
  const countAds = countDocuments(({type}) => type);
  const unreadAds = Information$.find({
    $or: condoIds.map(condoId => ({
      condoId,
      updatedAt: {$gt: joined[condoId] || 0},
    })),
    userUpdateView: {$not: { $elemMatch: { $eq: userId } } }
  }, {fields: {type: 1, condoId: 1}})
  .pipe(countAds()).observeChanges();

  return merge(permissions, unreadAds)
    .pipe(
      mergeBadgePermisionCount(condoIds, getAdsPermissionsCounts)
    );
}

function getAdsPermissionsCounts (counts, permissions) {
  const result = {
    event: 0,
    information: 0
  }
  if (permissions['actuality.see']) {
    counts['event'] && (result.event += counts['event'] || 0);
    counts['information'] && (result.information += counts['information'] || 0);
  }
  return result;
}
