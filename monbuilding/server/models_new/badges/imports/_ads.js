import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount, getJoinedDate } from './helpers';

export function watchAds (context, session, condoIds) {
  const ClassifiedsAd$ = new RxCollection(ClassifiedsAds);
  const userId = context.userId;

  // Get an object with dates of when user joined a condo
  const joined = getJoinedDate(context, session);

  // Whatch permissions
  const permissions = session.watchMultiplePermission(condoIds, [
    'ads.see'
  ]);

  // Watch posts
  const countAds = countDocuments(({createdBy}) => createdBy === userId ? 'mine' : 'others');
  const unreadAds = ClassifiedsAd$.find({
    $and: [
      {$or: condoIds.map(condoId => ({
        condoId,
        updatedAt: {$gt: joined[condoId] || 0},
      }))},
      {$or: [
        {userViews: {$not: { $elemMatch: { $eq: userId } } }},
        {
          userUpdateView: {$not: { $elemMatch: { $eq: userId } } },
          participants: userId
        }
      ]}
    ]
  }, {fields: {createdBy: 1, condoId: 1}})
  .pipe(countAds()).observeChanges();

  return merge(permissions, unreadAds)
    .pipe(
      mergeBadgePermisionCount(condoIds, getAdsPermissionsCounts)
    );
}

function getAdsPermissionsCounts (counts, permissions) {
  const result = {
    ads: 0,
    myads: 0
  }
  if (permissions['ads.see']) {
    counts['mine'] && (result.myads += counts['mine'] || 0);
    counts['others'] && (result.ads += counts['others'] || 0);
  }
  return result;
}
