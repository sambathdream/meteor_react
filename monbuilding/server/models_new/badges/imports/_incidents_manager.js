import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount, getJoinedDate } from './helpers';

export function watchIncidentsManager (context, session, condoIds, residentRoleId) {
  const Incident$ = new RxCollection(Incidents);
  const userId = context.userId;

  // Get an object with dates of when user joined a condo
  const joined = getJoinedDate(context, session);

  // Whatch permissions
  const permissions = session.watchMultiplePermission(condoIds, [
    'incident.see'
  ]);

  // Watch posts
  const countIncidents = countDocuments(() => 'incident');
  const unreadIncidents = Incident$.find({
    'condoId': { $in: condoIds},
    "state.status": {$in: [0, 1]}
  }, {fields: {condoId: 1}})
  .pipe(countIncidents()).observeChanges();

  return merge(permissions, unreadIncidents)
    .pipe(
      mergeBadgePermisionCount(condoIds, getAdsPermissionsCounts)
    );
}

function getAdsPermissionsCounts (counts, permissions) {
  const result = {
    incident: 0,
  }
  if (permissions['incident.see']) {
    counts['incident'] && (result.incident += counts['incident'] || 0);
  }
  return result;
}
