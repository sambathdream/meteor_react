import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount, getJoinedDate } from './helpers';

export function watchIncidents (context, session, condoIds, residentRoleId) {
  const Incident$ = new RxCollection(Incidents);
  const userId = context.userId;

  // Get an object with dates of when user joined a condo
  const joined = getJoinedDate(context, session);

  // Whatch permissions
  const permissions = session.watchMultiplePermission(condoIds, [
    'incident.see'
  ]);

  // Watch posts
  const countIncidents = countDocuments(() => 'incidentResident');
  const statusQuery = [
    {'state.status': { $in: [1, 2] } },
    {
      'state.status': { $in: [0, 3] },
      'declarer.userId': userId
    }
  ];
  if (residentRoleId) {
    perStatusQuery.push({
      "state.status": 3,
      [`history.share.${residentRoleId}.state`]: true
    });
  }
  const unreadIncidents = Incident$.find({
    $or: condoIds.map(condoId => ({
      condoId,
      updatedAt: {$gt: joined[condoId] || 0},
    })),
    $or: statusQuery,
    userViews: {$not: { $elemMatch: { $eq: userId } } }
  }, {fields: {condoId: 1}})
  .pipe(countIncidents()).observeChanges();

  return merge(permissions, unreadIncidents)
    .pipe(
      mergeBadgePermisionCount(condoIds, getAdsPermissionsCounts)
    );
}

function getAdsPermissionsCounts (counts, permissions) {
  const result = {
    incidentResident: 0,
  }
  if (permissions['incident.see']) {
    counts['incidentResident'] && (result.incidentResident += counts['incidentResident'] || 0);
  }
  return result;
}
