import {map} from 'rxjs/operators';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments } from './helpers';

export function watchReservations (context, session, condoIds) {
  const Reservation$ = new RxCollection(Reservations);
  const userId = context.userId;
  const emptyResponse = {};
  for (const condoId of condoIds) {
    emptyResponse[condoId] = {
      reservation_reject: 0,
      reservation_pending: 0,
      reservation: 0
    }
  }

  // Watch reservations
  const countReservations = countDocuments(({rejected, pending}) => {
    switch (true) {
      case rejected: return 'reservation_reject';
      case pending: return 'reservation_pending';
      default: return 'reservation';
    }
  });
  const unreadReservations = Reservation$.find({
    $or: [
      { createdBy: userId },
      { participants: userId },
    ],
    origin: userId,
    condoId: { $in: condoIds },
    end: { $gte: Date.now() },
    needValidation: true,
    pending: false,
    userViews: {$not: { $elemMatch: { $eq: userId } } }
  }, {fields: {rejected: 1, pending: 1, condoId: 1}})
  .pipe(
    countReservations(),
    map(({counts}) => ({
      ...emptyResponse,
      ...counts
    }))
  ).observeChanges();

  return unreadReservations
}
