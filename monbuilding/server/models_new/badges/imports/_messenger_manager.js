import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount } from './helpers';

export function watchManagerMessenger (context, session, condoIds) {
  const Message$ = new RxCollection(Messages);
  const userId = context.userId;

  const permissions = session.watchMultiplePermission(condoIds, [
    'messenger.seeAllMsgEnterprise'
  ]);

  const countMessages = countDocuments(({global}) => global ? 'messenger_personnal' : 'messenger_enterprise');
  const unreadMessagesObserver = Message$.find({
    condoId: { $in: condoIds },
    target: 'manager',
    $or: [
      { incidentId: { $exists: true } },
      {
        $and: [
          { type: { $exists: true } },
          { details: { $exists: true } },
        ]
      }
    ],
    isArchived: {$ne: true},
    managerReplied: {$ne: true},
    // userUpdateView: {$not: { $elemMatch: { $eq: userId } } },
  }, {fields: {condoId: 1, global: 1}})
    .pipe(countMessages()).observeChanges();

  return merge(permissions, unreadMessagesObserver)
    .pipe(
      mergeBadgePermisionCount(condoIds, getMessengerPermissionsCounts)
    );
}

function getMessengerPermissionsCounts (counts, permissions) {
  const result = {
    messenger_personnal: 0,
    messenger_enterprise: 0,
    messenger: 0
  };
  if (permissions['messenger.seeAllMsgEnterprise']) {
    result['messenger_personnal'] = counts['messenger_personnal'] || 0;
    result['messenger_enterprise'] = counts['messenger_enterprise'] || 0;
    result['messenger'] = result['messenger_personnal'] + result['messenger_enterprise'];
  }
  return result;
}
