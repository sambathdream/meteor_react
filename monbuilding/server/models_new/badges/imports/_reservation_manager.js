import {map} from 'rxjs/operators';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments } from './helpers';

export function watchReservationsManager (context, session, condoIds) {
  const Reservation$ = new RxCollection(Reservations);
  const userId = context.userId;
  let emptyResponse = {};
  for (const condoId of condoIds) {
    emptyResponse[condoId] = {
      reservation_validated: 0,
      reservation_pending: 0
    }
  }
  // Watch reservations
  const countReservations = countDocuments(({pending}) => pending ? 'reservation_pending' : 'reservation_validated');
  const unreadReservations = Reservation$.find({
    condoId: { $in: condoIds },
    start: { $gte: Date.now() },
    $or: [
      {
        pending: true,
        $or: [
          {
            needValidation: true,
            validators: userId
          },
          {
            needValidation: false
          }
        ]
      },
      {
        pending: false,
        rejected: false,
        userViews: {$not: { $elemMatch: { $eq: userId } } }
      }

    ]
  }, {fields: { pending: 1, condoId: 1}})
  .pipe(
    countReservations(),
    map(({counts}) => ({
      ...emptyResponse,
      ...counts
    }))
  ).observeChanges();

  return unreadReservations
}
