import {map} from 'rxjs/operators';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments } from './helpers';
import { Observable } from '../../../../node_modules/rxjs';

export function watchUsers (context, session, condoIds) {
  const Resident$ = new RxCollection(Residents);
  const userId = context.userId;
  let emptyResponse = {};
  for (const condoId of condoIds) {
    emptyResponse[condoId] = {
      newUser: 0
    }
  }
  // Watch new users
  const countNewUsers = countDocuments(() => 'newUser');
  const newUsers = Resident$.find({
    _id: { $ne: userId },
    'pendings.condoId': {$in: condoIds},
    'validation.type': 'gestionnaire'
  }, {fields: {'pendings.condoId': 1}})
  .pipe(
    function (source) {
      return Observable.create(subscriber => {
        const handler = source.subscribe(event => {
          if (event.fields) {
            for (const {condoId} of event.fields.pendings) {
              subscriber.next({
                ...event,
                fields: { condoId }
              })
            }
          } else {
            subscriber.next(event);
          }
        });
        return handler;
      });
    },
    countNewUsers(),
    map(({counts}) => ({
      ...emptyResponse,
      ...counts
    }))
  ).observeChanges();

  return newUsers;
}
