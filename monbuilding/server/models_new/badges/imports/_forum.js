import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount, getJoinedDate } from './helpers';

export function watchForum (context, session, condoIds) {
  const ForumPost$ = new RxCollection(ForumPosts);
  const userId = context.userId;

  // Get an object with dates of when user joined a condo
  const joined = getJoinedDate(context, session);

  // Whatch permissions
  const permissions = session.watchMultiplePermission(condoIds, [
    'forum.seePoll',
    'forum.seeSubject'
  ]);

  const readAllDates = UserLastReadAll.findOne({_id: userId}, {fields: {forum: 1}});
  const readAllDate = (readAllDates && readAllDates.forum) ? readAllDates.forum : 0;

  // Watch posts
  const countPosts = countDocuments(({createdBy, type}) => createdBy === userId ? `mine-${type}` : `others-${type}`);
  const unreadForumPosts = ForumPost$.find({
    $and: [
      {$or: condoIds.map(condoId => ({
        condoId,
        updatedAt: {$gt: Math.max(readAllDate, joined[condoId] || 0)},
      }))},
      {$or: [
        {userViews: {$not: { $elemMatch: { $eq: userId } } }},
        {
          userUpdateView: {$not: { $elemMatch: { $eq: userId } } },
          participants: userId
        }
      ]}
    ]

  }, {fields: {type: 1, createdBy: 1, condoId: 1}})
  .pipe(countPosts()).observeChanges();

  return merge(permissions, unreadForumPosts)
    .pipe(
      mergeBadgePermisionCount(condoIds, getForumPermissionsCounts)
    );
}

function getForumPermissionsCounts (counts, permissions, condoId) {
  const result = {
    forum: 0,
    myforum: 0
  }
  if (permissions['forum.seePoll']) {
    counts['mine-poll'] && (result.myforum += counts['mine-poll'] || 0);
    counts['others-poll'] && (result.forum += counts['others-poll'] || 0);
  }
  if (permissions['forum.seeSubject']) {
    counts['mine-post'] && (result.myforum += counts['mine-post'] || 0);
    counts['others-post'] && (result.forum += counts['others-post'] || 0);
  }
  return result;
}
