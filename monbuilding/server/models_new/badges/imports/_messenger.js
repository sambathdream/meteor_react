import {merge} from 'rxjs';
import { RxCollection } from '../../imports/collection-observer';
import { countDocuments, mergeBadgePermisionCount } from './helpers';

const destinationMap = {
  occupant: 'messagerieResident',
  council: 'messagerieConseilSyndical',
  keeper: 'messagerieGardien',
  manager: 'messagerieGestionnaire',
  global: 'messagerieGestionnaire',
  incident: 'messagerieGestionnaire'
};

const badges = [
  {permission: 'writeToResident', badge: 'messagerieResident'},
  {permission: 'writeToManager', badge: 'messagerieGestionnaire'},
  {permission: 'writeToUnionCouncil', badge: 'messagerieConseilSyndical'},
  {permission: 'writeToSupervisors', badge: 'messagerieGardien'}
];

export function watchMessenger (context, session, condoIds) {
  const Message$ = new RxCollection(Messages);
  const userId = context.userId;

  const permissions = session.watchMultiplePermission(condoIds, [
    'messenger.writeToResident',
    'messenger.writeToManager',
    'messenger.writeToUnionCouncil',
    'messenger.writeToSupervisors'
  ]);
  const countMessages = countDocuments(({destination}) => destinationMap[destination]);
  const unreadMessagesObserver = Message$.find({
    condoId: { $in: condoIds },
    participants: userId,
    isArchived: {$ne: true},
    userUpdateView: {$not: { $elemMatch: { $eq: userId } } },
  }, {fields: {condoId: 1, destination: 1}})
    .pipe(countMessages()).observeChanges();

  return merge(permissions, unreadMessagesObserver)
    .pipe(
      mergeBadgePermisionCount(condoIds, getMessengerPermissionsCounts)
    );
}

function getMessengerPermissionsCounts (counts, permissions) {
  const result = {}
  for (const {permission, badge} of badges) {
    let cCount = 0;
    if (permissions[`messenger.${permission}`]) {
      if (counts[badge]) {
        cCount = counts[badge];
      }
    }
    result[badge] = cCount;
  }
  return result;
}
