import {Observable, merge} from 'rxjs';
import {map} from 'rxjs/operators';
import { RxCollection } from '../../imports/collection-observer';

export function watchManual (context, session, condoIds) {
  const ViewCondoDocument$ = new RxCollection(ViewCondoDocument);

  // watch permissions
  const manualPermissionObserver = session.watchUserPermission(condoIds, 'manual.see').pipe(map(v => ({permission: {manual: v}})));
  const mapPermissionObserver = session.watchUserPermission(condoIds, 'map.see').pipe(map(v => ({permission: {map: v}})));
  const unreadDocsObserver = ViewCondoDocument$.find(
    {
      'condoId': { $in: condoIds},
      users: {$not: { $elemMatch: { $eq: context.userId } } }
    },
    { fields: {condoId: 1, type: 1}}
  ).observeChanges().pipe(viewsFeedToObject());

  return merge(
    manualPermissionObserver,
    mapPermissionObserver,
    unreadDocsObserver
  ).pipe(mergePermissionsAndViews(condoIds))
}

function viewsFeedToObject () {
  return function (source) {
    return Observable.create(subscriber => {
      const unviewedDocs = {
        manual: {},
        map: {}
      }
      const views = {};
      let ready = false;
      const handler = source.subscribe(({type, fields, id}) => {
        const docType = (fields && fields.type) || (views[id] && views[id].type);
        let docs = unviewedDocs[docType];

        switch(type) {
          case 'changed': {
            docs[views[id]] = false;
          }
          case 'added': {
            docs[fields.condoId] = true;
            views[id] = {condoId: fields.condoId, type: fields.type};
            break;
          }
          case 'removed': {
            const view = views[id];
            if (view) {
              unviewedDocs[view.type][view.condoId] = false;
              delete views[id];
            }
            break;
          }
          case 'ready': {
            ready = true;
            break
          }
        }
        if (ready) {
          subscriber.next({unviewed: unviewedDocs});
        }
      })
      return handler;
    });
  }
}

function mergePermissionsAndViews(condoIds) {
  return function (source) {
    return Observable.create(subscriber => {
      let permissions = {};
      let unviewed = {};
      const handler = source.subscribe(value => {
        if (value.unviewed) { unviewed = value.unviewed; }
        permissions = {...permissions, ...value.permission };
        if (permissions.map && permissions.manual && unviewed.map && unviewed.manual) {
          const values = {};
          for (const condoId of condoIds) {
            values[condoId] = {
              map: permissions.map[condoId] && unviewed.map[condoId] ? 1 : 0,
              manual: permissions.manual[condoId] && unviewed.manual[condoId] ? 1 : 0,
            }
          }
          subscriber.next(values);
        }
      });
      return handler;
    })
  }
}

