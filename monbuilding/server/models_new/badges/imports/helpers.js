import {Observable} from 'rxjs';
import { RxCollection, RxDerivedCursor } from '../../imports/collection-observer';

export function countDocuments (keyGetter) {
  return function () {
    return function (source) {
      const counts = {};
      const idMap = {};
      let ready = false;
      return Observable.create(subscriber => {
        const handler = source.subscribe(({id, type, fields}) => {
          switch (type) {
            case 'added': {
              const target = keyGetter(fields)
              const {condoId} = fields;
              if (!target) { break; }
              if (!counts[condoId]) { counts[condoId] = {}; }
              if (!counts[condoId][target]) { counts[condoId][target] = 0; }
              idMap[id] = {condoId, target};
              counts[condoId][target] += 1;
              break;
            }
            case 'removed': {
              const {condoId, target} = idMap[id];
              counts[condoId][target] -= 1;
              break;
            }
            case 'ready': {
              ready = true;
              break;
            }
          }
          if (ready) {
            subscriber.next({counts});
          }
        });
        return handler;
      });
    }
  }
}

export function mergeBadgePermisionCount (condoIds, getCounts) {
  return function (source) {
    let permissions = {};
    let count = {};
    let countsReady = false;
    let permissionsReady = false;
    return Observable.create(subscriber => {
      const handler = source.subscribe(value => {
        if (value.counts) {
          countsReady = true;
          count = value.counts;
        } else {
          permissionsReady = true;
          permissions = value;
        }
        if (permissionsReady && countsReady) {
          const badgeCount = {};
          for (const condoId of condoIds) {
            badgeCount[condoId] = getCounts(count[condoId] || {}, permissions[condoId] || {}, condoId);
          }
          subscriber.next(badgeCount);
        }
      });
      return handler;
    });
  }
}

export function getJoinedDate (context, session) {
  let joined = {};
  const userId = context.userId;
  if (session.getValue('role') === 'occupant') {
    const residentData = Residents.findOne({userId}, { fields: {'condos.condoId': 1, 'condos.joined': 1}});
    if (residentData) {
      joined = residentData.condos.reduce((obj, c) => {
        obj[c.condoId] = c.joined && new Date(c.joined).getTime();
        return obj
      }, {});
    }
  } else {
    const enterprise = Enterprises.findOne(
      {'users.userId': userId},
      {fields: {'users': 1}}
    );
    const condos = enterprise.users.find(u => u.userId === userId).condosInCharge;
    if (!condos) { return {}; }
    joined = condos.reduce((obj, c) => {
      obj[c.condoId] = c.joined;
      return obj;
    }, {});
  }
  return joined;
}


export function wachLastReadAllTime (context, section, subscription) {
  const UserLastReadAll$ = new RxCollection(UserLastReadAll);
  return Observable.create(subscriber => {
    const newBadgesCursor = new RxDerivedCursor()

    const handler = UserLastReadAll$
      .find({_id: context.userId}, {fields: {[section]: 1}})
      .observeChanges()
      .subscribe(({fields}) => {
        if (fields && fields.hasOwnProperty(section)) {
          newBadgesCursor.setSource(subscription())
        }
      });
    let ready = false;
    let added = false;
    const readHandler = newBadgesCursor.observe().subscribe(({id, type, fields}) => {
      if (type === 'ready' && !ready) {
        ready = true;
      }
      if (type === 'added') {
        if (added) { type = 'changed' }
        else { added = true }
      }
      subscriber.next({id, type, fields})
    })
    return function () {
      handler.unsubscribe();
      readHandler && readHandler.unsubscribe();
    }
  });
}
