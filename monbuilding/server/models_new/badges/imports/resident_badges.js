import {Observable, merge} from 'rxjs';
import {tap, map} from 'rxjs/operators';
import { pipeToSubscription, asCursor } from '../../imports/collection-observer';
import { watchManual} from './_manual';
import {watchMessenger} from './_messenger';
import {watchForum} from './_forum';
import {watchInformation} from './_information';
import {watchAds} from './_ads';
import {watchIncidents} from './_incidents';
import {watchReservations} from './_reservation';
import {watchUsers} from './_users';
import {wachLastReadAllTime} from './helpers'

export function feedOccupantBadges(context, session) {
  const badgeObservable = session.watchValue('condos').pipe(
    function (source) {
      let allHandle;
      const counter = new Set();
      const condos = new Set();
      let readySent = false;
      let prevCondoIds = [];
      return Observable.create(subscriber => {
        const handle = source.subscribe(condoIds => {
          const removedCondos = _.difference(prevCondoIds, condoIds);
          prevCondoIds = condoIds;
          for (const condoId of removedCondos) {
            if (condos.has(condoId)) {
              condos.delete(condoId);
              subscriber.next({type: 'removed', id: condoId});
            }
          }
          if (allHandle) { allHandle.unsubscribe(); }
          allHandle = merge(
              watchManual(context, session, condoIds).pipe(mapToDDP('manual', condos, counter, 8)),
              watchMessenger(context, session, condoIds).pipe(mapToDDP('messenger', condos, counter, 8)),
              wachLastReadAllTime(
                context,
                'forum',
                () => watchForum(context, session, condoIds).pipe(mapToDDP('forum', condos, counter, 8))
              ),
              watchAds(context, session, condoIds).pipe(mapToDDP('ads', condos, counter, 8)),
              watchInformation(context, session, condoIds).pipe(mapToDDP('info', condos, counter, 8)),
              watchIncidents(context, session, condoIds).pipe(mapToDDP('incidents', condos, counter, 8)),
              watchReservations(context, session, condoIds).pipe(mapToDDP('resa', condos, counter, 8)),
              watchUsers(context, session, condoIds).pipe(mapToDDP('users', condos, counter, 8))
            ).subscribe(event => {
              if (event.type === 'ready' && !readySent) {
                subscriber.next(event);
                readySent = true;
              } else {
                subscriber.next(event);
              }
            });

        });
        return function () {
          handle.unsubscribe();
          allHandle && allHandle.unsubscribe();
        }
      })
    },
  );
  const badgeCursor = asCursor({
    collectionName: 'residentBadges',
    observe: badgeObservable
  });
  pipeToSubscription(context, badgeCursor);
};

function mapToDDP (name, condoSet, nameSet, max) {
  return function (source) {
    return Observable.create(subscriber => {
      let once = false;
      const handler = source.subscribe(event => {
        const condos = Object.keys(event);
        for (const id of condos) {
          const type = condoSet.has(id) ? 'changed' : 'added';
          condoSet.add(id);
          subscriber.next({ type, id, fields: event[id] });
        }
        if (!once) {
          once = true;
          nameSet.add(name);
          if (nameSet.size === max) {
            subscriber.next({type: 'ready'});
          }
        }
      });
      return handler;
    });
  };
}

