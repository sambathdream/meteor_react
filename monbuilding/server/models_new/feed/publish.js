import {IndexLoader} from './imports/load_indexes';
import {DataCollector} from '../imports/collect_extra_data';
import { Session } from '../imports/session';
import { RxCollection, pipeToSubscription } from '../imports/collection-observer';
import { Meteor } from 'meteor/meteor'
import { eventFilter } from '/server/sharedFunctions/events'

const FEED_PAGE_SIZE = 40;
Meteor.startup(() => {
  const Incident$ = new RxCollection(Incidents);

  const ClassifiedsAd$ = new RxCollection(ClassifiedsAds);
  const ForumPost$ = new RxCollection(ForumPosts);
  const Information$ = new RxCollection(ActuPosts);
  const User$ = new RxCollection(Meteor.users);
  const IncidentDetail$ = new RxCollection(IncidentDetails);

  Meteor.publish('feed', function({pageNumber, condoId} = {}) {
    const session = new Session();
    // validate user
    session.validateLogged();
    const restart = pageNumber === 0;
    const indexLoader = new IndexLoader(this, session, condoId, FEED_PAGE_SIZE, restart);
    const userLoader = new DataCollector(new RxCollection(Meteor.users), 'users', {avatar: 1, 'profile.firstname': 1, 'profile.lastname': 1, status: 1});

    (async () => {
      let indexes = await indexLoader.getPage(pageNumber);

      // calculate module indexes
      const adsCursor = getCursorFor('ads', indexes, ClassifiedsAd$, {createdBy: 1, editedAt: 1, title: 1, price: 1, description: 1, attachments: 1, viewsCount: 1, commentCount: 1});

      const informationsCursor = getCursorFor('informations', indexes, Information$, {
        commentCount: 1,
        viewsCount: 1,
        startDate: 1,
        attachments: 1,
        startHour: 1,
        locate: 1,
        endDate: 1,
        endHour: 1,
        allowComment: 1,
        allowAction: 1,
        title: 1,
        description: 1,
        createdBy: 1,
        createdAt: 1,
        updatedAt: 1,
        type: 1
      }, eventFilter(Meteor.userId()))

      const incidentsCursor = getCursorFor('incidents', indexes, Incident$, {'history.share.declarer.files': 1, 'info.title': 1, 'info.comment': 1, 'info.type': 1, 'state.status': 1, 'declared.userId': 1});
      const forumCursor = getCursorFor('forum', indexes, ForumPost$, {createdBy: 1, editedAt: 1, subject: 1, type: 1, endDate: 1, endHour: 1, inputs: 1, isEditable: 1, condoId: 1, votes: 1, viewsCount: 1, commentCount: 1, attachments: 1});
      const usersCursor = getCursorFor('users', indexes, User$, {avatar: 1, 'profile.firstname': 1, 'profile.lastname': 1});

      // load related users
      userLoader.collectDataFrom(adsCursor, ({createdBy}) => [createdBy]);
      userLoader.collectDataFrom(informationsCursor, ({createdBy}) => [createdBy]);
      userLoader.collectDataFrom(forumCursor, ({createdBy}) => [createdBy]);
      userLoader.collectDataFrom(usersCursor, ({_id}) => [_id]);

      const padding = FEED_PAGE_SIZE * pageNumber;
      const elements = {};
      for (let i = 0; i < indexes.length; i += 1) {
        const idx = indexes[i];
         elements[padding + i] = {itemId: idx._id, module: idx.name, date: idx.date};
      }
      this.added('feed', condoId, elements);

      let newItemsCursor = [];
      if (restart) {
        newItemsCursor = indexLoader.newItemCountCursor();
        for (const cursor of newItemsCursor) {
          switch (cursor.collectionName) {
            case 'ClassifiedsAds':
            case 'ActuPosts':
            case 'forumPosts':
              userLoader.collectDataFrom(cursor, ({createdBy}) => [createdBy]);
              break;
            case 'residents':
              userLoader.collectDataFrom(cursor, ({userId}) => [userId]);
              break;
          }
        }
      }
      pipeToSubscription(this,
        adsCursor,
        informationsCursor,
        incidentsCursor,
        forumCursor,
        usersCursor,
        userLoader,
        restart && IncidentDetail$.find({}),
        newItemsCursor
      );
    })();
  });
});

function getCursorFor (name, indexes, Collection, fields, query = {}) {
  return Collection.find({
    _id: { $in: indexes.filter(v => v.name === name).map(v => v._id) },
    ...query
  }, {fields})
}
