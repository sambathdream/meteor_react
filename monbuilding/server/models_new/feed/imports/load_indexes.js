
import {merge, Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import { RxCollection, asCursor, subjectForCursor } from '../../imports/collection-observer';
import { Meteor } from 'meteor/meteor'
import { eventFilter } from '/server/sharedFunctions/events'

const feedModules = [
  'ads',
  'informations',
  'incidents',
  'forum',
  'users'
];



const COUNT_MULTIPLIER = 5;
export class IndexLoader {
  constructor (context, session, condoId, pageSize, restart) {
    this.context = context;
    this.session = session;
    this.condoId = condoId;
    this.pageSize = pageSize;
    const userId = context.userId;
    const role = session.getValue('role');
    this._loadDataFromSession(restart);

    // setup loaders
    const Incident$ = new RxCollection(Incidents);
    const ClassifiedsAd$ = new RxCollection(ClassifiedsAds);
    const ForumPost$ = new RxCollection(ForumPosts);
    const Information$ = new RxCollection(ActuPosts);

    // queries
    // this queries are used to further filter data
    const incidentOccupant = {
      $and: [
        {$or: [
          {public: {$ne: false}},
          {participants: userId}
        ]},
        {$or: [
          {'state.status': { $in: [1, 2] }},
          {$and: [
            { "state.status": { $in: [0, 3] } },
            { "declarer.userId": userId }
          ]}
        ]}
      ]
    };
    const incidentManager = {"state.status": {$in: [0, 1]}};

    // loaders
    // We only add loaders if user has permission to see the module
    // Forum is a bit more complicated, as users can see either polls, subject or both
    // Users that joined condo are always displayed
    // For loaders you basically send, how many documents to load and which documents to ignore

    const loaders = {};
    const watchers = [];
    const loadCursor = (name, Collection, query, watchFilter) => {
      loaders[name] = createQueryFor(name, Collection, query).bind(0, condoId, this.timestamp);
      watchers.push(watchNewElementsFor(name, Collection, query, watchFilter).bind(0, condoId));
    }
    if (!this.completed['ads'] && session.userHasPermission(condoId, 'ads.see')) {
      loadCursor('ads', ClassifiedsAd$, {}, {createdBy: 1, editedAt: 1, title: 1, price: 1, description: 1, attachments: 1, viewsCount: 1, commentCount: 1});
    }
    if (!this.completed['informations'] && session.userHasPermission(condoId, 'actuality.see')) {
      loadCursor('informations', Information$, eventFilter(Meteor.userId()), {
        commentCount: 1,
        viewsCount: 1,
        startDate: 1,
        attachments: 1,
        startHour: 1,
        locate: 1,
        endDate: 1,
        endHour: 1,
        allowComment: 1,
        allowAction: 1,
        title: 1,
        description: 1,
        createdBy: 1,
        createdAt: 1,
        updatedAt: 1,
        type: 1
      })

    }
    if (!this.completed['incidents'] && session.userHasPermission(condoId, 'incident.see')) {
      loadCursor('incidents', Incident$, role === 'occupant' ? incidentOccupant : incidentManager, {'history.share.declarer.files': 1, 'info.title': 1, 'info.comment': 1, 'info.type': 1, 'state.status': 1, 'declared.userId': 1});

    }
    const canSeePoll = session.userHasPermission(condoId, 'forum.seePoll');
    const canSeePost = session.userHasPermission(condoId, 'forum.seeSubject');
    if (!this.completed['forum'] && canSeePoll && canSeePost) {
      loadCursor('forum', ForumPost$, {}, {createdBy: 1, editedAt: 1, subject: 1, type: 1, endDate: 1, endHour: 1, inputs: 1, isEditable: 1, condoId: 1, votes: 1, viewsCount: 1, commentCount: 1, attachments: 1});
    }
    if (!this.completed['forum'] && canSeePoll && !canSeePost) {
      loadCursor('forum', ForumPost$, {type: 'poll'}, {createdBy: 1, editedAt: 1, subject: 1, type: 1, endDate: 1, endHour: 1, inputs: 1, isEditable: 1, condoId: 1, votes: 1, viewsCount: 1, commentCount: 1, attachments: 1});
    }
    if (!this.completed['forum'] && !canSeePoll && canSeePost) {
      loadCursor('forum', ForumPost$, {type: 'post'}, {createdBy: 1, editedAt: 1, subject: 1, type: 1, endDate: 1, endHour: 1, inputs: 1, isEditable: 1, condoId: 1, votes: 1, viewsCount: 1, commentCount: 1, attachments: 1});
    }
    if (!this.completed['users']) {
      loaders['users'] = _loadCondoUsers.bind(0, condoId, this.timestamp);
      watchers.push(_watchCondoUsers);
    }
    this.loaders = loaders;
    this.watchers = watchers;
  }

  _loadDataFromSession (restart) {
    if (!restart) {
      this.current = this.session.getValue(`feed-current${this.condoId}`);
      this.counters = this.session.getValue(`feed-counters${this.condoId}`);
      this.indexes = this.session.getValue(`feed-indexes${this.condoId}`);
      this.pages = this.session.getValue(`feed-pages${this.condoId}`);
      this.completed = this.session.getValue(`feed-completed${this.condoId}`);
      this.timestamp = this.session.getValue(`feed-timestamp${this.condoId}`);
    }
    if (!this.current) {
      this.current = _.zipObject(feedModules, [...feedModules].fill([]));
      this.session.setValue(`feed-current${this.condoId}`, this.current);
    }
    if (!this.counters) {
      this.counters = _.zipObject(feedModules, [...feedModules].fill(0));
      this.session.setValue(`feed-counters${this.condoId}`, this.counters);
    }
    if (!this.indexes) {
      this.indexes = _.zipObject(feedModules, [...feedModules].fill([]));
      this.session.setValue(`feed-indexes${this.condoId}`, this.indexes);
    }
    if (!this.pages) {
      this.pages = [];
      this.session.setValue(`feed-pages${this.condoId}`, this.pages);
    }
    if (!this.completed) {
      this.completed = _.zipObject(feedModules, [...feedModules].fill(false));
      this.session.setValue(`feed-completed${this.condoId}`, this.completed);
    }
    if (!this.timestamp) {
      this.timestamp = Date.now();
      this.session.setValue(`feed-timestamp${this.condoId}`, this.timestamp);
    }
  }

  newItemCountCursor () {
    let count = 0;
    let index = 0;
    let countSubscriber;
    const countObservable = new Observable(subscriber => {
      subscriber.next({id: this.condoId, type: 'added', fields: {count}});
      subscriber.next({type: 'ready'});
      countSubscriber = subscriber;
    });


    const countNews = ({id, type, fields, __name}) => {
      const data = {};
      if (type === 'added') { count += 1; }
      else if (type === 'removed') { count -= 1; }
      else { return; }
      data.count = count;

      if (type === 'added') {
        data[`${index}`] = __name === 'users'
        ? { itemId: fields.userId, date: condos[0].joined, module: __name, idx: `-${index+1}`}
        : {itemId: id, module: __name, date: fields.updatedAt, idx: `-${index+1}`};
        index += 1;
      }

      countSubscriber && countSubscriber.next({id: this.condoId, type: 'changed', fields: data});
    }
    const cursors = this.watchers.map(watcher => watcher(this.timestamp).pipe(tap(countNews)));
    cursors.push( asCursor({
      collectionName: 'feed-counter',
      observeChanges: countObservable
    }));
    return cursors;
  }

  async getPage (page) {
    let pages = this.pages;
    if (!pages[page]) {
      const count = this.pageSize * (page + 1);
      const loaders = this.loaders;
      const current = this.current;
      const indexes = this.indexes;
      const counters = this.counters;
      const updatedPages = [...pages];
      const loadingOperations = [];
      const modules = Object.keys(loaders);
      for (const module of modules) {
        if (current[module].length - counters[module] < count) {
          loadingOperations.push((async () => {
            const newItems = await loaders[module](count + this.pageSize * COUNT_MULTIPLIER, indexes[module]);
            if (newItems.length < this.pageSize) {
              delete loaders[module];
              this.session.setValue(`feed-completed${this.condoId}`, {
                ...this.completed,
                [module]: true
              })
            }
            current[module] = [...current[module], ...newItems];
            indexes[module] = [...indexes[module], ...newItems.map(i => i._id)];
          })());
        }
      }
      await Promise.all(loadingOperations);
      for (let currentPage = pages.length; currentPage <= page; currentPage += 1) {
        const pageItems = [];
        for (let i = 0; i < this.pageSize; i += 1) {
          let minItem = null;
          let minDate = 0;
          let minModule;
          for (const module of modules) {
            const item = current[module][counters[module]];
            if (item && item.date > minDate) {
              minItem = item;
              minDate = item.date;
              minModule = module;
            }
          }
          if (minItem) {
            counters[minModule] += 1;
            pageItems.push(minItem);
          } else {
            break;
          }
        }
        updatedPages[currentPage] = pageItems;
      }
      this.session.setValue(`feed-current${this.condoId}`, current);
      this.session.setValue(`feed-indexes${this.condoId}`, indexes);
      this.session.setValue(`feed-counters${this.condoId}`, counters);
      this.session.setValue(`feed-pages${this.condoId}`, updatedPages);
      this.pages = updatedPages;
      pages = updatedPages;
    }
    return pages[page];
  }

}

async function _loadCondoUsers (condoId, timestamp, count) {
  const Resident$ = new RxCollection(Residents);
  const residentData = await Resident$.find(
    {
      'condos.condoId': condoId,
      'condos.joined': {$lte: timestamp}
    },
    {fields: {userId: 1, condos: {$elemMatch: {condoId}}}, sort: {'condos.joined': -1}, limit: count}
  ).fetch();

  return residentData.map(({userId, condos}) => ({
    _id: userId,
    date: condos[0].joined,
    name: 'users'
  }));
}

function _watchCondoUsers (condoId, timestamp) {
  const Resident$ = new RxCollection(Residents);
  return Resident$.find(
    {
      'condos.condoId': condoId,
      'condos.joined': {$gt: timestamp}
    },
    {fields: {userId: 1, condos: {$elemMatch: {condoId}}, avatar: 1, 'profile.firstname': 1, 'profile.lastname': 1}}
  ).pipe(
    map(event => {
      event.__name = 'users';
      return event;
    })
  );
}


function createQueryFor (name, Collection, query) {
  return async function (condoId, timestamp, count, skip) {

    const elements = await Collection.find(
      {
        _id: { $nin: skip },
        condoId,
        updatedAt: { $lte: timestamp},
        ...query
      },
      {fields: {updatedAt: 1, createdAt: 1}, sort: {updatedAt: -1}, limit: count}
    ).fetch();
    return elements.map(({_id, updatedAt}) => ({
      _id,
      name,
      date: updatedAt
    }))
  };
}

function watchNewElementsFor (name, Collection, query, filter) {
  return function (condoId, timestamp) {
    return Collection.find(
      {
        condoId,
        createdAt: { $gt: timestamp},
        ...query
      },
      {fields: {updatedAt: 1, ...filter}}
    ).pipe(
      map(event => {
        event.__name = name;
        return event;
      })
    );
  };
}
