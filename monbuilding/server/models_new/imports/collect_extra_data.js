import {tap} from 'rxjs/operators';
import {Observable, BehaviorSubject} from 'rxjs';

export class DataCollector {
  constructor (Collection, name, fields) {
    this.userPerDoc = {};
    this.usersSubject = new BehaviorSubject([]);
    this.lastUsers = [];
    this.totalCount = 0;
    this.readyCount = 0;
    this.Collection = Collection;
    this.fields = fields;
    this.collectionName = name;
  }

  observeChanges () {
    return Observable.create(subscriber => {
      let subscription;
      let usersSent = new Set();
      let readySent = false;
      const handle = this.usersSubject.subscribe(lastUsers => {
        if (subscription) {
          subscription.unsubscribe();
          subscription = null;
        }
        subscription = this.Collection
          .find({_id: {$in: lastUsers}}, {fields: this.fields})
          .pipe(
            tap(({id, type, fields}) => {
              if (type === 'removed') {
                if (usersSent.has(id)) {
                  usersSent.delete(id);
                  subscriber.next({id, type});
                }
                return;
              }
              if (type === 'ready' && readySent) { return; }
              if (type === 'added' && usersSent.has(id)) { type === 'changed'; }
              if (type === 'changed' && !usersSent.has(id)) { type === 'added'; }
              if (type === 'ready') { readySent = true; }
              usersSent.add(id);
              subscriber.next({id, type, fields});
            })
          ).observeChanges().subscribe();
      });
      return () => {
        handle.unsubscribe();
        subscription && subscription.unsubscribe();
      }
    });
  }

  collectDataFrom (cursor, getter) {
    this.totalCount += 1;
    cursor.pipe(
      tap(({id, type, fields}) => {
        if (fields) {
          this.userPerDoc[id] = getter(fields);
        } else if (type === 'removed') {
          delete this.userPerDoc[id];
        } else if (type === 'ready') {
          this.readyCount += 1;
        } else {
          return;
        }
        // collect all users needed;
        const usersSet = new Set();
        for (const userList of Object.values(this.userPerDoc)) {
          userList.forEach(user => usersSet.add(user));
        }
        // check for changes
        let change = false;
        const sortedUsers = [...usersSet].sort();
        if (!_.isEqual(sortedUsers, this.lastUsers)) {
          this.lastUsers = sortedUsers;
          change = true;
        }
        // update client
        if (this.totalCount !== this.readyCount) { return; } // if not all subs ready, we don't do anything
        this.usersSubject.next(this.lastUsers);
      })
    );
  }

  _startSubscription () {

  }

}
