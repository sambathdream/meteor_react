import {RxCollection} from '../collection-observer';

export function usersCursor (ids) {
  const User$ = new RxCollection(Meteor.users);
  const fields = {createdAt: true, profile: true, emails: true, identities: true, lastCondoId: true}
  if (Array.isArray(ids)) {
    return User$.find({ _id: {$in: ids}}, fields)
  } else {
    return User$.find({ _id: ids}, fields)
  }
}
