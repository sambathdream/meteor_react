import { Observable } from 'rxjs';
import {RxCollection, pipeToSubscription, subjectForCursor} from '../collection-observer';

function extractEnterprises (enterprisesIds) {
  return function (source) {
    return Observable.create(subscriber => {
      var subscription = source.subscribe(event => {
        switch (event.type) {
          case 'added':
          case 'changed': {
            if (event.fields.users) {
              const users = event.fields.users;
              users.forEach(user => {
                const userId = user.userId;
                if (enterprisesIds && !enterprisesIds.includes(userId)) { return; }
                subscriber.next({
                  type: event.type,
                  id: userId,
                  fields: user
                })
              })
            }
            break;
          }
          case 'ready': {
            subscriber.next({type: 'ready'});
          }

        }
      },
      err => subscriber.error(err),
      () => subscriber.complete());
      return subscription;
    });
  }
}

export function enterpriseByIdCursor (enterprisesIds) {
  const Enterprise$ = new RxCollection(Enterprises);
  return Enterprise$.find(
    {'users.userId': {$in: enterprisesIds}},
    {fields: {'users': 1}}
  ).pipe(
    extractEnterprises(enterprisesIds)
  );
}
