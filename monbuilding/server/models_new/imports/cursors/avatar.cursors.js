import {RxCollection, asCursor} from '../collection-observer';
import { map, filter, expand } from 'rxjs/operators'
import { Subject, of, merge } from 'rxjs';

export function avatarCursor (userIds) {
  userIds = Array.isArray(userIds) ? userIds : [userIds]
  const idsSet = new Set(userIds);
  const Resident$ = new RxCollection(Residents);
  const Enterprise$ = new RxCollection(Enterprises);
  const GestionnaireUnregistered$ = new RxCollection(GestionnaireUnregistered);


  const residentCursor = Resident$.find({userId: {$in: userIds}}, { fields: {userId: 1, fileId: 1}})
    .observeChanges().pipe(
      map(doc => ({
        type: doc.type,
        id: doc.id,
        fields: !doc.fields ? undefined : {
          userId: doc.id,
          fileId: doc.fields && (doc.fields.fileId || undefined)
        }
      }))
    );
  const unregisteredGestionnairesCursor = GestionnaireUnregistered$.find({_id: {$in: userIds}}, { fields: {pictureId: 1}})
    .observeChanges().pipe(
      map(doc => ({
        type: doc.type,
        id: doc.id,
        fields: !doc.fields ? undefined : {
          userId: doc.id,
          fileId: doc.fields && doc.fields.pictureId
        }
      }))
    );

  const enterpriseCursor = Enterprise$.find(
    {'users.userId': {$in: userIds}},
    { fields: {users: 1, 'users.userId': 1, 'users.profile.fileId': 1}}
  ).observeChanges().pipe(
    map(doc => {
      doc.source = true;
      return doc;
    }),
    expand((object) => {
      if (object.source) {
        if (object.type === 'ready') {
          return of({type: 'ready'});
        } else {
          return of.apply(null, object.fields.users.map((usr) => (
            {
              type: object.type,
              id: usr.userId,
              fields: {
                userId: usr.userId,
                fileId: (usr.profile && usr.profile.fileId) || ''
              }
            }
          )))
        }
      } else {
        return of()
      }
    }),
    filter((document) => !document.source && (!document.fields || idsSet.has(document.fields.userId)))
  );
  const userMap = {};
  let ready = 0
  const avatarObservable = merge(
    residentCursor,
    enterpriseCursor,
    unregisteredGestionnairesCursor
  ).pipe(
    filter(event => {
      return event.type !== 'ready' || ++ready >= 3
    }),
    map(event => {
      if (event.type === 'ready' || event.type === 'removed') {
        return event;
      } else if (event.fields && event.fields.fileId) {
        if (event.fields.userId) {
          userMap[event.id] = event.fields.userId;
        }
        const fileId = event.fields.fileId;
        const baseUrl = `${Meteor.absoluteUrl('download/UserFiles', null, event.fields.userId)}`;
        return {
          type: event.type,
          id: event.fields.userId || userMap[event.id],
          fields: {
            userId: event.fields.userId,
            fileId: fileId,
            avatar: {
              avatar: `${baseUrl}/${fileId}/avatar/${fileId}.jpg`,
              large: `${baseUrl}/${fileId}/large/${fileId}.jpg`,
              original: `${baseUrl}/${fileId}/original/${fileId}.jpg`,
              preview: `${baseUrl}/${fileId}/preview/${fileId}.jpg`,
              thumb100: `${baseUrl}/${fileId}/thumb100/${fileId}.jpg`,
              thumbnail40: `${baseUrl}/${fileId}/thumbnail40/${fileId}.jpg`
            },
          }
        };
      }
    })
  );

  return asCursor({
    collectionName: 'avatars',
    observeChanges: avatarObservable,
    observe: avatarObservable
  });
}
