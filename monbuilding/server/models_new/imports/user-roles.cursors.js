import { RxCollection, asCursor } from '../imports/collection-observer'
import { Meteor } from 'meteor/meteor'
import { map, filter } from 'rxjs/operators'

export function userRoles (userId) {
  const User$ = new RxCollection(Meteor.users)
  const UserHandler = new UserHandlerClass()
  const userCursor = User$.find({ _id: userId })
    .observeChanges()
    .pipe(
      filter(event => event.type && event.type !== 'ready' && event.type !== 'remove'),
      map(event => {
        if (event.fields && event.fields.roles) {
          return {
            ...event,
            fields: UserHandler.getAvailableCondos(userId)
          }
        }
      })
    )
  return asCursor({
    collectionName: 'userRoles',
    observeChanges: userCursor,
    observe: userCursor
  })
}
