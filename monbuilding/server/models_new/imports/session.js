import {BehaviorSubject, merge, Observable} from 'rxjs';
import { tap, map, filter, distinctUntilChanged } from 'rxjs/operators';
const sessionsStore = {};

function getStoreId () {
  const context = DDP._CurrentMethodInvocation.get() || DDP._CurrentPublicationInvocation.get();
  if (!context) {
    throw new Meteor.Error(400, 'not context available');
  }
  return context.connection.id;
}

function getStore (storeId) {
  if (!sessionsStore[storeId]) {
    sessionsStore[storeId] = {_id: storeId};
  }
  return sessionsStore[storeId];
}

function _setSessionValue (key, value, store) {
  if (!store[key]) {
    store[key] = {
      value,
      subject: new BehaviorSubject(value)
    }
  } else {
    const stored = store[key];
    stored.value = value;
    stored.subject.next(value);
  }
}

export class Session {
  constructor () {
    this.storeId = getStoreId();
  }

  bindToPublish(context) {
    const storeId = this.storeId;
    context.onStop(() => {
      const store = sessionsStore[storeId];
      if (store) {
        const values = Object.values(store);
        for (const value of values) {
          if (value.subject) {
            value.subject.complete();
          }
        }
        sessionsStore[storeId].__destroyed = true;
        delete sessionsStore[storeId];
      }
    });
  }

  setValue (key, value) {
    const store = getStore(this.storeId);
    if (!store) { return; }
    return _setSessionValue(key, value, store);
  }

  getValue (key) {
    const store = getStore(this.storeId);
    if (!store || !store[key]) { return null; }
    return store[key].value;
  }

  watchValue (key) {
    const store = getStore(this.storeId);
    if (!store) { return null; }
    if (!store[key]) {
      this.setValue(key, null);
    }
    return store[key].subject.asObservable();
  }

  syncFromCursor (key) {
    const store = getStore(this.storeId);
    const buffer = {};
    let ready = false;
    return function (source) {
      return source.pipe(
        tap(event => {
          if (store.__destroyed) {
            return;
          }
          switch (event.type) {
            case 'added': {
              buffer[event.id] = event.fields;
              ready && _setSessionValue(key, {...buffer}, store);
              return;
            }
            case 'changed': {
              buffer[event.id] = {...buffer[event.id], ...event.fields};
              ready && _setSessionValue(key, {...buffer}, store);
              return;
            }
            case 'removed': {
              delete buffer[event.id];
              ready && _setSessionValue(key, {...buffer}, store);
              return;
            }
            case 'ready': {
              ready = true;
              _setSessionValue(key, {...buffer}, store)
            }
          }
        })
      );
    };
  }

  /* Extra utilities */

  userHasPermission (condoId, permission) {
    const permissions = this.getValue('permissions');
    if (!permissions) {
      return false;
    } else {
      const condoPermissions = permissions[condoId];
      if (!condoPermissions) {
        return false;
      } else {
        return _.get(condoPermissions, permission) || false;
      }
    }
  }

  watchUserPermission (condosId, permission) {
    if (Array.isArray(condosId)) {
      const watchers = condosId.map(condoId =>
          this.watchUserPermission(condoId, permission).pipe(
            map(value => ({condoId, value}))
          )
      );
      // Single condo implementation
      const result = {};
      let ready = false;
      const allCondos = new Set();
      return merge.apply(null, watchers).pipe(
        map(({condoId, value}) => {
          if (!ready) {
            allCondos.add(condoId);
            if (allCondos.size === condosId.length) {
              ready = true;
            }
          }
          result[condoId] = value;
          return result;
        }),
        filter(() => ready)
      );
    } else {
      return this.watchValue('permissions').pipe(
        map(permissions => {
          if (!permissions) {
            return false;
          } else {
            const condoPermissions = permissions[condosId];
            if (!condoPermissions) {
              return false;
            } else {
              return _.get(condoPermissions, permission) || false;
            }
          }
        }),
        distinctUntilChanged()
      );
    }
  }

  watchMultiplePermission (condoIds, permissionList) {
    if (Array.isArray(condoIds)) {
      const watchers = condoIds.map(condoId => {
        return this.watchMultiplePermission(condoId, permissionList).pipe(
          map(value => ({condoId, value}))
        );
      });
      const result = {};
      let ready = false;
      const allCondos = new Set();
      return merge.apply(null, watchers).pipe(
        map(({condoId, value}) => {
          result[condoId] = value;
          if (ready) { return result; }
          allCondos.add(condoId);
          ready = allCondos.size === condoIds.length;
          return result;
        }),
        filter(() => ready)
      );
    }
    // Single condo implementation
    let tempState = permissionList.reduce((obj, key) => {
      obj[key] = null;
      return obj;
    }, {});
    let newState
    return this.watchValue('permissions').pipe(
      map(permissions => {
        if (!permissions) {
          newState = permissionList.reduce((obj, key) => {
            obj[key] = false;
            return obj;
          }, {});
          return permissionList.reduce((obj, key) => _.set(obj, key, false), {});
        } else {
          const condoPermissions = permissions[condoIds];
          if (!condoPermissions) {
            newState = permissionList.reduce((obj, key) => {
              obj[key] = false;
              return obj;
            }, {});
            return permissionList.reduce((obj, key) => _.set(obj, key, false), {});
          } else {
            newState = {};
            return permissionList.reduce((obj, key) => {
              const value = _.get(condoPermissions, key) || false;
              newState[key] = value;
              obj[key] = value;
              return obj;
            }, {});
          }
        }
      }),
      filter(() => {
        const hasChange = _.isEqual(tempState, newState);
        tempState = newState;
        newState = undefined;
        return !hasChange;
      })
    );
  }

  // validators
  validateContext () {
    const context = DDP._CurrentMethodInvocation.get() || DDP._CurrentPublicationInvocation.get();
    if (!context) {
      throw new Meteor.Error(400, 'not context available');
    }
    return context;
  }

  validateLogged () {
    const context = this.validateContext();
    const userId = context.userId;
    if (!userId) {
      throw new Meteor.Error(300, "Not Authenticated");
    }
    return userId;
  }
}
