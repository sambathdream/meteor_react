import {Observable, Subject, BehaviorSubject} from 'rxjs';
import {tap} from 'rxjs/operators';

let subCount = 0
let subsArray = [];

export function asCursor({collectionName, observeChanges, observe}) {
  return {
    pipe(...args) {
      observe && (observe = observe.pipe(...args));
      observeChanges && (observeChanges = observeChanges.pipe(...args));
      return this;
    },
    rename(collectionName) {
      this.collectionName = collectionName;
    },
    collectionName,
    observe: () => observe || observeChanges,
    observeChanges: () => observeChanges || observe
  }
}

export function cursorFromArray(collectionName, array) {
  const observable = Observable.create(subscriber => {

    array.forEach(e => {
      subscriber.next({id: e._id, type: 'added', fields: e})
    });
    subscriber.next({type: 'ready'});
  });
  observable.collectionName = collectionName;
  return observable
}

export function subjectForCursor (collectionName) {
  const subject = new Subject();
  subject.collectionName = collectionName;
  return subject;
}

export class BroadcasterCursor {
  constructor (collectionName) {
    this.collectionName = collectionName;
    this.broadcaster = new Subject();
    this.items = {};
  }

  set(id, fields) {
    const type = this.items[id] ? 'changed' : 'added';
    if (type === 'changed' && _.isEqual(fields, this.items[id])) {
      return
    }
    this.items[id] = {...this.items[id], ...fields};
    this.broadcaster.next({ type, id, fields });
  }

  get(id) {
    return Object.assign({}, this.items[id])
  }

  getAll() {
    return this.items
  }

  remove(id) {
    delete this.items[id];
    const type = 'removed';
    this.broadcaster.next({ type, id });
  }

  observe () {
    return Observable.create(subscriber => {
      const keys = Object.keys(this.items);
      for (const id of keys) {
        const fields = this.items[id];
        subscriber.next({id, fields, type: 'added'});
      }
      subscriber.next({type: 'ready'});
      const handle = this.broadcaster.subscribe(({id, type}) => {
        subscriber.next({id, type, fields: this.items[id]})
      });
      return handle;
    })
  }

  observeChanges () {
    return Observable.create(subscriber => {
      const keys = Object.keys(this.items);
      for (const id of keys) {
        const fields = this.items[id];
        subscriber.next({id, fields, type: 'added'});
      }
      subscriber.next({type: 'ready'});
      const handle = this.broadcaster.subscribe(({id, type, fields}) => {
        subscriber.next({id, type, fields})
      })
      return handle;
    });
  }
}

export function pipeToSubscription(context, ...cursors) {
  let readyCount = 0;
  cursors = _.flatten(cursors);
  const totalCount = cursors.length;

  const handlers = cursors.map(cursor => {
    if (!cursor) {
      readyCount += 1;
      return;
    }
    const subscription = cursor.observeChanges ? cursor.observeChanges() : cursor;
    if (cursor.collectionName) {
      return subscription
        .subscribe(event => {
          let readySent = false;
          if (event) {
            switch (event.type) {
              case 'added': return context.added(cursor.collectionName, event.id, event.fields);
              case 'changed': return context.changed(cursor.collectionName, event.id, event.fields);
              case 'removed': return context.removed(cursor.collectionName, event.id, event.fields);
              case 'ready':
                if (readySent) { return; }
                readySent = true;
                ++readyCount === totalCount && context.ready()
            }
          }
        });
    } else {
      readyCount += 1;
      return subscription.subscribe();
    }
  });
  context.onStop(() => handlers.forEach(h => h && h.unsubscribe()));
}

export class RxSubjectCursor {
  constructor (name) {
    this.collectionName = name;
    this.subject = new Subject();
    this.pipeArgs = [];
  }

  rename (name) {
    this.collectionName = name;
  }

  observe () {
    return this.subject.pipe(...this.pipeArgs);
  }

  observeChanges () {
    return this.subject.pipe(...this.pipeArgs);
  }

  pipe (...args) {
    this.pipeArgs = args;
    return this;
  }

  next (event) {
    this.subject.next(event);
  }
}

export class RxDerivedCursor {
  constructor (name, source) {
    this.collectionName = name;
    this.source = source || (new BehaviorSubject({type: 'ready'}));
    this.subscribers = new Map();
    this.pipeArgs = [];
  }

  rename (name) {
    this.collectionName = name;
  }

  observe () {
    if (!this._handler) {
      this._handler = Observable.create(subscriber => {
        this._subscribe(subscriber);
        return () => {
          this._unsubscribe(subscriber);
        }
      });

    }
    return this._handler.pipe(...this.pipeArgs);
  }

  observeChanges () {
    return this.observe();
  }

  pipe (...args) {
    this.pipeArgs = args;
    return this;
  }

  setSource (source) {
    this.source = source;
    for (const subscriber of this.subscribers.keys()) {
      const oldHandler = this.subscribers.get(subscriber);
      if (oldHandler) {
        oldHandler.unsubscribe();
      }
      this._subscribe(subscriber);
    }
  }

  _subscribe (subscriber) {
    let handler
    if (this.source) {
      const observable = this.source.observeChanges ? this.source.observeChanges() : this.source;
      handler = observable.subscribe(this._handleEvent(subscriber));
    }
    this.subscribers.set(subscriber, handler);
  }

  _unsubscribe (subscriber) {
    const handler = this.subscribers.get(subscriber);
    this.subscribers.delete(subscriber);
    if (handler) {
      handler.unsubscribe();
    }
  }

  _handleEvent (subscriber) {
    return function (event) {
      subscriber.next(event);
    }
  }
}

export class RxCursor {
  constructor (cursor) {
    this.cursor = cursor;
    this.collectionName = cursor._cursorDescription.collectionName;
    this.derivedCursors = [];
    this.pipeArg = [];
  }

  rename(collectionName) {
    this.collectionName = collectionName;
  }

  deriveCursor (collectionName, callback) {
    const derivedCursor = new RxDerivedCursor(collectionName);
    this.derivedCursors.push({ cursor: derivedCursor, callback});
    return derivedCursor;
  }

  pipe(...args) {
    this.pipeArg = [...this.pipeArg, ...args];
    return this;
  }

  fetch () {
    return new Promise((resolve) => {
      const result = [];
      const handle = this.observeChanges().subscribe(({id, type, fields}) => {
        switch(type) {
          case 'added': {
            result.push({
              ...fields,
              _id: id
            });
            break;
          }
          case 'ready': {
            handle.unsubscribe();
            resolve(result);
          }
        }
      });
    });
  }

  fetchMap () {
    return new Promise((resolve) => {
      const result = {};
      const handle = this.observeChanges().subscribe(({id, type, fields}) => {
        switch(type) {
          case 'added': {
            result[id] = {
              ...fields,
              _id: id
            };
            break;
          }
          case 'ready': {
            handle.unsubscribe();
            resolve(result);
          }
        }
      });
    });
  }

  observe () {
    if (!this._observe) {
      this._observe = this._handleFor('observe', observer => ({
        addedAt: (document, atIndex, before) => observer.next({ type: 'added', document, atIndex, before }),
        changedAt: (document, oldDocument, atIndex) => observer.next({ type: 'changed', document, oldDocument, atIndex }),
        removedAt: (document, atIndex) => observer.next({ type: 'removed', document, atIndex })
      }));
    }
    return this._observe;
  }

  observeChanges () {
    if (!this._observeChanges) {
      this._observeChanges = this._handleFor('observeChanges', observer => ({
        addedBefore: (id, fields, before) => observer.next({type: 'added', id, fields, before}),
        changed: (id, fields) => observer.next({type: 'changed', id, fields}),
        movedBefore: (id, before) => observer.next({type: 'moved', id, before}),
        removed: (id) => observer.next({type: 'removed', id}),
      }));
    }
    return this._observeChanges;
  }

  _handleDerivedCursor (event, {cursor, callback}) {
    const newCursor = callback(event);
    if (newCursor) {
      cursor.setSource(newCursor);
    }
  }

  _handleFor(observeType, callbacks) {
    return Observable.create((observer) => {
      let handle;
      let shouldStop = false;
      (async () => {
        handle = this.cursor[observeType](callbacks(observer));
        subCount += 1;
        subsArray.push(this.collectionName);
        if (shouldStop) {
          subCount -= 1;
          let index = subsArray.indexOf(this.collectionName);
          if (index > -1) {
            subsArray.splice(index, 1);
          }
          handle.stop();
        }
        observer.next({type: 'ready'});
      })();
      return () => {
        if (handle) {
          subCount -= 1;
          let index = subsArray.indexOf(this.collectionName);
          if (index > -1) {
            subsArray.splice(index, 1);
          }
          handle && handle.stop();
        } else {
          shouldStop = true;
        }
      }
    }).pipe(
      tap(event => this.derivedCursors.map(cur => this._handleDerivedCursor(event, cur))),
      ...this.pipeArg
    );
  }
}

export class RxCollection {

  constructor (collection) {
    this.collection = collection;
  }

  find(q, p) {
    const cursor = this.collection.find(q, p);
    return new RxCursor(cursor);
  }
}
