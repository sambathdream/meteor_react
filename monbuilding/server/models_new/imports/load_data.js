import {cursorFromArray} from './collection-observer';
const COUNT_MULTIPLIER = 5;

export class DataLoader {
  constructor(context, session, options) {
    this.context = context
    this.session = session
    this.name = options.name
    this.Collection = options.Collection
    this.field = options.field
    this.query = options.query || {}
    this.condoId = options.condoId || 'all'
    this.pageSize = options.pageSize
    this.restart = options.restart
    this.sortField = options.sortField
    this.filter = options.filter

    if (this.condoId === 'all') {
      this.condoList = session.getValue('condos')
    }
    this._loadDataFromSession(this.restart)

  }

  _loadDataFromSession (restart) {
    if (!restart) {
      this.current = this.session.getValue(`${this.name}-current${this.condoId}`);
      this.counters = this.session.getValue(`${this.name}-counters${this.condoId}`);
      this.indexes = this.session.getValue(`${this.name}-indexes${this.condoId}`);
      this.pages = this.session.getValue(`${this.name}-pages${this.condoId}`);
      this.completed = this.session.getValue(`${this.name}-completed${this.condoId}`);
      this.timestamp = this.session.getValue(`${this.name}-timestamp${this.condoId}`);
    }
    if (!this.current) {
      this.current = [];
      this.session.setValue(`${this.name}-current${this.condoId}`, this.current);
    }
    if (!this.counters) {
      this.counters = 0;
      this.session.setValue(`${this.name}-counters${this.condoId}`, this.counters);
    }
    if (!this.indexes) {
      this.indexes = [];
      this.session.setValue(`${this.name}-indexes${this.condoId}`, this.indexes);
    }
    if (!this.pages) {
      this.pages = [];
      this.session.setValue(`${this.name}-pages${this.condoId}`, this.pages);
    }
    if (!this.completed) {
      this.completed = false;
      this.session.setValue(`${this.name}-completed${this.condoId}`, this.completed);
    }
    if (!this.timestamp) {
      this.timestamp = Date.now();
      this.session.setValue(`${this.name}-timestamp${this.condoId}`, this.timestamp);
    }
  }

  async getPage (page) {
    const indexes = await this.getPageIndexes(page)
    const data = this.Collection.find(
      {_id: { $in: indexes.map(({_id}) => _id)}},
      {fields: this.filter}
    );
    const padding = this.pageSize * page;
    const elements = {_id: this.condoId};
    for (let i = 0; i < indexes.length; i += 1) {
      const idx = indexes[i];
       elements[padding + i] = {itemId: idx._id, date: idx[this.sortField]};
    }
    const index = cursorFromArray(this.name, [elements])
    return { index, data }
  }

  async getPageIndexes (page) {
    let pages = this.pages;
    if (!pages[page]) {
      const count = this.pageSize * (page + 1);
      const updatedPages = [...pages];


      const completed = this.completed;
      const Collection = this.Collection;
      let indexes = this.indexes;
      let current = this.current;
      let counters = this.counters;
      const sortField = this.sortField;
      const condoId = this.condoId;
      const query = this.query;

      if (!completed && current.length - counters < count) {
        const newItems = await Collection.find(
          {
            ...query,
            _id: { $nin: indexes },
            condoId: condoId === 'all' ? { $in: this.condoList } : condoId,
          },
          {fields: {[sortField]: 1}, sort: {[sortField]: -1}, limit: count + this.pageSize * COUNT_MULTIPLIER}
        ).fetch();
        if (newItems.length < this.pageSize) {
          this.session.setValue(`${this.name}-completed${this.condoId}`, true)
          this.completed = true
        }
        current = [...current, ...newItems]
        indexes = [...indexes, ...newItems.map(i => i._id)]
      }

      for (let currentPage = pages.length; currentPage <= page; currentPage += 1) {
        const pageItems = [];
        for (let i = 0; i < this.pageSize; i += 1) {
          let minItem = null;
          let minVal = 0;
          const item = current[counters];
          if (item && item[sortField] > minVal) {
            minItem = item;
            minVal = item.date;
          }
          if (minItem) {
            counters += 1;
            pageItems.push(minItem);
          } else {
            break;
          }
        }
        updatedPages[currentPage] = pageItems;
      }
      this.session.setValue(`${this.name}-current${this.condoId}`, current);
      this.session.setValue(`${this.name}-indexes${this.condoId}`, indexes);
      this.session.setValue(`${this.name}-counters${this.condoId}`, counters);
      this.session.setValue(`${this.name}-pages${this.condoId}`, updatedPages);
      this.pages = updatedPages;
      pages = updatedPages;
    }
    return pages[page];
  }


}
