import { Observable } from 'rxjs';
import { RxCollection } from './collection-observer';
export function queryForUserViews (userId) {
  return {
    userUpdateView: { $elemMatch: { $eq: userId}},
    userViews: { $elemMatch: { $eq: userId}},
    participants: { $elemMatch: { $eq: userId}},
    updatedAt: 1
  }
}

export function markPostAsUnead (userId, keep = false) {
  const UserLastReadAll$ = new RxCollection(UserLastReadAll);
  return function (source) {
    return Observable.create(subscriber => {
      const docs = {}
      let lastDate = 0
      const sourceHandler = source.subscribe(({id, type, fields}) => {
        if (fields) {
          const update = {}
          if (fields.userUpdateView) { update.userUpdateView = fields.userUpdateView.length > 0 }
          if (fields.userViews) { update.userViews = fields.userViews.length > 0 }
          if (fields.participants) { update.participants = fields.participants.length > 0 }
          if (fields.updatedAt) { update.updatedAt = fields.updatedAt }
          if (!keep) {
            delete fields.userUpdateView
            delete fields.userViews
            delete fields.participants
          }
          docs[id] = {
            ...docs[id],
            ...update
          }
          fields.unread = update.updatedAt > lastDate && (!docs[id].userUpdateView && (fields.participants || !fields.userViews))
        }
        subscriber.next({id, type, fields})
      })
      const lastWatchAllHandler = UserLastReadAll$.find({_id: userId}, {fields: {forum: 1}}).observeChanges()
        .subscribe(({id, type, fields}) => {
          if (fields && fields.hasOwnProperty('forum')) {
            lastDate = fields['forum']
            for (const docId of Object.keys(docs)) {
              if (docs[docId].updatedAt < lastDate) {
                const unread = !docs[docId].userUpdateView && (fields.participants || !fields.userViews)
                if (unread) {
                  subscriber.next({id: docId, type: 'changed', fields: {unread: false}})
                }
              }
            }
          }
        })
      return function () {
        sourceHandler.unsubscribe()
        lastWatchAllHandler.unsubscribe()
      }
    })
  }
}
