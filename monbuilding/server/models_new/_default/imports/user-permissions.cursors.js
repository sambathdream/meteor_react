import { RxCollection, asCursor } from '../../imports/collection-observer'
import { merge, Observable } from 'rxjs'
import { tap } from 'rxjs/operators'

function module2right(module) {
  switch (module) {
    case 'digitalWallet': return 'wallet';
    case 'incident': return 'incidents';
    case 'actuality': return 'informations';
    case 'reservation': return 'reservations';
    case 'classifieds': return 'ads';
    case 'marketPlace': return 'marketPlace';
    case 'print': return 'print';
    // TODO: Fix concierge right
    case 'view': return 'conciergerie';
    default: return module;
  }
}

export function userPermissions (userId, condoIds) {
  const UserRight$ = new RxCollection(UsersRights);
  const DefaultRight$ = new RxCollection(DefaultRoles);
  const Condo$ = new RxCollection(Condos);
  const CondoRight$ = new RxCollection(CondoRole);

  const userRights = UserRight$.find(
    { userId: userId, condoId: {$in: condoIds}},
    { condoId: true, defaultRoleId: true, module: true }
  ).pipe(
    parseUserRights(DefaultRight$)
  );

  const defaultRights = DefaultRight$.find(
    {},
    { fields: { rights: 1 } }
  ).pipe(parseDefaultRights());

  const condoRights = CondoRight$.find(
    {condoId: {$in: condoIds}},
    {fields: {condoId: 1, rights: 1, defaultRoleId: 1}}
  ).pipe(parseCondoRights())

  const condos = Condo$.find(
    {_id: {$in: condoIds}},
    {fields: { 'settings.options': 1 }},
  ).pipe(parseCondoModules());

  const rightsSub = merge(
    userRights.observeChanges(),
    defaultRights.observeChanges(),
    condos.observeChanges(),
    condoRights.observeChanges()
  ).pipe(
    mergeUserRights(),
  );

  return asCursor({
    collectionName: 'UserPermissions',
    observeChanges: rightsSub
  });
}

// helpers
function mergeUserRights () {
  return function (source) {
    let readyCount = 0;
    const condoModules = {};
    const userRights = {};
    const roleRights = {};
    const condoRights = {};
    const condoRoles = {};
    const lastValue = {};

    return Observable.create(subscriber => {
      function updateSub (id) {
        if (condoModules[id] && userRights[id] && roleRights[ userRights[id].__defaultRoleId ]) {
          const roleId = userRights[id].__defaultRoleId
          const type = lastValue[id] ? 'changed' : 'added';
          const rights = {
            ...roleRights[ roleId ],
            ...condoRights[`${id}%${roleId}`],
            ...userRights[id]
          };
          delete rights.__defaultRoleId;
          const finalRights = applyModulesToRights(condoModules[id], rights);
          if (!_.isEqual(finalRights, lastValue[id])) {
            lastValue[id] = finalRights;
            subscriber.next({type, id, fields: finalRights});
          }
        }
      }
      const handler = source.subscribe(({id, type, fields}) => {
        if (type === 'ready' && ++readyCount === 4) {
          subscriber.next({type});
        }
        if (type === 'removed') {
          // TODO: Should not ignore
        }
        if (!fields) { return; }
        const {role, user, modules, condo} = fields;
        if (!role && !user && !modules && !condo) { return; }
        if (user) {
          if (user.__defaultRoleId) {
            const roleId = user.__defaultRoleId;
            const oldRights = userRights[id];
            const oldRoleId = oldRights && oldRights.__defaultRoleId;
            if (oldRoleId && oldRoleId !== roleId) {
              condoRoles[oldRoleId].delete(id);
            }
            if (!condoRoles[roleId]) { condoRoles[roleId] = new Set(); }
            condoRoles[roleId].add(id);
          }
          userRights[id] = {
            __defaultRoleId: userRights[id] ? userRights[id].__defaultRoleId : undefined,
            ...user
          };
          updateSub(id);
        }
        if (modules) {
          condoModules[id] = modules;
          updateSub(id);
        }
        if (condo) {
          condoRights[id] = condo.condoRights
          updateSub(condo.condoId)
        }
        if (role) {
          roleRights[id] = role;
          if (!condoRoles[id]) { return; }
          for (const condoId of condoRoles[id]) {
            updateSub(condoId);
          }
        }
      });
      return handler;
    });
  };
}

function parseCondoModules () {
  return function (source) { return Observable.create(subscriber => {
    const handler = source.subscribe(({id, type, fields}) => {
      if (type === 'ready') { subscriber.next({type}); }
      if (type === 'removed') { subscriber.next({id, type}); }
      if (!fields || !fields.settings || !fields.settings.options) { return; }
      const options = fields.settings.options;
      const modules = {};
      for (const moduleName of Object.keys(options)) {
        const rightName = module2right(moduleName);
        modules[rightName] = options[moduleName];
      }
      subscriber.next({id, type, fields: {modules}});
    });
    return handler;
  }); };
}

function parseDefaultRights () {
  return function (source) { return Observable.create(subscriber => {
    const handler = source.subscribe(({id, type, fields}) => {
      if (type === 'ready') { subscriber.next({type}); }
      if (type === 'removed') { subscriber.next({id, type}); }
      if (!fields) { return; }
      const {rights} = fields;
      if (!rights) { return; }
      const role = defaultRoleMapper(rights);
      subscriber.next({id,type, fields: {role}});
    });
    return handler;
  }); }
}

function parseCondoRights () {
  return function (source) { return Observable.create(subscriber => {
    const idmapper = {}
    const handler = source.subscribe(({id, type, fields}) => {
      if (type === 'ready') { subscriber.next({type}); }
      if (type === 'removed') { subscriber.next({id: idmapper[id], type}); }
      if (!fields) { return; }
      const {rights, condoId, defaultRoleId} = fields;
      if (condoId && defaultRoleId) { idmapper[id] = `${condoId}%${defaultRoleId}`}
      const realId = idmapper[id]
      if (!rights) { return; }
      const condoRights = defaultRoleMapper(rights);
      subscriber.next({id: realId, type, fields: {condo: { condoId, condoRights }}});
    });
    return handler;
  }); }
}

function parseUserRights () {
  const condoEventMap = {};
  return function (source) { return Observable.create(subscriber => {
    const handler = source.subscribe(({id, type, fields}) => {
      if (type === 'ready') { return subscriber.next({type}); }
      if (!fields) { return; }
      let {condoId, module, defaultRoleId} = fields;
      if (condoId) { condoEventMap[id] = condoId; }
      condoId = condoEventMap[id];
      if (type === 'removed') { return subscriber.next({id, type}); }
      if (!module && !defaultRoleId) { return; }
      let user = {};
      if (module) { user = userRightsMapper(module); }
      if (defaultRoleId) { user.__defaultRoleId = defaultRoleId; }
      subscriber.next({id: condoId, type, fields: {user}});
    });
    return handler;
  });};
}

function userRightsMapper (userRights) {
  const rights = {};
  Object.keys(userRights).forEach(module => {
    Object.keys(userRights[module]).forEach((right) => {
      rights[`${module}.${right}`] = userRights[module][right];
    });
  });
  return rights;
}

function defaultRoleMapper (roleRights) {
  const rights = {};
  for (const item of roleRights) {
    const {module, right} = item;
    rights[`${module}.${right}`] = item.default;
  }
  return rights;
}

function applyModulesToRights (modules, rights) {
  const newRights = {...rights};
  for (const right of Object.keys(rights)) {
    const moduleName = module2right(right.split('.')[0]);
    // TODO: The fix (... || true) is for Manager missing modules:
    // setContact
    // managerList
    // integrations
    newRights[right] = !!(modules[moduleName] !== false && newRights[right]);
  }
  return newRights;
}
