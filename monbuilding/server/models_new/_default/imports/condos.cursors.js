import { RxCollection, asCursor } from '../../imports/collection-observer'
import {map} from 'rxjs/operators';
import { Observable, merge } from 'rxjs';
import {enterpriseByIdCursor} from '../../imports/cursors/enterprise.cursors';

export function userCondosObserver(userId) {
  const Resident$ = new RxCollection(Residents);

  const residentCondosObserver = Resident$.find({userId: userId}, { fields: { userId: 1, 'condos.condoId': 1}})
    .pipe(getCondosFromResident()).observeChanges();

  const managerCondosCursor = enterpriseByIdCursor([userId])
    .pipe(getCondosFromManager()).observeChanges();

  return merge(
    residentCondosObserver,
    managerCondosCursor
  )
}

function getCondosFromResident () {
  let condos
  return function (source) {
    return Observable.create(subscriber => {
      var subscription = source.subscribe(({fields}) => {
        if (fields && fields.condos) {
          const newCondos = fields.condos.map(condoData => condoData.condoId).sort();
          if (!_.isEqual(condos, newCondos)) {
            condos = newCondos
            subscriber.next([...condos]);
          }
        }
      },
      err => subscriber.error(err),
      () => subscriber.complete());
      return subscription;
    });
  }
}

function getCondosFromManager () {
  let condos
  return function (source) {
    return Observable.create(subscriber => {
      var subscription = source.subscribe(({fields}) => {
        if (fields && fields.condosInCharge) {
          const newCondos = fields.condosInCharge.map(condoData => condoData.condoId).sort();
          if (!_.isEqual(condos, newCondos)) {
            condos = newCondos
            subscriber.next([...condos]);
          }
        }
      },
      err => subscriber.error(err),
      () => subscriber.complete());
      return subscription;
    });
  }
}
