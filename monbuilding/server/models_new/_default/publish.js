import {usersCursor} from '../imports/cursors/user.cursors';
import {userCondosObserver} from './imports/condos.cursors';
import { userPermissions } from './imports/user-permissions.cursors'
import {RxCollection, RxDerivedCursor, RxSubjectCursor, pipeToSubscription, subjectForCursor} from '../imports/collection-observer';
import {tap, map} from 'rxjs/operators';

import { Session } from '../imports/session';
import { Observable } from 'rxjs';


Meteor.startup(() => {

/*
 * Default subscription set base values for both client & backend (using session)
 */
Meteor.publish(null, defaultSubscription);
Meteor.publish('facebook-default', defaultSubscription);
Meteor.publish('old-data', oldDefaultSubscription);

});

function oldDefaultSubscription () {
  const Condo$ = new RxCollection(Condos);
  const Resident$ = new RxCollection(Residents);
  const Enterprise$ = new RxCollection(Enterprises);
  const residentCursor = Resident$.find({userId: this.userId});
  const managerCursor = Enterprise$.find({"users.userId": this.userId});

  // condos observer
  const condosCursor = new RxDerivedCursor('condos');
  const condosObserver = userCondosObserver(this.userId).pipe(
    tap(condos => {
      condosCursor.setSource(Condo$.find({_id: {$in: condos}}));
    })
  );

  pipeToSubscription(this,
    residentCursor,
    managerCursor,
    condosCursor,
    //
    condosObserver
  );
}


function defaultSubscription () {
  const Condo$ = new RxCollection(Condos);
  const Resident$ = new RxCollection(Residents);
  const CondosModulesOption$ = new RxCollection(CondosModulesOptions);
  // session
  const session = new Session();
  session.bindToPublish(this);

  if (this.userId) {
    let role
    const userCursor = usersCursor(this.userId).pipe(
      tap(event => {
        if (event.fields && event.fields.identities) {
          let newRole
          if (event.fields.identities.residentId) {
            newRole = 'occupant';
          } else if (event.fields.identities.gestionnaireId) {
            newRole = 'manager';
          }
          if (newRole !== role) {
            role = newRole;
            session.setValue('role', role);
          }
        }
      })
    );

    // condo derived cursors
    const condosCursor = new RxDerivedCursor('condos');
    const moduleSettings = new RxDerivedCursor('moduleSettings').pipe(
      mapModuleSettings,
      session.syncFromCursor('moduleSettings')
    );
    const permissionsCursor = new RxDerivedCursor('permissions').pipe(
      session.syncFromCursor('permissions')
    );

    // resident observer
    const residentCursor = Resident$.find({ userId: this.userId });
    // condos observer
    const condosObserver = userCondosObserver(this.userId).pipe(
      tap(condos => {
        session.setValue('condos', condos);
        condosCursor.setSource(Condo$.find({_id: {$in: condos}},
          { fields: {name: 1, info: 1, 'settings.options': 1, 'settings.colorPalette': 1} }
        ));
        moduleSettings.setSource(CondosModulesOption$.find({condoId: {$in: condos}}))
        permissionsCursor.setSource(userPermissions(this.userId, condos));
      })
    );

    // available modules
    const availableModulesSubject = new RxSubjectCursor('availableModules').pipe(
      session.syncFromCursor('condoModules')
    );
    const condoHasColor = {}
    const condoPaletteSubject = new RxSubjectCursor('condoPalette')
    condosCursor.pipe(
      map(event => {
        if (event.fields && event.fields.settings && event.fields.settings.options) {
          const options = event.fields.settings.options;
          const colorPalette = event.fields.settings.colorPalette;
          delete event.fields.settings;
          availableModulesSubject.next({
            type: event.type,
            id: event.id,
            fields: options
          });
          switch (event.type) {
            case 'ready':
            case 'removed':
              condoPaletteSubject.next({
                type: event.type,
                id: event.id
              })
              break
            default:
              if (!colorPalette) { break }
              const type = condoHasColor[event.id] ? 'changed' : 'added'
              condoHasColor[event.id] = true
              condoPaletteSubject.next({
                type,
                id: event.id,
                fields: colorPalette
              })
          }
        } else if (event.type === 'ready') {
          availableModulesSubject.next({type: 'ready'});
        }
        return event;
      })
    );

    pipeToSubscription(this,
      permissionsCursor,
      availableModulesSubject,
      condoPaletteSubject,
      condosCursor,
      userCursor,
      moduleSettings,
      residentCursor,
      // condosObserver doesn't feed client
      condosObserver
    );
  }
  else {
    return null;
  }
}


function mapModuleSettings (source) {
  return Observable.create(function (subscriber) {
    const idMapper = {};
    const handler = source.subscribe(({id, type, fields}) => {
      if (fields) {
        if (fields.condoId) {
          idMapper[id] = fields.condoId
          delete fields.condoId
        }
      }
      subscriber.next({id: idMapper[id], type, fields})
    })
    return handler;
  });
}
