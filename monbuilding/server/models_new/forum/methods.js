Meteor.startup(function() {
	Meteor.methods({
    markAllForumPostAsRead () {
      UserLastReadAll.upsert({_id: this.userId}, {
        $set: {
          forum: Date.now()
        }
      })
    }
  })
})
