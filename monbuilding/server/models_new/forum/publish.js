import {DataLoader} from '../imports/load_data';
import {DataCollector} from '../imports/collect_extra_data';
import {queryForUserViews, markPostAsUnead} from '../imports/post_views';
import { Session } from '../imports/session';
import { RxCollection, pipeToSubscription } from '../imports/collection-observer';
import { Observable } from 'rxjs';

Meteor.startup(() => {
  Meteor.publish('forum-feed', forumFeed());
  Meteor.publish('myforum-feed', forumFeed(true));
});

function forumFeed (myForum) {
  const ForumPost$ = new RxCollection(ForumPosts);
  const User$ = new RxCollection(Meteor.users);
  const ForumCategorie$ = new RxCollection(ForumCategories);

  return function({pageNumber, condoId, filterCriteria} = {}) {
    let query
    let name
    if (myForum) {
      query = {createdBy: this.userId}
      name ='myforum-feed'
    } else {
      query = {}
      name ='forum-feed'
    }

    const session = new Session();
    // validate user
    session.validateLogged();
    const restart = pageNumber === 0;
    filterCriteria = filterCriteria || {};

    let searchText
    let searchCategory
    if (filterCriteria.searchText && filterCriteria.searchText !== '') {
      searchText = {subject: { $regex: filterCriteria.searchText, $options: 'i'}}
    }
    if (filterCriteria.categories && filterCriteria.categories.length > 0) {
      searchCategory = {
        categoriesId: { $elemMatch: { $in: filterCriteria.categories}}
      }
    }

    const condos = !condoId || condoId === 'all' ? session.getValue('condos') : [condoId]
    const availableModules = session.getValue('condoModules')
    const condoQueryContent = condos.map(condoId => {
      const canSeePoll = session.userHasPermission(condoId, 'forum.seePoll')
      const canSeeForum = session.userHasPermission(condoId, 'forum.seeSubject')
      if ((!canSeePoll && !canSeeForum) || !(availableModules[condoId] || {})['forum']) {
        return {
          condoId: {$ne: condoId},
        }
      }
      if (!canSeePoll && canSeeForum) {
        return {
          condoId,
          type: 'post'
        }
      }
      if (canSeePoll && !canSeeForum) {
        return {
          condoId,
          type: 'poll'
        }
      }
      return null
    }).filter(q => q !== null)

    const condoQuery = condoQueryContent.length > 0 ? {$or: condoQueryContent} : {}

    const userLoader = new DataCollector(User$, 'users', {avatar: 1, 'profile.firstname': 1, 'profile.lastname': 1, status: 1});
    const forumLoader = new DataLoader(this, session, {
      name,
      Collection: ForumPost$,
      sortField: '_id',
      condoId,
      pageSize: 40,
      query: {
        ...query,
        ...searchText,
        ...searchCategory,
        ...condoQuery
      },
      sortField: 'updatedAt',
      filter: {
        createdBy: 1,
        editedAt: 1,
        subject: 1,
        type: 1,
        endDate: 1,
        endHour: 1,
        inputs: 1,
        isEditable: 1,
        condoId: 1,
        votes: 1,
        viewsCount: 1,
        commentCount: 1,
        attachments: 1,
        ...queryForUserViews(this.userId)
      },
      restart
    });

    const forumCategories = ForumCategorie$.find({
      $or: [
        {condoId},
        {condoId: 'default'}
      ]
    }).pipe(mapCategory(condoId));

    (async () => {
      let {index, data} = await forumLoader.getPage(pageNumber)
      data = data.pipe(markPostAsUnead(this.userId))
      userLoader.collectDataFrom(data, ({createdBy}) => [createdBy]);
      pipeToSubscription(this,
        index,
        data,
        userLoader,
        restart && forumCategories
      );
    })();
  }
}

function mapCategory (condoId) {
  return function (source) {
    return Observable.create(subscriber => {
      const values = {};
      let ready = false;
      let once = false;
      const idMap = {};
      const handler = source.subscribe(({id, type, fields}) => {
        if (fields) {
          if (fields.condoId) {
            idMap[id] = fields.condoId
          }
          values[idMap[id]] = {...values[idMap[id]], ...fields}
          delete values[idMap[id]].condoId
        }
        if (type === 'ready') {
          ready = true;
        }
        if (type === 'removed') {
          delete values[idMap[id]]
          delete idMap[id]
        }
        if (!ready) { return; }
        const resultValue = values[condoId] || values['default'];
        subscriber.next({id: condoId, type: once ? 'changed' : 'added', fields: resultValue});
        if (!once) {
          subscriber.next({type: 'ready'});
          once = true;
          return;
        }
      });
      return handler;
    })
  }
}

