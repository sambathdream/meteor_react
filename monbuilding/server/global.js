/*
** Global function and vars for server side only
*/
Meteor.global = {
  email: {
    notification_delay_minutes: 2,

    email_titles: {
      incident: {
        reminderDeclare: "Rappel - Un incident déclaré n'a pas été validé",
        reminderValid: "Rappel - Un incident est en attente",
        newIncidentDeclared: "Un nouvel incident a été déclaré",
        newIncidentValid: "Un nouvel incident a été validé par votre gestionnaire",
        incidentSolved: "Un incident a été résolu",
        incidentReopen: "Un incident résolu a été réouvert par votre gestionnaire",
        incidentMessage: "Vous avez reçu un nouveau message de votre gestionnaire"
      }
    }
  },
  api: {
    release: "1.6.1",
    version: "1.1.0"
  }
};

if (Meteor.isServer) {
  if (Meteor.isDevelopment || process.env.ROOT_URL != "https://www.monbuilding.com/") {
    Meteor.settings = {
      oneSignal: {
        default: {
          'apiKey': 'NDk5OGI2YTEtOGFlZi00NDhhLTlhMDMtZmQzYmI0NDAwNDhj',
          'appId': '9b0a3b08-c116-4dd5-8bb6-7d77acdcd1aa'
        },
        MonBuilding: {
          'apiKey': 'NDk5OGI2YTEtOGFlZi00NDhhLTlhMDMtZmQzYmI0NDAwNDhj',
          'appId': '9b0a3b08-c116-4dd5-8bb6-7d77acdcd1aa'
        },
        StudentFactory: {
          'apiKey': 'NDgyNzI3ZWMtM2E1OC00M2EwLWE0MDctYTIwYjJkODE0MWY0',
          'appId': 'a5398147-3cb5-43d0-81a7-6d8341c19eb3'
        },
        Kley: {
          'apiKey': 'MDQ3NDJmMTgtNGU2MC00M2YyLThlMDktOTQzNTVhYTU0N2Nk',
          'appId': '22e15f9f-8e38-4a45-805a-a8a6ace603fa'
        },
        MonBuildingProd: {
          'apiKey': 'MjA0NTI2M2YtMDhhMC00YWZlLTgxNTYtN2FkM2M3OTM4N2Vm',
          'appId': 'e8a43c0d-fa03-4374-97b2-b9a3fc8b5953'
        }
      }
    };
  } else {
    Meteor.settings = {
      oneSignal: {
        StudentFactory: {
          'apiKey': 'NDgyNzI3ZWMtM2E1OC00M2EwLWE0MDctYTIwYjJkODE0MWY0',
          'appId': 'a5398147-3cb5-43d0-81a7-6d8341c19eb3'
        },
        Kley: {
          'apiKey': 'MDQ3NDJmMTgtNGU2MC00M2YyLThlMDktOTQzNTVhYTU0N2Nk',
          'appId': '22e15f9f-8e38-4a45-805a-a8a6ace603fa'
        },
        MonBuilding: {
          'apiKey': 'NDk5OGI2YTEtOGFlZi00NDhhLTlhMDMtZmQzYmI0NDAwNDhj',
          'appId': '9b0a3b08-c116-4dd5-8bb6-7d77acdcd1aa'
        },
        MonBuildingProd: {
          'apiKey': 'MjA0NTI2M2YtMDhhMC00YWZlLTgxNTYtN2FkM2M3OTM4N2Vm',
          'appId': 'e8a43c0d-fa03-4374-97b2-b9a3fc8b5953',
          'authKey': 'MjllMmE0NGYtMDVmYS00OTM5LThjZTItMDBjYWYwYjViMTJl'
        },
        default: {
          'apiKey': 'NDk5OGI2YTEtOGFlZi00NDhhLTlhMDMtZmQzYmI0NDAwNDhj',
          'appId': '9b0a3b08-c116-4dd5-8bb6-7d77acdcd1aa'
        }
      }
    };
  }
  Meteor.settings.jwtSecret = '@lolXD2704@'
}

Meteor.startup(function () {
  Meteor.methods({
    isConnected: function (userId) {
      if (userId) {
        var sockets = Meteor.default_server.stream_server.open_sockets;
        var ret = false;
        for (var i = 0; i < sockets.length; i++) {
          if (sockets && sockets[i] && sockets[i]._meteorSession && sockets[i]._meteorSession.userId && sockets[i]._meteorSession.userId === userId) {
            ret = true;
          }
        }
        if (ret)
          return ret;
        else
          return false;
      }
    },
    checkApiVersion: function (api_version) {
      if (api_version === Meteor.global.api.version) {
        return true
      } else {
        return false
      }
    }
  });
});
