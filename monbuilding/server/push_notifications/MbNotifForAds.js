
// ********************* //
//                       //
//          ADS          //
//                       //
// ********************* //

export function mbCreateNotifForAds(container) {
    let ads_notif = {
        new_ad: {
            en: 'New Ad: [TITLE]',
            fr: 'Nouvelle petite annonce : [TITLE]'
        },
        new_comment: {
            en: 'New comment on ads: [TITLE]',
            fr: 'Nouveau commentaire : [TITLE]'
        },
    }


    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for ads, container is incorrect.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let ads = ClassifiedsAds.findOne(container.dataId)
    if (!ads) {
        console.log('Ads not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = ads_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    contents.en = contents.en.replace(/\[TITLE\]/i, ads.title)
    contents.fr = contents.fr.replace(/\[TITLE\]/i, ads.title)
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
        ...container.data,
        type: container.type,
        condoId: condo._id,
        postId: ads._id
    }
    return {
        title,
        contents,
        data
    }
}
