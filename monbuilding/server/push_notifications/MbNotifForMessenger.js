
// ********************* //
//                       //
//       MESSENGER       //
//                       //
// ********************* //

//  /!\ /!\ /!\ /!\
//  /!\ /!\ /!\ /!\  use   container.dataId === MessageFeedId     for `new_message`
//  /!\ /!\ /!\ /!\
//  /!\ /!\ /!\ /!\  use   container.dataId === MessageId         for `add_occupant`
//  /!\ /!\ /!\ /!\
//  /!\ /!\ /!\ /!\  use   container.dataId === UserId            for `join_conversation`
//  /!\ /!\ /!\ /!\

export function mbCreateNotifForMessenger(container) {
  let messenger_notif = {
    new_message: {
      en: 'New message from [FIRSTNAME L]: [MESSAGE]',
      fr: 'Nouveau message de [FIRSTNAME L] : [MESSAGE]'
    },
    new_message_incident: {
      en: 'New incident message from an occupant: [PRIORITY][TYPE][TITLE] - [MESSAGE]',
      fr: 'Nouveau message incident d\'un occupant : [PRIORITY][TYPE][TITLE] - [MESSAGE]'
    },
    new_message_other: {
      en: 'New message from an occupant: [PRIORITY][TYPE][DETAILS] - [MESSAGE]',
      fr: 'Nouveau message d\'un occupant : [PRIORITY][TYPE][DETAILS] - [MESSAGE]'
    },
    add_occupant: {
      en: 'Added to a conversation with [LIST GROUP]',
      fr: 'Vous avez été ajouté(e) à une conversation entre [LIST GROUP]'
    },
    join_conversation: {
      en: '[FIRSTNAME L] joined the conversation',
      fr: '[FIRSTNAME L] a rejoint la conversation'
    },
  }

  let postId = null
  if (!(container && container.key && container.dataId && container.condoId)) {
    console.log('Impossible to create notif for messenger.')
    return null
  }
  let condo = Condos.findOne(container.condoId)
  if (!condo) {
    console.log('Condo not found.')
    return null
  }
  let title = {}
  title.en = condo.getName()
  title.fr = condo.getName()
  let contents = messenger_notif[container.key]
  if (!contents || !contents.en || !contents.fr) {
    console.log('Contents not set.')
    return null
  }

  function _getUserName(userId) {
    let user = Meteor.users.findOne(userId)
    if (user) {
      return user.profile.firstname + ' ' + user.profile.lastname[0] + '.'
    }
  }

  if (container.key === 'new_message') {
    let messageFeed = MessagesFeed.findOne(container.dataId)
    if (!messageFeed) {
      console.log('MessageFeed not found.')
      return null
    }
    let message = Messages.findOne(messageFeed.messageId)
    if (!message) {
      console.log('Conversation not found.')
      return null
    }
    postId = message._id
    contents.en = contents.en.replace(/\[FIRSTNAME L\]/i, _getUserName(messageFeed.userId))
    contents.fr = contents.fr.replace(/\[FIRSTNAME L\]/i, _getUserName(messageFeed.userId))
    contents.en = contents.en.replace(/\[MESSAGE\]/i, messageFeed.text)
    contents.fr = contents.fr.replace(/\[MESSAGE\]/i, messageFeed.text)
  }

  else if (container.key === 'new_message_incident') {
    let messageFeed = MessagesFeed.findOne(container.dataId)
    if (!messageFeed) {
      console.log('MessageFeed not found.')
      return null
    }
    let message = Messages.findOne(messageFeed.messageId)
    if (!message) {
      console.log('Conversation not found.')
      return null
    }

    if (!message.incidentId) {
      console.log('This message is not from an incident.')
      return null
    }

    let incident = Incidents.findOne({ _id: message.incidentId })
    postId = message._id

    function getPriority(priorityId, lang) {
      let priority = IncidentPriority.findOne({ _id: priorityId })
      if (priority && priority['value-' + lang]) {
        return priority['value-' + lang] + ' / '
      } else {
        return ''
      }
    }
    function getType(typeId, lang) {
      let type = IncidentDetails.findOne({ _id: typeId })
      if (type && type['value-' + lang]) {
        return type['value-' + lang] + ' / '
      } else {
        return ''
      }
    }
    contents.en = contents.en.replace(/\[TITLE\]/i, incident.info.title)
    contents.fr = contents.fr.replace(/\[TITLE\]/i, incident.info.title)
    contents.en = contents.en.replace(/\[PRIORITY\]/i, getPriority(incident.info.priority, 'en'))
    contents.fr = contents.fr.replace(/\[PRIORITY\]/i, getPriority(incident.info.priority, 'fr'))
    contents.en = contents.en.replace(/\[TYPE\]/i, getType(incident.info.type, 'en'))
    contents.fr = contents.fr.replace(/\[TYPE\]/i, getType(incident.info.type, 'fr'))
    contents.en = contents.en.replace(/\[MESSAGE\]/i, messageFeed.text)
    contents.fr = contents.fr.replace(/\[MESSAGE\]/i, messageFeed.text)
  }

  else if (container.key === 'new_message_other') {
    let messageFeed = MessagesFeed.findOne(container.dataId)
    if (!messageFeed) {
      console.log('MessageFeed not found.')
      return null
    }
    let message = Messages.findOne(messageFeed.messageId)
    if (!message) {
      console.log('Conversation not found.')
      return null
    }
    postId = message._id

    function getPriority(priorityId, lang) {
      let priority = IncidentPriority.findOne({ _id: priorityId })
      if (priority && priority['value-' + lang]) {
        return priority['value-' + lang] + ' / '
      } else {
        return ''
      }
    }
    function getType(typeId, lang) {
      let type = IncidentType.findOne({ _id: typeId })
      if (type && type['value-' + lang]) {
        return type['value-' + lang] + ' / '
      } else {
        return ''
      }
    }
    function getDetails(typeId, lang) {
      let type = DeclarationDetails.findOne({ _id: typeId })
      if (type && type['value-' + lang]) {
        return type['value-' + lang]
      } else {
        return ''
      }
    }
    contents.en = contents.en.replace(/\[PRIORITY\]/i, getPriority(message.urgent, 'en'))
    contents.fr = contents.fr.replace(/\[PRIORITY\]/i, getPriority(message.urgent, 'fr'))
    contents.en = contents.en.replace(/\[TYPE\]/i, getType(message.type, 'en'))
    contents.fr = contents.fr.replace(/\[TYPE\]/i, getType(message.type, 'fr'))
    contents.en = contents.en.replace(/\[DETAILS\]/i, getDetails(message.details, 'en'))
    contents.fr = contents.fr.replace(/\[DETAILS\]/i, getDetails(message.details, 'fr'))
    contents.en = contents.en.replace(/\[MESSAGE\]/i, messageFeed.text)
    contents.fr = contents.fr.replace(/\[MESSAGE\]/i, messageFeed.text)
  }

  else if (container.key === 'add_occupant') {
    if (!container.userAllreadyInConv) {
      console.log('`userAllreadyInConv` not found in `container`')
    }
    function _allUserInConv(users, lang) {
      if (users && Array.isArray(users)) {

        usersName = users.reduce((usersName, user) => {
          return usersName + _getUserName(user) + (users.indexOf(user) === (users.length - 2) ? lang === 'fr' ? ' et ' : ' and ' : ', ')
        }, '')
        if (usersName && usersName !== '') {
          usersName = usersName.slice(0, -2)
          return usersName
        } else {
          return '...'
        }
      } else {
        return '...'
      }
    }

    let message = Messages.findOne(container.dataId)
    if (!message) {
      console.log('Conversation not found.')
      return null
    }
    postId = message._id
    contents.en = contents.en.replace(/\[LIST GROUP\]/i, _allUserInConv(container.userAllreadyInConv, 'en'))
    contents.fr = contents.fr.replace(/\[LIST GROUP\]/i, _allUserInConv(container.userAllreadyInConv, 'fr'))
  }

  else if (container.key === 'join_conversation') {
    contents.en = contents.en.replace(/\[FIRSTNAME L\]/i, _getUserName(container.userToAdd))
    contents.fr = contents.fr.replace(/\[FIRSTNAME L\]/i, _getUserName(container.userToAdd))
    postId = container.dataId
  }
  let data = {
    target: container.data && container.data.target,
    type: container.type,
    postId: postId,
    condoId: condo._id
  }
  return {
    title,
    contents,
    data
  }
}
