
// ********************* //
//                       //
//        LAUNDRY        //
//                       //
// ********************* //

export function mbCreateNotifForLaundry(container) {
    let laundry_notif = {
        'machine-freed': {
            en: 'Your machine is now available',
            fr: 'Votre machine est disponible'
        },
        'any-machine-freed': {
          en: 'A machine is available',
          fr: 'Une machine est disponible'
      },
    }

    console.log('container', container)
    if (!(container && container.key && container.condoId)) {
        console.log('Impossible to create notif for the laundry.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = laundry_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
      type: container.type,
      condoId: container.condoId
    }
    console.log('FIREEE')
    return {
        title,
        contents,
        data
    }
}
