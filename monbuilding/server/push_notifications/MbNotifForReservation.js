
// ********************* //
//                       //
//      RESERVATION      //
//                       //
// ********************* //

export function mbCreateNotifForReservation(container) {
    let reservation_notif = {
        manager_reply: {
            en: '[APPROVED_OR_REFUSED] booking: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Réservation [APPROVED_OR_REFUSED] : [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        new_booking: {
            en: 'Booking confirmation: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Confirmation de la réservation: [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        reminder_booking: {
            en: 'Booking reminder: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Rappel de la réservation: [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        invit_booking: {
            en: 'You were added to the booking: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Vous avez été ajouté(e) à une réservation: [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        change_booking: {
            en: 'Booking date change: booking of[RESOURCE_NAME][RESERVATION_TITLE] is now scheduled on [DAY] [HOUR]',
            fr: 'Changement de la date de réservation: [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        cancel_booking: {
            en: 'Confirmation of booking cancellation: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Confirmation de l\'annulation de la réservation : [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        need_validation_booking: {
            en: 'Booking to approve: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Réservation à valider : [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
        cancel_pending_booking: {
            en: 'Booking to approve cancelled: [RESOURCE_NAME][RESERVATION_TITLE] on [DAY] [HOUR]',
            fr: 'Réservation à valider annulée : [RESOURCE_NAME][RESERVATION_TITLE], le [DAY] [HOUR]'
        },
    }


    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for reservation.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let reservation = Reservations.findOne({ 'eventId': container.dataId })
    if (!reservation) {
        console.log('Reservation not found.')
        return null
    }
    let resource = Resources.findOne({_id: reservation.resourceId})
    if (!resource) {
        console.log('Resource not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = reservation_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    contents = MbChangeContents(contents, resource, reservation)
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
        type: container.type,
        condoId: condo._id,
        postId: reservation._id,
        reservation: {
            _id: reservation._id
        }
    }
    return {
        title,
        contents,
        data
    }
}

function MbChangeContents(contents, resource, reservation) {
    // APPROVED_OR_REFUSED
    contents.en = contents.en.replace(/\[APPROVED_OR_REFUSED\]/i, reservation.rejected !== true ? 'Approved' : (!!reservation.pending? 'Refused': 'Canceled'))
    contents.fr = contents.fr.replace(/\[APPROVED_OR_REFUSED\]/i, reservation.rejected !== true ? 'approuvée' : (!!reservation.pending? 'refusée': 'annulée'))
    // RESOURCE_NAME
    contents.en = contents.en.replace(/\[RESOURCE_NAME\]/i, resource.type === 'edl' ? getResourceTypeByKey('edl', 'en') : resource.name)
    contents.fr = contents.fr.replace(/\[RESOURCE_NAME\]/i, resource.type === 'edl' ? getResourceTypeByKey('edl', 'fr') : resource.name)
    // RESERVATION_TITLE
    contents.en = contents.en.replace(/\[RESERVATION_TITLE\]/i, reservation.title ? ' - ' + reservation.title : '')
    contents.fr = contents.fr.replace(/\[RESERVATION_TITLE\]/i, reservation.title ? ' - ' + reservation.title : '')
    // DAY
    moment.locale('en')
    contents.en = contents.en.replace(/\[DAY\]/i, moment(reservation.start).format('dddd MMM Do'))
    moment.locale('fr')
    contents.fr = contents.fr.replace(/\[DAY\]/i, moment(reservation.start).format('dddd DD MMM'))
    // HOUR
    moment.locale('en')
    contents.en = contents.en.replace(/\[HOUR\]/i, reservation.isAllDay !== true ? moment(reservation.start).format('[at] HH:mm') : '')
    moment.locale('fr')
    contents.fr = contents.fr.replace(/\[HOUR\]/i, reservation.isAllDay !== true ? moment(reservation.start).format('[à] HH:mm') : '')

    return contents
}
