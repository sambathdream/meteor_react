
if (Meteor.isServer) {
    sendNotifForNewCondo = async function (userId, condoId) {
      let user = Meteor.users.findOne(userId)
      if (!user) {
        throw new Meteor.Error(404, 'User Not Found `sendNotifForNewCondo`')
      }
      let condo = Condos.findOne(condoId)
      if (!condo){
        throw new Meteor.Error(404, 'Condo Not Found `sendNotifForNewCondo`')
      }
      try {
        return await sendNotificationToUsers([userId], false, false, {
          title: {
              en: condo.getName(),
              fr: condo.getName()
          },
          contents: {
              en: 'Welcome to the application of the building!',
              fr: 'Bienvenue sur l\'espace de l\'immeuble !'
          },
          data: {
            type: 'condo_welcome',
            condoId: condoId
          }
        });
      } catch (e) {
        throw new Meteor.Error(406, 'Error: `sendNotifForNewCondo` fail ', e);
      }
    }
    sendNotifRemoveCondo = async function (userId, condoId) {
      let user = Meteor.users.findOne(userId)
      if (!user) {
        throw new Meteor.Error(404, 'User Not Found `sendNotifRemoveCondo`')
      }
      let condo = Condos.findOne(condoId)
      if (!condo) {
        throw new Meteor.Error(404, 'Condo Not Found `sendNotifRemoveCondo`')
      }
      try {
        return await sendNotificationToUsers([userId], false, false, {
          title: {
              en: condo.getName(),
              fr: condo.getName()
          },
          contents: {
              en: 'Your access has been removed from the building. We are sorry to see you go! ',
              fr: 'Vos accès à l\'espace de l\'immeuble ont été retirés. A la prochaine :) '
          },
        });
      } catch (e) {
        throw new Meteor.Error(406, 'Error: `sendNotifRemoveCondo` fail ', e)
      }
    }
    sendNotifForOccupantValidation = async function (newUserId, condoId) {
        let user = Meteor.users.findOne(newUserId)
        if (!user) {
          throw new Meteor.Error(404, 'User Not Found `sendNotifForOccupantValidation`')
        }
        let condo = Condos.findOne(condoId)
        if (!condo) {
          throw new Meteor.Error(404, 'Condo Not Found `sendNotifForOccupantValidation`')
        }

        // return await sendNotificationToUsers([userId], false, false, {

        let playersId = []

        let condoContact = CondoContact.findOne({ condoId: condo._id })
        let validEntryDetailId = DeclarationDetails.findOne({ key: "validEntry" })
        let validEntryCondoContact = condoContact && condoContact.contactSet && condoContact.contactSet.find(function (contact) {
            return contact.declarationDetailId == validEntryDetailId._id
        })
        let gestDefaultEntry
        let gestDefault = condoContact.defaultContact.userId
        if (validEntryCondoContact && validEntryCondoContact.userIds.length > 0) {
            gestDefaultEntry = _.without(validEntryCondoContact.userIds, null, undefined)
        }

        if (gestDefaultEntry && gestDefaultEntry.length > 0) {
          userIds = gestDefaultEntry
        } else {
          userIds = gestDefault
        }

        try {
          return await sendNotificationToUsers(userIds, false, false, {
            title: {
                en: condo.getName(),
                fr: condo.getName()
            },
            contents: {
                en: 'New occupant to validate: ' + user.profile.firstname + ' ' + user.profile.lastname,
                fr: 'Nouvel occupant à valider : ' + user.profile.firstname + ' ' + user.profile.lastname
            },
            playerIds: playersId
        });
        } catch (e) {
          throw new Meteor.Error(406, 'Error: `sendNotifRemoveCondo` fail ', e)
        }
    }
}
