
// ********************* //
//                       //
//       INCIDENT        //
//                       //
// ********************* //

export function mbCreateNotifForIncident(container) {
    let incident_notif = [{
        incident_1: {
            en: 'Incident validated by the manager: [TITLE]',
            fr: 'Incident validé par le gestionnaire : [TITLE]'
        },
        incident_2: {
            en: 'New incident: [TITLE]',
            fr: 'Nouvel incident : [TITLE]'
        },
        incident_3: {
            en: 'New incident: [TITLE]',
            fr: 'Nouvel incident : [TITLE]'
        },
        incident_4: {
            en: 'Note added on incident: [TITLE]',
            fr: 'Ajout d\'une note pour l\'incident : [TITLE]',
        },
        incident_5: {
            en: 'Quote added on incident: [TITLE]',
            fr: 'Ajout d\'un devis pour l\'incident : [TITLE]',
        },
        incident_7: {
            en: 'Intervention scheduled on incident: [TITLE]',
            fr: 'Intervention prévue pour l\'incident: [TITLE]',
        },
        incident_8: {
            en: 'Incident solved: [TITLE]',
            fr: 'Incident résolu : [TITLE]',
        },
        incident_9: {
            en: 'Manager feedback on private incident: [TITLE]',
            fr: 'Retour du manager sur l\'incident privé : [TITLE]',
        },
        incident_10: {
            en: 'Incident reopen: [TITLE]',
            fr: 'Incident ré-ouvert : [TITLE]',
        },
        incident_11: {
            en: 'Intervention cancelled on incident: [TITLE]',
            fr: 'Intervention annulée pour l\'incident: [TITLE]',
        }
    }, {
        incident_1: {
            en: 'Incident validated by the manager: [TITLE]',
            fr: 'Incident validé par le gestionnaire : [TITLE]'
        },
        incident_2: {
            en: 'New incident (occupant): [PRIORITY][TYPE][TITLE]',
            fr: 'Nouvel incident (occupant) : [PRIORITY][TYPE][TITLE]'
        },
        incident_3: {
            en: 'New incident (manager): [PRIORITY][TYPE][TITLE]',
            fr: 'Nouvel incident (gestionnaire) : [PRIORITY][TYPE][TITLE]'
        },
        incident_4: {
            en: 'Note added on incident: [TITLE]',
            fr: 'Ajout d\'une note pour l\'incident : [TITLE]',
        },
        incident_5: {
            en: 'Quote added on incident: [TITLE]',
            fr: 'Ajout d\'un devis pour l\'incident : [TITLE]',
        },
        incident_7: {
            en: 'Intervention scheduled on incident: [TITLE]',
            fr: 'Intervention prévue pour l\'incident: [TITLE]',
        },
        incident_8: {
            en: 'Incident solved: [TITLE]',
            fr: 'Incident résolu : [TITLE]',
        },
        incident_9: {
            en: 'Incident marked as private by a manager: [TITLE]',
            fr: 'Incident classé "privé" par un gestionnaire : [TITLE]',
        },
        incident_10: {
            en: 'Incident reopen: [TITLE]',
            fr: 'Incident ré-ouvert : [TITLE]',
        },
        incident_11: {
            en: 'Intervention cancelled on incident: [TITLE]',
            fr: 'Intervention annulée pour l\'incident: [TITLE]',
        }
    }]


    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for incident.')
        return null
    }
    if (!container.forManager && container.forManager !== false) {
        console.log('Impossible to create notif for incident. (container.forManager missing)')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let incident = Incidents.findOne(container.dataId)
    if (!incident || !incident.info) {
        console.log('Incident not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = incident_notif[container.forManager ? 1 : 0][container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }

    function fillContent(contents, incident) {

        function getPriority(priorityId, lang) {
            let priority = IncidentPriority.findOne({_id: priorityId})
            if (priority && priority['value-' + lang]) {
                return priority['value-'+lang] + ' / '
            } else {
                return ''
            }
        }
        function getType(typeId, lang) {
            let type = IncidentDetails.findOne({_id: typeId})
            if (type && type['value-' + lang]) {
                return type['value-'+lang] + ' / '
            } else {
                return ''
            }
        }
        contents.en = contents.en.replace(/\[TITLE\]/i, incident.info.title)
        contents.fr = contents.fr.replace(/\[TITLE\]/i, incident.info.title)
        contents.en = contents.en.replace(/\[PRIORITY\]/i, getPriority(incident.info.priority, 'en'))
        contents.fr = contents.fr.replace(/\[PRIORITY\]/i, getPriority(incident.info.priority, 'fr'))
        contents.en = contents.en.replace(/\[TYPE\]/i, getType(incident.info.type, 'en'))
        contents.fr = contents.fr.replace(/\[TYPE\]/i, getType(incident.info.type, 'fr'))
        return contents
    }
    contents = fillContent(contents, incident)
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
        type: container.type,
        condoId: condo._id,
        postId: incident._id
    }
    return {
        title,
        contents,
        data
    }
}
