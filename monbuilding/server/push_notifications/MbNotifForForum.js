
// ********************* //
//                       //
//          ADS          //
//                       //
// ********************* //

export function mbCreateNotifForForum(container) {
    let forum_notif = {
        new_post: {
            en: 'New Forum [TYPE]: [TITLE]',
            fr: 'Nouveau [TYPE] : [TITLE]'
        },
        poll_expire: {
            en: 'Poll will [EXPIRE]: [TITLE]',
            fr: 'Le sondage [EXPIRE] : [TITLE]'
        },
        new_comment: {
            en: 'New comment: [TITLE]',
            fr: 'Nouveau commentaire : [TITLE]'
        },
    }

    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for ads, container is incorrect.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let forum = ForumPosts.findOne({_id: container.dataId})
    if (!forum) {
        console.log('Forum not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = forum_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    contents.en = contents.en.replace(/\[TYPE\]/i, forum.type === 'post' ? 'post' : 'poll')
    contents.fr = contents.fr.replace(/\[TYPE\]/i, forum.type === 'post' ? 'post' : 'sondage')
    contents.en = contents.en.replace(/\[TITLE\]/i, forum.subject)
    contents.fr = contents.fr.replace(/\[TITLE\]/i, forum.subject)
    if (forum.type === 'poll' && container.key === 'poll_expire') {
        contents.en = contents.en.replace(/\[EXPIRE\]/i, mbGetDateFormat('en', forum.endDate))
        contents.fr = contents.fr.replace(/\[EXPIRE\]/i, mbGetDateFormat('fr', forum.endDate))
    }
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }

    let data = {
        type: container.type,
        condoId: condo._id,
        postId: forum._id
    }
    return {
        title,
        contents,
        data
    }
}

function mbGetDateFormat(lang, endDate) {
    moment.locale(lang);

    let end = null
    if (endDate) {
        end = moment(endDate)
    }
    if (end) {
        return 'expire ' + end.fromNow()
    } else {
        return ''
    }
}
