
// ********************* //
//                       //
//      SET CONTACT      //
//                       //
// ********************* //

export function mbCreateNotifForSetContact(container) {
    let set_contact_notif = {
        'addDefault': {
            en: 'You have been selected as the manager on duty in this building (any request not allocated to a specific person will be directed to you)',
            fr : 'Vous avez été désigné(e) comme le contact par défaut de l\'immeuble (toute demande non allouée à une personne en particulier sera redirigée vers vous)'
        },
        'removeDefault': {
            en: 'You are no longer the manager on duty in this building',
            fr: 'Vous n\'êtes plus le contact par défaut de l\'immeuble'
        },
        'addOther': {
            en: 'You have been selected as the "[CATEGORY]" manager on duty in this building (any request in relation to the "[CATEGORY]" will be directed to you)',
            fr: 'Vous avez été désigné(e) comme le contact par défaut pour toute demande liée aux "[CATEGORY]"'
        },
        'removeOther': {
            en: 'You are no longer the "[CATEGORY]" manager on duty in this building',
            fr: 'Vous n\'êtes plus le contact par défaut "[CATEGORY]" de l\'immeuble'
        },
        'addValidEntry': {
            en: 'You have been selected as the occupants access manager on duty in this building (any request in relation to the occupants access will be directed to you)',
            fr: 'Vous avez été désigné(e) comme le contact par défaut pour toutes demandes entrées / sorties des utilisateurs'
        },
        'removeValidEntry': {
            en: 'You are no longer the occupants access manager on duty in this building',
            fr: 'Vous n\'êtes plus le contact par défaut "entrées/sorties des utilisateurs" de l\'immeuble'
        },
        'addReservation': {
            en: 'You have been selected as the bookings validation manager on duty in this building (any request in relation to the bookings validation will be directed to you)',
            fr: 'Vous avez été désigné(e) comme le contact par défaut pour toute validation des réservations'
        },
        'removeReservation': {
            en: 'You are no longer the bookings validation manager on duty in this building',
            fr: 'Vous n\'êtes plus le contact par défaut pour toute validation des réservations'
        },
    }


    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for ads, container is incorrect.')
        return null
    }
    const declaration = DeclarationDetails.findOne({ _id: container.dataId });
    if (!declaration && container.dataId !== 'defaultContact') {
        console.log('DeclarationDetails not found.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = set_contact_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    if (declaration) {
        contents.en = contents.en.replace(/\[CATEGORY\]/ig, declaration['value-en'])
        contents.fr = contents.fr.replace(/\[CATEGORY\]/ig, declaration['value-fr'])
    }
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
        ...container.data,
        type: container.type,
        condoId: condo._id,
        postId: declaration ? declaration._id : 'defaultContact' 
    }
    return {
        title,
        contents,
        data
    }
}
