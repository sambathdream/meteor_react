
// ********************* //
//                       //
//       ACTUALITY       //
//                       //
// ********************* //

export function mbCreateNotifForActuality(container) {
    let actuality_notif = {
        new_info: {
            en: 'New information: [TITLE]',
            fr: 'Nouvelle information : [TITLE]'
        },
        new_event: {
            en: 'New event: [TITLE], [DATE]',
            fr: 'Nouvel évènement : [TITLE], [DATE]'
        },
        reminder_event: {
            en: 'Reminder event: [TITLE], [DATE]',
            fr: 'Rappel évènement : [TITLE], [DATE]'
        },
        change_event: {
            en: 'Date change on event: [TITLE], [DATE]',
            fr: 'Changement date évènement : [TITLE], [DATE]'
        },
        cancel_event: {
            en: 'Canceled event: [TITLE], [DATE]',
            fr: 'Evènement annulé : [TITLE], [DATE]'
        },
        new_comment: {
          en: 'New comment: [TITLE]',
          fr: 'Nouveau commentaire : [TITLE]'
        },
    }


    if (!(container && container.key && container.dataId && container.condoId)) {
        console.log('Impossible to create notif for actuality.')
        return null
    }
    let condo = Condos.findOne(container.condoId)
    if (!condo) {
        console.log('Condo not found.')
        return null
    }
    let actuality = ActuPosts.findOne(container.dataId)
    if (!actuality) {
        console.log('Actuality not found.')
        return null
    }
    let title = {}
    title.en = condo.getName()
    title.fr = condo.getName()
    let contents = actuality_notif[container.key]
    if (!contents || !contents.en || !contents.fr) {
        console.log('Contents not set.')
        return null
    }
    contents.en = contents.en.replace(/\[TITLE\]/i, actuality.title)
    contents.fr = contents.fr.replace(/\[TITLE\]/i, actuality.title)
    if (actuality.startDate) {
        contents.en = contents.en.replace(/\[DATE\]/i, mbGetDateFormat('en', actuality.startDate, actuality.startHour, actuality.endDate, actuality.endHour))
        contents.fr = contents.fr.replace(/\[DATE\]/i, mbGetDateFormat('fr', actuality.startDate, actuality.startHour, actuality.endDate, actuality.endHour))
    }
    if (contents.en.length > 2048) {
        contents.en = contents.en.substring(0, 2048) + '...'
    }
    if (contents.fr.length > 2048) {
        contents.fr = contents.fr.substring(0, 2048) + '...'
    }
    let data = {
        type: container.type,
        condoId: condo._id,
        postId: actuality._id,
        deleted: container.key === 'cancel_event',
        event: container.data && container.data.event,
        post: container.data && container.data.post
    }
    return {
        title,
        contents,
        data
    }
}

function mbGetDateFormat(lang, startDate, startHour, endDate, endHour) {
    moment.locale(lang);

    let start = startHour !== '-1' ? moment(startDate + ', ' + startHour, 'DD/MM/YYYY, HH:mm') : moment(startDate, 'DD/MM/YYYY')
    let end = endDate !== '' ? endHour !== '-1' ? moment(endDate + ', ' + endHour, 'DD/MM/YYYY, HH:mm') : moment(endDate, 'DD/MM/YYYY') : null

    let sameDay = start.isSame(end, 'day')
    if (!sameDay) {
        if (lang === 'en') {
            return 'on ' + start.format('dddd DD MMM') +
                (startHour && startHour !== '-1' ? ' at ' + startHour : '') +
                (endDate && endDate !== '' ? ' to ' + end.format('dddd DD MMM') : '') +
                (endHour && endHour !== '-1' ? ' at ' + endHour : '')
        } else {
            return (end !== null ? 'du ' : 'le ') + start.format('dddd DD MMM') +
                (startHour && startHour !== '-1' ? ' à ' + startHour : '') +
                (end !== null ? ' au ' + end.format('dddd DD MMM') : '') +
                (endHour && endHour !== '-1' ? ' à ' + endHour : '')
        }
    } else {
        if (lang === 'en') {
            return 'on ' + start.format('dddd DD MMM') +
                (startHour && startHour !== '-1' ? ' at ' + startHour : '') +
                (endHour && endHour !== '-1' ? ' to ' + endHour : '')
        } else {
            return 'le ' + start.format('dddd DD MMM') +
                (startHour && startHour !== '-1' ? (endHour && endHour !== '-1' ? ' de ' : ' à ') + startHour : '') +
                (endHour && endHour !== '-1' ? ' à ' + endHour : '')
        }
    }
}
