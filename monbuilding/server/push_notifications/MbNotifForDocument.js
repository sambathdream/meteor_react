
// ********************* //
//                       //
//       DOCUMENT        //
//                       //
// ********************* //

export function mbCreateNotifForDocument(container) {
  let document_notif = {
    new_manual: {
      en: 'Manual: new document',
      fr: 'Manuel : nouveau document'
    },
    new_map: {
      en: 'Plan: new plan',
      fr: 'Plan : nouveau plan'
    },
  }


  if (!(container && container.key && container.condoId)) {
    console.log('Impossible to create notif for document.')
    return null
  }
  let condo = Condos.findOne(container.condoId)
  if (!condo) {
    console.log('Condo not found.')
    return null
  }
  let title = {}
  title.en = condo.getName()
  title.fr = condo.getName()
  let contents = document_notif[container.key]
  if (!contents || !contents.en || !contents.fr) {
    console.log('Contents not set.')
    return null
  }

  let data = {
    type: container.type,
    postId: container.key === 'new_manual' ? 'manual' : 'map',
    condoId: condo._id
  }
  return {
    title,
    contents,
    data
  }
}
