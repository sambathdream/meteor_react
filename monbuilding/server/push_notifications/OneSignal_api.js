import moment from "moment";

import { mbCreateNotifForActuality } from './MbNotifForActuality'
import { mbCreateNotifForIncident } from './MbNotifForIncident'
import { mbCreateNotifForDocument } from './MbNotifForDocument'
import { mbCreateNotifForMessenger } from './MbNotifForMessenger'
import { mbCreateNotifForReservation } from './MbNotifForReservation'
import { mbCreateNotifForAds } from './MbNotifForAds'
import { mbCreateNotifForForum } from './MbNotifForForum'
import { mbCreateNotifForSetContact } from './MbNotifForSetContact'
import { mbCreateNotifForLaundry } from './MbNotifForLaundry'
// import { mbCreateNotifForOther } from './MbNotifForOther'
import fetch from 'node-fetch';

OneSignal = {};

Meteor.startup(function () {

  const moduleRightMap = {
    actuality: ['actuality.see'],
    incident: ['incident.see'],
    forum: ['seePoll', 'seeSubject'],
    ads: ['ads.see'],
    reservation: ['reservation.seeAll', 'seeAllEdl'],
    messenger: []
  }

  const validateUserIds = (userIds) => {
    if (!userIds) {
      return []
    } else if (!Array.isArray(userIds)) {
      return [userIds.toString()]
    } else {
      return userIds
    }
  }

  const checkUserPermissions = (userIds, condoId, module) => {
    const rights = moduleRightMap[module]
    if (!rights || rights.length === 0) {
      return userIds
    }
    userIds = validateUserIds(userIds)
    const query = {
      userId: {$in: userIds},
      condoId: condoId,
      $and: rights.map(r => ({
        [`module.${r}`]: false
      }))
    };
    const negatedUsersResult = UsersRights.find(query, {
      fields: {_id: 1, userId: 1}
    }).fetch()
    const negatedUsers = negatedUsersResult.map(nu => nu.userId)
    if (negatedUsers.length === 0) {
      return userIds
    } else {
      return userIds.filter(id => !negatedUsers.includes(id))
    }
  }

  sendNotification = function (data, clientName = 'default') {
    // console.log(data, clientName);
    // console.log('It PN time !')
    const { appId, apiKey } = Meteor.settings.oneSignal[clientName];
    if (!appId || !apiKey) {
      throw new Meteor.Error(406, 'Error sending PN invalid client ', clientName);
    }
    if (!data.contents || !data.contents.en) {
      throw new Meteor.Error(406, 'Contents in english is required');
    }

    const body = {
      app_id: appId,
      headings: data.title,
      contents: data.contents,
      ios_badgeType: data.ios_badgeType,
      send_after: data.sendAfter,
      data: data.data
    };
    // console.log('SENDING PN', body);

    if (data.playerIds) {
      body.include_player_ids = data.playerIds;
    } else if (data.filters) {
      body.filters = data.filters;
    } else if (data.segments) {
      body.included_segments = data.segments;
    }

    const result = fetch('https://onesignal.com/api/v1/notifications', {
      method: 'POST',
      headers: {
        'Authorization': `Basic ${apiKey}`,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })
    .then(response => response.json()) // parses response to JSON

    result.then(console.log.bind(0, 'PUSH NOTIFICATION'))
    return result
  }

  sendNotificationToUsers = async function (userIds, condoId, module, data) {
    console.log('VALIDATE', 1)
    userIds = validateUserIds(userIds)
    console.log('VALIDATE', 2)
    let query = { '_id': {$in :userIds }}
    if (module && condoId) {
      let notification = `notification.${condoId}.${module}`;
      query = {
        $and: [
          query,
          { [notification]: { $ne: false } }
        ]
      }
    }

    let players = PushNotificationsId.find(
      query, {
      fields: {playerIds: 1}
    }).fetch();

    if (players.length === 0) {
      throw 'Error: PushNotificationsId not found.'
    }
    console.log('VALIDATE', 3)
    const devices = players.reduce((list, user) => [...list, ...user.playerIds], []);
    const groups = Object.values(_.groupBy(devices, 'client'));
    const responses = [];
    groups.forEach(g => {
      let playerIds
      if (typeof g[0] === 'string') {
        playerIds = g
        responses.push(sendNotification({...data, playerIds}))

      } else if (g.length > 0) {
        playerIds = g.map(did => did.playerId);
        responses.push(sendNotification({...data, playerIds}, g[0].client))
      }
    });
    const result = await Promise.all(responses);

    return result;
  }


  sendNotificationToClient = async function (data, clientName) {
    let response // declare response here as it is used outside try/catch
    try {
      response = await sendNotification(data, clientName)
    } catch (e) {
      // process e properly
      throw new Meteor.Error(406, 'Error sending PN: ' + e.message)
    }
    if (response.data.errors) {
      throw new Meteor.Error(response.httpResponse.statusCode, response.data)
    } else {
      return response.data.id
    }
  }

  sendNotificationToAllClients = async function (data) {
    Object.keys(Meteor.settings.oneSignal).forEach(async (client) => {
      if (client === 'default') { return }
      let response; // declare response here as it is used outside try/catch
      try {
        response = await sendNotification(data, client)
      } catch (e) {
        // process e properly
        throw new Meteor.Error(406, 'Error sending PN: ' + e.message)
      }
      if (response.data && response.data.errors) {
        throw new Meteor.Error(response.httpResponse.statusCode, response.data)
      }
    });
  }

  OneSignal.MbNotification = {
    configure(container) {
      if (container.type) {
        switch (container.type) {
          case 'actuality':
            return mbCreateNotifForActuality(container)
          case 'incident':
            return mbCreateNotifForIncident(container)
          case 'document':
            return mbCreateNotifForDocument(container)
          case 'messenger':
            return mbCreateNotifForMessenger(container)
          case 'reservation':
            return mbCreateNotifForReservation(container)
          case 'ads':
            return mbCreateNotifForAds(container)
          case 'forum':
            return mbCreateNotifForForum(container)
          case 'setContact':
            return mbCreateNotifForSetContact(container)
          case 'laundry':
            return mbCreateNotifForLaundry(container)
          // case 'other':
          //   return mbCreateNotifForOther(container)
          default:
            return null;
        }
      }
    },
    sendToUserIds(userIds = [], container = {}, sendAfter = null, cb = null, sendToAll = false) {
      console.log('SEND PN TO ALL USERS', userIds);
      let data = null
      if (container)
        data = this.configure(container);
      if (!data) {
        if (cb) cb(null, 'Error: data not set, notification is not send.')
        return false
      }
      if (!sendToAll) {
        userIds = userIds.filter((uId) => uId !== Meteor.userId())
      }
      if (!userIds || Array.isArray(userIds) === false || userIds.length === 0) {
        if (cb) cb(null, 'Error: userId not found or incorrect.')
        return false
      }

      console.log('Filter 1', userIds)
      userIds = checkUserPermissions(userIds, container.condoId, container.type);

      console.log('Filter 2', userIds)

      sendNotificationToUsers(userIds, container.condoId, container.type, {
        'title': data.title,
        'contents': data.contents,
        'data': data.data,
        sendAfter,
      }).then(response => {
          if (response) {
            if (cb) cb(response, 'Succes: send notif to users ' + userIds)
          }
          return response
        })
        .catch(error => {
          if (cb) cb(null, 'Error: ' + error.message)
          return false
        });
    },

    sendToOneUser(userId = null, container = {}, sendAfter = null, cb = null) {
      // console.log('SEND PN TO ONE USER', userId);
      let data = null
      if (container)
        data = this.configure(container);

      if (!data) {
        if (cb) cb(null, 'Error: data not set, notification is not send.')
        return false
      }

      if (!userId || typeof userId !== 'string' || !container.condoId) {
        if (cb) cb(null, 'Error: userId not found or incorrect.')
        return false
      }

      userId = checkUserPermissions([userId], container.condoId, container.type)[0];
      if (!userId) {
        return false;
      }

      sendNotificationToUsers([userId], container.condoId, container.type, {
        'title': data.title,
        'contents': data.contents,
        'data': data.data,
        sendAfter,
      }).then(response => {
          if (response) {
            if (cb) cb(response, 'Succes: send notif to user ' + userId)
          }
          return response
        })
        .catch(error => {
          if (cb) cb(null, 'Error: ' + error.message)
          return false
        })
    },
    // /!\ WARNING : IT'S NOT AFFECT BY USER'S NOTIFICATION SETTINGS
    /* commented as not used */
    // sendToSegment(segments = [], container = {}, sendAfter = null, cb = null) {
    //   let data = null
    //   if (container)
    //     data = this.configure(container);

    //   if (!data) {
    //     if (cb) cb(null, 'Error: data not set, notification is not send.')
    //     return false
    //   }
    //   if (!segments || segments.length === 0) {
    //     if (cb) cb(null, 'Error: segments not found or incorrect.')
    //     return false
    //   }

    //   sendNotification({
    //     'title': data.title,
    //     'contents': data.contents,
    //     'data': data.data,
    //     segments,
    //     sendAfter,
    //   })
    //     .then(response => {
    //       if (response) {
    //         if (cb) cb(response, 'Succes: send notif to segments ' + segments)
    //       }
    //       return response
    //     })
    //     .catch(error => {
    //       if (cb) cb(null, 'Error: ' + error.message)
    //       return false
    //     })
    // },
    // /!\ WARNING : IT'S NOT AFFECT BY USER'S NOTIFICATION SETTINGS
    sendToFilter(filters = [], container = {}, sendAfter = null) {
      let data = null
      if (container) {
        data = this.configure(container);
      }
      if (!data) {
        console.log(new Error('Error: data not set, notification is not send.'));
        return false
      }
      if (!filters || filters.length === 0) {
        console.log(new Error('Error: filter not found or incorrect.'));
        return false
      }

      sendNotificationToAllClients({
        'title': data.title,
        'contents': data.contents,
        'data': data.data,
        filters,
        sendAfter,
      })
      .catch(error => {
        console.log(new Error('Error: ' + error.message))
        return false
      })
    },
  }
});

OneSignal.cancelNotification  = function (pnIds, cb) {
  if (!Array.isArray(pnIds)) {
    pnIds = [pnIds];
  }
  Object.keys(Meteor.settings.oneSignal).forEach(async (clientName) => {
    if (clientName === 'default') { return; }
    const { appId, apiKey } = Meteor.settings.oneSignal[clientName];
    pnIds.forEach(async pnId => {
      pnId = pnId.id ? pnId.id : pnId
      try {
        const response = fetch(`https://onesignal.com/api/v1/notifications/${pnId}?app_id=${appId}`, {
          method: 'DELETE',
          headers: {
            'Authorization': `Basic ${apiKey}`
          }
        })
        response.then(/* console.log.bind(0, 'CANCEL PUSH NOTIFICATION') */)
        await response
      } catch (e) { }
    });
  });
}
