import { Meteor } from 'meteor/meteor'
import { getCentralStatus } from './laundryApi'
import { LaundryRegister } from '/common/collections/Laundry'
import { BroadcasterCursor, pipeToSubscription, RxCollection } from '/server/models_new/imports/collection-observer'
import { map } from 'rxjs/operators'
import moment from 'moment'
import { LaundrySettings } from '../../../common/collections/Laundry';

const MACHINE_STATUS = {
  1: {
    text: 'laundryRoom_free', // FREE
    tag: 'tag-blue',
    hasButton: false
  },
  2: {
    text: 'laundryRoom_busy', // BUSY
    tag: 'tag-red',
    hasButton: true
  },
  3: {
    text: 'laundryRoom_out_of_service',  // OUT OF SERVICE
    tag: 'tag-grey',
    hasButton: false
  },
  4: {
    text: 'laundryRoom_busy', // TIME REMAINING
    tag: 'tag-red',
    hasButton: true
  },
  5: {
    text: 'laundryRoom_out_of_service', // OUTPUT UNAVAILABLE
    tag: 'tag-grey',
    hasButton: false
  },
  6: {
    text: 'laundryRoom_out_of_service', // TIME INVALID
    tag: 'tag-grey',
    hasButton: false
  },
  7: {
    text: 'laundryRoom_out_of_service', // MANUALLY ON
    tag: 'tag-grey',
    hasButton: false
  },
  8: {
    text: 'laundryRoom_out_of_service', // MANUALLY OFF
    tag: 'tag-grey',
    hasButton: false
  },
  9: {
    text: 'laundryRoom_out_of_service', // AUTOMATIC ON
    tag: 'tag-grey',
    hasButton: false
  },
  10: {
    text: 'laundryRoom_out_of_service', // AUTOMATIC OFF
    tag: 'tag-grey',
    hasButton: false
  },
  11: {
    text: 'laundryRoom_busy', // TIME REMAINING ESTIMATED
    tag: 'tag-red',
    hasButton: true
  }
}



Meteor.startup(function () {
  let laundryRoomBroadcaster = new BroadcasterCursor('laundryRoom')
  let laundryRoomInfoBroadcaster = new BroadcasterCursor('laundryRoomInfo')

  const LaundryRegister$ = new RxCollection(LaundryRegister)
  const timeMin = 5 * 60 * 1000 // micro sec update
  const timeMinActive = 10 * 1000 // micro sec update
  let subscriptions = {}
  let timeoutHandler = {}

  async function getUpdateFromWilane (condoId, central) { // GET UPDATE FROM WI-LINE
    const status = getCentralStatus(condoId, central)
    if (!status) {
      return // something went wrong... PANIC!
    }

    let nextTimeout = LaundryRegister.find().count() > 0 ? timeMinActive : timeMin
    laundryRoomInfoBroadcaster.set(central, {name: status.name})

    let userArray = []
    const handler = status.machine_list.map(async (el) => {
      if (el.machine_number === '0') { // FILTER NOT USEFULL DATA
        return
      }
      /* PARSE MACHINE DATA */
      const id = central + el.machine_number + el.name
      let price = el.price
      if (el.reduced_start !== '00:00:00' && el.reduced_stop !== '00:00:00') {
        let rStart = moment(el.reduced_start, 'HH:mm:ss')
        let rEnd = moment(el.reduced_end, 'HH:mm:ss')
        price = (moment().isBetween(rStart, rEnd)) ? el.price_reduced : el.price
      }
      const data = {
        _id: id,
        central: central,
        machine_number: el.machine_number,
        status: MACHINE_STATUS[el.status],
        type_name: el.type_name,
        remaining_time: (el.remaining_time || 0) * 1000,
        price: price / 100 // They use ct
      }
      laundryRoomBroadcaster.set(id, data)

      /* SEND PN */
      if (el.status === 1) { // only send PN for ready machines
        const registrations = await LaundryRegister$.find({
          $or: [
            {machine: data._id},
            {machine: { $exists: false}, type: data.type_name}
          ]}, {users: 1}).fetch()
        if (registrations.length > 0) {
          LaundryRegister.remove({type: data.type_name})
          for (const {users} of registrations) {
            userArray = [...userArray, ...users]
          }
        }
      }

      /* set next timeout */
      if (data.remaining_time !== 0) {
        nextTimeout = Math.min(data.remaining_time + 100)
      }
    })
    await Promise.all(handler)
    if (userArray.length > 0) {
      const users = new Set(userArray)
      OneSignal.MbNotification.sendToUserIds(Array.from(users), {
        type: 'laundry',
        key: 'machine-freed',
        condoId: condoId,
        data: { }
      }, null, null, true)
    }
    if (timeoutHandler[central]) {
      Meteor.clearTimeout(timeoutHandler[central])
      timeoutHandler[central] = null
    }

    timeoutHandler[central] = Meteor.setTimeout(() => getUpdateFromWilane(condoId, central), nextTimeout)
  }


  // let updater = 0
  Meteor.publish('getLaundry', function (condoId, central) {
    subscriptions[central] = subscriptions[central] ?  subscriptions[central] + 1 : 1
    getUpdateFromWilane(condoId, central)

    const laundryCursor = LaundryRegister$.find(
      {'users': Meteor.userId()},
      {fields: {'users$': 1, machine: 1, type: 1}
    })

    pipeToSubscription(this, laundryRoomBroadcaster, laundryRoomInfoBroadcaster, laundryCursor)

    this.onStop(() => { subscriptions[central] -= 1 })
  })

  Meteor.publish('getLaundrySetting', function (condoId) {
    if (condoId) {
      const config = IntegrationConfig.findOne({ condoId: condoId, integrationId: wiLineApiId })
      const centrals = config.reduce((array, c) => [...array, ...c.central_code.split(';').map(s => s.trim())], [])
      return LaundrySettings.find({
        _id: {$in: centrals}
      })
    } else {
      return LaundrySettings.find()
    }
  })
})
