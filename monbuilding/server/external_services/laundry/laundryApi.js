/* globals IntegrationConfig */
/* globals Meteor */
import { HTTP } from 'meteor/http'
import { wiLineApiId } from '/common/collections/API'
import { LaundryRegister, LaundrySettings } from '/common/collections/Laundry'

const host = 'https://www.api.wi-line.fr'
let token_list = {};
// let token = null

export function getWiLineInfo (condoId) {
  return IntegrationConfig.findOne({ condoId: condoId, integrationId: wiLineApiId })
}

export function getAuthToken (condoId) {
  const wiLineInfo = getWiLineInfo(condoId)
  const t = (new Date()).getTime()
  // console.log('token_list', token_list)
  if (wiLineInfo) {
    if (token_list[wiLineInfo.api_pass] && token_list[wiLineInfo.api_pass].exp > t)
      return token_list[wiLineInfo.api_pass]
    let r
    r = HTTP.call('POST', host + '/auth/',
      { params: { user: wiLineInfo.user, api_key: wiLineInfo.api_pass } })
    token_list[wiLineInfo.api_pass] = {
      token: r.data.token,
      exp: r.data.exp
    }
    return token_list[wiLineInfo.api_pass]
  }
  return null
}

export function getCentralStatus (condoId, central) {
  const url = host + '/centrale/' + central + '/status/'
  return queryWiLine(url, condoId)
}

export function getMachineStatus (condoId, central, machineId) {
  const url = host + '/centrale/' + central + '/machine/' + machineId
  return queryWiLine(url, condoId)
}

export function laundryRegister (condoId, central, machineId, type) {
  let u = Meteor.user()
  LaundryRegister.update({machine: machineId, type: type}, {$addToSet: {users: u._id}}, {upsert: true})
}

export function laundryRegisterAll (condoId, central, type) {
  let u = Meteor.user()
  LaundryRegister.update({machine: {$exists: false}, type: type}, {$addToSet: {users: u._id}}, {upsert: true})
}

export function laundrySettings (data) {
  console.log('data', data);
  let ret = LaundrySettings.update({_id: data._id}, data, {upsert: true})
  console.log('ret', ret);
  console.log(LaundrySettings.find({}).fetch());
}

function queryWiLine (url, condoId) {
  const token = getAuthToken(condoId)
  if (!token) {
    return null
  }
  try {
    let r = HTTP.call('GET', url, {
      headers: {
        'Authorization': token.token
      }
    })
    console.log('r', r);
    return r.data
  } catch (e) {
    return null
  }
}
