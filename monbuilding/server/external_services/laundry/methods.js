import { Meteor } from 'meteor/meteor'
import { check } from 'meteor/check'

import {
  getWiLineInfo,
  getAuthToken,
  getCentralStatus,
  getMachineStatus,
  laundryRegister,
  laundryRegisterAll,
  laundrySettings
} from './laundryApi'
import { LaundrySettings } from '../../../common/collections/Laundry';

Meteor.startup(function () {
  Meteor.methods({
    getAuthWiLine: async function (condoId) {
      if (!this.userId) {
        throw new Meteor.Error(401, 'Not logged in')
      }
      check(condoId, String)
      return (getAuthToken(condoId))
    },
    getCentralStatus: async function (condoId, central) {
      if (!this.userId) {
        throw new Meteor.Error(401, 'Not logged in')
      }
      check(condoId, String)
      var ret = getCentralStatus(condoId, central)
      return ret
    },
    getMachineStatus: async function (condoId, central, machineId) {
      if (!this.userId) {
        throw new Meteor.Error(401, 'Not logged in')
      }
      check(condoId, String)
      check(machineId, String)
      check(central, String)
      var ret = getMachineStatus(condoId, central, machineId)
      return ret
    },
    getListOfCentral: function (condoId) {
      check(condoId, String)
      if (condoId === 'all') {
        return []
      }
      let list = getWiLineInfo(condoId)
      return list && list.central_code.split(';')
    },
    laundryRegisterUser: function (condoId, central, machineId, type) {
      check(condoId, String)
      check(central, String)
      check(machineId, String)
      check(type, String)
      laundryRegister(condoId, central, machineId, type)
    },
    laundryRegisterUserAll: function (condoId, central, type) {
      check(condoId, String)
      check(central, String)
      check(type, String)
      laundryRegisterAll(condoId, central, type)

    },
    setLaundrySetting: function (serial, desc, location, opening, condoId) {
      // check(id, String)
      check(desc, String)
      check(location, String)
      check(opening, String)
      check(serial, String)
      if (!Meteor.userHasRight('laundryRoom', 'edit', condoId)) {
        return
      }

      const data= {
        // _id: id,
        _id: serial,
        desc,
        location,
        opening
      }
      laundrySettings(data)
      // LaundrySettings()
    }

  })
})