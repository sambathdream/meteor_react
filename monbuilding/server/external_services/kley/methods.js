import { HTTP } from 'meteor/http'
const fs = require('fs');
const http = require('http');
const API_URL = 'http://35.180.69.47/api'

function getFileExt (path) {
  return path.split('.').pop();
}
function getMimetype (path) {
  const ext = getFileExt(path);
  switch (ext) {
    case 'pdf': return 'application/pdf';
    case 'jpg': return 'image/jpg';
    case 'zip': return 'application/zip';
    default: return 'application/octet-stream';
  }
}

export async function load_kley_users() {
  console.log('====================================================');
  console.log('RETRIEVING KLEY USERS');
  const users_raw = getKleyData()
  const users = processData(users_raw)

  let found = 0
  let notfound = 0

  for (const user_id of Object.keys(users)) {
    const user = users[user_id];
    if (!userIsValid(user)) {
      console.log('BAD USER', user);
      continue;
    }
    const dbUser = getUser(user)
    let userId
    if (dbUser) {
      const _id = dbUser._id;
      userId = dbUser._id;
      dbUser.profile.civilite = dbUser.profile.civilite || user.profile.civilite
      dbUser.profile.tel = dbUser.profile.tel || user.profile.tel
      dbUser.profile.tel2 = dbUser.profile.tel2 || user.profile.tel2
      // emails disabled to allow Kley to manually & safely edit emails on MonBuilding
      // for (const email of user.emails) {
      //   const hasEmail = dbUser.emails.includes(e => e.address === email);
      //   if (!hasEmail) {
      //     dbUser.emails.push({
      //       address: email,
      //       verified: false
      //     })
      //   }
      // }
      if (!dbUser.source) {
        dbUser.source = {
          id: user._id,
          name: 'Kley',
          files: []
        }
      }
      for (const file of dbUser.source.files) {
        delete user.files[file]
      }
      dbUser.source.files = [...dbUser.source.files, ...Object.keys(user.files)]
      Meteor.users.update({_id}, dbUser);
      found += 1;
    } else {
      if (user.emails.size === 0) {
        console.log('BAD USER', user);
        continue;
      }
      console.log('ADD USER:', JSON.stringify(user))
      Meteor.call('createNewOccupantFromManagerSide', {
        firstname: user.profile.firstname,
        lastname: user.profile.lastname,
        email: Array.from(user.emails)[0].trim(),
        phone: user.profile.tel || null,
        phoneCode: '',
        landline: user.profile.tel2 || null,
        phoneCodeLandline: '',
        condos: ['Q458rRH5bNyiuCEM9'], // TODO: @Kevin check please
        roles: ['XN6pR6rhsHcpdqms4'],
        building: [],
        status: ['user'],
        company: [],
        diploma: [],
        school: [],
        studies: [],
        door: [],
        floor: [],
        office: [],
        civilite: user.profile.civilite === 'Madame' ? 'mme' :(user.profile.civilite === 'Monsieur' ? 'm' : null),
        deferredRegistrationDate: null,
        source: {
          name: 'Kley',
          id: user._id,
          files: Object.keys(user.files)
        }
      }, [ null ], from = 'kley', (error, result) => { })
      notfound += 1;
      const thisUser = Accounts.findUserByEmail(Array.from(user.emails)[0].trim())
      if (thisUser) {
        userId = thisUser._id
      } else {
        console.log('UNKNOWN USER => ', Array.from(user.emails)[0].trim())
      }
    }
    await loadUserFiles(userId, user.files)
  }
  console.log('FOUND', found)
  console.log('NOT FOUND', notfound)
}


Meteor.startup(function () {
  Meteor.methods({
    load_kley_users
  })
})


function capitalize (string) {
  return string && string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

function getKleyData () {
  let response = HTTP.call('GET', `${API_URL}/users`);
  return response.data
}

function processData (users_raw) {
  const users = {};
  users_raw.forEach(data => {
    const id = data.userId;
    if (!users[id]) {
      users[id] = {
        _id: id,
        profile: {},
        files: {},
        emails: new Set()
      }
    }

    const user = users[id];
    user.profile.civilite = data.civilite || user.profile.civilite
    user.profile.lastname = capitalize(data.lastname) || user.profile.lastname
    user.profile.firstname = capitalize(data.name) || user.profile.firstname
    user.profile.tel = data.telephone || user.profile.tel
    user.profile.tel2 = data.mobile || user.profile.tel2
    user.profile.tel2 = data.mobile || user.profile.tel2
    data.email && user.emails.add(data.email.trim())
    data.email2 && user.emails.add(data.email2.trim())
    if (data.fileId) {
      user.files[data.fileId] = {
        _id: data.fileId,
        name: data.fileName,
        path: data.filePath
      }
    }
  });
  return users;
}

function userIsValid (user) {
  return user.profile.firstname && user.profile.lastname
}

async function loadUserFiles (userId, files) {
  console.log(userId, JSON.stringify(files))
  const fileData = Object.values(files);
  await Promise.all(fileData.map(async file => {
    const {size, path} = await getFile(file.path);
    if (size) {
      const docid = UserDocumentFiles.collection.insert({
        size: size,
        type: getMimetype(path),
        name: file.name,
        meta: {
          userId: userId,
          uploader: userId,
          condoId: 'Q458rRH5bNyiuCEM9'
        },
        ext: getFileExt(path),
        extension: getFileExt(path),
        extensionWithDot: `.${getFileExt(path)}`,
        mime: getMimetype(path),
        'mime-type': getMimetype(path),
        userId: userId,
        path: path,
        versions: {
          original: {
            path: path,
            size: size,
            type: getMimetype(path),
            extension: getMimetype(path)
          }
        },
        _downloadRoute: '/download',
        _collectionName: 'userDocumentFiles',
        isVideo: false,
        isAudio: false,
        isImage: getMimetype(path).indexOf('image') === 0,
        isText: false,
        isJSON: false,
        isPDF: getFileExt(path) === 'pdf',
        _storagePath: path,
        public: false
      });
      console.log('DOC ID', docid);
      let fileLink = '/download/userDocumentFiles/' + docid + '/orignal/' + docid

      UserDocuments.insert({
        condoId: 'Q458rRH5bNyiuCEM9',
        title: file.name,
        fileId: docid,
        userId: userId,
        fileLink: fileLink,
        ext: getFileExt(path),
        type: getMimetype(path),
        uploaderId: userId,
        date: Date.now()
      })
    }
  }))

}

function getFile (path) {
  const sourceUrl = `${API_URL}/file/${path}`;
  const targetPath = `assets/app/uploads/userDocumentFiles/${path}`;
  let content_length;
  console.log('Downloading file', path)
  return new Promise((resolve, reject) => {
    http.get(sourceUrl, res => {
      switch(res.statusCode) {
        case 200:
          console.log('File found');
          content_length = res.headers['content-length'];
          break;
        case 404:
          console.log("File Not Found:", path);
        default:
          resolve({size: 0, path: ''});
          return;
      }
      const write_file = fs.createWriteStream(targetPath);
      res.on('data', function(chunk) {
        write_file.write(chunk);
      });
      res.on('end', function() {
        resolve({
          size: content_length,
          path: targetPath
        });
        write_file.end();
      });
    });
  });
}

function getUser (user) {
  const emails = Array.from(user.emails).forEach(e => e.trim())
  return Meteor.users.findOne({
    $or: [
      {
        'profile.firstname': { $regex: user.profile.firstname || 'NO NAME', $options: 'i'},
        'profile.lastname': { $regex: user.profile.lastname || 'NO NAME', $options: 'i'}
      },
      {
        'emails.address': { $in: emails }
      },
      {
        'source.id': user._id,
        'source.name': 'Kley'
      }
    ]
  });
}
