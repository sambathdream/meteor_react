import {load_kley_users} from '../methods'
import { SyncedCron } from 'meteor/percolate:synced-cron';
const DDPCommon = Package['ddp-common'].DDPCommon;

const ADDING_KLEY_USERS_INTERVAL = 'every 12 hours';
// Meteor.startup(function () {
//   Meteor.methods({
//     'addKeyUsersCron': function () {
//       this.setUserId('')
//       Meteor.call('load_kley_users')
//     }
//   })
// })

export function setupKleyCron () {
  SyncedCron.add({
    persist: false,
    name: 'Adding Kley users',
    schedule(parser) {
      return parser.text(ADDING_KLEY_USERS_INTERVAL);
    },
    job () {
      console.log('CALLING START')

      const invocation = new DDPCommon.MethodInvocation({
        isSimulation: false,
        userId: 'EXagBySN2uRAjCBhk', // PROD ADMIN
        setUserId: () => {},
        unblock: () => {},
        connection: null,
        randomSeed: Math.random()
      });


      DDP._CurrentInvocation.withValue(invocation, load_kley_users)
      console.log('CALLING DONE')
    }
  });
}
