/* globals IntegrationConfig */

import _ from 'lodash'
import { Meteor } from 'meteor/meteor'
import { WebApp } from 'meteor/webapp'
import moment from 'moment'

import { UserTransactions, UsersPayzen } from '/common/collections/Payments'
import { payzenApiId } from '/common/collections/API'
import CryptoJS from 'crypto-js'
import uuid from 'uuid'
import soap from 'soap'

const allowedUrl = [
  'refused',
  'error',
  'success'
]

export function getSoapHeader (condoId) {
  const generatedUUID = uuid()
  const generatedTimestamp = moment.utc().format('', moment.ISO_8601)

  const integrationConfig = IntegrationConfig.findOne({ condoId, integrationId: payzenApiId })
  if (!integrationConfig) {
    throw new Meteor.Error(404, 'Error: integration missing for this building')
  }
  const certificate = integrationConfig.prodCert === null ? integrationConfig.testCert : integrationConfig.prodCert
  const mode = integrationConfig.prodCert === null ? 'TEST' : 'PRODUCTION'
  const shopId = integrationConfig.siteId

  const hmac2565 = CryptoJS.enc.Base64.stringify(CryptoJS.HmacSHA256(generatedUUID + generatedTimestamp, certificate))

  return '<soap:Header xmlns:soapHeader="http://v5.ws.vads.lyra.com/Header">' +
    '<soapHeader:shopId>' + shopId + '</soapHeader:shopId>' +
    '<soapHeader:requestId>' + generatedUUID + '</soapHeader:requestId>' +
    '<soapHeader:timestamp>' + generatedTimestamp + '</soapHeader:timestamp>' +
    '<soapHeader:mode>' + mode + '</soapHeader:mode>' +
    '<soapHeader:authToken>' + hmac2565 + '</soapHeader:authToken>' +
    '</soap:Header>'
}

/*
  creditCard = {
    code_city: String || ''
    country : String || ''
    street : String || ''
    cvc : String(3)
    expiration : String(MM/YYYY)
    name : String
    number : String
    sameBillingAddress : Boolean
    saveCard : Boolean
    type : String(visa|amex|mastercard|maestro)
  }
*/
export async function createAlias (userId, condoId, creditCard) {
  const user = Meteor.users.findOne({ _id: userId })
  const integrationConfig = IntegrationConfig.findOne({ condoId, integrationId: payzenApiId })

  console.log('integrationConfig', integrationConfig)

  const generatedTimestamp = moment.utc().format('', moment.ISO_8601)
  const newSoapHeader = getSoapHeader(condoId)

  console.log('newSoapHeader', newSoapHeader)

  var url = integrationConfig.wsdl_url
  var args = {
    commonRequest: {
      submissionDate: generatedTimestamp
    },
    cardRequest: {
      number: creditCard.number.replace(/ /gi, ''),
      scheme: creditCard.type.toUpperCase(),
      expiryMonth: creditCard.expiration.split('/')[0],
      expiryYear: creditCard.expiration.split('/')[1],
      cardSecurityCode: creditCard.cvc
    },
    customerRequest: {
      billingDetails: {
        firstname: user.profile.firstname,
        lastName: user.profile.lastname,
        phoneNumber: user.profile.tel || undefined,
        email: user.emails[0].address
      }
    }
  }

  console.log('args', args)
  let awaitResponse = await basicSoapRequest(url, newSoapHeader, args, 'createToken')

  console.log('awaitResponse', awaitResponse)
  if (awaitResponse && awaitResponse.createTokenResult && awaitResponse.createTokenResult.commonResponse && awaitResponse.createTokenResult.commonResponse.responseCode === 0) {
    UsersPayzen.insert({
      userId,
      condoId,
      displayableNumber: '**** ' + creditCard.number.slice(-4),
      cardType: creditCard.type,
      paymentToken: awaitResponse.createTokenResult.commonResponse.paymentToken
    })
    return { success: true, token: awaitResponse.createTokenResult.commonResponse.paymentToken }
  } else {
    return { success: false, detail: awaitResponse.createTokenResult.commonResponse.responseCodeDetail }
  }
}

export async function getAliases (userInfos) {
  return userInfos
}

export async function removeAlias (userId, condoId, paymentToken) {
  const userInfo = UsersPayzen.findOne({ userId, condoId, paymentToken })
  const integrationConfig = IntegrationConfig.findOne({ condoId, integrationId: payzenApiId })
  if (userInfo) {
    const generatedTimestamp = moment.utc().format('', moment.ISO_8601)
    const newSoapHeader = getSoapHeader(condoId)

    var url = integrationConfig.wsdl_url
    var args = {
      commonRequest: {
        submissionDate: generatedTimestamp
      },
      queryRequest: {
        paymentToken: paymentToken
      }
    }

    let awaitResponse = await basicSoapRequest(url, newSoapHeader, args, 'cancelToken')

    if (awaitResponse && awaitResponse.cancelTokenResult && awaitResponse.cancelTokenResult.commonResponse && awaitResponse.cancelTokenResult.commonResponse.responseCode === 0) {
      UsersPayzen.remove({ _id: userInfo._id })
      return awaitResponse.cancelTokenResult.commonResponse
    } else {
      return null
    }
  }
}

export function basicSoapRequest (url, newSoapHeader, args, requestName) {
  let waitResponse = new Promise(function (resolve, reject) {
    soap.createClient(url, function (err, client) {
      if (!err) {
        client.addSoapHeader(newSoapHeader)

        client[requestName](args, function (error, result) {
          if (!error) {
            resolve(result)
          } else {
            reject(error)
          }
        })
      }
    })
  })
  return waitResponse
}

// const postRoute = Picker.filter(function (req, res) {
//   return req.method === 'POST'
// })
const bodyParser = require('body-parser')
Picker.middleware(bodyParser.json())
Picker.middleware(bodyParser.urlencoded({ extended: false }))
Picker
  .route('/api/payzen/threeDsResult', async function (params, req, res, next) {
    let authLoginUrl = ''
    console.log('params', params)
    console.log('req.body', req.body)
    const { email, token } = params.query
    const { firstName, lastName } = req.body
  })

Meteor.startup(function () {
  WebApp.connectHandlers.use('/api/payzen/threeDsResult', function (req, res, next) {
    const url = req.url.split('/')
    let payzenResp = req.body
    const mdSplit = (payzenResp && payzenResp.MD) ? payzenResp.MD.split('+') : ['', '']
    const merchantData = { JSESSIONID: mdSplit[0], requestId: mdSplit[1] }
    const transactionId = url[1]

    res.writeHead(200)

    let returnFunc = Meteor.call('finalizeThreeDS', transactionId, payzenResp.PaRes || '', merchantData)
    res.end(returnFunc.createPaymentResult.commonResponse.responseCode === 0 ? '<script>window.close()</script>' : 'ERROR')
  })

  WebApp.connectHandlers.use('/api/payzen', function (req, res, next) {
    var body = ''
    const url = req.url.split('/')
    console.log('yo')

    if (url && url[1]) {
      const transId = url[2] || null
      if (transId && _.includes(allowedUrl, url[1])) {
        UserTransactions.update({ _id: transId }, {
          $set: {
            status: url[1]
          }
        })
      }
    }
    req.on('data', Meteor.bindEnvironment(function (data) {
      body += data
    }))

    req.on('end', Meteor.bindEnvironment(function () {
      let payzenResp = {}
      body = body.split('&').forEach(elem => {
        const temp1 = elem.split('=')
        payzenResp[temp1[0]] = temp1[1]
      })
      if (payzenResp && payzenResp.vads_order_id) {
        UserTransactions.update({ _id: payzenResp.vads_order_id }, {
          $set: {
            vads_return: payzenResp,
            status: payzenResp.vads_payment_error ? 'error' : 'success'
          }
        })
      }
      const url = req.url.split('/')
      if (url && url[1]) {
        console.log('body', payzenResp)
        console.log('url[1]', url)
        res.writeHead(200)
        res.end((!!url && !!url[1] && url[1].toUpperCase()) || 'ERROR')
      }
    }))
    // return next()
  })
})
