import { Meteor } from 'meteor/meteor'
import { check, Match } from 'meteor/check'
import { UserTransactions, UsersPayzen } from '/common/collections/Payments'
import { payzenApiId } from '/common/collections/API'
import {
  getSoapHeader,
  createAlias,
  getAliases,
  removeAlias,
  basicSoapRequest
} from '/server/external_services/PayZen/PayZenAPI'

import { validatePaidBasket } from '/server/models/marketPlace/methods'
import { validatePaidBook } from '/server/models/reservations/methods'

import moment from 'moment'
import uniqId from 'uniqid'
// import fetch from 'node-fetch'
import soap from 'soap'

Meteor.startup(function () {
  Meteor.methods({
    getPayZenAliases: async function (condoId) {
      if (this.userId) {
        let userInfos = UsersPayzen.find({ userId: this.userId, condoId })
        if (!userInfos.count()) {
          return null
        }
        return getAliases(userInfos.fetch() || null)
      } else {
        throw new Meteor.Error(401, 'Not logged in')
      }
    },
    /*
      creditCard = {
        cvc : String(3)
        expiration : String(MM/YYYY)
        name : String
        number : String
        saveCard : Boolean
        type : String(visa|amex|mastercard|maestro)
      }
    */
    createPayZenAliases: async function (condoId, creditCard) {
      if (this.userId) {
        check(condoId, String)
        check(creditCard, Match.ObjectIncluding({
          cvc: String,
          expiration: String,
          name: String,
          number: String,
          saveCard: Boolean,
          type: Match.OneOf('visa', 'amex', 'mastercard', 'maestro')
        }))
        console.log('creditCard', creditCard)
        const paymentToken = await createAlias(this.userId, condoId, creditCard)
        console.log('paymentToken', paymentToken)
        return paymentToken
      } else {
        throw new Meteor.Error(401, 'Not logged in')
      }
    },
    updatePayZenAliases: function (condoId) {
      if (this.userId) {
        const userInfos = UsersPayzen.findOne({ userId: this.userId })
        if (!userInfos) {
          return null
        } else {
          return 'no user info'
        }
      } else {
        throw new Meteor.Error(401, 'Not logged in')
      }
    },
    removePayZenAlias: async function (condoId, paymentToken) {
      if (this.userId) {
        let res = await removeAlias(this.userId, condoId, paymentToken)
        console.log('resREMOVE', res)
        return !!res
      } else {
        throw new Meteor.Error(401, 'Not logged in')
      }
    },
    finalizeThreeDS: async function (transactionId, PaRes, merchantData) {
      if (!this.connection) { // this means it's called from server side
        check(PaRes, String)
        check(merchantData, Match.ObjectIncluding({ JSESSIONID: String, requestId: String }))

        const thisTransaction = UserTransactions.findOne({ _id: transactionId })
        const integrationConfig = IntegrationConfig.findOne({ condoId: thisTransaction.condoId, integrationId: payzenApiId })
        const generatedTimestamp = moment.utc().format('', moment.ISO_8601)

        const newSoapHeader = getSoapHeader(thisTransaction.condoId)

        var url = integrationConfig.wsdl_url
        var args = {
          commonRequest: {
            submissionDate: generatedTimestamp
          },
          threeDSRequest: {
            mode: 'ENABLED_FINALIZE',
            pares: PaRes,
            requestId: merchantData.requestId
          }
        }

        let waitResponse = await basicSoapRequest(url, newSoapHeader, args, 'createPayment')

        UserTransactions.update({ _id: transactionId }, {
          $set: {
            status: waitResponse.createPaymentResult.commonResponse.transactionStatusLabel,
            threeDsResponse: waitResponse
          }
        })
        if (thisTransaction.basketId && waitResponse.createPaymentResult.commonResponse.transactionStatusLabel === 'AUTHORISED') {
          validatePaidBasket(thisTransaction.basketId, transactionId)
        }
        if (thisTransaction.reservationId && waitResponse.createPaymentResult.commonResponse.transactionStatusLabel === 'AUTHORISED') {
          validatePaidBook(thisTransaction.reservationId, transactionId)
        }
        return waitResponse
      } else {
        throw new Meteor.Error(301, 'Access Denied', 'This method is not allowed from client side')
      }
    },
    createPayment: async function (condoId, isAliasPayment = false, paymentToken = null, cardInfo = null, amount = 0, billingDetails = null, basketId = null, reservationId = null) {
      const orderId = uniqId()
      const generatedTimestamp = moment.utc().format('', moment.ISO_8601)
      const user = Meteor.user()

      if (!billingDetails) {
        const c = Condos.findOne({ _id: condoId })
        if (c) {
          billingDetails = {
            address: c.info.address,
            zipCode: c.info.code,
            city: c.info.city,
            country: ''
          }
        }
      }

      if (billingDetails.country.match(/france/gi)) {
        billingDetails.country = 'FR'
      } else {
        billingDetails.country = ''
      }

      const newSoapHeader = getSoapHeader(condoId)
      const integrationConfig = IntegrationConfig.findOne({ condoId, integrationId: payzenApiId })

      const url = integrationConfig.wsdl_url
      let args = {
        commonRequest: {
          submissionDate: generatedTimestamp
        },
        paymentRequest: {
          amount: amount.toString(),
          currency: '978'
        },
        orderRequest: {
          orderId
        },
        customerRequest: {
          billingDetails: {
            firstname: user.profile.firstname,
            lastName: user.profile.lastname,
            phoneNumber: user.profile.tel || undefined,
            email: user.emails[0].address,
            address: billingDetails.address,
            zipCode: billingDetails.zipCode,
            city: billingDetails.city,
            country: billingDetails.country
          }
        },
        threeDSRequest: {
          mode: 'ENABLED_CREATE'
        }
      }
      let cardType = ''
      if (isAliasPayment) {
        args.cardRequest = {
          paymentToken: paymentToken
        }
        const thisPayzenToken = UsersPayzen.findOne({ paymentToken })
        cardType = thisPayzenToken ? thisPayzenToken.cardType : ''
      } else {
        args.cardRequest = {
          number: cardInfo.number.replace(/ /gi, ''),
          scheme: cardInfo.type.toUpperCase(),
          expiryMonth: cardInfo.expiration.split('/')[0],
          expiryYear: cardInfo.expiration.split('/')[1],
          cardSecurityCode: cardInfo.cvc
        }
        cardType = cardInfo.type
      }

      let threeDSRequestId
      const userId = Meteor.userId()
      let waitResponse = await new Promise(Meteor.bindEnvironment(function (resolve, reject) {
        soap.createClient(url, Meteor.bindEnvironment(function (err, client) {
          if (!err) {
            // console.log('1')
            client.addSoapHeader(newSoapHeader)
            let jsessionId
            // console.log('2')
            // console.log('args', args)
            client.createPayment(args, Meteor.bindEnvironment(function (error, result, rawResponse, soapHeader, rawRequest) {
              // console.log('3')
              if (!error) {
                // console.log('4')
                const queryParams = {}
                if (result && result.createPaymentResult.commonResponse.responseCode === 0) {
                  let status = 'PENDING'
                  if (result.createPaymentResult.commonResponse.transactionStatusLabel) {
                    status = result.createPaymentResult.commonResponse.transactionStatusLabel
                  }
                  // console.log('status', status)
                  if (status !== 'PENDING' || result.createPaymentResult.threeDSResponse.authenticationResultData) {
                    const transactionId = UserTransactions.insert({
                      cardId: null,
                      amount,
                      type: 'TRANSACTION_payment',
                      condoId: condoId,
                      isCredit: false,
                      isRecurring: false,
                      frequency: null,
                      status: status,
                      date: Date.now(),
                      userId: userId,
                      basketId,
                      reservationId,
                      cardType,
                      createPayment: { result },
                      threeDsResponse: null
                    })
                    // console.log('status', status)
                    // console.log('result.createPaymentResult', result.createPaymentResult)
                    if (basketId && status === 'AUTHORISED') {
                      validatePaidBasket(basketId, transactionId)
                    }
                    if (reservationId && status === 'AUTHORISED') {
                      validatePaidBook(reservationId, transactionId)
                    }
                    resolve({ transactionId })
                  } else {
                    threeDSRequestId = result.createPaymentResult.threeDSResponse.authenticationRequestData.threeDSRequestId
                    const threeDSAcsUrl = result.createPaymentResult.threeDSResponse.authenticationRequestData.threeDSAcsUrl + ';jsessionid=' + jsessionId
                    queryParams.MD = jsessionId + '+' + threeDSRequestId
                    queryParams.PaReq = result.createPaymentResult.threeDSResponse.authenticationRequestData.threeDSEncodedPareq
                    queryParams.TermUrl = Meteor.absoluteUrl('api/payzen/threeDsResult/')

                    const transactionId = UserTransactions.insert({
                      cardId: null,
                      amount,
                      type: 'TRANSACTION_payment',
                      condoId: condoId,
                      isCredit: false,
                      isRecurring: false,
                      frequency: null,
                      status: 'PENDING',
                      date: Date.now(),
                      userId: userId,
                      basketId,
                      reservationId,
                      cardType,
                      createPayment: { threeDSAcsUrl, queryParams, threeDSRequestId, result },
                      threeDsResponse: null
                    })
                    queryParams.TermUrl += transactionId
                    // console.log('queryParams', queryParams)
                    resolve({ threeDSAcsUrl, queryParams, transactionId })
                  }
                } else {
                  // console.log('5')
                  // console.log('result.createPaymentResult.commonResponse.responseCodeDetail', result)
                  resolve(result.createPaymentResult.commonResponse.responseCodeDetail)
                }
              } else {
                // console.log('error', error)
                resolve(error)
              }
            }))
            client.on('response', (body, response, eid) => {
              jsessionId = response.headers['set-cookie'][0].split(';')[0].split('=')[1] // Maybe add some verification on it later, just maybe... :D
            })
          } else {
            console.log('err', err)
            resolve(err)
          }
        }))
      }))
      return waitResponse
    }
  })
})
