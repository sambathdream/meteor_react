import { payzenApiId } from '/common/collections/API'
import { UsersPayzen } from '/common/collections/Payments'
import { Meteor } from 'meteor/meteor'

Meteor.startup(function () {
  Meteor.publish('gePayzenAlias', function (condoId) {
    if (this.userId) {
      return UsersPayzen.find({ userId: this.userId, condoId })
    }
  })

  Meteor.publish('payzenInformations', function () {
    const user = Meteor.user()
    let condoIds = []
    if (user && user.identities && user.identities.residentId) {
      const resident = Residents.findOne({ userId: Meteor.userId() })
      if (resident) {
        condoIds = _.map(resident.condos, (condo) => condo.condoId)
      }
    }
    return IntegrationConfig.find({ integrationId: payzenApiId, condoId: { $in: condoIds } })
  })

  const integrationPayzenGrenelle = CondoIntegrationConfig.findOne({ integrationId: payzenApiId, condoId: 'niuSbhant98XgcD7Q' })
  let integrationPayzenGrenelleId = null

  if (!integrationPayzenGrenelle) {
    integrationPayzenGrenelleId = CondoIntegrationConfig.insert({
      activatedOn: Date.now(),
      active: true,
      condoId: "niuSbhant98XgcD7Q",
      integrationId: payzenApiId
    })
  } else {
    integrationPayzenGrenelleId = integrationPayzenGrenelle._id
  }
  const integrationPayzenGrenelleConfig = IntegrationConfig.findOne({ integrationId: payzenApiId, condoId: 'niuSbhant98XgcD7Q' })

  if (!integrationPayzenGrenelleConfig) {
    IntegrationConfig.insert({
      condoId: "niuSbhant98XgcD7Q",
      integrationId: payzenApiId,
      siteId: '14157244',
      testCert: '2846414230635531',
      prodCert: null,
      wsdl_url: 'https://secure.payzen.eu/vads-ws/v5?wsdl'
    })
  }
})
