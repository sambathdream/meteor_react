import { MarketServices, MarketReservations } from '/common/collections/marketPlace'
import { Runner } from '../autoscript-runner'

Runner.newPatch(5, 'Add defaultRoleId to old condoRole', function () {
  const existingReservations = MarketReservations.find({ use_time_slot: true, time_slot_scale: { $exists: false } }).fetch()
  let changes = 0

  existingReservations.forEach(mResa => {
    const thisService = MarketServices.findOne({ _id: mResa.serviceId })
    if (thisService) {
      MarketReservations.update({ _id: mResa._id }, {
        $set: {
          time_slot_scale: thisService.time_slot_scale,
          time_slot_duration: thisService.time_slot_duration,
        }
      })
      changes++
    }
  })
  return changes
})
