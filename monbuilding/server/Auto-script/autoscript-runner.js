const DbVersion = new Mongo.Collection('db-version')

class AutoScriptPatch {
  constructor (baseVersion, description, patcher) {
    this.description = description
    this.patcher = patcher
    this.baseVersion = baseVersion
  }

  run () {
    const start = Date.now()
    console.log('Running patch:', this.description)
    const time = Date.now() - start
    const affectedDocuments = this.patcher()
    DbVersion.insert({
      version: this.baseVersion + 1,
      description: this.description,
      time,
      affectedDocuments
    })

    console.log('Total time:', time)
    if (typeof affectedDocuments === 'number') {
      console.log('Affected documents:', affectedDocuments)
    }

  }
}


class AutoScriptRunner {
  constructor () {
    this.versions = []
  }

  newPatch (baseVersion, description, patcher) {
    if (typeof baseVersion !== 'number' || (baseVersion | 0) !== baseVersion) { throw 'newPatch integer expected as baseVersion'}
    if (!this.versions[baseVersion]) {
      this.versions[baseVersion] = []
    }
    const patch = new AutoScriptPatch(baseVersion, description, patcher)
    this.versions[baseVersion].push(patch)
    return patch
  }

  run () {
    const {version} = DbVersion.findOne({}, {sort: {version: -1}, fields: {version: 1}}) || {version: 0}
    let didChange = false
    let lastVersion = version
    for (let currentVersion = version; this.versions[currentVersion] !== undefined; currentVersion +=1 ) {
      console.log(`================ Running patches for version ${version.toString().padStart(2, ' ')} ================`)
      let once = false
      lastVersion = currentVersion
      didChange = true
      const patches = this.versions[currentVersion]
      for (const patch of patches) {
        if (!once) {
          once = true
        } else {
          console.log(`----------------------------------------------------------------`)
        }
        patch.run()
      }
      console.log(`================================================================`)
    }

    if (didChange) {
      console.log(`DB updated from version ${version} to version ${lastVersion + 1}`)
    } else {
      console.log('DB Version:', version)
    }


  }
}

const runner = new AutoScriptRunner()

export const Runner = runner

Meteor.startup(function () {
  runner.run()
})
