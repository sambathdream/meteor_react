import { Runner } from '../autoscript-runner'

Runner.newPatch(4, 'Add defaultRoleId to old condoRole', function () {
  const existingCondoRole = CondoRole.find({ defaultRoleId: { $exists: false } }).fetch()
  let changes = 0

  existingCondoRole.forEach(cR => {
    const defaultRole = DefaultRoles.findOne({ for: cR.for, name: cR.name })
    if (defaultRole) {
      CondoRole.update({ _id: cR._id }, {
        $set: {
          defaultRoleId: defaultRole._id
        }
      })
      changes++
    }
  })
  return changes
})
