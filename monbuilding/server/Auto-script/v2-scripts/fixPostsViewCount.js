import {Runner} from '../autoscript-runner'

Runner.newPatch(2, 'Update view count for info/events', function () {
  const posts = ActuPosts.find({}, {fields: {userViews: 1, viewsCount: 1, views: 1}}).fetch()
  if ( posts.length === 0 ) { return 0 }
  let changes = 0
  for (const post of posts) {
    if (!post.userViews) {
      changes += 1
      const userViews = Array.from(new Set(post.views))
      ActuPosts.update(
        {_id: post._id},
        {$set: {
          userViews: userViews,
          viewsCount: userViews.length
        }}
      )
    }
    else if (post.viewsCount !== post.userViews.length) {
      changes += 1
      ActuPosts.update(
        {_id: post._id},
        {$set: {
          viewsCount: post.userViews.length
        }}
      )
    }
  }
  return changes
})

Runner.newPatch(1, 'Update view count for ads', function () {
  const posts = ClassifiedsAds.find({}, {fields: {userViews: 1, viewsCount: 1}}).fetch()
  if ( posts.length === 0 ) { return 0 }
  let changes = 0
  for (const post of posts) {
    if (post.viewsCount !== post.userViews.length) {
      changes += 1
      ClassifiedsAds.update(
        {_id: post._id},
        {$set: { viewsCount: post.userViews.length }}
      )
    }
  }
  return changes
})

Runner.newPatch(1, 'Update view count for forum', function () {
  const posts = ForumPosts.find({}, {fields: {userViews: 1, viewsCount: 1}}).fetch()
  if ( posts.length === 0 ) { return 0 }
  let changes = 0
  for (const post of posts) {
    if (post.viewsCount !== post.userViews.length) {
      changes += 1
      ForumPosts.update(
        {_id: post._id},
        {$set: { viewsCount: post.userViews.length }}
      )
    }
  }
  return changes
})
