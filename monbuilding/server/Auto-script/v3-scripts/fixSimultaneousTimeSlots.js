
import { Runner } from '../autoscript-runner'
import { MarketServices } from '/common/collections/marketPlace'

Runner.newPatch(3, 'Change simultaneous time slots', function () {
  const services = MarketServices.find({ simultaneous_time_slot: { $ne: null } })
  let changes = 0

  services.forEach(service => {
    let simultaneous = service.simultaneous_time_slot
    Object.keys(service.openedDays).forEach((d) => {
      service.openedDays[d].forEach((t, iT) => {
        service.openedDays[d][iT].simultaneous_time_slot = simultaneous
      })
    })
    delete service.simultaneous_time_slot
    console.log('service._id', service._id)
    MarketServices.update({ _id: service._id }, service)
    changes++
  });
  return changes
})
