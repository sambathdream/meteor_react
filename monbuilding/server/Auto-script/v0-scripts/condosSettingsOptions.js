import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Update condo settings options', function () {
  const modulesToChange = {
    marketPlace: false,
    laundryRoom: false
  }
  let total = 0
  _.each(modulesToChange, (value, key) => {
    total += Condos.update({ ['settings.options.' + key]: { $exists: false } }, {
      $set: {
        ['settings.options.' + key]: value
      }
    }, { multi: true })
  })
  return total
})
