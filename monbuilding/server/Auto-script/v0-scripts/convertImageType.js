import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Convert ads images', function () {
  const ads = ClassifiedsAds.find(
    {attachments: { $exists: false}},
    {fields: {files: 1}}
  ).fetch();
  if (ads.length === 0) { return 0; }
  ads.forEach(m => {
    const files = m.files || [];
    const attachments = [];
    for (const file of files) {
      if (typeof file === 'string') {
        const fileData = UserFiles.findOne({_id: file});
        if (fileData) {
          attachments.push({
            fileId: file,
            mime: fileData.type,
            ext: fileData.extension
          })
        } else {
          console.log(`file ${file} not found for Ad ${m._id}`);
        }
      } else if (typeof file === 'object' && typeof file.fileId === 'string') {
        attachments.push(file);
      } else {
        console.log(`Unknow file type in Ad ${m._id}: ${JSON.stringify(file)}`)
      }
    }
    ClassifiedsAds.update(m._id, {
      $set: {
        attachments
      }});
  });
  return ads.length;
})

Runner.newPatch(0, 'Convert info images', function () {
  const infos = ActuPosts.find(
    {attachments: { $exists: false}},
    {fields: {files: 1}}
  ).fetch();
  if (infos.length === 0) { return 0; }
  infos.forEach(m => {
    const files = m.files || [];
    const attachments = [];
    for (const file of files) {
      if (typeof file === 'string') {
        const fileData = UserFiles.findOne({_id: file});
        if (fileData) {
          attachments.push({
            fileId: file,
            mime: fileData.type,
            ext: fileData.extension
          })
        } else {
          console.log(`file ${file} not found for Info ${m._id}`);
        }
      } else if (typeof file === 'object' && typeof file.fileId === 'string') {
        attachments.push(file);
      } else {
        console.log(`Unknow file type in Info ${m._id}: ${JSON.stringify(file)}`)
      }
    }

    ActuPosts.update(m._id, {
      $set: {
        attachments
      }});
  });
  return infos.length;
})

Runner.newPatch(0, 'Convert forum images', function () {
  const infos = ForumPosts.find(
    {attachments: { $exists: false}},
    {fields: {filesId: 1}}
  ).fetch();
  if (infos.length === 0) { return 0; }
  infos.forEach(m => {
    const files = m.filesId || [];
    const attachments = [];
    for (const file of files) {
      if (typeof file === 'string') {
        const fileData = ForumFiles.findOne({_id: file});
        if (fileData) {
          attachments.push({
            fileId: file,
            mime: fileData.type,
            ext: fileData.extension
          })
        } else {
          console.log(`file ${file} not found for Forum ${m._id}`);
        }
      } else if (file && typeof file === 'object' && typeof file.fileId === 'string') {
        attachments.push(file);
      } else {
        console.log(`Unknow file type in Forum ${m._id}: ${JSON.stringify(file)}`)
      }
    }

    ForumPosts.update(m._id, {
      $set: {
        attachments
      }});
  });
  return infos.length;
})

Runner.newPatch(0, 'Convert condo images', function () {
  const condos = Condos.find(
    {photo: { $exists: false}},
    {logo: { $exists: false}}
  ).fetch();
  if (condos.length === 0) { return 0; }
  condos.forEach(m => {
    const photos = CondoPhotos.find({condoId: m._id}).fetch();
    const logos = CondoLogos.find({condoId: m._id}).fetch();
    let photo = null
    let logo = null
    if (photos.length > 1) {
      console.log(`Condo ${m._id} has ${photos.length} photos`)
    }
    if (logos.length > 1) {
      console.log(`Condo ${m._id} has ${logos.length} logos`)
    }

    if (photos.length > 0) {
      const file = photos[0]
      if (!file.fileId || !file.type || !file.ext) {
        console.log(`Photo data incomplete for condo ${m._id}`)
      }
      photo = {
        fileId: file.fileId,
        mime: file.type,
        ext: file.ext
      }
    }
    if (logos.length > 0) {
      const file = logos[0]
      if (!file.fileId || !file.type || !file.ext) {
        console.log(`Logo data incomplete for condo ${m._id}`)
      }
      logo = {
        fileId: file.fileId,
        mime: file.type,
        ext: file.ext
      }
    }

    Condos.update(m._id, {
      $set: {
        photo,
        logo
      }});
  });
  return condos.length;
})
