import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Add Manager Join Date', function () {
  const enterprises = Enterprises.find(
    {createdAt: { $exists: false}},
    { fields: { users: 1}}
  ).fetch();
  if (enterprises.length === 0) { return 0; }
  const date = 1529812800000.0;
  for (const enterprise of enterprises) {
    for (const user of enterprise.users) {
      user.createdAt = date;
      for (const condo of user.condosInCharge) {
        condo.joined = date;
      }
    }
    Enterprises.update({_id: enterprise._id}, { $set: {
      createdAt: date,
      users: enterprise.users
    }});
  }
  return enterprises.length;
})
