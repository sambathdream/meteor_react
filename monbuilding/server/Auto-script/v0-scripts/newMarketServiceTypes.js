import { MarketServiceTypes, MarketReservations, MarketServices } from '/common/collections/marketPlace'
import _ from 'lodash'

const services = [
  {
    key: 'housekeeping'
  },
  {
    key: 'laundry'
  },
  {
    key: 'ironing'
  },
  {
    key: 'food'
  },
  {
    key: 'saleofgoods'
  },
  {
    key: 'offerofservices'
  }
]


import {Runner} from '../autoscript-runner'

Runner.newPatch(1, 'Add services types', function () {
  const condos = Condos.find({ 'settings.options.marketPlace': true }).fetch()
  let updateCount = 0
  if (condos) {
    const condoIds = condos.map(c => c._id)

    const existing = MarketServiceTypes.find({ condoId: { $in: condoIds } }, { $group: { _id: "$condoId" }}).fetch()

    const existingCondoIds = _.uniq(existing.map(c => c.condoId)) || []
    const diff = _.difference(condoIds, existingCondoIds)

    if (diff && diff.length) {
      diff.forEach(condoId => {
        services.forEach(service => {
          MarketServiceTypes.insert({
            condoId,
            ...service
          })
        })
        updateCount++
      })
      console.log('diff', diff)
    }
    if (!!existing) {
      existing.forEach(e => {
        const service = services.find(s => s.key === e.key)
        // This will take the existing value on the dB and only add new fields from the above `services` object
        let newValues = { ...service, ...e }
        updateCount++
        MarketServiceTypes.update({ _id: e._id }, {
          ...service
        })
      })
    }
  }
  return updateCount
})
