import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Add forum categories', function () {
  const entry = ForumCategories.findOne({})
  if (!entry) {
    return ForumCategories.insert({
      condoId: 'default',
      categories: [
        {
          "id": "category0",
          "fr": "Informatique",
          "en": "IT"
        },
        {
          "id": "category1",
          "fr": "RH",
          "en": "HR"
        },
        {
          "id": "category2",
          "fr": "Middle Office",
          "en": "Middle Office"
        },
        {
          "id": "category3",
          "fr": "Back Office",
          "en": "Back Office"
        },
        {
          "id": "category4",
          "fr": "Front Office",
          "en": "Front Office"
        },
        {
          "id": "category5",
          "fr": "Directoire",
          "en": "Board of directors"
        }
      ]
    })
  } else {
    return 0
  }
})
