import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Add information comment counter', function () {
  const information = ActuPosts.find(
    {$or: [
      {viewsCount: { $exists: false}},
      {commentCount: { $exists: false}}
    ]},
    {fields: {views: 1, condoId: 1}}
  ).fetch();
  if (information.length === 0) { return 0; }
  information.forEach(m => {
    const condoId = m.condoId;
    if (EventCommentsFeed[condoId] === undefined || !EventCommentsFeed[condoId]) {
      name = "EventCommentsFeed" + condoId;
      EventCommentsFeed[condoId] = new Mongo.Collection(name);
    }
    const views = new Set(m.views);
    const viewsArray = Array.from(views);
    commentCount = EventCommentsFeed[condoId].find({
      eventId: m._id
    }).count();

    ActuPosts.update(m._id, {
      $set: {
        viewsCount: views.size,
        views: viewsArray,
        userViews: viewsArray,
        commentCount
      }});
  });
  return information.length;
})

Runner.newPatch(0, 'Add ads comment counter', function () {
  const ads = ClassifiedsAds.find(
    {$or: [
      {viewsCount: { $exists: false}},
      {commentCount: { $exists: false}}
    ]},
    {fields: {userViews: 1}}
  ).fetch();
  if (ads.length === 0) { return 0; }
  ads.forEach(m => {
    const condoId = m.condoId;
    commentCount = ClassifiedsFeed.find({
      classifiedId: m._id
    }).count();

    ClassifiedsAds.update(m._id, {
      $set: {
        viewsCount: m.userViews.length,
        commentCount
      }});
  });
  return ads.length;
})

Runner.newPatch(0, 'Add forum comment counter', function () {
  const ads = ForumPosts.find(
    {$or: [
      {viewsCount: { $exists: false}},
      {commentCount: { $exists: false}}
    ]},
    {fields: {userViews: 1}}
  ).fetch();
  if (ads.length === 0) { return 0; }
  ads.forEach(m => {
    commentCount = ForumCommentPosts.find({
      postId: m._id
    }).count();

    ForumPosts.update(m._id, {
      $set: {
        viewsCount: m.userViews.length,
        commentCount
      }});
  });
  return ads.length;
})
