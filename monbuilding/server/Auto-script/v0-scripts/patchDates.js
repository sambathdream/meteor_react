import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Patch incident updateAt date', function () {
  const incidents = Incidents.find({
    $or: [
      {updatedAt: {$type: 'date'}},
      {createdAt: {$type: 'date'}}
    ]
  },{fields: {updatedAt: 1, createdAt: 1}}).fetch();
  if (incidents.length === 0) { return 0; }
  incidents.forEach(incident => {
    Incidents.update({_id: incident._id}, { $set: {
      updatedAt: new Date(incident.updatedAt).getTime(),
      createdAt: new Date(incident.createdAt).getTime()
    }});
  });
  return incidents.length;
})

Runner.newPatch(0, 'Patch resident condo joined date', function () {
  const residents = Residents.find({
    'condos.joined': {$type: 'date'}
  }).fetch();
  if (residents.length === 0) { return 0; }
  residents.forEach(resident => {
    for (const condo of resident.condos) {
      condo.joined = new Date(condo.joined).getTime();
    }
    Residents.update({_id: resident._id}, resident);
  });
  return residents.length;
})
