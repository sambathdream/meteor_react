import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Copy image data to users', function () {
  const users = Meteor.users.find({
    $or: [
      {avatar: {$exists: false}},
      {$and: [{'avatar.ext': {$exists: false}}, {avatar: {$ne: null}}]}
    ]
  },{fields: {_id: 1, identities: 1}}).fetch();
  if (users.length === 0) { return 0; }
  users.forEach(user => {
    let avatar = null;
    let fileId = null;
    if (user.identities && user.identities.residentId) {
      const residentData = Residents.findOne({_id: user.identities.residentId}, {fields: {fileId: 1}});
      if (residentData) {
        fileId = residentData.fileId;
      } else {
        console.log('Resident not found for user:', user._id);
      }
    } else if (user.identities && user.identities.gestionnaireId) {
      const enterprise = Enterprises.findOne(
        {_id: user.identities.gestionnaireId},
        {fields: {'users' : {$elemMatch: {userId: user._id}}, "users.profile.fileId": 1}}
      );
      if (!enterprise) {
        console.log('enterprise not found for user:', user._id);
      } else if (!enterprise.users || !enterprise.users[0] || !enterprise.users[0].profile) {
        console.log('enterprise user not found for user:', user._id);
      } else {
        fileId = enterprise.users[0].profile.fileId;
      }
    }
    if (fileId) {
      const fileData = UserFiles.findOne({_id: fileId});
      if (fileData) {
        avatar = {
          fileId,
          mime: fileData.type,
          ext: fileData.extension
        }
      } else {
        console.log('file not found for ', user.identities.residentId ? 'resident' : 'manager', user._id);
      }
    }
    Meteor.users.update({_id: user._id}, { $set: {
      avatar
    }});
  });
  return users.length;
})
