const newRightsOccupant = [
  {
    "module": "reservation",
    displayOrder: 8.9,
    "right": "see",
    "default": true,
    "value-fr": "Voir la rubrique « Réservation »",
    "value-en": "See the « Reservation » section"
  },
  {
    "module": "marketPlace",
    displayOrder: 15,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Market Place »",
    "value-en": "See the « Market Place » section"
  },
  {
    "module": "laundryRoom",
    displayOrder: 16,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Laverie »",
    "value-en": "See the « Laundry » section"
  },
  {
    "module": "print",
    displayOrder: 17,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Impression »",
    "value-en": "See the « Print » section"
  },
]

const newRightsManager = [
  {
    "module": "reservation",
    displayOrder: 9.9,
    "right": "see",
    "default": true,
    "value-fr": "Voir la rubrique « Réservation »",
    "value-en": "See the « Reservation » section"
  },
  {
    "module": "marketPlace",
    displayOrder: 15,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Market Place »",
    "value-en": "See the « Market Place » section"
  },
  {
    "module": "marketPlace",
    displayOrder: 15,
    "right": "addItem",
    "default": false,
    "value-fr": "Ajouter un article à la market place",
    "value-en": "Add an item to the market place"
  },
  {
    "module": "marketPlace",
    displayOrder: 15,
    "right": "editItem",
    "default": false,
    "value-fr": "Editer un article/service de la market place",
    "value-en": "Edit an item/service of the market place"
  },
  {
    "module": "marketPlace",
    displayOrder: 15,
    "right": "deleteItem",
    "default": false,
    "value-fr": "Supprimer un article/service de la market place",
    "value-en": "Delete an item/service of the market place"
  },
  {
    "module": "laundryRoom",
    displayOrder: 16,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Laverie »",
    "value-en": "See the « Laundry » section"
  },
  {
    "module": "laundryRoom",
    displayOrder: 16,
    "right": "edit",
    "default": false,
    "value-fr": "Editer les infos des laveries",
    "value-en": "Edit the infos of an laundry"
  },
  {
    "module": "trombi",
    displayOrder: 6,
    "right": "edit",
    "default": false,
    "value-fr": "Editer le profile",
    "value-en": "Edit user profile"
  },
  {
    "module": "print",
    displayOrder: 17,
    "right": "see",
    "default": false,
    "value-fr": "Voir la rubrique « Impression »",
    "value-en": "See the « Print » section"
  },
]

import {Runner} from '../autoscript-runner'

Runner.newPatch(0, 'Add rights for: reservation, marketPlace, laundryRoom, trombi, print', function () {
  let updateCount = 0
  const defaultRolesOccupantId = _.map(DefaultRoles.find({ for: 'occupant' }, { fields: { _id: true } }).fetch(), '_id')
  newRightsOccupant.forEach(right => {
    defaultRolesOccupantId.forEach(roleId => {
      const thisRole = DefaultRoles.findOne({
        _id: roleId, rights: {
          $elemMatch: {
            module: right.module,
            right: right.right
          }
        }
      })

      if (!thisRole) {
        console.log(`Adding right: occupant / ${right.module} / ${right.right}`)
        updateCount += DefaultRoles.update({ _id: roleId }, {
          $push: {
            rights: right
          }
        })
      }
    })
  })

  const defaultRolesManagerId = _.map(DefaultRoles.find({ for: 'manager' }, { fields: { _id: true } }).fetch(), '_id')
  newRightsManager.forEach(right => {
    defaultRolesManagerId.forEach(roleId => {
      const thisRole = DefaultRoles.findOne({
        _id: roleId, rights: {
          $elemMatch: {
            module: right.module,
            right: right.right
          }
        }
      })

      if (!thisRole) {
        console.log(`Adding right: manager / ${right.module} / ${right.right}`)
        updateCount += DefaultRoles.update({ _id: roleId }, {
          $push: {
            rights: right
          }
        })
      }
    })
  })
  return updateCount
})
