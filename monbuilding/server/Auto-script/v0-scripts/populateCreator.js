import {Runner} from '../autoscript-runner'

Runner.newPatch(0, '[Optimization] Add new fields to Messenger', function () {
  const defaultRoles = DefaultRoles.find({}, {fields: {name: 1}}).fetch();
  const dfrMap = defaultRoles.reduce((map, df) => {
    map[df._id] = getRoleDestination(df.name);
    return map;
  }, {});
  dfrMap['manager'] = 'manager';
  const messages = Messages.find(
    {$or: [
      {createdBy: { $exists: false}},
      {createdAt: { $exists: false}},
      {userUpdateView: {$exists: false}}
    ]},
    {fields: {origin: 1, 'users.userId': 1, 'users.lastVisit': 1, lastMsg: 1, target: 1, date: 1}}
  ).fetch();
  if (messages.length === 0) { return 0; }
  messages.forEach(m => {
    const lastUpdate = Math.max(m.date || 0, m.lastMsg || 0)
    Messages.update(m._id, {
      $set: {
        createdBy: m.origin,
        participants: Object.values(m.users).map(u => u.userId),
        userUpdateView: Array.from(new Set(Object.values(m.users).filter(u => u.lastVisit >= lastUpdate).map(u => u.userId))),
        destination: dfrMap[m.target],
        createdAt: m.date || 0,
        updatedAt: lastUpdate
      }});
  });
  return messages.length;
})

Runner.newPatch(0, '[Optimization] Add new fields to Forum', function () {
  const forums = ForumPosts.find(
    {$or: [
      {createdBy: { $exists: false}},
      {createdAt: { $exists: false}},
      {userUpdateView: { $exists: false}}
    ]},
    {fields: {userId: 1, views: 1, updatedAt: 1, date: 1}}
  ).fetch();
  if (forums.length === 0) { return 0; }
  forums.forEach(f => {
    const comments = ForumCommentPosts.find({postId: f._id}, {fields: {userId: 1}}).fetch();
    const participants = Array.from(comments.reduce((set, c) => set.add(c.userId), new Set([f.userId])));
    const lastUpdate = Math.max(f.updatedAt || 0, f.date || 0);
    ForumPosts.update(f._id, {
      $set: {
        createdBy: f.userId,
        userViews: Object.keys(f.views),
        userUpdateView: Array.from(new Set(Object.keys(f.views).filter(u => f.views[u] >= lastUpdate))),
        participants: participants,
        updatedAt: lastUpdate,
        createdAt: f.date || 0
      }});
  });
  return forums.length;
})

Runner.newPatch(0, '[Optimization] Add new fields to Ads', function () {
  const ads = ClassifiedsAds.find(
    {userUpdateView: { $exists: false}},
    {fields: {userId: 1, views: 1, updatedAt: 1, createdAt: 1}}
  ).fetch();
  if (ads.length === 0) { return 0; }
  ads.forEach(f => {
    let comments = ClassifiedsFeed.find({classifiedId: f._id}, {fields: {userId: 1}}).fetch();
    const participants = Array.from(comments.reduce((set, c) => set.add(c.userId), new Set([f.userId])));
    const lastUpdate = Math.max(f.updatedAt || 0, f.createdAt);
    ClassifiedsAds.update(f._id, {
      $set: {
        createdBy: f.userId,
        userViews: Object.keys(f.views),
        userUpdateView: Array.from(new Set(Object.keys(f.views).filter(u => f.views[u] >= lastUpdate))),
        participants: participants,
        updatedAt: lastUpdate
      }});
  });
  return ads.length;
})

Runner.newPatch(0, '[Optimization] Add new fields to Information', function () {
  const ads = ActuPosts.find(
    {userUpdateView: { $exists: false}},
    {fields: {userId: 1, views: 1, updatedAt: 1, createdAt: 1, participants: 1, startDate: 1}}
  ).fetch();
  if (ads.length === 0) { return 0; }
  ads.forEach(f => {
    const lastUpdate = Math.max(f.updatedAt || 0, f.createdAt);
    ActuPosts.update(f._id, {
      $set: {
        createdBy: f.userId,
        userUpdateView: f.views,
        updatedAt: lastUpdate,
        participants: f.participants || [],
        type: f.startDate === '' ? 'information' : 'event'
      }});
  });
  return ads.length;
})

Runner.newPatch(0, '[Optimization] Add new fields to Incidents', function () {
  const incidents = Incidents.find(
    {createdBy: { $exists: false}},
    {fields: { 'declarer.userId': 1, 'history.userId': 1, 'eval.userId': 1, updatedAt: 1, createdAt: 1, view: 1}}
  ).fetch();
  if (incidents.length === 0) { return 0; }
  incidents.forEach(f => {
    const participants = new Set();
    (f.history || []).reduce((obj, {userId}) => participants.add(userId), participants);
    (f.eval || []).reduce((obj, {userId}) => participants.add(userId), participants);
    const lastUpdate = Math.max(f.updatedAt || 0, f.createdAt);

    Incidents.update(f._id, {
      $set: {
        createdBy: f.declarer.userId,
        userViews: Array.from(new Set(Object.values(f.view).filter(u => Math.max(u.lastView || 0, u.lastViewHistoric || 0) > lastUpdate).map(u => u.userId))),
        updatedAt: lastUpdate,
        participants: Array.from(participants)
      }});
  });
  return incidents.length;
})

Runner.newPatch(0, '[Optimization] Add new fields to Reservations', function () {
  const reservations = Reservations.find(
    {$or: [
      {createdBy: { $exists: false}},
      {validators: { $exists: false}}
    ]},
    {fields: { origin: 1, bookedBy: 1, creation: 1, updatedAt: 1, eventId: 1, participants: 1, resourceId: 1 }}
  ).fetch();
  if (reservations.length === 0) { return 0; }
  reservations.forEach(f => {
    const lastUpdate = Math.max(f.updatedAt || 0, f.creation || 0);
    const eventId = f.eventId || f.eventId._str
    const views = ViewOfReservation.findOne({$or: [{ _id: eventId }, { '_id._str': eventId }]}, {fields: {view: 1}});
    const resource = Resources.findOne({_id: f.resourceId});
    const needValidation = resource && resource.needValidation ? resource.needValidation.status : false;
    const validators = resource && resource.needValidation ? resource.needValidation.managerIds : [];
    Reservations.update(f._id, {
      $set: {
        createdBy: f.bookedBy || f.createdBy || '',
        userViews: views ? Array.from(new Set(Object.keys(views.view).filter(u => views.view[u] > lastUpdate))) : [],
        updatedAt: lastUpdate,
        participants: f.participants || [],
        validators,
        needValidation
      }});
  });
  return reservations.length;
});

/* Helpers */
const getRoleDestination = function (name) {
  switch (name) {
    case 'Conseil Syndical': return 'council';
    case 'Gardien': return 'keeper';
    case 'manager': return 'manager';
    case 'Occupant':
    case 'Services Généraux':
    case 'Services Courrier':
    case 'Office Manager':
    case 'Asset Manager':
    case 'Property Manager':
    case 'Mainteneur':
    case 'Opérateurs Restauration':
    case 'PC Sécurité':
    case 'Gardien':
    default:
      return 'occupant';
  }
}
