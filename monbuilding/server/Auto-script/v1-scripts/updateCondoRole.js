import {Runner} from '../autoscript-runner'

Runner.newPatch(1, 'Link DefaultRole to CondoRole using _id', function () {
  const condos = CondoRole.find({defaultRoleId: {$exists: false}}).fetch()
  if ( condos.length === 0 ) { return 0 }
  const defaultRoles = DefaultRoles.find({}, {fields: {name: 1}}).fetch()
  const roleByName = {}
  for (const role of defaultRoles) {
    roleByName[role.name] = role._id
  }

  for (const condoRole of condos) {
    condoRole.defaultRoleId = roleByName[condoRole.name]
    CondoRole.update({_id: condoRole._id}, condoRole)
  }
  return condos.length
})
