const eventFilter = (userId) => {
  return {
    $or: [
      {visibility: {$exists: false}},
      {
        $and: [
          {visibility: {$exists: true}},
          {
            $or: [
              {visibility: 'public'},
              {
                $and: [
                  {visibility: 'private'},
                  {participants: userId}
                ]
              }
            ]
          }
        ]
      }
    ]
  }
}

export {
  eventFilter
}
