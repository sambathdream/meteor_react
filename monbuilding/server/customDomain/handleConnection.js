import { WebApp } from 'meteor/webapp'
import { Meteor } from 'meteor/meteor'
import fs from 'fs'

const knownSubDomain = [
  'student-factory',
  'kley',
  'nexity-studea'
]

Meteor.startup(function () {
  WebApp.connectHandlers.use('/logo/enterpriseLarge.png', function (req, res, next) {  // PREFERED SIZE 60 * 250
    const subdomain = req.headers.host.split('.')[0]

    let fav_buffer = null
    let favicon = Assets.getBinary('customDomainConfig/enterpriseLarge.png')
    if (knownSubDomain.includes(subdomain)) {
      favicon = Assets.getBinary('customDomainConfig/' + subdomain + '/enterpriseLarge.png')
    }
    fav_buffer = new Buffer(favicon, 'binary')

    res.writeHead(200, {
      'Content-Type': 'image'
    })
    res.write(fav_buffer)
    res.end()
  })

  WebApp.connectHandlers.use('/logo/enterprise.png', function (req, res, next) {
    const subdomain = req.headers.host.split('.')[0]

    let fav_buffer = null
    let favicon = Assets.getBinary('customDomainConfig/enterpriseLogo.png')
    if (knownSubDomain.includes(subdomain)) {
      favicon = Assets.getBinary('customDomainConfig/' + subdomain + '/enterpriseLogo.png')
    }
    fav_buffer = new Buffer(favicon, 'binary')

    res.writeHead(200, {
      'Content-Type': 'image'
    })
    res.write(fav_buffer)
    res.end()
  })

  WebApp.connectHandlers.use('/favicon.ico', function (req, res, next) {
    const subdomain = req.headers.host.split('.')[0]

    let fav_buffer = null
    let favicon = Assets.getBinary('customDomainConfig/favico.ico')
    if (knownSubDomain.includes(subdomain)) {
      favicon = Assets.getBinary('customDomainConfig/' + subdomain + '/favico.ico')
    }
    fav_buffer = new Buffer(favicon, 'binary')

    res.writeHead(200, {
      'Content-Type': 'image'
    })
    res.write(fav_buffer)
    res.end()
  })

  WebApp.connectHandlers.use('/client-data.json', function (req, res, next) {
    const subdomain = req.headers.host.split('.')[0]
    let appConfig = Assets.getText('customDomainConfig/datas.js')

    if (knownSubDomain.includes(subdomain)) {
      appConfig = Assets.getText('customDomainConfig/' + subdomain + '/datas.js')
    }
    res.end(appConfig)
  })
})
