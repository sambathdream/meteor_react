Meteor.startup(function() {
	Meteor.methods({
		fixtures_residents() {
			if (!Meteor.fixtures)
				throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
			Residents.remove({});

			console.log("-- Residents");

			let user1 = Meteor.users.findOne({"emails.0.address": "resident1@gmail.com"});
			let user2 = Meteor.users.findOne({"emails.0.address": "resident2@gmail.com"});
			let userEliane = Meteor.users.findOne({"emails.0.address": "monbuilding75@gmail.com"});
			let eliane = Meteor.users.findOne({"emails.0.address": "eliane.lugassy@essec.edu"});
			let userQuentin = Meteor.users.findOne({"emails.0.address": "stopLeSpamDesNotifs@gmail.com"});
			let alex = Meteor.users.findOne({ "emails.0.address": "alex@gmail.com"});
			let victor = Meteor.users.findOne({"emails.0.address": "vmariot@student.42.fr"});
			let harith = Meteor.users.findOne({"emails.0.address": "harith.jaiel@mail.novancia.fr"});
			let alexis = Meteor.users.findOne({"emails.0.address": "fakeAddress1@yahoo.fr"});

			let condo1 = Condos.findOne({name: "Les Tuileries"});
			let condo2 = Condos.findOne({name: "Résidence de Grenelle"});

			let building1 = Buildings.findOne({"name": "Building"});
			let building2 = Buildings.findOne({"name": "Bâtiment A"});
			let building3 = Buildings.findOne({"name": "Bâtiment B"});
			let notifications = {
				"actualite": true,
				"incident": true,
				"forum_forum": true,
				"forum_syndic": true,
				"forum_reco": true,
				"forum_boite": true,
				"classifieds": true,
				"resa_new": true,
				"resa_rappel": true,
				"msg_resident": true,
				"msg_conseil": true,
				"msg_gardien" : true,
				"msg_gestion": true,
				"edl" : true
			};
			let notificationsFalse = {
				"actualite": false,
				"incident": false,
				"forum_forum": false,
				"forum_syndic": false,
				"forum_reco": false,
				"forum_boite": false,
				"classifieds": false,
				"resa_new": false,
				"resa_rappel": false,
				"msg_resident": false,
				"msg_conseil": false,
				"msg_gardien" : false,
				"msg_gestion": false,
				"edl" : false
			};

			let resident1 = Residents.insert({
				"userId": user1._id,
				"condos": [
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"invited": new Date(),
					"joined": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					},
					{
						"buildingId": building3._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});


			let resident2 = Residents.insert({
				"userId": user2._id,
				"condos": [
				{
					"condoId" : condo1._id,
					"notifications": notifications,
					"invited": new Date(),
					"joined": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			let residentEliane = Residents.insert({
				"userId": userEliane._id,
				"condos": [
				{
					"condoId": condo1._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "tenant"
					}
					],
				},
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			let rEliane = Residents.insert({
				"userId": eliane._id,
				"condos": [
				{
					"condoId": condo1._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "owner"
					}
					],
				},
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			let rAlex = Residents.insert({
				"userId": alex._id,
				"condos": [
				{
					"condoId": condo1._id,
					"notifications": notificationsFalse,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "owner"
					}
					],
				},
				{
					"condoId": condo2._id,
					"notifications": notificationsFalse,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			let rQuentin = Residents.insert({
				"userId": userQuentin._id,
				"condos": [
				{
					"condoId": condo1._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "owner"
					}
					],
				},
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			let rVictor = Residents.insert({
				"userId": victor._id,
				"condos": [
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "owner"
					},
					{
						"buildingId": building3._id,
						"type": "tenant"
					}
					],
				}
				],
				"pendings": []
			});
			let rHarith = Residents.insert({
				"userId": harith._id,
				"condos": [
				{
					"condoId": condo2._id,
					"notifications": notifications,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "tenant"
					}
					],
				}
				],
				"pendings": []
			});
			let rAlexis = Residents.insert({
				"userId": alexis._id,
				"condos": [
				{
					"condoId": condo1._id,
					"notifications": notificationsFalse,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building1._id,
						"type": "tenant"
					}
					],
				},
				{
					"condoId": condo2._id,
					"notifications": notificationsFalse,
					"joined": new Date(),
					"invited": new Date(),
					"preferences": {messagerie: true},
					"buildings" : [
					{
						"buildingId": building2._id,
						"type": "tenant"
					},
					{
						"buildingId": building3._id,
						"type": "owner"
					}
					],
				}
				],
				"pendings": []
			});

			Meteor.users.update(user1._id,
				{$set: {"identities.residentId": resident1}}
				);

			Meteor.users.update(user2._id,
				{$set: {"identities.residentId": resident2}}
				);

			Meteor.users.update(userEliane._id,
				{$set: {"identities.residentId": residentEliane}});

			Meteor.users.update(eliane._id,
				{$set: {"identities.residentId": rEliane}});

			Meteor.users.update(alex._id,
				{$set: {"identities.residentId": rAlex}});

			Meteor.users.update(userQuentin._id,
				{$set: {"identities.residentId": rQuentin}});

			Meteor.users.update(victor._id,
				{$set: {"identities.residentId": rVictor}});

			Meteor.users.update(harith._id,
				{$set: {"identities.residentId": rHarith}});

			Meteor.users.update(alexis._id,
				{$set: {"identities.residentId": rAlexis}});

			console.log("-- 9 identitites Resident added");
		},
	});
});
