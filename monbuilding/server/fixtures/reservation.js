Meteor.startup(function() {
   Meteor.methods({
     fixtures_module_reservation() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");

      console.log("-- Module Réservation");

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "reservation",
            title: "Réservation",
            slug: "reservation",
            defaultDiv: true,
            boards: ["resident"],
            data: {}
        }}});

      Condos.update(condo2._id,
          {$push: {modules: {
              name: "reservation",
              title: "Réservation",
              slug: "reservation",
              defaultDiv: true,
              boards: ["resident"],
              data: {}
          }}});

      console.log("-- Module reservation added in 2 Condos");

    },
  })
});
