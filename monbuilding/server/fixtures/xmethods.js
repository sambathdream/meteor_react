/*
** Fixtures
** Build si la table Condo est vide uniquement
** xmethods chargé en dernier
** Chaque methode fixture doit check si Meteor.fixtures vaut vrai
*/
Meteor.startup(function() {
   Meteor.methods({
     build_fixtures() {
       if (Condos.find().count() !== 0) {
         try {
           Meteor.call('user_allow_backoffice')
         } catch (e) {
           console.log("*** Fixture not build ***");
           return ;
         }
       }
      console.log("*** Building fixtures ***");
      Meteor.fixtures = true;
      Meteor.call('fixtures_users');
      Meteor.call('fixtures_condos');
      Meteor.call('fixtures_buildings');
      Meteor.call('fixtures_residents');
      Meteor.call('fixtures_admins');
      Meteor.call('fixtures_gestionnaires');
      Meteor.call('fixtures_module_incidents');
      Meteor.call('fixtures_module_forums');
      Meteor.call('fixtures_module_actualities');
      Meteor.call('fixtures_module_messageries');
      Meteor.call('fixtures_module_trombinoscope');
      Meteor.call('fixtures_module_reservation');
      Meteor.call('fixtures_schools');
      Meteor.call('fixtures_resources');
      Meteor.call('fixtures_phone_code');
      Meteor.call('fixtures_ServerScripts')
      Meteor.fixtures = false;
      console.log("*** Fixtures rebuild ***");
    }
   });

  Meteor.call('build_fixtures');
});
