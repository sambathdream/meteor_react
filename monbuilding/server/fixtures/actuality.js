Meteor.startup(function() {
  Meteor.methods({

    fixtures_module_actualities() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Actus.remove({});

      console.log("-- Module Actuality");

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      let actuality1 = Actus.insert({
        unfollowers: [],
      });
      let actuality2 = Actus.insert({
        unfollowers: [],
      });

      console.log("-- 2 actuality created");

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "actuality",
            title: "Information",
            slug: "information",
            defaultDiv: false,
            boards: ["resident", "gestionnaire"],
            data: {
              actualityId: actuality1
            }
        }}});

      Condos.update(condo2._id,
          {$push: {modules: {
              name: "actuality",
              title: "Information",
              slug: "information",
              defaultDiv: false,
              boards: ["resident", "gestionnaire"],
              data: {
                actualityId: actuality2
              }
          }}});

      console.log("-- 2 actuality modules added (on 2 condos)");
    },
  });
});
