Meteor.startup(function() {
    Meteor.methods({
        fixtures_resources() {
            if (!Meteor.fixtures)
                throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
        Resources.remove({});

        console.log("-- Fixtures resources");

        let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
        let condo2 = Condos.findOne({"name": "Les Tuileries"});

        let horaires = [
                {
                    "dow" : [0],
                    "start" : "00:00",
                    "end" : "00:01",
                    "open" : false,
                    "full" : false
                },
                {
                    "dow" : [1],
                    "start" : "08:00",
                    "end" : "20:00",
                    "open" : true,
                    "full" : false
                },
                {
                    "dow" : [2],
                    "start" : "08:00",
                    "end" : "20:00",
                    "open" : true,
                    "full" : false
                },
                {
                    "dow" : [3],
                    "start" : "08:00",
                    "end" : "12:00",
                    "open" : true,
                    "full" : false
                },
                {
                    "dow" : [4],
                    "start" : "08:00",
                    "end" : "20:00",
                    "open" : true,
                    "full" : false
                },
                {
                    "dow" : [5],
                    "start" : "08:00",
                    "end" : "20:00",
                    "open" : true,
                    "full" : false
                },
                {
                    "dow" : [6],
                    "start" : "00:00",
                    "end" : "00:01",
                    "open" : false,
                    "full" : false
                }
            ];
        Resources.insert({
            name: "Bellevue",
            type: "Salle de réunion",
            etage: "2e étage",
            condoId: condo1._id,
            seats: 20,
            persons: 40,
            events: [],
            horaires: horaires
        });
            Resources.insert({
                name: "Parisienne",
                type: "Salle de réunion",
                etage: "2e étage",
                condoId: condo2._id,
                seats: 20,
                persons: 40,
                events: [],
                horaires: horaires
            });

        Resources.insert({
            name: "Longchamps",
            type: "Salle de réunion",
            etage: "1er étage",
            condoId: condo1._id,
            seats: 30,
            persons: 60,
            events: [],
            horaires: horaires
        });
            Resources.insert({
                name: "Rose",
                type: "Salle de réunion",
                etage: "1er étage",
                condoId: condo2._id,
                seats: 30,
                persons: 60,
                events: [],
                horaires: horaires
            });

        Resources.insert({name: "Cowork 1", type: "Espace de coworking", etage: "1er étage", condoId: condo1._id, horaires: horaires, number: 1, events: []});
        Resources.insert({name: "Cowork 1", type: "Espace de coworking", etage: "1er étage", condoId: condo1._id, horaires: horaires, number: 2, events: []});
        Resources.insert({name: "Cowork 1", type: "Espace de coworking", etage: "1er étage", condoId: condo1._id, horaires: horaires, number: 3, events: []});
        Resources.insert({name: "Cowork 2", type: "Espace de coworking", etage: "3e étage", condoId: condo1._id, horaires: horaires, number: 1, events: []});
        Resources.insert({name: "Cowork 2", type: "Espace de coworking", etage: "3e étage", condoId: condo1._id, horaires: horaires, number: 2, events: []});
        Resources.insert({name: "Cowork 2", type: "Espace de coworking", etage: "3e étage", condoId: condo1._id, horaires: horaires, number: 3, events: []});
        Resources.insert({name: "WorkingSpace", type: "Espace de coworking", etage: "4e étage", condoId: condo2._id, horaires: horaires, number: 1, events: []});
        Resources.insert({name: "WorkingSpace", type: "Espace de coworking", etage: "4e étage", condoId: condo2._id, horaires: horaires, number: 2, events: []});
        Resources.insert({name: "WorkingSpace", type: "Espace de coworking", etage: "4e étage", condoId: condo2._id, horaires: horaires, number: 3, events: []});
        Resources.insert({name: "OpenS", type: "Espace de coworking", etage: "2e étage", condoId: condo2._id, horaires: horaires, number: 1, events: []});
        Resources.insert({name: "OpenS", type: "Espace de coworking", etage: "2e étage", condoId: condo2._id, horaires: horaires, number: 2, events: []});
        Resources.insert({name: "OpenS", type: "Espace de coworking", etage: "2e étage", condoId: condo2._id, horaires: horaires, number: 3, events: []});

        Resources.insert({name: "Etat des lieux", type: "Etat des lieux", etage: "-", condoId: condo1._id, horaires: horaires, events: [], edlDuration: 45});
        Resources.insert({name: "Etat des lieux", type: "Etat des lieux", etage: "-", condoId: condo2._id, horaires: horaires, events: [], edlDuration: 45});
        }
    });
});
