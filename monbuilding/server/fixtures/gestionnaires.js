Meteor.startup(function() {
   Meteor.methods({
     fixtures_gestionnaires() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Enterprises.remove({});

      console.log("-- Gestionnaires");

      let user1 = Meteor.users.findOne({"emails.0.address": "elianelugassy@gmail.com"});
      let user2 = Meteor.users.findOne({"emails.0.address": "gestionnaire@gmail.com"});
      let user3 = Meteor.users.findOne({"emails.0.address": "dev.monbuilding@gmail.com"});
      let user4 = Meteor.users.findOne({"emails.0.address": "kevin.longer11@gmail.com"});

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      let notifications = {
          "incidents": true,
          "forum_forum": true,
          "forum_syndic": true,
          "actualites": true,
          "classifieds": true,
          "messagerie": true,
          "edl": true,
          "new_user": true,
         };
        // access: 0 = only see; 1 = see + write; 2 = see + delete
      let gestionnaire = Enterprises.insert({
        createdAt: Date.now(),
        "name": "Foncia",
        "info": {
          'siret': '732 829 320 00074',
          'address': '1 rue des lilas',
          'code': '75020',
          'city': 'Paris',
          'email': 'foncia.enterprise@foncia.com',
          'tel': '0123456789',
          'chef': 'Quentin Rochefort :)',
        },
        "logo": {
          'path': '',
          'fileId': '',
          'showLogo': false,
          'showMonBuilding': true,
        },
        "condos": [condo1._id, condo2._id],
        "inactive": false,
        "users": [
          {
            createdAt: Date.now(),
            'userId': user1._id,
            'firstname': user1.profile.firstname,
            'lastname': user1.profile.lastname,
            'tel': user1.profile.tel,
            'level': 5,
            'type': "directionSiege",
            'condosInCharge': [
                {access: [0], joined: Date.now(), condoId: condo1._id, notifications: notifications},
                {access: [0], joined: Date.now(), condoId: condo2._id, notifications: notifications}],
              'profile': {
                'picture': '',
                'fileId': '',
              },
          },
          {
            createdAt: Date.now(),
            'userId': user2._id,
            'firstname': user2.profile.firstname,
            'lastname': user2.profile.lastname,
            'tel': user2.profile.tel,
            'level': 5,
            'type': "directionSiege",
            'condosInCharge': [
                {access: [0], joined: Date.now(), condoId: condo1._id, notifications: notifications},
                {access: [0], joined: Date.now(), condoId: condo2._id, notifications: notifications}],
              'profile': {
                'picture': '',
                'fileId': '',
              },
          },
          {
            createdAt: Date.now(),
            'userId': user3._id,
            'firstname': user3.profile.firstname,
            'lastname': user3.profile.lastname,
            'tel': user3.profile.tel,
            'level': 5,
            'type': "directionSiege",
            'condosInCharge': [
                {access: [0], joined: Date.now(), condoId: condo1._id, notifications: notifications},
                {access: [0], joined: Date.now(), condoId: condo2._id, notifications: notifications}],
              'profile': {
                'picture': '',
                'fileId': '',
              },
          },
          {
            createdAt: Date.now(),
            'userId': user4._id,
            'firstname': user4.profile.firstname,
            'lastname': user4.profile.lastname,
            'tel': user4.profile.tel,
            'level': 5,
            'type': "directionSiege",
            'condosInCharge': [
                {access: [0], joined: Date.now(), condoId: condo1._id, notifications: notifications},
                {access: [0], joined: Date.now(), condoId: condo2._id, notifications: notifications}],
              'profile': {
                'picture': '',
                'fileId': '',
              },
          }
        ],
        "settings": {
           "validUsers" : {
               "copro": true,
               "mono": true,
               "etudiante": false,
               "office": true,
           }

        },
        "history": []
      });

      Meteor.users.update(user1._id,
        {$set: {"identities.gestionnaireId": gestionnaire}}
      );
      Meteor.users.update(user2._id,
        {$set: {"identities.gestionnaireId": gestionnaire}}
      );
      Meteor.users.update(user3._id,
        {$set: {"identities.gestionnaireId": gestionnaire}}
      );
      Meteor.users.update(user4._id,
        {$set: {"identities.gestionnaireId": gestionnaire}}
      );


      console.log("-- 1 identitites Gestionnaire added (with 4 user)");
    },
  });
});
