Meteor.startup(function() {
	Meteor.methods({
		fixtures_phone_code() {
			if (!Meteor.fixtures)
				throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
			PhoneCode.remove({});

			console.log("-- Phone Code");


			PhoneCode.insert({
        country: "Afghanistan",
        countryEn: 'Afghanistan',
				code: 93
			});

			PhoneCode.insert({
        country: "Afrique du Sud",
        countryEn: 'South Africa',
				code: 27
			});

			PhoneCode.insert({
        country: "Albanie",
        countryEn: 'Albania',
				code: 355
			});

			PhoneCode.insert({
        country: "Algérie",
        countryEn: 'Algeria',
				code: 213
			});

			PhoneCode.insert({
        country: "Allemagne",
        countryEn: 'Germany',
				code: 49
			});

			PhoneCode.insert({
        country: "Andorre",
        countryEn: 'Andorra',
				code: 376
			});

			PhoneCode.insert({
        country: "Angola",
        countryEn: 'Angola',
				code: 244
			});

			PhoneCode.insert({
        country: "Anguilla",
        countryEn: 'Anguilla',
				code: 1264
			});

			PhoneCode.insert({
        country: "Antigua-et-Barbuda",
        countryEn: 'Antigua and Barbuda',
				code: 1268
			});

			PhoneCode.insert({
        country: "Arabie saoudite",
        countryEn: 'Saudi Arabia',
				code: 966
			});

			PhoneCode.insert({
        country: "Argentine",
        countryEn: 'Argentina',
				code: 54
			});

			PhoneCode.insert({
        country: "Arménie",
        countryEn: 'Armenia',
				code: 374
			});

			PhoneCode.insert({
        country: "Aruba",
        countryEn: 'Aruba',
				code: 297
			});

			PhoneCode.insert({
        country: "Australie",
        countryEn: 'Australia',
				code: 61
			});

			PhoneCode.insert({
        country: "Autriche",
        countryEn: 'Austria',
				code: 43
			});

			PhoneCode.insert({
        country: "Azerbaïdjan",
        countryEn: 'Azerbaijan',
				code: 994
			});

			PhoneCode.insert({
        country: "Bahamas",
        countryEn: 'The Bahamas',
				code: 1242
			});

			PhoneCode.insert({
        country: "Bahreïn",
        countryEn: 'Bahrain',
				code: 973
			});

			PhoneCode.insert({
        country: "Bangladesh",
        countryEn: 'Bangladesh',
				code: 880
			});

			PhoneCode.insert({
        country: "Barbade",
        countryEn: 'Barbados',
				code: 1246
			});

			PhoneCode.insert({
        country: "Belgique",
        countryEn: 'Belgium',
				code: 32
			});

			PhoneCode.insert({
        country: "Belize",
        countryEn: 'Belize',
				code: 501
			});

			PhoneCode.insert({
        country: "Bermudes",
        countryEn: 'Bermuda',
				code: 1441
			});

			PhoneCode.insert({
        country: "Bhoutan",
        countryEn: 'Bhutan',
				code: 975
			});

			PhoneCode.insert({
        country: "Birmanie",
        countryEn: 'Burma',
				code: 95
			});

			PhoneCode.insert({
        country: "Biélorussie",
        countryEn: 'Belarus',
				code: 375
			});

			PhoneCode.insert({
        country: "Bolivie",
        countryEn: 'Bolivia',
				code: 591
			});

			PhoneCode.insert({
        country: "Bosnie-Herzégovine",
        countryEn: 'Bosnia and Herzegovina',
				code: 387
			});

			PhoneCode.insert({
        country: "Botswana",
        countryEn: 'Botswana',
				code: 267
			});

			PhoneCode.insert({
        country: "Brunei",
        countryEn: 'Brunei',
				code: 673
			});

			PhoneCode.insert({
        country: "Brésil",
        countryEn: 'Brazil',
				code: 55
			});

			PhoneCode.insert({
        country: "Bulgarie",
        countryEn: 'Bulgaria',
				code: 359
			});

			PhoneCode.insert({
        country: "Burkina Faso",
        countryEn: 'Burkina Faso',
				code: 226
			});

			PhoneCode.insert({
        country: "Burundi",
        countryEn: 'Burundi',
				code: 257
			});

			PhoneCode.insert({
        country: "Bénin",
        countryEn: 'Benin',
				code: 229
			});

			PhoneCode.insert({
        country: "Cambodge",
        countryEn: 'Cambodia',
				code: 855
			});

			PhoneCode.insert({
        country: "Cameroun",
        countryEn: 'Cameroon',
				code: 237
			});

			PhoneCode.insert({
        country: "Canada",
        countryEn: 'Canada',
				code: 1
			});

			PhoneCode.insert({
        country: "Cap-Vert",
        countryEn: 'Cape Verde',
				code: 238
			});

			PhoneCode.insert({
        country: "Centrafrique",
        countryEn: 'The Centrafrique',
				code: 236
			});

			PhoneCode.insert({
        country: "Chili",
        countryEn: 'Chile',
				code: 56
			});

			PhoneCode.insert({
        country: "Chypre",
        countryEn: 'Cyprus',
				code: 357
			});

			PhoneCode.insert({
        country: "Colombie",
        countryEn: 'Colombia',
				code: 57
			});

			PhoneCode.insert({
        country: "Comores",
        countryEn: 'Comoros',
				code: 269
			});

			PhoneCode.insert({
        country: "Corée du Nord",
        countryEn: 'North Korea',
				code: 850
			});

			PhoneCode.insert({
        country: "Corée du Sud",
        countryEn: 'South Korea',
				code: 82
			});

			PhoneCode.insert({
        country: "Costa Rica",
        countryEn: 'Costa Rica',
				code: 506
			});

			PhoneCode.insert({
        country: "Croatie",
        countryEn: 'Croatia',
				code: 385
			});

			PhoneCode.insert({
        country: "Cuba",
        countryEn: 'Cuba',
				code: 53
			});

			PhoneCode.insert({
        country: "Côte dIvoire",
        countryEn: 'Ivory Coast',
				code: 225
			});

			PhoneCode.insert({
        country: "Danemark",
        countryEn: 'Denmark',
				code: 45
			});

			PhoneCode.insert({
        country: "Djibouti",
        countryEn: 'Djibouti',
				code: 253
			});

			PhoneCode.insert({
        country: "Dominique",
        countryEn: 'Dominique',
				code: 1767
			});

			PhoneCode.insert({
        country: "Espagne",
        countryEn: 'Spain',
				code: 34
			});

			PhoneCode.insert({
        country: "Estonie",
        countryEn: 'Estonia',
				code: 372
			});

			PhoneCode.insert({
        country: "Fidji",
        countryEn: 'Fiji',
				code: 679
			});

			PhoneCode.insert({
        country: "Finlande",
        countryEn: 'Finland',
				code: 358
			});

			PhoneCode.insert({
        country: "France",
        countryEn: 'France',
				code: 33
			});

			PhoneCode.insert({
        country: "Gabon",
        countryEn: 'Gabon',
				code: 241
			});

			PhoneCode.insert({
        country: "Gambie",
        countryEn: 'Gambia',
				code: 220
			});

			PhoneCode.insert({
        country: "Ghana",
        countryEn: 'Ghana',
				code: 233
			});

			PhoneCode.insert({
        country: "Gibraltar",
        countryEn: 'Gibraltar',
				code: 350
			});

			PhoneCode.insert({
        country: "Grenade",
        countryEn: 'Grenada(Granada)',
				code: 1473
			});

			PhoneCode.insert({
        country: "Groenland",
        countryEn: 'Greenland',
				code: 299
			});

			PhoneCode.insert({
        country: "Grèce",
        countryEn: 'Greece',
				code: 30
			});

			PhoneCode.insert({
        country: "Guadeloupe",
        countryEn: 'Guadeloupe',
				code: 590
			});

			PhoneCode.insert({
        country: "Guam",
        countryEn: 'Guam',
				code: 1671
			});

			PhoneCode.insert({
        country: "Guatemala",
        countryEn: 'Guatemala',
				code: 502
			});

			PhoneCode.insert({
        country: "Guinée",
        countryEn: 'Guinea',
				code: 224
			});

			PhoneCode.insert({
        country: "Guinée équatoriale",
        countryEn: 'Equatorial Guinea',
				code: 240
			});

			PhoneCode.insert({
        country: "Guinée-Bissau",
        countryEn: 'Guinea-Bissau',
				code: 245
			});

			PhoneCode.insert({
        country: "Guyana",
        countryEn: 'Guyana',
				code: 592
			});

			PhoneCode.insert({
        country: "Guyane",
        countryEn: 'Guiana',
				code: 594
			});

			PhoneCode.insert({
        country: "Géorgie",
        countryEn: 'Georgia',
				code: 995
			});

			PhoneCode.insert({
        country: "Haïti",
        countryEn: 'Haiti',
				code: 509
			});

			PhoneCode.insert({
        country: "Honduras",
        countryEn: 'Honduras',
				code: 504
			});

			PhoneCode.insert({
        country: "Hong Kong",
        countryEn: 'Hong-Kong',
				code: 852
			});

			PhoneCode.insert({
        country: "Hongrie",
        countryEn: 'Hungary',
				code: 36
			});

			PhoneCode.insert({
        country: "Inde",
        countryEn: 'India',
				code: 91
			});

			PhoneCode.insert({
        country: "Indonésie",
        countryEn: 'Indonesia',
				code: 62
			});

			PhoneCode.insert({
        country: "Irak",
        countryEn: 'Iraq',
				code: 964
			});

			PhoneCode.insert({
        country: "Iran",
        countryEn: 'Iran',
				code: 98
			});

			PhoneCode.insert({
        country: "Irlande",
        countryEn: 'Ireland',
				code: 353
			});

			PhoneCode.insert({
        country: "Islande",
        countryEn: 'Iceland',
				code: 354
			});

			PhoneCode.insert({
        country: "Israël",
        countryEn: 'Israel',
				code: 972
			});

			PhoneCode.insert({
        country: "Italie",
        countryEn: 'Italy',
				code: 39
			});

			PhoneCode.insert({
        country: "Jamaïque",
        countryEn: 'Jamaica',
				code: 1876
			});

			PhoneCode.insert({
        country: "Japon",
        countryEn: 'Japan',
				code: 81
			});

			PhoneCode.insert({
        country: "Jordanie",
        countryEn: 'Jordan',
				code: 962
			});

			PhoneCode.insert({
        country: "Kazakhstan",
        countryEn: 'Kazakhstan',
				code: 7
			});

			PhoneCode.insert({
        country: "Kenya",
        countryEn: 'Kenya',
				code: 254
			});

			PhoneCode.insert({
        country: "Kirghizistan",
        countryEn: 'Kyrgyzstan',
				code: 996
			});

			PhoneCode.insert({
        country: "Kiribati",
        countryEn: 'Kiribati',
				code: 686
			});

			PhoneCode.insert({
        country: "Koweït",
        countryEn: 'Kuwait',
				code: 965
			});

			PhoneCode.insert({
        country: "La Réunion",
        countryEn: 'Reunion',
				code: 262
			});

			PhoneCode.insert({
        country: "Laos",
        countryEn: 'Laos',
				code: 856
			});

			PhoneCode.insert({
        country: "Lesotho",
        countryEn: 'Lesotho',
				code: 266
			});

			PhoneCode.insert({
        country: "Lettonie",
        countryEn: 'Latvia',
				code: 371
			});

			PhoneCode.insert({
        country: "Liban",
        countryEn: 'Lebanon',
				code: 961
			});

			PhoneCode.insert({
        country: "Liberia",
        countryEn: 'Liberia',
				code: 231
			});

			PhoneCode.insert({
        country: "Libye",
        countryEn: 'Libya',
				code: 218
			});

			PhoneCode.insert({
        country: "Liechtenstein",
        countryEn: 'Liechtenstein',
				code: 423
			});

			PhoneCode.insert({
        country: "Lituanie",
        countryEn: 'Lithuania',
				code: 370
			});

			PhoneCode.insert({
        country: "Luxembourg",
        countryEn: 'Luxembourg',
				code: 352
			});

			PhoneCode.insert({
        country: "Macao",
        countryEn: 'Macao',
				code: 853
			});

			PhoneCode.insert({
        country: "Madagascar",
        countryEn: 'Madagascar',
				code: 261
			});

			PhoneCode.insert({
        country: "Malaisie",
        countryEn: 'Malaysia',
				code: 60
			});

			PhoneCode.insert({
        country: "Malawi",
        countryEn: 'Malawi',
				code: 265
			});

			PhoneCode.insert({
        country: "Maldives",
        countryEn: 'The Maldive Islands',
				code: 960
			});

			PhoneCode.insert({
        country: "Mali",
        countryEn: 'Mali',
				code: 223
			});

			PhoneCode.insert({
        country: "Malouines",
        countryEn: 'Falklands',
				code: 500
			});

			PhoneCode.insert({
        country: "Malte",
        countryEn: 'Malta',
				code: 356
			});

			PhoneCode.insert({
        country: "Maroc",
        countryEn: 'Morocco',
				code: 212
			});

			PhoneCode.insert({
        country: "Martinique",
        countryEn: 'Martinique',
				code: 596
			});

			PhoneCode.insert({
        country: "Maurice",
        countryEn: 'Maurice',
				code: 230
			});

			PhoneCode.insert({
        country: "Mauritanie",
        countryEn: 'Mauritania',
				code: 222
			});

			PhoneCode.insert({
        country: "Mayotte",
        countryEn: 'Mayotte',
				code: 262
			});

			PhoneCode.insert({
        country: "Mexique",
        countryEn: 'Mexico',
				code: 52
			});

			PhoneCode.insert({
        country: "Micronésie",
        countryEn: 'Micronesia',
				code: 691
			});

			PhoneCode.insert({
        country: "Moldavie",
        countryEn: 'Moldavia(Moldova)',
				code: 373
			});

			PhoneCode.insert({
        country: "Monaco",
        countryEn: 'Monaco',
				code: 377
			});

			PhoneCode.insert({
        country: "Mongolie",
        countryEn: 'Mongolia',
				code: 976
			});

			PhoneCode.insert({
        country: "Montserrat",
        countryEn: 'Montserrat',
				code: 1664
			});

			PhoneCode.insert({
        country: "Monténégro",
        countryEn: 'Montenegro',
				code: 382
			});

			PhoneCode.insert({
        country: "Mozambique",
        countryEn: 'Mozambique',
				code: 258
			});

			PhoneCode.insert({
        country: "Namibie",
        countryEn: 'Namibia',
				code: 264
			});

			PhoneCode.insert({
        country: "Nauru",
        countryEn: 'Nauru',
				code: 674
			});

			PhoneCode.insert({
        country: "Nicaragua",
        countryEn: 'Nicaragua',
				code: 505
			});

			PhoneCode.insert({
        country: "Niger",
        countryEn: 'Niger',
				code: 227
			});

			PhoneCode.insert({
        country: "Nigeria",
        countryEn: 'Nigeria',
				code: 234
			});

			PhoneCode.insert({
        country: "Niue",
        countryEn: 'Niue',
				code: 683
			});

			PhoneCode.insert({
        country: "Norvège",
        countryEn: 'Norway',
				code: 47
			});

			PhoneCode.insert({
        country: "Nouvelle-Calédonie",
        countryEn: 'New Caledonia',
				code: 687
			});

			PhoneCode.insert({
        country: "Nouvelle-Zélande",
        countryEn: 'New Zealand',
				code: 64
			});

			PhoneCode.insert({
        country: "Népal",
        countryEn: 'Nepal',
				code: 977
			});

			PhoneCode.insert({
        country: "Oman",
        countryEn: 'Oman',
				code: 968
			});

			PhoneCode.insert({
        country: "Ouganda",
        countryEn: 'Uganda',
				code: 256
			});

			PhoneCode.insert({
        country: "Ouzbékistan",
        countryEn: 'Uzbekistan',
				code: 998
			});

			PhoneCode.insert({
        country: "Pakistan",
        countryEn: 'Pakistan',
				code: 92
			});

			PhoneCode.insert({
        country: "Palaos",
        countryEn: 'Palaos',
				code: 680
			});

			PhoneCode.insert({
        country: "Panama",
        countryEn: 'Panama',
				code: 507
			});

			PhoneCode.insert({
        country: "Papouasie-Nouvelle-Guinée",
        countryEn: 'Papua New Guinea',
				code: 675
			});

			PhoneCode.insert({
        country: "Paraguay",
        countryEn: 'Paraguay',
				code: 595
			});

			PhoneCode.insert({
        country: "Pays-Bas",
        countryEn: 'Netherlands',
				code: 31
			});

			PhoneCode.insert({
        country: "Philippines",
        countryEn: 'The Philippines(Filipinos)',
				code: 63
			});

			PhoneCode.insert({
        country: "Pologne",
        countryEn: 'Poland',
				code: 48
			});

			PhoneCode.insert({
        country: "Polynésie française",
        countryEn: 'French Polynesia',
				code: 689
			});

			PhoneCode.insert({
        country: "Porto Rico",
        countryEn: 'Puerto Rico',
				code: 1
			});

			PhoneCode.insert({
        country: "Portugal",
        countryEn: 'Portugal',
				code: 351
			});

			PhoneCode.insert({
        country: "Pérou",
        countryEn: 'Peru',
				code: 51
			});

			PhoneCode.insert({
        country: "Qatar",
        countryEn: 'Qatar',
				code: 974
			});

			PhoneCode.insert({
        country: "Roumanie",
        countryEn: 'Romania',
				code: 40
			});

			PhoneCode.insert({
        country: "Royaume-Uni",
        countryEn: 'The United Kingdom',
				code: 44
			});

			PhoneCode.insert({
        country: "Russie",
        countryEn: 'Russia',
				code: 7
			});

			PhoneCode.insert({
        country: "Rwanda",
        countryEn: 'Rwanda',
				code: 250
			});

			PhoneCode.insert({
        country: "République de Macédoine",
        countryEn: 'Republic of Macedonia',
				code: 389
			});

			PhoneCode.insert({
        country: "République dominicaine",
        countryEn: 'Dominican Republic',
				code: 1
			});

			PhoneCode.insert({
        country: "République du Congo",
        countryEn: 'Republic of the Congo',
				code: 242
			});

			PhoneCode.insert({
        country: "République démocratique du Congo",
        countryEn: 'Democratic Republic of the Congo',
				code: 243
			});

			PhoneCode.insert({
        country: "République tchèque",
        countryEn: 'Czech Republic',
				code: 420
			});

			PhoneCode.insert({
        country: "Saint-Christophe-et-Niévès",
        countryEn: 'Saint Kitts and Nevis',
				code: 1869
			});

			PhoneCode.insert({
        country: "Saint-Marin",
        countryEn: 'San Marino',
				code: 378
			});

			PhoneCode.insert({
        country: "Saint-Pierre-et-Miquelon",
        countryEn: 'Saint Pierre and Miquelon',
				code: 508
			});

			PhoneCode.insert({
        country: "Saint-Vincent-et-les Grenadines",
        countryEn: 'Saint-Vincent-et-les Grenadines',
				code: 1784
			});

			PhoneCode.insert({
        country: "Sainte-Lucie",
        countryEn: 'Saint Lucia',
				code: 1758
			});

			PhoneCode.insert({
        country: "Salomon",
        countryEn: 'Salomon',
				code: 677
			});

			PhoneCode.insert({
        country: "Salvador",
        countryEn: 'Salvador',
				code: 503
			});

			PhoneCode.insert({
        country: "Samoa",
        countryEn: 'Samoa',
				code: 685
			});

			PhoneCode.insert({
        country: "Samoa américaines",
        countryEn: 'American Samoa',
				code: 1684
			});

			PhoneCode.insert({
        country: "Sao Tomé-et-Principe",
        countryEn: 'Sao Tomé-et-Principe',
				code: 239
			});

			PhoneCode.insert({
        country: "Serbie",
        countryEn: 'Serbia',
				code: 381
			});

			PhoneCode.insert({
        country: "Seychelles",
        countryEn: 'Seychelles',
				code: 248
			});

			PhoneCode.insert({
        country: "Sierra Leone",
        countryEn: 'Sierra Leone',
				code: 232
			});

			PhoneCode.insert({
        country: "Singapour",
        countryEn: 'Singapore',
				code: 65
			});

			PhoneCode.insert({
        country: "Slovaquie",
        countryEn: 'Slovakia',
				code: 421
			});

			PhoneCode.insert({
        country: "Slovénie",
        countryEn: 'Slovenia',
				code: 386
			});

			PhoneCode.insert({
        country: "Somalie",
        countryEn: 'Somalia',
				code: 252
			});

			PhoneCode.insert({
        country: "Soudan",
        countryEn: 'Sudan',
				code: 249
			});

			PhoneCode.insert({
        country: "Soudan du Sud",
        countryEn: 'South Sudan',
				code: 211
			});

			PhoneCode.insert({
        country: "Sri Lanka",
        countryEn: 'Sri Lanka',
				code: 94
			});

			PhoneCode.insert({
        country: "Suisse",
        countryEn: 'Switzerland(Swiss)',
				code: 41
			});

			PhoneCode.insert({
        country: "Suriname",
        countryEn: 'Suriname',
				code: 597
			});

			PhoneCode.insert({
        country: "Suède",
        countryEn: 'Sweden',
				code: 46
			});

			PhoneCode.insert({
        country: "Swaziland",
        countryEn: 'Swaziland',
				code: 268
			});

			PhoneCode.insert({
        country: "Syrie",
        countryEn: 'Syria',
				code: 963
			});

			PhoneCode.insert({
        country: "Sénégal",
        countryEn: 'Senegal',
				code: 221
			});

			PhoneCode.insert({
        country: "Tadjikistan",
        countryEn: 'Tadjkistan',
				code: 992
			});

			PhoneCode.insert({
        country: "Tanzanie",
        countryEn: 'Tanzania',
				code: 255
			});

			PhoneCode.insert({
        country: "Tchad",
        countryEn: 'Chad',
				code: 235
			});

			PhoneCode.insert({
        country: "Thaïlande",
        countryEn: 'Thailand',
				code: 66
			});

			PhoneCode.insert({
        country: "Timor oriental",
        countryEn: 'East Timor',
				code: 670
			});

			PhoneCode.insert({
        country: "Togo",
        countryEn: 'Togo',
				code: 228
			});

			PhoneCode.insert({
        country: "Tokelau",
        countryEn: 'Tokelau',
				code: 690
			});

			PhoneCode.insert({
        country: "Tonga",
        countryEn: 'Tonga',
				code: 676
			});

			PhoneCode.insert({
        country: "Trinité-et-Tobago",
        countryEn: 'Trinidad and Tobago',
				code: 1868
			});

			PhoneCode.insert({
        country: "Tunisie",
        countryEn: 'Tunisia',
				code: 216
			});

			PhoneCode.insert({
        country: "Turkménistan",
        countryEn: 'Turkmenistan',
				code: 993
			});

			PhoneCode.insert({
        country: "Turquie",
        countryEn: 'Turkey',
				code: 90
			});

			PhoneCode.insert({
        country: "Tuvalu",
        countryEn: 'Tuvalu',
				code: 688
			});

			PhoneCode.insert({
        country: "Ukraine",
        countryEn: 'Ukraine',
				code: 380
			});

			PhoneCode.insert({
        country: "Uruguay",
        countryEn: 'Uruguay',
				code: 598
			});

			PhoneCode.insert({
        country: "Vanuatu",
        countryEn: 'Vanuatu',
				code: 678
			});

			PhoneCode.insert({
        country: "Venezuela",
        countryEn: 'Venezuela',
				code: 58
			});

			PhoneCode.insert({
        country: "Viêt Nam",
        countryEn: 'Vietnam',
				code: 84
			});

			PhoneCode.insert({
        country: "Wallis-et-Futuna",
        countryEn: 'Wallis and Futuna',
				code: 681
			});

			PhoneCode.insert({
        country: "Yémen",
        countryEn: 'Yemen',
				code: 967
			});

			PhoneCode.insert({
        country: "Zambie",
        countryEn: 'Zambia',
				code: 260
			});

			PhoneCode.insert({
        country: "Zimbabwe",
        countryEn: 'Zimbabwe',
				code: 263
			});

			PhoneCode.insert({
        country: "Égypte",
        countryEn: 'Egypt',
				code: 20
			});

			PhoneCode.insert({
        country: "Émirats arabes unis",
        countryEn: 'United Arab Emirates',
				code: 971
			});

			PhoneCode.insert({
        country: "Équateur",
        countryEn: 'Ecuador',
				code: 593
			});

			PhoneCode.insert({
        country: "Érythrée",
        countryEn: 'Eritrea',
				code: 291
			});

			PhoneCode.insert({
        country: "États-Unis",
        countryEn: 'The United States',
				code: 1
			});

			PhoneCode.insert({
        country: "Éthiopie",
        countryEn: 'Ethiopia',
				code: 251
			});

			PhoneCode.insert({
        country: "Îles Caïmans",
        countryEn: 'Cayman Islands',
				code: 1345
			});

			PhoneCode.insert({
        country: "Îles Cook",
        countryEn: 'Cook Islands',
				code: 682
			});

			PhoneCode.insert({
        country: "Îles Féroé",
        countryEn: 'Faroe Islands',
				code: 298
			});

			PhoneCode.insert({
        country: "Îles Mariannes du Nord",
        countryEn: 'Northern Mariana Islands',
				code: 1670
			});

			PhoneCode.insert({
        country: "Îles Marshall",
        countryEn: 'Marshall Islands',
				code: 692
			});

			PhoneCode.insert({
        country: "Îles Turks-et-Caïcos",
        countryEn: 'Islands Turks-et-Caïcos',
				code: 1649
			});

			PhoneCode.insert({
        country: "Îles Vierges britanniques",
        countryEn: 'British Virgin Islands',
				code: 1284
			});

			PhoneCode.insert({
        country: "Îles Vierges des États-Unis",
        countryEn: 'Virgin Islands of the United States',
				code: 1340
			});
		},
	});
});
