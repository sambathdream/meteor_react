Meteor.startup(() => {
    Meteor.methods({
        fixtures_schools() {
            if (!Meteor.fixtures)
                throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
            Schools.remove({});
            let tab = Assets.getText('schools.csv').split(",");
            let objTab = [];
            for (let i = 0; i < tab.length; i++) {
                objTab.push({name: tab[i]});
            }
            Schools.insert({schools: objTab});
        }
});
});
