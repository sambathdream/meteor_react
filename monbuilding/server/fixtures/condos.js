Meteor.startup(function() {
   Meteor.methods({
     fixtures_condos() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Condos.remove({});
      DefaultCondo.remove({});

         let user2 = Meteor.users.findOne({"emails.0.address": "gestionnaire@gmail.com"});

       console.log("-- Condos");

      Condos.insert({
         "createdAt" : new Date(),
         "name" : "Les Tuileries",
         "buildings" : [],
         "info" : {
           "id": "111",
           "address" : "36 quai des Orfèvres",
           "city" : "Paris",
           "code" : "75001",
           "placeId" : "ChIJcVZ1B-Bx5kcRcbkJduQ3f6g",
           "lat" : "48.855783",
           "lng" : "2.344510"
         },
         "modules" : [],
           "contacts" : {
               "Facturation / Comptabilité" : {
                   "Montant des charges de la copropriété": [],
                   "Impayé, recouvrement, contentieux": [],
                   "Paiement des charges": [],
                   "Régularisation des charges": [],
                   "Autre" : []
               },
               "Autre" : {
                   "Appels de fonds" : [],
                   "Assemblée générale" : [],
                   "Badge" : [],
                   "Carnet d'entretien" : [],
                   "Contrat de syndic" : [],
                   "Devis travaux" : [],
                   "Diagnostic" : [],
                   "Juridique" : [],
                   "Règlement de copropriété" : [],
                   "Vente du logement" : [],
                   "Voisinage" : [],
                   "Autre2" : [],
                   "Incidents" : [],
                   "Trombi": [],
                   "validEntry": [],
               },
               "Defaut": user2._id
           },
           "settings": {
             "condoType": "etudiante",
             "conseilSyndical": false,
               "options" : {
                   "couronne": false,
                   "forumSyndic": false,
                   "messengerSyndic": false,
                   "forum": true,
                   "classifieds": true,
                   "reservations": true,
                   "map": true,
                   "manual": true,
                   "trombi": true,
                   "messenger": true,
                   "incidents": true,
                   "informations": true,
                   "messengerResident": true,
                   "messengerGestionnaire": true,
                   "messengerGardien": true,
                   "EDL": true,
                   "conciergerie": true,
                   "gestionColis": true,
                   "prestaExte": true,
                   "trombiAll": true,
                   "trombiKeeper": false,
                   "trombiCS": false,
               }
           }
       });
       Condos.insert({
         "createdAt" : new Date(),
         "name" : "Résidence de Grenelle",
         "buildings" : [],
         "info" : {
           "id": "222",
           "address" : "168 rue de grenelle",
           "city" : "Paris",
           "code" : "75007",
           "placeId" : "ChIJ-3WtYNhv5kcRf4GV0oVbBuo",
           "lat" : "48.8581146",
           "lng" : "2.3084221"
         },
         "modules" : [],
           "contacts" : {
               "Facturation / Comptabilité" : {
                   "Montant des charges de la copropriété": [],
                   "Impayé, recouvrement, contentieux": [],
                   "Paiement des charges": [],
                   "Régularisation des charges": [],
                   "Autre" : []
               },
               "Autre" : {
                   "Appels de fonds" : [],
                   "Assemblée générale" : [],
                   "Badge" : [],
                   "Carnet d'entretien" : [],
                   "Contrat de syndic" : [],
                   "Devis travaux" : [],
                   "Diagnostic" : [],
                   "Juridique" : [],
                   "Règlement de copropriété" : [],
                   "Vente du logement" : [],
                   "Voisinage" : [],
                   "Autre2" : [],
                   "Incidents" : [],
                   "Trombi": [],
                   "validEntry": [],
               },
               "Defaut" : user2._id
           },
           "settings": {
             "condoType": "copro",
             "conseilSyndical": true,
               "options" : {
                   "couronne": true,
                   "forumSyndic": true,
                   "messengerSyndic": true,
                   "forum": true,
                   "classifieds": true,
                   "reservations": true,
                   "map": true,
                   "manual": true,
                   "trombi": true,
                   "messenger": true,
                   "incidents": true,
                   "informations": true,
                   "messengerResident": true,
                   "messengerGestionnaire": true,
                   "messengerGardien": true,
                   "EDL": true,
                   "conciergerie": true,
                   "gestionColis": true,
                   "prestaExte": true,
                   "trombiAll": true,
                   "trombiKeeper": false,
                   "trombiCS": false,
               }
           }
       });

       DefaultCondo.insert({
        "default":
       [
        {
            "name" : "incident",
            "title" : "Incident",
            "slug" : "incident",
            "defaultDiv" : true,
            "boards" : [ 
                "resident", 
                "gestionnaire"
            ],
            "data" : {}
        }, 
        {
            "name" : "forum",
            "title" : "Forum",
            "slug" : "forum",
            "defaultDiv" : false,
            "boards" : [ 
                "resident"
            ],
            "data" : {
                "forumId" : ""
            }
        }, 
        {
            "name" : "classifieds",
            "title" : "Petites annonces",
            "slug" : "annonces",
            "defaultDiv" : false,
            "boards" : [ 
                "resident"
            ],
            "data" : {
                "classifiedsId" : ""
            }
        }, 
        {
            "name" : "actuality",
            "title" : "Information",
            "slug" : "information",
            "defaultDiv" : false,
            "boards" : [ 
                "resident", 
                "gestionnaire"
            ],
            "data" : {
                "actualityId" : ""
            }
        }, 
        {
            "name" : "messagerie",
            "title" : "Messagerie",
            "slug" : "messagerie",
            "defaultDiv" : true,
            "boards" : [ 
                "resident", 
                "gestionnaire"
            ],
            "data" : {
                "messagerieId" : ""
            }
        }, 
        {
            "name" : "trombinoscope",
            "title" : "Trombinoscope",
            "slug" : "trombinoscope",
            "defaultDiv" : false,
            "boards" : [ 
                "resident"
            ],
            "data" : {}
        }, 
        {
            "name" : "reservation",
            "title" : "Réservation",
            "slug" : "reservation",
            "defaultDiv" : true,
            "boards" : [ 
                "resident"
            ],
            "data" : {}
        }
      ]});

       console.log("-- 2 Condos added, 1 default condo added");
     }
   });
});
