Messageries = new Mongo.Collection('messageries');

Meteor.startup(function() {
   Meteor.methods({
     fixtures_module_messageries() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Messageries.remove({});

      console.log("-- Module Messagerie");

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      let messagerie1 = Messageries.insert({});
      let messagerie2 = Messageries.insert({});

      console.log("-- 2 messagerie created");

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "messagerie",
            title: "Messagerie",
            slug: "messagerie",
            defaultDiv: true,
            boards: ["resident", "gestionnaire"],
            data: {
              messagerieId: messagerie1
            }
        }}});

        Condos.update(condo2._id,
          {$push: {modules: {
              name: "messagerie",
              title: "Messagerie",
              slug: "messagerie",
              defaultDiv: true,
              boards: ["resident", "gestionnaire"],
              data: {
                messagerieId: messagerie2
              }
          }}});

      console.log("-- 2 messageries modules added (on 2 condos)");
    },
  });
});
