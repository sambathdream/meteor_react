Meteor.startup(function() {
   Meteor.methods({
     fixtures_buildings() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Buildings.remove({});

      console.log("-- Buildings");

       let condo1 = Condos.findOne({name: "Les Tuileries"});
       let condo2 = Condos.findOne({name: "Résidence de Grenelle"});

       let building1 = Buildings.insert({
         "createdAt" : new Date(),
         "condoId" : condo1._id,
         "name": "Building",
         "info" : {
           "address" : "36 quai des Orfèvres",
           "city" : "Paris",
           "code" : "75001",
           "placeId" : "ChIJcVZ1B-Bx5kcRcbkJduQ3f6g",
           "lat" : "48.855783",
           "lng" : "2.344510"
         },
         "modules" : [],
         "details" : {
           "constructDate" : "1999",
           "nb_housing" : "44",
           "nb_lvl" : "4",
           "parking" : {"nb": "30", "details" : "Deux sous-sol"},
           "nb_car_box" : "10",
           "nb_cellar" : "30",
           "nb_attic" : "5",
           "elevator" : true,
           "vmc" : false,
           "cleaning_company" : false,
           "balcon" : {"is": false, "details" : ""},
           "rubbish_chute" : {"is": true, "details" : ""},
           "green_area" : {"is": false, "details" : ""},
           "security_company" : {"is": true, "details" : ""},
           "elevator_car" : {"is": false, "details" : ""},
           "antenna" : {"is": true, "details" : ""},
           "water_treatment" : {"is": true, "details" : ""},
           "remote_monitoring" : {"is": true, "details" : ""},
           "interphone" : {"is": true, "details" : ""},
           "tv_cable" : {"is": true, "details" : ""},
           "generator" : {"is": true, "details" : ""},
           "digicode" : {"is": true, "details" : ""},
           "swimming_pool" : {"is": false, "details" : ""},
           "automatic_gate" : {"is": true, "details" : ""},
           "video_phone" : {"is": true, "details" : ""},
           "air_conditioner" : {"is": true, "details" : ""},
           "lifts" : {"is": false, "details" : ""},
           "tennis" : {"is": false, "details" : ""},
           "heating" : "individual",
           "hot_water" : "collective",
           "hot_water_counter" : {"is": true, "details" : ""},
           "heating_counter" : {"is": true, "details" : ""},
           "individual_counter" : {"is": true, "details" : ""},
           "bicycle_local" : {"is": true, "details" : ""},
           "nb_service_room" : "3"
         },
         "contents" : {
           "professionnal_in" : ["Un médecin généraliste", "Un dentiste"]
         },
         "caretaker" : {
           "type" : "?",
           "userId" : "?",
           "timetable" : ["?"],
           "phone" : "0605040605"
         }
       });

       let building2 = Buildings.insert({
         "createdAt" : new Date(),
         "condoId" : condo2._id,
         "name": "Bâtiment A",
         "info" : {
           "address" : "168 rue de grennelle",
           "city" : "Paris",
           "code" : "75007",
           "placeId" : "ChIJLVrJYNhv5kcRuVCIpz5pVXY",
           "lat" : "48.8581146",
           "lng" : "2.3084221"
         },
         "modules" : [],
         "details" : {
           "constructDate" : "1975",
           "nb_housing" : "103",
           "nb_lvl" : "7",
           "parking" : {"nb": "0", "details" : ""},
           "nb_car_box" : "0",
           "nb_cellar" : "21",
           "nb_attic" : "7",
           "elevator" : true,
           "vmc" : true,
           "cleaning_company" : true,
           "balcon" : {"is": true, "details" : "Premier étage uniquement"},
           "rubbish_chute" : {"is": false, "details" : ""},
           "green_area" : {"is": true, "details" : "Jardin commune avec fontaine"},
           "security_company" : {"is": false, "details" : ""},
           "elevator_car" : {"is": false, "details" : ""},
           "antenna" : {"is": true, "details" : ""},
           "water_treatment" : {"is": false, "details" : ""},
           "remote_monitoring" : {"is": false, "details" : ""},
           "interphone" : {"is": false, "details" : ""},
           "tv_cable" : {"is": true, "details" : ""},
           "generator" : {"is": false, "details" : ""},
           "digicode" : {"is": true, "details" : ""},
           "swimming_pool" : {"is": true, "details" : ""},
           "automatic_gate" : {"is": false, "details" : ""},
           "video_phone" : {"is": false, "details" : ""},
           "air_conditioner" : {"is": false, "details" : ""},
           "lifts" : {"is": false, "details" : ""},
           "tennis" : {"is": false, "details" : ""},
           "heating" : "individual",
           "hot_water" : "collective",
           "hot_water_counter" : {"is": false, "details" : ""},
           "heating_counter" : {"is": false, "details" : ""},
           "individual_counter" : {"is": false, "details" : ""},
           "bicycle_local" : {"is": false, "details" : ""},
           "nb_service_room" : "3"
         },
         "contents" : {
           "professionnal_in" : ["Un cabinet de notaire"]
         },
         "caretaker" : {
           "type" : "?",
           "userId" : "?",
           "timetable" : ["?"],
           "phone" : "0605040605"
         }
       });

       let building3 = Buildings.insert({
         "createdAt" : new Date(),
         "condoId" : condo2._id,
         "name": "Bâtiment B",
         "info" : {
           "address" : "168 rue de grennelle",
           "city" : "Paris",
           "code" : "75007",
           "placeId" : "ChIJLVrJYNhv5kcRuVCIpz5pVXY",
           "lat" : "48.8581146",
           "lng" : "2.3084221"
         },
         "modules" : [],
         "details" : {
           "constructDate" : "1975",
           "nb_housing" : "97",
           "nb_lvl" : "7",
           "parking" : {"nb": "0", "details" : ""},
           "nb_car_box" : "0",
           "nb_cellar" : "12",
           "nb_attic" : "7",
           "elevator" : true,
           "vmc" : true,
           "cleaning_company" : true,
           "balcon" : {"is": true, "details" : "Premier étage uniquement"},
           "rubbish_chute" : {"is": false, "details" : ""},
           "green_area" : {"is": true, "details" : "Jardin commune avec fontaine"},
           "security_company" : {"is": false, "details" : ""},
           "elevator_car" : {"is": false, "details" : ""},
           "antenna" : {"is": true, "details" : ""},
           "water_treatment" : {"is": false, "details" : ""},
           "remote_monitoring" : {"is": false, "details" : ""},
           "interphone" : {"is": false, "details" : ""},
           "tv_cable" : {"is": true, "details" : ""},
           "generator" : {"is": false, "details" : ""},
           "digicode" : {"is": true, "details" : ""},
           "swimming_pool" : {"is": true, "details" : ""},
           "automatic_gate" : {"is": false, "details" : ""},
           "video_phone" : {"is": false, "details" : ""},
           "air_conditioner" : {"is": false, "details" : ""},
           "lifts" : {"is": false, "details" : ""},
           "tennis" : {"is": false, "details" : ""},
           "heating" : "individual",
           "hot_water" : "collective",
           "hot_water_counter" : {"is": false, "details" : ""},
           "heating_counter" : {"is": false, "details" : ""},
           "individual_counter" : {"is": false, "details" : ""},
           "bicycle_local" : {"is": false, "details" : ""},
           "nb_service_room" : "3"
         },
         "contents" : {
           "professionnal_in" : ["Entreprise monbuilding.com"]
         },
         "caretaker" : {
           "type" : "?",
           "userId" : "?",
           "timetable" : ["?"],
           "phone" : "0605040605"
         }
       });

       Condos.update(condo1._id, {$push: {"buildings": building1}});
       Condos.update(condo2._id, {$push: {"buildings": building2}});
       Condos.update(condo2._id, {$push: {"buildings": building3}});

       console.log("-- 3 Buildings added in 2 condos");
     }
   });
});
