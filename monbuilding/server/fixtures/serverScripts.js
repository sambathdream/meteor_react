Meteor.startup(function() {
  Meteor.methods({
    fixtures_ServerScripts() {
      if (!Meteor.fixtures) {
        throw new Meteor.Error(301, "Access Denied", "This methods is not allowed")
      }
      console.log("-- ServerScriptCreateCondosModulesOptions");
      Meteor.call('ServerScriptCreateCondosModulesOptions')

      console.log("-- ServerScriptCreateDefaultRoles");
      Meteor.call('ServerScriptCreateDefaultRoles')

      console.log("-- ServerScriptApplyDefaultRolesToUsersWithoutRole");
      Meteor.call('ServerScriptApplyDefaultRolesToUsersWithoutRole')

      console.log("-- ServerScriptCreateUsersRightsForAllUsers");
      Meteor.call('ServerScriptCreateUsersRightsForAllUsers')

      console.log("-- ServerScriptSetTypoToOption");
      Meteor.call('ServerScriptSetTypoToOption')

      console.log("-- ServerScriptTransformPhone");
      Meteor.call('ServerScriptTransformPhone')

      console.log("-- ServerScriptSetOldContactToNewContactForCondo");
      Meteor.call('ServerScriptSetOldContactToNewContactForCondo')

      // console.log("-- ServerScriptAddResourcesOption");
      // Meteor.call('ServerScriptAddResourcesOption')

      console.log("-- ServerScriptUpdateOldPostToNew");
      Meteor.call('ServerScriptUpdateOldPostToNew')

      console.log("-- ServerScriptMigrateToNewReservation");
      Meteor.call('ServerScriptMigrateToNewReservation')

      console.log("-- Merge Condo Collections");
      Meteor.call("ServerScriptMergePerCondoCollections")
    }
  })
})
