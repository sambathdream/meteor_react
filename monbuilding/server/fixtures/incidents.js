Meteor.startup(function() {
   Meteor.methods({
     fixtures_module_incidents() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Incidents.remove({});

      console.log("-- Module Incidents");

      let resident1 = Meteor.users.findOne({"emails.0.address": "resident1@gmail.com"});
      let resident2 = Meteor.users.findOne({"emails.0.address": "resident2@gmail.com"});

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "incident",
            title: "Incident",
            slug: "incident",
            defaultDiv: true,
            boards: ["resident", "gestionnaire"],
            data: {}
        }}});

      Condos.update(condo2._id,
          {$push: {modules: {
              name: "incident",
              title: "Incident",
              slug: "incident",
              defaultDiv: true,
              boards: ["resident", "gestionnaire"],
              data: {}
          }}});

      console.log("-- Module incident added in 2 Condos");

      let incident1 = Incidents.insert({
        "createdAt": new Date(),
        "lastUpdate": new Date(),
        "condoId": condo1._id,
        "declarer": {
          "firstname": resident1.profile.firstname,
          "lastname": resident1.profile.lastname,
          "phone": resident1.profile.phone,
          "email": resident1.emails[0].address,
          "userId": resident1._id,
          "autoDeclare": false
        },
        "history" : [
          {
            "date": new Date(),
            "userId": resident1._id,
            "name": resident1.profile.lastname,
            "title": "Incident déclaré",
            "details": "",
            "share": {
              "all": {"state": false, "comment": "", "files": []},
              "syndic": {"state": false, "comment": "", "files": []},
              "declarer": {"state": true, "comment": "", "files": []},
              "intern": {"state": false, "comment": "", "files": []},
              "custom": []
            }
          }
        ],
        "info": {
          "priority": 1,
          "type": "Electricité",
          "zone": 1,
          "title": "Minuterie du 5e HS",
          "comment": "Depuis une semaine il n'y a plus de lumière"
        },
        "state": {
          "status": 0,
          "intervention": []
        },
        "view": [],
        "eval": []
      });

      let incident2 = Incidents.insert({
        "createdAt": new Date(),
        "lastUpdate": new Date(),
        "condoId": condo2._id,
        "declarer": {
          "firstname": resident2.profile.firstname,
          "lastname": resident2.profile.lastname,
          "phone": resident2.profile.phone,
          "email": resident2.emails[0].address,
          "userId": resident2._id,
          "autoDeclare": false,
        },
        "history" : [
          {
            "date": new Date(),
            "userId": resident2._id,
            "name": resident2.profile.lastname,
            "title": "Incident déclaré",
            "details": "",
            "share": {
              "all": {"state": false, "comment": "", "files": []},
              "syndic": {"state": false, "comment": "", "files": []},
              "declarer": {"state": true, "comment": "", "files": []},
              "intern": {"state": false, "comment": "", "files": []},
              "custom": []
            }
          }
        ],
        "info": {
          "priority": 0,
          "type": "Eau",
          "zone": 0,
          "title": "Dégats des eaux sur 3 étages",
          "comment": "Ma baignoire fuit et a endomagé les deux étages en dessous de chez moi"
        },
        "state": {
          "status": 0,
          "intervention": []
        },
        "view": [],
        "eval": []
      });

      console.log("-- 2 incident added (on 2 condos)");
    },
  });
});
