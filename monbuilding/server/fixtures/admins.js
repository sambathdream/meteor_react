Admin = new Mongo.Collection('admin');

Meteor.startup(function() {
   Meteor.methods({
     fixtures_admins() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Admin.remove({});

      console.log("-- Admins");

      let user = Meteor.users.findOne({"emails.0.address": "admin@gmail.com"});

      let admin = Admin.insert({
        "userId": user._id,
      });

      Meteor.users.update(user._id,
        {$set: {"identities.adminId": admin}}
      );

      console.log("-- 1 identitites Admin added");
    },
  });
});
