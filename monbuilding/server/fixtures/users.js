Meteor.startup(function() {
   Meteor.methods({
     fixtures_users() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Meteor.users.remove({});

      console.log("-- Users");

      Accounts.createUser({
        email: 'resident1@gmail.com',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
          lastname: 'Abeille',
          firstname: 'Rodolphe',
          role: 'Resident',
          tel: '0668394032',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'alex@gmail.com',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
          lastname: 'Dana',
          firstname: 'Alex',
          role: 'Resident',
          tel: '0668394032',
          state: true,
          lang: "en"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'resident2@gmail.com',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
            lastname: 'Lopez',
          firstname: 'Amaury',
          role: 'Resident',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'dev.monbuilding@gmail.com',
        password: 'gestionnaire',
        profile: {
            civilite: "Monsieur",
            lastname: 'Dev',
          firstname: 'Building',
          role: 'Gestionnaire',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'monbuilding75@gmail.com',
        password: 'resident',
        profile: {
            civilite: "Madame",
            lastname: 'LugassyResidente',
          firstname: 'Eliane',
          role: 'Resident',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'gestionnaire@gmail.com',
        password: 'gestionnaire',
        profile: {
            civilite: "Monsieur",
            lastname: 'Lefebvre',
          firstname: 'Damien',
          tel: '0102030405',
          role: 'Gestionnaire',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'elianelugassy@gmail.com',
        password: 'gestionnaire',
        profile: {
            civilite: "Madame",
            lastname: 'Lugassy',
          firstname: 'Eliane',
          tel: '0102030405',
          role: 'Gestionnaire',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'admin@gmail.com',
        password: 'admin',
        profile: {
            civilite: "Madame",
            lastname: 'Lugassy',
          firstname: 'Eliane',
          role: 'Administrateur',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'stopLeSpamDesNotifs@gmail.com',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
            lastname: 'Rochefort',
          firstname: 'Quentin',
          role: 'Resident',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
         email: 'eliane.lugassy@essec.edu',
         password: 'JULY2017!!',
         profile: {
             civilite: "Madame",
             lastname: 'Lugassy',
             firstname: 'Eliane',
             role: 'Resident',
             tel: '0605040302',
             state: true,
             lang: "fr"
         },
         identitites: {}
      });

      /* NEW */
      Accounts.createUser({
        email: 'vmariot@student.42.fr',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
            lastname: 'Mariot',
          firstname: 'Victor',
          role: 'Resident',
          tel: '',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'harith.jaiel@mail.novancia.fr',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
            lastname: 'Jaiel',
          firstname: 'Harith',
          role: 'Resident',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'fakeAddress1@yahoo.fr',
        password: 'resident',
        profile: {
            civilite: "Monsieur",
            lastname: 'Leleu',
          firstname: 'Alexis',
          role: 'Resident',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });
      Accounts.createUser({
        email: 'kevin.longer11@gmail.com',
        password: 'gestionnaire',
        profile: {
            civilite: "Monsieur",
            lastname: 'Kevin',
          firstname: 'Longer',
          role: 'Gestionnaire',
          tel: '0102030405',
          state: true,
          lang: "fr"
        },
        identitites: {}
      });

      console.log("-- 12 Users added (6 resident, 4 gestionnaire, 1 admin, 1 extern)");
    },
  });
});
