Meteor.startup(function() {
   Meteor.methods({
     fixtures_module_forums() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");
      Forums.remove({});
      Classifieds.remove({});

      console.log("-- Module Forums");

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      let building3 = Buildings.findOne({"name": "Building"});

      let forum1 = Forums.insert({
        unfollowers: [],
      });
      let forum2 = Forums.insert({
        unfollowers: [],
      });
      let forum3 = Forums.insert({
        unfollowers: [],
      });
      let forum4 = Forums.insert({
        unfollowers: [],
      });
      let classifieds1 = Classifieds.insert({
        unfollowers: [],
      });
      let classifieds2 = Classifieds.insert({
        unfollowers: [],
      });

      console.log("-- 5 forums created");

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "forum",
            title: "Forum",
            slug: "forum",
            defaultDiv: false,
            boards: ["resident"],
            data: {
              forumId: forum1
            }
        }}});
         Condos.update(condo1._id,
             {$push: {modules: {
                 name: "classifieds",
                 title: "Petites annonces",
                 slug: "annonces",
                 defaultDiv: false,
                 boards: ["resident"],
                 data: {
                     classifiedsId: classifieds1
                 }
             }}});

    Condos.update(condo2._id,
        {$push: {modules: {
            name: "forum",
            title: "Forum",
            slug: "forum",
            defaultDiv: false,
            boards: ["resident"],
            data: {
              forumId: forum2
            }
        }}});
         Condos.update(condo2._id,
             {$push: {modules: {
                 name: "classifieds",
                 title: "Petites annonces",
                 slug: "annonces",
                 defaultDiv: false,
                 boards: ["resident"],
                 data: {
                     classifiedsId: classifieds2
                 }
             }}});

             Buildings.update(building3._id,
          {$push: {modules: {
              name: "forum",
              title: "Boite à idées",
              slug: "idees",
              defaultDiv: false,
              boards: ["resident"],
              data: {
                forumId: forum3
              }
          }}});

      Buildings.update(building3._id,
          {$push: {modules: {
                  name: "forum",
                  title: "Discussions",
                  slug: "discussions-serieuses",
                  defaultDiv: false,
                  boards: ["resident"],
                  data: {
                    forumId: forum4
                  }
              }}});

      Buildings.update(building3._id,
          {$push: {modules: {
                  name: "classifieds",
                  title: "Petites annonces",
                  slug: "annonces",
                  defaultDiv: false,
                  boards: ["resident"],
                  data: {
                      classifiedsId: classifieds1
                  }
              }}});

      console.log("-- 5 modules forums added (on 2 condos and 1 building)");
    },
  });
});
