Meteor.startup(function() {
   Meteor.methods({
     fixtures_module_trombinoscope() {
       if (!Meteor.fixtures)
         throw new Meteor.Error(301, "Access Denied", "This methods is not allowed");

      console.log("-- Module Trombinoscope");

      let condo1 = Condos.findOne({"name": "Résidence de Grenelle"});
      let condo2 = Condos.findOne({"name": "Les Tuileries"});

      Condos.update(condo1._id,
        {$push: {modules: {
            name: "trombinoscope",
            title: "Trombinoscope",
            slug: "trombinoscope",
            defaultDiv: false,
            boards: ["resident"],
            data: {}
        }}});

      Condos.update(condo2._id,
          {$push: {modules: {
              name: "trombinoscope",
              title: "Trombinoscope",
              slug: "trombinoscope",
              defaultDiv: false,
              boards: ["resident"],
              data: {}
          }}});

      console.log("-- Module trombinoscope added in 2 Condos");

    },
  })
});
