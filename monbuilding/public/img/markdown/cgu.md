Le site Internet www.monbuilding.com, ci-après dénommé « MonBuilding.com
» propose un service, via Internet, de dépôt et de consultation
d’informations sur des immeubles en copropriété ou monopropriété et une
plateforme d’échange au sein de chacun de ces immeubles, et destiné aux
particuliers.

L'accès au site, sa consultation et son utilisation sont subordonnés à
l'acceptation sans réserve des présentes Conditions Générales
d'Utilisation de MonBuilding.com.

Le site est édité par MonBuilding&Co, SAS au capital de 100 euros,
immatriculée au registre du commerce et des sociétés de Paris sous le
numéro 820 950 103. Siège social : 146 boulevard de grenelle, 75015
Paris.

Le Directeur de Publication de MonBuilding.com est Eliane Lugassy.

Pour toute question, vous pouvez nous envoyer un courriel à l’adresse
suivante : bonjour@monbuilding.com.

Monbuilding.com est hébergé par la société OVH sis 2 rue kellermann BP
80157 59053 ROUBAIX CEDEX 1 - France. 

Les présentes conditions sont en vigueur à compter du 31/01/2017.

### Préambule – définitions

Chacun des termes mentionnés ci-dessous aura dans les présentes
Conditions Générales d’Utilisation du Service MonBuilding (ci-après
dénommées les « CGU ») la signification suivante :

**Annonce** : désigne l'ensemble des éléments et données (visuelles,
textuelles, sonores, photographies, dessins), déposé par un Annonceur
sous sa responsabilité éditoriale, et diffusé sur le Site.

**Annonceur** : désigne toute personne physique majeure ou personne
morale établie en France, titulaire d’un Compte Personnel ou d’un Compte
Pro, et ayant déposé et mis en ligne une Information / Annonce via le
Service MonBuilding. Le terme « Annonceur » regroupe dans les CGU les
deux types d’Annonceurs suivant :

-   **Buildimer** : désigne toute personne physique majeure ou personne
    morale résidente et/ou copropriétaire d’un ou plusieurs immeuble(s)
    présent(s) sur le Site, et agissant exclusivement à des
    fins privées. Tout Buildimer doit impérativement être titulaire d’un
    Compte Personnel et y être connecté pour accéder et publier une 
    Annonce sur la plateforme réservée à l’/aux immeuble(s) dans
    lequel/lesquels il est résident et/ou copropriétaire ;

-   **Annonceur Pro** : désigne toute personne physique majeure ou
    personne morale déposant, exclusivement à des fins professionnelles,
    des Informations / Annonces à partir du Site. Tout Annonceur Pro
    doit impérativement être titulaire d’un Compte Pro pour déposer et
    gérer ses Annonces.

**Compte Personnel** : désigne l'espace gratuit, accessible depuis le
Site, que tout Buildimer d’un immeuble présent sur le Site peut se créer
afin de bénéficier du Service MonBuilding (article 3 ci-dessous). Un
Compte Personnel peut regrouper plusieurs immeubles.

**Compte Pro** : désigne l'espace gratuit, accessible depuis le Site,
que tout Annonceur Pro doit se créer, et à partir duquel il peut
notamment diffuser, gérer, visualiser ses Annonces et effectuer ses
commandes (article 3 ci-dessous).

**Fiche immeuble ** : désigne la page internet décrivant la structure et les équipements de chaque immeuble du Site.

**Information **: désigne l'ensemble des éléments et données (visuelles, textuelles, sonores, photographies, dessins), déposé par un Utilisateur sous sa responsabilité éditoriale, sur la Fiche immeuble, et diffusé sur le Site – à l’exception des éléments et données (visuelles, textuelles, sonores, photographies, dessins) à caractère immobilier ou publicitaire qui sont considérés comme une Annonce.

**MonBuilding&Co** : désigne la société qui édite et exploite le Site -
MonBuilding&Co, SAS au capital de 100 euros, immatriculée au registre du
commerce et des sociétés de Paris sous le numéro 820 950 103, dont le
siège social est situé 146 boulevard de grenelle Paris.

**Options Payantes** : options disponibles pour les Annonces immobilières (type vente, (co)location, viager) présentes sur la Fiche immeuble. Ces Options sont : l’ajout d’un logo urgent, le pack photos supplémentaires.

**Service MonBuilding** : désigne les services MonBuilding mis à la disposition des Utilisateurs et des Annonceurs sur le Site tels que décrit à l’article 3 des présentes CGU.

**Services Payants** : renvoie aux prestations payantes pour l’Annonceur Pro, souscrites depuis son Compte Pro. Ces prestations sont le dépôt d’Annonces immobilières et publicitaires au niveau de la Fiche immeuble. 

**Site** : désigne le site internet accessible principalement depuis
l’URL [www.monbuilding.com](http://www.monbuilding.com) et permettant
aux Utilisateurs et Annonceurs d’accéder via internet au Service
MonBuilding décrit à l’article 3 des présentes CGU.

**Utilisateur** : désigne tout visiteur ayant accès au Service
MonBuilding via le Site et consultant le Service MonBuilding.

### 1. Objet

Les CGU ont pour objet de déterminer les conditions d'utilisation du
Service MonBuilding mis à disposition des Utilisateurs et des Annonceurs
via le Site. Les conditions de souscription aux Services Payants et
Options Payantes sont fixées dans les Conditions Générales de Vente.

### 2. Acceptation

Tout Utilisateur – Annonceur déclare en accédant et utilisant le Service
MonBuilding depuis le Site, avoir pris connaissance des présentes
Conditions Générales d’Utilisation et les accepter expressément sans
réserve et/ou modification de quelque nature que ce soit. Les présentes
CGU sont donc pleinement opposables aux Utilisateurs – Annonceurs.

### 3. Description du service

Le Service MonBuilding proposé aux Utilisateurs et aux Annonceurs varie
en fonction de la qualité de « Buildimer » ou de « Pro » de l’Annonceur
:

* *3.1. Fonctionnalités accessibles aux Utilisateurs et Annonceurs*

	- La consultation des Fiches immeuble présentes sur le Site 
	- La possibilité de contacter le Site pour l’alimenter avec un immeuble qui les intéresse
	- La possibilité de signaler une erreur ou de compléter la fiche immeuble en contactant le Site
  - La possibilité de demander à recevoir une notification en cas de mise en ligne d’un immeuble qui les intéresse
  - La consultation du blog MonBuilding

* *3.2. Fonctionnalités accessibles aux Buildimers connectés à leur Compte Personnel*

	-   La possibilité de publier une Annonce sur la plateforme réservée à leur(s) immeuble(s) de rattachement :
		-   Signaler / connaitre l’existence d’un incident dans l’espace « Incidents » et plus largement de contacter le gestionnaire d’immeuble via un formulaire, si cette option a été souscrite par le gestionnaire de l’immeuble concerné
		- Lancer un sujet sur le forum, partager des recommandations ou une idée travaux et/ou participer aux échanges sur ces thèmes 
		- Contacter le Conseil syndical s’il s’agit d’un immeuble en copropriété
		- Déposer une petite annonce 

	-   La possibilité de supprimer / modifier une Information / son Annonce
	
	- La possibilité de solliciter le Site pour une mise en relation avec un professionnel
	
	- Le suivi (facultatif) via l’envoi d’un courriel des publications liées à son/ses immeuble(s) de rattachement
	- L’accès à l’ « Espace membre » :
		-   La gestion des informations personnelles renseignées dans le Compte Personnel, à l’exception du nom/prénom. Tout changement ou ajout d’immeuble de rattachement engendre la fourniture d’un nouveau justificatif de domicile de moins de six (6) mois
		-   La gestion des notifications
		-   La suppression du Compte Personnel
		
* *3.3. Fonctionnalités accessibles aux administrateurs connectés à leur Compte Personnel*	

  Il existe deux types d’administrateurs :

  * 3.3.1. Membres du Conseil syndical ou propriétaire de l’immeuble

    * 3.3.1.1. Membres du Conseil syndical

      Les membres du Conseil syndical qui ont demandés à avoir le statut d’administrateur dans leur(s) immeuble(s) peuvent :
      -	Publier un évènement connu à l’avance qui sera visible par tous les Buildimers inscrits 
      -	Publier des informations générales visibles par tous les Buildimers inscrits
      -	Accéder à la liste des Buildimers inscrits sur le Site
      -	Faire partie du Forum Conseil syndical et recevoir toutes les demandes effectuées par les Buildimers inscrits au Conseil syndical
      -	Supprimer un post contraire aux règles de diffusion du Site 

    * 3.3.1.2. Propriétaire de l’immeuble

      Le propriétaire de l’immeuble qui a demandé à avoir le statut d’administrateur peut :
      -	Publier un évènement connu à l’avance qui sera visible par tous les Buildimers inscrits 
      -	Publier des informations générales visibles par tous les Buildimers inscrits
      -	Accéder à la liste des Buildimers inscrits sur le Site
      -	Supprimer un post contraire aux règles de diffusion du Site 

    * 3.3.2. Gardien(s) d’immeuble

      Le ou les gardiens de l’immeuble peuvent demander à être administrateur(s), ce qui leur permet de :
      -	Publier un évènement connu à l’avance qui sera visible par tous les Buildimers inscrits 
      -	Publier des informations générales visibles par tous les Buildimers inscrits
      -	Accéder à la liste des Buildimers inscrits sur le Site

* *3.4. Fonctionnalités accessibles aux Annonceurs Pro connectés à leur Compte Pro*

	Les Services Payants suivants sont à la disposition des Annonceurs Pro :

	-  Le dépôt d’Annonce immobilière (type référence de transactions et gérances locatives) sur la Fiche immeuble en s’assurant que cette Annonce a pour objet une opération réalisée au maximum dans les deux (2) ans précédant le dépôt et figure uniquement sur la plateforme de l’immeuble dans lequel se situe le bien de l’Annonce

	-   Le dépôt d’Annonce immobilière (type vente, (co)location, viager) sur la Fiche immeuble en s’assurant que cette Annonce, dans le cas d’un bien disponible, n’a pas déjà été publiée par un autre Annonceur Pro dans les quatre (4) mois précédant le dépôt, et qu’elle figure uniquement sur la plateforme de l’immeuble dans lequel se situe le bien de l’Annonce

	-   Le dépôt d’Annonce dans les espaces « Calendrier », « Informations générales » et « Incidents » par les Annonceurs Pro ayant la qualité de gestionnaire d’immeuble, dans le cadre du contrat de prestation de service conclu entre l’Annonceur Pro et MonBuilding&Co

	-  Le dépôt d’Annonce publicitaire (logo et lien vers l’extranet) sur la Fiche immeuble par les Annonceurs Pro ayant la qualité de gestionnaire d’immeuble

	-   La gestion d’Annonce via le tableau de bord du Compte Pro :

    	-   La souscription à des Services Payants par carte bancaire

    	-   La souscription à des Options Payantes par carte bancaire

    	-   La prolongation d’Annonces immobilières (type vente, (co)location, viager) à l’issue de la durée maximale de quatre (4) mois

    	-   La suppression d’Annonces

    	-   La résiliation des Services Payants mensuels (i.e. Annonces immobilières type référence de transactions / gérances locatives et publicitaires)

	-   L’accès et la gestion du Compte Pro :

    	-   La gestion (actualisation, modification, etc), à tout moment,
        des informations personnelles renseignées lors de la création du
        Compte Pro

    	-   La suppression du Compte Pro

### 4. Contenu du Site

Le Site a pour objet de présenter le Service MonBuilding, tels que
décrits dans les pages du Site.

MonBuilding&Co s'efforce de vérifier et valider les Informations /
Annonces qu'il propose à la consultation sur le Site.

Toutefois, MonBuilding&Co ne prend aucune obligation quelconque liée à
l'exactitude des Informations / Annonces, ou relative aux utilisations
qui seraient effectuées desdites Informations / Annonces. MonBuilding&Co
n'assure donc aucune garantie, expresse ou tacite, concernant le contenu
du Site.

MonBuilding&Co se réserve le droit d'ajouter, de supprimer ou de
modifier tout ou partie des espaces du Site qu'elle édite ou des
Services qu'elle propose, sans préjudice de l'application d'éventuelles
conditions antérieurement conclues.

MonBuilding&Co se réserve le droit de les mettre à jour, de les modifier
ou de les supprimer à tout moment et sans préavis.

### 5. Accès et sécurité

La souscription, l'accès ou l'utilisation de certains Services
MonBuilding, et notamment des espaces membres, peut nécessiter
l'ouverture d'un Compte Personnel ou d’un Compte Pro impliquant la
fourniture d'un identifiant et le choix d'un mot de passe.

Le mot de passe est personnel et confidentiel. L’Annonceur en est seul
responsable. Il s'engage à ne pas le divulguer à des tiers, sous quelque
forme que ce soit, et à prendre toutes les précautions nécessaires pour
éviter que des tiers ne puissent y avoir accès.

L’Annonceur s'engage à avertir sans délai MonBuilding&Co en cas de perte
ou de vol de son mot de passe.

A défaut, et sauf preuve contraire, toute connexion ou transmission
d'ordres ou de données effectuées au moyen du mot de passe sera réputée
provenir de l’Annonceur et sous sa responsabilité exclusive.

L’Annonceur peut à tout moment demander l’annulation de son Compte
Personnel ou de son Compte Pro en adressant un courriel à
inscriptions@monbuilding.com.

### 6. Propriété intellectuelle – Parasitisme – Protection du Site et des bases de données

**6.1.** Tous les droits de propriété intellectuelle (tels que notamment
droits d'auteur, droits voisins, droits des marques, droits des
producteurs de bases de données) portant tant sur la structure que sur
les contenus du Site et notamment les images, sons, vidéos,
photographies, logos, marques, éléments graphiques, textuels, visuels,
outils, logiciels, documents, données, etc. (ci-après désignés dans leur
ensemble " Eléments ") sont réservés. Ces Eléments sont la propriété de
MonBuilding&Co. Ces Eléments sont mis à disposition des Utilisateurs et
des Annonceurs, à titre gracieux, pour la seule utilisation du Service
MonBuilding et dans le cadre d'une utilisation normale de ses
fonctionnalités. Les Utilisateurs et les Annonceurs s'engagent à ne
modifier en aucune manière les Eléments.

Toute utilisation non expressément autorisée des Eléments du Site
entraîne une violation des droits d'auteur et constitue une contrefaçon.
Elle peut aussi entraîner une violation des droits à l'image, droits des
personnes ou de tous autres droits et réglementations en vigueur. Elle
peut donc engager la responsabilité civile et/ou pénale de son auteur.

**6.2.** Il est interdit à tout Utilisateur et Annonceur de copier,
modifier, créer une œuvre dérivée, inverser la conception ou
l'assemblage ou de toute autre manière tenter de trouver le code source,
vendre, attribuer, sous licencier ou transférer de quelque manière que
ce soit tout droit afférent aux Eléments.

Tout Utilisateur et Annonceur du Service MonBuilding s'engagent
notamment à ne pas :

-   utiliser ou interroger le Service MonBuilding pour le compte ou au
    profit d'autrui

-   extraire, à des fins commerciales ou non, tout ou partie des
    Informations / Annonces présentes sur le Service MonBuilding et sur
    le Site

-   intégrer tout ou partie du contenu du Site dans un site tiers, à des
    fins commerciales ou non

-   reproduire sur tout autre support, à des fins commerciales ou non,
    tout ou partie des Informations / Annonces présentes sur le Service
    MonBuilding et sur le Site permettant de reconstituer tout ou partie
    des fichiers d'origine

-   utiliser un robot, notamment d'exploration (spider), une application
    de recherche ou récupération de sites Internet ou tout autre moyen
    permettant de récupérer ou d'indexer tout ou partie du contenu du
    Site, excepté en cas d'autorisation expresse et préalable de
    MonBuilding&Co

Toute reproduction, représentation, publication, transmission,
utilisation, modification ou extraction de tout ou partie des Eléments
et ce de quelque manière que ce soit, faite sans l'autorisation
préalable et écrite de MonBuilding&Co est illicite. Ces actes illicites
engagent la responsabilité de ses auteurs et sont susceptibles
d'entraîner des poursuites judiciaires à leur encontre et notamment pour
contrefaçon.

**6.3.** Les marques et logos MonBuilding et MonBuilding.com ainsi que les
marques et logos des partenaires de MonBuilding&Co sont des marques
déposées. Toute reproduction totale ou partielle de ces marques et/ou
logos sans l'autorisation préalable et écrite de MonBuilding&Co est
interdite.

**6.4.** MonBuilding&Co est producteur des bases de données du Service
MonBuilding. En conséquence, toute extraction et/ou réutilisation de la
ou des bases de données au sens des articles L 342-1 et L 342-2 du code
de la propriété intellectuelle est interdite.

**6.5.** Le Site peut contenir des liens hypertextes redirigeant vers des
contenus émanant de tiers ou vers des sites Internet exploités par des
tiers. Ces liens sont fournis à simple titre d’information.
MonBuilding&Co n'est pas responsable de la qualité ou de l'exactitude de
ces contenus ni de ces sites Internet et ne peut pas non plus être
considéré comme approuvant, publiant ou autorisant ces sites Internet ou
ces contenus. MonBuilding&Co décline toute responsabilité quant aux
dommages pouvant résulter de l'utilisation de ces sites. La décision
d’activer ces liens relève de la pleine et entière responsabilité de
l’Utilisateur.

**6.6.** Aucun lien hypertexte ne peut être créé vers le Service MonBuilding
sans l'accord préalable et exprès de MonBuilding&Co. Si un internaute ou
une personne morale désire créer, à partir de son site, un lien
hypertexte vers le Service MonBuilding et ce quel que soit le support,
il doit préalablement prendre contact avec MonBuilding&Co en lui
adressant un courriel à l'adresse suivante <bonjour@monbuilding.com>.
Tout silence de MonBuilding&Co devra être interprété comme un refus.

**6.7.** Le contenu des Informations / Annonces appartient aux Annonceurs
les ayant déposées, néanmoins, en déposant des Informations / Annonces
sur le Site, l’Annonceur concède :

-   à MonBuilding&Co le droit d’exploitation non exclusif, transférable,
    sous licenciable, à titre gracieux, pour le monde entier sur (i)
    l’ensemble du contenu des Informations / Annonces et notamment sur
    les photographies, textes, vidéos, illustrations, marques, logos,
    titres (ci-après le « Contenu »), au fur et à mesure de leur
    publication sur le Site ainsi (ii) qu’une licence sur l’ensemble des
    droits de propriété intellectuelle afférant au Contenu et notamment
    sur les droits d’auteurs sur les éléments utilisés dans son
    Information / Annonce, tels que les photographies, textes, vidéos,
    dessins, illustrations, éléments sonores, et ce pour toute la durée
    légale de ses droits de propriété intellectuelle et pour le monde
    entier

    Les droits ainsi concédés incluent le droit de reproduire,
    représenter, diffuser, adapter, modifier, réaliser une œuvre dérivée,
    traduire tout ou partie du Contenu par tous procédés, sous quelque
    forme que ce soit et sur tous supports (numérique, imprimé…) connus ou
    inconnus à ce jour, dans le cadre du Service MonBuilding ou en
    relation avec l’activité de MonBuilding&Co, et ce à des fins
    commerciales ou non et notamment publicitaires, ainsi que dans le
    cadre d’une diffusion sur les réseaux sociaux sur lesquels MonBuilding
    est ou sera présent et notamment les pages Facebook, Instagram et
    Twitter de MonBuilding

    En particulier, les photographies des Informations / Annonces pourront
    être reproduites et intégrées dans des formats publicitaires diffusés
    sur le Site, uniquement en lien avec l’Information / Annonce déposée

    L’Annonceur ayant publié une Information / Annonce sur le Site accorde
    son consentement à la reprise de son Information / Annonce et de leur
    Contenu sur les réseaux sociaux, notamment Facebook, Instagram et
    Twitter. Par conséquent, l’Annonceur atteste avoir pris connaissance
    des conditions générales d’utilisation des sites Facebook, Instagram,
    et Twitter et en accepter les termes, particulièrement en matière de
    réutilisation du Contenu et des données personnelles

    Au titre de cette licence, MonBuilding&Co, sans que cela ne crée à sa
    charge une obligation d’agir, est en droit de s’opposer à la
    reproduction et l’exploitation par des tiers non autorisés des
    Informations / Annonces diffusées sur le Site et de leur Contenu

-   aux Utilisateurs, le droit non exclusif d’accéder au Contenu via le
    Service MonBuilding et d’utiliser et de représenter le Contenu dans
    la mesure autorisée par les fonctionnalités du Service MonBuilding,
    et ce pour le monde entier

### 7. Protection, collecte, utilisation et communication des données personnelles & Cookies

* *7.1. Protection des données personnelles*

	Conformément à la loi nº78-17 du 6 janvier 1978, dite " Informatique et
libertés ", MonBuilding&Co a fait l'objet d'une déclaration auprès de la
Commission Nationale de l'Informatique et des Libertés (C.N.I.L) sous le
numéro 1971071.

	Conformément aux articles 38, 39 et 40 de la loi nº78-17 du 6 janvier
1978, tout Utilisateur et Buildimer (agissant exclusivement à des fins
privées et non commerciales) du Service MonBuilding disposent à tout
moment d'un droit d'opposition, d'accès, de rectification, de
suppression ainsi que d'opposition au traitement des données le
concernant.

	L'Utilisateur et le Buildimer peuvent exercer ce droit en contactant
MonBuilding&Co à l’adresse suivante : inscriptions@monbuilding.com

* *7.2. Collecte et utilisation des données personnelles*

	Les données personnelles sont susceptibles d'être utilisées par
MonBuilding&Co aux fins suivantes :

	-  **Collecte des données personnelles**

		Les données personnelles sont collectées dans le cadre :

		-   De la création du Compte Personnel, le Buildimer remplissant un
    formulaire et renseignant, a minima, les informations suivantes :
    nom, prénom, pseudo, courriel, mot de passe, immeuble(s) dans
    lequel/lesquels il est résident et/ou copropriétaire, statut
    (locataire/ propriétaire), et fournissant un justificatif de
    domicile de moins de 6 mois ou de propriété pour chaque immeuble
		
			Il est possible, si le titulaire d’un Compte Personnel le souhaite,
    de compléter l’onglet « Mon profil » dudit Compte en renseignant des
    informations telles que l’étage, le numéro d’appartement, la date de
    présence dans l’/les immeuble(s).

		-  De la création du Compte Pro, l'Annonceur Pro remplit un formulaire et renseigne les informations suivantes : nom d’agence, nom, prénom, adresse postale, courriel, activité professionnelle (agent immobilier, mandataire, gestionnaire d’immeuble)

	- **Utilisation des données personnelles**

		Toutes données personnelles collectées sont susceptibles d'être
utilisées par MonBuilding&Co aux fins suivantes :

		-   les besoins de la gestion des opérations effectuées sur le Site,
    notamment pour :

    		-   la gestion / validation des Informations / Annonces

    		-   la publication et le suivi de l’Information / Annonce

    		-   l’envoi de formulaires de réponses
    		
    		- l’envoi d’information au sujet du Site
    		
    		- l’envoi de propositions commerciales suite à une demande du Buildimer dans le cadre du service de conciergerie

    		-   l’envoi d’enquêtes de satisfaction

		-   la réalisation de statistiques

* *7.3. Communication des données personnelles*

	Conformément à la loi nº78-17 du 6 janvier 1978, MonBuilding&Co s'engage
à conserver toutes les données personnelles recueillies via le service
MonBuilding et à ne les transmettre à aucun tiers sans l’accord
préalable de l’Annonceur.

	Par dérogation, l'Utilisateur et l'Annonceur sont informés que
MonBuilding&Co peut être amenée à communiquer les données personnelles
collectées via le service MonBuilding :

	-   pour se conformer à des obligations légales ou pour obéir à des
    injonctions judiciaires ;

	-   pour protéger et défendre les droits de MonBuilding&Co, notamment
    ses droits de propriété, dans le cadre d’un contentieux la mettant
    en cause

	-   pour faire respecter les présentes CGU

* *7.4. Traitements de données techniques - Cookies*

	MonBuilding&Co souhaite informer l’Utilisateur de manière claire et
transparente sur l'usage des cookies lors de la navigation sur le Site.

	MonBuilding&Co procède en effet à l'enregistrement automatique de
certaines données techniques.

	Lors de la consultation du Site, les données techniques susceptibles
d'être enregistrées, au titre de l'accès ou de l'utilisation du Site
sont l'adresse Internet Protocol (IP) de l'Utilisateur et les
informations relatives à la configuration (type de machine, de
navigateur, etc.) et à la navigation (date, heure, pages consultées,
survenance d'erreurs, etc.) de l'Utilisateur.

	Ces dernières informations peuvent être stockées sur le disque dur du
terminal de l’Utilisateur, par l’intermédiaire de son logiciel de
navigation à l'occasion de la consultation d'un service en ligne, sous
réserve de ses choix, dans un espace dédié du disque. MonBuilding&Co
traitent ces données techniques de façon totalement anonyme, ne les
rattachant à aucune information permettant d'identifier l'Utilisateur
et, ne les transmettent pas à des tiers.

	Les cookies, en fonction de leur catégorie, sont utilisés pour les
finalités suivantes :

	-   **Les cookies fonctionnels** permettent notamment :

    	-   d'adapter la présentation du Site et espaces publicitaires aux
        préférences d'affichage du terminal de l’Utilisateur (langue
        utilisée, résolution d'affichage, système d'exploitation
        utilisé, etc), selon les matériels et les logiciels de
        visualisation ou de lecture que son terminal comporte

    	-   d'améliorer son expérience utilisateur en pré-remplissant
        certains champs de nos formulaires

	-   **Les cookies de statistiques** (ou analytiques) émis permettent de
    mieux connaitre notre Site et d’améliorer nos services en
    établissant des statistiques et volumes de fréquentation et
    d'utilisation (rubriques et contenus visités, parcours...)

	-   **Les cookies publicitaires** servent à afficher des publicités plus
    pertinentes pour l’Utilisateur et ses centres d'intérêts. Ils sont
    aussi utilisés pour limiter le nombre de fois où ce dernier voit une
    publicité et pour aider à mesurer l'efficacité d'une campagne
    publicitaire

	-   **Les cookies émis par des tiers** : lorsque l’Utilisateur visite
    notre Site, certains cookies ne sont pas enregistrés par nous mais
    par des tiers partenaires pour la diffusion de publicité ciblée.

	Il est important de noter que tout paramétrage que l’Utilisateur peut
entreprendre sera susceptible de modifier sa navigation sur Internet et
ses conditions d'accès à certains services nécessitant l'utilisation de
cookies.

	Si l’Utilisateur refuse l'enregistrement de ces derniers dans son
terminal, ou s’il supprime ceux qui y sont enregistrés, il ne pourra
plus bénéficier d'un certain nombre de fonctionnalités qui peuvent être
nécessaires pour naviguer dans certains espaces du Site.

	L’Utilisateur peut faire le choix à tout moment d'exprimer et de
modifier ses souhaits, par les moyens décrits ci-dessous :

-   Les choix offerts par le logiciel de navigation

	L’Utilisateur peut configurer son logiciel de navigation de manière à ce
que des cookies soient enregistrés dans son terminal ou, au contraire,
qu'ils soient rejetés. Certains navigateurs permettent de définir des
règles pour gérer les cookies site par site. Pour la gestion des
cookies, la configuration de chaque navigateur est différente. Elle est
décrite dans le menu d'aide du navigateur, qui permettra de savoir de
quelle manière modifier ses souhaits :

	-   Pour Internet Explorer™ :
    	<http://windows.microsoft.com/fr-FR/windows-vista/Block-or-allow-cookies>

	-   Pour Safari™ :
    	<http://docs.info.apple.com/article.html?path=Safari/3.0/fr/9277.html>

	-   Pour Chrome™ :
	   <http://support.google.com/chrome/bin/answer.py?hl=fr&hlrm=en&answer=95647>

	-   Pour Firefox™ :
    	<http://support.mozilla.org/fr/kb/activer-desactiver-cookies>

	-   Pour Opera™ : <http://help.opera.com/Windows/10.20/fr/cookies.html>


-   **Les choix exprimés en ligne auprès de plateformes
    interprofessionnelles**

	L’Utilisateur peut se connecter au site
http://www.youronlinechoices.com/fr/controler-ses-cookies/. Cette
plateforme partagée par de nombreux professionnels de la publicité sur
Internet offre la possibilité de refuser ou d'accepter les cookies
publicitaires utilisés par ces entreprises.
	
	Cette procédure n'empêchera pas l'affichage de publicités sur les sites
Internet visités. Elle ne bloquera que les technologies qui permettent
d'adapter des publicités aux centres d'intérêts.

### 8. Responsabilité et garanties

* *8.1. Engagements de l’Annonceur*

	L'Annonceur garantit détenir tous les droits (notamment des droits de
propriété intellectuelle) ou avoir obtenu toutes les autorisations
nécessaires à la publication de son Information / Annonce.

	L'Annonceur garantit que l'Information / Annonce ne contrevient à aucune
réglementation en vigueur (notamment relatives à la publicité, à la
concurrence, à la promotion des ventes, à l'utilisation de la langue
française, à l'utilisation de données personnelles, à la prohibition de
la commercialisation de certains biens ou services), ni aucun droit de
tiers (notamment aux droits de propriété intellectuelle et aux droits de
la personnalité) et qu'il ne comporte aucun message diffamatoire ou
dommageable à l'égard de tiers.

	Ainsi, l'Annonceur s'engage notamment à ce que l'Information / Annonce
ne contienne :

	-   aucune information imprécise, fausse, mensongère ou de nature à
    induire en erreur les Utilisateurs / Annonceurs

	-   aucune mention diffamatoire, médisante, calomnieuse ou de nature à
    nuire aux intérêts et/ou à l'image de MonBuilding&Co ou de tout
    tiers

	-   aucun contenu portant atteinte aux droits de propriété
    intellectuelle de tiers, obscène, pédophile, discriminatoire ou
    incitant à la violence / haine raciale, religieuse ou ethnique

	Le Buildimer s’engage à utiliser les rubriques à leur disposition à des
fins non professionnelles. Tous messages publicitaires et de parrainage
sont interdits.

	L’Annonceur Pro :

	-   déclare sur l’honneur l’exactitude et la sincérité des Informations
    / Annonces diffusées sur le Site

	-   déclare que son activité est exercée en conformité avec la
    législation et réglementation afférente à sa profession et s’engage
    expressément à respecter toutes les législations et réglementations
    en vigueur

	-   s’engage à avertir sans délai, MonBuilding en cas de changement
    affectant l’exercice de sa profession (ex : incapacité
    professionnelle, non renouvellement du mandat de gestion, etc)

	-   s’engage à ce que les Annonces immobilières :

    	-   ne soit publiées que sur la plateforme de l’immeuble dans lequel
        se situe le bien de l’Annonce

    	- Type référence de transactions et gérances locatives : aient pour objet des opérations réalisées maximum dans les deux (2) ans précédant le dépôt sur la Fiche immeuble 

    	-   Type vente, (co)location, viager : n’aient pas déjà été publiées, pour des biens disponibles, par un autre Annonceur Pro dans les quatre (4) mois précédant le dépôt, et ne contiennent aucun contenu à caractère promotionnel ou publicitaire en lien avec son activité

	MonBuilding se réserve la possibilité de consulter à tout moment le
registre des mandats de l’Annonceur Pro ainsi que des dossiers de
transactions afin de vérifier l’exactitude des données qui lui sont
transmises et, en cas de refus de l’Annonceur Pro, de supprimer tout ou
partie de ses Annonces.

	Toute Annonce immobilière (type vente, (co)location, viager) d’un Annonceur Pro est diffusée, à compter du jour de son dépôt, sur le Site et ce pour une durée maximale de quatre (4) mois. Passée cette durée initiale de quatre (4) mois, MonBuilding&Co envoie à l'Annonceur Pro un e-mail 10 jours avant l’expiration de son Annonce afin de lui proposer de reconduire son Annonce pour deux (2) mois supplémentaires. Si l'Annonceur Pro ne reconduit pas son Annonce avant la date d’expiration, il ne pourra pas prolonger son Annonce et devra la déposer à nouveau.

	L'Annonceur s'engage à ne proposer que des biens disponibles dont il dispose et qu'il détient ou, dans le cas particulier des produits immobiliers, dont on lui a confié la commercialisation ou des biens dont on lui a confié la recherche. L'Annonceur s'engage, en cas d'indisponibilité du bien, à procéder au retrait de l'Annonce du Service MonBuilding dans les plus brefs délais. 

	Toute Annonce immobilière d’un Annonceur Pro (type référence de transactions et gérances locatives) est diffusée, à compter du jour de son dépôt, sur le Site et ce pour une durée maximale de deux (2) ans, la résiliation de ce Service Payant intervenant automatiquement dès lors que l’opération mentionnée dans l’Annonce a plus de deux (2) ans.

	L'Annonceur Pro reconnaît et accepte que pour des raisons d'ordre
technique, la mise en ligne d'une Annonce sur le Site peut ne pas être
instantanée avec sa validation.

	L'Annonceur déclare connaître l'étendue de diffusion du Site, avoir pris
toutes précautions pour respecter la législation en vigueur des lieux de
réception et décharger MonBuilding&Co de toutes responsabilités à cet
égard.

	Dans ce cadre, l'Annonceur déclare et reconnaît qu'il est seul
responsable du contenu des Informations / Annonces qu'il publie et rend
accessibles aux Utilisateurs, ainsi que de tout document ou information
qu'il transmet aux Utilisateurs.

	L'Annonceur assume l'entière responsabilité éditoriale du contenu des
Informations / Annonces qu'il publie.

	En conséquence, l'Annonceur relève MonBuilding&Co, ses sous-traitants et
fournisseurs, de toutes responsabilités, les garantit contre tout
recours ou action en relation avec l'Information / Annonce qui pourrait
être intenté contre ces derniers par tout tiers, et prendra à sa charge
tous les dommages-intérêts ainsi que les frais et dépens auxquels ils
pourraient être condamnés ou qui seraient prévus à leur encontre par un
accord transactionnel signé par ces derniers avec ce tiers , nonobstant
tout dommages-intérêts dont MonBuilding&Co, ses sous-traitants et
fournisseurs pourraient réclamer à raison des faits dommageables de
l’Annonceur.

	En déposant toute Information / Annonce, chaque Annonceur reconnaît et
accepte que MonBuilding&Co puisse supprimer, ou refuser, à tout moment,
sans indemnité ni droit à remboursement des sommes engagées par
l'Annonceur, une Information / Annonce qui serait contraire notamment à
la loi française, aux règles de diffusion du Service MonBuilding
énoncées notamment dans les CGU et/ou susceptible de porter atteinte aux
droits de tiers.

	Si une Annonce est refusée, avant sa mise en ligne, par MonBuilding&Co,
l'Annonceur en sera informé par email envoyé à l'adresse indiquée lors
de la création du Compte, et le cas échéant les sommes engagées seront
remboursées en recréditant la carte bancaire ayant servi au paiement.

	L'Annonceur s'engage également à ce que son Compte Personnel et/ou son
Compte Pro ne contienne :

	-   aucune information fausse et/ou mensongère

	-   aucune information portant atteinte aux droits d'un tiers

	Dans ce cadre, le titulaire déclare et reconnaît qu'il est seul
responsable des informations renseignées lors de la création de son
Compte Personnel et/ou de son Compte Pro.

	En créant un Compte Personnel et/ou un Compte Pro, chaque titulaire
reconnaît et accepte que MonBuilding&Co puisse supprimer, à tout moment,
sans indemnité ni droit à remboursement des sommes engagées par
l'Annonceur, un compte qui serait contraire notamment à la loi française
et/ou aux règles de diffusion fixées par MonBuilding&Co et notamment
énoncées dans les CGU.

* *8.2. Responsabilité et obligations de MonBuilding&Co*

	En sa qualité d'hébergeur, MonBuilding&Co est soumise à un régime de
responsabilité atténuée prévu aux articles 6.I.2. et suivants de la loi
nº2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique.

	MonBuilding&Co ne saurait donc en aucun cas être tenue responsable du
contenu des Informations / Annonces publiées par les Annonceurs et ne
donne aucune garantie, expresse ou implicite, à cet égard.

	MonBuilding&Co est un tiers aux correspondances et relations entre les
Annonceurs, entre les Annonceurs et les Utilisateurs, et exclut de ce
fait toute responsabilité à cet égard.

* *8.3 Limitation de responsabilités*

	MonBuilding&Co s'engage à mettre en œuvre tous les moyens nécessaires
afin d'assurer au mieux la fourniture du Service MonBuilding aux
Utilisateurs et aux Annonceurs.

	Toutefois, MonBuilding&Co décline toute responsabilité en cas de :

	-   interruptions, de pannes, de modifications et de dysfonctionnement
    du Service MonBuilding quel que soit le support de communication
    utilisé et ce quelles qu'en soient l'origine et la provenance

	-   perte de données ou d'informations stockées par MonBuilding&Co. Il
    incombe aux Annonceurs de prendre toutes précautions nécessaires
    pour conserver les Informations / Annonces qu'ils publient via le
    Service MonBuilding

	-   impossibilité momentanée d'accès au Site en raison de problèmes
    techniques et ce quelles qu'en soient l'origine et la provenance

	-   dommages directs ou indirects causés à l'Utilisateur ou l'Annonceur,
    quelle qu'en soit la nature, résultant du contenu des Informations /
    Annonces et/ou de l'accès, de la gestion, de l'Utilisation, de
    l'exploitation, du dysfonctionnement et/ou de l'interruption du
    Service MonBuilding,

	-   utilisation anormale ou d'une exploitation illicite du Service
    MonBuilding par tout Utilisateur ou Annonceur

	-   attaque ou piratage informatique, privation, suppression ou
    interdiction, temporaire ou définitive, et pour quelque cause que ce
    soit, de l’accès au réseau internet

	La responsabilité de MonBuilding&Co ne pourra être engagée que pour les
dommages directs subis par l’Annonceur, résultant d’un manquement à ses
obligations contractuelles telles que définies aux présentes.
L’Utilisateur – l’Annonceur renonce donc à demander réparation à
MonBuilding&Co à quelque titre que ce soit, de dommages indirects tels
que le manque à gagner, la perte de chance, le préjudice commercial ou
financier, l’augmentation de frais généraux ou les pertes trouvant leur
origine ou étant la conséquence de l’exécution des présentes.

	Tout Utilisateur / Annonceur est alors seul responsable des dommages
causés aux tiers et des conséquences des réclamations ou actions qui
pourraient en découler. L'Utilisateur / Annonceur renonce également à exercer tout
recours contre MonBuilding&Co dans le cas de poursuites diligentées par
un tiers à son encontre du fait de l'Utilisation et/ou de l'exploitation
illicite du Service MonBuilding, en cas de perte par un Utilisateur ou
un Annonceur de son mot de passe ou en cas d'usurpation de son identité.

### 9. Modération des Informations / Annonces

* *9.1 Suppression des Informations / Annonces***

	MonBuilding&Co se réserve le droit de supprimer, sans préavis ni
indemnité ni droit à remboursement, toute Information / Annonce qui ne
serait pas conforme aux règles de diffusion du Service MonBuilding et/ou
qui serait susceptible de porter atteinte aux droits d'un tiers.

* *9.2 Gestion des abus *

	Il est permis à tout Utilisateur - Annonceur de signaler un contenu
abusif:

	-   soit en cliquant sur le lien "signaler un abus" situé en bas de
    chaque page

	-   soit par courriel à l’adresse suivante : <bonjour@monbuilding.com>

	Une fois l’abus constaté, les prochaines Informations / Annonces
publiées par l’Annonceur responsable du contenu abusif seront mises sous
surveillance. Si un deuxième abus est constaté, le Compte de l’Annonceur
sera supprimé du Site.

### 10. Dispositions diverses

MonBuilding&Co se réserve la possibilité de modifier, à tout moment, en
tout ou partie les CGU. Les Utilisateurs et les Annonceurs sont invités
à consulter régulièrement les présentes CGU afin de prendre connaissance
de changements éventuels effectués. L'Utilisation du Site par les
Utilisateurs et les Annonceurs constitue l'acceptation par ces derniers
des modifications apportées aux CGU.

Si une partie des CGU devait s'avérer illégale, invalide ou
inapplicable, pour quelle que raison que ce soit, les dispositions en
question seraient réputées non écrites, sans remettre en cause la
validité des autres dispositions qui continueront de s'appliquer entre
les Utilisateurs - Annonceurs et MonBuilding&Co.

Le site et les présentes CGU sont soumis au droit français.

Toute contestation et/ou difficulté d'interprétation ou d'exécution des
présentes conditions générales relèvera des tribunaux compétents de la
ville de Paris.
