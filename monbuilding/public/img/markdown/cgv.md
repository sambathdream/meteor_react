Le site Internet www.monbuilding.com, ci-après dénommé « MonBuilding.com
» propose un service, via Internet, de dépôt et de consultation
d’informations sur des immeubles en copropriété ou monopropriété et une
plateforme d’échange au sein de chacun de ces immeubles, et destiné aux
particuliers.

Le site est édité par MonBuilding&Co, SAS au capital de 100 euros,
immatriculée au registre du commerce et des sociétés de Paris sous le
numéro 820 950 103. Siège social : 146 boulevard de grenelle, 75015
Paris.

Les présentes conditions sont en vigueur à compter du 01/11/2016.

### Préambule – définitions

Chacun des termes mentionnés ci-dessous aura dans les présentes
Conditions Générales de Vente du Service MonBuilding (ci-après dénommées
les « CGU ») la signification suivante :

**Annonce** : désigne l'ensemble des éléments et données (visuelles,
textuelles, sonores, photographies, dessins), déposé par un Annonceur
Pro sous sa responsabilité éditoriale, et diffusé sur le Site.

**Annonceur** : désigne toute personne physique majeure ou personne
morale établie en France, titulaire d’un Compte Personnel ou d’un Compte
Pro, et ayant déposé et mis en ligne une Information / Annonce via le
Service MonBuilding. Le terme « Annonceur » regroupe dans les CGU les
deux types d’Annonceurs suivant :

-   **Buildimer** : désigne toute personne physique majeure ou personne
    morale résidente et/ou copropriétaire d’un ou plusieurs immeuble(s)
    présent(s) sur le Site, et agissant exclusivement à des
    fins privées. Tout Buildimer doit impérativement être titulaire d’un
    Compte Personnel et y être connecté pour publier une Information /
    Annonce sur la plateforme réservée à l’/aux immeuble(s) dans
    lequel/lesquels il est résident et/ou copropriétaire

-   **Annonceur Pro** : désigne toute personne physique majeure ou
    personne morale déposant, exclusivement à des fins professionnelles,
    des Informations / Annonces à partir du Site. Tout Annonceur Pro
    doit impérativement être titulaire d’un Compte Pro pour déposer et
    gérer sa ou ses Informations / Annonces. Il existe deux catégories
    d’Annonceur Pro : les gestionnaires d’immeuble et, les autres
    professionnels de l’immobilier (agences immobilières, réseaux de
    mandataires, etc)

**Commande** : tout achat passé par un Annonceur Pro depuis son Compte
Pro aux fins d’insertion d’Annonces, et ce dans les conditions fixées à
l’article 4 des présentes CGV.

**Compte Pro** : désigne l'espace gratuit, accessible depuis le Site,
que tout Annonceur Pro doit se créer, et à partir duquel il peut
notamment diffuser, gérer, visualiser ses Annonces et effectuer ses
Commandes (article 4 des présentes CGV).

**MonBuilding&Co** : désigne la société qui édite et exploite le Site -
MonBuilding&Co, SAS au capital de 100 euros, immatriculée au registre du
commerce et des sociétés de Paris sous le numéro 820 950 103, dont le
siège social est situé 146 boulevard de grenelle Paris.

**Options Payantes** : options disponibles pour les Annonces
immobilières de la rubrique « Petites annonces ». Ces Options sont :
l’ajout d’un logo urgent, le pack photos supplémentaires.

**Pack MonBuilding** : terme regroupant les deux types de Pack auxquels
un Annonceur Pro peut souscrire s’il souhaite publier des Annonces dans
la rubrique « Information » - chaque Pack étant indépendant de l’autre :

-   **Pack Références **: désigne un ensemble d’Annonces immobilières. Il
    existe \[4\] catégories de Pack Références en fonction du nombre
    d’Annonces souhaitées sur le Site

-   **Pack Pub** : désigne un ensemble d’Annonces publicitaires. Il
    existe \[6\] catégories de Pack Pub en fonction du nombre d’Annonces
    souhaitées sur le Site

**Service Client** : désigne le service MonBuilding&Co auprès duquel
l'Annonceur Pro peut obtenir toute information complémentaire. Ce
service peut être contacté par e-mail en cliquant sur le lien « nous
contacter » situé sur le Site.

**Service MonBuilding** : désigne les services MonBuilding&Co mis à la
disposition des Utilisateurs et des Annonceurs sur le Site tels que
décrit à l’article 3 des CGU.

**Site** : désigne le site internet accessible principalement depuis
l’URL [www.monbuilding.com](http://www.monbuilding.com) et permettant
aux Utilisateurs et Annonceurs d’accéder via internet au Service
MonBuilding décrit à l’article 3 des CGU.

**Utilisateur** : désigne tout visiteur ayant accès au Service
MonBuilding via le Site et consultant le Service MonBuilding.

### 1. Objet

Les présentes CGV établissent les conditions contractuelles
exclusivement applicables aux Commandes passées par un Annonceur Pro
depuis le Site.

### 2. Acceptation

-	L'Annonceur Pro déclare en souscrivant au Service MonBuilding avoir pris
connaissance des présentes CGV et les accepter expressément, sans
réserve et/ou modification de quelque nature que ce soit. L'Annonceur
Pro renonce ainsi à se prévaloir de ses propres conditions générales
d'achat.

-	Toute condition contraire opposée par l'Annonceur Pro sera donc à
défaut, d'acceptation expresse, inopposable à MonBuilding&Co, quel que
soit le moment où elle aura pu être portée à sa connaissance.

-	Le fait que MonBuilding&Co ne se prévale pas à un moment donné de l'une
quelconque des présentes conditions générales de vente ne peut être
interprété comme valant renonciation à se prévaloir ultérieurement de
l'une quelconque desdites conditions.

### 3. Conditions d’Utilisation du Service MonBuilding par l’Annonceur Pro

***3.1. Règles relatives au Compte Pro***

* *3.1.1. Création et titularité du Compte Pro*

	Tout Annonceur Pro qui souhaite diffuser des Annonces via le Service
MonBuilding doit obligatoirement se créer un « Compte Pro » à partir du
Site en remplissant le formulaire d’inscription mis à sa disposition sur
le Site, et choisir ses identifiants de connexion (courriel et mot de
passe). Le Compte Pro doit comprendre ses coordonnées et informations
bancaires exactes et mises à jour.

	Un Compte Pro peut faire l’objet d’une procédure de vérification par
MonBuilding&Co afin de s’assurer que les informations communiquées par
l’Annonceur Pro sont correctes.

	Un Compte Pro est personnel à l'Annonceur Pro et ne peut être transféré
ou cédé à aucun tiers sans l'accord écrit et préalable de
MonBuilding&Co.

* *3.1.2. Ordres réalisables par l'Annonceur Pro depuis son Compte Pro*

	Le dépôt d’Annonce immobilière (transactions et gérances locatives) dans la 
  rubrique « Information » en s’assurant que cette Annonce a pour objet une 
  opération réalisée au maximum dans les deux (2) ans précédant le dépôt et 
  figure uniquement sur la plateforme de l’immeuble dans lequel se situe le 
  bien de l’Annonce

	Le dépôt d’Annonce immobilière (vente, (co)location, viager) dans la 
  rubrique « Petites annonces » en s’assurant que cette Annonce, dans le cas 
  d’un bien disponible, n’a pas déjà été publiée par un autre Annonceur Pro 
  dans les quatre (4) mois précédant le dépôt, et qu’elle figure uniquement 
  sur la plateforme de l’immeuble dans lequel se situe le bien de l’Annonce

	Le dépôt d’Annonce dans la rubrique « Actualité » et « Incidents » par les 
  Annonceurs Pro ayant la qualité de gestionnaire d’immeuble, dans le cadre du 
  contrat de prestation de service conclu entre l’Annonceur Pro 
  et MonBuilding&Co

	Le dépôt d’Annonce publicitaire (logo et lien vers l’extranet) dans la 
  rubrique « Information » par les Annonceurs Pro ayant la qualité de 
  gestionnaire d’immeuble

	-   La gestion d’Annonce via le tableau de bord du Compte Pro :
		-   La mise en ligne et la suppression d’Annonces

    	-   La souscription, modification et résiliation des Packs
        MonBuilding

    	-   Le paiement et la prolongation d’Annonces immobilières de la
        rubrique « Petites annonces » à l’issue de la durée maximale de
        quatre (4) mois

    	-   La souscription à des Options Payantes lors du dépôt de
        l’Annonce

***3.2. Règles de diffusion des Annonces***

L'Annonceur Pro s'engage à ne diffuser d'Annonces qu'en son nom et pour
son propre compte. Ainsi, sauf accord préalable et exprès de
MonBuilding&Co, l'Annonceur Pro ne peut utiliser le Service MonBuilding
pour diffuser des Annonces au nom et/ ou pour le compte d'un tiers.

A défaut, MonBuilding&Co se réserve le droit de :

-   supprimer toute Annonce diffusée par l'Annonceur Pro au nom et/ou
    pour le compte d'un tiers et ce sans qu'aucun remboursement et/ou
    indemnisation ne puisse lui être réclamé(e) par l'Annonceur Pro

-   supprimer sans préavis le « Compte Pro » et toutes Annonces en cours
    de diffusion et ce sans qu'aucun remboursement et/ou indemnisation
    ne puisse lui être réclamé(e) par l'Annonceur Pro

-   interdire l'utilisation du Service MonBuilding aux fins de diffusion
    d'Annonce et ce sans qu'aucun remboursement et/ou indemnisation ne
    puisse lui être réclamé(e) par l'Annonceur Pro

Sans que cela ne crée à sa charge une obligation de vérifier le contenu,
MonBuilding&Co se réserve le droit d'accepter, de refuser ou de
supprimer, toute Annonce déposée ou modifiée par l'Annonceur Pro.

MonBuilding&Co se réserve le droit de refuser ou supprimer une Annonce
qui contreviendrait aux dispositions des présentes CGV, qui ne serait
pas conforme au présent paragraphe et aux règles de diffusion du Service
MonBuilding, ou dont le contenu serait manifestement contraire aux
dispositions légales et réglementaires en vigueur, telles que détaillées
ci-après à l'article 5 « Engagement et garantie de l'Annonceur Pro ».

L’Annonceur Pro s’engage à ce que l’Annonce immobilière :

-   ne soit publiée que sur la plateforme de l’immeuble dans lequel se
    situe le bien de l’Annonce

-   de la rubrique « Information » : ait pour objet une opération
    réalisée au maximum dans les deux (2) ans précédant le dépôt dans la
    rubrique « Information » 

-   de la rubrique « Petites annonces » : n’ait pas déjà été publiée,
    pour un bien disponible (vente, (co)location, viager), par un autre
    Annonceur Pro dans les quatre (4) mois précédant le dépôt dans la
    rubrique « Petites annonces  », et ne contienne aucun contenu à
    caractère promotionnel ou publicitaire en lien avec son activité

MonBuilding&Co se réserve la possibilité de consulter à tout moment le
registre des mandats de l’Annonceur Pro ainsi que des dossiers de
transactions afin de vérifier l’exactitude des données qui lui sont
transmises et, en cas de refus de l’Annonceur Pro, de supprimer tout ou
partie de ses Annonces.

Toute Annonce immobilière d’un Annonceur Pro dans la rubrique « Petites
annonces » est diffusée, à compter du jour de son dépôt, sur le Site et
ce pour une durée maximale de quatre (4) mois. Passée cette durée
initiale de quatre (4) mois, MonBuilding&Co envoie à l'Annonceur Pro un
e-mail 10 jours avant l’expiration de son Annonce afin de lui proposer
de reconduire son Annonce pour deux (2) mois supplémentaires. Si
l'Annonceur Pro ne reconduit pas son Annonce avant la date d’expiration,
il ne pourra pas prolonger son Annonce et devra la déposer à nouveau.

L'Annonceur Pro s'engage à ne proposer dans les Annonces de la rubrique
« Petites annonces » que des biens disponibles dont on lui a confié la
commercialisation ou des biens dont on lui a confié la recherche.
L'Annonceur s'engage, en cas d'indisponibilité du bien, à procéder au
retrait de l'Annonce du Service MonBuilding dans les plus brefs délais.

Les Annonces de la rubrique « Petites annonces » sont classées par ordre
chronologique, en fonction de la date et de l'heure de leur mise en
ligne. En conséquence, l'Annonceur Pro reconnaît et accepte que la
présence en tête de liste de son Annonce ne soit que provisoire.

Toute Annonce immobilière d’un Annonceur Pro dans la rubrique
« Information » est diffusée, à compter du jour de son dépôt, sur le
Site et ce pour une durée maximale de deux (2) ans, la résiliation de
cette Annonce intervenant automatiquement dès lors que l’opération
mentionnée dans l’Annonce a plus de deux (2) ans.

Les Annonces immobilières de la rubrique « Information » sont classées
par date d’opération, de la plus récente à la plus ancienne, et comme
énoncé ci-dessus ne peuvent être intervenues il y a plus de deux (2)
ans.

Si une Annonce est refusée, avant sa mise en ligne par MonBuilding&Co,
l'Annonceur Pro en sera informé par email envoyé à l'adresse indiquée
lors de la création du Compte Pro, et le cas échéant les sommes engagées
seront remboursées à l'Annonceur Pro en recréditant la carte bancaire
ayant servi au paiement.

Un tel refus ne fait naître au profit de l'Annonceur Pro aucun droit à
indemnité.

### 4. Les Commandes

Le bénéfice de toute Commande est personnel à l'Annonceur Pro qui l'a
effectuée et ne peut être cédé, transféré sans l'accord de
MonBuilding&Co. Aucun remboursement n'est possible après le début
d'exécution de toute commande passée.

L’Annonceur Pro peut passer une Commande directement depuis son Compte
Pro.

L’Annonceur Pro ne peut passer Commande que dans les
rubriques « Information » et « Petites annonces » en respectant les
règles de diffusion énoncées à l’article 3 des présentes CGV.

* *4.1 Commande d’Annonces immobilières dans la rubrique
« Information »*

  L’insertion d’Annonces immobilières par un Annonceur Pro dans la
  rubrique « Information » est payante et est soumise à la souscription
  d’un Pack Références parmi les \[4\] catégories proposées sur le Site.

  Ces Annonces immobilières ont pour objet de faire connaitre aux
  Utilisateurs / Annonceurs les transactions et gestions locatives
  réalisées par l’Annonceur Pro dans l’immeuble. L’opération indiquée dans
  l’Annonce doit avoir été réalisée au plus tard dans les deux (2) années
  précédant la date du dépôt et ne peut figurer que dans la rubrique
  « Information » de la plateforme réservée à l’immeuble dans lequel se
  trouve le bien mentionné dans l’Annonce.

  L’Annonce consiste en la publication, en bas de la page de la rubrique,
  de :

  -   Date de l’opération

  -   Type d’opération (transaction / gestion locative)

  -   Type de bien (studio, 2P, box, etc)

  -   Nom de l’Annonceur Pro

  -   Contact de l’Annonceur Pro

* *4.2 Commande d’Annonces publicitaires dans la rubrique
« Information »*

  L’insertion d’Annonces publicitaires par un Annonceur Pro dans la
  rubrique « Information » est payante et est soumise à la souscription
  d’un Pack Pub parmi les \[6\] catégories proposées sur le Site. Seuls
  les Annonceurs Pro ayant la qualité de gestionnaire d’immeuble peuvent
  passer cette Commande.

  Ces Annonces publicitaires ont pour objet de faire connaitre aux
  Utilisateurs / Annonceurs le nom du gestionnaire de l’immeuble et de
  faciliter l’accès des Buildimers à l’extranet mis en place par ledit
  gestionnaire.

  L’Annonce consiste en la publication, en haut à gauche de la rubrique,
  du :

  -   Logo du gestionnaire d’immeuble

  -   Lien vers son extranet

* *4.3 Commande d’Annonces immobilières dans la rubrique « Petites
annonces »*

  * *4.3.1 Frais d’insertion*

    L’insertion d’une Annonce immobilière par un Annonceur Pro dans la
  rubrique « Petites annonces » est payante.

    Cette Annonce immobilière a pour objet de faire connaitre aux
  Utilisateurs / Annonceurs qu’un bien est disponible (vente,
  (co)location, viager) ou est recherché par l’Annonceur Pro dans
  l’immeuble. Tout contenu uniquement à caractère promotionnel ou
  publicitaire en lien avec l’activité de l’Annonceur Pro est interdit.
  Dans le cas d’un bien disponible, une Commande ne peut être passée que
  i) si une Annonce pour ce même bien n’a pas déjà été publiée par un
  autre Annonceur Pro dans les quatre (4) mois précédant le dépôt dans
  cette rubrique et, ii) que sur la plateforme de l’immeuble dans lequel
  se situe le bien de l’Annonce.

  * *4.3.2 Options Payantes*

  	Une Option Payante n'est souscrite que pour une seule Annonce. En
  conséquence, il n'est pas possible de transférer le bénéfice d'une
  Option Payante d'une Annonce à une autre.

  	Il est possible de souscrire à plusieurs Options Payantes pour une même
  Annonce et ce de manière simultanée. En effet, les Options Payantes ne
  peuvent être souscrites qu’au moment du dépôt de l’Annonce.

  	Les Options Payantes sont souscrites pour la durée de diffusion de
  l'Annonce hors prolongation, soit pour une durée maximum de quatre (4)
  mois.

  	Lors du retrait anticipé de l'Annonce (soit du fait de l'Annonceur Pro,
  soit du fait de MonBuilding&Co notamment en cas de non-respect des
  présentes CGV ou des Règles de diffusion sur le Service MonBuilding) ou
  à l'expiration de sa durée de diffusion, l'Option Payante cesse de
  produire ses effets. La prolongation d'une Annonce ne prolonge pas la
  durée de l'Option Payante.

	* 	*4.3.2.1 Ajouter un Logo Urgent*

		Pour afficher un Logo Urgent sur une Annonce, il convient de se rendre
sur son Compte Pro et, au moment du dépôt, de sélectionner cette Option
Payante sur le formulaire de dépôt de l’Annonce.

		Si l'Annonce est publiée par MonBuilding&Co, elle sera mise en ligne sur
le Site et sera signalée par un logo urgent dans la liste de résultats.

	* *4.3.2.2 Pack Photos supplémentaires*

		Cette Option Payante permet d'ajouter cinq (5) photographies
supplémentaires dans une Annonce et ainsi de présenter au maximum dix
(10) photographies dans une Annonce.

		Pour pouvoir ajouter des photos supplémentaires dans une Annonce,
l'Annonceur Pro doit souscrire à cette Option au moment du dépôt en la
sélectionnant sur le formulaire de dépôt de l'Annonce

* *4.4. Exécution, tarif et paiement*

	* *4.4.1 Confirmation de la Commande*

		MonBuilding&Co accuse réception sans délai à l’Annonceur Pro, par
courrier électronique, du bon de commande et du paiement effectué, et
l’informe de la mise en ligne de l’Annonce commandée dans les conditions
décrites ci-après.

	* *4.4.2 Exécution de la Commande*

		L’exécution de la Commande intervient après validation du paiement par
MonBuilding&Co et dans un délai maximal de 7 jours à compter du paiement
effectif du bon de commande par l’Annonceur Pro.

		Le paiement effectif est réalisé dès lors que les sommes correspondantes
à la Commande sont définitivement créditées sur le compte de
MonBuilding&Co.

		Passé ce délai et à défaut de mise en ligne par MonBuilding&Co,
l’Annonceur Pro est en droit de demander l'annulation de la transaction
et le remboursement des sommes déjà versées.

	* *4.4.3 Tarifs & Paiement*

		Les Annonces commandées sont mentionnées dans le bon de commande ; les
frais associés s'entendent toutes taxes comprises sauf indication
contraire et sont payables en euros.

		Les tarifs en vigueur sont disponibles en consultation en ligne sur le
Site et sur demande auprès de MonBuilding&Co, à l'adresse suivante :
<bonjour@monbuilding.com>.

		MonBuilding&Co adressera par courrier électronique et/ou mettra à la
disposition de l’Annonceur Pro via son Compte Pro une facture après
chaque paiement. L’Annonceur Pro accepte expressément que la facture lui
soit transmise par voie électronique.

		MonBuilding&Co se réserve le droit de répercuter, sans délai, toute
nouvelle taxe ou toute augmentation de taux des taxes existantes.

		Tout désaccord concernant la facturation et les Annonces devra être
exprimé auprès du Service Client dans un délai d'un mois après émission
du bon de commande.

		Le paiement des Annonces (à l’unité ou des Packs MonBuilding) et des
Options Payantes s’effectue par carte bancaire au moment de la Commande

		L’Annonceur Pro est seul responsable du paiement de l'ensemble des
sommes dues. De convention expresse et sauf report sollicité à temps et
accordé par MonBuilding&Co de manière particulière et écrite, le défaut
total ou partiel de paiement à l'échéance de toute somme due entraînera
de plein droit et sans mise en demeure préalable :

		-   l'exigibilité immédiate de toutes les sommes restant dues par
    l’Annonceur Pro

		-   la suspension de toutes les Annonces non réglées, sans préjudice
    pour MonBuilding&Co d'user de la faculté de résiliation du contrat –
    à l’exception des \[10\] Annonces gratuites des Packs MonBuilding.
    Seules les \[10\] Annonces les plus récentes seront maintenues en
    ligne

		-   l'application d'un intérêt à un taux égal à 1.5 fois le taux
    d’intérêt légal en vigueur en France

		Dans l'hypothèse où des frais seraient exposés par MonBuilding&Co,
MonBuilding&Co en informera l’Annonceur Pro et lui communiquera les
justificatifs et la facture correspondant. L’Annonceur Pro devra alors
régler la somme due en euros.

		L’Annonceur Pro en situation de retard de paiement est de plein droit
débiteur, à l'égard de MonBuilding&Co, d'une indemnité forfaitaire pour
frais de recouvrement à hauteur de 40€, en application de la loi
2012-387 du 22 mars 2012.

		*Particularités liées aux Packs MonBuilding*

		L’Annonceur Pro peut opérer un changement de configuration en cours de
facturation. Dans ce cas, le basculement vers une configuration
inférieure ou supérieure (ci-après le Basculement) est facturé à
l’Annonceur Pro selon la base tarifaire applicable à la nouvelle
configuration et consultable sur le Site.

		Si MonBuilding&Co n'est pas en mesure de prélever sur la carte bancaire
associée, un courriel sera transmis à l’Annonceur Pro l'invitant à
régler dans les plus brefs délais le montant de sa facture en attente de
règlement. A défaut, la Commande sera suspendue de plein droit par
MonBuilding&Co.

		Le Basculement intervient à compter de la date de l’opération jusqu'à la
date d’expiration du Pack. Il est facturé de la différence entre le
tarif mensuel applicable au Pack MonBuilding actuel et le tarif
applicable au nouveau Pack MonBuilding au prorata-temporis. Toute somme
créditrice de l’Annonceur Pro postérieurement au Basculement de
configuration n’est pas remboursable.

		MonBuilding&Co se réserve la faculté de modifier ses prix à tout moment,
sous réserve d'en informer l’Annonceur Pro par courrier électronique ou
par un avertissement en ligne sur le Site un (1) mois à l'avance si les
nouveaux tarifs hors taxes sont moins favorables à l’Annonceur Pro. Dans
cette hypothèse, l’Annonceur Pro disposera à compter de cette
information d'un délai d'un (1) mois pour résilier sans pénalité. A
défaut, l’Annonceur Pro sera réputé avoir accepté les nouveaux tarifs.
Les modifications de tarifs seront applicables à tous les contrats et
notamment à ceux en cours d'exécution.

	* *4.4.4 Durée*

		Le Compte Pro est gratuit et créé pour une durée indéterminée. Il peut
être résilié à tout moment sans préavis par l’Annonceur Pro en écrivant
au Service Client.

		Par défaut, l’Annonce a la durée prévue à la Commande et figurant sur la
facture remise par MonBuilding&Co. Les données seront effacées à
l'expiration de l’Annonce. MonBuilding&Co s'engage à effectuer, au
minimum, un (1) rappel par courrier électronique avant l'expiration de
l’Annonce.

		Pour les Packs MonBuilding, la durée initiale est d’un mois renouvelable
par tacite reconduction pour une durée identique. Le renouvellement est
automatique via l’enregistrement obligatoire de la carte bancaire de
l’Annonceur Pro. Le renouvellement automatique n'implique pas de durée
d'engagement et peut donc être désactivé à tout moment.

		Pour une résiliation avant échéance du Pack MonBuilding, l’Annonceur Pro
est libre de résilier le contrat par simple courriel à l’adresse
suivante : <bonjour@monbuilding.com>. Dans ce cas, l’Annonceur Pro ne
pourra prétendre au remboursement par MonBuilding&Co des sommes déjà
versées.

		S’agissant des Annonces du Pack Références, la résiliation de ces Annonces
intervient automatiquement dès lors que les opérations mentionnées ont
plus de deux (2) ans. Une nouvelle Annonce pourra donc prendre sa place
dans le Pack Références, sans pour autant augmenter la durée de validité
restant à courir du Pack Références.

		MonBuilding&Co se réserve le droit de résilier à tout moment le Compte
Pro, avec une prise d’effet à l’expiration d’un préavis minimal d’un
mois, par voie électronique ou postale.

		La résiliation dans les conditions visées ci-dessus ne donnera lieu au
versement d’aucune indemnité de quelque nature que ce soit.

### 5. Engagement et garantie de l'Annonceur Pro

**5.1.** L'Annonceur Pro certifie que l'Annonce, quelle que soit sa
diffusion, est conforme à l'ensemble des dispositions légales et
réglementaires en vigueur (notamment relatives à la publicité, à la
concurrence, à la promotion des ventes, à l'utilisation de la langue
française, à l'utilisation de données personnelles, à la prohibition de
la commercialisation de certains biens ou services), respecte les
dispositions des présentes CGV et les règles de diffusion du Service
MonBuilding et ne porte pas atteinte aux droits des tiers (notamment aux
droits de propriété intellectuelle et aux droits de la personnalité …).

En particulier, l’Annonceur Pro s’engage à ce que l’Annonce ne contienne
aucune information imprécise, fausse, mensongère ou de nature à induire
en erreur les Utilisateurs ni aucune mention diffamatoire, médisante,
calomnieuse ou de nature à nuire aux intérêts et/ou à l'image de
MonBuilding&Co ou de tout tiers, ni aucun contenu obscène, pédophile,
discriminatoire ou incitant à la violence / haine raciale, religieuse ou
ethnique.

L'Annonceur Pro garantit que le contenu de ses Annonces est strictement
conforme aux obligations légales s'imposant à son activité.

L'Annonceur Pro garantit MonBuilding&Co être l'auteur unique et exclusif
du texte, des dessins, photographies etc. composant l'Annonce. A défaut,
il déclare disposer de tous les droits, notamment les droits de
propriété intellectuelle et autorisations nécessaires à la diffusion de
l'Annonce.

En conséquence, toute Annonce déposée et diffusée sur le Service
MonBuilding paraît sous la responsabilité exclusive de l'Annonceur Pro.

En conséquence, l'Annonceur Pro relève MonBuilding&Co, ses
sous-traitants et fournisseurs, de toutes responsabilités, les garantit
contre tout recours ou action en relation avec l'Annonce qui pourrait
être intenté contre ces derniers par tout tiers, et prendra à sa charge
tous les dommages-intérêts ainsi que les frais et dépens auxquels ils
pourraient être condamnés ou qui seraient prévus à leur encontre par un
accord transactionnel signé par ces derniers avec ce tiers, nonobstant
tout dommages-intérêts dont MonBuilding&Co, ses sous-traitants et
fournisseurs pourraient réclamer à raison des faits dommageables de
l'Annonceur Pro.

En déposant toute Annonce, l’Annonceur Pro reconnaît et accepte que
MonBuilding&Co puisse supprimer, ou refuser, à tout moment, sans
indemnité ni droit à remboursement des sommes engagées par l'Annonceur
Pro, une Annonce qui serait contraire notamment à la loi française, aux
règles de diffusion du Service MonBuilding et/ou susceptible de porter
atteinte aux droits de tiers.

**5.2.** A ce titre, l'Annonceur Pro reconnaît et accepte que pour des
raisons d'ordre technique, la mise en ligne d'une Annonce sur le Site
peut ne pas être instantanée avec sa validation.

**5.3.** L'Annonceur Pro déclare connaître l'étendue de diffusion du Site,
avoir pris toutes précautions pour respecter la législation en vigueur
des lieux de réception et décharger MonBuilding&Co de toutes
responsabilités à cet égard.

**5.4.** L'Annonceur Pro accepte que les données collectées ou recueillies
sur le Site soient conservées par les fournisseurs d'accès et utilisées
à des fins statistiques ou pour répondre à des demandes déterminées ou
émanant des pouvoirs publics.

**5.5.** Pour être recevable, toute réclamation devra indiquer précisément
le(s) défaut(s) allégué(s) de l'Annonce et être transmise par écrit à
MonBuilding&Co dans un délai de huit (8) jours ouvrables à compter de la
date de dépôt.

**5.6.** L'Annonceur Pro déclare être informé qu'il devra, pour accéder au
Service MonBuilding, disposer d'un accès à l'Internet souscrit auprès du
fournisseur de son choix, dont le coût est à sa charge, et reconnaît que
:

-   La fiabilité des transmissions est aléatoire en raison, notamment,
    du caractère hétérogène des infrastructures et réseaux sur
    lesquelles elles circulent et que, en particulier, des pannes ou
    saturations peuvent intervenir

-   Il appartient à l'Annonceur Pro de prendre toute mesure qu'il jugera
    appropriée pour assurer la sécurité de son équipement et de ses
    propres données, logiciels ou autres, notamment contre la
    contamination par tout virus et/ou de tentative d'intrusion dont il
    pourrait être victime

-   Tout équipement connecté au Site est et reste sous l'entière
    responsabilité de l'Annonceur Pro, la responsabilité de
    MonBuilding&Co ne pourra pas être recherchée pour tout dommage
    direct ou indirect qui pourrait subvenir du fait de la connexion au
    Site

**5.7.** L'Annonceur Po s'engage également à ce que son Compte Pro ne
contienne :

-   aucune information fausse et/ou mensongère

-   aucune information portant atteinte aux droits d'un tiers

Dans ce cadre, le titulaire déclare et reconnaît qu'il est seul
responsable des informations renseignées lors de la création de son
Compte Pro.

En créant un Compte Pro, chaque titulaire reconnaît et accepte que
MonBuilding&Co puisse supprimer, à tout moment, sans indemnité ni droit
à remboursement des sommes engagées par l'Annonceur Pro, un compte qui
serait contraire notamment à la loi française et/ou aux règles de
diffusion fixées par MonBuilding&Co.

L'Annonceur Pro s'engage en outre à respecter et à maintenir la
confidentialité des identifiants de connexion (courriel et mot de passe)
à son Compte Pro et reconnaît expressément que toute connexion sur son
Compte Pro, ainsi que toute transmission de données sur ou depuis son
Compte Pro sera réputée avoir été effectuée par l'Annonceur Pro.

Toute perte, détournement ou utilisation des identifiants de connexion
et leurs éventuelles conséquences relèvent de la seule et entière
responsabilité de l'Annonceur Pro.

### 6. Responsabilité / garantie

***6.1 Limitation de responsabilité***

MonBuilding&Co s'engage à mettre en oeuvre tous les moyens nécessaires
afin d'assurer au mieux la fourniture du Service MonBuilding qu'elle
propose à l'Annonceur Pro dans le cadre d'une obligation de moyens. Sauf
engagement écrit contraire, la prestation commercialisée par
MonBuilding&Co se limite à la diffusion d'Annonce, avec souscription
d'Options Payantes pour les Annonces immobilières de la rubrique
« Petites annonces », sur le Site, à l'exclusion de toute autre
prestation. MonBuilding&Co ne garantit aucunement les éventuels
résultats escomptés par l'Annonceur Pro suite à la diffusion des
Annonces.

MonBuilding&Co ne pourra être tenue responsable de la capture des
données qui serait faite à son insu, ni de la traçabilité qui en
résulterait.

MonBuilding&Co ne pourra être tenue responsable des interruptions et
modifications du Service MonBuilding et/ou du Site, et de la perte de
données ou d'informations stockées par MonBuilding&Co ou par l'Annonceur
Pro sur son Compte Pro ; il incombe à l'Annonceur Pro de prendre toute
précautions utiles pour conserver les Annonces qu'ils publient sur le
Site.

MonBuilding&Co ne pourra pas non plus être tenue responsable en cas
d'utilisation non conforme du Service MonBuilding par l'Annonceur Pro,
et en cas de non-conformité du Service MonBuilding aux besoins ou aux
attentes spécifiques de l'Annonceur Pro.

MonBuilding&Co ne pourra être tenue responsable, notamment ni du fait de
préjudices ou dommages directs ou indirects, de quelque nature que ce
soit, résultant de la gestion, l'utilisation, l'exploitation,
l'interruption ou le dysfonctionnement du Site et/ou du Service
MonBuilding.

MonBuilding&Co, ses sous-traitants ou fournisseurs, ne pourront être
tenus pour responsables des retards ou impossibilités de remplir leurs
obligations contractuelles, en cas :

-   de force majeure

-   d'interruption de la connexion au Site en raison d'opérations de
    maintenance ou d'actualisation des informations publiées

-   d'impossibilité momentanée d'accès au Site en raison de problèmes
    techniques, quelle qu'en soit l'origine

-   d'attaque ou piratage informatique, privation, suppression ou
    interdiction, temporaire ou définitive, et pour quelque cause que ce
    soit, de l'accès au réseau Internet

L'Annonceur Pro reconnaît en outre qu'en l'état actuel de la technique
et en l'absence de garantie des opérateurs de télécommunications, la
disponibilité permanente du Service MonBuilding et notamment du Site ne
peut être garantie.

Sauf dol ou faute lourde de MonBuilding&Co, ses sous-traitants et
fournisseurs dans l’exécution du Service MonBuilding, ces derniers ne
seront tenus en aucun cas à réparation, pécuniaire ou en nature et ne
pourront en aucun cas se voir refuser un paiement, même partiel, ni être
tenus à une quelconque indemnisation.

Il est expressément prévu que, dans l'hypothèse où l'un des Services
MonBuilding fournit par MonBuilding&Co à l'Annonceur Pro n'aboutirait
pas, la responsabilité de MonBuilding&Co au titre des présentes est
limitée à la refourniture de la prestation mise en cause et n'ayant pas
abouti.

En tout état de cause, la responsabilité de MonBuilding&Co au titre d'un
dommage quelconque sera limitée à un montant ne pouvant excéder les
sommes hors taxes effectivement payées par l'Annonceur Pro à
MonBuilding&Co au cours des trois (3) mois précédant le fait générateur
de la responsabilité de MonBuilding&Co. En outre, la responsabilité de
MonBuilding&Co ne pourra être engagée que pour les dommages directs
subis par l'Annonceur Pro, résultant d'un manquement à ses obligations
contractuelles telles que définies aux présentes. L'Annonceur Pro
renonce donc à demander réparation à MonBuilding&Co à quelque titre que
ce soit, de dommages indirects tels que le manque à gagner, la perte de
chance, le préjudice commercial ou financier, l'augmentation de frais
généraux ou les pertes trouvant leur origine ou étant la conséquence de
l'exécution des présentes.

***6.2 Garantie de MonBuilding&Co***

MonBuilding&Co s'engage à assurer la permanence, la continuité et la
qualité de l'accès et de l'exploitation du Site et du Compte Pro.

A ce titre, MonBuilding&Co fera ses meilleurs efforts pour maintenir un
accès à son Site 24h/24H et 7j/7j, avec une accessibilité de garantie de
99% du temps sur une année sauf cas de force majeure. En cas de
nécessité, MonBuilding&Co se réserve la possibilité de limiter ou de
suspendre l'accès au Site et/ou au Compte Pro pour procéder à toute
opération de maintenance et/ou d'amélioration.

L'Annonceur Pro reconnaît expressément que la présente garantie ne
couvre pas toute panne ou interruption du service intervenant du fait
des opérateurs télécoms et/ou de la société hébergeant le Site.

Il est expressément convenu entre les parties que les obligations
souscrites par MonBuilding&Co dans le cadre du présent article sont de
moyens.

### 7. Force majeure

Ni l'Annonceur Pro, d'une part, ni MonBuilding&Co, ses sous-traitants ou
fournisseurs, d'autre part, ne pourra être tenu pour responsable de tout
retard, inexécution ou autre manquement à ses obligations résultant d'un
cas de force majeure. Seront notamment considérés comme des cas de force
majeur ceux habituellement retenus par la jurisprudence des Cours et
Tribunaux français ainsi que les grèves totales ou partielles, internes
ou externes à l'une des parties, à un fournisseur ou sous-traitant,
lock-out, blocages des moyens de transport ou d'approvisionnement pour
quelque raison que ce soit, incendies, tempêtes, inondations, dégâts des
eaux, restrictions gouvernementales ou légales, modifications légales ou
réglementaires des formes de commercialisation, blocage des moyens de
télécommunications, y compris les réseaux, et tout autre cas indépendant
de la volonté de l'Annonceur Pro, de MonBuilding&Co, ses sous-traitants
ou fournisseurs empêchant l'exécution normale des prestations.

Chaque partie notifiera à l'autre partie par lettre recommandée avec
accusé de réception la survenance de tout cas de force majeure.

En présence d'un cas de force majeur, si l'empêchement d'exécuter
normalement l'obligation contractuelle devait perdurer plus d’un (1)
mois, les parties seront libérées de leurs obligations réciproques, de
plein droit, sans formalité judiciaire, sans préavis et sans qu'aucune
indemnité de quelque nature que ce soit ne puisse être réclamée à la
partie défaillante, après l'envoi d'une lettre recommandée avec accusé
de réception ayant effet immédiat.

### 8. Propriété intellectuelle

**8.1.** Tous les droits de propriété intellectuelle (tels que notamment
droits d'auteur, droits voisins, droits des marques, droits des
producteurs de bases de données) portant tant sur la structure que sur
les contenus du Site et notamment les images, sons, vidéos,
photographies, logos, marques, éléments graphiques, textuels, visuels,
outils, logiciels, documents, données, etc. (ci-après désignés dans leur
ensemble " Eléments ") sont réservés. Ces Eléments sont la propriété de
MonBuilding&Co. Ces Eléments sont mis à disposition des Utilisateurs et
des Annonceurs, à titre gracieux, pour la seule utilisation du Service
MonBuilding et dans le cadre d'une utilisation normale de ses
fonctionnalités. Les Utilisateurs et les Annonceurs s'engagent à ne
modifier en aucune manière les Eléments.

Toute utilisation non expressément autorisée des Eléments du Site
entraîne une violation des droits d'auteur et constitue une contrefaçon.
Elle peut aussi entraîner une violation des droits à l'image, droits des
personnes ou de tous autres droits et réglementations en vigueur. Elle
peut donc engager la responsabilité civile et/ou pénale de son auteur.

**8.2.** Il est interdit à tout Utilisateur et Annonceur de copier,
modifier, créer une œuvre dérivée, inverser la conception ou
l'assemblage ou de toute autre manière tenter de trouver le code source,
vendre, attribuer, sous licencier ou transférer de quelque manière que
ce soit tout droit afférent aux Eléments.

Tout Utilisateur et Annonceur du Service MonBuilding s'engagent
notamment à ne pas :

-   utiliser ou interroger le Service MonBuilding pour le compte ou au
    profit d'autrui

-   extraire/reproduire en nombre, à des fins commerciales ou non, tout
    ou partie des Annonces présentes sur le Service MonBuilding et sur
    le Site

-   intégrer tout ou partie du contenu du Site dans un site tiers, à des
    fins commerciales ou non ;

-   reproduire sur tout autre support, à des fins commerciales ou non,
    tout ou partie des Annonces présentes sur le Service MonBuilding et
    sur le Site permettant de reconstituer tout ou partie des fichiers
    d'origine

-   utiliser un robot, notamment d'exploration (spider), une application
    de recherche ou récupération de sites Internet ou tout autre moyen
    permettant de récupérer ou d'indexer tout ou partie du contenu du
    Site, excepté en cas d'autorisation expresse et préalable de
    MonBuilding&Co

Toute reproduction, représentation, publication, transmission,
utilisation, modification ou extraction de tout ou partie des Eléments
et ce de quelque manière que ce soit, faite sans l'autorisation
préalable et écrite de MonBuilding&Co est illicite. Ces actes illicites
engagent la responsabilité de ses auteurs et sont susceptibles
d'entraîner des poursuites judiciaires à leur encontre et notamment pour
contrefaçon.

**8.3.** Les marques et logos MonBuilding et MonBuilding.com ainsi que les
marques et logos des partenaires de MonBuilding&Co sont des marques
déposées. Toute reproduction totale ou partielle de ces marques et/ou
logos sans l'autorisation préalable et écrite de MonBuilding&Co est
interdite.

**8.4.** MonBuilding&Co est producteur des bases de données du Service
MonBuilding. En conséquence, toute extraction et/ou réutilisation de la
ou des bases de données au sens des articles L 342-1 et L 342-2 du code
de la propriété intellectuelle est interdite.

**8.5.** Le Site peut contenir des liens hypertextes redirigeant vers des
contenus émanant de tiers ou vers des sites Internet exploités par des
tiers. Ces liens sont fournis à simple titre d’information.
MonBuilding&Co n'est pas responsable de la qualité ou de l'exactitude de
ces contenus ni de ces sites Internet et ne peut pas non plus être
considéré comme approuvant, publiant ou autorisant ces sites Internet ou
ces contenus. MonBuilding&Co décline toute responsabilité quant aux
dommages pouvant résulter de l'utilisation de ces sites. La décision
d’activer ces liens relève de la pleine et entière responsabilité de
l’Utilisateur.

**8.6.** Aucun lien hypertexte ne peut être créé vers le Service MonBuilding
sans l'accord préalable et exprès de MonBuilding&Co. Si un internaute ou
une personne morale désire créer, à partir de son site, un lien
hypertexte vers le Service MonBuilding et ce quel que soit le support,
il doit préalablement prendre contact avec MonBuilding&Co en lui
adressant un courriel à l'adresse suivante <bonjour@monbuilding.com>.
Tout silence de MonBuilding&Co devra être interprété comme un refus.

**8.7.** Le contenu des Annonces appartient aux Annonceurs les ayant
déposées, néanmoins, en déposant des Annonces sur le Site, l’Annonceur
concède :

-   à MonBuilding&Co le droit d’exploitation non exclusif, transférable,
    sous licenciable, à titre gracieux, pour le monde entier sur (i)
    l’ensemble du contenu des Annonces et notamment sur les
    photographies, textes, vidéos, illustrations, marques, logos, titres
    (ci-après le « Contenu »), au fur et à mesure de leur publication
    sur le Site ainsi (ii) qu’une licence sur l’ensemble des droits de
    propriété intellectuelle afférant au Contenu et notamment sur les
    droits d’auteurs sur les éléments utilisés dans son Annonce, tels
    que les photographies, textes, vidéos, dessins, illustrations,
    éléments sonores, et ce pour toute la durée légale de ses droits de
    propriété intellectuelle et pour le monde entier

	Les droits ainsi concédés incluent le droit de reproduire, représenter, 
  diffuser, adapter, modifier, réaliser une œuvre dérivée, traduire tout ou 
  partie du Contenu par tous procédés, sous quelque forme que ce soit et sur 
  tous supports (numérique, imprimé…) connus ou inconnus à ce jour, dans le 
  cadre du Service MonBuilding ou en relation avec l’activité de 
  MonBuilding&Co, et ce à des fins commerciales ou non et notamment 
  publicitaires, ainsi que dans le cadre d’une diffusion sur les réseaux 
  sociaux sur lesquels MonBuilding est ou sera présent et notamment les 
  pages Facebook, Instagram et Twitter de MonBuilding

	En particulier, les photographies des Annonces pourront être reproduites 
  et intégrées dans des formats publicitaires diffusés sur le Site, 
  uniquement en lien avec l’Annonce déposée

	L’Annonceur ayant publié une Annonce sur le Site accorde son
consentement à la reprise de son Annonce et de leur Contenu sur les
réseaux sociaux, notamment Facebook, Instagram et Twitter. Par
conséquent, l’Annonceur atteste avoir pris connaissance des conditions
générales d’utilisation des sites Facebook, Instagram, et Twitter et
en accepter les termes, particulièrement en matière de réutilisation
du Contenu et des données personnelles

	Au titre de cette licence, MonBuilding&Co, sans que cela ne crée à sa
charge une obligation d’agir, est en droit de s’opposer à la
reproduction et l’exploitation par des tiers non autorisés des
Annonces diffusées sur le Site et de leur Contenu

-   aux Utilisateurs, le droit non exclusif d’accéder au Contenu via le
    Service MonBuilding et d’utiliser et de représenter le Contenu dans
    la mesure autorisée par les fonctionnalités du Service MonBuilding,
    et ce pour le monde entier

### 9. Sous traitance

MonBuilding&Co pourra librement recourir aux prestataires et/ou
sous-traitants de son choix pour la réalisation de tout ou partie des
prestations, sans avoir à en informer l'Annonceur Pro, ni à requérir son
accord sur l'identité de ces tiers.

### 10. Données à caractère personnel

MonBuilding&Co garantit que toutes les données à caractère personnel au
sens de la loi n°78-17 du 6 janvier 1978 modifiée quelle pourrait
collecter dans le cadre de la fourniture du Service MonBuilding, le sont
dans le cadre d'un traitement strictement nécessaire à la fourniture
dudit Service MonBuilding.

MonBuilding&Co s'engage à respecter toutes les obligations légales et
réglementaires en matière de protection des données à caractère
personnel visant à garantir notamment la sécurité et la confidentialité
de ces données. Les engagements de MonBuilding&Co pris en application
des articles 38 et suivants de la loi n°78-17 du 6 janvier 1978 sont
détaillés à l'article 7. « Protection, collecte, utilisation et
communication des données personnelles & Cookies » des CGU.

### 11. Dispositions diverses

MonBuilding&Co se réserve la possibilité de modifier, à tout moment, en
tout ou partie les CGV. Les Annonceurs Pro sont invités à consulter
régulièrement les présentes CGV afin de prendre connaissance de
changements éventuels effectués.

Si une partie des CGV devait s'avérer illégale, invalide ou
inapplicable, pour quelle que raison que ce soit, les dispositions en
question seraient réputées non écrites, sans remettre en cause la
validité des autres dispositions qui continueront de s'appliquer entre
les Annonceurs Pro et MonBuilding&Co.

Il est à noter que le présent contrat et les présentes CGV sont soumises
aux dispositions de la loi du N°2004-575 du 21 Juin 2004 art 25-II et
l'Ordonnance N°2005-674 du 16 Juin 2005 et aux articles 1369-1 à 1369-2
du Code Civil .

Toute contestation et/ou difficulté d'interprétation ou d'exécution des
présentes conditions générales relèvera des tribunaux compétents de la
ville de Paris, même en cas d’appel en garantie ou de pluralité de
défendeurs, ou en cas de procédure d’urgence ou conservatoire, en référé
ou par requête.

Les présentes conditions générales de vente sont soumises à la loi
française.
