Package.describe({
  name: 'mon:photoset-grid',
  version: '0.0.1',
  summary: 'Meteor wrapper https://github.com/stylehatch/photoset-grid/',
  git: 'https://github.com/stylehatch/photoset-grid/',
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('METEOR@0.9.2.2');
  api.use([
    'templating'
  ], 'client');
  api.addFiles([
    'jquery.photoset-grid.js',
    'js/jquery.colorbox.js',
    'css/colorbox.css',
  ], 'client');
});