
$.fullCalendar.locale("fr", {
	buttonText: {
		year: "Année",
		month: "Mois",
		week: "Semaine",
		day: "Jour",
		list: "Mon planning"
	},
	allDayHtml: "Toute la<br/>journée",
	eventLimitText: "en plus",
	noEventsMessage: "Aucun évènement à afficher"
});
