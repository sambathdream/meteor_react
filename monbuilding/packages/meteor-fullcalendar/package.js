Package.describe({
    name: 'monbuilding:fullcalendar',
    summary: "Full-sized drag & drop event calendar (jQuery plugin)",
    version: "3.4.0",
    git: "https://github.com/rzymek/meteor-fullcalendar.git"
});

Package.onUse(function(api) {
    api.versionsFrom('METEOR@0.9.2.2');
    api.use([
        'momentjs:moment@2.8.4',
        'templating'
    ], 'client');
    api.addFiles([
        'fullcalendar/dist/fullcalendar.js',
        'fullcalendar/dist/fullcalendar.css',
        'fullcalendar/locale/fr.js',
        // 'fullcalendar/dist/gcal.js',
        'fullcalendar-scheduler/dist/scheduler.css',
        'fullcalendar-scheduler/dist/scheduler.js',
        'template.html',
        'template.js'
    ], 'client');
});
