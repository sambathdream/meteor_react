import { Meteor } from 'meteor/meteor'

let filesServerUrl = 'https://files.monbuilding.com'

if (
  Meteor.isDevelopment ||
  (Meteor.absoluteUrl().search('www.monbuilding.com') === -1)
) {
  filesServerUrl = filesServerUrl + '/develop'
}

// filesServerUrl = 'http://192.168.86.85:3001'

export const environment = {
  filesServerUrl
}
