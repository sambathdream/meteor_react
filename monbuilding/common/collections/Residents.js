ResidentUsers = new Mongo.Collection("residentUsers");
ResidentPendingUsers = new Mongo.Collection("residentPendingUsers");
Residents = new Mongo.Collection("residents");
UsersJoinedDate = new Mongo.Collection("usersJoinedDate");

UsersRights = new Mongo.Collection("usersRights");

Messages = new Mongo.Collection("messages");
MessagesFeed = new Mongo.Collection("messagesFeed");
DefaultRights = new Mongo.Collection("defaultRights");
DefaultRoles = new Mongo.Collection("defaultRoles");
CondoRole = new Mongo.Collection("condoRole");

if (Meteor.isClient) {
	UsersProfile = new Mongo.Collection("usersProfile")
}

if (Meteor.isServer) {
	Meteor.createDefaultRightsForManager = function(userId, defaultRoleId) {
		if (!Meteor.isServer)
			return;
		let enterprises = Enterprises.find({"users.userId": userId}).fetch();
		let condoIds = [];
		let j = 0;
		while (j < enterprises.length)
		{
			let i = 0;
			while (i < enterprises[j].users.length) {
				if (enterprises[j].users[i].userId == userId) {
					_.each(enterprises[j].users[i].condosInCharge, function(condoInCharge){
						let condoId = condoInCharge.condoId;
						let managerRights = UsersRights.findOne({userId: userId, condoId: condoId});

						if (!managerRights || (managerRights && !managerRights.defaultRoleId)) {
							UsersRights.upsert({
								condoId: condoId,
								userId: userId,
							}, {
								$set: {defaultRoleId: defaultRoleId}
							});
						}
					});
				}
				i++;
			}
			j++;
		}
	}

	Meteor.createDefaultRightsForOccupant = function(userId, defaultRoleId) {
		if (!Meteor.isServer)
			return;
		let resident = Residents.findOne({userId: userId});
		let condoIds = [];
		let occupantRights;
		if (resident)
			condoIds = _.map(resident.condos, function(condo){
				return condo.condoId;
			});
		let i = 0;
		while (i < condoIds.length)
		{
			Meteor.createDefaultRightsForUserWithCondo(userId, condoIds[i], defaultRoleId);
			i++;
		}
	}

	Meteor.createDefaultRightsForNewOccupant = function(userId) {
		if (!Meteor.isServer)
			return;
		let resident = Residents.findOne({userId: userId});
		let condos = [];
		let occupantRights;
		if (resident)
			condos = _.map(resident.condos, function(condo){
				return {condoId: condo.condoId, roleId: condo.roleId};
			});
		let i = 0;
		while (i < condos.length)
		{
			Meteor.createDefaultRightsForUserWithCondo(userId, condos[i].condoId, condos[i].roleId);
			i++;
		}
	}

	Meteor.createDefaultRightsForNewOccupantWithCondo = function(userId, condoId) {
		if (!Meteor.isServer)
			return;
		let resident = Residents.findOne({userId: userId});
		let condos = [];
		let occupantRights;
		if (resident)
			condos = _.map(resident.condos, function(condo){
				if (condo.condoId == condoId)
					return {condoId: condo.condoId, roleId: condo.roleId};
			});
		let i = 0;
		while (i < condos.length)
		{
			Meteor.createDefaultRightsForUserWithCondo(userId, condos[i].condoId, condos[i].roleId);
			i++;
		}
	}

	Meteor.createDefaultRightsForUserWithCondo = function(userId, condoId, defaultRoleId) {
		if (!Meteor.isServer)
			return;
		if (!defaultRoleId) {
			defaultRoleId = DefaultRoles.findOne({ name: "Occupant", for: "occupant" })._id
		}
		let userrights = UsersRights.findOne({
			condoId: condoId,
			userId: userId,
		});

		if (!userrights || (userrights && !userrights.defaultRoleId)) {
			UsersRights.upsert({
				condoId: condoId,
				userId: userId,
			}, {
				$set: {defaultRoleId: defaultRoleId}
			});
		}
	}
	Meteor.createDefaultRightsForManagerWithCondo = function(userId, condoId, defaultRoleId) {
		if (!Meteor.isServer)
			return;
    UsersRights.upsert({
      condoId: condoId,
      userId: userId,
    }, {
      $set: {defaultRoleId: defaultRoleId}
    });
	}

	Meteor.removeUserRights = function(userId, condoId) {
		UsersRights.remove({
			userId: userId,
			condoId: condoId
		});
	}
}

Residents.helpers({
	getBuildings() {
		let buildingsId = [];
		for (let i = 0; i < this.condos.length; ++i) {
			for (let j = 0; j < this.condos[i].buildings.length; j++) {
				if (buildingsId.indexOf(this.condos[i].buildings[j].buildingId) === -1)
					buildingsId.push(this.condos[i].buildings[j].buildingId);
			}
		}
		return Buildings.find( { _id : { $in : buildingsId } } ).fetch();
	},
	getCondos() {
		let condosId = this.condos.map((b) => b.condoId);
		return Condos.find( { _id : { $in : condosId } } ).fetch();
	},
});

DefaultRoles.helpers({
	getResidentsByCondo(condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.find({
			condoId: condoId,
			defaultRoleId: roleId,
		}).fetch();
		let userIdsWithRole = _.map(usersRights, function(elem) {
			return elem.userId;
		});
		return ResidentUsers.find({
			_id: {$in: userIdsWithRole}
		}).fetch()
	},
	getUserIdsByCondo(condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.find({
			condoId: condoId,
			defaultRoleId: roleId,
		}).fetch();
		let userIdsWithRole = _.map(usersRights, function(elem) {
			return elem.userId;
		});
		return userIdsWithRole;
	},
	getResidentsOtherByCondo(condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.find({
			condoId: condoId,
			defaultRoleId: {$ne: roleId},
		}).fetch();
		let userIdsWithRole = _.map(usersRights, function(elem) {
			return elem.userId;
		});
		return ResidentUsers.find({
			_id: {$in: userIdsWithRole}
		}).fetch()
	},
	getResidentsByCondoCount(condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.find({
			condoId: condoId,
			defaultRoleId: roleId,
		}).fetch();
		let userIdsWithRole = _.map(usersRights, function(elem) {
			return elem.userId;
		});
		return ResidentUsers.find({
			_id: {$in: userIdsWithRole}
		}).count()
	},
	existOneUserInCondo(condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.findOne({
			condoId: condoId,
			defaultRoleId: roleId,
		});
		return usersRights ? true : false;
	},
	isUserMember(userId, condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.findOne({
			condoId: condoId,
			userId: userId,
			defaultRoleId: roleId,
		});
		return usersRights ? true : false;
	},
	isUserNotMember(userId, condoId) {
		let roleId = this._id;
		let usersRights = UsersRights.findOne({
			condoId: condoId,
			userId: userId,
			defaultRoleId: roleId,
		});
		return usersRights ? false : true;
	},
})
