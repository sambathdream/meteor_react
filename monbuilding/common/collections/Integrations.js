CondoIntegrationConfig = new Mongo.Collection("condoIntegrationConfig");

/*
    _id: Document._id
    activatedOn: number > date of first integration in millis
    condoId: String > id for condo
    integrationId: String > identifying integration
    order: Number > hint for order on section
    section: String > id for section
    config: Object > integration specific config
    active: Boolian > if integration is active
*/

IntegrationConfig = new Mongo.Collection("integrationConfig");

/*
    _id: Document._id
    integrationId: String > identifying integration
    section: String > id for section hint
    config: Object > integration specific config
*/

Integrations = new Mongo.Collection("integrations");

/*
 Virtual collection
 */
