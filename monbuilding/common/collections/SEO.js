SEO = new FlowRouterSEO({
  database: true,
  databaseName: 'SEOdata'
});

let meta = {
		'name="author"': 'monBuilding & Co',
		'property="og:locale"': "fr_FR",
		'property="og:locale:alternate"': "en_US",
}

SEO.setDefaults({
	title: 'MonBuilding - Votre immeuble, en mieux.',
	description: 'Pratique & sécurisée, MonBuilding est la solution pour rester informé et communiquer dans l’immeuble : actualités, incidents, messenger, conciergerie.',
	meta: meta
});

// console.log(SEO.routes.findOne({routeName: 'app.errorAccessDenied'}));
if (Meteor.isServer) {
	SEO.routes.upsert({routeName: 'app.infos.ourvalues'}, {$set: {
		title: 'Nos valeurs',
		description: 'Garantir le respect de vos données, assurer la fiabilité des informations, ... Découvrez nos valeurs qui vous placent au centre de nos préoccupations.',
		meta: meta
	}});

	SEO.routes.upsert({routeName: 'app.infos.aboutus'}, {$set: {
		title: 'A propos de MonBuilding',
		description: 'MonBuilding c\'est avant tout l\'idée de faciliter les échanges souvent complexes et inadaptés au sein des immeubles. Découvrez l\'histoire de MonBuilding.',
		meta: meta
	}});

	SEO.routes.upsert({routeName: 'app.infos.faq'}, {$set: {
		title: 'FAQ',
		description: 'Qui peut s\'inscrire sur le site ? Comment nous contacter ? Retrouvez ici toutes les réponses à vos questions.',
		meta: meta
	}});

	SEO.routes.upsert({routeName: 'app.login.login'}, {$set: {
		title: 'Connexion',
		description: 'Connectez-vous pour accéder à la plateforme de votre immeuble et à toutes ses fonctionnalités ! Toujours pas inscrit ? Rejoignez votre immeuble dès maintenant.',
		meta: meta
	}});

	SEO.routes.upsert({routeName: 'app.login.signup'}, {$set: {
		title: 'S\'inscrire',
		description: 'Rejoignez dès maintenant la plateforme dédiée à votre immeuble (actualité de l\'immeuble, messenger occupant & gestionnaire, et d\'autres nombreux services) ! ',
		meta: meta
	}});
}
