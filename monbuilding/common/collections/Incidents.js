// import { Random } from 'meteor/random'

Incidents = new Mongo.Collection('incidents');
ResidentIncidents = new Mongo.Collection('residentIncidents');

IncidentType = new Mongo.Collection('incidentType');
IncidentDetails = new Mongo.Collection('incidentDetails');
IncidentPriority = new Mongo.Collection('incidentPriority');
IncidentArea = new Mongo.Collection('incidentArea');
IncidentTitle = new Mongo.Collection('incidentTitle');

DeclarationDetails = new Mongo.Collection('declarationDetails');
ContactManagement = new Mongo.Collection('contactManagement');
EmergencyContact = new Mongo.Collection('emergencyContact');
EmergencyContactPhotos = new Mongo.Collection('emergencyContactPhotos');

if (Meteor.isServer) {
	let resetIncidentType = false;
	let resetIncidentDetails = false;
	let resetIncidentPriority = false;
	let resetIncidentArea = false;
	let resetIncidentTitle = false;
	let resetDeclarationDetails = false;
	let resetContactManagement = false;

	if (DeclarationDetails.findOne() == undefined || resetDeclarationDetails == true) {
		if (resetDeclarationDetails == true) {
			DeclarationDetails.remove({});
		}

		DeclarationDetails.upsert({
			"key": "condoExpenses",
		},
		{
			"key": "condoExpenses",
			"isDropdownValue": true,
			"value-fr": "Montant des charges de la copropriété",
			"value-en": "Amount of expenses of the condominium",
		});

		DeclarationDetails.upsert({
			"key": "rentAmount",
		},
		{
			"key": "rentAmount",
			"isDropdownValue": true,
			"value-fr": "Montant du loyer",
			"value-en": "Rent amount",
		});

		DeclarationDetails.upsert({
			"key": "unpaid",
		},
		{
			"key": "unpaid",
			"isDropdownValue": true,
			"value-fr": "Impayé, recouvrement, contentieux",
			"value-en": "Unpaid, recovery, litigation",
		});

		DeclarationDetails.upsert({
			"key": "chargesPay",
		},
		{
			"key": "chargesPay",
			"isDropdownValue": true,
			"value-fr": "Paiement des charges",
			"value-en": "Payment of charges",
		});

		DeclarationDetails.upsert({
			"key": "rentPay",
		},
		{
			"key": "rentPay",
			"isDropdownValue": true,
			"value-fr": "Paiement du loyer",
			"value-en": "Payment of the rent",
		});

		DeclarationDetails.upsert({
			"key": "chargesRegularization",
		},
		{
			"key": "chargesRegularization",
			"isDropdownValue": true,
			"value-fr": "Régularisation des charges",
			"value-en": "Regularization of charges",
		});

		DeclarationDetails.upsert({
			"key": "rentRegularization",
		},
		{
			"key": "rentRegularization",
			"isDropdownValue": true,
			"value-fr": "Régularisation du loyer",
			"value-en": "Regularization of the rent",
		});

		DeclarationDetails.upsert({
			"key": "other",
		},
		{
			"key": "other",
			"isDropdownValue": true,
			"value-fr": "Autre",
			"value-en": "Other",
		});
		DeclarationDetails.upsert({
			"key": "other_billing",
		},
		{
			"key": "other_billing",
			"isDropdownValue": true,
			"value-fr": "Autre",
			"value-en": "Other",
		});
		DeclarationDetails.upsert({
			"key": "callFunds",
		},
		{
			"key": "callFunds",
			"isDropdownValue": true,
			"value-fr": "Appels de fonds",
			"value-en": "Calls for funds",
		});
		DeclarationDetails.upsert({
			"key": "genAssembly",
		},
		{
			"key": "genAssembly",
			"isDropdownValue": true,
			"value-fr": "Assemblée générale",
			"value-en": "General Assembly",
		});
		DeclarationDetails.upsert({
			"key": "badge",
		},
		{
			"key": "badge",
			"isDropdownValue": true,
			"value-fr": "Badge",
			"value-en": "Badge",
		});
		DeclarationDetails.upsert({
			"key": "serviceBook",
		},
		{
			"key": "serviceBook",
			"isDropdownValue": true,
			"value-fr": "Carnet d'entretien",
			"value-en": "Service book",
		});
		DeclarationDetails.upsert({
			"key": "syndicContract",
		},
		{
			"key": "syndicContract",
			"isDropdownValue": true,
			"value-fr": "Contrat de syndic",
			"value-en": "Syndic contract",
		});
		DeclarationDetails.upsert({
			"key": "devisWorks",
		},
		{
			"key": "devisWorks",
			"isDropdownValue": true,
			"value-fr": "Devis travaux",
			"value-en": "Devis works",
		});
		DeclarationDetails.upsert({
			"key": "diagnostic",
		},
		{
			"key": "diagnostic",
			"isDropdownValue": true,
			"value-fr": "Diagnostic",
			"value-en": "Diagnostic",
		});
		DeclarationDetails.upsert({
			"key": "legal",
		},
		{
			"key": "legal",
			"isDropdownValue": true,
			"value-fr": "Juridique",
			"value-en": "Legal",
		});
		DeclarationDetails.upsert({
			"key": "condoRegulation",
		},
		{
			"key": "condoRegulation",
			"isDropdownValue": true,
			"value-fr": "Règlement de copropriété",
			"value-en": "Condominium regulations",
		});
		DeclarationDetails.upsert({
			"key": "saleHousing",
		},
		{
			"key": "saleHousing",
			"isDropdownValue": true,
			"value-fr": "Vente du logement",
			"value-en": "Sale of housing",
		});
		DeclarationDetails.upsert({
			"key": "neighborhood",
		},
		{
			"key": "neighborhood",
			"isDropdownValue": true,
			"value-fr": "Voisinage",
			"value-en": "Neighborhood",
		});
		DeclarationDetails.upsert({
			"key": "homeInsurance",
		},
		{
			"key": "homeInsurance",
			"isDropdownValue": true,
			"value-fr": "Assurance habitation",
			"value-en": "Home Insurance",
		});
		DeclarationDetails.upsert({
			"key": "managerWAccount",
		},
		{
			"key": "managerWAccount",
			"isDropdownValue": false,
			"value-fr": "Gestionnaire avec compte",
			"value-en": "Manager with account",
		});
		DeclarationDetails.upsert({
			"key": "edl",
		},
		{
			"key": "edl",
			"isDropdownValue": false,
			"value-fr": "Etat des lieux",
			"value-en": "State of play",
		});
		DeclarationDetails.upsert({
			"key": "validEntry",
		},
		{
			"key": "validEntry",
			"isDropdownValue": false,
			"value-fr": "Validation des inscriptions",
			"value-en": "Registration validation",
		});
		DeclarationDetails.upsert({
			"key": "incident",
		},
		{
			"key": "incident",
			"isDropdownValue": false,
			"value-fr": "Incidents",
			"value-en": "Incidents",
		});
		DeclarationDetails.upsert({
			"key": "defaultContact",
		},
		{
			"key": "defaultContact",
			"isDropdownValue": false,
			"value-fr": "Contact par defaut",
			"value-en": "Contact par défaut",
		});
	}

	if (IncidentType.find().fetch()[0] == undefined || resetIncidentType == true) {
		if (resetIncidentType == true) {
			IncidentType.remove({});
		}
		IncidentType.upsert({
			condoId: "default",
			'defaultType': 'incident',
		},
		{
			condoId: "default",
			'defaultType': 'incident',
			"value-fr": "Incident",
			"value-en": "Incident",
			"picto": "/img/icons/message-1.png"
		});
		IncidentType.upsert({
			condoId: "default",
			'defaultType': 'billing',
		},
		{
			condoId: "default",
			'defaultType': 'billing',
			"value-fr": "Facturation / Comptabilité",
			"value-en": "Billing / Accounting",
			"picto": "/img/icons/message-2.png"
		});
		IncidentType.upsert({
			condoId: "default",
			'defaultType': 'other',
		},
		{
			condoId: "default",
			'defaultType': 'other',
			"value-fr": "Autre Demande",
			"value-en": "Other inquiry",
			"picto": "/img/icons/message-3.png"
		});
		console.log("Incident Type default inserted");
	}

	if (ContactManagement.findOne() == undefined || resetContactManagement == true) {
		if (resetContactManagement == true) {
			ContactManagement.remove({});
		}

		ContactManagement.upsert({
			condoId: "default",
			forCondoType: "copro"
		},
		{
			condoId: "default",
			forCondoType: "copro",
			messengerSet: [
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "incident"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "incident"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "billing"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "condoExpenses"})._id,
						DeclarationDetails.findOne({"key": "chargesPay"})._id,
						DeclarationDetails.findOne({"key": "chargesRegularization"})._id,
						DeclarationDetails.findOne({"key": "unpaid"})._id,
						DeclarationDetails.findOne({"key": "other_billing"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "other"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "callFunds"})._id,
						DeclarationDetails.findOne({"key": "genAssembly"})._id,
						DeclarationDetails.findOne({"key": "serviceBook"})._id,
						DeclarationDetails.findOne({"key": "syndicContract"})._id,
						DeclarationDetails.findOne({"key": "devisWorks"})._id,
						DeclarationDetails.findOne({"key": "diagnostic"})._id,
						DeclarationDetails.findOne({"key": "legal"})._id,
						DeclarationDetails.findOne({"key": "condoRegulation"})._id,
						DeclarationDetails.findOne({"key": "saleHousing"})._id,
						DeclarationDetails.findOne({"key": "badge"})._id,
						DeclarationDetails.findOne({"key": "neighborhood"})._id,
						DeclarationDetails.findOne({"key": "other"})._id,
					]
				}
			]

		});

		ContactManagement.upsert({
			condoId: "default",
			forCondoType: "mono"
		},
		{
			condoId: "default",
			forCondoType: "mono",
			messengerSet: [
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "incident"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "incident"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "billing"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "rentAmount"})._id,
						DeclarationDetails.findOne({"key": "rentPay"})._id,
						DeclarationDetails.findOne({"key": "rentRegularization"})._id,
						DeclarationDetails.findOne({"key": "unpaid"})._id,
						DeclarationDetails.findOne({"key": "other_billing"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "other"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "homeInsurance"})._id,
						DeclarationDetails.findOne({"key": "badge"})._id,
						DeclarationDetails.findOne({"key": "neighborhood"})._id,
						DeclarationDetails.findOne({"key": "other"})._id,
					]
				}
			]

		});

		ContactManagement.upsert({
			condoId: "default",
			forCondoType: "etudiante"
		},
		{
			condoId: "default",
			forCondoType: "etudiante",
			messengerSet: [
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "incident"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "incident"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "billing"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "rentAmount"})._id,
						DeclarationDetails.findOne({"key": "rentPay"})._id,
						DeclarationDetails.findOne({"key": "rentRegularization"})._id,
						DeclarationDetails.findOne({"key": "unpaid"})._id,
						DeclarationDetails.findOne({"key": "other_billing"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "other"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "homeInsurance"})._id,
						DeclarationDetails.findOne({"key": "badge"})._id,
						DeclarationDetails.findOne({"key": "neighborhood"})._id,
						DeclarationDetails.findOne({"key": "other"})._id,
					]
				}
			]

		});

		ContactManagement.upsert({
			condoId: "default",
			forCondoType: "office"
		},
		{
			condoId: "default",
			forCondoType: "office",
			messengerSet: [
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "incident"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "incident"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "billing"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "condoExpenses"})._id,
						DeclarationDetails.findOne({"key": "chargesPay"})._id,
						DeclarationDetails.findOne({"key": "chargesRegularization"})._id,
						DeclarationDetails.findOne({"key": "unpaid"})._id,
						DeclarationDetails.findOne({"key": "other_billing"})._id,
					]
				},
				{
					id: IncidentType.findOne({"condoId": "default", "defaultType": "other"})._id,
					declarationDetailIds: [
						DeclarationDetails.findOne({"key": "callFunds"})._id,
						DeclarationDetails.findOne({"key": "genAssembly"})._id,
						DeclarationDetails.findOne({"key": "serviceBook"})._id,
						DeclarationDetails.findOne({"key": "syndicContract"})._id,
						DeclarationDetails.findOne({"key": "devisWorks"})._id,
						DeclarationDetails.findOne({"key": "diagnostic"})._id,
						DeclarationDetails.findOne({"key": "legal"})._id,
						DeclarationDetails.findOne({"key": "condoRegulation"})._id,
						DeclarationDetails.findOne({"key": "saleHousing"})._id,
						DeclarationDetails.findOne({"key": "badge"})._id,
						DeclarationDetails.findOne({"key": "neighborhood"})._id,
						DeclarationDetails.findOne({"key": "other"})._id,
					]
				}
			]

		});
	}



	if (IncidentDetails.find().fetch()[0] == undefined || resetIncidentDetails == true) {
		if (resetIncidentDetails == true) {
			IncidentDetails.remove({});
		}
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'electricity',
		},
		{
			condoId: "default",
			'defaultDetail': 'electricity',
			"value-fr": "Electricité",
			"value-en": "Electricity",
			"picto": "/img/icons/details-1.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'plumbing',
		},
		{
			condoId: "default",
			'defaultDetail': 'plumbing',
			"value-fr": "Plomberie / Fuite d'eau",
			"value-en": "Plumbing / Water leak",
			"picto": "/img/icons/details-2.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'Chauffage',
		},
		{
			condoId: "default",
			'defaultDetail': 'heating',
			"value-fr": "Chauffage",
			"value-en": "Heating",
			"picto": "/img/icons/details-3.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'elevator',
		},
		{
			condoId: "default",
			'defaultDetail': 'elevator',
			"value-fr": "Ascenseur",
			"value-en": "Elevator",
			"picto": "/img/icons/details-4.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'water',
		},
		{
			condoId: "default",
			'defaultDetail': 'water',
			"value-fr": "Eau",
			"value-en": "Water",
			"picto": "/img/icons/details-5.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'greenSpace',
		},
		{
			condoId: "default",
			'defaultDetail': 'greenSpace',
			"value-fr": "Espace vert",
			"value-en": "Green space",
			"picto": "/img/icons/details-6.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'cleaning',
		},
		{
			condoId: "default",
			'defaultDetail': 'cleaning',
			"value-fr": "Nettoyage",
			"value-en": "Cleaning",
			"picto": "/img/icons/details-7.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'door',
		},
		{
			condoId: "default",
			'defaultDetail': 'door',
			"value-fr": "Porte",
			"value-en": "Door",
			"picto": "/img/icons/details-8.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'parking',
		},
		{
			condoId: "default",
			'defaultDetail': 'parking',
			"value-fr": "Parking",
			"value-en": "Parking",
			"picto": "/img/icons/details-9.png"
		});
		IncidentDetails.upsert(
		{
			condoId: "default",
			'defaultDetail': 'other',
		},
		{
			condoId: "default",
			'defaultDetail': 'other',
			"value-fr": "Autre",
			"value-en": "Other",
			"picto": "/img/icons/details-10.png"
		});
		console.log("Incident Details default inserted");
	}

	if (IncidentPriority.find().fetch()[0] == undefined || resetIncidentPriority == true) {
		if (resetIncidentPriority == true) {
			IncidentPriority.remove({});
		}
		IncidentPriority.upsert(
		{
			condoId: "default",
			'defaultPriority': 'no-urgent',
		},
		{
			condoId: "default",
			'defaultPriority': 'no-urgent',
			"value-fr": "Non Urgent",
			"value-en": "Not Urgent",
			"picto": "/img/icons/priorite-1.png"
		});
		IncidentPriority.upsert(
		{
			condoId: "default",
			'defaultPriority': 'urgent',
		},
		{
			condoId: "default",
			'defaultPriority': 'urgent',
			"value-fr": "Urgent",
			"value-en": "Urgent",
			"picto": "/img/icons/priorite-2.png"
		});
		console.log("Incident Priority default inserted");
	}

	if (IncidentArea.find().fetch()[0] == undefined || resetIncidentArea == true) {
		if (resetIncidentArea == true) {
			IncidentArea.remove({});
		}
		IncidentArea.upsert(
		{
			condoId: "default",
			'defaultArea': 'common',
		},
		{
			condoId: "default",
			'defaultArea': 'common',
			"value-fr": "Parties communes",
			"value-en": "Common areas",
			"picto": "/img/icons/zone-1.png"
		});
		IncidentArea.upsert(
		{
			condoId: "default",
			'defaultArea': 'private',
		},
		{
			condoId: "default",
			'defaultArea': 'private',
			"value-fr": "Parties privatives",
			"value-en": "Private areas",
			"picto": "/img/icons/zone-2.png"
		});
		console.log("Incident Area default inserted");
	}
	if (IncidentTitle.find().fetch()[0] == undefined || resetIncidentTitle == true) {
		if (resetIncidentTitle == true) {
			IncidentTitle.remove({});
		}
		IncidentTitle.insert(
		{
			0: 'Incident validé',
			1: 'Incident déclaré par un occupant',
			2: 'Incident déclaré par le gestionnaire',
			3: 'Note',
			4: 'Devis',
			5: 'Ordre de service envoyé',
			6: 'Intervention programmée',
			7: 'Incident résolu',
			9: 'Incident privé',
			10: 'Incident réouvert',
			11: 'Intervention annulée'
		});
		console.log("Incident Titles default inserted");
	}
}
