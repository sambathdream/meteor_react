ResourceType = new Mongo.Collection("resourceType");
ResourceServices = new Mongo.Collection("resourceServices");
Schools = new Mongo.Collection("schools");
Reservations = new Mongo.Collection("reservations");
Resources = new Mongo.Collection("resources");
ViewOfReservation = new Mongo.Collection("ViewOfReservation");

if (Meteor.isServer) {
    // delete book for deleted resources
    const resources = Resources.find().fetch();
    const resourceIds = _.map(resources, r => r._id);
    Reservations.remove({resourceId: { $nin: resourceIds }});
    const deletedResourceBooks = Reservations.find({resourceId: { $nin: resourceIds }}).fetch();

    // update stucked book
    const stuckBook = Reservations.find({needValidation: false, pending: true}).fetch();
    stuckBook.forEach((b) => {
      Reservations.update(b._id, {$set: {pending: false}});
    })

    // feed resource services default data
    ResourceServices.remove({});
    if (ResourceServices.findOne() === undefined) {
        const data = [{
            "key": "catering",
            "value-fr": "Traiteur - Restauration",
            "value-en": "Catering",
            "children": [{
                "key": "orange-juice",
                "value-fr": "Jus d'orange",
                "value-en": "Orange Juice"
            }, {
                "key": "tea",
                "value-fr": "Thé",
                "value-en": "Tea"
            }, {
                "key": "finger-food",
                "value-fr": "Gateaux apéritifs",
                "value-en": "Fingers Food"
            }, {
                "key": "breakfast",
                "value-fr": "Petit déjeuner",
                "value-en": "Breakfast"
            }, {
                "key": "lunch",
                "value-fr": "Déjeuner",
                "value-en": "Lunch"
            }, {
                "key": "dinner",
                "value-fr": "Diner",
                "value-en": "Dinner"
            }]
        }, {
            "key": "equipment",
            "value-fr": "Equipement",
            "value-en": "Equipment",
            "children": [{
                "key": "projector",
                "value-fr": "Retroprojecteur",
                "value-en": "Overhead projector"
            }, {
                "key": "hdmi-cable",
                "value-fr": "Cable HDMI",
                "value-en": "HDML cable"
            }, {
                "key": "vga-cable",
                "value-fr": "Cable VGA",
                "value-en": "VGA cable"
            }, {
                "key": "additional-chair",
                "value-fr": "Chaises additionnelles",
                "value-en": "Additional chairs"
            }]
        }
        // , {
        //     "key": "extra",
        //     "value-fr": "Extras",
        //     "value-en": "Extras",
        //     "children": []
        // }
        ]

        _.each(data, (d) => {
            ResourceServices.insert(d)
        })
    }
}
