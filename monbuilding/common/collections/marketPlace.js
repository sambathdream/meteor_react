import { Mongo } from 'meteor/mongo'

export const MarketServices = new Mongo.Collection('marketServices')
export const ReservedSlots = new Mongo.Collection('reservedSlots')
export const MarketServiceTypes = new Mongo.Collection('marketServiceTypes')
export const MarketBasket = new Mongo.Collection('marketBasket')
export const MarketReservations = new Mongo.Collection('marketReservations')
export const TermsOfServices = new Mongo.Collection('termsOfServices')
