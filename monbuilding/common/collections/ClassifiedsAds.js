ClassifiedsAds = new Mongo.Collection('ClassifiedsAds');

ClassifiedsAds.helpers({
	user() {
		return Meteor.users.findOne(this.userId);
	},
	replies() {
		return ClassifiedsAds.findOne(this._id).message;
	},
	isFollowing() {
		const userId = Meteor.userId();
		return (this.followers.indexOf(userId) >= 0);
	},
	classifieds() {
		return Classifieds.findOne(this.classifiedsId);
	},
});

ClassifiedsFeed = new Mongo.Collection("classifiedsFeed")
if (Meteor.isClient) {

	ClassifiedsFeedCounter = new Mongo.Collection("classifiedsFeedCounter")
}
