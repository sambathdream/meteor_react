if (Meteor.isClient) {
    FeedNewPostsCounter = new Mongo.Collection('feedNewPostsCounter')
    RecentlyJoinedUsers = new Mongo.Collection('recentlyJoinedUsers')
    ClassifiedViewsCounter = new Mongo.Collection('classifiedViewsCounter')
    ForumNewPost = new Mongo.Collection('forumNewPost')
    ForumViewsCounter = new Mongo.Collection('forumViewsCounter')
    ActuCounter = new Mongo.Collection('actuCounter')
}
