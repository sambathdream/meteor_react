import { Mongo } from 'meteor/mongo'

export const LaundryRegister = new Mongo.Collection('laundryRegister')
export const LaundryRoom = new Mongo.Collection('laundryRoom')
export const LaundrySettings = new Mongo.Collection('laundryRoomSettings')

export const LaundryBoardCollection = new Mongo.Collection(null)
