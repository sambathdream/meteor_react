Buildings = new Mongo.Collection("buildings");

Buildings.helpers({
  getCondo() {
    return Condos.findOne(this.condoId);
  },
  getName() {
    if (this.name && this.name != "" && (this.info.address && this.name != this.info.address))
      return this.name;
    else
      return this.info.address + ", " + this.info.code + " " + this.info.city;
  },
  getResidentModules() {
    return this.modules;
  },
  residents() {
    return residents = Residents.find({
      "condos.condoId": this.condoId,
    }).fetch();
  }
});

