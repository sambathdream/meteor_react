Classifieds = new Mongo.Collection('classifieds');

Classifieds.helpers({
  ads() {
    return ClassifiedsAds.find({ classifiedsId: this._id });
  },
  isFollowing() {
    const userId = Meteor.userId();
    return (this.unfollowers.indexOf(userId) === -1);
  },
  condo() {
    return Condos.findOne({
      modules: { $elemMatch: { name: "classifieds", "data.classifiedsId": this._id } }
    });
  },
  building() {
    return Buildings.findOne({
      modules: { $elemMatch: { name: "classifieds", "data.classifiedsId": this._id } }
    });
  }


});
