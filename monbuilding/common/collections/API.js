export const payzenApiId = 'payZenIntegration'
export const wiLineApiId = 'wiLineIntegration'
export const assaAbloyApiId = 'assaAbloyIntegration'

const crypto = require('crypto')

function sha1(data, cert) {
  var generator = crypto.createHash('sha1')
  generator.update(data)
  return generator.digest('hex')
}

export function updateSignaturePayzen(formData, cert) {
  // const formData = t.formData.all()
  if (cert === null) {
    const datas = IntegrationConfig.findOne()
    if (datas) {
      cert = datas.testCert
    }
  }

  let signature = ''

  Object.keys(formData).sort().forEach(function (key) {
    if (key.substring(0, 5) === 'vads_') {
      signature += formData[key] + '+'
    }
  })
  signature += cert
  signature = sha1(signature)

  return signature
}

if (Meteor.isClient) {
  const hostname = window.location.hostname

  export let payzenCallBack = null
  if (hostname === 'localhost') {
    payzenCallBack = 'https://2adf30e7.eu.ngrok.io/api/payzen/check'
  } else {
    payzenCallBack = Meteor.absoluteUrl('api/payzen/check')
  }
}
