ActuPosts = new Mongo.Collection('ActuPosts');
cronActuPosts = new Mongo.Collection('cronActuPosts');
Actus = new Mongo.Collection("actus");

if (Meteor.isServer) {
  let condos = Condos.find().fetch()

  EventCommentsFeed = [];

  _.each(condos, function(condo) {
    name = "EventCommentsFeed" + condo._id;
    EventCommentsFeed[condo._id] = new Mongo.Collection(name);
  })
}

if (Meteor.isClient) {
  EventCommentsFeed = new Mongo.Collection("eventCommentsFeed")
  EventCommentsFeedCounter = new Mongo.Collection("eventCommentsFeedCounter")
}
