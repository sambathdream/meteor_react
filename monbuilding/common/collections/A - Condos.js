Condos = new Mongo.Collection("condos");
ViewCondoDocument = new Mongo.Collection("viewCondoDocument");
SigninCondos = new Mongo.Collection("SigninCondos");
SigninBuildings = new Mongo.Collection("SigninBuildings");
CondosListBackoffice = new Mongo.Collection("CondosListBackoffice");
DefaultCondo = new Mongo.Collection("defaultCondo");
Avatars = new Mongo.Collection("avatars");
PhoneCode = new Mongo.Collection('phone-code');
CondoContact = new Mongo.Collection('condoContact');
CondosModulesOptions = new Mongo.Collection('condosModulesOptions');
BuildingStatus = new Mongo.Collection('buildingStatus');
CondoPhotos = new Mongo.Collection('condoPhotos');
CondoLogos = new Mongo.Collection('condoLogos');

CompanyName = new Mongo.Collection('companyName')

if(Meteor.isServer) {
  IsTyping = new Meteor.Collection('IsTyping', { connection: null })
}

if(Meteor.isClient) {
  IsTyping = new Meteor.Collection('IsTyping')
}

Condos.helpers({
  residents () {
    return Residents.find({
      'condos.condoId': this._id
    }).fetch()
  },
  getName () {
    if (this.name && this.name !== '' && (this.info.address && this.name !== this.info.address)) {
      return this.name
    } else {
      return this.info.address + ', ' + this.info.code + ' ' + this.info.city
    }
  },
  getNameOrDash () {
    if (this.name && this.name != "" && (this.info.address && this.name != this.info.address))
      return this.name;
    else
      return '-'
  },
  getNameWithId () {
    let condoId = ''
    if (this.info.id !== '-1') {
      condoId = this.info.id + ' - '
    }
    if (this.name && this.name !== '' && (this.info.address && this.name !== this.info.address)) {
      return condoId + this.name
    } else {
      return condoId + this.info.address + ', ' + this.info.code + ' ' + this.info.city
    }
  },
  getNameWithAddress () {
    if (this.name && this.name !== '' && (this.info.address && this.name !== this.info.address)) {
      return this.name + ' - ' + this.info.address + ', ' + this.info.code + ' ' + this.info.city
    } else {
      return this.info.address + ', ' + this.info.code + ' ' + this.info.city
    }
  },
  getAddress () {
    return this.info.address + ', ' + this.info.code + ' ' + this.info.city
  },
  getResidentWithRoleId (roleId) {
    let allUserWithRole = UsersRights.find({ condoId: this._id, defaultRoleId: roleId }).fetch()
    if (allUserWithRole) {
      return Residents.find({
        userId: { $in: _.map(allUserWithRole, function (user) { return user.userId }) }
      }).fetch()
    }
  },
  hasResidentWithRoleId (roleId) {
    let allUserWithRole = UsersRights.find({ condoId: this._id, defaultRoleId: roleId }, { field: { _id: 1 } }).count()
    return allUserWithRole > 0
  },
  getRolesIdInCondo () {
    let rolesIds = UsersRights.find({ condoId: this._id }, { field: { defaultRoleId: 1 } })
    return _.unique(rolesIds.map(role => role.defaultRoleId))
  },
  getLogo () {
    let avatar = CondoLogos.findOne({ condoId: this._id })
    if (avatar && avatar.avatar) {
      return avatar.avatar
    }
    return false
  },
});

if (Meteor.isServer) {
  let resetCompanyName = false

  if (CompanyName.findOne() === undefined || resetCompanyName === true) {
    if (resetCompanyName === true) {
      CompanyName.remove({})
    }

    CompanyName.insert({ name: 'Google' })
    CompanyName.insert({ name: 'Facebook' })
    CompanyName.insert({ name: 'Starbucks' })
    CompanyName.insert({ name: 'Coca Cola' })
    CompanyName.insert({ name: 'Ubisoft' })
    CompanyName.insert({ name: 'Apple' })
    CompanyName.insert({ name: 'Renault' })
    CompanyName.insert({ name: 'Georges' })
    CompanyName.insert({ name: 'Pasquier' })
    CompanyName.insert({ name: 'HP' })
    CompanyName.insert({ name: 'Microsoft' })
    CompanyName.insert({ name: 'LG' })
    CompanyName.insert({ name: 'Samsung' })
  }

  let resetBuildingStatus = false

  if (BuildingStatus.findOne() === undefined || resetBuildingStatus === true) {
    if (resetBuildingStatus === true) {
      BuildingStatus.remove({})
    }

    BuildingStatus.upsert({
      'key': 'owner'
    }, {
      'key': 'owner',
      'value-fr': 'propriétaire',
      'value-en': 'owner'
    })
    console.log('BuildingStatus => owner inserted')
    BuildingStatus.upsert({
      'key': 'tenant'
    }, {
      'key': 'tenant',
      'value-fr': 'locataire',
      'value-en': 'tenant'
    })
    console.log('BuildingStatus => tenant inserted')
    BuildingStatus.upsert({
      'key': 'user'
    }, {
      'key': 'user',
      'value-fr': 'utilisateur',
      'value-en': 'user'
    })
    console.log('BuildingStatus => occupant inserted')
  }
}
