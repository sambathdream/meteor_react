const Entities = require('html-entities').XmlEntities;
import moment from 'moment'
import { FlowRouter } from 'meteor/kadira:flow-router'
import * as translate from "/common/lang/lang.js"

var PNF = require('google-libphonenumber').PhoneNumberFormat
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance()

Template.registerHelper('condoHasAccountingOption', (condoId, option, isBuildingOption) => {
  const thisCondo = Condos.findOne({ _id: condoId })
  const settings = thisCondo && thisCondo.settings
  return settings && settings.accounting && settings.accounting[option] && (isBuildingOption ? settings.accounting[option].isActive : true)
})

Template.registerHelper('toLower', (str) => {
  return str && str.toLowerCase()
})

Template.registerHelper('getTimeOrDash', (time, isParsed = false) => {
  return Meteor.getTimeOrDash(time, isParsed)
})

Template.registerHelper('getTotalPrice', (price, quantity) => {
  return price * quantity
})

Template.registerHelper('isPriceNotNull', (price = null) => {
  return price && price !== '0' && price !== 0
})

Template.registerHelper('hasStock', (unlimited_quantity, quantity_available) => {
  return unlimited_quantity || !!parseInt(quantity_available)
})

Template.registerHelper('getPrice', (isThirdParty, value, removeVat = false, vat, serviceIncluded = true) => {
  if (serviceIncluded && (value === 0 || value === '0')) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr = new translate.CommonTranslation(lang)
    return isThirdParty ? tr.commonTranslation.free : tr.commonTranslation.service_included
  } else if (!value && serviceIncluded) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr = new translate.CommonTranslation(lang)
    return isThirdParty ? '' : tr.commonTranslation.service_included
  } else {
    if (removeVat === true) {
      const vatNum = 1 + (parseFloat(vat) / 100)
      const valueWithoutVat = value / vatNum
      return valueWithoutVat.toFixed(2) + '€'
    } else {
      return parseFloat(value).toFixed(2) + '€'
    }
  }
})

Template.registerHelper('getVatPrice', (value, vat) => {
  if (!value) {
    return '0€'
  } else {
    const vatNum = 1 + (parseFloat(vat) / 100)
    const valueWithoutVat = value / vatNum
    return (value - valueWithoutVat).toFixed(2) + '€'
  }
})

Template.registerHelper('getDurationText', (key) => {
  if (!!key) {
    const lang = FlowRouter.getParam('lang') || 'fr'
    const tr = new translate.CommonTranslation(lang)
    const options = {
      '60': tr.commonTranslation.hour,
      '30': '30 ' + tr.commonTranslation.minute + 's'
    }

    return !!options[key]? options[key]: ''
  } else {
    return ''
  }
})

Template.registerHelper('hyperlink', (text) => {
  return autoLink(text)
})

Template.registerHelper('getDomainName', () => {
  return domainConfig.name
})

Template.registerHelper('restrictToLoginPage', () => {
  return domainConfig.restrictToLoginPage
})

Template.registerHelper('getEnterpriseName', (condoId) => {
  const enterprise = Enterprises.findOne({condos: condoId})
  if (enterprise) {
    return enterprise.name
  }
  return ''
})

Template.registerHelper('_IsManagerView', () => {
	return Meteor.isManager || false;
});

Template.registerHelper('condoHasModule', (condoModuleName, condoId) => {
  return Meteor.condoHasModule(condoModuleName, condoId)
});

Template.registerHelper('displayNumberCard', (number) => {
  return '****' + number.slice(-4)
});

Template.registerHelper('getCompanyProfile', (companyId) => {
  if (companyId === '-') {
    return '-'
  }
  let company = CompanyName.findOne({_id: companyId})
  if (company) {
    return company.name
  } else {
    return '-'
  }
})

Template.registerHelper('shouldDisplayDevElement', () => {
  return Meteor.isDevelopment || window.location.origin !== 'https://monbuilding.com'
})

Template.registerHelper('getCompany', () => {
  let company = CompanyName.find().fetch()
  return company
})

Template.registerHelper('isNotFalse', (value) => {
  return value !== false
})

Template.registerHelper('getRoleRightVal', (right) => {
  let lang = FlowRouter.getParam('lang') || 'fr'
  return right['value-' + lang]
})

Template.registerHelper('condoModulesOptions', (condoId, module, option, all) => {
  let condoModuleOptions = CondosModulesOptions.findOne({ condoId: condoId, [module]: { $exists: true } });

	if (condoModuleOptions) {
		if (option == "one") {
			let atLeastOne = false;
			atLeastOne = _.find(condoModuleOptions[module], function(value) { return value }) == undefined ? false : true;

			return atLeastOne;
		}
		else if (!_.isArray(option)) {
			return condoModuleOptions[module][option];
		}
		else if (all != undefined && all != null && _.isBoolean(all) && all == true) {
			isAllTrue = true;
			_.each(condoModuleOptions[module], function(value, key) {
				if (_.contains(option, key) && value == false)
					isAllTrue = false;
			})
			return isAllTrue
		}
		else if (all != undefined && all != null && _.isBoolean(all) && all == false) {
			isOneTrue = false;
			_.each(condoModuleOptions[module], function(value, key) {
				if (_.contains(option, key) && value == true)
					isOneTrue = true;
			})
			return isOneTrue
		}
	}
	else
		return false;
});

Template.registerHelper('displayTitle',  (title, description) => {
	if (title != undefined && title && title != "" && title.length > 0)
		return title;
	else if (description != undefined && description && description != "" && description.length > 0) {
		if (description.length > 50)
			return description.substring(0,50) + "...";
		else
			return description
	}
});

Template.registerHelper('GetAllPhoneCode', () => {
  const lang = FlowRouter.getParam('lang') || 'fr'
  if (lang === 'fr') {
		console.log(PhoneCode.find({}, {sort: {country: 1}}).fetch())
    return PhoneCode.find({}, {sort: {country: 1}}).fetch()
  } else {
    return PhoneCode.find({}, { sort: { ['country_' + lang]: 1 } }).fetch()
  }
});

Template.registerHelper('parsePhoneCode', (allPhoneCode) => {
	const lang = FlowRouter.getParam('lang') || 'fr'
  return _.map(allPhoneCode, (phoneCode) => {
    return {
      text: lang === 'fr' ? phoneCode.country : phoneCode['country_' + lang],
      id: phoneCode.code
    }
  })
})

Template.registerHelper('getSelectedCode', (elem, type) => {
	if (!elem)
		return 33;
	if (type == "mobile" && elem.PhoneCode2 && elem.PhoneCode2 != "")
		return elem.tel2Code;
	else if (type == "landline" && elem.telCode && elem.telCode != "")
		return elem.telCode;
	else
		return 33;
});

Template.registerHelper('sameCode', (code1, code2) => {
	return code1 == code2
})



Template.registerHelper('getBuildingStatusValueBlaze', (key, upperCase, lang) => {
	return Meteor.getBuildingStatusValue(key, upperCase, lang);
});

Template.registerHelper('notEmpty', (value) => {
	if (value == undefined || !value || value == "")
		return false;
	return true;
})

Template.registerHelper('getSatusValue', (status, lang, upperCase) => {
	if (lang == undefined || lang == null)
		lang = (FlowRouter.getParam("lang") || "fr");
	let value = status["value-" + lang];
	if (upperCase == true) {
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
	return value;
});

Template.registerHelper('getCustomRole', (shared, type) => {
	let res = [];
	_.each(shared, function(elem, index) {
		if (index != "all" && index != "custom" && index != "declarer" && index != "intern") {
			if (type == "visible" && elem.state == true) {
				if (index == "syndic")
					res.push(DefaultRoles.findOne({name: "Conseil Syndical"}));
				else
					res.push(DefaultRoles.findOne(index));
			}
			else if (type == "comment" && elem.comment != "") {
				if (index == "syndic")
					res.push(DefaultRoles.findOne({name: "Conseil Syndical"}));
				else
					res.push(DefaultRoles.findOne(index));
			}
		}
	});
	res = _.without(res, undefined, null);
	if (res != undefined && res)
		return _.pluck(res, 'name');
});

Template.registerHelper('getCustomRoleId', (shared, type) => {
	let res = [];
	_.each(shared, function(elem, index) {
		if (index != "all" && index != "custom" && index != "declarer" && index != "intern") {
			if (type == "visible" && elem.state == true) {
				if (index == "syndic")
					res.push(DefaultRoles.findOne({name: "Conseil Syndical"}));
				else
					res.push(DefaultRoles.findOne(index));
			}
			else if (type == "comment" && elem.comment != "") {
				if (index == "syndic")
					res.push(DefaultRoles.findOne({name: "Conseil Syndical"}));
				else
					res.push(DefaultRoles.findOne(index));
			}
		}
	});
	if (res)
		return _.pluck(res, '_id');
});

Template.registerHelper('getRoleNameForUser', (userId, condoId) => {
	let roleId = UsersRights.findOne({userId: userId, condoId: condoId});
	if (!roleId)
		return "-";
	roleId = roleId.defaultRoleId;
	return DefaultRoles.findOne(roleId).name;
})

Template.registerHelper('getRoleName', (roleId) => {
	return DefaultRoles.findOne(roleId).name;
});


// MARVELOUS IDEA BY ALEX DANA AKA ALEXOU <3 \\
Template.registerHelper('or', (arg1, arg2) => {
	return arg1 || arg2;
});

Template.registerHelper('and', (arg1, arg2) => {
	return arg1 && arg2;
});
// END OF MARVELOUS IDEA \\

Template.registerHelper('reverseBool', (bool) => {
  return !bool;
});
Template.registerHelper('toBool', (bool) => {
  return !!bool;
});

// MARVELOUS IDEA BY VIC AKA VCASTRO- <3 \\
Template.registerHelper('orArray', (array) => {
	return _.contains(array, true);
});

Template.registerHelper('andArray', (array) => {
	return _.find(array, function(bool) { return !bool }) ? true : false;
});

Template.registerHelper('exists', (item) => {
	return item && item != undefined && !_.isEmpty(item);
});

Template.registerHelper('notExists', (item) => {
	return !item || item == undefined || _.isEmpty(item);
});

Template.registerHelper('array', function(){
	return Array.from(arguments).slice(0, arguments.length-1);
});
// END OF MARVELOUS IDEA \\

Template.registerHelper('logger', (elem) => {
	console.log(elem);
});

Template.registerHelper('logger2', (elem, elem2) => {
	console.log(`${elem} => ${elem2}`)
});



Template.registerHelper('displayDate33', function(date) {
	let lang = (FlowRouter.getParam("lang") || "fr");

	if (date == moment().locale('fr').format('L')) {
		return new translate.ModuleMessagerieIndex(lang).moduleMessagerieIndex['today'];
	}
	else if (moment().diff(moment(date, 'DD[/]MM[/]YYYY'), 'days') == 1){
    return new translate.ModuleMessagerieIndex(lang).moduleMessagerieIndex['yesterday'];
	}
	else {
		return moment(date, 'DD[/]MM[/]YYYY').locale(lang).format('LL');
	}
});

Template.registerHelper('isDatePassed', (date) => {
	return (moment(new Date(date)).diff(Date.now()) > 1 ? true : false);
});

Template.registerHelper('humanDateFromNow', (date) => {
	return moment(new Date(date)).fromNow();
});

Template.registerHelper('humanDate', (date) => {
	let lang = (FlowRouter.getParam("lang") || "fr");
  let at = new translate.Email(lang).email['_at_'];
	return moment(new Date(date)).format("DD/MM/YYYY[" + at + "]HH:mm");
});
Template.registerHelper('humanUtcDate', (date) => {
	let lang = (FlowRouter.getParam("lang") || "fr");
  let at = new translate.Email(lang).email['_at_'];
	return moment.utc(date).format("DD/MM/YYYY[" + at + "]HH:mm");
});

Template.registerHelper('humanTime', (date) => {
	return moment(new Date(date)).format("HH:mm");
});

Template.registerHelper('humanUtcTime', (date) => {
	return moment.utc(new Date(date)).format("HH:mm");
});

Template.registerHelper('humanDateShort', (date) => {
  if (date === '-') {
    return date
  }
	return moment(new Date(date)).format("DD/MM/YYYY");
});
Template.registerHelper('humanDateShortNow', () => {
	return moment().format("DD/MM/YYYY");
});
Template.registerHelper('humanUtcDateShort', (date) => {
	return moment.utc(new Date(date)).format("DD/MM/YYYY");
});

Template.registerHelper('me', () => {
	return Meteor.user();
});

Template.registerHelper('isInArray', (array, elem) => {
	return array.indexOf(elem) != -1;
});

Template.registerHelper('concatStr', (...args) => {

	args.splice(-1, 1);
	return "".concat(...args);
});

Template.registerHelper('dateEn', (dateFr) => {
	dateFr = dateFr.split('/');
	return `${dateFr[1]}/${dateFr[0]}/${dateFr[2]}`;
});

Template.registerHelper('getAddress', (condoId) => {
	let c;
	if (condoId)
		c = Condos.findOne(condoId)
	else if (FlowRouter.getParam("condo_id"))
		c = Condos.findOne(FlowRouter.getParam("condo_id"));
	else if (FlowRouter.getParam("condoId"))
		c = Condos.findOne(FlowRouter.getParam("condoId"));
	if (c)
		return c.getName();
});

Template.registerHelper('hasSelected', (c1, c2) => {
	return c1 === c2 ? "selected" : "";
});

Template.registerHelper('hasChecked', (c1, c2) => {
	return c1 === c2 ? "checked" : "";
});

Template.registerHelper('hasDisabled', (c1, c2) => {
	return c1 === c2 ? "disabled" : "";
});

Template.registerHelper('getOuiNon', (bool) => {
	return bool ? "Oui" : "Non";
});

Template.registerHelper("intelligentDate", (date) => {
	let newDate = new Date(date);
	let nowDate = new Date();
	let timediff = nowDate.getTime() - newDate.getTime();
	moment.locale(FlowRouter.getParam("lang") || "fr");
	if ((timediff / (1000 * 3600 * 24)) < 1 && newDate.getDate() === nowDate.getDate())
		return moment(newDate).format("HH:mm");
	if ((timediff / (1000 * 3600 * 24)) < 7)
		return moment(newDate).format("dddd");
	return moment(newDate).format("DD/MM/YY");
});

Template.registerHelper("postedDate", (postedDate) => {
	return new Date(postedDate).toString().split(' ')[2];
});

Template.registerHelper("postedDayDate", (postedDate) => {
	let listMonth = ["JAN", "FEV", "MAR", "AVR", "MAI", "JUIN", "JUIL", "AOU", "SEP", "OCT", "NOV", "DEC"];
	let date = new Date(postedDate);

	return listMonth[date.getMonth()];
});

Template.registerHelper("fullDate", (date) => {
	const lang = (FlowRouter.getParam("lang") || "fr");
	let listMonth
	if (lang == "fr") {
		listMonth = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
	} else {
		listMonth = ["january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"];
	}
	let fullDate = new Date(`${date.split('/')[1]}/${date.split('/')[0]}/${date.split('/')[2]}`)
	return fullDate.getDate() + ' ' + listMonth[fullDate.getMonth()] + ' ' + fullDate.getFullYear();
});

Template.registerHelper("reduceMessage", (message, id, idx) => {
	var totalLength = 0;
	var isBalise = false;
	var count = 0;
	var openedBalise = [""];
	var openedBaliseIndex = 0;
	var cutMessage = "";
	if (message && message.length) {
		totalLength += message.length;
		if (totalLength > 250) {
			for (let i = 0; i != message.length; i++) {
				if (message[i] == '<')
					isBalise = true;
				else if (message[i] == '>') {
					let temp = openedBalise[openedBaliseIndex].split(' ');
					openedBalise[openedBaliseIndex] = temp[0];
					openedBaliseIndex++;
					openedBalise[openedBaliseIndex] = "";
					isBalise = false;
				}
				else {
					if (isBalise == true) {
						openedBalise[openedBaliseIndex] += message[i];
					}
					if (!isBalise)
						count += 1;
					if (count >= 250) {
						cutMessage = message.substr(0, i);
						break;
					}
				}
			}
		}
		return totalLength > 250
		? cutMessage + ` <span style="font-style: normal"> ... </span><a class='seeMore' post-id=${id} ${typeof idx !== 'object' ? 'idx=' + idx : ''} style='font-size: 12px; font-style: normal'>Voir plus</a>` : message;
	}
	return message;
});

Template.registerHelper('moreThanOne', (nb) => {
	return nb > 1;
});

Template.registerHelper('getIncidentHistoryTitle', (historyNum) => {
	const lang = (FlowRouter.getParam("lang") || "fr");
  var tr_incidentRes = new translate.IncidentResidentIndex(lang);
	return tr_incidentRes.incidentResidentIndex["title" + historyNum];
});


Template.registerHelper('getDeclarationDetailValue', (declarationId, displayVal, customLang) => {
	let typeCollection = DeclarationDetails.findOne(declarationId);
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (typeCollection && typeCollection["value-" + lang])
		return typeCollection["value-" + lang];
	if (displayVal == true)
		return typeId; /* if created from gestionnaire, value entered manually */
	return "-";

});

Template.registerHelper('getMessageType', (typeId, displayVal, customLang) => {
	let typeCollection = IncidentType.findOne(typeId);
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (typeCollection && typeCollection["value-" + lang])
		return typeCollection["value-" + lang];
	if (displayVal == true)
		return typeId; /* if created from gestionnaire, value entered manually */
	return "-";

});

Template.registerHelper('getIncidentType', (typeId, displayVal, customLang) => {

	let typeCollection = IncidentDetails.findOne(typeId);
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (typeCollection && typeCollection["value-" + lang])
		return typeCollection["value-" + lang];
	if (displayVal == true) {
		let declarationDetail = DeclarationDetails.findOne(typeId);
		if (declarationDetail)
			return declarationDetail["value-" + lang];
		return typeId; /* if created from gestionnaire, value entered manually */
	}
	return "-";
})

Template.registerHelper('getIncidentDetailPicto', (typeId, customLang)=> {
	let typeCollection = IncidentDetails.findOne(typeId);
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (typeCollection && typeCollection['picto']){
		return typeCollection['picto'];
	} else {
		if (typeCollection && typeCollection["value-" + lang])
			return typeCollection["value-" + lang];
	}
	return '-'
})

Template.registerHelper('getIncidentAreaPicto', (areaId) => {
	let areaCollection = IncidentArea.findOne(areaId);
	let lang = (FlowRouter.getParam("lang") || "fr");
	if (areaCollection && areaCollection.picto)
		return areaCollection.picto;
	return "-";
})

Template.registerHelper('getIncidentArea', (areaId, customLang) => {
	let areaCollection = IncidentArea.findOne(areaId);
	let lang = "fr";
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (areaCollection && areaCollection["value-" + lang])
		return areaCollection["value-" + lang];
	return "-";
})

Template.registerHelper('getIncidentPriority', (priorityId, customLang) => {
	let priorityCollection = IncidentPriority.findOne(priorityId);
	let lang = "fr";
	if (customLang && customLang != undefined && customLang != null && _.isString(customLang))
		lang = customLang;
	else
		lang = (FlowRouter.getParam("lang") || "fr");
	if (priorityCollection && priorityCollection["value-" + lang])
		return priorityCollection["value-" + lang];
	return "-";
})

Template.registerHelper('isUrgentDefaultPriority', (priorityId) => {
		let priority = IncidentPriority.findOne(priorityId)
		if (priority && priority != undefined && priority.defaultPriority && priority.defaultPriority == "urgent")
			return true;
		return false;
});

Template.registerHelper('getMessageTypePicto', (type) => {
	let typeCollection = IncidentType.findOne(type);
	if (typeCollection && typeCollection.picto)
		return typeCollection.picto;
	if (type === "Facturation / Comptabilité")
		return "/img/icons/message-2.png";
	if (type === "Incident")
		return "/img/icons/message-1.png";
	return "/img/icons/message-3.png";
});

Template.registerHelper('getIncidentTypePicto', (type) => {
	let typeCollection = IncidentDetails.findOne(type);
	if (typeCollection && typeCollection.picto)
		return typeCollection.picto;
	if (type == "Electricité")
		return "/img/icons/details-1.png";
	if (type == "Fuite d'eau")
		return "/img/icons/details-2.png";
	if (type == "Chauffage")
		return "/img/icons/details-3.png";
	if (type == "Ascenseur")
		return "/img/icons/details-4.png";
	if (type == "Eau")
		return "/img/icons/details-5.png";
	if (type == "Espace Vert")
		return "/img/icons/details-6.png";
	if (type == "Nettoyage")
		return "/img/icons/details-7.png";
	if (type == "Porte")
		return "/img/icons/details-8.png";
	if (type == "Parking")
		return "/img/icons/details-9.png";
	return "/img/icons/details-10.png";
});

Template.registerHelper('incidentType', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentType.find({
			$or:
			[
				{isNewEmpty: {$exists: false}},
				{isNewEmpty: {$ne: true}}
			]}).fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentType.find(
			{
				$and: [
					{condoId: condoId},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentType.find(
			{
				$and: [
					{condoId: "default"},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		else
			return getNotDefault;
	}
	return [];
});

Template.registerHelper('incidentTypeWithEmpty', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentType.find().fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentType.find({condoId: condoId}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentType.find({condoId: "default"}).fetch();
		else
			return getNotDefault;
	}
	return [];
});

Template.registerHelper('incidentDetails', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentDetails.find({
			$or:
			[
				{isNewEmpty: {$exists: false}},
				{isNewEmpty: {$ne: true}}
			]}).fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentDetails.find(
			{
				$and: [
					{condoId: condoId},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentDetails.find(
			{
				$and: [
					{condoId: "default"},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		else
			return getNotDefault;
	}
	return [];
});

Template.registerHelper('incidentDetailsWithEmpty', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentDetails.find().fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentDetails.find({condoId: condoId}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentDetails.find({condoId: "default"}).fetch();
		else
			return getNotDefault;
	}
	return [];
});

Template.registerHelper('incidentPriority', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "") {
		return IncidentPriority.find({
			$or:
			[
				{isNewEmpty: {$exists: false}},
				{isNewEmpty: {$ne: true}}
			]}).fetch();
	}
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentPriority.find(
			{
				$and: [
					{condoId: condoId},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0) {
			return IncidentPriority.find(
			{
				$and: [
					{condoId: "default"},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		}
		else {
			return getNotDefault;
		}
	}
	return [];
});

Template.registerHelper('incidentPriorityWithEmpty', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentPriority.find().fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentPriority.find({condoId: condoId}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentPriority.find({condoId: "default"}).fetch();
		else
			return getNotDefault;
	}
});

Template.registerHelper('incidentArea', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentArea.find({
			$or:
			[
				{isNewEmpty: {$exists: false}},
				{isNewEmpty: {$ne: true}}
			]}).fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentArea.find(
			{
				$and: [
					{condoId: condoId},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentArea.find(
			{
				$and: [
					{condoId: "default"},
					{$or:
						[
							{isNewEmpty: {$exists: false}},
							{isNewEmpty: {$ne: true}}
						]
					}
				]
			}).fetch();
		else
			return getNotDefault;
	}
	return [];
});

Template.registerHelper('incidentAreaWithEmpty', (condoId) => {
	if ((!condoId || condoId == undefined) && condoId != "")
		return IncidentArea.find().fetch();
	else if (condoId != "" && condoId != "all") {
		let getNotDefault = IncidentArea.find({condoId: condoId}).fetch();
		if (!getNotDefault || getNotDefault == undefined || getNotDefault.length == 0)
			return IncidentArea.find({condoId: "default"}).fetch();
		else
			return getNotDefault;
	}
});

Template.registerHelper('getValueByLang', (elem) => {
	let lang = FlowRouter.getParam("lang") || "fr";
	return elem["value-" + lang];
});


Template.registerHelper('getCondoName', (condoId) => {
	let condo = Condos.findOne(condoId);
	if (condo)
		return condo.getName();
});

Template.registerHelper('getCondoNameOrDash', (condoId) => {
	let condo = Condos.findOne(condoId);
	if (condo) {
    return condo.getNameOrDash()
  } else {
    return '-'
  }
});

Template.registerHelper('getCondoNameForEmail', (condoId) => {
	let condo = Condos.findOne(condoId);
	if (condo)
		return condo.getNameWithAddress();
});

Template.registerHelper('moduleActivated', (queryCondo, name) => {
	if (!queryCondo)
		return true;
	let resident = Residents.findOne({userId: Meteor.userId()});
	if (resident) {
		let condo = resident.condos.find((elem) => {return elem.condoId === queryCondo._id});
		if (condo && condo.preferences)
			return condo.preferences[name];
	}
	return true;
});

Template.registerHelper('createOptionArray', (optionsInString) => {
	return optionsInString.split('_//');
});

Template.registerHelper('postOfGestionnaire', (gestionnaireId, userId) => {
	const post = {'gardien': 'Gardien',
	'mainteneurAvance': 'Mainteneur du site avancé',
	'assistantCopro': 'Assistant de copropriété',
	'gestionnaireCopro': 'Gestionnaire de copropriété',
	'respoCondo': 'Responsable de résidence',
	'respoCopro': 'Responsable de copropriété',
	'directeurRegional': 'Directeur régional',
	'directeurEtablissement': "Directeur d'établissement",
	'directeurMarque': 'Directeur de marque',
	'directionSiege': 'Direction siège'};
	const gestionnaire = Enterprises.findOne(gestionnaireId);
	if (gestionnaire) {
		const user = _.find(gestionnaire.users, function(elem) {
			return elem.userId == userId;
		});
		if (user) {
			return post[user.type];
		}
	}
});

Template.registerHelper('i18nVarWrapped', (section, variable) => {
	const lang = FlowRouter.getParam('lang') || "fr"
	return translate.i18nVarWrapped({ lang, section, variable })
})

Template.registerHelper('i18n', (variable) => {
	const lang = FlowRouter.getParam('lang') || "fr"
	return translate.i18n(lang, variable)
})

Template.registerHelper('getDicoVar', (session, variable) => {
	let lang = (FlowRouter.getParam("lang") || "fr");
	let classVar = null;

	switch (session) {
		case "commonTranslation":
			classVar = new translate.CommonTranslation(lang);
			break;
		case "buildingsView":
			classVar = new translate.BuildingsView(lang);
			break;
		case "roleName":
		  classVar = new translate.RoleName(lang);
			break;
		case "notConnected":
			classVar = new translate.NotConnected(lang);
			break;
		case "home":
			classVar = new translate.Home(lang);
			break;
		case "homePage":
			classVar = new translate.HomePage(lang);
			break;
		case "whoCanSeeHeader":
			classVar = new translate.WhoCanSeeHeader(lang);
			break;
		case "resourceForm":
			classVar = new translate.ResourceForm(lang);
			break;
    case "moduleReportAnAbuse":
        classVar = new translate.ModuleReportAnAbuse(lang);
        break;
    case "stats":
        classVar = new translate.Stats(lang);
        break;
		case "googleApiError":
			classVar = new translate.GoogleApiError(lang);
			break;
		case "moduleActualityIndex":
			classVar = new translate.ModuleActualityIndex(lang);
			break;
		case "moduleMap":
			classVar = new translate.ModuleMap(lang);
			break;
		case "moduleConcierge":
			classVar = new translate.ModuleConcierge(lang);
			break;
		case "moduleTrombinoscopeIndex":
			classVar = new translate.ModuleTrombinoscopeIndex(lang);
			break;
		case "moduleActualityModal":
			classVar = new translate.ModuleActualityModal(lang);
			break;
		case "moduleClassifiedsCreateAdModal":
			classVar = new translate.ModuleClassifiedsCreateAdModal(lang);
			break;
		case "moduleClassifiedsList":
			classVar = new translate.ModuleClassifiedsList(lang);
			break;
		case "moduleClassifiedsViewAd":
			classVar = new translate.ModuleClassifiedsViewAd(lang);
			break;
		case "board":
			classVar = new translate.Board(lang);
			break;
		case "feedList":
			classVar = new translate.FeedList(lang);
			break;
		case "feedListModal":
			classVar = new translate.FeedListModal(lang);
			break;
		case "moduleForumCreatePostModal":
			classVar = new translate.ModuleForumCreatePostModal(lang);
			break;
		case "moduleForumList":
			classVar = new translate.ModuleForumList(lang);
			break;
		case "moduleForumViewPost":
			classVar = new translate.ModuleForumViewPost(lang);
			break;
		case "moduleManual":
			classVar = new translate.ModuleManual(lang);
			break;
		case "moduleIncidentIndex":
			classVar = new translate.ModuleIncidentIndex(lang);
			break;
		case "incidentResidentIndex":
			classVar = new translate.IncidentResidentIndex(lang);
			break;
		case "moduleIncidentFeed":
			classVar = new translate.ModuleIncidentFeed(lang);
			break;
		case "incidentGestionnaireElem":
			classVar = new translate.IncidentGestionnaireElem(lang);
			break;
		case "incidentGestionnaireList":
			classVar = new translate.IncidentGestionnaireList(lang);
			break;
		case "incidentModalNew":
			classVar = new translate.IncidentModalNew(lang);
			break;
		case "incidentGestionnaireModalEval":
			classVar = new translate.IncidentGestionnaireModalEval(lang);
			break;
		case "incidentGestionnaireModalAction":
			classVar = new translate.IncidentGestionnaireModalAction(lang);
			break;
		case "incidentGestionnaireModalHistory":
			classVar = new translate.IncidentGestionnaireModalHistory(lang);
			break;
		case "moduleMessagerieIndex":
			classVar = new translate.ModuleMessagerieIndex(lang);
			break;
		case "messageInput":
			classVar = new translate.MessageInput(lang);
			break;
		case "newMessageMobile":
			classVar = new translate.NewMessageMobile(lang);
			break;
		case "newMessageCS":
			classVar = new translate.NewMessageCS(lang);
			break;
		case "newMessageGardien":
			classVar = new translate.NewMessageGardien(lang);
			break;
		case "moduleReservationIndex":
			classVar = new translate.ModuleReservationIndex(lang);
			break;
		case "ModuleStat":
			classVar = new translate.ModuleStat(lang);
			break;
		case "deletePost":
			classVar = new translate.DeletePost(lang);
			break;
		case "main":
			classVar = new translate.Main(lang);
			break;
		case "preferenceProfile":
			classVar = new translate.PreferenceProfile(lang);
			break;
		case "internalError":
			classVar = new translate.InternalError(lang);
			break;
		case "aboutus":
			classVar = new translate.AboutUs(lang);
			break;
		case "dayofweek":
			classVar = new translate.DayOfWeek(lang);
			break;
		case "monthofyear":
			classVar = new translate.MonthOfYear(lang);
			break;
		case "faq":
			classVar = new translate.FAQ(lang);
			break;
		case "ourvalues":
			classVar = new translate.OurValues(lang);
			break;
		case "satisfaction":
			classVar = new translate.Satisfaction(lang);
			break;
		case "unfollow":
			classVar = new translate.Unfollow(lang);
			break;
		case "common":
			classVar = new translate.Common(lang);
			break;
		case "userView":
			classVar = new translate.UserView(lang);
			break;
		case "trombi":
			classVar = new translate.Trombi(lang);
			break;
		case "roleName":
			classVar = new translate.Rolename(lang);
			break;
		case "forumLang":
			classVar = new translate.ForumLang(lang);
			break;
		case "error_message":
			classVar = new translate.ErrorMessagingControler(lang);
			break;
		case "moduleReservationError":
			classVar = new translate.ModuleReservationError(lang);
			break;
		case "condoView":
			classVar = new translate.CondoView(lang);
			break;
		case "managerView":
			classVar = new translate.ManagerView(lang);
      break;
    // case "commonTranslation":
		// 	classVar = new translate.IntegrationsView(lang);
		// 	break;
		case "userDetail":
			classVar = new translate.UserDetail(lang);
			break;
		case "statDetails":
			classVar = new translate.StatDetails(lang);
			break;
		case "inventory":
			classVar = new translate.Inventory(lang);
			break;
		case "documentModule":
			classVar = new translate.DocumentModule(lang);
			break;
		case "contactManagement":
			classVar = new translate.ContactManagement(lang);
			break;
		default:
			classVar = null;
  }
  if (variable) {
    return classVar[session][variable];
  } else {
    return classVar
  }
});

Template.registerHelper('convertPtoBr', (html) => {
	return html.replace(/<p>/ig, "").replace(/<\/p>/ig, "<br>");
});

function typeOfTag(tag) {
	let close = new RegExp(/<\/\w*>/ig);
	let open = new RegExp(/<[a-zA-Z0-9_ ="'`]*\/?>/ig);
	let img = new RegExp(/<img.*\/?>/ig);
	let br = new RegExp(/<br\/?>/ig);

	if (close.exec(tag) || img.exec(tag) || br.exec(tag))
		return -1;
	else if (open.exec(tag))
		return 0;
	return 1;
}

Template.registerHelper('doSeeMoreLess', (html, len) => {
	if (html && typeof html === 'string') {
		let text = html.replace(/<\/?[a-zA-Z0-9_ ="'`]*\/?>/ig, "");
		return text.length > len;
	}
});

Template.registerHelper('seeMoreMessage', (html, len) => {
	let i = 0;
	let text = html.replace(/<\/?[^<>]*\/?>/ig, "");
	if (text.length <= len) {
		return html;
    } else {
        const truncate = require('truncate')
        return truncate(html, len);
	}
});

Template.registerHelper('nl2br', (str) => {
	const entities = new Entities();
	if (!str)
		return null;
	str = entities.encode(str)
	return str.split(/\r\n|\n\r|\n|\r/).join("<br/>");
});

Template.registerHelper('tester', (str) => {
	return "";
});

Template.registerHelper('BlazeFlowRouter', (route, params = null) => {
	if (_.isString(params)) {
		return FlowRouter.path(route, JSON.parse(params));
	}
	return FlowRouter.path(route);
});

Template.registerHelper('formatMobileNum', (tel, telCode) => {
	try {
		let parseTel = phoneUtil.parse("+" + telCode + tel, "FR");
		if (phoneUtil.isValidNumber(parseTel, PNF.INTERNATIONAL)) {
			return phoneUtil.format(parseTel, PNF.INTERNATIONAL);
		}
		return tel;
	} catch(e) {
		return "";
	}
});

Template.registerHelper('formatMobileNumNoError', (tel, telCode) => {
	try {
    if (!telCode) {
      telCode = '33'
    }
		let parseTel = phoneUtil.parse("+" + telCode + tel, "FR");
		if (phoneUtil.isValidNumber(parseTel, PNF.INTERNATIONAL)) {
			return phoneUtil.format(parseTel, PNF.INTERNATIONAL);
		}
		return tel;
	} catch(e) {
    if (tel && tel.length > 0) {
      return tel
    }
		return "-";
	}
});

Template.registerHelper('loopCount', function(count){
    var countArr = [];
    for (var i=0; i<count; i++){
      countArr.push({});
    }
    return countArr;
});

Template.registerHelper('userHasRight', function(module, right, condoId = null, userId = null) {
	if (!_.isString(userId))
		userId = null;
	return Meteor.userHasRight(module, right, condoId, userId);
});

Template.registerHelper('hasRightInCondos', function(module, right, condoIds = null) {
	let gestionnaireMeteorUser = Meteor.user();

	if (gestionnaireMeteorUser) {
		if (!_.isArray(condoIds)) {
      const enterprise = Enterprises.findOne({ _id: gestionnaireMeteorUser.identities.gestionnaireId }, { fields: { users: true } })
      const mapUser = enterprise.users
      const findUser = _.find(mapUser, function (elem) {
        return elem.userId == Meteor.userId();
      })
      condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);
		}
		if (condoIds) {
			let i = 0;
			while (i < condoIds.length)
			{
				if (Meteor.userHasRight(module, right, condoIds[i++]) === true)
					return true;
			}
		}
	}
	return false;
});


Template.registerHelper('hasRightInEachCondo', function(module, right, condoIds) {
	let i = 0;
	while (i < condoIds.length)
	{
		if (!Meteor.userHasRight(module, right, condoIds[i]))
			return false;
		i++;
	}
	return true;
});

Template.registerHelper('getCurrentCondo', function() {
	return localStorage.getItem('selectedCondoId')
});

Template.registerHelper('getCondoIdsfromCondo', function(condos) {
	return _.map(condos, function (condo){
		return condo.condoId || condo._id;
	})
});

Template.registerHelper('equals', (a, b) => a === b)
Template.registerHelper('notEquals', (a, b) => a !== b)

Template.registerHelper('parseAdPrice', (price) => {
	const lang = FlowRouter.getParam('lang') || 'fr'
  const translation = new translate.ModuleClassifiedsList(lang);
	if (!price) {
		return 'NC'
	} else if (price == 0) {
		return translation.moduleClassifiedsList.free
	} else {
		return lang === 'fr' ? `${formatPrice(price, 2, 3, ' ', ',')}€` : `${formatPrice(price)}€`
	}
});

Template.registerHelper('getUserCreatedAt', () => {
	const lang = FlowRouter.getParam('lang') || 'fr'
  const translation = new translate.Email(lang);
	return !!Meteor.user() && !!Meteor.user().createdAt && moment(Meteor.user().createdAt).locale(FlowRouter.getParam('lang') || 'fr').format('DD MMM YYYY[' + translation.email['_at_'] + ']HH:mm');
})

Template.registerHelper('$empty', (arr) => arr.constructor === Array && arr.length === 0)

Template.registerHelper('getIncidentText', (status) => {
  const lang = FlowRouter.getParam("lang");
  const t = new translate.Common(lang);

  switch (status) {
		case 0:
			return t.common.waiting;
    case 1:
      return t.common.in_progress;
    case 2:
      return t.common.resolved;
    case 3:
      return t.common.private;
	}
});

Template.registerHelper('getIncidentClass', (status) => {
	const classes = ['danger', 'calm', 'warning', 'refuse'];

	return !!classes[status] ? classes[status] : false;
});

Template.registerHelper('getFullName', (userId) => {
    let user = UsersProfile.findOne(userId)
    if (user) {
        return user.firstname + ' ' + user.lastname
    } else {
        translation = new translate.ModuleMessagerieIndex((FlowRouter.getParam("lang") || "fr"))
        return translation.moduleMessagerieIndex["desactivate_account"];
    }
});

Template.registerHelper('isSmallerDevice', () => {
	if ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
		return true;
	} else {
		return false;
	}
});

Template.registerHelper('getCalendarType', () => {
  return [{
    type: 'ical',
    name: 'iCalendar'
  }, {
    type: 'outlook',
    name: 'OutLook'
  }, {
    type: 'gcal',
    name: 'Google Calendar'
  }]
});

Template.registerHelper('condoHaveLogo', (condoId) => {
  if (condoId === 'all' || !condoId) {
    // if all is selected assuming condo have logo so it'll hide MB wording
    return true
  } else {
    const condo = Condos.findOne(condoId)
    return !!condo && !!condo.getLogo();
  }
});

Template.registerHelper('getCondoLogo', (condoId, size) => {
  if (condoId === 'all' || !condoId) {
    return false
  } else {
    const condo = Condos.findOne(condoId)
    return !!condo && !!condo.getLogo() && condo.getLogo()[size];
  }
});

Template.registerHelper('getInterests', (userProfile) => {
  if (!!userProfile && !!userProfile.interests) {
    return userProfile.interests.split(',')
  }
});

Meteor.getManagerCondoIds = function() {
  let gestionnaireMeteorUser = Meteor.user();
  let condoIds = []

  if (gestionnaireMeteorUser && gestionnaireMeteorUser.identities) {
    const enterprise = Enterprises.findOne({ _id: gestionnaireMeteorUser.identities.gestionnaireId }, { fields: { users: true } })
    if (enterprise && enterprise.users) {
      const mapUser = enterprise.users
      const findUser = _.find(mapUser, function (elem) {
        return elem.userId == Meteor.userId();
      })
      condoIds = _.map(findUser.condosInCharge, (value) => value.condoId);
    }
  }
  return condoIds
}

Meteor.listCondoUserHasRight = function(module, right, condoIds = null) {
	let gestionnaireMeteorUser = Meteor.user();
	let condoLists = [];
	if (gestionnaireMeteorUser) {
		if (!_.isArray(condoIds)) {
      const enterprise = Enterprises.findOne(gestionnaireMeteorUser.identities.gestionnaireId);
      const users = enterprise.users;
      const user = users.find(u => u.userId === Meteor.userId());
      condoIds = user.condosInCharge.map(c => c.condoId);
		}
		if (condoIds) {
			let i = 0;
			while (i < condoIds.length)
			{
				if (Meteor.userHasRight(module, right, condoIds[i]) === true)
					condoLists.push(condoIds[i]);
				i++;
			}
		}
	}
	return condoLists;
};

Meteor.userHasRight = function(module, right, condoId = null, userId = null) {
	try {
		if (!_.isString(condoId)) {
      if (!this.connection) {
        return false
      }
      condoId = FlowRouter.getParam("condo_id") || FlowRouter.getParam("condoId") || localStorage.getItem('condoIdSelected')
		}
		if (condoId == 'all')
			return false;


		if (!_.isString(userId)) {
			userId = Meteor.userId();
    }
    let condoModuleName = convertRightModuleNameToCondoModuleName(module)
    if (condoModuleName && condoModuleName === 'messenger') {
      if (right === 'writeToResident' && !Meteor.condoHasModule('messengerResident', condoId)) {
        return false
      } else if (right === 'writeToUnionCouncil' && !Meteor.condoHasModule('messengerSyndic', condoId)) {
        return false
      } else if (right === 'writeToSupervisors' && !Meteor.condoHasModule('messengerGardien', condoId)) {
        return false
      } else if (right === 'writeToManager' && !Meteor.condoHasModule('messengerGestionnaire', condoId)) {
        return false
      } else if (right === 'seeAllMsgEnterprise' && !Meteor.condoHasModule('messenger', condoId)) {
        return false
      } else if (right === 'writeAllMsgEnterprise' && !Meteor.condoHasModule('messenger', condoId)) {
        return false
      }
    } else if (condoModuleName && Meteor.condoHasModule(condoModuleName, condoId) !== true) {
      return false
    }

		let userRight = UsersRights.findOne({condoId: condoId, userId: userId});

    if (Meteor.isClient && Meteor.user() && Meteor.user().identities && Meteor.user().identities.adminId) {
			return true;
		}
		else if (userRight && userRight.module && userRight.module[module] && _.isBoolean(userRight.module[module][right])) {
			return userRight.module[module][right];
		}
		else if (userRight && userRight.defaultRoleId) {
			let defaultRole = DefaultRoles.findOne(userRight.defaultRoleId);
			let condoRole = CondoRole.findOne({name: defaultRole.name, condoId: condoId});
			let result;

			if (condoRole) {
				result = Meteor.findRight(condoRole.rights, module, right);
			}
			if (!_.isBoolean(result)) {
				result = Meteor.findRight(defaultRole.rights, module, right);
			}
			return result;
		}
		else {
			return false;
		}
	} catch (e) {
		console.error(e);
	}
	return false;
}

Meteor.findRight = function(rights, module, right) {
	let i = 0;
	while (i < rights.length) {
		if (rights[i].module == module && rights[i].right == right)
			return rights[i].default;
		i++;
	}
	return null;
}

Meteor.getBuildingStatusValue = function(key, upperCase, lang) {
	if (lang == undefined || lang == null)
		lang = (FlowRouter.getParam("lang") || "fr");
	let value = BuildingStatus.findOne({key: key});
	if (!value || value == undefined)
		return "-";
	value = value["value-" + lang];
	if (upperCase == true) {
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
	return value;
}

Meteor.condoHasModule = function(moduleName, condoId = null) {
  if (typeof moduleName !== 'string') {
    throw Meteor.Error(401, 'Invalid module name')
  }
  if (!_.isString(condoId)) {
    condoId = FlowRouter.getParam("condo_id") || FlowRouter.getParam("condoId") || localStorage.getItem('condoIdSelected')
  }
  if (condoId == 'all') {
    throw Meteor.Error(401, 'Invalid condoId: condoId can\'t be egals to `all`')
  }
  let condo = Condos.findOne({ _id: condoId })
  if (condo) {
    return condo && condo.settings && condo.settings.options && condo.settings.options[moduleName]
  }
}

Meteor.absoluteUrl = function(suffix = '', condoId = null, userId = null, companyId = null) {
  if (Meteor.isClient) {
    const root_url = window.location.origin + '/'
    return root_url + suffix
  } else {
    let root_url = process.env.ROOT_URL
    let enterprise
    if (companyId) {
      enterprise = Enterprises.findOne({ _id: companyId, 'settings.domain': { $exists: true, $ne: '' } }, { fields: { name: true, settings: true } })
    } else if (condoId) {
      enterprise = Enterprises.findOne({ condos: condoId, 'settings.domain': { $exists: true, $ne: '' } }, { fields: { name: true, settings: true } })
    } else if (userId) {
      const whiteMarkEnterprises = Enterprises.find({ 'settings.domain': { $exists: true, $ne: '' } }, { fields: { name: true, settings: true } }).fetch()
      const whiteMarkEnterprisesId = _.map(whiteMarkEnterprises, (enterprise) => enterprise._id)
      const thisUser = Meteor.users.findOne({ _id: userId }, { fields: { identities: true } })
      if (thisUser && thisUser.identities.residentId) {
        const thisResident = Residents.findOne({ _id: thisUser.identities.residentId })
        if (thisResident) {
          let condoIds = [...thisResident.condos.map(c => condoId), ...thisResident.pendings.map(c => condoId)]
          for (let i = 0; i < condoIds.length; i++) {
            if (whiteMarkEnterprisesId.includes(condoIds[i])) {
              enterprise = Enterprises.findOne({ condos: condoIds[i], 'settings.domain': { $exists: true, $ne: '' } }, { fields: { name: true, settings: true } })
              break
            }
          }
        }
      } else if (thisUser && thisUser.identities.gestionnaireId) {
        if (whiteMarkEnterprisesId.includes(thisUser.identities.gestionnaireId)) {
          enterprise = whiteMarkEnterprises.find(e => e._id === thisUser.identities.gestionnaireId)
        }
      }
    }
    if (enterprise && enterprise.settings.domain) {
      if (root_url === 'https://develop.monbuilding.com/') {
        root_url = 'https://' + enterprise.settings.domain + '.develop.monbuilding.com/'
      } else if (root_url === 'https://www.monbuilding.com/') {
        root_url = 'https://' + enterprise.settings.domain + '.monbuilding.com/'
      } else {
        root_url = 'http://' + enterprise.settings.domain + '.localhost.com:3000/'
      }
    }

    // if (root_url === 'http://localhost:3000/') root_url = 'https://2adf30e7.eu.ngrok.io'

    // console.log('root_url', root_url)

    return root_url + suffix
  }
}

function convertRightModuleNameToCondoModuleName(rightModuleName) {
  const rightNameToModuleName = {
    emergencyContact: 'emergencyContact',
    wallet: 'digitalWallet',
    marketPlace: 'marketPlace',
    incident: 'incidents',
    actuality: 'informations',
    manual: 'manual',
    map: 'map',
    trombi: 'trombi',
    messenger: 'messenger',
    reservation: 'reservations',
    forum: 'forum',
    ads: 'classifieds',
    print: 'print'
  }

  return rightNameToModuleName[rightModuleName] || null
}

Isemail = {
  validate: function(email = '') {
    let regexp = new RegExp(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)

    return !!email.match(regexp)
  }
}

Meteor.getTimeOrDash = function (time, isParsed = false) {
  if (!time) {
    return '-'
  } else {
    return isParsed ? time : moment(time).format('HH:mm')
  }
}
