let Occupant = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: true,
        copro: true,
        office: true,
        student: true
    },
    "name" : "Occupant",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : true,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : true,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : false,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : true,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : true,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};

let ConseilSyndical = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: false,
        copro: true,
        office: false,
        student: false
    },
    "name" : "Conseil Syndical",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : true,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : true,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : false,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : true,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : true,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : true,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : true,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : true,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : true,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : true,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : true,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : true,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : true,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};

let ServicesGeneraux = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: false,
        copro: false,
        office: true,
        student: false
    },
    "name" : "Services Généraux",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : null,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : null,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : true,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : true,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};

let ServicesCourrier = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: false,
        copro: false,
        office: true,
        student: false
    },
    "name" : "Services Courrier",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : null,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : null,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : false,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : false,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : false,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};

let OfficeManager = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: false,
        copro: false,
        office: true,
        student: false
    },
    "name" : "Office Manager",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : true,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : true,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : false,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : false,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : false,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : false,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};

let Gardien = {
    "for" : "occupant",
    "forCondoType" : {
    	mono: true,
        copro: true,
        office: false,
        student: true
    },
    "name" : "Gardien",
    "rights" : [
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToResident",
            "default" : true,
            "value-fr" : "Messagerie entre occupants",
            "value-en" : "Messenger between occupants"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToUnionCouncil",
            "default" : true,
            "value-fr" : "Messagerie Conseil syndical",
            "value-en" : "Building Council Messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToSupervisors",
            "default" : true,
            "value-fr" : "Messagerie gardien",
            "value-en" : "Building supervisor messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "writeToManager",
            "default" : true,
            "value-fr" : "Messagerie gestionnaire",
            "value-en" : "Manager messenger"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "edit",
            "default" : true,
            "value-fr" : "Éditer un message envoyé",
            "value-en" : "Edit a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer un message envoyé",
            "value-en" : "Delete a sent message"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askBadges",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une demande de badge au gestionnaire",
            "value-en" : "Ask the manager for a badge"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askInvit",
            "default" : false,
            toRemove: true,
            "value-fr" : "Faire une demande d'accès au gestionnaire",
            "value-en" : "Ask the manager for access"
        },
        {
            "module" : "messenger",
            displayOrder: 8,
            "right" : "askOther",
            "default" : true,
            toRemove: true,
            "value-fr" : "Faire une \"autre\" demande au gestionnaire",
            "value-en" : "Make another demand to the manager"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Informations »",
            "value-en" : "See the « Informations » section"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier des informations",
            "value-en" : "Publish informations"
        },
        {
            "module" : "actuality",
            displayOrder: 0,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les informations publiées par un utilisateur",
            "value-en" : "Delete informations posted by a user"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Manuel »",
            "value-en" : "See the « Manual » sub-section"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "write",
            "default" : true,
            "value-fr" : "Modifier le manuel",
            "value-en" : "Edit the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "declare",
            "default" : true,
            "value-fr" : "Déclarer un incident",
            "value-en" : "Declare an incident"
        },
        {
            "module" : "manual",
            displayOrder: 1,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Effacer le manuel",
            "value-en" : "Delete the manual"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Incidents »",
            "value-en" : "See the « Incidents » section"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "vote",
            "default" : true,
            "value-fr" : "Noter une intervention",
            "value-en" : "Rate an intervention"
        },
        {
            "module" : "incident",
            displayOrder: 4,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer un message au gestionnaire au sujet d’un incident déclaré",
            "value-en" : "Send a message to the manager about a declared incident"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Trombinoscope »",
            "value-en" : "See the « Who's who » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "sendMessage",
            "default" : true,
            "value-fr" : "Envoyer message depuis le trombinoscope",
            "value-en" : "Send a message from the who's who"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "seeOccupantList",
            "default" : false,
            "value-fr" : "Voir la rubrique « Occupants » ",
            "value-en" : "See the « Occupants » section"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "addOccupant",
            "default" : false,
            "value-fr" : "Inviter un occupant",
            "value-en" : "Invite an occupant"
        },
        {
            "module" : "trombi",
            displayOrder: 3,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer un occupant d'un immeuble",
            "value-en" : "Delete an occupant from a building"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la sous rubrique « Plan »",
            "value-en" : "See the « Plan » sub-section"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "write",
            "default" : true,
            "value-fr" : "Modifier le plan",
            "value-en" : "Edit the plan"
        },
        {
            "module" : "map",
            displayOrder: 2,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer le plan",
            "value-en" : "Delete the plan"
        },
        {
            "module" : "wallet",
            displayOrder: 2.1,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir le digital wallet",
            "value-en" : "See digital wallet"
        },
        {
            "module" : "emergencyContact",
            displayOrder: 2.2,
            "right" : "see",
            "default" : false,
            "value-fr" : "Voir la rubrique « Numéro d'urgence »",
            "value-en" : "See the « Emergency Contacts » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seeSubject",
            "default" : true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writeSubject",
            "default" : true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deleteSubject",
            "default" : true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "seePoll",
            "default" : true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "writePoll",
            "default" : true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forum",
            displayOrder: 5,
            "right" : "deletePoll",
            "default" : true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir la rubrique « Forum »",
            "value-en" : "See the « Forum » section"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writeSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux posts du forum",
            "value-en" : "Publish and answer forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deleteSubject",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les posts du forum publiés par un utilisateur",
            "value-en" : "Delete forum posts published by a user"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "seePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Voir les sondages du forum",
            "value-en" : "See forum posts"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "writePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Publier et répondre aux sondages du forum",
            "value-en" : "Publish and answer forum polls"
        },
        {
            "module" : "forumCS",
            displayOrder: 6,
            "right" : "deletePoll",
            "default" : false,
            toRemove: true,
            "value-fr" : "Supprimer les sondages du forum publiés par un utilisateur",
            "value-en" : "Delete forum polls published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir la rubrique « Petites annonces »",
            "value-en" : "See the « Ads » section"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "write",
            "default" : true,
            "value-fr" : "Publier une petite annonce",
            "value-en" : "Publish an ad"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "delete",
            "default" : true,
            "value-fr" : "Supprimer les petites annonces publiées par un utilisateur",
            "value-en" : "Delete ads published by a user"
        },
        {
            "module" : "ads",
            displayOrder: 7,
            "right" : "answer",
            "default" : true,
            "value-fr" : "Répondre à une petite annonce",
            "value-en" : "Answer to an ad"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "see",
            "default" : true,
            "value-fr" : "Voir le menu du RIE",
            "value-en" : "See the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "write",
            "default" : false,
            "value-fr" : "Publier le menu du RIE",
            "value-en" : "Publish the company restaurant's menu"
        },
        {
            "module" : "restaurant",
            toRemove: true,
            "right" : "delete",
            "default" : false,
            "value-fr" : "Supprimer le menu du RIE",
            "value-en" : "Delete the company restaurant's menu"
        },
        {
            "module" : "view",
            displayOrder: 14,
            "right" : "concierge",
            "default" : true,
            "value-fr" : "Voir la rubrique « Conciergerie »",
            "value-en" : "See the « Concierge Service » section"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "seeAll",
            "default" : false,
            "value-fr" : "Voir les reservations des autres utilisateurs",
            "value-en" : "See other users\' reservations"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "invite",
            "default" : true,
            "value-fr" : "Ajouter un participant lors de la réservation d'une ressource",
            "value-en" : "Add participants while making a reservation"
        },
        {
            "module" : "reservation",
            displayOrder: 9,
            "right" : "reserveOther",
            "default" : true,
            "value-fr" : "Réserver des services ad hoc lors de la réservation",
            "value-en" : "Book additionnal services during a reservation"
        },
    ]
};


export {Occupant, ConseilSyndical, ServicesGeneraux, ServicesCourrier, OfficeManager, Gardien };
