let AssetManager = {
  "for": "manager",
  "forCondoType": {
    mono: true,
    copro: true,
    office: true,
    student: true
  },
  "name": "Asset Manager",
  "rights": [
    {
      "module": "integrations",
      displayOrder: 9,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Integrations »",
      "value-en": "See the « Integrations » section"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "seeAllMsgEnterprise",
      "default": true,
      "value-fr": "Voir la messagerie gestionnaire",
      "value-en": "See the manager's messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "writeAllMsgEnterprise",
      "default": true,
      "value-fr": "Ecrire un message via la messagerie gestionnaire",
      "value-en": "Write a message via manager messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "edit",
      "default": true,
      "value-fr": "Éditer un message envoyé",
      "value-en": "Edit a sent message"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un message envoyé",
      "value-en": "Delete a sent message"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "see",
      "default": true,
      "value-fr": "Voir les contacts d'urgence",
      "value-en": "View emergency contacts"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "add",
      "default": true,
      "value-fr": "Ajouter un contact d'urgence",
      "value-en": "Add emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "edit",
      "default": true,
      "value-fr": "Editer un contact d'urgence",
      "value-en": "Edit emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un contact d'urgence",
      "value-en": "Delete emergency contact"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Informations & Evénements »",
      "value-en": "See the « Informations & Events » section"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "write",
      "default": true,
      "value-fr": "Publier des informations & événements",
      "value-en": "Publish informations & events"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer les informations & événements publiés par un utilisateur",
      "value-en": "Delete informations & events published by a user"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Manuel »",
      "value-en": "See the « Manual » column"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le manuel",
      "value-en": "Edit the manual"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le manuel",
      "value-en": "Delete the manual"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Plan »",
      "value-en": "See the « Plan » column"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le plan",
      "value-en": "Edit the plan"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le plan",
      "value-en": "Delete the plan"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Incidents »",
      "value-en": "See the « Incidents » section"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "autodeclare",
      "default": true,
      "value-fr": "Déclarer un incident",
      "value-en": "Declare an incident"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "valid",
      "default": true,
      "value-fr": "Valider les incidents",
      "value-en": "Validate incidents"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "setPrivate",
      "default": true,
      "value-fr": "Passer les incidents en privé (non visibles de tous)",
      "value-en": "Classify incidents as private (not visible by everyone)"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertQuote",
      "default": true,
      "value-fr": "Insérer un devis",
      "value-en": "Insert a quote"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertNote",
      "default": true,
      "value-fr": "Insérer une note",
      "value-en": "Insert a note"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "scheduleIntervention",
      "default": true,
      "value-fr": "Programmer une intervention",
      "value-en": "Schedule an intervention"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "resolve",
      "default": true,
      "value-fr": "Résoudre un incident",
      "value-en": "Resolve an incident"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "seeManager",
      "default": true,
      "value-fr": "Voir la rubrique « Gestionnaires » ",
      "value-en": "See the « Managers » section"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "addManager",
      "default": true,
      "value-fr": "Inviter un gestionnaire",
      "value-en": "Invite a manager"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "deleteCondoManager",
      "default": true,
      "value-fr": "Supprimer un gestionnaire d’un immeuble",
      "value-en": "Delete a manager from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Occupants » ",
      "value-en": "See the « Occupants » section"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seeOccupantDetails",
      "default": true,
      "value-fr": "Voir le détail des occupants",
      "value-en": "See occupant details"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "addOccupant",
      "default": true,
      "value-fr": "Inviter un occupant",
      "value-en": "Invite an occupant"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un occupant d'un immeuble",
      "value-en": "Delete an occupant from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seePersonalInfos",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les informations personnelles d'un occupant",
      "value-en": "See the occupants' personal informations"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeMessenger",
      "default": true,
      "value-fr": "Voir la sous rubrique « Messagerie »",
      "value-en": "See the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeMessenger",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Messagerie » ",
      "value-en": "Modify contacts in the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeWhosWho",
      "default": true,
      "value-fr": "Voir la sous rubrique « Trombinoscope »",
      "value-en": "See the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeWhosWho",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Trombinoscope »",
      "value-en": "Edit contacts in the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeValidation",
      "default": true,
      "value-fr": "Voir la sous rubrique « Validation entrées & réservations »",
      "value-en": "See the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeValidation",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Validation entrées & réservations »",
      "value-en": "Edit contacts in the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "validAwaitingOccupant",
      "default": true,
      "value-fr": "Valider / rejeter un occupant en attente",
      "value-en": "Validate / refuse a waiting occupant"
    },
    {
      "module": "view",
      displayOrder: 3.1,
      "right": "documents",
      "default": true,
      "value-fr": "Voir la rubrique « Documents »",
      "value-en": "See the « Documents » section"
    },
    {
      "module": "view",
      displayOrder: 14,
      "right": "concierge",
      "default": true,
      "value-fr": "Voir la rubrique « Conciergerie »",
      "value-en": "See the « Concierge Service » section"
    },
    {
      "module": "view",
      displayOrder: 0,
      "right": "buildingView",
      "default": true,
      "value-fr": "Voir la rubrique « Vue d’ensemble »",
      "value-en": "See the « Overview » section"
    },
    {
      "module": "view",
      displayOrder: 6.2,
      "right": "buildingList",
      "default": true,
      // toRemove: true,
      "value-fr": "Voir la liste des immeubles",
      "value-en": "See the building list"
    },
    {
      "module": "view",
      "right": "stats",
      displayOrder: 1,
      "default": true,
      "value-fr": "Voir la rubrique « Statistiques »",
      "value-en": "See the « Stats » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seeSubject",
      "default": true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writeSubject",
      "default": true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deleteSubject",
      "default": true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seePoll",
      "default": true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writePoll",
      "default": true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deletePoll",
      "default": true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seeSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writeSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deleteSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deletePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Petites annonces »",
      "value-en": "See the « Ads » section"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "write",
      "default": true,
      "value-fr": "Publier une petite annonce",
      "value-en": "Publish an ad"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer les petites annonces publiées par un utilisateur",
      "value-en": "Delete ads published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "answer",
      "default": true,
      "value-fr": "Répondre à une petite annonce",
      "value-en": "Answer to an ad"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "see",
      "default": true,
      "value-fr": "Voir le menu du RIE",
      "value-en": "See the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "write",
      "default": true,
      "value-fr": "Publier le menu du RIE",
      "value-en": "Publish the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer le menu du RIE",
      "value-en": "Delete the company restaurant's menu"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeAll",
      "default": true,
      "value-fr": "Voir les reservations des autres utilisateurs",
      "value-en": "See other users\' reservations"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "invit",
      "default": true,
      "value-fr": "Ajouter un participant lors de la réservation d'une ressource",
      "value-en": "Add participants while making a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "reserveOther",
      "default": true,
      "value-fr": "Réserver des services ad hoc lors de la réservation",
      "value-en": "Book additionnal services during a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeResources",
      "default": true,
      "value-fr": "Voir les ressources réservables",
      "value-en": "See the bookable resources"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "addResources",
      "default": true,
      "value-fr": "Ajouter une ressource",
      "value-en": "Add a resource"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "editLimit",
      "default": true,
      toRemove: true,
      "value-fr": "Fixer un max / crédit de réservation",
      "value-en": "Fixer un max / crédit de réservation"
    }
  ]
};

let PropertyManager = {
  "for": "manager",
  "forCondoType": {
    mono: true,
    copro: true,
    office: true,
    student: true
  },
  "name": "Property Manager",
  "rights": [
    {
      "module": "integrations",
      displayOrder: 9,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Integrations »",
      "value-en": "See the « Integrations » section"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "seeAllMsgEnterprise",
      "default": true,
      "value-fr": "Voir la messagerie gestionnaire",
      "value-en": "See the manager's messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "writeAllMsgEnterprise",
      "default": true,
      "value-fr": "Ecrire un message via la messagerie gestionnaire",
      "value-en": "Write a message via manager messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "edit",
      "default": true,
      "value-fr": "Éditer un message envoyé",
      "value-en": "Edit a sent message"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un message envoyé",
      "value-en": "Delete a sent message"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "see",
      "default": false,
      "value-fr": "Voir les contacts d'urgence",
      "value-en": "View emergency contacts"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "add",
      "default": false,
      "value-fr": "Ajouter un contact d'urgence",
      "value-en": "Add emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "edit",
      "default": false,
      "value-fr": "Editer un contact d'urgence",
      "value-en": "Edit emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un contact d'urgence",
      "value-en": "Delete emergency contact"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Informations & Evénements »",
      "value-en": "See the « Informations & Events » section"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "write",
      "default": true,
      "value-fr": "Publier des informations & événements",
      "value-en": "Publish informations & events"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer les informations & événements publiés par un utilisateur",
      "value-en": "Delete informations & events published by a user"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Manuel »",
      "value-en": "See the « Manual » column"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le manuel",
      "value-en": "Edit the manual"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le manuel",
      "value-en": "Delete the manual"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Plan »",
      "value-en": "See the « Plan » column"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le plan",
      "value-en": "Edit the plan"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le plan",
      "value-en": "Delete the plan"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Incidents »",
      "value-en": "See the « Incidents » section"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "autodeclare",
      "default": true,
      "value-fr": "Déclarer un incident",
      "value-en": "Declare an incident"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "valid",
      "default": true,
      "value-fr": "Valider les incidents",
      "value-en": "Validate incidents"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "setPrivate",
      "default": true,
      "value-fr": "Passer les incidents en privé (non visibles de tous)",
      "value-en": "Classify incidents as private (not visible by everyone)"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertQuote",
      "default": true,
      "value-fr": "Insérer un devis",
      "value-en": "Insert a quote"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertNote",
      "default": true,
      "value-fr": "Insérer une note",
      "value-en": "Insert a note"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "scheduleIntervention",
      "default": true,
      "value-fr": "Programmer une intervention",
      "value-en": "Schedule an intervention"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "resolve",
      "default": true,
      "value-fr": "Résoudre un incident",
      "value-en": "Resolve an incident"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "seeManager",
      "default": true,
      "value-fr": "Voir la rubrique « Gestionnaires » ",
      "value-en": "See la rubrique « Gestionnaires » "
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "addManager",
      "default": true,
      "value-fr": "Inviter un gestionnaire",
      "value-en": "Inviter un gestionnaire"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "deleteCondoManager",
      "default": true,
      "value-fr": "Supprimer un gestionnaire d’un immeuble",
      "value-en": "Supprimer un gestionnaire d’un immeuble"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Occupants » ",
      "value-en": "See the « Occupants » section"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seeOccupantDetails",
      "default": true,
      "value-fr": "Voir le détail des occupants",
      "value-en": "See occupant details"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "addOccupant",
      "default": true,
      "value-fr": "Inviter un occupant",
      "value-en": "Invite an occupant"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un occupant d'un immeuble",
      "value-en": "Delete an occupant from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seePersonalInfos",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les informations personnelles d'un occupant",
      "value-en": "See the occupants' personal informations"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeMessenger",
      "default": true,
      "value-fr": "Voir la sous rubrique « Messagerie »",
      "value-en": "See the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeMessenger",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Messagerie » ",
      "value-en": "Modify contacts in the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeWhosWho",
      "default": true,
      "value-fr": "Voir la sous rubrique « Trombinoscope »",
      "value-en": "See the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeWhosWho",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Trombinoscope »",
      "value-en": "Edit contacts in the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeValidation",
      "default": true,
      "value-fr": "Voir la sous rubrique « Validation entrées & réservations »",
      "value-en": "See the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeValidation",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Validation entrées & réservations »",
      "value-en": "Edit contacts in the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "validAwaitingOccupant",
      "default": true,
      "value-fr": "Valider / rejeter un occupant en attente",
      "value-en": "Validate / refuse a waiting occupant"
    },
    {
      "module": "view",
      displayOrder: 3.1,
      "right": "documents",
      "default": true,
      "value-fr": "Voir la rubrique « Documents »",
      "value-en": "See the « Documents » section"
    },
    {
      "module": "view",
      displayOrder: 14,
      "right": "concierge",
      "default": true,
      "value-fr": "Voir la rubrique « Conciergerie »",
      "value-en": "See the « Concierge Service » section"
    },
    {
      "module": "view",
      displayOrder: 0,
      "right": "buildingView",
      "default": true,
      "value-fr": "Voir la rubrique « Vue d’ensemble »",
      "value-en": "See the « Overview » section"
    },
    {
      "module": "view",
      displayOrder: 6.2,
      "right": "buildingList",
      "default": true,
      // toRemove: true,
      "value-fr": "Voir la liste des immeubles",
      "value-en": "See the building list"
    },
    {
      "module": "view",
      "right": "stats",
      displayOrder: 1,
      "default": true,
      "value-fr": "Voir la rubrique « Statistiques »",
      "value-en": "See the « Stats » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seeSubject",
      "default": true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writeSubject",
      "default": true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deleteSubject",
      "default": true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seePoll",
      "default": true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writePoll",
      "default": true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deletePoll",
      "default": true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seeSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writeSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deleteSubject",
      "default": true,
      toRemove: true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deletePoll",
      "default": true,
      toRemove: true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Petites annonces »",
      "value-en": "See the « Ads » section"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "write",
      "default": true,
      "value-fr": "Publier une petite annonce",
      "value-en": "Publish an ad"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer les petites annonces publiées par un utilisateur",
      "value-en": "Delete ads published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "answer",
      "default": true,
      "value-fr": "Répondre à une petite annonce",
      "value-en": "Answer to an ad"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "see",
      "default": true,
      "value-fr": "Voir le menu du RIE",
      "value-en": "See the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "write",
      "default": true,
      "value-fr": "Publier le menu du RIE",
      "value-en": "Publish the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer le menu du RIE",
      "value-en": "Delete the company restaurant's menu"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeAll",
      "default": true,
      "value-fr": "Voir les reservations des autres utilisateurs",
      "value-en": "See other users\' reservations"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "invit",
      "default": true,
      "value-fr": "Ajouter un participant lors de la réservation d'une ressource",
      "value-en": "Add participants while making a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "reserveOther",
      "default": true,
      "value-fr": "Réserver des services ad hoc lors de la réservation",
      "value-en": "Book additionnal services during a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeResources",
      "default": true,
      "value-fr": "Voir la rubrique « Ressources »",
      "value-en": "See the bookable resources"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "addResources",
      "default": true,
      "value-fr": "Ajouter une ressource",
      "value-en": "Add a resource"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "editLimit",
      "default": true,
      toRemove: true,
      "value-fr": "Fixer un max / crédit de réservation",
      "value-en": "Fixer un max / crédit de réservation"
    }
  ]
};

let Mainteneur = {
  "for": "manager",
  "forCondoType": {
    mono: false,
    copro: false,
    office: true,
    student: false
  },
  "name": "Mainteneur",
  "rights": [
    {
      "module": "integrations",
      displayOrder: 9,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Integrations »",
      "value-en": "See the « Integrations » section"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "seeAllMsgEnterprise",
      "default": true,
      "value-fr": "Voir la messagerie gestionnaire",
      "value-en": "See the manager's messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "writeAllMsgEnterprise",
      "default": true,
      "value-fr": "Ecrire un message via la messagerie gestionnaire",
      "value-en": "Write a message via manager messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "edit",
      "default": true,
      "value-fr": "Éditer un message envoyé",
      "value-en": "Edit a sent message"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un message envoyé",
      "value-en": "Delete a sent message"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "see",
      "default": false,
      "value-fr": "Voir les contacts d'urgence",
      "value-en": "View emergency contacts"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "add",
      "default": false,
      "value-fr": "Ajouter un contact d'urgence",
      "value-en": "Add emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "edit",
      "default": false,
      "value-fr": "Editer un contact d'urgence",
      "value-en": "Edit emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un contact d'urgence",
      "value-en": "Delete emergency contact"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Informations & Evénements »",
      "value-en": "See the « Informations & Events » section"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "write",
      "default": true,
      "value-fr": "Publier des informations & événements",
      "value-en": "Publish informations & events"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer les informations & événements publiés par un utilisateur",
      "value-en": "Delete informations & events published by a user"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Manuel »",
      "value-en": "See the « Manual » column"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le manuel",
      "value-en": "Edit the manual"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le manuel",
      "value-en": "Delete the manual"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Plan »",
      "value-en": "See the « Plan » column"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le plan",
      "value-en": "Edit the plan"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le plan",
      "value-en": "Delete the plan"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Incidents »",
      "value-en": "See the « Incidents » section"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "autodeclare",
      "default": true,
      "value-fr": "Déclarer un incident",
      "value-en": "Declare an incident"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "valid",
      "default": true,
      "value-fr": "Valider les incidents",
      "value-en": "Validate incidents"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "setPrivate",
      "default": true,
      "value-fr": "Passer les incidents en privé (non visibles de tous)",
      "value-en": "Classify incidents as private (not visible by everyone)"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertQuote",
      "default": true,
      "value-fr": "Insérer un devis",
      "value-en": "Insert a quote"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertNote",
      "default": true,
      "value-fr": "Insérer une note",
      "value-en": "Insert a note"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "scheduleIntervention",
      "default": true,
      "value-fr": "Programmer une intervention",
      "value-en": "Schedule an intervention"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "resolve",
      "default": true,
      "value-fr": "Résoudre un incident",
      "value-en": "Resolve an incident"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "seeManager",
      "default": false,
      "value-fr": "Voir la rubrique « Gestionnaires » ",
      "value-en": "See the « Managers » section"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "addManager",
      "default": false,
      "value-fr": "Inviter un gestionnaire",
      "value-en": "Invite a manager"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "deleteCondoManager",
      "default": false,
      "value-fr": "Supprimer un gestionnaire d’un immeuble",
      "value-en": "Delete a manager from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Occupants » ",
      "value-en": "See the « Occupants » section"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seeOccupantDetails",
      "default": true,
      "value-fr": "Voir le détail des occupants",
      "value-en": "See occupant details"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "addOccupant",
      "default": true,
      "value-fr": "Inviter un occupant",
      "value-en": "Invite an occupant"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un occupant d'un immeuble",
      "value-en": "Delete an occupant from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seePersonalInfos",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les informations personnelles d'un occupant",
      "value-en": "See the occupants' personal informations"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeMessenger",
      "default": true,
      "value-fr": "Voir la sous rubrique « Messagerie »",
      "value-en": "See the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeMessenger",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Messagerie » ",
      "value-en": "Modify contacts in the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeWhosWho",
      "default": true,
      "value-fr": "Voir la sous rubrique « Trombinoscope »",
      "value-en": "See the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeWhosWho",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Trombinoscope »",
      "value-en": "Edit contacts in the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeValidation",
      "default": true,
      "value-fr": "Voir la sous rubrique « Validation entrées & réservations »",
      "value-en": "See the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeValidation",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Validation entrées & réservations »",
      "value-en": "Edit contacts in the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "validAwaitingOccupant",
      "default": true,
      "value-fr": "Valider / rejeter un occupant en attente",
      "value-en": "Validate / refuse a waiting occupant"
    },
    {
      "module": "view",
      displayOrder: 3.1,
      "right": "documents",
      "default": true,
      "value-fr": "Voir la rubrique « Documents »",
      "value-en": "See the « Documents » section"
    },
    {
      "module": "view",
      displayOrder: 14,
      "right": "concierge",
      "default": true,
      "value-fr": "Voir la rubrique « Conciergerie »",
      "value-en": "See the « Concierge Service » section"
    },
    {
      "module": "view",
      displayOrder: 0,
      "right": "buildingView",
      "default": false,
      "value-fr": "Voir la rubrique « Vue d’ensemble »",
      "value-en": "See the « Overview » section"
    },
    {
      "module": "view",
      displayOrder: 6.2,
      "right": "buildingList",
      "default": false,
      // toRemove: true,
      "value-fr": "Voir la liste des immeubles",
      "value-en": "See the building list"
    },
    {
      "module": "view",
      "right": "stats",
      displayOrder: 1,
      "default": false,
      "value-fr": "Voir la rubrique « Statistiques »",
      "value-en": "See the « Stats » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seeSubject",
      "default": false,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writeSubject",
      "default": false,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deleteSubject",
      "default": false,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seePoll",
      "default": false,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writePoll",
      "default": false,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deletePoll",
      "default": false,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deleteSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deletePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Petites annonces »",
      "value-en": "See the « Ads » section"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "write",
      "default": false,
      "value-fr": "Publier une petite annonce",
      "value-en": "Publish an ad"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer les petites annonces publiées par un utilisateur",
      "value-en": "Delete ads published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "answer",
      "default": true,
      "value-fr": "Répondre à une petite annonce",
      "value-en": "Answer to an ad"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "see",
      "default": true,
      "value-fr": "Voir le menu du RIE",
      "value-en": "See the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "write",
      "default": true,
      "value-fr": "Publier le menu du RIE",
      "value-en": "Publish the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer le menu du RIE",
      "value-en": "Delete the company restaurant's menu"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeAll",
      "default": true,
      "value-fr": "Voir les reservations des autres utilisateurs",
      "value-en": "See other users\' reservations"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "invit",
      "default": true,
      "value-fr": "Ajouter un participant lors de la réservation d'une ressource",
      "value-en": "Add participants while making a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "reserveOther",
      "default": true,
      "value-fr": "Réserver des services ad hoc lors de la réservation",
      "value-en": "Book additionnal services during a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeResources",
      "default": true,
      "value-fr": "Voir la rubrique « Ressources »",
      "value-en": "See the bookable resources"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "addResources",
      "default": true,
      "value-fr": "Ajouter une ressource",
      "value-en": "Add a resource"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "editLimit",
      "default": true,
      toRemove: true,
      "value-fr": "Fixer un max / crédit de réservation",
      "value-en": "Fixer un max / crédit de réservation"
    }
  ]
};

let OperateursRestauration = {
  "for": "manager",
  "forCondoType": {
    mono: false,
    copro: false,
    office: true,
    student: true
  },
  "name": "Opérateurs Restauration",
  "rights": [
    {
      "module": "integrations",
      displayOrder: 9,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Integrations »",
      "value-en": "See the « Integrations » section"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "seeAllMsgEnterprise",
      "default": false,
      "value-fr": "Voir la messagerie gestionnaire",
      "value-en": "See the manager's messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "writeAllMsgEnterprise",
      "default": false,
      "value-fr": "Ecrire un message via la messagerie gestionnaire",
      "value-en": "Write a message via manager messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "edit",
      "default": false,
      "value-fr": "Éditer un message envoyé",
      "value-en": "Edit a sent message"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un message envoyé",
      "value-en": "Delete a sent message"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "see",
      "default": false,
      "value-fr": "Voir les contacts d'urgence",
      "value-en": "View emergency contacts"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "add",
      "default": false,
      "value-fr": "Ajouter un contact d'urgence",
      "value-en": "Add emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "edit",
      "default": false,
      "value-fr": "Editer un contact d'urgence",
      "value-en": "Edit emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un contact d'urgence",
      "value-en": "Delete emergency contact"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Informations & Evénements »",
      "value-en": "See the « Informations & Events » section"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "write",
      "default": false,
      "value-fr": "Publier des informations & événements",
      "value-en": "Publish informations & events"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer les informations & événements publiés par un utilisateur",
      "value-en": "Delete informations & events published by a user"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Manuel »",
      "value-en": "See the « Manual » column"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "write",
      "default": false,
      "value-fr": "Modifier le manuel",
      "value-en": "Edit the manual"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "delete",
      "default": false,
      "value-fr": "Effacer le manuel",
      "value-en": "Delete the manual"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Plan »",
      "value-en": "See the « Plan » column"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "write",
      "default": false,
      "value-fr": "Modifier le plan",
      "value-en": "Edit the plan"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "delete",
      "default": false,
      "value-fr": "Effacer le plan",
      "value-en": "Delete the plan"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Incidents »",
      "value-en": "See the « Incidents » section"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "autodeclare",
      "default": false,
      "value-fr": "Déclarer un incident",
      "value-en": "Declare an incident"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "valid",
      "default": false,
      "value-fr": "Valider les incidents",
      "value-en": "Validate incidents"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "setPrivate",
      "default": false,
      "value-fr": "Passer les incidents en privé (non visibles de tous)",
      "value-en": "Classify incidents as private (not visible by everyone)"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertQuote",
      "default": false,
      "value-fr": "Insérer un devis",
      "value-en": "Insert a quote"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertNote",
      "default": false,
      "value-fr": "Insérer une note",
      "value-en": "Insert a note"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "scheduleIntervention",
      "default": false,
      "value-fr": "Programmer une intervention",
      "value-en": "Schedule an intervention"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "resolve",
      "default": false,
      "value-fr": "Résoudre un incident",
      "value-en": "Resolve an incident"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "seeManager",
      "default": false,
      "value-fr": "Voir la rubrique « Gestionnaires » ",
      "value-en": "See the « Managers » section"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "addManager",
      "default": false,
      "value-fr": "Inviter un gestionnaire",
      "value-en": "Invite a manager"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "deleteCondoManager",
      "default": false,
      "value-fr": "Supprimer un gestionnaire d’un immeuble",
      "value-en": "Delete a manager from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Occupants » ",
      "value-en": "See the « Occupants » section"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "addOccupant",
      "default": false,
      "value-fr": "Inviter un occupant",
      "value-en": "Invite an occupant"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un occupant d'un immeuble",
      "value-en": "Delete an occupant from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seePersonalInfos",
      "default": false,
      toRemove: true,
      "value-fr": "Voir les informations personnelles d'un occupant",
      "value-en": "See the occupants' personal informations"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeMessenger",
      "default": false,
      "value-fr": "Voir la sous rubrique « Messagerie »",
      "value-en": "See the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeMessenger",
      "default": false,
      "value-fr": "Modifier les contacts de la sous rubrique « Messagerie » ",
      "value-en": "Modify contacts in the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeWhosWho",
      "default": false,
      "value-fr": "Voir la sous rubrique « Trombinoscope »",
      "value-en": "See the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeWhosWho",
      "default": false,
      "value-fr": "Modifier les contacts de la sous rubrique « Trombinoscope »",
      "value-en": "Edit contacts in the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeValidation",
      "default": false,
      "value-fr": "Voir la sous rubrique « Validation entrées & réservations »",
      "value-en": "See the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeValidation",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Validation entrées & réservations »",
      "value-en": "Edit contacts in the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "validAwaitingOccupant",
      "default": true,
      "value-fr": "Valider / rejeter un occupant en attente",
      "value-en": "Validate / refuse a waiting occupant"
    },
    {
      "module": "view",
      displayOrder: 3.1,
      "right": "documents",
      "default": true,
      "value-fr": "Voir la rubrique « Documents »",
      "value-en": "See the « Documents » section"
    },
    {
      "module": "view",
      displayOrder: 14,
      "right": "concierge",
      "default": true,
      "value-fr": "Voir la rubrique « Conciergerie »",
      "value-en": "See the « Concierge Service » section"
    },
    {
      "module": "view",
      displayOrder: 0,
      "right": "buildingView",
      "default": false,
      "value-fr": "Voir la rubrique « Vue d’ensemble »",
      "value-en": "See the « Overview » section"
    },
    {
      "module": "view",
      displayOrder: 6.2,
      "right": "buildingList",
      "default": false,
      // toRemove: true,
      "value-fr": "Voir la liste des immeubles",
      "value-en": "See the building list"
    },
    {
      "module": "view",
      "right": "stats",
      displayOrder: 1,
      "default": false,
      "value-fr": "Voir la rubrique « Statistiques »",
      "value-en": "See the « Stats » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seeSubject",
      "default": false,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writeSubject",
      "default": false,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deleteSubject",
      "default": false,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seePoll",
      "default": false,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writePoll",
      "default": false,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deletePoll",
      "default": false,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deleteSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deletePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Petites annonces »",
      "value-en": "See the « Ads » section"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "write",
      "default": false,
      "value-fr": "Publier une petite annonce",
      "value-en": "Publish an ad"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer les petites annonces publiées par un utilisateur",
      "value-en": "Delete ads published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "answer",
      "default": true,
      "value-fr": "Répondre à une petite annonce",
      "value-en": "Answer to an ad"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "see",
      "default": true,
      "value-fr": "Voir le menu du RIE",
      "value-en": "See the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "write",
      "default": true,
      "value-fr": "Publier le menu du RIE",
      "value-en": "Publish the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer le menu du RIE",
      "value-en": "Delete the company restaurant's menu"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeAll",
      "default": true,
      "value-fr": "Voir les reservations des autres utilisateurs",
      "value-en": "See other users\' reservations"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "invit",
      "default": true,
      "value-fr": "Ajouter un participant lors de la réservation d'une ressource",
      "value-en": "Add participants while making a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "reserveOther",
      "default": true,
      "value-fr": "Réserver des services ad hoc lors de la réservation",
      "value-en": "Book additionnal services during a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeResources",
      "default": false,
      "value-fr": "Voir la rubrique « Ressources »",
      "value-en": "See the bookable resources"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "addResources",
      "default": false,
      "value-fr": "Ajouter une ressource",
      "value-en": "Add a resource"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "editLimit",
      "default": false,
      toRemove: true,
      "value-fr": "Fixer un max / crédit de réservation",
      "value-en": "Fixer un max / crédit de réservation"
    }
  ]
};

let PCSecurite = {
  "for": "manager",
  "forCondoType": {
    mono: false,
    copro: false,
    office: true,
    student: true
  },
  "name": "PC Sécurité",
  "rights": [
    {
      "module": "integrations",
      displayOrder: 9,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Integrations »",
      "value-en": "See the « Integrations » section"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "seeAllMsgEnterprise",
      "default": true,
      "value-fr": "Voir la messagerie gestionnaire",
      "value-en": "See the manager's messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "writeAllMsgEnterprise",
      "default": true,
      "value-fr": "Ecrire un message via la messagerie gestionnaire",
      "value-en": "Write a message via manager messenger"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "edit",
      "default": true,
      "value-fr": "Éditer un message envoyé",
      "value-en": "Edit a sent message"
    },
    {
      "module": "messenger",
      displayOrder: 9,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un message envoyé",
      "value-en": "Delete a sent message"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "see",
      "default": false,
      "value-fr": "Voir les contacts d'urgence",
      "value-en": "View emergency contacts"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "add",
      "default": false,
      "value-fr": "Ajouter un contact d'urgence",
      "value-en": "Add emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "edit",
      "default": false,
      "value-fr": "Editer un contact d'urgence",
      "value-en": "Edit emergency contact"
    },
    {
      "module": "emergencyContact",
      displayOrder: 8.1,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer un contact d'urgence",
      "value-en": "Delete emergency contact"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Informations & Evénements »",
      "value-en": "See the « Informations & Events » section"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "write",
      "default": false,
      "value-fr": "Publier des informations & événements",
      "value-en": "Publish informations & events"
    },
    {
      "module": "actuality",
      displayOrder: 3,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer les informations & événements publiés par un utilisateur",
      "value-en": "Delete informations & events published by a user"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Manuel »",
      "value-en": "See the « Manual » column"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "write",
      "default": false,
      "value-fr": "Modifier le manuel",
      "value-en": "Edit the manual"
    },
    {
      "module": "manual",
      displayOrder: 4,
      "right": "delete",
      "default": false,
      "value-fr": "Effacer le manuel",
      "value-en": "Delete the manual"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "see",
      "default": true,
      "value-fr": "Voir la colonne « Plan »",
      "value-en": "See the « Plan » column"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "write",
      "default": true,
      "value-fr": "Modifier le plan",
      "value-en": "Edit the plan"
    },
    {
      "module": "map",
      displayOrder: 5,
      "right": "delete",
      "default": true,
      "value-fr": "Effacer le plan",
      "value-en": "Delete the plan"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Incidents »",
      "value-en": "See the « Incidents » section"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "autodeclare",
      "default": true,
      "value-fr": "Déclarer un incident",
      "value-en": "Declare an incident"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "valid",
      "default": false,
      "value-fr": "Valider les incidents",
      "value-en": "Validate incidents"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "setPrivate",
      "default": false,
      "value-fr": "Passer les incidents en privé (non visibles de tous)",
      "value-en": "Classify incidents as private (not visible by everyone)"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertQuote",
      "default": true,
      "value-fr": "Insérer un devis",
      "value-en": "Insert a quote"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "insertNote",
      "default": true,
      "value-fr": "Insérer une note",
      "value-en": "Insert a note"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "scheduleIntervention",
      "default": false,
      "value-fr": "Programmer une intervention",
      "value-en": "Schedule an intervention"
    },
    {
      "module": "incident",
      displayOrder: 2,
      "right": "resolve",
      "default": false,
      "value-fr": "Résoudre un incident",
      "value-en": "Resolve an incident"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "seeManager",
      "default": false,
      "value-fr": "Voir la rubrique « Gestionnaires » ",
      "value-en": "See the « Managers » section"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "addManager",
      "default": false,
      "value-fr": "Inviter un gestionnaire",
      "value-en": "Invite a manager"
    },
    {
      "module": "managerList",
      displayOrder: 6.1,
      "right": "deleteCondoManager",
      "default": false,
      "value-fr": "Supprimer un gestionnaire d’un immeuble",
      "value-en": "Delete a manager from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "see",
      "default": true,
      "value-fr": "Voir la rubrique « Occupants » ",
      "value-en": "See the « Occupants » section"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seeOccupantDetails",
      "default": true,
      "value-fr": "Voir le détail des occupants",
      "value-en": "See occupant details"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "addOccupant",
      "default": true,
      "value-fr": "Inviter un occupant",
      "value-en": "Invite an occupant"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "delete",
      "default": true,
      "value-fr": "Supprimer un occupant d'un immeuble",
      "value-en": "Delete an occupant from a building"
    },
    {
      "module": "trombi",
      displayOrder: 6,
      "right": "seePersonalInfos",
      "default": true,
      toRemove: true,
      "value-fr": "Voir les informations personnelles d'un occupant",
      "value-en": "See the occupants' personal informations"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeMessenger",
      "default": false,
      "value-fr": "Voir la sous rubrique « Messagerie »",
      "value-en": "See the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeMessenger",
      "default": false,
      "value-fr": "Modifier les contacts de la sous rubrique « Messagerie » ",
      "value-en": "Modify contacts in the « Messenger » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeWhosWho",
      "default": false,
      "value-fr": "Voir la sous rubrique « Trombinoscope »",
      "value-en": "See the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeWhosWho",
      "default": false,
      "value-fr": "Modifier les contacts de la sous rubrique « Trombinoscope »",
      "value-en": "Edit contacts in the « Who's who » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "seeValidation",
      "default": false,
      "value-fr": "Voir la sous rubrique « Validation entrées & réservations »",
      "value-en": "See the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "writeValidation",
      "default": true,
      "value-fr": "Modifier les contacts de la sous rubrique « Validation entrées & réservations »",
      "value-en": "Edit contacts in the « Entries and bookings validation » sub-section"
    },
    {
      "module": "setContact",
      displayOrder: 8,
      "right": "validAwaitingOccupant",
      "default": true,
      "value-fr": "Valider / rejeter un occupant en attente",
      "value-en": "Validate / refuse a waiting occupant"
    },
    {
      "module": "view",
      displayOrder: 3.1,
      "right": "documents",
      "default": true,
      "value-fr": "Voir la rubrique « Documents »",
      "value-en": "See the « Documents » section"
    },
    {
      "module": "view",
      displayOrder: 14,
      "right": "concierge",
      "default": true,
      "value-fr": "Voir la rubrique « Conciergerie »",
      "value-en": "See the « Concierge Service » section"
    },
    {
      "module": "view",
      displayOrder: 0,
      "right": "buildingView",
      "default": false,
      "value-fr": "Voir la rubrique « Vue d’ensemble »",
      "value-en": "See the « Overview » section"
    },
    {
      "module": "view",
      displayOrder: 6.2,
      "right": "buildingList",
      "default": false,
      // toRemove: true,
      "value-fr": "Voir la liste des immeubles",
      "value-en": "See the building list"
    },
    {
      "module": "view",
      "right": "stats",
      displayOrder: 1,
      "default": false,
      "value-fr": "Voir la rubrique « Statistiques »",
      "value-en": "See the « Stats » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seeSubject",
      "default": false,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writeSubject",
      "default": false,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deleteSubject",
      "default": false,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "seePoll",
      "default": false,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "writePoll",
      "default": false,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forum",
      displayOrder: 11,
      "right": "deletePoll",
      "default": false,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Voir la rubrique « Forum »",
      "value-en": "See the « Forum » section"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writeSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux posts du forum",
      "value-en": "Publish and answer forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deleteSubject",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les posts du forum publiés par un utilisateur",
      "value-en": "Delete forum posts published by a user"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "seePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Voir les sondages du forum",
      "value-en": "See forum posts"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "writePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Publier et répondre aux sondages du forum",
      "value-en": "Publish and answer forum polls"
    },
    {
      "module": "forumCS",
      displayOrder: 12,
      "right": "deletePoll",
      "default": false,
      toRemove: true,
      "value-fr": "Supprimer les sondages du forum publiés par un utilisateur",
      "value-en": "Delete forum polls published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "see",
      "default": false,
      "value-fr": "Voir la rubrique « Petites annonces »",
      "value-en": "See the « Ads » section"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "write",
      "default": false,
      "value-fr": "Publier une petite annonce",
      "value-en": "Publish an ad"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer les petites annonces publiées par un utilisateur",
      "value-en": "Delete ads published by a user"
    },
    {
      "module": "ads",
      displayOrder: 13,
      "right": "answer",
      "default": true,
      "value-fr": "Répondre à une petite annonce",
      "value-en": "Answer to an ad"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "see",
      "default": true,
      "value-fr": "Voir le menu du RIE",
      "value-en": "See the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "write",
      "default": false,
      "value-fr": "Publier le menu du RIE",
      "value-en": "Publish the company restaurant's menu"
    },
    {
      "module": "restaurant",
      toRemove: true,
      "right": "delete",
      "default": false,
      "value-fr": "Supprimer le menu du RIE",
      "value-en": "Delete the company restaurant's menu"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeAll",
      "default": false,
      "value-fr": "Voir les reservations des autres utilisateurs",
      "value-en": "See other users\' reservations"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "invit",
      "default": false,
      "value-fr": "Ajouter un participant lors de la réservation d'une ressource",
      "value-en": "Add participants while making a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "reserveOther",
      "default": false,
      "value-fr": "Réserver des services ad hoc lors de la réservation",
      "value-en": "Book additionnal services during a reservation"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "seeResources",
      "default": false,
      "value-fr": "Voir la rubrique « Ressources »",
      "value-en": "See the bookable resources"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "addResources",
      "default": false,
      "value-fr": "Ajouter une ressource",
      "value-en": "Add a resource"
    },
    {
      "module": "reservation",
      displayOrder: 10,
      "right": "editLimit",
      "default": false,
      toRemove: true,
      "value-fr": "Fixer un max / crédit de réservation",
      "value-en": "Fixer un max / crédit de réservation"
    }
  ]
};

export { AssetManager, PropertyManager, Mainteneur, OperateursRestauration, PCSecurite }
