const generalEN = {
  loading: 'Loading',
  cancel: 'Cancel',
  confirm: 'Confirm'
}

const generalFR = {
  loading: 'Traitement',
  cancel: 'Annuler',
  confirm: 'Confirmer'
}

export {
  generalEN,
  generalFR
}
