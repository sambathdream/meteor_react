const activeDirectoryEN = {
  modalTitle: 'Active Directory',
  modalConfirmationMessage: 'Are you sure you want to update the list of users with the latest version of your Active Directory?',
  deferredDate: 'Deferred date',
  genericAlertMessage: 'Impossible to connect',
  withoutAccessMessage: 'This building does not have access to Active Directory',
  completedMessage: 'Process completed',
  refreshButton: 'Update from Active Directory'
}

const activeDirectoryFR = {
  modalTitle: 'Active Directory',
  modalConfirmationMessage: 'Êtes vous sur de vouloir mettre à jour la liste des utilisateurs avec la dernière version de votre Active Directory ?',
  deferredDate: 'Date différée',
  genericAlertMessage: 'Impossible de se connecter',
  withoutAccessMessage: 'Ce bâtiment n\'a pas accès à Active Directory',
  completedMessage: 'Processus terminé',
  refreshButton: 'Actualiser à partir d\'Active Directory'
}

export {
  activeDirectoryEN,
  activeDirectoryFR
}
