import { Meteor } from 'meteor/meteor'

const mimeType = (type) => {
  return type.split('/')[0]
}

async function afterUpload (_fileRef) {
  if (/png|jpe?g/i.test(_fileRef.extension || '')) {
    Meteor.setTimeout(() => {
      createThumbnails(this, _fileRef, (error) => {
        if (error) {
          console.error(error)
        }
      })
    }, 1024)
  }
}

StatsCSV = new FilesCollection({
  collectionName: 'StatsCSV',
  downloadRoute: '/download'
})

VideoThumb = new FilesCollection({
  collectionName: 'VideoThumb',
  downloadRoute: '/download',
  onAfterUpload: afterUpload
})

ForumFiles = new FilesCollection({
  collectionName: 'ForumFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

export const MarketPlaceFiles = new FilesCollection({
  collectionName: 'MarketPlaceFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

export const BasketFiles = new FilesCollection({
  collectionName: 'BasketFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

UserDocumentFiles = new FilesCollection({
  collectionName: 'userDocumentFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    } else if (mimeType(file.type) === 'video') {
      return 'VIDEO_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

MessengerFiles = new FilesCollection({
  collectionName: 'MessengerFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

CondoPhotoFiles = new FilesCollection({
  collectionName: 'condoPhotoFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

CondoLogoFiles = new FilesCollection({
  collectionName: 'condoLogoFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

EmergencyContactPhotoFiles = new FilesCollection({
  collectionName: 'emergencyContactPhotoFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

UserFiles = new FilesCollection({
  collectionName: 'UserFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

UsersJustifFiles = new FilesCollection({
  collectionName: 'UsersJustifFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

CondoDocuments = new FilesCollection({
  collectionName: 'CondoDocuments',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760)
      {return ("heavy_file_up_to_10mb")}
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

SchoolPhotos = new FilesCollection({
  collectionName: 'SchoolPhotos',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file['mime-type'] === 'image/svg+xml') {
      return 'SVG_Files_not_alowed'
    }
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})

IncidentFiles = new FilesCollection({
  collectionName: 'IncidentFiles',
  downloadRoute: '/download',
  onBeforeUpload: function (file) {
    if (file.size >= 10485760) {
      return ('heavy_file_up_to_10mb')
    }
    return true
  },
  onBeforeRemove: function (file) {
    return true
  },
  onAfterUpload: afterUpload
})
